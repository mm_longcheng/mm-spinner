#ifndef __mm_db_mysql_h__
#define __mm_db_mysql_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_spinlock.h"
#include "core/mm_string.h"
#include "core/mm_list.h"

#include "container/mm_holder32.h"

#include "pthread.h"
#include <mysql.h>

#define MM_DB_MYSQL_TRYTIMES 3;
// alloc value_size roundup32 * MM_REAL_ESCAPE_STRING_FACTOR size to cache real escape string.
#define MM_REAL_ESCAPE_STRING_FACTOR 2

enum mm_db_mysql_state_t
{
	db_mysql_state_unknown = -1,// -1 is unknown.
	db_mysql_state_success =  0,//  0 is success.
	db_mysql_state_failure =  1,//  1 is failure.
};

// config for mm_db_mysql.
struct mm_db_mysql_config
{
	struct mm_string node;    // default "127.0.0.1"
	struct mm_string username;// default "mm"
	struct mm_string password;// default "xxxxxx"
	struct mm_string basename;// default "mm"
	struct mm_string charsets;// default "utf8"
	mm_ushort_t port;         // default "3306"
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_db_mysql_config_init(struct mm_db_mysql_config* p);
MM_EXPORT_DLL void mm_db_mysql_config_destroy(struct mm_db_mysql_config* p);
//////////////////////////////////////////////////////////////////////////
// 0 is same.
MM_EXPORT_DLL int mm_db_mysql_config_compare(struct mm_db_mysql_config* p,struct mm_db_mysql_config* t);
// assign.
MM_EXPORT_DLL void mm_db_mysql_config_assign(struct mm_db_mysql_config* p,struct mm_db_mysql_config* t);
//////////////////////////////////////////////////////////////////////////
struct mm_db_mysql
{
	struct mm_db_mysql_config config;// mysql config.
	mm_atomic_t locker;// locker for this instance.
	MYSQL context;// mysql context.
	MYSQL_ROW row;// current row for query.
	MYSQL_RES* result;// current result.strong ref.
	int trytimes;// try connect times.default is MM_DB_MYSQL_TRYTIMES.
	int context_state;// mysql context connect state.mm_db_mysql_state_t.
	unsigned long* field_length;// weak ref for MYSQL_RES
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_db_mysql_init(struct mm_db_mysql* p);
MM_EXPORT_DLL void mm_db_mysql_destroy(struct mm_db_mysql* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_db_mysql_lock(struct mm_db_mysql* p);
MM_EXPORT_DLL void mm_db_mysql_unlock(struct mm_db_mysql* p);
//////////////////////////////////////////////////////////////////////////
// config for connect.
MM_EXPORT_DLL void mm_db_mysql_set_config(struct mm_db_mysql* p, struct mm_db_mysql_config* config);
MM_EXPORT_DLL struct mm_db_mysql_config* mm_db_mysql_get_config(struct mm_db_mysql* p);
// try connect times.default is MM_DB_MYSQL_TRYTIMES.
MM_EXPORT_DLL void mm_db_mysql_set_trytimes(struct mm_db_mysql* p,int trytimes);
MM_EXPORT_DLL int mm_db_mysql_get_trytimes(struct mm_db_mysql* p);
//////////////////////////////////////////////////////////////////////////
// get mysql context.
MM_EXPORT_DLL MYSQL* mm_db_mysql_get_context(struct mm_db_mysql* p);
// context connect state.you must make sure state is success,if not any api for MYSQL will crash.
// default is unknown.
// -1 is unknown.
//  0 is success.
//  1 is failure.
MM_EXPORT_DLL int mm_db_mysql_get_context_state(struct mm_db_mysql* p);
//////////////////////////////////////////////////////////////////////////
// try connect to target.return context_state.
// -1 is unknown.
//  0 is success.
//  1 is failure.
MM_EXPORT_DLL int mm_db_mysql_connect(struct mm_db_mysql* p);
// check context_state and reconnect if need,and then query result.
// -1 is unknown.
//  0 is success.
//  1 is failure.
MM_EXPORT_DLL int mm_db_mysql_query(struct mm_db_mysql* p, const char* sql);
// ping remote.
// Zero if the connection to the server is active. 
// Nonzero if an error occurred. 
// A nonzero return does not indicate whether the MySQL server itself is down; 
// the connection might be broken for other reasons such as network problems.
MM_EXPORT_DLL int mm_db_mysql_ping(struct mm_db_mysql* p);
// set auto commit flag.sets autocommit mode on if mode is 1, off if mode is 0.
// Zero for success. Nonzero if an error occurred.
MM_EXPORT_DLL int mm_db_mysql_autocommit(struct mm_db_mysql* p, int flag);
// commit immediately.
// Zero for success. Nonzero if an error occurred.
MM_EXPORT_DLL int mm_db_mysql_commit(struct mm_db_mysql* p);
//////////////////////////////////////////////////////////////////////////
// real escape string.
MM_EXPORT_DLL unsigned long mm_db_mysql_real_escape_string(struct mm_db_mysql* p, char* export_buff,const char* import_buff, size_t import_size);
//////////////////////////////////////////////////////////////////////////
// get if has next row for result.
// 1 has row.
// 0 not row.
MM_EXPORT_DLL int mm_db_mysql_get_has_row(struct mm_db_mysql* p);
// get current result field count.you must check before GetField for read value.
MM_EXPORT_DLL unsigned int mm_db_mysql_get_field_count(struct mm_db_mysql* p);
// get current result affected rows.
MM_EXPORT_DLL mm_uint64_t mm_db_mysql_get_affected_rows(struct mm_db_mysql* p);
// free current result.
MM_EXPORT_DLL void mm_db_mysql_free_current_result(struct mm_db_mysql* p);
// free unused result.
MM_EXPORT_DLL void mm_db_mysql_free_unused_result(struct mm_db_mysql* p);
// next result for query.if first will only call mysql_store_result.
// note: you must free current result to make sure not memory leak.
MM_EXPORT_DLL void mm_db_mysql_store_next_result(struct mm_db_mysql* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_db_mysql_get_field_uint64(struct mm_db_mysql* p, int idx, mm_uint64_t* v);
MM_EXPORT_DLL void mm_db_mysql_get_field_sint64(struct mm_db_mysql* p, int idx, mm_sint64_t* v);

MM_EXPORT_DLL void mm_db_mysql_get_field_uint32(struct mm_db_mysql* p, int idx, mm_uint32_t* v);
MM_EXPORT_DLL void mm_db_mysql_get_field_sint32(struct mm_db_mysql* p, int idx, mm_sint32_t* v);

MM_EXPORT_DLL void mm_db_mysql_get_field_uint16(struct mm_db_mysql* p, int idx, mm_uint16_t* v);
MM_EXPORT_DLL void mm_db_mysql_get_field_sint16(struct mm_db_mysql* p, int idx, mm_sint16_t* v);

MM_EXPORT_DLL void mm_db_mysql_get_field_uint8(struct mm_db_mysql* p, int idx, mm_uint8_t* v);
MM_EXPORT_DLL void mm_db_mysql_get_field_sint8(struct mm_db_mysql* p, int idx, mm_sint8_t* v);

MM_EXPORT_DLL void mm_db_mysql_get_field_float32(struct mm_db_mysql* p, int idx, mm_float32_t* v);
MM_EXPORT_DLL void mm_db_mysql_get_field_float64(struct mm_db_mysql* p, int idx, mm_float64_t* v);

MM_EXPORT_DLL void mm_db_mysql_get_field_string(struct mm_db_mysql* p, int idx, struct mm_string* v);
//////////////////////////////////////////////////////////////////////////
struct mm_db_mysql_section
{
	struct mm_db_mysql_config config;// mysql config.
	struct mm_list_head list;// silk strong ref list.
	mm_atomic_t list_locker;
	mm_atomic_t locker;// locker for this instance.
	pthread_key_t thread_key;// thread key.
	mm_uint32_t sid;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_db_mysql_section_init(struct mm_db_mysql_section* p);
MM_EXPORT_DLL void mm_db_mysql_section_destroy(struct mm_db_mysql_section* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_db_mysql_section_lock(struct mm_db_mysql_section* p);
MM_EXPORT_DLL void mm_db_mysql_section_unlock(struct mm_db_mysql_section* p);
//////////////////////////////////////////////////////////////////////////
// config for connect.
MM_EXPORT_DLL void mm_db_mysql_section_set_config(struct mm_db_mysql_section* p, struct mm_db_mysql_config* config);
MM_EXPORT_DLL struct mm_db_mysql_config* mm_db_mysql_section_get_config(struct mm_db_mysql_section* p);
// sid for connect.
MM_EXPORT_DLL void mm_db_mysql_section_set_sid(struct mm_db_mysql_section* p, mm_uint32_t sid);
MM_EXPORT_DLL mm_uint32_t mm_db_mysql_section_get_sid(struct mm_db_mysql_section* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_db_mysql_section_connect(struct mm_db_mysql_section* p);
//////////////////////////////////////////////////////////////////////////
// get current thread conn.
MM_EXPORT_DLL struct mm_db_mysql* mm_db_mysql_section_thread_instance(struct mm_db_mysql_section* p);
// clear all thread conn.
MM_EXPORT_DLL void mm_db_mysql_section_clear(struct mm_db_mysql_section* p);
//////////////////////////////////////////////////////////////////////////
struct mm_db_mysql_contact
{
	struct mm_holder_u32_vpt holder;
	mm_atomic_t holder_locker;
	mm_atomic_t locker;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_db_mysql_contact_init(struct mm_db_mysql_contact* p);
MM_EXPORT_DLL void mm_db_mysql_contact_destroy(struct mm_db_mysql_contact* p);
//////////////////////////////////////////////////////////////////////////
// lock to make sure the mt_contact is thread safe.
MM_EXPORT_DLL void mm_db_mysql_contact_lock(struct mm_db_mysql_contact* p);
// unlock mt_contact.
MM_EXPORT_DLL void mm_db_mysql_contact_unlock(struct mm_db_mysql_contact* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL struct mm_db_mysql_section* mm_db_mysql_contact_add(struct mm_db_mysql_contact* p,int sid,struct mm_db_mysql_config* config);
MM_EXPORT_DLL void mm_db_mysql_contact_rmv(struct mm_db_mysql_contact* p,int sid);
MM_EXPORT_DLL struct mm_db_mysql_section* mm_db_mysql_contact_get(struct mm_db_mysql_contact* p,int sid);
MM_EXPORT_DLL struct mm_db_mysql_section* mm_db_mysql_contact_get_instance(struct mm_db_mysql_contact* p,int sid,struct mm_db_mysql_config* config);
MM_EXPORT_DLL void mm_db_mysql_contact_clear(struct mm_db_mysql_contact* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_db_mysql_h__