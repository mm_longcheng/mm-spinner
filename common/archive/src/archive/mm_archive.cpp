#include "mm_archive.h"
#include <istream>
namespace mm
{
	//////////////////////////////////////////////////////////////////////////
	i_archive::i_archive( struct mm_streambuf& _streambuf)
		:streambuf(_streambuf)
	{

	}

	const i_archive& i_archive::operator >> (mm_schar_t& val)const 
	{
		mm_streambuf_sgetn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_schar_t));
		return *this;
	}

	const i_archive& i_archive::operator >> (mm_uchar_t& val)const 
	{
		mm_streambuf_sgetn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_uchar_t));
		return *this;
	}

	const i_archive& i_archive::operator >> (mm_sshort_t& val)const 
	{
		mm_streambuf_sgetn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_sshort_t));
		return *this;
	}

	const i_archive& i_archive::operator >> (mm_ushort_t& val)const 
	{
		mm_streambuf_sgetn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_ushort_t));
		return *this;
	}

	const i_archive& i_archive::operator >> (mm_sint_t& val)const 
	{
		mm_streambuf_sgetn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_sint_t));
		return *this;
	}

	const i_archive& i_archive::operator >> (mm_uint_t& val)const 
	{
		mm_streambuf_sgetn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_uint_t));
		return *this;
	}

	const i_archive& i_archive::operator >> (mm_slong_t& val)const 
	{
		mm_streambuf_sgetn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_slong_t));
		return *this;
	}

	const i_archive& i_archive::operator >> (mm_ulong_t& val)const 
	{
		mm_streambuf_sgetn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_ulong_t));
		return *this;
	}

	const i_archive& i_archive::operator >> (mm_sllong_t& val)const 
	{
		mm_streambuf_sgetn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_sllong_t));
		return *this;
	}

	const i_archive& i_archive::operator >> (mm_ullong_t& val)const 
	{
		mm_streambuf_sgetn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_ullong_t));
		return *this;
	}

	const i_archive& i_archive::operator >> (float& val)const 
	{
		mm_streambuf_sgetn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(float));
		return *this;
	}

	const i_archive& i_archive::operator >> (double& val)const 
	{
		mm_streambuf_sgetn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(double));
		return *this;
	}

	const i_archive& i_archive::operator >> (std::string& val)const 
	{
		size_t size = 0;
		mm_streambuf_sgetn(&this->streambuf,(mm_uint8_t*)&size, 0, sizeof(size_t));
		if ( 0 != size )
		{
			val.resize(size);
			mm_streambuf_sgetn(&this->streambuf,(mm_uint8_t*)val.data(), 0, size);
		}
		return *this;
	}

	const i_archive& i_archive::operator >> (bool& val)const 
	{
		mm_streambuf_sgetn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(bool));
		return *this;
	}
	//////////////////////////////////////////////////////////////////////////
	o_archive::o_archive( struct mm_streambuf& _streambuf)
		:streambuf(_streambuf)
	{

	}

	o_archive& o_archive::operator << (const mm_schar_t& val)
	{
		mm_streambuf_sputn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_schar_t));
		return *this;
	}

	o_archive& o_archive::operator << (const mm_uchar_t& val)
	{
		mm_streambuf_sputn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_uchar_t));
		return *this;
	}

	o_archive& o_archive::operator << (const mm_sshort_t& val)
	{
		mm_streambuf_sputn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_sshort_t));
		return *this;
	}

	o_archive& o_archive::operator << (const mm_ushort_t& val)
	{
		mm_streambuf_sputn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_ushort_t));
		return *this;
	}

	o_archive& o_archive::operator << (const mm_sint_t& val)
	{
		mm_streambuf_sputn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_sint_t));
		return *this;
	}

	o_archive& o_archive::operator << (const mm_uint_t& val)
	{
		mm_streambuf_sputn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_uint_t));
		return *this;
	}

	o_archive& o_archive::operator << (const mm_slong_t& val)
	{
		mm_streambuf_sputn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_slong_t));
		return *this;
	}

	o_archive& o_archive::operator << (const mm_ulong_t& val)
	{
		mm_streambuf_sputn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_ulong_t));
		return *this;
	}

	o_archive& o_archive::operator << (const mm_sllong_t& val)
	{
		mm_streambuf_sputn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_sllong_t));
		return *this;
	}

	o_archive& o_archive::operator << (const mm_ullong_t& val)
	{
		mm_streambuf_sputn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(mm_ullong_t));
		return *this;
	}

	o_archive& o_archive::operator << (const float& val)
	{
		mm_streambuf_sputn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(float));
		return *this;
	}

	o_archive& o_archive::operator << (const double& val)
	{
		mm_streambuf_sputn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(double));
		return *this;
	}

	o_archive& o_archive::operator << (const std::string& val)
	{
		size_t size = val.size();
		mm_streambuf_sputn(&this->streambuf,(mm_uint8_t*)&size, 0, sizeof(size_t));
		if ( 0 != size )
		{
			mm_streambuf_sputn(&this->streambuf,(mm_uint8_t*)val.c_str(), 0, size);
		}
		return *this;
	}

	o_archive& o_archive::operator << (const bool& val)
	{
		mm_streambuf_sputn(&this->streambuf,(mm_uint8_t*)&val, 0, sizeof(bool));
		return *this;
	}
}
