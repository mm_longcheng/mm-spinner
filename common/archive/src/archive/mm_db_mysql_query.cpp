#include "mm_db_mysql_query.h"
#include "core/mm_atoi.h"
//////////////////////////////////////////////////////////////////////////
namespace mm
{
	//////////////////////////////////////////////////////////////////////////
	db_mysql_stream::db_mysql_stream( MYSQL* _context )
		: context(_context)
	{

	}

	db_mysql_stream::~db_mysql_stream()
	{

	}

	std::ostringstream& db_mysql_stream::get_stream_impl()
	{
		return this->stream_impl;
	}

	MYSQL* db_mysql_stream::get_context()
	{
		return this->context;
	}

	const std::string db_mysql_stream::get_string() const
	{
		return this->stream_impl.str();
	}

	//////////////////////////////////////////////////////////////////////////
	MM_EXPORT_DLL db_mysql_stream& operator<<( db_mysql_stream& o, std::string& _value )
	{
		//////////////////////////////////////////////////////////////////////////
		std::string binary;
		//////////////////////////////////////////////////////////////////////////
		std::ostringstream& oss = o.get_stream_impl();
		MYSQL* context = o.get_context();
		//////////////////////////////////////////////////////////////////////////
		size_t value_size = _value.size();
		const char* value_buff = _value.data();
		size_t binary_size = value_size;
		mm_roundup32(binary_size);
		// here we alloc value_size roundup32 * 2 size to cache real escape string.
		binary_size *= MM_REAL_ESCAPE_STRING_FACTOR;
		// resize for contain all binary data.
		binary.resize(binary_size);
		unsigned long binary_real_size = mysql_real_escape_string(context,(char*)binary.data(),value_buff,value_size);
		// resize for real binary size for append.
		binary.resize(binary_real_size);
		//////////////////////////////////////////////////////////////////////////
		oss << binary;
		//////////////////////////////////////////////////////////////////////////
		return o;
	}
	//////////////////////////////////////////////////////////////////////////
	db_mysql_query::db_mysql_query( struct mm_db_mysql* _db_mysql ) 
		: db_mysql(_db_mysql)
	{

	}
	//////////////////////////////////////////////////////////////////////////
	void db_mysql_query::get_field( int idx , mm_ullong_t& _value )
	{
		_value = (mm_ullong_t)( mm_atoi64( db_mysql->row[idx] ) & 0xFFFFFFFFFFFFFFFF );
	}
	void db_mysql_query::get_field( int idx , mm_sllong_t& _value )
	{
		_value = (mm_sllong_t)( mm_atoi64( db_mysql->row[idx] ) & 0xFFFFFFFFFFFFFFFF );
	}

	void db_mysql_query::get_field( int idx , mm_ulong_t& _value )
	{
		_value = (mm_ulong_t)( mm_atoi64( db_mysql->row[idx] ) & 0xFFFFFFFFFFFFFFFF );
	}
	void db_mysql_query::get_field( int idx , mm_slong_t& _value )
	{
		_value = (mm_slong_t)( mm_atoi64( db_mysql->row[idx] ) & 0xFFFFFFFFFFFFFFFF );
	}

	void db_mysql_query::get_field( int idx , mm_uint_t& _value )
	{
		_value = (mm_uint_t)( mm_atoi32( db_mysql->row[idx] ) & 0xFFFFFFFF );
	}
	void db_mysql_query::get_field( int idx , mm_sint_t& _value )
	{
		_value = (mm_sint_t)( mm_atoi32( db_mysql->row[idx] ) & 0xFFFFFFFF );
	}

	void db_mysql_query::get_field( int idx , mm_ushort_t& _value )
	{
		_value = (mm_ushort_t)( mm_atoi32( db_mysql->row[idx] ) & 0xFFFF );
	}
	void db_mysql_query::get_field( int idx , mm_sshort_t& _value )
	{
		_value = (mm_sshort_t)( mm_atoi32( db_mysql->row[idx] ) & 0xFFFF );
	}

	void db_mysql_query::get_field( int idx , mm_uchar_t& _value )
	{
		_value = (mm_uchar_t)( mm_atoi32( db_mysql->row[idx] ) & 0xFF );
	}
	void db_mysql_query::get_field( int idx , mm_schar_t& _value )
	{
		_value = (mm_schar_t)( mm_atoi32( db_mysql->row[idx] ) & 0xFF );
	}

	void db_mysql_query::get_field( int idx , float& _value )
	{
		_value = (float)atof( db_mysql->row[idx] );
	}
	void db_mysql_query::get_field( int idx , double& _value )
	{
		_value = (double)atof( db_mysql->row[idx] );
	}
	void db_mysql_query::get_field( int idx , bool& _value )
	{
		_value = 0 != (mm_atoi32( db_mysql->row[idx] ) & 0xFF );
	}

	void db_mysql_query::get_field( int idx , std::string& _value )
	{
		char* buff = db_mysql->row[idx];
		unsigned long size = db_mysql->field_length[idx];
		_value.resize(size);
		mm_memcpy((void*)_value.data(),buff ? buff : "",size);
	}
}