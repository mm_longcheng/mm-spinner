#ifndef	__mm_archive_h__
#define	__mm_archive_h__

#include "core/mm_core.h"
#include "core/mm_streambuf.h"
#include <vector>
#include <stack>
#include <string>
namespace mm
{
	//
	//	Input stream.
	//
	class MM_EXPORT_DLL i_archive
	{
	public:
		i_archive(struct mm_streambuf& _streambuf);

		template<class complex_type>
		const i_archive& operator >> (complex_type& val)const 
		{
			val.decode(*this);
			return *this;
		}
		//template<class complex_type>
		//IArchive& operator >> (complex_type& val)
		//{
		//	serialization::decode(*this,val);
		//	return *this;
		//}
		//list vector decode will not trigger the value_type's copy construction ,
		//because list vector insert return the value reference.
		template<
			typename value_type,
			typename alloc_type,
			template<class,class> class complex_container >
		const i_archive& operator >> (complex_container<alloc_type,value_type>& val)const 
		{
			typedef complex_container<alloc_type,value_type> complex_container_type;
			typedef typename complex_container_type::iterator iterator;
			size_t size = 0;
			(*this) >> size;
			val.resize(size);
			for (iterator it = val.begin();
				it!=val.end();++it)
			{
				(*this) >> (*it);
			}
			return (*this);
		}
		//pair
		template<
			typename type_1,
			typename type_2>
		const i_archive& operator >> (std::pair<type_1,type_2>& val)const 
		{
			(*this) >> val.first;
			(*this) >> val.second;
			return (*this);
		}
		//map multimap decode will not trigger the mapped_type's copy construction ,
		//because map multimap insert return the value reference.
		template<
			typename key_type,
			typename value_type,
			typename pr_type,
			typename alloc_type,
			template<class,class,class,class> class complex_container >
		const i_archive& operator >> (complex_container<key_type,value_type,pr_type,alloc_type>& val)const 
		{
			typedef complex_container<key_type,value_type,pr_type,alloc_type> complex_container_type;
			typedef typename complex_container_type::mapped_type mapped_type;
			typedef typename complex_container_type::iterator iterator;
			typedef typename std::pair<iterator,bool> result_type;
			size_t size = 0;
			(*this) >> size;
			val.clear();
			while( 0 < size )
			{
				size--;
				key_type k;
				(*this) >> k;
				result_type _result = val.insert(value_type(k,mapped_type()));
				(*this) >> (_result.first->second);
			}
			return (*this);
		}
		//set multiset decode will trigger the key_type copy construction ,
		//because set multiset insert return the value reference but not assignable.
		//Can't modify a set's members in place.
		//http://stackoverflow.com/questions/16299125/return-iterator-from-stdsetinsert-is-const
		template<
			typename key_type,
			typename pr_type,
			typename alloc_type,
			template<class,class,class> class complex_container >
		const i_archive& operator >> (complex_container<key_type,pr_type,alloc_type>& val)const 
		{
			size_t size = 0;
			(*this) >> size;
			val.clear();
			while( 0 < size )
			{
				size--;
				key_type v;
				(*this) >> v;
				val.insert(v);
			}
			return (*this);
		}
		//stack decode will trigger a copy construction ,
		//because stack push not return the value reference, and only only value.
		template<
			typename value_type,
			typename container>
		const i_archive& operator >> (std::stack<value_type,container>& val)const 
		{
			size_t size = 0;
			(*this) >> size;
			while ( ! val.empty() )
			{
				val.pop();
			}
			while( 0 < size )
			{
				size--;
				value_type v;
				(*this) >> v;
				val.push(v);
			}
			return (*this);
		}

		const i_archive& operator >> (mm_schar_t& val)const ;
		const i_archive& operator >> (mm_uchar_t& val)const ;
		const i_archive& operator >> (mm_sshort_t& val)const ;
		const i_archive& operator >> (mm_ushort_t& val)const ;
		const i_archive& operator >> (mm_sint_t& val)const ;
		const i_archive& operator >> (mm_uint_t& val)const ;
		const i_archive& operator >> (mm_slong_t& val)const ;
		const i_archive& operator >> (mm_ulong_t& val)const ;
		const i_archive& operator >> (mm_sllong_t& val)const ;
		const i_archive& operator >> (mm_ullong_t& val)const ;
		const i_archive& operator >> (float& val)const ;
		const i_archive& operator >> (double& val)const ;
		const i_archive& operator >> (bool& val)const ;
		const i_archive& operator >> (std::string& val)const ;

		size_t size() const
		{
			return mm_streambuf_size(&this->streambuf);
		}

		const struct mm_streambuf& get_streambuf()const
		{
			return this->streambuf;
		}

		struct mm_streambuf& get_streambuf_ref()
		{
			return this->streambuf;
		}
	private:
		struct mm_streambuf& streambuf;
	};

	//
	//	Output stream.
	//
	class MM_EXPORT_DLL o_archive
	{
	public:
		o_archive(struct mm_streambuf& _streambuf);

		template<typename complex_type>
		o_archive& operator << (const complex_type& val)
		{
			val.encode(*this);
			return *this;
		}
		//template<typename complex_type>
		//OArchive& operator << (const complex_type& val)
		//{
		//	serialization::encode(*this,val);
		//	return *this;
		//}
		//list vector
		template<
			typename value_type,
			typename alloc_type,
			template<class,class> class complex_container >
		o_archive& operator << (const complex_container<alloc_type,value_type>& val)
		{
			typedef complex_container<alloc_type,value_type> complex_container_type;
			typedef typename complex_container_type::const_iterator const_iterator;
			size_t size = val.size();
			(*this) << size;
			if ( 0 < size )
			{
				for (const_iterator it = val.begin(); 
					it!=val.end(); it++)
				{
					(*this) << (*it);
				}
			}
			return (*this);
		}
		//pair
		template<
			typename type_1,
			typename type_2>
		o_archive& operator << (const std::pair<type_1,type_2>& val)
		{
			(*this) << val.first;
			(*this) << val.second;
			return (*this);
		}
		//map
		template<
			typename key_type,
			typename value_type,
			typename pr_type,
			typename alloc_type,
			template<class,class,class,class> class complex_container >
		o_archive& operator << (const complex_container<key_type,value_type,pr_type,alloc_type>& val)
		{
			typedef complex_container<key_type,value_type,pr_type,alloc_type> complex_container_type;
			typedef typename complex_container_type::const_iterator const_iterator;
			size_t size = val.size();
			(*this) << size;
			if ( 0 < size )
			{
				for (const_iterator it = val.begin(); 
					it!=val.end(); it++)
				{
					(*this) << it->first;
					(*this) << it->second;
				}
			}
			return (*this);
		}
		//set
		template<
			typename key_type,
			typename pr_type,
			typename alloc_type,
			template<class,class,class> class complex_container >
		o_archive& operator << (const complex_container<key_type,pr_type,alloc_type>& val)
		{
			typedef complex_container<key_type,pr_type,alloc_type> complex_container_type;
			typedef typename complex_container_type::const_iterator const_iterator;
			size_t size = val.size();
			(*this) << size;
			if ( 0 < size )
			{
				for (const_iterator it = val.begin(); 
					it!=val.end(); it++)
				{
					(*this) << (*it);
				}
			}
			return (*this);
		}
		//stack
		template<
			typename value_type,
			typename container>
		o_archive& operator << (const std::stack<value_type,container>& val)
		{
			typedef typename std::stack<value_type,container> complex_container_type;
			typedef typename complex_container_type::container_type container_type;
			typedef typename complex_container_type::const_iterator const_iterator;
			size_t size = val.size();
			(*this) << size;
			if ( 0 < size )
			{
				const container_type& cval = val._Get_container();
				for (const_iterator it = cval.begin(); 
					it!=cval.end(); it++)
				{
					(*this) << (*it);
				}
			}
			return (*this);
		}
		o_archive& operator << (const mm_schar_t& val);
		o_archive& operator << (const mm_uchar_t& val);
		o_archive& operator << (const mm_sshort_t& val);
		o_archive& operator << (const mm_ushort_t& val);
		o_archive& operator << (const mm_sint_t& val);
		o_archive& operator << (const mm_uint_t& val);
		o_archive& operator << (const mm_slong_t& val);
		o_archive& operator << (const mm_ulong_t& val);
		o_archive& operator << (const mm_sllong_t& val);
		o_archive& operator << (const mm_ullong_t& val);
		o_archive& operator << (const float& val);
		o_archive& operator << (const double& val);
		o_archive& operator << (const bool& val);
		o_archive& operator << (const std::string& val);

		size_t size() const
		{
			return mm_streambuf_size(&this->streambuf);
		}

		const struct mm_streambuf& get_streambuf()const
		{
			return this->streambuf;
		}

		struct mm_streambuf& get_streambuf_ref()
		{
			return this->streambuf;
		}
	private:
		struct mm_streambuf& streambuf;
	};
}
#endif//__mm_archive_h__
