##code server and proto lib.
###script
1.script is build and launch script.
###design
1.design for doc.
###lib
1.query is script generate mysql query code.
2.proto is script generate protobuf code.
###app
1.sync
	a.client_ctrl tool for server state ctrl.
	b.client_entry too for server_entry test.
	c.module_proxy_loavg is proxy server for msg use loavg.
	d.module_proxy_mixed is proxy server for msg use loavg and user_id mixed.
	e.module_proxy_modid is proxy server for msg use user_id.
	f.server_entry is client first entry point use udp.
	g.server_lobby is conntion keeper for client use tcp.
	h.sync is sync dir all project common lib.
	i.sync_factor is tool for sync_server for strategy update.
	j.sync_server hold the msg dispatch strategy data.
##compile 
Environment Variables
DISH_HOME    d:\hg\dish
COLD_HOME    d:\hg\cold
MYSQL_HOME   d:\tools\MySQL\MySQL Server 5.7
LAKE_HOME    d:\hg\lake