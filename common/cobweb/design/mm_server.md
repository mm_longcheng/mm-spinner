﻿启动参数使用服务类型和服务编号从数据库获取.

静态:
{
	uint32 unique_id;// uid for server.
	string node;// default is invalid "0.0.0.0"
	uint32 port;// address port.
	uint32 worker_number;// logic thread count.
	string desc // description.
}

动态
{
	统计数据
	{
		uint64 pid;
		uint64 vm_rss;
		float32 cpu_sys;
		float32 cpu_proc;
		float32 mem_proc;
		uint64 rchar;
		uint64 wchar;
		uint32 sampling;
		uint32 cores;
		float32 lavg_01;
		float32 lavg_05;
		float32 lavg_15;
		uint32 nr_running;
		uint32 nr_threads;
		uint64 last_pid;
	}

	计算数据
	{
		float32 weight;// 权重
	}
}

组合:[unique_id]node-port(worker_number)
例子:[65535]192.168.111.123-65535(2)

zookeeper redis mysql worker_number无效为1

内部服务(100)
{
	entry     = 1,
	lobby     = 2,
	proxy     = 3,
	cback     = 4,

	mysql     = 70,
	redis     = 71,
	zookeeper = 72,
}

集群种类				
zookeeper_cluster
{
	entry = 1,// lobby 的写 entry 的读
	lobby = 2,// proxy 的写 lobby 的读
	proxy = 3,// worki 的写 proxy 的读
}
cluster_id = 集群种类 * 10000 + 实例编号(work编号或者0)
zk_cluster_entry = 1 * 10000 + 0 = 10000
zk_cluster_lobby = 2 * 10000 + 0 = 20000
zk_cluster_proxy = 3 * 10000 + i = 30000

运行时:
内存占比
运算占比
负载情况

mm_module_account_000:* mm_module_account_100:* mm_module_basis_000:* mm_module_basis_100:* mm_server_cback_000:* mm_server_entry_000:* mm_server_lobby_000:* mm_server_proxy_001:* mm_server_proxy_100:* mm_server_proxy_150:* mm_server_proxy_300:*