﻿lobby_launch
{
	uint32 unique_id      服务编号         10001
	string name           程序名           ./application
	string log            日志文件夹路径   ./log
	string addr_dbconfig  地址簿数据库地址 192.168.111.123-65535
	uint32 addr_zkimport  读zookeeper地址  65535
	uint32 addr_zkexport  写zookeeper地址  65535
	uint32 addr_external  外部业务地址     65535
	uint32 addr_internal  内部控制地址     65535
	uint32 addr_dbcredis  写缓存地址       65535
}
lobby_launch_print
{
	uint32 unique_id      服务编号         10001
	string name           程序名           ./application
	string log            日志文件夹路径   ./log/10001
	string addr_dbconfig  地址簿数据库地址 192.168.111.123-65535
	string addr_zkimport  读zookeeper地址  [65535]192.168.111.123-65535(2)
	string addr_zkexport  写zookeeper地址  [65535]192.168.111.123-65535(2)
	string addr_external  外部业务地址     [65535]192.168.111.123-65535(2)
	string addr_internal  内部控制地址     [65535]192.168.111.123-65535(2)
	string addr_dbcredis  写缓存地址       [65535]192.168.111.123-65535(2)
}

启动参数
	mm_printf("%s\n", "程序名            log    地址簿数据库地址 服务编号");
	mm_printf("%s\n", "./mm_server_lobby ../log 127.0.0.1-3306   2002000");

zk数据结构	
lobby(写)
	2002000 json_encode(MRuntimeState)
	
proxy(读)
	2002000 json_encode(MRuntimeState)
	