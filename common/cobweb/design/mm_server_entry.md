﻿launch_entry
{
	uint32 unique_id      服务编号         10001
	string name           程序名           ./mm_server_proxy
	string log            日志文件夹路径   ../log
	string addr_dbconfig  地址簿数据库地址 192.168.111.123-65535
	uint32 addr_zkimport  读zookeeper地址  65535
	uint32 addr_zkexport  读zookeeper地址  65535
	uint32 addr_external  外部业务地址     65535
	uint32 addr_internal  内部控制地址     65535
}
launch_entry_print
{
	uint32 unique_id      服务编号         10001
	string name           程序名           ./mm_server_proxy
	string log            日志文件夹路径   ../log
	string addr_dbconfig  地址簿数据库地址 192.168.111.123-65535
	string addr_zkimport  读zookeeper地址  [65535]192.168.111.123-65535(2)
	string addr_zkexport  读zookeeper地址  [65535]192.168.111.123-65535(2)
	string addr_external  外部业务地址     [65535]192.168.111.123-65535(2)
	string addr_internal  内部控制地址     [65535]192.168.111.123-65535(2)
}

启动参数
	mm_printf("%s\n", "程序名            log    地址簿数据库地址 服务编号");
	mm_printf("%s\n", "./mm_server_proxy ../log 127.0.0.1-3306   2001000");

zk数据结构	
/lobby(读)
	2002000 json_encode(MRuntimeState)

zoo_awget
	监听/lobby
zoo_awget_children	
	监听/lobby子节点
	
	对所有子节点设置zoo_awget
	获取子节点数据
	
	ZOO_CREATED_EVENT 创建
	ZOO_DELETED_EVENT 移除
	ZOO_CHANGED_EVENT 改变
