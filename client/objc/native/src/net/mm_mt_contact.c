#include "net/mm_mt_contact.h"
#include "core/mm_alloc.h"
//////////////////////////////////////////////////////////////////////////
static void __static_mt_contact_event_mt_tcp_alloc( void* p, struct mm_mt_tcp* mt_tcp )
{

}
static void __static_mt_contact_event_mt_tcp_relax( void* p, struct mm_mt_tcp* mt_tcp )
{

}
MM_EXPORT_DLL void mm_mt_contact_event_mt_tcp_alloc_init(struct mm_mt_contact_event_mt_tcp_alloc* p)
{
	p->event_mt_tcp_alloc = &__static_mt_contact_event_mt_tcp_alloc;
	p->event_mt_tcp_relax = &__static_mt_contact_event_mt_tcp_relax;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_mt_contact_event_mt_tcp_alloc_destroy(struct mm_mt_contact_event_mt_tcp_alloc* p)
{
	p->event_mt_tcp_alloc = &__static_mt_contact_event_mt_tcp_alloc;
	p->event_mt_tcp_relax = &__static_mt_contact_event_mt_tcp_relax;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mt_contact_init(struct mm_mt_contact* p)
{
	struct mm_holder_u32_vpt_alloc holder32_alloc;

	mm_mt_contact_event_mt_tcp_alloc_init(&p->event_mt_tcp_alloc);
	mm_crypto_callback_init(&p->crypto_callback);
	mm_holder_u32_vpt_init(&p->holder);
	pthread_mutex_init(&p->signal_mutex, NULL);  
	pthread_cond_init(&p->signal_cond, NULL);
	mm_spin_init(&p->holder_locker,NULL);
	mm_spin_init(&p->instance_locker,NULL);
	mm_spin_init(&p->locker,NULL);
	p->state = ts_closed;// mm_thread_state_t,default is ts_closed(0)
	p->u = NULL;

	holder32_alloc.alloc = &mm_holder_u32_vpt_weak_alloc;
	holder32_alloc.relax = &mm_holder_u32_vpt_weak_relax;
	holder32_alloc.obj = p;
	mm_holder_u32_vpt_assign_alloc(&p->holder,&holder32_alloc);
}
MM_EXPORT_DLL void mm_mt_contact_destroy(struct mm_mt_contact* p)
{
	mm_mt_contact_clear(p);
	//
	mm_mt_contact_event_mt_tcp_alloc_destroy(&p->event_mt_tcp_alloc);
	mm_crypto_callback_destroy(&p->crypto_callback);
	mm_holder_u32_vpt_destroy(&p->holder);
	pthread_mutex_destroy(&p->signal_mutex);  
	pthread_cond_destroy(&p->signal_cond);
	mm_spin_destroy(&p->holder_locker);
	mm_spin_destroy(&p->instance_locker);
	mm_spin_destroy(&p->locker);
	p->state = 0;
	p->u = NULL;
}
//////////////////////////////////////////////////////////////////////////
// lock to make sure the mt_contact is thread safe.
MM_EXPORT_DLL void mm_mt_contact_lock(struct mm_mt_contact* p)
{
	mm_spin_lock(&p->locker);
}
// unlock mt_contact.
MM_EXPORT_DLL void mm_mt_contact_unlock(struct mm_mt_contact* p)
{
	mm_spin_unlock(&p->locker);
}
//////////////////////////////////////////////////////////////////////////
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_mt_contact_assign_event_mt_tcp_alloc(struct mm_mt_contact* p,struct mm_mt_contact_event_mt_tcp_alloc* event_mt_tcp_alloc)
{
	assert(NULL != event_mt_tcp_alloc && "you can not assign null event_mt_tcp_alloc.");
	p->event_mt_tcp_alloc = *event_mt_tcp_alloc;
}
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_mt_contact_assign_crypto_callback(struct mm_mt_contact* p,struct mm_crypto_callback* crypto_callback)
{
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	struct mm_mt_tcp* mt_tcp = NULL;
	//
	mm_spin_lock(&p->holder_locker);
	n = mm_rb_first(&p->holder.rbt);
	assert(NULL != crypto_callback && "crypto_callback is a null.");
	p->crypto_callback = *crypto_callback;
	while(NULL != n)
	{
		it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n, struct mm_holder_u32_vpt_iterator, n);
		mt_tcp = (struct mm_mt_tcp*)(it->v);
		n = mm_rb_next(n);
		mm_mt_tcp_lock(mt_tcp);
		mm_mt_tcp_assign_crypto_callback(mt_tcp,&p->crypto_callback);
		mm_mt_tcp_unlock(mt_tcp);
	}
	mm_spin_unlock(&p->holder_locker);
}
// assign context handle.
MM_EXPORT_DLL void mm_mt_contact_assign_context(struct mm_mt_contact* p,void* u)
{
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	struct mm_mt_tcp* mt_tcp = NULL;
	//
	mm_spin_lock(&p->holder_locker);
	n = mm_rb_first(&p->holder.rbt);
	p->u = u;
	while(NULL != n)
	{
		it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n, struct mm_holder_u32_vpt_iterator, n);
		mt_tcp = (struct mm_mt_tcp*)(it->v);
		n = mm_rb_next(n);
		mm_mt_tcp_lock(mt_tcp);
		mm_mt_tcp_assign_context(mt_tcp,p->u);
		mm_mt_tcp_unlock(mt_tcp);
	}
	mm_spin_unlock(&p->holder_locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mt_contact_check(struct mm_mt_contact* p)
{
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	struct mm_mt_tcp* mt_tcp = NULL;
	//
	mm_spin_lock(&p->holder_locker);
	n = mm_rb_first(&p->holder.rbt);
	while(NULL != n)
	{
		it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n, struct mm_holder_u32_vpt_iterator, n);
		mt_tcp = (struct mm_mt_tcp*)(it->v);
		n = mm_rb_next(n);
		mm_mt_tcp_lock(mt_tcp);
		mm_mt_tcp_check(mt_tcp);
		mm_mt_tcp_unlock(mt_tcp);
	}
	mm_spin_unlock(&p->holder_locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mt_contact_start(struct mm_mt_contact* p)
{
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	struct mm_mt_tcp* mt_tcp = NULL;
	//
	mm_spin_lock(&p->holder_locker);
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	n = mm_rb_first(&p->holder.rbt);
	while(NULL != n)
	{
		it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n, struct mm_holder_u32_vpt_iterator, n);
		mt_tcp = (struct mm_mt_tcp*)(it->v);
		n = mm_rb_next(n);
		mm_mt_tcp_lock(mt_tcp);
		mm_mt_tcp_start(mt_tcp);
		mm_mt_tcp_unlock(mt_tcp);
	}
	mm_spin_unlock(&p->holder_locker);
}
MM_EXPORT_DLL void mm_mt_contact_interrupt(struct mm_mt_contact* p)
{
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	struct mm_mt_tcp* mt_tcp = NULL;
	//
	mm_spin_lock(&p->holder_locker);
	p->state = ts_closed;
	n = mm_rb_first(&p->holder.rbt);
	while(NULL != n)
	{
		it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n, struct mm_holder_u32_vpt_iterator, n);
		mt_tcp = (struct mm_mt_tcp*)(it->v);
		n = mm_rb_next(n);
		mm_mt_tcp_lock(mt_tcp);
		mm_mt_tcp_interrupt(mt_tcp);
		mm_mt_tcp_unlock(mt_tcp);
	}
	mm_spin_unlock(&p->holder_locker);
	//
	pthread_mutex_lock(&p->signal_mutex);
	pthread_cond_signal(&p->signal_cond);
	pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mm_mt_contact_shutdown(struct mm_mt_contact* p)
{
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	struct mm_mt_tcp* mt_tcp = NULL;
	//
	mm_spin_lock(&p->holder_locker);
	p->state = ts_finish;
	n = mm_rb_first(&p->holder.rbt);
	while(NULL != n)
	{
		it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n, struct mm_holder_u32_vpt_iterator, n);
		mt_tcp = (struct mm_mt_tcp*)(it->v);
		n = mm_rb_next(n);
		mm_mt_tcp_lock(mt_tcp);
		mm_mt_tcp_shutdown(mt_tcp);
		mm_mt_tcp_unlock(mt_tcp);
	}
	mm_spin_unlock(&p->holder_locker);
	//
	pthread_mutex_lock(&p->signal_mutex);
	pthread_cond_signal(&p->signal_cond);
	pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mm_mt_contact_join(struct mm_mt_contact* p)
{
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	struct mm_mt_tcp* mt_tcp = NULL;
	if (ts_motion == p->state)
	{
		// we can not lock or join until cond wait all thread is shutdown.
		pthread_mutex_lock(&p->signal_mutex);
		pthread_cond_wait(&p->signal_cond,&p->signal_mutex);
		pthread_mutex_unlock(&p->signal_mutex);
	}
	//
	mm_spin_lock(&p->holder_locker);
	n = mm_rb_first(&p->holder.rbt);
	while(NULL != n)
	{
		it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n, struct mm_holder_u32_vpt_iterator, n);
		mt_tcp = (struct mm_mt_tcp*)(it->v);
		n = mm_rb_next(n);
		mm_mt_tcp_lock(mt_tcp);
		mm_mt_tcp_join(mt_tcp);
		mm_mt_tcp_unlock(mt_tcp);
	}
	mm_spin_unlock(&p->holder_locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL struct mm_mt_tcp* mm_mt_contact_add(struct mm_mt_contact* p,int unique_id,const char* node_n,const char* node_r,mm_ushort_t port)
{
	struct mm_mt_tcp* mt_tcp = NULL;
	// only lock for instance create process.
	mm_spin_lock(&p->instance_locker);
	mt_tcp = mm_mt_contact_get(p,unique_id);
	if (NULL == mt_tcp)
	{
		mt_tcp = (struct mm_mt_tcp*)mm_malloc(sizeof(struct mm_mt_tcp));
		mm_mt_tcp_init(mt_tcp);

		mm_mt_tcp_assign_native(mt_tcp, node_n, port);
		mm_mt_tcp_assign_remote(mt_tcp, node_r, port);
		// assign sid index.
		mt_tcp->unique_id = unique_id;
		mm_mt_tcp_assign_context(mt_tcp,p->u);
		//
		mm_mt_tcp_assign_crypto_callback(mt_tcp,&p->crypto_callback);
		//
		(*(p->event_mt_tcp_alloc.event_mt_tcp_alloc))(p,mt_tcp);
		//
		if ( ts_motion == p->state )
		{
			// if current mt_contact is in motion we start the new route .
			mm_mt_tcp_start(mt_tcp);
		}
		else
		{
			// if current mt_contact is not motion,we assign the same state.
			mt_tcp->state = p->state;
		}
		//
		mm_spin_lock(&p->holder_locker);
		mm_holder_u32_vpt_set(&p->holder,unique_id,mt_tcp);
		mm_spin_unlock(&p->holder_locker);
	}
	mm_spin_unlock(&p->instance_locker);
	return mt_tcp;
}
MM_EXPORT_DLL void mm_mt_contact_rmv(struct mm_mt_contact* p,int unique_id)
{
	struct mm_holder_u32_vpt_iterator* it = NULL;
	//
	mm_spin_lock(&p->holder_locker);
	it = mm_holder_u32_vpt_get_iterator(&p->holder,unique_id);
	if (NULL != it)
	{
		struct mm_mt_tcp* mt_tcp = (struct mm_mt_tcp*)(it->v);
		mm_mt_tcp_lock(mt_tcp);
		mm_holder_u32_vpt_erase(&p->holder,it);
		mm_mt_tcp_unlock(mt_tcp);
		(*(p->event_mt_tcp_alloc.event_mt_tcp_relax))(p,mt_tcp);
		mm_mt_tcp_destroy(mt_tcp);
		mm_free(mt_tcp);
	}
	mm_spin_unlock(&p->holder_locker);
}
MM_EXPORT_DLL struct mm_mt_tcp* mm_mt_contact_get(struct mm_mt_contact* p,int unique_id)
{
	struct mm_mt_tcp* e = NULL;
	//
	mm_spin_lock(&p->holder_locker);
	e = (struct mm_mt_tcp*)mm_holder_u32_vpt_get(&p->holder,unique_id);
	mm_spin_unlock(&p->holder_locker);
	return e;
}
MM_EXPORT_DLL struct mm_mt_tcp* mm_mt_contact_get_instance(struct mm_mt_contact* p,int unique_id,const char* node_n,const char* node_r,mm_ushort_t port)
{
	struct mm_mt_tcp* mt_tcp = mm_mt_contact_get(p,unique_id);
	if (NULL == mt_tcp)
	{
		mt_tcp = mm_mt_contact_add(p,unique_id,node_n,node_r,port);
		//cache.
		mm_string_assigns(&mt_tcp->cache_native_node,node_n);
		mm_string_assigns(&mt_tcp->cache_remote_node,node_r);
		mt_tcp->cache_remote_port = port;
	}
	else
	{
		mm_mt_tcp_lock(mt_tcp);
		if ( 
			0 != mm_string_compare_c_str(&mt_tcp->cache_native_node,node_n) || 
			0 != mm_string_compare_c_str(&mt_tcp->cache_remote_node,node_r) ||
			mt_tcp->cache_remote_port != port)
		{
			// if ip and port not equal , we reset it.
			// reset mt_tcp address.
			mm_mt_tcp_assign_native(mt_tcp, node_n, port);
			mm_mt_tcp_assign_remote(mt_tcp, node_r, port);
			//cache.
			mm_string_assigns(&mt_tcp->cache_native_node,node_n);
			mm_string_assigns(&mt_tcp->cache_remote_node,node_r);
			mt_tcp->cache_remote_port = port;
			// the mt_tcp is invalid.
			mm_mt_tcp_detach_socket(mt_tcp);
		}
		mm_mt_tcp_unlock(mt_tcp);
	}
	return mt_tcp;
}
MM_EXPORT_DLL void mm_mt_contact_clear(struct mm_mt_contact* p)
{
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	struct mm_mt_tcp* mt_tcp = NULL;
	//
	mm_spin_lock(&p->instance_locker);
	mm_spin_lock(&p->holder_locker);
	n = mm_rb_first(&p->holder.rbt);
	while(NULL != n)
	{
		it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n, struct mm_holder_u32_vpt_iterator, n);
		mt_tcp = (struct mm_mt_tcp*)(it->v);
		n = mm_rb_next(n);
		mm_mt_tcp_lock(mt_tcp);
		mm_holder_u32_vpt_erase(&p->holder,it);
		mm_mt_tcp_unlock(mt_tcp);
		(*(p->event_mt_tcp_alloc.event_mt_tcp_relax))(p,mt_tcp);
		mm_mt_tcp_destroy(mt_tcp);
		mm_free(mt_tcp);
	}
	mm_spin_unlock(&p->holder_locker);
	mm_spin_unlock(&p->instance_locker);
}