#include "mm_buffer_packet.h"
#include "core/mm_logger.h"
//////////////////////////////////////////////////////////////////////////
// packet handle tcp buffer and length for string handle only one packet.
MM_EXPORT_DLL void mm_buffer_packet_handle_tcp( mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length,packet_handle_tcp handle, void* obj)
{
	struct mm_logger* g_logger = mm_logger_instance();
	//
	struct mm_packet pack;
	mm_uint32_t head_data = 0;
	mm_uint16_t msg_size = 0;
	size_t buff_size = 0;

	mm_uint8_t* buff = (mm_uint8_t*)(buffer + offset);

	buff_size = length;
	mm_memcpy(&head_data,buff,sizeof(mm_uint32_t));
	msg_size = (mm_uint16_t)(head_data & MM_MSG_HEAD_L_MASK);
	if (buff_size == msg_size)
	{
		// here we just use the weak ref from streambuf.for data recv callback.
		pack.hbuff.length = head_data >> MM_MSG_SIZE_BIT_NUM;
		pack.hbuff.buffer = buff;
		pack.hbuff.offset = (mm_uint32_t)(sizeof(mm_uint32_t));
		//
		pack.bbuff.length = msg_size - sizeof(mm_uint32_t) - pack.hbuff.length;
		pack.bbuff.buffer = buff;
		pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
		//
		mm_packet_head_base_decode(&pack);
		//
		// fire the field buffer recv event.
		(*(handle))(obj,&pack);
	}
	else
	{
		mm_logger_log_W(g_logger,"%s %d data size is invalid.buff_size:%" PRIuPTR " msg_size:%u",__FUNCTION__,__LINE__,buff_size,msg_size);
	}
}
MM_EXPORT_DLL void mm_buffer_packet_handle_udp(mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length,packet_handle_udp handle, void* obj, struct sockaddr_storage* remote)
{
	struct mm_logger* g_logger = mm_logger_instance();
	//
	struct mm_packet pack;
	mm_uint32_t head_data = 0;
	mm_uint16_t msg_size = 0;
	size_t buff_size = 0;

	mm_uint8_t* buff = (mm_uint8_t*)(buffer + offset);

	buff_size = length;
	mm_memcpy(&head_data,buff,sizeof(mm_uint32_t));
	msg_size = (mm_uint16_t)(head_data & MM_MSG_HEAD_L_MASK);
	if (buff_size == msg_size)
	{
		// here we just use the weak ref from streambuf.for data recv callback.
		pack.hbuff.length = head_data >> MM_MSG_SIZE_BIT_NUM;
		pack.hbuff.buffer = buff;
		pack.hbuff.offset = (mm_uint32_t)(sizeof(mm_uint32_t));
		//
		pack.bbuff.length = msg_size - sizeof(mm_uint32_t) - pack.hbuff.length;
		pack.bbuff.buffer = buff;
		pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
		//
		mm_packet_head_base_decode(&pack);
		//
		// fire the field buffer recv event.
		(*(handle))(obj,&pack,remote);
	}
	else
	{
		mm_logger_log_W(g_logger,"%s %d data size is invalid.buff_size:%" PRIuPTR " msg_size:%u",__FUNCTION__,__LINE__,buff_size,msg_size);
	}
}