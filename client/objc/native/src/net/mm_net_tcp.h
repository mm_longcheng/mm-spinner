#ifndef __mm_net_tcp_h__
#define __mm_net_tcp_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_spinlock.h"
#include "core/mm_timer_task.h"

#include "net/mm_tcp.h"
#include "net/mm_nuclear.h"
#include "net/mm_crypto.h"
#include "net/mm_packet.h"

#include "container/mm_holder32.h"

// if bind listen failure will msleep this time.
#define MM_NET_TCP_TRY_MSLEEP_TIME 20
// net tcp nonblock timeout..
#define MM_NET_TCP_NONBLOCK_TIMEOUT 1000
// net tcp one time try time.
#define MM_NET_TCP_TRYTIME 3

typedef void (*net_tcp_handle)(void* obj, void* u, struct mm_packet* pack);
typedef void (*net_tcp_broken)(void* obj);
typedef void (*net_tcp_nready)(void* obj);
typedef void (*net_tcp_finish)(void* obj);
struct mm_net_tcp_callback
{
	net_tcp_handle handle;
	net_tcp_broken broken;
	net_tcp_nready nready;
	net_tcp_finish finish;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_net_tcp_callback_init(struct mm_net_tcp_callback* p);
MM_EXPORT_DLL void mm_net_tcp_callback_destroy(struct mm_net_tcp_callback* p);

typedef void (*net_tcp_event_tcp_alloc)( void* p, struct mm_tcp* tcp );
typedef void (*net_tcp_event_tcp_relax)( void* p, struct mm_tcp* tcp );
struct mm_net_tcp_event_tcp_alloc
{
	net_tcp_event_tcp_alloc event_tcp_alloc;// event tcp alloc and add holder.
	net_tcp_event_tcp_relax event_tcp_relax;// event tcp relax and rmv holder.
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_net_tcp_event_tcp_alloc_init(struct mm_net_tcp_event_tcp_alloc* p);
MM_EXPORT_DLL void mm_net_tcp_event_tcp_alloc_destroy(struct mm_net_tcp_event_tcp_alloc* p);

enum mm_net_tcp_state_t
{
	net_tcp_state_closed = 0,// fd is closed,connect closed.invalid at all.
	net_tcp_state_motion = 1,// fd is opened,connect closed.at connecting.
	net_tcp_state_finish = 2,// fd is opened,connect opened.at connected.
	net_tcp_state_broken = 3,// fd is opened,connect broken.connect is broken fd not closed.
};

struct mm_net_tcp
{
	struct mm_tcp tcp;// strong ref.
	struct mm_nuclear nuclear;// strong ref.
	struct mm_holder_u32_vpt holder;// rb tree for msg_id <--> callback.
	struct mm_net_tcp_callback callback;// value ref. tcp callback.
	struct mm_net_tcp_event_tcp_alloc event_tcp_alloc;
	struct mm_crypto_callback crypto_callback;// crypto callback.
	mm_atomic_t holder_locker;
	mm_atomic_t locker;

	mm_msec_t nonblock_timeout;// tcp nonblock timeout.default is MM_NET_TCP_NONBLOCK_TIMEOUT milliseconds.

	int tcp_state;// mm_net_tcp_state_t,default is tcp_state_closed(0)

	mm_uint32_t trytimes;// try bind and listen times.default is MM_NET_TCP_TRYTIME.
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
	void* u;// user data.
};
/////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_tcp_init(struct mm_net_tcp* p);
MM_EXPORT_DLL void mm_net_tcp_destroy(struct mm_net_tcp* p);
//////////////////////////////////////////////////////////////////////////
// assign native address but not connect.
MM_EXPORT_DLL void mm_net_tcp_assign_native(struct mm_net_tcp* p,const char* node,mm_ushort_t port);
// assign remote address but not connect.
MM_EXPORT_DLL void mm_net_tcp_assign_remote(struct mm_net_tcp* p,const char* node,mm_ushort_t port);
// assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
MM_EXPORT_DLL void mm_net_tcp_assign_native_storage(struct mm_net_tcp* p, struct sockaddr_storage* ss_native);
// assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
MM_EXPORT_DLL void mm_net_tcp_assign_remote_storage(struct mm_net_tcp* p, struct sockaddr_storage* ss_remote);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_tcp_assign_callback(struct mm_net_tcp* p,mm_uint32_t id,net_tcp_handle callback);
MM_EXPORT_DLL void mm_net_tcp_assign_net_tcp_callback(struct mm_net_tcp* p,struct mm_net_tcp_callback* net_tcp_callback);
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_net_tcp_assign_event_tcp_alloc(struct mm_net_tcp* p,struct mm_net_tcp_event_tcp_alloc* event_tcp_alloc);
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_net_tcp_assign_crypto_callback(struct mm_net_tcp* p,struct mm_crypto_callback* crypto_callback);
// assign context handle.
MM_EXPORT_DLL void mm_net_tcp_assign_context(struct mm_net_tcp* p,void* u);
// assign nonblock_timeout.
MM_EXPORT_DLL void mm_net_tcp_assign_nonblock_timeout(struct mm_net_tcp* p,mm_msec_t nonblock_timeout);
// assign trytimes.
MM_EXPORT_DLL void mm_net_tcp_assign_trytimes(struct mm_net_tcp* p,mm_uint32_t trytimes);
//////////////////////////////////////////////////////////////////////////
// context for tcp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_DLL void mm_net_tcp_set_addr_context(struct mm_net_tcp* p, void* u);
MM_EXPORT_DLL void* mm_net_tcp_get_addr_context(struct mm_net_tcp* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_tcp_clear_callback_holder(struct mm_net_tcp* p);
//////////////////////////////////////////////////////////////////////////
// net_tcp crypto encrypt buffer by tcp.
MM_EXPORT_DLL void mm_net_tcp_crypto_encrypt(struct mm_net_tcp* p,struct mm_tcp* tcp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length);
// net_tcp crypto decrypt buffer by tcp.
MM_EXPORT_DLL void mm_net_tcp_crypto_decrypt(struct mm_net_tcp* p,struct mm_tcp* tcp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length);
//////////////////////////////////////////////////////////////////////////
// lock to make sure the net_tcp is thread safe.
MM_EXPORT_DLL void mm_net_tcp_lock(struct mm_net_tcp* p);
// unlock net_tcp.
MM_EXPORT_DLL void mm_net_tcp_unlock(struct mm_net_tcp* p);
//////////////////////////////////////////////////////////////////////////
// fopen socket.
MM_EXPORT_DLL void mm_net_tcp_fopen_socket(struct mm_net_tcp* p);
// synchronize connect.can call this as we already open connect check thread.
MM_EXPORT_DLL void mm_net_tcp_connect(struct mm_net_tcp* p);
// close socket.
MM_EXPORT_DLL void mm_net_tcp_close_socket(struct mm_net_tcp* p);
// shutdown socket.
MM_EXPORT_DLL void mm_net_tcp_shutdown_socket(struct mm_net_tcp* p, int opcode);
// set socket block.
MM_EXPORT_DLL void mm_net_tcp_set_block(struct mm_net_tcp* p, int block);
// streambuf reset.
MM_EXPORT_DLL void mm_net_tcp_streambuf_reset(struct mm_net_tcp* p);
//////////////////////////////////////////////////////////////////////////
// check connect and reconnect if disconnect.not thread safe.
MM_EXPORT_DLL void mm_net_tcp_check(struct mm_net_tcp* p);
// get finally.return 0 for all regular.not thread safe.
MM_EXPORT_DLL int mm_net_tcp_finally_state(struct mm_net_tcp* p);
//////////////////////////////////////////////////////////////////////////
// reset streambuf and fire event broken.
MM_EXPORT_DLL void mm_net_tcp_apply_broken(struct mm_net_tcp* p);
// reset streambuf and fire event finish.
MM_EXPORT_DLL void mm_net_tcp_apply_finish(struct mm_net_tcp* p);
// synchronize attach to socket.not thread safe.
MM_EXPORT_DLL void mm_net_tcp_attach_socket(struct mm_net_tcp* p);
// synchronize detach to socket.not thread safe.
MM_EXPORT_DLL void mm_net_tcp_detach_socket(struct mm_net_tcp* p);
//////////////////////////////////////////////////////////////////////////
// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_net_tcp_buffer_send(struct mm_net_tcp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length);
// handle send data by flush send buffer.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_net_tcp_flush_send(struct mm_net_tcp* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_tcp_start(struct mm_net_tcp* p);
MM_EXPORT_DLL void mm_net_tcp_interrupt(struct mm_net_tcp* p);
MM_EXPORT_DLL void mm_net_tcp_shutdown(struct mm_net_tcp* p);
MM_EXPORT_DLL void mm_net_tcp_join(struct mm_net_tcp* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_net_tcp_h__