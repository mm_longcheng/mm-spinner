#include "net/mm_tcp.h"
#include "core/mm_logger.h"
#include "core/mm_errno.h"
#include "core/mm_alloc.h"
/////////////////////////////////////////////////////////////
static void __static_tcp_handle( void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length )
{

}
static void __static_tcp_broken( void* obj )
{

}
/////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_tcp_callback_init(struct mm_tcp_callback* p)
{
	p->handle = &__static_tcp_handle;
	p->broken = &__static_tcp_broken;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_tcp_callback_destroy(struct mm_tcp_callback* p)
{
	p->handle = &__static_tcp_handle;
	p->broken = &__static_tcp_broken;
	p->obj = NULL;
}
/////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_tcp_init(struct mm_tcp* p)
{
	mm_addr_init(&p->addr);// transport address.
	mm_streambuf_init(&p->buff_recv);
	mm_streambuf_init(&p->buff_send);
	mm_tcp_callback_init(&p->callback);
	p->unique_id = 0;
}
MM_EXPORT_DLL void mm_tcp_destroy(struct mm_tcp* p)
{
	mm_addr_shutdown_socket(&p->addr, MM_BOTH_SHUTDOWN);
	mm_addr_destroy(&p->addr);
	mm_streambuf_destroy(&p->buff_recv);
	mm_streambuf_destroy(&p->buff_send);
	mm_tcp_callback_destroy(&p->callback);
	p->unique_id = 0;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_tcp_lock(struct mm_tcp* p)
{
	mm_addr_lock(&p->addr);
}
MM_EXPORT_DLL void mm_tcp_unlock(struct mm_tcp* p)
{
	mm_addr_unlock(&p->addr);
}
//////////////////////////////////////////////////////////////////////////
// assign addr native by ip port.
MM_EXPORT_DLL void mm_tcp_assign_native(struct mm_tcp* p, const char* node, mm_ushort_t port)
{
	mm_addr_assign_native(&p->addr, node, port);
}
// assign addr remote by ip port.
MM_EXPORT_DLL void mm_tcp_assign_remote(struct mm_tcp* p, const char* node, mm_ushort_t port)
{
	mm_addr_assign_remote(&p->addr, node, port);
}
// assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
MM_EXPORT_DLL void mm_tcp_assign_native_storage(struct mm_tcp* p, struct sockaddr_storage* ss_native)
{
	mm_addr_assign_native_storage(&p->addr, ss_native);
}
// assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
MM_EXPORT_DLL void mm_tcp_assign_remote_storage(struct mm_tcp* p, struct sockaddr_storage* ss_remote)
{
	mm_addr_assign_remote_storage(&p->addr, ss_remote);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_tcp_assign_callback(struct mm_tcp* p,struct mm_tcp_callback* cb)
{
	assert(NULL != cb && "cb is a null.");
	p->callback = (*cb);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_tcp_set_unique_id(struct mm_tcp* p, mm_uint64_t unique_id)
{
	p->unique_id = unique_id;
}
MM_EXPORT_DLL mm_uint64_t mm_tcp_get_unique_id(struct mm_tcp* p)
{
	return p->unique_id;
}
// context for tcp will use the mm_addr.u user data.
MM_EXPORT_DLL void mm_tcp_set_context(struct mm_tcp* p, void* u)
{
	mm_addr_set_context(&p->addr,u);
}
MM_EXPORT_DLL void* mm_tcp_get_context(struct mm_tcp* p)
{
	return mm_addr_get_context(&p->addr);
}
//////////////////////////////////////////////////////////////////////////
// fopen socket.
MM_EXPORT_DLL void mm_tcp_fopen_socket(struct mm_tcp* p)
{
	char link_name[MM_LINK_NAME_LENGTH];
	struct mm_logger* g_logger = mm_logger_instance();
	//
	mm_addr_fopen_socket(&p->addr,p->addr.ss_remote.ss_family,SOCK_STREAM,0);
	//
	mm_addr_string(&p->addr,link_name);
	//
	if (MM_INVALID_SOCKET != p->addr.socket)
	{
		mm_logger_log_I(g_logger,"%s %d fopen_socket %s success.",__FUNCTION__,__LINE__,link_name);
	}
	else
	{
		//
		mm_logger_log_E(g_logger,"%s %d fopen_socket %s failure.",__FUNCTION__,__LINE__,link_name);
	}
}
// close socket.
MM_EXPORT_DLL void mm_tcp_close_socket(struct mm_tcp* p)
{
	mm_addr_close_socket(&p->addr);
}
// shutdown socket.
MM_EXPORT_DLL void mm_tcp_shutdown_socket(struct mm_tcp* p, int opcode)
{
	mm_addr_shutdown_socket(&p->addr,opcode);
}
//////////////////////////////////////////////////////////////////////////
// handle recv for buffer pool and pool max size.
MM_EXPORT_DLL void mm_tcp_handle_recv(struct mm_tcp* p, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length)
{
	int real_len = 0;
	char link_name[MM_LINK_NAME_LENGTH];
	assert(p->callback.handle && p->callback.broken && "callback.handle or callback.broken is invalid.");
	//
	do
	{
		mm_memset(buffer + offset,0,max_length - offset);
		real_len = mm_addr_recv(&p->addr, buffer, offset, (int)max_length, 0);
		if( -1 == real_len )
		{
			mm_err_t errcode = mm_errno;
			if( MM_EAGAIN == errcode )
			{
				// MM_EAGAIN errno:(10035) A non-blocking socket operation could not be completed immediately.
				// this error is not serious.
				break;
			}
			else if(
				MM_ECONNRESET == errcode ||
				MM_ECONNABORTED == errcode ||
				MM_ENOTSOCK == errcode )
			{
				struct mm_logger* g_logger = mm_logger_instance();
				mm_addr_string(&p->addr, link_name);
				// An existing connection was forcibly closed by the remote host.
				mm_logger_log_I(g_logger, "%s %d %s broken.", __FUNCTION__, __LINE__, link_name);
				mm_logger_socket_error(g_logger, MM_LOG_INFO, errcode);
				(*(p->callback.broken))(p);
				break;
			}
			else
			{
				struct mm_logger* g_logger = mm_logger_instance();
				mm_addr_string(&p->addr, link_name);
				// error occur.
				mm_logger_log_I(g_logger, "%s %d %s error occur.", __FUNCTION__, __LINE__, link_name);
				mm_logger_socket_error(g_logger, MM_LOG_INFO, errcode);
				(*(p->callback.broken))(p);
				break;
			}
		}
		else if( 0 == real_len )
		{
			// if this recv operate 0 == real_len,means the recv buffer is full or target socket is closed.
			// so we must use poll for checking the io event make sure read event is fire.
			// at here send length is 0 will fire the broken event.
			mm_err_t errcode = mm_errno;
			struct mm_logger* g_logger = mm_logger_instance();
			mm_addr_string(&p->addr, link_name);
			// the other side close the socket.
			mm_logger_log_I(g_logger, "%s %d %s broken.", __FUNCTION__, __LINE__, link_name);
			mm_logger_socket_error(g_logger, MM_LOG_INFO, errcode);
			(*(p->callback.broken))(p);
			break;
		}
		if( real_len != max_length )
		{
			mm_tcp_buffer_recv(p, buffer, offset, real_len);
			break;
		}
		else
		{
			mm_tcp_buffer_recv(p, buffer, offset, real_len);
		}
	} while ( 1 );
}
// handle send for buffer pool and pool max size.
MM_EXPORT_DLL void mm_tcp_handle_send(struct mm_tcp* p, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length)
{
	// do nothing.
}
//////////////////////////////////////////////////////////////////////////
// handle recv for buffer pool and pool max size.
MM_EXPORT_DLL int mm_tcp_buffer_recv(struct mm_tcp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length)
{
	assert(p->callback.handle && "callback.handle is invalid.");
	(*(p->callback.handle))(p, buffer, offset, length);
	return 0;
}
// handle send for buffer pool and pool max size.
MM_EXPORT_DLL int mm_tcp_buffer_send(struct mm_tcp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length)
{
	int real_len = 0;
	int send_len = 0;
	char link_name[MM_LINK_NAME_LENGTH];
	// assert(p->callback.handle&&p->callback.broken&&"callback.handle or callback.broken is invalid.");
	do 
	{
		if (0 == length)
		{
			// nothing for send break immediately.
			break;
		}
		real_len = mm_addr_send( &p->addr, buffer, offset, (int)length, 0 );
		if ( -1 == real_len )
		{
			mm_err_t errcode = mm_errno;
			if ( MM_EAGAIN == errcode ||
				 MM_ENOBUFS == errcode)
			{
				// MM_EAGAIN errno:(10035) A non-blocking socket operation could not be completed immediately.
				// this error is not serious.
				// MM_ENOBUFS if send buffer is full.break.
				// send_len will be 0.
				send_len = 0;
				break;
			}
			else
			{
				struct mm_logger* g_logger = mm_logger_instance();
				//
				mm_addr_string(&p->addr, link_name);
				mm_logger_log_I(g_logger, "%s %d %s broken.", __FUNCTION__, __LINE__, link_name);
				mm_logger_socket_error(g_logger, MM_LOG_INFO, errcode);
				// (*(p->callback.broken))(p);
				// at send process,we can not trigger the broken call back.
				mm_addr_shutdown_socket(&p->addr, MM_BOTH_SHUTDOWN);
				// error is occur,we assign the return code to -1 and break it immediately.
				send_len = -1;
				break;
			}
		}
		else if ( real_len == length )
		{
			// complete
			send_len += real_len;
			break;
		}
		else if ( 0 == real_len )
		{
			// if this send operate 0 == real_len,means the send buffer is full or target socket is closed.
			// so we must use poll for checking the io event make sure read event is fire.
			// at here send length is 0 will shutdown this socket.
			mm_err_t errcode = mm_errno;
			struct mm_logger* g_logger = mm_logger_instance();
			mm_addr_string(&p->addr, link_name);
			// conn disconnect
			mm_logger_log_I(g_logger, "%s %d %s broken.", __FUNCTION__, __LINE__, link_name);
			mm_logger_socket_error(g_logger, MM_LOG_INFO, errcode);
			// (*(p->callback.broken))(p);
			// at send process,we can not trigger the broken call back.
			mm_addr_shutdown_socket(&p->addr, MM_BOTH_SHUTDOWN);
			// error is occur,we assign the return code to -1 and break it immediately.
			send_len = -1;
			break;
		}
		else
		{
			// need cycle send.
			offset += real_len;
			length -= real_len;
			send_len += real_len;
		}
	} while ( 1 );
	return send_len;
}
// handle send data by flush send buffer.
MM_EXPORT_DLL int mm_tcp_flush_send(struct mm_tcp* p)
{
	size_t sz = mm_streambuf_size(&p->buff_send);
	int rt = mm_tcp_buffer_send(p, p->buff_send.buff, (mm_uint32_t)p->buff_send.gptr, (mm_uint32_t)sz);
	if ( 0 < rt )
	{
		// 0 <  rt,means rt buffer is send,we must gbump rt size.
		// 0 == rt,means the send buffer can be full.
		// 0 >  rt,means the send process can be failure.
		mm_streambuf_gbump(&p->buff_send, rt);
	}
	return rt;
}
// tcp streambuf recv send reset.
MM_EXPORT_DLL void mm_tcp_streambuf_reset(struct mm_tcp* p)
{
	mm_streambuf_reset(&p->buff_recv);
	mm_streambuf_reset(&p->buff_send);
}
