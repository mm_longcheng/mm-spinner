#include "net/mm_mailbox.h"
#include "net/mm_streambuf_packet.h"
#include "core/mm_alloc.h"
#include "core/mm_logger.h"
//
static void __static_mailbox_tcp_handle_callback(void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length);
static void __static_mailbox_tcp_broken_callback(void* obj);

static void __static_mailbox_nuclear_handle_recv(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length);
static void __static_mailbox_nuclear_handle_send(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length);

static void __static_mailbox_tcp_handle_packet_callback(void* obj,struct mm_packet* pack);

static void __static_mailbox_accepter_accept( void* obj, mm_socket_t fd, struct sockaddr_storage* remote );
//////////////////////////////////////////////////////////////////////////
static void __static_mailbox_handle(void* obj, void* u, struct mm_packet* pack)
{

}
static void __static_mailbox_broken(void* obj)
{

}
static void __static_mailbox_nready(void* obj)
{

}
static void __static_mailbox_finish(void* obj)
{

}
MM_EXPORT_DLL void mm_mailbox_callback_init(struct mm_mailbox_callback* p)
{
	p->handle = &__static_mailbox_handle;
	p->broken = &__static_mailbox_broken;
	p->nready = &__static_mailbox_nready;
	p->finish = &__static_mailbox_finish;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_mailbox_callback_destroy(struct mm_mailbox_callback* p)
{
	p->handle = &__static_mailbox_handle;
	p->broken = &__static_mailbox_broken;
	p->nready = &__static_mailbox_nready;
	p->finish = &__static_mailbox_finish;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
static void __static_mailbox_event_tcp_alloc(void* p,struct mm_tcp* tcp)
{

}
static void __static_mailbox_event_tcp_relax(void* p,struct mm_tcp* tcp)
{

}

MM_EXPORT_DLL void mm_mailbox_event_tcp_alloc_init(struct mm_mailbox_event_tcp_alloc* p)
{
	p->event_tcp_alloc = &__static_mailbox_event_tcp_alloc;
	p->event_tcp_relax = &__static_mailbox_event_tcp_relax;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_mailbox_event_tcp_alloc_destroy(struct mm_mailbox_event_tcp_alloc* p)
{
	p->event_tcp_alloc = &__static_mailbox_event_tcp_alloc;
	p->event_tcp_relax = &__static_mailbox_event_tcp_relax;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mailbox_init(struct mm_mailbox* p)
{
	struct mm_holder_u32_vpt_alloc holder32_alloc;
	struct mm_accepter_callback accepter_callback;
	struct mm_nuclear_callback nuclear_callback;

	mm_accepter_init(&p->accepter);
	mm_nucleus_init(&p->nucleus);
	mm_holder_u32_vpt_init(&p->holder);
	mm_mailbox_callback_init(&p->callback);
	mm_mailbox_event_tcp_alloc_init(&p->event_tcp_alloc);
	mm_crypto_callback_init(&p->crypto_callback);
	mm_spin_init(&p->holder_locker, NULL);
	mm_spin_init(&p->locker, NULL);
	p->u = NULL;

	accepter_callback.accept = &__static_mailbox_accepter_accept;
	accepter_callback.obj = p;
	mm_accepter_assign_callback(&p->accepter,&accepter_callback);

	nuclear_callback.handle_recv = &__static_mailbox_nuclear_handle_recv;
	nuclear_callback.handle_send = &__static_mailbox_nuclear_handle_send;
	nuclear_callback.obj = p;
	mm_nucleus_assign_callback(&p->nucleus,&nuclear_callback);

	holder32_alloc.alloc = &mm_holder_u32_vpt_weak_alloc;
	holder32_alloc.relax = &mm_holder_u32_vpt_weak_relax;
	holder32_alloc.obj = p;
	mm_holder_u32_vpt_assign_alloc(&p->holder,&holder32_alloc);
}
MM_EXPORT_DLL void mm_mailbox_destroy(struct mm_mailbox* p)
{
	mm_mailbox_clear_callback_holder(p);
	mm_mailbox_clear(p);

	mm_accepter_destroy(&p->accepter);
	mm_nucleus_destroy(&p->nucleus);
	mm_holder_u32_vpt_destroy(&p->holder);
	mm_mailbox_callback_destroy(&p->callback);
	mm_mailbox_event_tcp_alloc_destroy(&p->event_tcp_alloc);
	mm_crypto_callback_destroy(&p->crypto_callback);
	mm_spin_destroy(&p->holder_locker);
	mm_spin_destroy(&p->locker);
	p->u = NULL;
}
MM_EXPORT_DLL void mm_mailbox_assign_native(struct mm_mailbox* p,const char* node,mm_ushort_t port)
{
	mm_accepter_assign_native(&p->accepter, node, port);
}
MM_EXPORT_DLL void mm_mailbox_assign_remote(struct mm_mailbox* p,const char* node,mm_ushort_t port)
{
	mm_accepter_assign_remote(&p->accepter, node, port);
}
// assign protocol_size.
MM_EXPORT_DLL void mm_mailbox_set_length(struct mm_mailbox* p, mm_uint32_t length)
{
	mm_nucleus_set_length(&p->nucleus,length);
}
MM_EXPORT_DLL mm_uint32_t mm_mailbox_get_length(struct mm_mailbox* p)
{
	return mm_nucleus_get_length(&p->nucleus);
}
// assign context handle.
MM_EXPORT_DLL void mm_mailbox_assign_context(struct mm_mailbox* p,void* u)
{
	p->u = u;
}
// fopen.
MM_EXPORT_DLL void mm_mailbox_fopen_socket(struct mm_mailbox* p)
{
	mm_accepter_fopen_socket(&p->accepter);
}
// bind.
MM_EXPORT_DLL void mm_mailbox_bind(struct mm_mailbox* p)
{
	mm_accepter_bind(&p->accepter);
}
// listen.
MM_EXPORT_DLL void mm_mailbox_listen(struct mm_mailbox* p)
{
	mm_accepter_listen(&p->accepter);
}
// close socket. mm_mailbox_join before call this.
MM_EXPORT_DLL void mm_mailbox_close_socket(struct mm_mailbox* p)
{
	mm_accepter_close_socket(&p->accepter);
}
//////////////////////////////////////////////////////////////////////////
// lock to make sure the mailbox is thread safe.
MM_EXPORT_DLL void mm_mailbox_lock(struct mm_mailbox* p)
{
	mm_spin_lock(&p->locker);
}
// unlock mailbox.
MM_EXPORT_DLL void mm_mailbox_unlock(struct mm_mailbox* p)
{
	mm_spin_unlock(&p->locker);
}

MM_EXPORT_DLL void mm_mailbox_assign_callback(struct mm_mailbox* p,mm_uint32_t id,mailbox_handle callback)
{
	mm_spin_lock(&p->holder_locker);
	mm_holder_u32_vpt_set(&p->holder,id,(void*)callback);
	mm_spin_unlock(&p->holder_locker);
}
MM_EXPORT_DLL void mm_mailbox_assign_event_tcp_alloc(struct mm_mailbox* p,struct mm_mailbox_event_tcp_alloc* event_tcp_alloc)
{
	assert(NULL != event_tcp_alloc && "you can not assign null event_tcp_alloc.");
	p->event_tcp_alloc = *event_tcp_alloc;
}
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_mailbox_assign_crypto_callback(struct mm_mailbox* p,struct mm_crypto_callback* crypto_callback)
{
	assert(NULL != crypto_callback && "you can not assign null crypto_callback.");
	p->crypto_callback = *crypto_callback;
}
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_mailbox_assign_mailbox_callback(struct mm_mailbox* p,struct mm_mailbox_callback* mailbox_callback)
{
	assert(NULL != mailbox_callback && "you can not assign null mailbox_callback.");
	p->callback = *mailbox_callback;
}
MM_EXPORT_DLL void mm_mailbox_assign_tcp_alloc(struct mm_mailbox* p,struct mm_holder_u32_vpt_alloc* alloc)
{
	mm_spin_lock(&p->holder_locker);
	mm_holder_u32_vpt_assign_alloc(&p->holder,alloc);
	mm_spin_unlock(&p->holder_locker);
}
MM_EXPORT_DLL void mm_mailbox_clear_callback_holder(struct mm_mailbox* p)
{
	mm_spin_lock(&p->holder_locker);
	mm_holder_u32_vpt_clear(&p->holder);
	mm_spin_unlock(&p->holder_locker);
}
// mailbox crypto encrypt buffer by tcp.
MM_EXPORT_DLL void mm_mailbox_crypto_encrypt(struct mm_mailbox* p,struct mm_tcp* tcp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length)
{
	(*(p->crypto_callback.encrypt))(p,tcp,buffer,offset,buffer,offset,length);
}
// mailbox crypto decrypt buffer by tcp.
MM_EXPORT_DLL void mm_mailbox_crypto_decrypt(struct mm_mailbox* p,struct mm_tcp* tcp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length)
{
	(*(p->crypto_callback.decrypt))(p,tcp,buffer,offset,buffer,offset,length);
}
//////////////////////////////////////////////////////////////////////////
// not lock inside,lock tcp manual.
MM_EXPORT_DLL void mm_mailbox_traver(struct mm_mailbox* p,mailbox_traver func,void* u)
{
	mm_uint32_t i = 0;
	//
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	struct mm_nuclear* nuclear = NULL;
	struct mm_tcp* tcp = NULL;
	//
	struct mm_holder_u32_vpt fd_holder;
	//
	mm_holder_u32_vpt_init(&fd_holder);
	mm_spin_lock(&p->nucleus.arrays_locker);
	for ( i = 0; i < p->nucleus.length; ++i)
	{
		nuclear = p->nucleus.arrays[i];
		// the nuclear is only one thread fd set.
		// N fd at one set.we copy a weak ref for set.
		// note: if the set is too big will case performance problem.
		// N is less than [2000,5000] recommend.
		// machine 2 cores 2G keep 2 * 5000 = 10000 connect.
		mm_nuclear_fd_copy_holder(nuclear,&fd_holder);
		//
		n = mm_rb_first(&fd_holder.rbt);
		while(NULL != n)
		{
			it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n, struct mm_holder_u32_vpt_iterator, n);
			n = mm_rb_next(n);
			// here we use the cache fd at fd_holder.
			tcp = (struct mm_tcp*)mm_nucleus_fd_get(&p->nucleus,it->k);
			if (NULL != tcp)
			{
				// if the tcp value is exists.
				// we trigger callback.
				// note: the callback function inside recommend lock the tcp ref immediately
				//       make sure the tcp memory can be a valid.
				(*(func))(p,tcp,u);
			}
		}
	}
	mm_spin_unlock(&p->nucleus.arrays_locker);
	mm_holder_u32_vpt_destroy(&fd_holder);
}
// poll size.
MM_EXPORT_DLL size_t mm_mailbox_poll_size(struct mm_mailbox* p)
{
	return mm_nucleus_poll_size(&p->nucleus);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mailbox_start(struct mm_mailbox* p)
{
	mm_accepter_start(&p->accepter);
	mm_nucleus_start(&p->nucleus);
}
MM_EXPORT_DLL void mm_mailbox_interrupt(struct mm_mailbox* p)
{
	mm_accepter_interrupt(&p->accepter);
	mm_nucleus_interrupt(&p->nucleus);
}
MM_EXPORT_DLL void mm_mailbox_shutdown(struct mm_mailbox* p)
{
	mm_accepter_shutdown(&p->accepter);
	mm_nucleus_shutdown(&p->nucleus);
}
MM_EXPORT_DLL void mm_mailbox_join(struct mm_mailbox* p)
{
	mm_accepter_join(&p->accepter);
	mm_nucleus_join(&p->nucleus);
}

MM_EXPORT_DLL struct mm_tcp* mm_mailbox_add(struct mm_mailbox* p,mm_socket_t fd,struct sockaddr_storage* remote)
{
	struct mm_tcp_callback tcp_callback;
	struct mm_tcp* tcp = (struct mm_tcp*)mm_malloc(sizeof(struct mm_tcp));
	mm_tcp_init(tcp);
	//
	tcp_callback.handle = &__static_mailbox_tcp_handle_callback;
	tcp_callback.broken = &__static_mailbox_tcp_broken_callback;
	tcp_callback.obj = p;
	mm_tcp_assign_callback(tcp,&tcp_callback);
	//
	mm_tcp_assign_remote_storage(tcp,remote);
	mm_tcp_assign_native_storage(tcp,&p->accepter.addr.ss_native);
	tcp->addr.socket = fd;
	mm_addr_set_block(&tcp->addr, MM_NONBLOCK);
	//
	mm_nucleus_fd_add(&p->nucleus, fd, tcp);
	//
	(*(p->event_tcp_alloc.event_tcp_alloc))(p,tcp);
	//
	return tcp;
}
MM_EXPORT_DLL struct mm_tcp* mm_mailbox_get(struct mm_mailbox* p,mm_socket_t fd)
{
	return (struct mm_tcp*)mm_nucleus_fd_get(&p->nucleus,fd);
}
MM_EXPORT_DLL struct mm_tcp* mm_mailbox_get_instance(struct mm_mailbox* p,mm_socket_t fd,struct sockaddr_storage* remote)
{
	struct mm_tcp* tcp = mm_mailbox_get(p,fd);
	if (NULL == tcp)
	{
		tcp = mm_mailbox_add(p,fd,remote);
	}
	return tcp;
}
MM_EXPORT_DLL void mm_mailbox_rmv(struct mm_mailbox* p,mm_socket_t fd)
{
	struct mm_tcp* tcp = mm_mailbox_get(p,fd);
	if (NULL != tcp)
	{
		// mm_mailbox_rmv_tcp(p,tcp);
		// here we can lock the tcp,becaue all callback.broken only recv process trigger.
		mm_tcp_lock(tcp);
		mm_nucleus_fd_rmv(&p->nucleus, fd, tcp);
		mm_tcp_unlock(tcp);
		(*(p->event_tcp_alloc.event_tcp_relax))(p,tcp);
		mm_tcp_destroy(tcp);
		mm_free(tcp);
	}
}
MM_EXPORT_DLL void mm_mailbox_rmv_tcp(struct mm_mailbox* p,struct mm_tcp* tcp)
{
	mm_nucleus_fd_rmv(&p->nucleus, tcp->addr.socket, tcp);
}
MM_EXPORT_DLL void mm_mailbox_clear(struct mm_mailbox* p)
{
	mm_uint32_t i = 0;
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	struct mm_tcp* tcp = NULL;
	struct mm_nuclear* nuclear = NULL;
	//
	mm_spin_lock(&p->nucleus.arrays_locker);
	for ( i = 0; i < p->nucleus.length; ++i)
	{
		nuclear = p->nucleus.arrays[i];
		pthread_mutex_lock(&nuclear->cond_locker);
		n = mm_rb_first(&nuclear->holder.rbt);
		while(NULL != n)
		{
			it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n, struct mm_holder_u32_vpt_iterator, n);
			n = mm_rb_next(n);
			tcp = (struct mm_tcp*)(it->v);
			mm_tcp_lock(tcp);
			mm_nuclear_fd_rmv_poll(nuclear, tcp->addr.socket, tcp);
			mm_tcp_unlock(tcp);
			(*(p->event_tcp_alloc.event_tcp_relax))(p,tcp);
			mm_tcp_destroy(tcp);
			mm_free(tcp);
		}
		pthread_mutex_unlock(&nuclear->cond_locker);
		// trigger poll wake signal.
		mm_nuclear_fd_poll_signal(nuclear);
	}
	mm_spin_unlock(&p->nucleus.arrays_locker);
}
//////////////////////////////////////////////////////////////////////////
// use this function for manual shutdown tcp.
// note: you can not use rmv or rmv_tcp for shutdown a activation tcp.
MM_EXPORT_DLL void mm_mailbox_shutdown_tcp(struct mm_mailbox* p,struct mm_tcp* tcp)
{
	mm_tcp_shutdown_socket(tcp,MM_BOTH_SHUTDOWN);
}
//////////////////////////////////////////////////////////////////////////
static void __static_mailbox_tcp_handle_callback(void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length)
{
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_mailbox* mailbox = (struct mm_mailbox*)(tcp->callback.obj);
	mm_mailbox_crypto_decrypt(mailbox,tcp,buffer,offset,length);
	mm_streambuf_packet_handle_tcp(&tcp->buff_recv, buffer, offset, length, &__static_mailbox_tcp_handle_packet_callback, tcp);
}
static void __static_mailbox_tcp_broken_callback(void* obj)
{
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_mailbox* mailbox = (struct mm_mailbox*)(tcp->callback.obj);
	assert(mailbox->callback.broken&&"mailbox->broken is a null.");
	(*(mailbox->callback.broken))(tcp);
	mm_mailbox_rmv(mailbox,tcp->addr.socket);
}

static void __static_mailbox_nuclear_handle_recv(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length)
{
	struct mm_tcp* tcp = (struct mm_tcp*)u;
	mm_tcp_handle_recv(tcp,buffer,offset,max_length);
}
static void __static_mailbox_nuclear_handle_send(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length)
{
	struct mm_tcp* tcp = (struct mm_tcp*)u;
	mm_tcp_handle_send(tcp,buffer,offset,max_length);
}

static void __static_mailbox_tcp_handle_packet_callback(void* obj,struct mm_packet* pack)
{
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_mailbox* mailbox = (struct mm_mailbox*)(tcp->callback.obj);
	mailbox_handle handle = NULL;
	mm_spin_lock(&mailbox->holder_locker);
	handle = (mailbox_handle)mm_holder_u32_vpt_get(&mailbox->holder,pack->phead.mid);
	mm_spin_unlock(&mailbox->holder_locker);
	if (NULL != handle)
	{
		// fire the handle event.
		(*(handle))(tcp,mailbox->u,pack);
	}
	else
	{
		assert(mailbox->callback.handle&&"mailbox->handle is a null.");
		(*(mailbox->callback.handle))(tcp,mailbox->u,pack);
	}
}
static void __static_mailbox_accepter_accept( void* obj, mm_socket_t fd, struct sockaddr_storage* remote )
{
	struct mm_accepter* accepter = (struct mm_accepter*)obj;
	struct mm_mailbox* mailbox = (struct mm_mailbox*)(accepter->callback.obj);
	mm_mailbox_get_instance(mailbox,fd,remote);
}
