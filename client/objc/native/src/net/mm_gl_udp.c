#include "net/mm_gl_udp.h"
#include "net/mm_streambuf_packet.h"
#include "core/mm_alloc.h"
#include "core/mm_logger.h"
//////////////////////////////////////////////////////////////////////////
static void __static_gl_udp_udp_handle_callback(void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote);
static void __static_gl_udp_udp_broken_callback(void* obj);

static void __static_gl_udp_udp_handle_packet_callback(void* obj, struct mm_packet* pack, struct sockaddr_storage* remote);

static void __static_gl_udp_net_udp_handle_callback(void* obj, void* u, struct mm_packet* pack, struct sockaddr_storage* remote);
static void __static_gl_udp_net_udp_broken_callback(void* obj);
static void __static_gl_udp_net_udp_nready_callback(void* obj);
static void __static_gl_udp_net_udp_finish_callback(void* obj);

static int __static_gl_udp_flush_signal_task_factor(void* obj);
static int __static_gl_udp_flush_signal_task_handle(void* obj);
static int __static_gl_udp_state_signal_task_factor(void* obj);
static int __static_gl_udp_state_signal_task_handle(void* obj);
//////////////////////////////////////////////////////////////////////////
static void __static_gl_udp_handle(void* obj, void* u, struct mm_packet* pack, struct sockaddr_storage* remote)
{

}
MM_EXPORT_DLL void mm_gl_udp_callback_init(struct mm_gl_udp_callback* p)
{
	p->handle = &__static_gl_udp_handle;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_gl_udp_callback_destroy(struct mm_gl_udp_callback* p)
{
	p->handle = &__static_gl_udp_handle;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_udp_init(struct mm_gl_udp* p)
{
	struct mm_udp_callback udp_callback;
	struct mm_net_udp_callback net_udp_callback;
	struct mm_signal_task_callback flush_task_callback;
	struct mm_signal_task_callback state_task_callback;

	mm_sockaddr_storage_init(&p->ss_native);
	mm_sockaddr_storage_init(&p->ss_remote);
	mm_net_udp_init(&p->net_udp);
	mm_mq_udp_init(&p->queue_udp);
	mm_gl_udp_callback_init(&p->callback);
	mm_signal_task_init(&p->flush_task);
	mm_signal_task_init(&p->state_task);
	mm_spin_init(&p->locker, NULL);
	p->state_check_flag = gl_udp_check_inactive;
	p->push_to_queue_flag = gl_udp_ptq_activate;
	p->state = ts_closed;
	p->u = NULL;

	udp_callback.handle = &__static_gl_udp_udp_handle_callback;
	udp_callback.broken = &__static_gl_udp_udp_broken_callback;
	udp_callback.obj = p;
	mm_udp_assign_callback(&p->net_udp.udp,&udp_callback);

	net_udp_callback.handle = &__static_gl_udp_net_udp_handle_callback;
	net_udp_callback.broken = &__static_gl_udp_net_udp_broken_callback;
	net_udp_callback.nready = &__static_gl_udp_net_udp_nready_callback;
	net_udp_callback.finish = &__static_gl_udp_net_udp_finish_callback;
	net_udp_callback.obj = p;
	mm_net_udp_assign_net_udp_callback(&p->net_udp,&net_udp_callback);

	flush_task_callback.factor = &__static_gl_udp_flush_signal_task_factor;
	flush_task_callback.handle = &__static_gl_udp_flush_signal_task_handle;
	flush_task_callback.obj = p;
	mm_signal_task_assign_callback(&p->flush_task,&flush_task_callback);

	state_task_callback.factor = &__static_gl_udp_state_signal_task_factor;
	state_task_callback.handle = &__static_gl_udp_state_signal_task_handle;
	state_task_callback.obj = p;
	mm_signal_task_assign_callback(&p->state_task,&state_task_callback);

	mm_signal_task_assign_success_nearby(&p->flush_task,MM_GL_UDP_SEND_INTERVAL_HANDLE_TIME);
	mm_signal_task_assign_failure_nearby(&p->flush_task,MM_GL_UDP_SEND_INTERVAL_BROKEN_TIME);
	mm_signal_task_assign_success_nearby(&p->state_task,MM_GL_UDP_HANDLE_MSEC_CHECK);
	mm_signal_task_assign_failure_nearby(&p->state_task,MM_GL_UDP_BROKEN_MSEC_CHECK);
}
MM_EXPORT_DLL void mm_gl_udp_destroy(struct mm_gl_udp* p)
{
	mm_gl_udp_clear_callback_holder(p);
	mm_sockaddr_storage_destroy(&p->ss_native);
	mm_sockaddr_storage_destroy(&p->ss_remote);
	mm_net_udp_destroy(&p->net_udp);
	mm_mq_udp_destroy(&p->queue_udp);
	mm_gl_udp_callback_destroy(&p->callback);
	mm_signal_task_destroy(&p->flush_task);
	mm_signal_task_destroy(&p->state_task);
	mm_spin_destroy(&p->locker);
	p->state_check_flag = gl_udp_check_inactive;
	p->push_to_queue_flag = gl_udp_ptq_activate;
	p->state = ts_closed;
	p->u = NULL;
}
//////////////////////////////////////////////////////////////////////////
// assign native address but not connect.
MM_EXPORT_DLL void mm_gl_udp_assign_native(struct mm_gl_udp* p,const char* node,mm_ushort_t port)
{
	mm_net_udp_assign_native(&p->net_udp,node,port);
}
// assign remote address but not connect.
MM_EXPORT_DLL void mm_gl_udp_assign_remote(struct mm_gl_udp* p,const char* node,mm_ushort_t port)
{
	mm_net_udp_assign_remote(&p->net_udp,node,port);
}
// assign native address but not connect.target value not current real.will apply at state thread.
MM_EXPORT_DLL void mm_gl_udp_assign_native_target(struct mm_gl_udp* p,const char* node,mm_ushort_t port)
{
	mm_sockaddr_storage_assign(&p->ss_native, node, port);
	p->ss_remote.ss_family = p->ss_native.ss_family;
}
// assign remote address but not connect.target value not current real..will apply at state thread.
MM_EXPORT_DLL void mm_gl_udp_assign_remote_target(struct mm_gl_udp* p,const char* node,mm_ushort_t port)
{
	mm_sockaddr_storage_assign(&p->ss_remote, node, port);
	p->ss_native.ss_family = p->ss_remote.ss_family;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_udp_assign_q_default_callback(struct mm_gl_udp* p,struct mm_mq_udp_callback* mq_udp_callback)
{
	mm_mq_udp_assign_queue_udp_callback(&p->queue_udp,mq_udp_callback);
}
MM_EXPORT_DLL void mm_gl_udp_assign_n_default_callback(struct mm_gl_udp* p,struct mm_gl_udp_callback* gl_udp_callback)
{
	assert(NULL != gl_udp_callback && "you can not assign null gl_udp_callback.");
	p->callback = *gl_udp_callback;
}
// assign queue callback.at main thread.
MM_EXPORT_DLL void mm_gl_udp_assign_q_callback(struct mm_gl_udp* p,mm_uint32_t id,net_udp_handle callback)
{
	mm_mq_udp_assign_callback(&p->queue_udp,id,callback);
}
// assign net callback.not at main thread.
MM_EXPORT_DLL void mm_gl_udp_assign_n_callback(struct mm_gl_udp* p,mm_uint32_t id,net_udp_handle callback)
{
	mm_net_udp_assign_callback(&p->net_udp,id,callback);
}
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_gl_udp_assign_event_udp_alloc(struct mm_gl_udp* p,struct mm_net_udp_event_udp_alloc* event_udp_alloc)
{
	mm_net_udp_assign_event_udp_alloc(&p->net_udp,event_udp_alloc);
}
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_gl_udp_assign_crypto_callback(struct mm_gl_udp* p,struct mm_crypto_callback* crypto_callback)
{
	mm_net_udp_assign_crypto_callback(&p->net_udp,crypto_callback);
}
// assign context handle.
MM_EXPORT_DLL void mm_gl_udp_assign_context(struct mm_gl_udp* p,void* u)
{
	p->u = u;
	mm_mq_udp_assign_context(&p->queue_udp,p->u);
	mm_net_udp_assign_context(&p->net_udp,p->u);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_udp_clear_callback_holder(struct mm_gl_udp* p)
{
	mm_mq_udp_clear_callback_holder(&p->queue_udp);
	mm_net_udp_clear_callback_holder(&p->net_udp);
}
//////////////////////////////////////////////////////////////////////////
// context for udp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_DLL void mm_gl_udp_set_addr_context(struct mm_gl_udp* p, void* u)
{
	mm_net_udp_set_addr_context(&p->net_udp,u);
}
MM_EXPORT_DLL void* mm_gl_udp_get_addr_context(struct mm_gl_udp* p)
{
	return mm_net_udp_get_addr_context(&p->net_udp);
}
//////////////////////////////////////////////////////////////////////////
// gl_udp crypto encrypt buffer by udp.
MM_EXPORT_DLL void mm_gl_udp_crypto_encrypt(struct mm_gl_udp* p,struct mm_udp* udp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length)
{
	mm_net_udp_crypto_encrypt(&p->net_udp,udp,buffer,offset,length);
}
// gl_udp crypto decrypt buffer by udp.
MM_EXPORT_DLL void mm_gl_udp_crypto_decrypt(struct mm_gl_udp* p,struct mm_udp* udp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length)
{
	mm_net_udp_crypto_decrypt(&p->net_udp,udp,buffer,offset,length);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_udp_assign_flush_handle_nearby(struct mm_gl_udp* p,mm_msec_t milliseconds)
{
	mm_signal_task_assign_success_nearby(&p->flush_task,milliseconds);
}
MM_EXPORT_DLL void mm_gl_udp_assign_flush_broken_nearby(struct mm_gl_udp* p,mm_msec_t milliseconds)
{
	mm_signal_task_assign_failure_nearby(&p->flush_task,milliseconds);
}
MM_EXPORT_DLL void mm_gl_udp_assign_state_handle_nearby(struct mm_gl_udp* p,mm_msec_t milliseconds)
{
	mm_signal_task_assign_success_nearby(&p->state_task,milliseconds);
}
MM_EXPORT_DLL void mm_gl_udp_assign_state_broken_nearby(struct mm_gl_udp* p,mm_msec_t milliseconds)
{
	mm_signal_task_assign_failure_nearby(&p->state_task,milliseconds);
}
MM_EXPORT_DLL void mm_gl_udp_assign_state_check_flag(struct mm_gl_udp* p,mm_uint8_t flag)
{
	p->state_check_flag = flag;
}
MM_EXPORT_DLL void mm_gl_udp_assign_push_to_queue_flag(struct mm_gl_udp* p,mm_uint8_t flag)
{
	p->push_to_queue_flag = flag;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_udp_assign_max_poper_number(struct mm_gl_udp* p,size_t max_pop)
{
	mm_mq_udp_assign_max_poper_number(&p->queue_udp,max_pop);
}
//////////////////////////////////////////////////////////////////////////
// lock to make sure the mm_gl_udp is thread safe.
MM_EXPORT_DLL void mm_gl_udp_lock(struct mm_gl_udp* p)
{
	mm_spin_lock(&p->locker);
}
// unlock net_udp.
MM_EXPORT_DLL void mm_gl_udp_unlock(struct mm_gl_udp* p)
{
	mm_spin_unlock(&p->locker);
}
// lock the flush send udp buffer.
MM_EXPORT_DLL void mm_gl_udp_flush_lock(struct mm_gl_udp* p)
{
	mm_udp_lock(&p->net_udp.udp);
}
// unlock the flush send udp buffer.
MM_EXPORT_DLL void mm_gl_udp_flush_unlock(struct mm_gl_udp* p)
{
	mm_udp_unlock(&p->net_udp.udp);
}
//////////////////////////////////////////////////////////////////////////
// fopen socket.
MM_EXPORT_DLL void mm_gl_udp_fopen_socket(struct mm_gl_udp* p)
{
	mm_net_udp_fopen_socket(&p->net_udp);
}
// bind.
MM_EXPORT_DLL void mm_gl_udp_bind(struct mm_gl_udp* p)
{
	mm_net_udp_bind(&p->net_udp);
}
// close socket.
MM_EXPORT_DLL void mm_gl_udp_close_socket(struct mm_gl_udp* p)
{
	mm_net_udp_close_socket(&p->net_udp);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_udp_check(struct mm_gl_udp* p)
{
	mm_net_udp_check(&p->net_udp);
}
// get state finally.return 0 for all regular.not thread safe.
MM_EXPORT_DLL int mm_gl_udp_finally_state(struct mm_gl_udp* p)
{
	return mm_net_udp_finally_state(&p->net_udp);
}
//////////////////////////////////////////////////////////////////////////
// synchronize attach to socket.not thread safe.
MM_EXPORT_DLL void mm_gl_udp_attach_socket(struct mm_gl_udp* p)
{
	mm_net_udp_attach_socket(&p->net_udp);
}
// synchronize detach to socket.not thread safe.
MM_EXPORT_DLL void mm_gl_udp_detach_socket(struct mm_gl_udp* p)
{
	mm_net_udp_detach_socket(&p->net_udp);
}
// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_DLL void mm_gl_udp_thread_handle(struct mm_gl_udp* p)
{
	mm_mq_udp_thread_handle(&p->queue_udp);
}
MM_EXPORT_DLL void mm_gl_udp_push(struct mm_gl_udp* p, void* obj, struct mm_packet* pack, struct sockaddr_storage* remote)
{
	mm_mq_udp_push(&p->queue_udp, obj, pack, remote);
}
//////////////////////////////////////////////////////////////////////////
// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_gl_udp_buffer_send(struct mm_gl_udp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote)
{
	return mm_net_udp_buffer_send(&p->net_udp,buffer,offset,length,remote);
}
// handle send data by flush send buffer.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_gl_udp_flush_send(struct mm_gl_udp* p)
{
	return mm_net_udp_flush_send(&p->net_udp);
}
//////////////////////////////////////////////////////////////////////////
// use for trigger the flush send thread signal.
// is useful for main thread asynchronous send message.
MM_EXPORT_DLL void mm_gl_udp_flush_signal(struct mm_gl_udp* p)
{
	mm_signal_task_signal(&p->flush_task);
}
// use for trigger the state check thread signal.
// is useful for main thread asynchronous state check.
MM_EXPORT_DLL void mm_gl_udp_state_signal(struct mm_gl_udp* p)
{
	mm_signal_task_signal(&p->state_task);
}
//////////////////////////////////////////////////////////////////////////
// start wait thread.
MM_EXPORT_DLL void mm_gl_udp_start(struct mm_gl_udp* p)
{
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	mm_net_udp_start(&p->net_udp);
	mm_signal_task_start(&p->flush_task);
	mm_signal_task_start(&p->state_task);
}
// interrupt wait thread.
MM_EXPORT_DLL void mm_gl_udp_interrupt(struct mm_gl_udp* p)
{
	p->state = ts_closed;
	mm_net_udp_interrupt(&p->net_udp);
	// 
	mm_mq_udp_cond_not_null(&p->queue_udp);
	mm_mq_udp_cond_not_full(&p->queue_udp);
	// signal the thread.
	mm_signal_task_interrupt(&p->flush_task);
	mm_signal_task_interrupt(&p->state_task);
}
// shutdown wait thread.
MM_EXPORT_DLL void mm_gl_udp_shutdown(struct mm_gl_udp* p)
{
	p->state = ts_finish;
	mm_net_udp_shutdown(&p->net_udp);
	// 
	mm_mq_udp_cond_not_null(&p->queue_udp);
	mm_mq_udp_cond_not_full(&p->queue_udp);
	// signal the thread.
	mm_signal_task_shutdown(&p->flush_task);
	mm_signal_task_shutdown(&p->state_task);
}
// join wait thread.
MM_EXPORT_DLL void mm_gl_udp_join(struct mm_gl_udp* p)
{
	mm_net_udp_join(&p->net_udp);
	// signal the thread.
	mm_signal_task_join(&p->flush_task);
	mm_signal_task_join(&p->state_task);
}
//////////////////////////////////////////////////////////////////////////
static void __static_gl_udp_udp_handle_callback(void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote)
{
	struct mm_udp* udp = (struct mm_udp*)(obj);
	struct mm_gl_udp* gl_udp = (struct mm_gl_udp*)(udp->callback.obj);
	mm_net_udp_crypto_decrypt(&gl_udp->net_udp,udp,buffer,offset,length);
	mm_streambuf_packet_handle_udp(&udp->buff_recv, buffer, offset, length, &__static_gl_udp_udp_handle_packet_callback, udp, remote);
}
static void __static_gl_udp_udp_broken_callback(void* obj)
{
	struct mm_udp* udp = (struct mm_udp*)(obj);
	struct mm_gl_udp* gl_udp = (struct mm_gl_udp*)(udp->callback.obj);
	struct mm_net_udp* net_udp = &gl_udp->net_udp;
	assert(NULL != net_udp->callback.broken && "net_udp->callback.broken is a null.");
	mm_net_udp_apply_broken(net_udp);
}
static void __static_gl_udp_udp_handle_packet_callback(void* obj, struct mm_packet* pack, struct sockaddr_storage* remote)
{
	struct mm_udp* udp = (struct mm_udp*)(obj);
	struct mm_gl_udp* gl_udp = (struct mm_gl_udp*)(udp->callback.obj);
	struct mm_net_udp* net_udp = &gl_udp->net_udp;
	assert(NULL != net_udp->callback.handle && "net_udp->callback.handle is a null.");
	(*(net_udp->callback.handle))(udp,gl_udp->u,pack,remote);
}
static void __static_gl_udp_net_udp_handle_callback(void* obj, void* u, struct mm_packet* pack, struct sockaddr_storage* remote)
{
	struct mm_udp* udp = (struct mm_udp*)(obj);
	struct mm_gl_udp* gl_udp = (struct mm_gl_udp*)(udp->callback.obj);
	struct mm_net_udp* net_udp = &gl_udp->net_udp;
	net_udp_handle handle = NULL;
	mm_spin_lock(&net_udp->holder_locker);
	handle = (net_udp_handle)mm_holder_u32_vpt_get(&net_udp->holder,pack->phead.mid);
	mm_spin_unlock(&net_udp->holder_locker);
	if (NULL != handle)
	{
		// fire the handle event.
		(*(handle))(udp,u,pack,remote);
	}
	else
	{
		assert(NULL != gl_udp->callback.handle && "gl_udp->callback.handle is a null.");
		(*(gl_udp->callback.handle))(udp,u,pack,remote);
	}
	// push to queue thread.
	if (gl_udp_ptq_activate == gl_udp->push_to_queue_flag)
	{
		mm_gl_udp_push(gl_udp, obj, pack, remote);
	}
}
static void __static_gl_udp_net_udp_broken_callback(void* obj)
{
	struct sockaddr_storage remote;
	struct mm_packet pack;
	struct mm_udp* udp = (struct mm_udp*)(obj);
	struct mm_gl_udp* gl_udp = (struct mm_gl_udp*)(udp->callback.obj);
	// push to queue thread.
	mm_sockaddr_storage_init(&remote);
	mm_packet_init(&pack);
	mm_packet_head_base_zero(&pack);
	pack.hbuff.length = MM_MSG_COMM_HEAD_SIZE;
	pack.bbuff.length = 0;
	mm_packet_shadow_alloc(&pack);
	pack.phead.mid = udp_mid_broken;
    mm_packet_head_base_encode(&pack);
	__static_gl_udp_net_udp_handle_callback(obj,gl_udp->u,&pack,&remote);
	mm_packet_shadow_free(&pack);
	mm_packet_destroy(&pack);
	mm_sockaddr_storage_destroy(&remote);
	// net state signal.
	mm_gl_udp_state_signal(gl_udp);
}
static void __static_gl_udp_net_udp_nready_callback(void* obj)
{
	struct sockaddr_storage remote;
	struct mm_packet pack;
	struct mm_udp* udp = (struct mm_udp*)(obj);
	struct mm_gl_udp* gl_udp = (struct mm_gl_udp*)(udp->callback.obj);
	// push to queue thread.
	mm_sockaddr_storage_init(&remote);
	mm_packet_init(&pack);
	mm_packet_head_base_zero(&pack);
	pack.hbuff.length = MM_MSG_COMM_HEAD_SIZE;
	pack.bbuff.length = 0;
	mm_packet_shadow_alloc(&pack);
	pack.phead.mid = udp_mid_nready;
    mm_packet_head_base_encode(&pack);
	__static_gl_udp_net_udp_handle_callback(obj,gl_udp->u,&pack,&remote);
	mm_packet_shadow_free(&pack);
	mm_packet_destroy(&pack);
	mm_sockaddr_storage_destroy(&remote);
	// net state signal.
	mm_gl_udp_state_signal(gl_udp);
}
static void __static_gl_udp_net_udp_finish_callback(void* obj)
{
	struct sockaddr_storage remote;
	struct mm_packet pack;
	struct mm_udp* udp = (struct mm_udp*)(obj);
	struct mm_gl_udp* gl_udp = (struct mm_gl_udp*)(udp->callback.obj);
	// push to queue thread.
	mm_sockaddr_storage_init(&remote);
	mm_packet_init(&pack);
	mm_packet_head_base_zero(&pack);
	pack.hbuff.length = MM_MSG_COMM_HEAD_SIZE;
	pack.bbuff.length = 0;
	mm_packet_shadow_alloc(&pack);
	pack.phead.mid = udp_mid_finish;
    mm_packet_head_base_encode(&pack);
	__static_gl_udp_net_udp_handle_callback(obj,gl_udp->u,&pack,&remote);
	mm_packet_shadow_free(&pack);
	mm_packet_destroy(&pack);
	mm_sockaddr_storage_destroy(&remote);
	// net state signal.
	mm_gl_udp_state_signal(gl_udp);
}
static int __static_gl_udp_flush_signal_task_factor(void* obj)
{
	int code = -1;
	struct mm_signal_task* signal_task = (struct mm_signal_task*)(obj);
	struct mm_gl_udp* gl_udp = (struct mm_gl_udp*)(signal_task->callback.obj);
	struct mm_udp* udp = &gl_udp->net_udp.udp;
	size_t buff_len = 0;
	mm_udp_lock(udp);
	buff_len = mm_streambuf_size(&udp->buff_send);
	mm_udp_unlock(udp);
	code = ( 0 == buff_len ? 0 : -1 );
	return code;
}
static int __static_gl_udp_flush_signal_task_handle(void* obj)
{
	int code = -1;
	struct mm_signal_task* signal_task = (struct mm_signal_task*)(obj);
	struct mm_gl_udp* gl_udp = (struct mm_gl_udp*)(signal_task->callback.obj);
	struct mm_udp* udp = &gl_udp->net_udp.udp;
	int real_len = 0;
	// check the state.
	if (0 == mm_gl_udp_finally_state(gl_udp))
	{
		// handle send data by flush send buffer.
		// 0 <  rt,means rt buffer is send,we must gbump rt size.
		// 0 == rt,means the send buffer can be full.
		// 0 >  rt,means the send process can be failure.
		mm_udp_lock(udp);
		real_len = mm_udp_flush_send(udp);
		mm_udp_unlock(udp);
		if ( 0 > real_len )
		{
			// the socket is broken.
			// but the send thread can not broken.
			// it is wait the reconnect and send.
			code = -1;
		}
		else
		{
			// the finally state is regular.
			// if 0 == real_len.the state is broken or the socket send buffer is full.
			// just think it is regular.
			code = 0;
		}
	}
	else
	{
		// the finally state is broken.
		// wake the state check process.
		mm_gl_udp_state_signal(gl_udp);
		code = -1;
	}
	return code;
}
static int __static_gl_udp_state_signal_task_factor(void* obj)
{
	int code = -1;
	struct mm_signal_task* signal_task = (struct mm_signal_task*)(obj);
	struct mm_gl_udp* gl_udp = (struct mm_gl_udp*)(signal_task->callback.obj);
	struct mm_udp* udp = &gl_udp->net_udp.udp;
	struct mm_addr* addr = &udp->addr;
	code = (
		0 == mm_sockaddr_storage_compare(&addr->ss_native,&gl_udp->ss_native) && 
		0 == mm_sockaddr_storage_compare(&addr->ss_remote,&gl_udp->ss_remote) && 
		0 == mm_gl_udp_finally_state(gl_udp) ) ? 0 : -1;
	return code;
}
static int __static_gl_udp_state_signal_task_handle(void* obj)
{
	int code = -1;
	struct mm_signal_task* signal_task = (struct mm_signal_task*)(obj);
	struct mm_gl_udp* gl_udp = (struct mm_gl_udp*)(signal_task->callback.obj);
	if (gl_udp_check_activate == gl_udp->state_check_flag)
	{
		struct mm_net_udp* net_udp = &gl_udp->net_udp;
		struct mm_udp* udp = &gl_udp->net_udp.udp;
		struct mm_addr* addr = &udp->addr;
		if ( 
			0 != mm_sockaddr_storage_compare(&addr->ss_native,&gl_udp->ss_native) || 
			0 != mm_sockaddr_storage_compare(&addr->ss_remote,&gl_udp->ss_remote) )
		{
			// if target address is not the real address,we disconnect current socket.
			mm_gl_udp_lock(gl_udp);
			// first disconnect current socket.
			mm_net_udp_detach_socket(net_udp);
			// assign the target address.
			mm_addr_assign_native_storage(addr,&gl_udp->ss_native);
			mm_addr_assign_remote_storage(addr,&gl_udp->ss_remote);
			mm_gl_udp_unlock(gl_udp);
		}
		if ( 
			0 == mm_sockaddr_storage_compare_address(&addr->ss_native,MM_ADDR_DEFAULT_NODE,MM_ADDR_DEFAULT_PORT) && 
			0 == mm_sockaddr_storage_compare_address(&addr->ss_remote,MM_ADDR_DEFAULT_NODE,MM_ADDR_DEFAULT_PORT)) 
		{
			// if the native and remote is empty.fire the broken event immediately.
			mm_gl_udp_lock(gl_udp);
			mm_net_udp_apply_broken(net_udp);
			mm_gl_udp_unlock(gl_udp);
		}
		else
		{
			mm_gl_udp_lock(gl_udp);
			mm_gl_udp_check(gl_udp);
			mm_gl_udp_unlock(gl_udp);
		}
	}
	code = ( 0 == mm_gl_udp_finally_state(gl_udp) ) ? 0 : -1;
	return code;
}
