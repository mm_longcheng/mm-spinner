#ifndef __mm_mq_udp_h__
#define __mm_mq_udp_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_time.h"
#include "core/mm_thread.h"

#include "net/mm_packet.h"
#include "net/mm_net_udp.h"

#include "container/mm_lock_queue.h"
#include "container/mm_holder32.h"

// max poper number at one loop.the default main thread timer frequency is 1/20 = 50 ms.
// 2ms one message handle is suitable 50/2 = 25.
#define MM_MQ_UDP_MAX_POPER_NUMBER 25

//////////////////////////////////////////////////////////////////////////
struct mm_mq_udp_data
{
	void* obj;// weak ref for udp handle callback.it is mm_udp.
	struct mm_packet pack;
	struct sockaddr_storage ss_remote;// remote udp end point address.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_udp_data_init(struct mm_mq_udp_data* p);
MM_EXPORT_DLL void mm_mq_udp_data_destroy(struct mm_mq_udp_data* p);
//////////////////////////////////////////////////////////////////////////
typedef void (*mm_mq_udp_handle)(void* obj, void* u, struct mm_packet* pack, struct sockaddr_storage* remote);
struct mm_mq_udp_callback
{
	mm_mq_udp_handle handle;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_mq_udp_callback_init(struct mm_mq_udp_callback* p);
MM_EXPORT_DLL void mm_mq_udp_callback_destroy(struct mm_mq_udp_callback* p);
//////////////////////////////////////////////////////////////////////////
// message queue for udp.
struct mm_mq_udp
{
	struct mm_lock_queue lock_queue;
	struct mm_holder_u32_vpt holder;// rb tree for msg_id <--> callback.
	struct mm_mq_udp_callback callback;// value ref. queue udp callback.
	mm_atomic_t holder_locker;
	mm_atomic_t locker;
	size_t max_pop;// max pop to make sure current not wait long times.default is MM_MQ_UDP_MAX_POPER_NUMBER. 
	void* u;// user data.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_udp_init(struct mm_mq_udp* p);
MM_EXPORT_DLL void mm_mq_udp_destroy(struct mm_mq_udp* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_udp_lock(struct mm_mq_udp* p);
MM_EXPORT_DLL void mm_mq_udp_unlock(struct mm_mq_udp* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_udp_assign_callback(struct mm_mq_udp* p,mm_uint32_t id,net_udp_handle callback);
MM_EXPORT_DLL void mm_mq_udp_assign_queue_udp_callback(struct mm_mq_udp* p,struct mm_mq_udp_callback* queue_udp_callback);
MM_EXPORT_DLL void mm_mq_udp_assign_max_poper_number(struct mm_mq_udp* p,size_t max_pop);
// assign context handle.
MM_EXPORT_DLL void mm_mq_udp_assign_context(struct mm_mq_udp* p,void* u);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_udp_clear_callback_holder(struct mm_mq_udp* p);
//////////////////////////////////////////////////////////////////////////
// you can interrupt block state.but will make the size error.
// other way is push a NULL.
MM_EXPORT_DLL void mm_mq_udp_cond_not_null(struct mm_mq_udp* p);
MM_EXPORT_DLL void mm_mq_udp_cond_not_full(struct mm_mq_udp* p);
//////////////////////////////////////////////////////////////////////////
// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_DLL void mm_mq_udp_thread_handle(struct mm_mq_udp* p);
// delete all not handle pack at queue.
MM_EXPORT_DLL void mm_mq_udp_dispose(struct mm_mq_udp* p);
// push a pack and context tp queue.
MM_EXPORT_DLL void mm_mq_udp_push(struct mm_mq_udp* p, void* obj, struct mm_packet* pack, struct sockaddr_storage* remote);
// pop and trigger handle.
MM_EXPORT_DLL void mm_mq_udp_pop(struct mm_mq_udp* p);
//////////////////////////////////////////////////////////////////////////

#include "core/mm_suffix.h"

#endif//__mm_mq_udp_h__