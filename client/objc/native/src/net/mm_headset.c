#include "net/mm_headset.h"
#include "core/mm_logger.h"
#include "core/mm_string.h"
#include "net/mm_buffer_packet.h"
//////////////////////////////////////////////////////////////////////////
static void* __static_mic_udp_poll_thread(void* arg);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mic_init(struct mm_mic* p)
{
	mm_udp_init(&p->udp);
	mm_spin_init(&p->locker,NULL);
	p->udp_state = headset_state_closed;
	p->state = ts_closed;
}
MM_EXPORT_DLL void mm_mic_destroy(struct mm_mic* p)
{
	mm_mic_clear(p);
	//
	mm_udp_destroy(&p->udp);
	mm_spin_destroy(&p->locker);
	p->udp_state = headset_state_closed;
	p->state = ts_closed;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mic_lock(struct mm_mic* p)
{
	mm_spin_lock(&p->locker);
}
MM_EXPORT_DLL void mm_mic_unlock(struct mm_mic* p)
{
	mm_spin_unlock(&p->locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mic_assign_udp_shared(struct mm_mic* p,struct mm_udp* udp)
{
	mm_sockaddr_storage_copy(&p->udp.addr.ss_native,&udp->addr.ss_native);
	mm_sockaddr_storage_copy(&p->udp.addr.ss_remote,&udp->addr.ss_remote);
	p->udp.addr.socket = udp->addr.socket;
	p->udp.addr.u = udp->addr.u;
	p->udp.addr = udp->addr;
	p->udp.callback = udp->callback;
	p->udp.unique_id = udp->unique_id;
}
MM_EXPORT_DLL void mm_mic_update_udp_state(struct mm_mic* p,int state)
{
	p->udp_state = state;
}
MM_EXPORT_DLL void mm_mic_clear(struct mm_mic* p)
{
	p->udp.addr.socket = MM_INVALID_SOCKET;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mic_start(struct mm_mic* p)
{
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	pthread_create(&p->poll_thread, NULL, &__static_mic_udp_poll_thread, p);
}
MM_EXPORT_DLL void mm_mic_interrupt(struct mm_mic* p)
{
	p->state = ts_closed;
}
MM_EXPORT_DLL void mm_mic_shutdown(struct mm_mic* p)
{
	p->state = ts_finish;
}
MM_EXPORT_DLL void mm_mic_join(struct mm_mic* p)
{
	pthread_join(p->poll_thread, NULL);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mic_poll_wait(struct mm_mic* p)
{
	mm_uint8_t buffer[MM_HEADSET_PAGE_SIZE];
	while( ts_motion == p->state && headset_state_broken != p->udp_state)
	{
		mm_udp_handle_recv(&p->udp,buffer,0,MM_HEADSET_PAGE_SIZE);
	}
}
//////////////////////////////////////////////////////////////////////////
// mic streambuf recv send reset.
MM_EXPORT_DLL void mm_mic_streambuf_reset(struct mm_mic* p)
{
	mm_streambuf_reset(&p->udp.buff_recv);
	mm_streambuf_reset(&p->udp.buff_send);
}
//////////////////////////////////////////////////////////////////////////
static void* __static_mic_udp_poll_thread(void* arg)
{
	struct mm_mic* p = (struct mm_mic*)(arg);
	mm_mic_poll_wait(p);
	return NULL;
}
//////////////////////////////////////////////////////////////////////////
static void __static_headset_udp_handle_callback(void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote);
static void __static_headset_udp_broken_callback(void* obj);
//////////////////////////////////////////////////////////////////////////
static void __static_headset_udp_handle_packet_callback(void* obj,struct mm_packet* pack, struct sockaddr_storage* remote);
//////////////////////////////////////////////////////////////////////////
static void __static_headset_handle( void* obj, void* u, struct mm_packet* pack, struct sockaddr_storage* remote )
{

}
static void __static_headset_broken( void* obj )
{

}
static void __static_headset_nready( void* obj )
{

}
static void __static_headset_finish( void* obj )
{

}
MM_EXPORT_DLL void mm_headset_callback_init(struct mm_headset_callback* p)
{
	p->handle = &__static_headset_handle;
	p->broken = &__static_headset_broken;
	p->nready = &__static_headset_nready;
	p->finish = &__static_headset_finish;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_headset_callback_destroy(struct mm_headset_callback* p)
{
	p->handle = &__static_headset_handle;
	p->broken = &__static_headset_broken;
	p->nready = &__static_headset_nready;
	p->finish = &__static_headset_finish;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_headset_init(struct mm_headset* p)
{
	struct mm_udp_callback udp_callback;
	//
	mm_udp_init(&p->udp);
	mm_headset_callback_init(&p->callback);
	mm_crypto_callback_init(&p->crypto_callback);
	mm_holder_u32_vpt_init(&p->holder);
	pthread_mutex_init(&p->signal_mutex,NULL);
	pthread_cond_init(&p->signal_cond,NULL);
	mm_spin_init(&p->arrays_locker,NULL);
	mm_spin_init(&p->holder_locker,NULL);
	mm_spin_init(&p->locker,NULL);
	p->length = 0;
	p->arrays = NULL;
	p->nonblock_timeout = MM_HEADSET_NONBLOCK_TIMEOUT;
	p->trytimes = MM_HEADSET_TRYTIME;
	p->state = ts_closed;
	p->udp_state = headset_state_closed;
	p->u = NULL;
	//
	udp_callback.handle = &__static_headset_udp_handle_callback;
	udp_callback.broken = &__static_headset_udp_broken_callback;
	udp_callback.obj = p;
	mm_udp_assign_callback(&p->udp,&udp_callback);
}
MM_EXPORT_DLL void mm_headset_destroy(struct mm_headset* p)
{
	mm_headset_clear_callback_holder(p);
	mm_headset_clear(p);
	//
	mm_udp_destroy(&p->udp);
	mm_headset_callback_destroy(&p->callback);
	mm_crypto_callback_destroy(&p->crypto_callback);
	mm_holder_u32_vpt_destroy(&p->holder);
	pthread_mutex_destroy(&p->signal_mutex);
	pthread_cond_destroy(&p->signal_cond);
	mm_spin_destroy(&p->arrays_locker);
	mm_spin_destroy(&p->holder_locker);
	mm_spin_destroy(&p->locker);
	p->length = 0;
	p->arrays = NULL;
	p->nonblock_timeout = MM_HEADSET_NONBLOCK_TIMEOUT;
	p->trytimes = MM_HEADSET_TRYTIME;
	p->state = ts_closed;
	p->udp_state = headset_state_closed;
	p->u = NULL;
}
//////////////////////////////////////////////////////////////////////////
// lock.
MM_EXPORT_DLL void mm_headset_lock(struct mm_headset* p)
{
	mm_spin_lock(&p->locker);
}
// unlock.
MM_EXPORT_DLL void mm_headset_unlock(struct mm_headset* p)
{
	mm_spin_unlock(&p->locker);
}
//////////////////////////////////////////////////////////////////////////
// assign native address but not connect.
MM_EXPORT_DLL void mm_headset_assign_native(struct mm_headset* p,const char* node,mm_ushort_t port)
{
	mm_uint32_t i = 0;
	mm_spin_lock(&p->arrays_locker);
	mm_udp_assign_native(&p->udp,node,port);
	for ( i = 0; i < p->length; ++i)
	{
		mm_mic_lock(p->arrays[i]);
		mm_udp_assign_native_storage(&p->arrays[i]->udp,&p->udp.addr.ss_native);
		mm_mic_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
}
// assign remote address but not connect.
MM_EXPORT_DLL void mm_headset_assign_remote(struct mm_headset* p,const char* node,mm_ushort_t port)
{
	mm_uint32_t i = 0;
	mm_spin_lock(&p->arrays_locker);
	mm_udp_assign_remote(&p->udp,node,port);
	for ( i = 0; i < p->length; ++i)
	{
		mm_mic_lock(p->arrays[i]);
		mm_udp_assign_remote_storage(&p->arrays[i]->udp,&p->udp.addr.ss_remote);
		mm_mic_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
}
// assign protocol_size.
MM_EXPORT_DLL void mm_headset_set_length(struct mm_headset* p, mm_uint32_t length)
{
	mm_spin_lock(&p->arrays_locker);
	if (length < p->length)
	{
		mm_uint32_t i = 0;
		struct mm_mic* e = NULL;
		for ( i = length; i < p->length; ++i)
		{
			e = p->arrays[i];
			mm_mic_lock(e);
			p->arrays[i] = NULL;
			mm_mic_unlock(e);
			mm_mic_destroy(e);
			mm_free(e);
		}
		p->arrays = (struct mm_mic**)mm_realloc(p->arrays,sizeof(struct mm_mic*) * length);
		p->length = length;
	}
	else if (length > p->length)
	{
		mm_uint32_t i = 0;
		p->arrays = (struct mm_mic**)mm_realloc(p->arrays,sizeof(struct mm_mic*) * length);
		for ( i = p->length; i < length; ++i)
		{
			p->arrays[i] = (struct mm_mic*)mm_malloc(sizeof(struct mm_mic));
			mm_mic_init(p->arrays[i]);
			mm_mic_assign_udp_shared(p->arrays[i],&p->udp);
			p->arrays[i]->state = p->state;
		}
		p->length = length;
	}
	mm_spin_unlock(&p->arrays_locker);
}
MM_EXPORT_DLL mm_uint32_t mm_headset_get_length(struct mm_headset* p)
{
	return p->length;
}
// assign context handle.
MM_EXPORT_DLL void mm_headset_assign_context(struct mm_headset* p,void* u)
{
	p->u = u;
}
// assign nonblock_timeout.
MM_EXPORT_DLL void mm_headset_assign_nonblock_timeout(struct mm_headset* p,mm_msec_t nonblock_timeout)
{
	p->nonblock_timeout = nonblock_timeout;
}
// assign trytimes.
MM_EXPORT_DLL void mm_headset_assign_trytimes(struct mm_headset* p,mm_uint32_t trytimes)
{
	p->trytimes = trytimes;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_headset_assign_callback(struct mm_headset* p,mm_uint32_t id,headset_handle callback)
{
	mm_spin_lock(&p->holder_locker);
	mm_holder_u32_vpt_set(&p->holder,id,(void*)callback);
	mm_spin_unlock(&p->holder_locker);
}
MM_EXPORT_DLL void mm_headset_clear_callback_holder(struct mm_headset* p)
{
	mm_spin_lock(&p->holder_locker);
	mm_holder_u32_vpt_clear(&p->holder);
	mm_spin_unlock(&p->holder_locker);
}
//////////////////////////////////////////////////////////////////////////
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_headset_assign_crypto_callback(struct mm_headset* p,struct mm_crypto_callback* crypto_callback)
{
	p->crypto_callback = *crypto_callback;
}
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_headset_assign_headset_callback(struct mm_headset* p,struct mm_headset_callback* headset_callback)
{
	assert(NULL != headset_callback && "you can not assign null headset_callback.");
	p->callback = *headset_callback;
}
// context for udp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_DLL void mm_headset_set_addr_context(struct mm_headset* p, void* u)
{
	mm_udp_set_context(&p->udp,u);
}
MM_EXPORT_DLL void* mm_headset_get_addr_context(struct mm_headset* p)
{
	return mm_udp_get_context(&p->udp);
}
//////////////////////////////////////////////////////////////////////////
// headset crypto encrypt buffer by udp.
MM_EXPORT_DLL void mm_headset_crypto_encrypt(struct mm_headset* p,struct mm_udp* udp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length)
{
	(*(p->crypto_callback.encrypt))(p,udp,buffer,offset,buffer,offset,length);
}
// headset crypto decrypt buffer by udp.
MM_EXPORT_DLL void mm_headset_crypto_decrypt(struct mm_headset* p,struct mm_udp* udp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length)
{
	(*(p->crypto_callback.decrypt))(p,udp,buffer,offset,buffer,offset,length);
}
//////////////////////////////////////////////////////////////////////////
// fopen.
MM_EXPORT_DLL void mm_headset_fopen_socket(struct mm_headset* p)
{
	mm_uint32_t i = 0;
	mm_spin_lock(&p->arrays_locker);
	mm_udp_fopen_socket(&p->udp);
	for ( i = 0; i < p->length; ++i)
	{
		mm_mic_lock(p->arrays[i]);
		p->arrays[i]->udp.addr.socket = p->udp.addr.socket;
		mm_mic_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
}
// bind.
MM_EXPORT_DLL void mm_headset_bind(struct mm_headset* p)
{
	char link_name[MM_LINK_NAME_LENGTH];
	struct mm_addr* addr = &p->udp.addr;
	struct mm_logger* g_logger = mm_logger_instance();
	//
	mm_addr_string(addr,link_name);
	//
	if ( MM_INVALID_SOCKET != addr->socket )
	{
		mm_uint32_t _try_times = 0;
		int rt = -1;
		p->udp_state = headset_state_motion;
		{
			mm_uint32_t i = 0;
			mm_spin_lock(&p->arrays_locker);
			for ( i = 0; i < p->length; ++i)
			{
				mm_mic_lock(p->arrays[i]);
				mm_mic_update_udp_state(p->arrays[i],p->udp_state);
				mm_mic_unlock(p->arrays[i]);
			}
			mm_spin_unlock(&p->arrays_locker);
		}
		// set socket to blocking.
		mm_addr_set_block(addr, MM_BLOCKING);
		do 
		{
			_try_times ++;
			rt = mm_addr_bind(addr);
			if (0 == rt)
			{
				// bind immediately.
				break;
			}
			if ( ts_finish == p->state )
			{
				break;
			}
			else
			{
				mm_logger_log_E(g_logger,"%s %d bind %s failure.try times:%d",__FUNCTION__,__LINE__,link_name,_try_times);
				mm_msleep(MM_HEADSET_TRY_MSLEEP_TIME);
			}
		} while ( 0 != rt && _try_times < p->trytimes );
		// set socket to blocking.
		mm_addr_set_block(addr, MM_BLOCKING);
		if ( 0 != rt )
		{
			mm_err_t errcode = mm_errno;
			mm_logger_log_E(g_logger,"%s %d bind %s failure.try times:%d",__FUNCTION__,__LINE__,link_name,_try_times);
			mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
			mm_headset_apply_broken(p);
			mm_addr_close_socket(addr);
		}
		else
		{
			mm_headset_apply_finish(p);
			mm_logger_log_I(g_logger,"%s %d %s success.try times:%d",__FUNCTION__,__LINE__,link_name,_try_times);
		}
	}
	else
	{
		mm_logger_log_E(g_logger,"%s %d bind target fd is invalid.",__FUNCTION__,__LINE__);
	}
}
// close socket.mm_headset_join before call this.
MM_EXPORT_DLL void mm_headset_close_socket(struct mm_headset* p)
{
	mm_udp_close_socket(&p->udp);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_headset_clear(struct mm_headset* p)
{
	mm_uint32_t i = 0;
	mm_spin_lock(&p->arrays_locker);
	for ( i = 0; i < p->length; ++i)
	{
		struct mm_mic* e = p->arrays[i];
		mm_mic_lock(e);
		p->arrays[i] = NULL;
		mm_mic_unlock(e);
		mm_mic_destroy(e);
		mm_free(e);
	}
	mm_free(p->arrays);
	p->arrays = NULL;
	p->length = 0;
	mm_spin_unlock(&p->arrays_locker);
}
MM_EXPORT_DLL void mm_headset_apply_broken(struct mm_headset* p)
{
	mm_uint32_t i = 0;
	mm_spin_lock(&p->arrays_locker);
	p->udp_state = headset_state_closed;
	mm_udp_streambuf_reset(&p->udp);
	for ( i = 0; i < p->length; ++i)
	{
		mm_mic_lock(p->arrays[i]);
		mm_mic_streambuf_reset(p->arrays[i]);
		mm_mic_update_udp_state(p->arrays[i],p->udp_state);
		mm_mic_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
	(*p->callback.broken)(&p->udp);
}
MM_EXPORT_DLL void mm_headset_apply_finish(struct mm_headset* p)
{
	mm_uint32_t i = 0;
	mm_spin_lock(&p->arrays_locker);
	p->udp_state = headset_state_finish;
	mm_udp_streambuf_reset(&p->udp);
	for ( i = 0; i < p->length; ++i)
	{
		mm_mic_lock(p->arrays[i]);
		mm_mic_streambuf_reset(p->arrays[i]);
		mm_mic_update_udp_state(p->arrays[i],p->udp_state);
		mm_mic_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
	(*p->callback.finish)(&p->udp);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_headset_start(struct mm_headset* p)
{
	mm_uint32_t i = 0;
	mm_spin_lock(&p->arrays_locker);
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	for ( i = 0; i < p->length; ++i)
	{
		mm_mic_lock(p->arrays[i]);
		mm_mic_start(p->arrays[i]);
		mm_mic_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
}
MM_EXPORT_DLL void mm_headset_interrupt(struct mm_headset* p)
{
	mm_uint32_t i = 0;
	mm_spin_lock(&p->arrays_locker);
	p->state = ts_closed;
	for ( i = 0; i < p->length; ++i)
	{
		mm_mic_lock(p->arrays[i]);
		mm_mic_interrupt(p->arrays[i]);
		mm_mic_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
	//
	pthread_mutex_lock(&p->signal_mutex);
	pthread_cond_signal(&p->signal_cond);
	pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mm_headset_shutdown(struct mm_headset* p)
{
	mm_uint32_t i = 0;
	//////////////////////////////////////////////////////////////////////////
	mm_addr_shutdown_socket(&p->udp.addr,MM_BOTH_SHUTDOWN);
	mm_addr_close_socket(&p->udp.addr);
	//////////////////////////////////////////////////////////////////////////
	mm_spin_lock(&p->arrays_locker);
	p->state = ts_finish;
	for ( i = 0; i < p->length; ++i)
	{
		mm_mic_lock(p->arrays[i]);
		mm_mic_shutdown(p->arrays[i]);
		mm_mic_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
	//
	pthread_mutex_lock(&p->signal_mutex);
	pthread_cond_signal(&p->signal_cond);
	pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mm_headset_join(struct mm_headset* p)
{
	mm_uint32_t i = 0;
	if (ts_motion == p->state)
	{
		// we can not lock or join until cond wait all thread is shutdown.
		pthread_mutex_lock(&p->signal_mutex);
		pthread_cond_wait(&p->signal_cond,&p->signal_mutex);
		pthread_mutex_unlock(&p->signal_mutex);
	}
	//
	mm_spin_lock(&p->arrays_locker);
	for ( i = 0; i < p->length; ++i)
	{
		mm_mic_lock(p->arrays[i]);
		mm_mic_join(p->arrays[i]);
		mm_mic_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
}
//////////////////////////////////////////////////////////////////////////
static void __static_headset_udp_handle_callback(void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote)
{
	struct mm_udp* udp = (struct mm_udp*)(obj);
	struct mm_headset* headset = (struct mm_headset*)(udp->callback.obj);
	mm_headset_crypto_decrypt(headset,udp,buffer,offset,length);
	mm_buffer_packet_handle_udp(buffer, offset, length, &__static_headset_udp_handle_packet_callback, udp, remote);
}
static void __static_headset_udp_broken_callback(void* obj)
{
	struct mm_udp* udp = (struct mm_udp*)(obj);
	struct mm_headset* headset = (struct mm_headset*)(udp->callback.obj);
	assert(NULL != headset->callback.broken && "headset->broken is a null.");
	mm_headset_apply_broken(headset);
}
//////////////////////////////////////////////////////////////////////////
static void __static_headset_udp_handle_packet_callback(void* obj,struct mm_packet* pack, struct sockaddr_storage* remote)
{
	struct mm_udp* udp = (struct mm_udp*)(obj);
	struct mm_headset* headset = (struct mm_headset*)(udp->callback.obj);
	net_udp_handle handle = NULL;
	mm_spin_lock(&headset->holder_locker);
	handle = (net_udp_handle)mm_holder_u32_vpt_get(&headset->holder,pack->phead.mid);
	mm_spin_unlock(&headset->holder_locker);
	if (NULL != handle)
	{
		// fire the handle event.
		(*(handle))(udp,headset->u,pack,remote);
	}
	else
	{
		assert(NULL != headset->callback.handle && "headset->handle is a null.");
		(*(headset->callback.handle))(udp,headset->u,pack,remote);
	}
}
//////////////////////////////////////////////////////////////////////////
