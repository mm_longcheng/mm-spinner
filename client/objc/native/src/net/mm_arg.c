#include "net/mm_arg.h"
#include "core/mm_logger.h"
#include "core/mm_string.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_arg_init(struct mm_arg* p)
{
	p->argc = 0;
	p->argv = NULL;
}
MM_EXPORT_DLL void mm_arg_destroy(struct mm_arg* p)
{
	p->argc = 0;
	p->argv = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_arg_print(struct mm_arg* p)
{
	int i = 0;
	for ( i = 0; i < p->argc; ++i)
	{
		mm_printf("%s ",p->argv[i]);
	}
	mm_printf("\n");
}
MM_EXPORT_DLL void mm_arg_logger(struct mm_arg* p)
{
	int i = 0;
	struct mm_string v;
	struct mm_logger* g_logger = mm_logger_instance();
	mm_string_init(&v);
	for ( i = 0; i < p->argc; ++i)
	{
		mm_string_puts(&v,p->argv[i]);
		mm_string_putc(&v,' ');
	}
	mm_logger_log_I(g_logger,"arg: %s",v.s);
	mm_string_destroy(&v);
}
//////////////////////////////////////////////////////////////////////////