#include "net/mm_crypto.h"
#include "core/mm_alloc.h"
#include "core/mm_logger.h"
//
//////////////////////////////////////////////////////////////////////////
static void __static_crypto_encrypt(void* p,void* e,mm_uint8_t* buffer_i,mm_uint32_t offset_i,mm_uint8_t* buffer_o,mm_uint32_t offset_o,mm_uint32_t length)
{

}
static void __static_crypto_decrypt(void* p,void* e,mm_uint8_t* buffer_i,mm_uint32_t offset_i,mm_uint8_t* buffer_o,mm_uint32_t offset_o,mm_uint32_t length)
{

}
MM_EXPORT_DLL void mm_crypto_callback_init(struct mm_crypto_callback* p)
{
	p->encrypt = &__static_crypto_encrypt;
	p->decrypt = &__static_crypto_decrypt;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_crypto_callback_destroy(struct mm_crypto_callback* p)
{
	p->encrypt = &__static_crypto_encrypt;
	p->decrypt = &__static_crypto_decrypt;
	p->obj = NULL;
}
