#include "net/mm_streambuf_packet.h"
#include "core/mm_logger.h"
#include "core/mm_errno.h"
#include "core/mm_alloc.h"
/////////////////////////////////////////////////////////////
// append pack data to streambuf.
MM_EXPORT_DLL mm_uint32_t mm_streambuf_packet_append(struct mm_streambuf* p, struct mm_packet* pack)
{
	mm_uint32_t msg_size = sizeof(mm_uint32_t) + pack->hbuff.length + pack->bbuff.length;
	mm_uint32_t head_data = (mm_uint32_t) (pack->hbuff.length << MM_MSG_SIZE_BIT_NUM) | (mm_uint32_t) msg_size;
	// if the idle put size is lack, we try remove the get buffer.
	mm_streambuf_aligned_memory(p,msg_size);
	//
	mm_streambuf_sputn(p,(mm_uint8_t*)&head_data,0,sizeof(mm_uint32_t));
	//
	mm_streambuf_sputn(p,pack->hbuff.buffer,pack->hbuff.offset,pack->hbuff.length);
	mm_streambuf_sputn(p,pack->bbuff.buffer,pack->bbuff.offset,pack->bbuff.length);
	return msg_size;
}
// overdraft pack.hbuff pack.bbuff for encode.
// will pbump streambuf.
MM_EXPORT_DLL mm_uint32_t mm_streambuf_packet_overdraft(struct mm_streambuf* p, struct mm_packet* pack)
{
	mm_uint32_t msg_size = sizeof(mm_uint32_t) + pack->hbuff.length + pack->bbuff.length;
	mm_uint32_t head_data = (mm_uint32_t) (pack->hbuff.length << MM_MSG_SIZE_BIT_NUM) | (mm_uint32_t) msg_size;
	// if the idle put size is lack, we try remove the get buffer.
	mm_streambuf_aligned_memory(p,msg_size);
	//
	pack->hbuff.buffer = p->buff;
	pack->hbuff.offset = (mm_uint32_t)(p->pptr + sizeof(mm_uint32_t));
	pack->bbuff.buffer = p->buff;
	pack->bbuff.offset = pack->hbuff.offset + pack->hbuff.length;
	mm_memcpy(p->buff + p->pptr,(void*)&head_data,sizeof(mm_uint32_t));
	mm_streambuf_pbump(p,msg_size);
	return msg_size;
}
MM_EXPORT_DLL void mm_streambuf_packet_assign_sid(struct mm_streambuf* p,mm_uint64_t sid)
{
	// the first mm_uint32_t is hsize|bsize.
	struct mm_packet* pack = (struct mm_packet*)( p->gptr + sizeof(mm_uint32_t) );
	pack->phead.sid = sid;
}
// if need quick transmit one transport recv data to another transport,call this.it is safe.
// assign pid quick,make sure the data must struct mm_packet format.
MM_EXPORT_DLL void mm_streambuf_packet_assign_pid(struct mm_streambuf* p,mm_uint32_t pid)
{
	// the first mm_uint32_t is hsize|bsize.
	struct mm_packet* pack = (struct mm_packet*)( p->gptr + sizeof(mm_uint32_t) );
	pack->phead.pid = pid;
}
// packet handle tcp buffer and length for streambuf handle separate packet.
MM_EXPORT_DLL void mm_streambuf_packet_handle_tcp(struct mm_streambuf* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, packet_handle_tcp handle, void* obj)
{
	struct mm_packet pack;
	mm_uint32_t head_data = 0;
	mm_uint16_t msg_size = 0;
	size_t buff_size = 0;
	assert(handle&&"handle callback is invalid.");
	// if the idle put size is lack, we try remove the get buffer.
	mm_streambuf_aligned_memory(p,length);
	// copy the recv buffer to streambuf.
	mm_streambuf_sputn(p,buffer,offset,length);
	do
	{
		buff_size = mm_streambuf_size(p);
		mm_memcpy(&head_data,p->buff + p->gptr,sizeof(mm_uint32_t));
		msg_size = (mm_uint16_t)(head_data & MM_MSG_HEAD_L_MASK);
		if (buff_size >= msg_size)
		{
			// here we just use the weak ref from streambuf.for data recv callback.
			pack.hbuff.length = head_data >> MM_MSG_SIZE_BIT_NUM;
			pack.hbuff.buffer = p->buff;
			pack.hbuff.offset = (mm_uint32_t)(p->gptr + sizeof(mm_uint32_t));
			//
			pack.bbuff.length = msg_size - sizeof(mm_uint32_t) - pack.hbuff.length;
			pack.bbuff.buffer = p->buff;
			pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
			//
			mm_packet_head_base_decode(&pack);
			//
			// fire the field buffer recv event.
			(*(handle))(obj,&pack);
			// no matter session_recv how much buffer reading,
			// we removeread fixed size to make sure the socket buffer sequence is correct.
			mm_streambuf_gbump(p,msg_size);
		}
	} while ( buff_size > msg_size && msg_size >= MM_MSG_BASE_HEAD_SIZE );
}
// packet handle udp buffer and length for streambuf handle separate packet.
MM_EXPORT_DLL void mm_streambuf_packet_handle_udp(struct mm_streambuf* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, packet_handle_udp handle, void* obj, struct sockaddr_storage* remote)
{
	struct mm_packet pack;
	mm_uint32_t head_data = 0;
	mm_uint16_t msg_size = 0;
	size_t buff_size = 0;
	assert(handle&&"handle callback is invalid.");
	// if the idle put size is lack, we try remove the get buffer.
	mm_streambuf_aligned_memory(p,length);
	// copy the recv buffer to streambuf.
	mm_streambuf_sputn(p,buffer,offset,length);
	do
	{
		buff_size = mm_streambuf_size(p);
		mm_memcpy(&head_data,p->buff + p->gptr,sizeof(mm_uint32_t));
		msg_size = (mm_uint16_t)(head_data & MM_MSG_HEAD_L_MASK);
		if (buff_size >= msg_size)
		{
			// here we just use the weak ref from streambuf.for data recv callback.
			pack.hbuff.length = head_data >> MM_MSG_SIZE_BIT_NUM;
			pack.hbuff.buffer = p->buff;
			pack.hbuff.offset = (mm_uint32_t)(p->gptr + sizeof(mm_uint32_t));
			//
			pack.bbuff.length = msg_size - sizeof(mm_uint32_t) - pack.hbuff.length;
			pack.bbuff.buffer = p->buff;
			pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
			//
			mm_packet_head_base_decode(&pack);
			//
			// fire the field buffer recv event.
			(*(handle))(obj,&pack,remote);
			// no matter session_recv how much buffer reading,
			// we removeread fixed size to make sure the socket buffer sequence is correct.
			mm_streambuf_gbump(p,msg_size);
		}
	} while ( buff_size > msg_size && msg_size >= MM_MSG_BASE_HEAD_SIZE );
}