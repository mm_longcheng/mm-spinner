#include "net/mm_addr.h"
#include "core/mm_alloc.h"
#include "core/mm_string.h"
#include "core/mm_spinlock.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_addr_init(struct mm_addr* p)
{
	mm_sockaddr_storage_init(&p->ss_native);
	mm_sockaddr_storage_init(&p->ss_remote);
	p->socket = MM_INVALID_SOCKET;
	mm_spin_init(&p->locker,NULL);
	p->u = NULL;
}

MM_EXPORT_DLL void mm_addr_destroy(struct mm_addr* p)
{
	mm_addr_close_socket(p);
	//
	mm_sockaddr_storage_destroy(&p->ss_native);
	mm_sockaddr_storage_destroy(&p->ss_remote);
	p->socket = MM_INVALID_SOCKET;
	mm_spin_destroy(&p->locker);
	p->u = NULL;
}
MM_EXPORT_DLL void mm_addr_lock(struct mm_addr* p)
{
	mm_spin_lock(&p->locker);
}
MM_EXPORT_DLL void mm_addr_unlock(struct mm_addr* p)
{
	mm_spin_unlock(&p->locker);
}
// alloc socket fd.
MM_EXPORT_DLL void mm_addr_fopen_socket(struct mm_addr* p, int domain, int type, int protocol)
{
	mm_addr_close_socket(p);
	p->socket = mm_socket(domain, type, protocol);
}
// close socket fd.
MM_EXPORT_DLL void mm_addr_close_socket(struct mm_addr* p )
{
	if ( MM_INVALID_SOCKET != p->socket )
	{
		mm_close_socket(p->socket);
		p->socket = MM_INVALID_SOCKET;
	}
}
// close socket fd.not set socket_t to invalid -1.
// use to fire socket fd close event.
MM_EXPORT_DLL void mm_addr_close_socket_event(struct mm_addr* p)
{
	// we not need assert the fd is must valid.
	// assert( -1 != p->fd && "you must alloc socket fd first." );
	mm_close_socket(p->socket);
}
// shutdown socket.
MM_EXPORT_DLL void mm_addr_shutdown_socket(struct mm_addr* p, int opcode)
{
	if ( MM_INVALID_SOCKET != p->socket )
	{
		mm_shutdown_socket(p->socket, opcode);
	}
}
// assign addr native by ip port.
MM_EXPORT_DLL void mm_addr_assign_native(struct mm_addr* p, const char* node, mm_ushort_t port)
{
	mm_sockaddr_storage_assign(&p->ss_native, node, port);
	p->ss_remote.ss_family = p->ss_native.ss_family;
}
// assign addr remote by ip port.
MM_EXPORT_DLL void mm_addr_assign_remote(struct mm_addr* p, const char* node, mm_ushort_t port)
{
	mm_sockaddr_storage_assign(&p->ss_remote, node, port);
	p->ss_native.ss_family = p->ss_remote.ss_family;
}
// assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
MM_EXPORT_DLL void mm_addr_assign_native_storage(struct mm_addr* p, struct sockaddr_storage* ss_native)
{
	mm_sockaddr_storage_copy(&p->ss_native, ss_native);
	p->ss_remote.ss_family = p->ss_native.ss_family;
}
// assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
MM_EXPORT_DLL void mm_addr_assign_remote_storage(struct mm_addr* p, struct sockaddr_storage* ss_remote)
{
	mm_sockaddr_storage_copy(&p->ss_remote, ss_remote);
	p->ss_native.ss_family = p->ss_remote.ss_family;
}
// context for tcp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_DLL void mm_addr_set_context(struct mm_addr* p, void* u)
{
	p->u = u;
}
MM_EXPORT_DLL void* mm_addr_get_context(struct mm_addr* p)
{
	return p->u;
}
// default is blocking.1 means block 0 no block.
MM_EXPORT_DLL int mm_addr_set_block(struct mm_addr* p, int block)
{
	// we not need assert the fd is must valid.
	// assert( -1 != p->fd && "you must alloc socket fd first." );
	return MM_NONBLOCK == block ? mm_nonblocking(p->socket) : mm_blocking(p->socket);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL int mm_addr_connect(struct mm_addr* p)
{
	// we not need assert the fd is must valid.
	// assert( -1 != p->fd && "you must alloc socket fd first." );
	return (int)connect(p->socket,(struct sockaddr*)&p->ss_remote, mm_sockaddr_storage_length(&p->ss_remote));
}
// bind address.
MM_EXPORT_DLL int mm_addr_bind(struct mm_addr* p)
{
	// we not need assert the fd is must valid.
	// assert( -1 != p->fd && "you must alloc socket fd first." );
	return (int)bind(p->socket,(struct sockaddr*)&p->ss_native, mm_sockaddr_storage_length(&p->ss_native));
}
// listen address.
MM_EXPORT_DLL int mm_addr_listen(struct mm_addr* p, int backlog)
{
	// we not need assert the fd is must valid.
	// assert( -1 != p->fd && "you must alloc socket fd first." );
	return (int)listen(p->socket,backlog);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL int mm_addr_recv(struct mm_addr* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, int flags)
{
	// we not need assert the fd is must valid.
	// assert( -1 != p->fd && "you must alloc socket fd first." );
	return (int)recv(p->socket, (char*)(buffer + offset), (int)length, flags);
}
MM_EXPORT_DLL int mm_addr_send(struct mm_addr* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, int flags)
{
	// we not need assert the fd is must valid.
	// assert( -1 != p->fd && "you must alloc socket fd first." );
	return (int)send(p->socket, (char*)(buffer + offset), (int)length, flags);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL int mm_addr_send_dgram(struct mm_addr* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, int flags, struct sockaddr* remote_addr, socklen_t  remote_size)
{
	// we not need assert the fd is must valid.
	// assert( -1 != p->fd && "you must alloc socket fd first." );
	// int sendto(int s, void * msg, int len, unsigned int flags, const struct sockaddr * to, socklen_t tolen);
	return (int)sendto(  p->socket, (char*)(buffer + offset), (int)length, flags, remote_addr, remote_size);
}
MM_EXPORT_DLL int mm_addr_recv_dgram(struct mm_addr* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, int flags, struct sockaddr* remote_addr, socklen_t*  remote_size)
{
	// we not need assert the fd is must valid.
	// assert( -1 != p->fd && "you must alloc socket fd first." );
	// int recvfrom(int s, void *buf, int len, unsigned int flags, struct sockaddr *from,socklen_t *fromlen);
	return (int)recvfrom(p->socket, (char*)(buffer + offset), (int)length, flags, remote_addr, remote_size);
}
MM_EXPORT_DLL void mm_addr_string(struct mm_addr* p, char link_name[MM_LINK_NAME_LENGTH])
{
	mm_sockaddr_storage_native_remote_string(&p->ss_native,&p->ss_remote,p->socket,link_name);
}
