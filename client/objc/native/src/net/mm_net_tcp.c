#include "net/mm_net_tcp.h"
#include "net/mm_streambuf_packet.h"
#include "core/mm_alloc.h"
#include "core/mm_logger.h"
/////////////////////////////////////////////////////////////
static void __static_net_tcp_tcp_handle_callback(void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length);
static void __static_net_tcp_tcp_broken_callback(void* obj);

static void __static_net_tcp_nuclear_handle_recv(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length);
static void __static_net_tcp_nuclear_handle_send(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length);

static void __static_net_tcp_tcp_handle_packet_callback(void* obj,struct mm_packet* pack);
/////////////////////////////////////////////////////////////
static void __static_net_tcp_handle(void* obj, void* u, struct mm_packet* pack)
{

}
static void __static_net_tcp_broken(void* obj)
{

}
static void __static_net_tcp_nready(void* obj)
{

}
static void __static_net_tcp_finish(void* obj)
{

}
MM_EXPORT_DLL void mm_net_tcp_callback_init(struct mm_net_tcp_callback* p)
{
	p->handle = &__static_net_tcp_handle;
	p->broken = &__static_net_tcp_broken;
	p->nready = &__static_net_tcp_nready;
	p->finish = &__static_net_tcp_finish;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_net_tcp_callback_destroy(struct mm_net_tcp_callback* p)
{
	p->handle = &__static_net_tcp_handle;
	p->broken = &__static_net_tcp_broken;
	p->nready = &__static_net_tcp_nready;
	p->finish = &__static_net_tcp_finish;
	p->obj = NULL;
}
/////////////////////////////////////////////////////////////
static void __static_net_tcp_event_tcp_alloc( void* p, struct mm_tcp* tcp )
{

}
static void __static_net_tcp_event_tcp_relax( void* p, struct mm_tcp* tcp )
{

}
MM_EXPORT_DLL void mm_net_tcp_event_tcp_alloc_init(struct mm_net_tcp_event_tcp_alloc* p)
{
	p->event_tcp_alloc = &__static_net_tcp_event_tcp_alloc;
	p->event_tcp_relax = &__static_net_tcp_event_tcp_relax;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_net_tcp_event_tcp_alloc_destroy(struct mm_net_tcp_event_tcp_alloc* p)
{
	p->event_tcp_alloc = &__static_net_tcp_event_tcp_alloc;
	p->event_tcp_relax = &__static_net_tcp_event_tcp_relax;
	p->obj = NULL;
}
/////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_tcp_init(struct mm_net_tcp* p)
{
	struct mm_holder_u32_vpt_alloc holder32_alloc;
	struct mm_tcp_callback tcp_callback;
	struct mm_nuclear_callback nuclear_callback;

	mm_tcp_init(&p->tcp);
	mm_nuclear_init(&p->nuclear);
	mm_holder_u32_vpt_init(&p->holder);
	mm_net_tcp_callback_init(&p->callback);
	mm_net_tcp_event_tcp_alloc_init(&p->event_tcp_alloc);
	mm_crypto_callback_init(&p->crypto_callback);
	mm_spin_init(&p->holder_locker, NULL);
	mm_spin_init(&p->locker, NULL);
	p->nonblock_timeout = MM_NET_TCP_NONBLOCK_TIMEOUT;// tcp connect timeout.default is MM_NET_TCP_NONBLOCK_TIMEOUT milliseconds.
	p->tcp_state = net_tcp_state_closed;
	p->trytimes = MM_NET_TCP_TRYTIME;
	p->state = ts_closed;
	p->u = NULL;

	tcp_callback.handle = &__static_net_tcp_tcp_handle_callback;
	tcp_callback.broken = &__static_net_tcp_tcp_broken_callback;
	tcp_callback.obj = p;
	mm_tcp_assign_callback(&p->tcp,&tcp_callback);

	nuclear_callback.handle_recv = &__static_net_tcp_nuclear_handle_recv;
	nuclear_callback.handle_send = &__static_net_tcp_nuclear_handle_send;
	nuclear_callback.obj = p;
	mm_nuclear_assign_callback(&p->nuclear,&nuclear_callback);

	holder32_alloc.alloc = &mm_holder_u32_vpt_weak_alloc;
	holder32_alloc.relax = &mm_holder_u32_vpt_weak_relax;
	holder32_alloc.obj = p;
	mm_holder_u32_vpt_assign_alloc(&p->holder,&holder32_alloc);
}
MM_EXPORT_DLL void mm_net_tcp_destroy(struct mm_net_tcp* p)
{
	mm_net_tcp_clear_callback_holder(p);
	mm_net_tcp_detach_socket(p);
	//
	mm_tcp_destroy(&p->tcp);
	mm_nuclear_destroy(&p->nuclear);
	mm_holder_u32_vpt_destroy(&p->holder);
	mm_net_tcp_callback_destroy(&p->callback);
	mm_net_tcp_event_tcp_alloc_destroy(&p->event_tcp_alloc);
	mm_crypto_callback_destroy(&p->crypto_callback);
	mm_spin_destroy(&p->holder_locker);
	mm_spin_destroy(&p->locker);
	p->nonblock_timeout = 0;
	p->tcp_state = net_tcp_state_closed;
	p->trytimes = 0;
	p->state = ts_closed;
	p->u = NULL;
}
/////////////////////////////////////////////////////////////
// assign native address but not connect.
MM_EXPORT_DLL void mm_net_tcp_assign_native(struct mm_net_tcp* p,const char* node,mm_ushort_t port)
{
	mm_tcp_assign_native(&p->tcp, node, port);
}
// assign remote address but not connect.
MM_EXPORT_DLL void mm_net_tcp_assign_remote(struct mm_net_tcp* p,const char* node,mm_ushort_t port)
{
	mm_tcp_assign_remote(&p->tcp, node, port);
}
// assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
MM_EXPORT_DLL void mm_net_tcp_assign_native_storage(struct mm_net_tcp* p, struct sockaddr_storage* ss_native)
{
	mm_tcp_assign_native_storage(&p->tcp, ss_native);
}
// assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
MM_EXPORT_DLL void mm_net_tcp_assign_remote_storage(struct mm_net_tcp* p, struct sockaddr_storage* ss_remote)
{
	mm_tcp_assign_remote_storage(&p->tcp, ss_remote);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_tcp_assign_callback(struct mm_net_tcp* p,mm_uint32_t id,net_tcp_handle callback)
{
	mm_spin_lock(&p->holder_locker);
	mm_holder_u32_vpt_set(&p->holder,id,(void*)callback);
	mm_spin_unlock(&p->holder_locker);
}
MM_EXPORT_DLL void mm_net_tcp_assign_net_tcp_callback(struct mm_net_tcp* p,struct mm_net_tcp_callback* net_tcp_callback)
{
	assert(NULL != net_tcp_callback && "you can not assign null net_tcp_callback.");
	p->callback = *net_tcp_callback;
}
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_net_tcp_assign_event_tcp_alloc(struct mm_net_tcp* p,struct mm_net_tcp_event_tcp_alloc* event_tcp_alloc)
{
	assert(NULL != event_tcp_alloc && "you can not assign null event_tcp_alloc.");
	p->event_tcp_alloc = *event_tcp_alloc;
}
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_net_tcp_assign_crypto_callback(struct mm_net_tcp* p,struct mm_crypto_callback* crypto_callback)
{
	assert(NULL != crypto_callback && "you can not assign null crypto_callback.");
	p->crypto_callback = *crypto_callback;
}
// assign context handle.
MM_EXPORT_DLL void mm_net_tcp_assign_context(struct mm_net_tcp* p,void* u)
{
	p->u = u;
}
// assign nonblock_timeout.
MM_EXPORT_DLL void mm_net_tcp_assign_nonblock_timeout(struct mm_net_tcp* p,mm_msec_t nonblock_timeout)
{
	p->nonblock_timeout = nonblock_timeout;
}
// assign trytimes.
MM_EXPORT_DLL void mm_net_tcp_assign_trytimes(struct mm_net_tcp* p,mm_uint32_t trytimes)
{
	p->trytimes = trytimes;
}
//////////////////////////////////////////////////////////////////////////
// context for tcp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_DLL void mm_net_tcp_set_addr_context(struct mm_net_tcp* p, void* u)
{
	mm_tcp_set_context(&p->tcp,u);
}
MM_EXPORT_DLL void* mm_net_tcp_get_addr_context(struct mm_net_tcp* p)
{
	return mm_tcp_get_context(&p->tcp);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_tcp_clear_callback_holder(struct mm_net_tcp* p)
{
	mm_spin_lock(&p->holder_locker);
	mm_holder_u32_vpt_clear(&p->holder);
	mm_spin_unlock(&p->holder_locker);
}
//////////////////////////////////////////////////////////////////////////
// net_tcp crypto encrypt buffer by tcp.
MM_EXPORT_DLL void mm_net_tcp_crypto_encrypt(struct mm_net_tcp* p,struct mm_tcp* tcp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length)
{
	(*(p->crypto_callback.encrypt))(p,tcp,buffer,offset,buffer,offset,length);
}
// net_tcp crypto decrypt buffer by tcp.
MM_EXPORT_DLL void mm_net_tcp_crypto_decrypt(struct mm_net_tcp* p,struct mm_tcp* tcp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length)
{
	(*(p->crypto_callback.decrypt))(p,tcp,buffer,offset,buffer,offset,length);
}
//////////////////////////////////////////////////////////////////////////
// lock to make sure the net_tcp is thread safe.
MM_EXPORT_DLL void mm_net_tcp_lock(struct mm_net_tcp* p)
{
	mm_spin_lock(&p->locker);
}
// unlock net_tcp.
MM_EXPORT_DLL void mm_net_tcp_unlock(struct mm_net_tcp* p)
{
	mm_spin_unlock(&p->locker);
}
//////////////////////////////////////////////////////////////////////////
// fopen socket.
MM_EXPORT_DLL void mm_net_tcp_fopen_socket(struct mm_net_tcp* p)
{
	mm_tcp_fopen_socket(&p->tcp);
	//mm_nuclear_fd_add(&p->nuclear,p->tcp.addr.socket,&p->tcp);
}
// synchronize connect.do not call this as we already open connect check thread.
MM_EXPORT_DLL void mm_net_tcp_connect(struct mm_net_tcp* p)
{
	char link_name[MM_LINK_NAME_LENGTH];
	struct mm_addr* addr = &p->tcp.addr;
	struct mm_logger* g_logger = mm_logger_instance();
	struct timeval tv;
	fd_set rset;
	fd_set wset;
	//
	mm_addr_string(addr,link_name);
	//
	tv.tv_sec  = (mm_timeval_ssec_t) ( p->nonblock_timeout / 1000);
	tv.tv_usec = (mm_timeval_usec_t) ((p->nonblock_timeout % 1000) * 1000);
	//
	if ( MM_INVALID_SOCKET != addr->socket )
	{
		mm_uint32_t _try_times = 0;
		int rt = -1;
		p->tcp_state = net_tcp_state_motion;
		// set socket to not block.
		mm_addr_set_block(addr, MM_NONBLOCK);
		do 
		{
			_try_times ++;
			FD_ZERO(&rset);    
			FD_ZERO(&wset);    
			FD_SET(addr->socket, &rset);
			FD_SET(addr->socket, &wset);
			rt = mm_addr_connect(addr);
			if (0 == rt)
			{
				// connect immediately.
				break;
			}
			rt = select(addr->socket + 1, &rset, &wset, NULL, &tv);
			if ( 0 >= rt )
			{ 
				mm_logger_log_E(g_logger,"%s %d connect %s failure.try times:%d",__FUNCTION__,__LINE__,link_name,_try_times);
				mm_msleep(MM_NET_TCP_TRY_MSLEEP_TIME);
				rt = -1;
			}
			else
			{
				// select return > 0 means success.
				rt = 0;
			}
			if ( ts_finish == p->state )
			{
				break;
			}
		} while ( 0 != rt && _try_times < p->trytimes );
		if ( 0 != rt )
		{
			mm_err_t errcode = mm_errno;
			mm_logger_log_E(g_logger,"%s %d connect %s failure.try times:%d",__FUNCTION__,__LINE__,link_name,_try_times);
			mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
			mm_net_tcp_apply_broken(p);
			mm_addr_close_socket(addr);
		}
		else
		{
			mm_net_tcp_apply_finish(p);
			mm_logger_log_I(g_logger,"%s %d %s success.try times:%d",__FUNCTION__,__LINE__,link_name,_try_times);
		}
	}
	else
	{
		mm_logger_log_E(g_logger,"%s %d connect target fd is invalid.",__FUNCTION__,__LINE__);
	}
}
// close socket.
MM_EXPORT_DLL void mm_net_tcp_close_socket(struct mm_net_tcp* p)
{
	//mm_nuclear_fd_rmv(&p->nuclear,p->tcp.addr.socket,&p->tcp);
	mm_addr_close_socket(&p->tcp.addr);
}
// shutdown socket.
MM_EXPORT_DLL void mm_net_tcp_shutdown_socket(struct mm_net_tcp* p, int opcode)
{
	mm_addr_shutdown_socket(&p->tcp.addr,opcode);
}
// set socket block.
MM_EXPORT_DLL void mm_net_tcp_set_block(struct mm_net_tcp* p, int block)
{
	mm_addr_set_block(&p->tcp.addr, block);
}
// streambuf reset.
MM_EXPORT_DLL void mm_net_tcp_streambuf_reset(struct mm_net_tcp* p)
{
	mm_tcp_streambuf_reset(&p->tcp);
}
// check connect and reconnect if disconnect.
MM_EXPORT_DLL void mm_net_tcp_check(struct mm_net_tcp* p)
{
	if ( 0 != mm_net_tcp_finally_state(p) )
	{
		// we first disconnect this old connect.
		mm_net_tcp_detach_socket(p);
		// fopen a socket.
		mm_net_tcp_fopen_socket(p);
		// net tcp connect.
		mm_net_tcp_connect(p);
	}
}
// get state finally.return 0 for all regular.not thread safe.
MM_EXPORT_DLL int mm_net_tcp_finally_state(struct mm_net_tcp* p)
{
	return ( MM_INVALID_SOCKET != p->tcp.addr.socket && net_tcp_state_finish == p->tcp_state ) ? 0 : -1;
}
// synchronize attach to socket.not thread safe.
MM_EXPORT_DLL void mm_net_tcp_attach_socket(struct mm_net_tcp* p)
{
	mm_net_tcp_connect(p);
}
// synchronize detach to socket.not thread safe.
MM_EXPORT_DLL void mm_net_tcp_detach_socket(struct mm_net_tcp* p)
{
	if ( MM_INVALID_SOCKET != p->tcp.addr.socket)
	{
		// set flag value.
		p->tcp_state = net_tcp_state_closed;
		// set block state.
		mm_net_tcp_set_block(p, MM_BLOCKING);
		// shutdown socket first.
		mm_net_tcp_shutdown_socket(p,MM_BOTH_SHUTDOWN);
		// close socket.
		mm_net_tcp_close_socket(p);
		// buffer is ivalid.
		mm_net_tcp_streambuf_reset(p);
	}
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_tcp_apply_broken(struct mm_net_tcp* p)
{
	p->tcp_state = net_tcp_state_closed;
	mm_tcp_streambuf_reset(&p->tcp);
	(*p->callback.broken)(&p->tcp);
	mm_nuclear_fd_rmv(&p->nuclear,p->tcp.addr.socket,&p->tcp);
}
MM_EXPORT_DLL void mm_net_tcp_apply_finish(struct mm_net_tcp* p)
{
	p->tcp_state = net_tcp_state_finish;
	mm_tcp_streambuf_reset(&p->tcp);
	mm_nuclear_fd_add(&p->nuclear,p->tcp.addr.socket,&p->tcp);
	(*p->callback.finish)(&p->tcp);
}
//////////////////////////////////////////////////////////////////////////
// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_net_tcp_buffer_send(struct mm_net_tcp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length)
{
	int rt = -1;
	if ( net_tcp_state_finish == p->tcp_state && MM_INVALID_SOCKET != p->tcp.addr.socket )
	{
		rt = mm_tcp_buffer_send(&p->tcp,buffer,offset,length);
		// update the tcp state,because the mm_tcp_buffer_send can not fire broken event immediately.
		p->tcp_state = -1 == rt ? net_tcp_state_broken : p->tcp_state;
	}
	else
	{
		// net tcp nready.fire event.
		(*p->callback.nready)(&p->tcp);
	}
	return rt;
}
// handle send data by flush send buffer.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_net_tcp_flush_send(struct mm_net_tcp* p)
{
	int rt = -1;
	if ( net_tcp_state_finish == p->tcp_state && MM_INVALID_SOCKET != p->tcp.addr.socket )
	{
		rt = mm_tcp_flush_send(&p->tcp);
		// update the tcp state,because the mm_tcp_buffer_send can not fire broken event immediately.
		p->tcp_state = -1 == rt ? net_tcp_state_broken : p->tcp_state;
	}
	else
	{
		// net tcp nready.fire event.
		(*p->callback.nready)(&p->tcp);
	}
	return rt;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_tcp_start(struct mm_net_tcp* p)
{
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	mm_nuclear_start(&p->nuclear);
	(*(p->event_tcp_alloc.event_tcp_alloc))(p,&p->tcp);
}
MM_EXPORT_DLL void mm_net_tcp_interrupt(struct mm_net_tcp* p)
{
	p->state = ts_closed;
	mm_nuclear_interrupt(&p->nuclear);
}
MM_EXPORT_DLL void mm_net_tcp_shutdown(struct mm_net_tcp* p)
{
	p->state = ts_finish;
	mm_nuclear_shutdown(&p->nuclear);
}
MM_EXPORT_DLL void mm_net_tcp_join(struct mm_net_tcp* p)
{
	mm_nuclear_join(&p->nuclear);
	(*(p->event_tcp_alloc.event_tcp_relax))(p,&p->tcp);
}
//////////////////////////////////////////////////////////////////////////
static void __static_net_tcp_tcp_handle_callback(void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length)
{
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(tcp->callback.obj);
	mm_net_tcp_crypto_decrypt(net_tcp,tcp,buffer,offset,length);
	mm_streambuf_packet_handle_tcp(&tcp->buff_recv, buffer, offset, length, &__static_net_tcp_tcp_handle_packet_callback, tcp);
}
static void __static_net_tcp_tcp_broken_callback(void* obj)
{
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(tcp->callback.obj);
	assert(NULL != net_tcp->callback.broken && "net_tcp->callback.broken is a null.");
	mm_net_tcp_apply_broken(net_tcp);
}

static void __static_net_tcp_nuclear_handle_recv(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length)
{
	struct mm_tcp* tcp = (struct mm_tcp*)u;
	mm_tcp_handle_recv(tcp,buffer,offset,max_length);
}
static void __static_net_tcp_nuclear_handle_send(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length)
{
	struct mm_tcp* tcp = (struct mm_tcp*)u;
	mm_tcp_handle_send(tcp,buffer,offset,max_length);
}

static void __static_net_tcp_tcp_handle_packet_callback(void* obj,struct mm_packet* pack)
{
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(tcp->callback.obj);
	net_tcp_handle handle = NULL;
	mm_spin_lock(&net_tcp->holder_locker);
	handle = (net_tcp_handle)mm_holder_u32_vpt_get(&net_tcp->holder,pack->phead.mid);
	mm_spin_unlock(&net_tcp->holder_locker);
	if (NULL != handle)
	{
		// fire the handle event.
		(*(handle))(tcp,net_tcp->u,pack);
	}
	else
	{
		assert(NULL != net_tcp->callback.handle && "net_tcp->callback.handle is a null.");
		(*(net_tcp->callback.handle))(tcp,net_tcp->u,pack);
	}
}
//////////////////////////////////////////////////////////////////////////
