#ifndef __mm_headset_h__
#define __mm_headset_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_string.h"
#include "core/mm_spinlock.h"
#include "core/mm_time.h"

#include "net/mm_net_udp.h"

#include "container/mm_holder32.h"

#include <pthread.h>

// headset default page size.
#define MM_HEADSET_PAGE_SIZE 1024

// net udp nonblock timeout..
#define MM_HEADSET_NONBLOCK_TIMEOUT 20
// if bind listen failure will msleep this time.
#define MM_HEADSET_TRY_MSLEEP_TIME 1000
// net udp one time try time.
#define MM_HEADSET_TRYTIME -1

enum mm_headset_state_t
{
	headset_state_closed = 0,// fd is closed,connect closed.invalid at all.
	headset_state_motion = 1,// fd is opened,connect closed.at connecting.
	headset_state_finish = 2,// fd is opened,connect opened.at connected.
	headset_state_broken = 3,// fd is opened,connect broken.connect is broken fd not closed.
};

// microphone.special for udp server.
struct mm_mic
{
	struct mm_udp udp;// weak ref.
	pthread_t poll_thread;
	mm_atomic_t locker;
	int udp_state;// mm_net_udp_state_t,default is headset_state_closed(0)
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mic_init(struct mm_mic* p);
MM_EXPORT_DLL void mm_mic_destroy(struct mm_mic* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mic_lock(struct mm_mic* p);
MM_EXPORT_DLL void mm_mic_unlock(struct mm_mic* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mic_assign_udp_shared(struct mm_mic* p,struct mm_udp* udp);
MM_EXPORT_DLL void mm_mic_update_udp_state(struct mm_mic* p,int state);
MM_EXPORT_DLL void mm_mic_clear(struct mm_mic* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mic_start(struct mm_mic* p);
MM_EXPORT_DLL void mm_mic_interrupt(struct mm_mic* p);
MM_EXPORT_DLL void mm_mic_shutdown(struct mm_mic* p);
MM_EXPORT_DLL void mm_mic_join(struct mm_mic* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mic_poll_wait(struct mm_mic* p);
//////////////////////////////////////////////////////////////////////////
// mic streambuf recv send reset.
MM_EXPORT_DLL void mm_mic_streambuf_reset(struct mm_mic* p);
//////////////////////////////////////////////////////////////////////////
typedef void (*headset_handle)( void* obj, void* u, struct mm_packet* pack, struct sockaddr_storage* remote );
typedef void (*headset_broken)( void* obj );
typedef void (*headset_nready)( void* obj );
typedef void (*headset_finish)( void* obj );
struct mm_headset_callback
{
	headset_handle handle;
	headset_broken broken;
	headset_nready nready;
	headset_finish finish;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_headset_callback_init(struct mm_headset_callback* p);
MM_EXPORT_DLL void mm_headset_callback_destroy(struct mm_headset_callback* p);

// mm_headset is a multithreading udp event accept.
struct mm_headset
{
	struct mm_udp udp;// strong ref.
	struct mm_headset_callback callback;
	struct mm_crypto_callback crypto_callback;// crypto callback.
	struct mm_holder_u32_vpt holder;// rb tree for msg_id <--> callback.
	pthread_mutex_t signal_mutex;
	pthread_cond_t signal_cond;
	mm_atomic_t arrays_locker;
	mm_atomic_t holder_locker;
	mm_atomic_t locker;
	mm_uint32_t length;// length. default is 0.
	struct mm_mic** arrays;// array pointer.
	mm_msec_t nonblock_timeout;// udp nonblock timeout.default is MM_HEADSET_NONBLOCK_TIMEOUT milliseconds.
	mm_uint32_t trytimes;// try bind and listen times.default is MM_HEADSET_TRYTIME.
	//
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
	int udp_state;// mm_net_udp_state_t,default is udp_state_closed(0)
	void* u;// user data.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_headset_init(struct mm_headset* p);
MM_EXPORT_DLL void mm_headset_destroy(struct mm_headset* p);
//////////////////////////////////////////////////////////////////////////
// lock.
MM_EXPORT_DLL void mm_headset_lock(struct mm_headset* p);
// unlock.
MM_EXPORT_DLL void mm_headset_unlock(struct mm_headset* p);
//////////////////////////////////////////////////////////////////////////
// assign native address but not connect.
MM_EXPORT_DLL void mm_headset_assign_native(struct mm_headset* p,const char* node,mm_ushort_t port);
// assign remote address but not connect.
MM_EXPORT_DLL void mm_headset_assign_remote(struct mm_headset* p,const char* node,mm_ushort_t port);
// assign protocol_size.
MM_EXPORT_DLL void mm_headset_set_length(struct mm_headset* p, mm_uint32_t length);
MM_EXPORT_DLL mm_uint32_t mm_headset_get_length(struct mm_headset* p);
// assign context handle.
MM_EXPORT_DLL void mm_headset_assign_context(struct mm_headset* p,void* u);
// assign nonblock_timeout.
MM_EXPORT_DLL void mm_headset_assign_nonblock_timeout(struct mm_headset* p,mm_msec_t nonblock_timeout);
// assign trytimes.
MM_EXPORT_DLL void mm_headset_assign_trytimes(struct mm_headset* p,mm_uint32_t trytimes);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_headset_assign_callback(struct mm_headset* p,mm_uint32_t id,headset_handle callback);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_headset_clear_callback_holder(struct mm_headset* p);
//////////////////////////////////////////////////////////////////////////
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_headset_assign_crypto_callback(struct mm_headset* p,struct mm_crypto_callback* crypto_callback);
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_headset_assign_headset_callback(struct mm_headset* p,struct mm_headset_callback* headset_callback);
// context for udp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_DLL void mm_headset_set_addr_context(struct mm_headset* p, void* u);
MM_EXPORT_DLL void* mm_headset_get_addr_context(struct mm_headset* p);
//////////////////////////////////////////////////////////////////////////
// headset crypto encrypt buffer by udp.
MM_EXPORT_DLL void mm_headset_crypto_encrypt(struct mm_headset* p,struct mm_udp* udp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length);
// headset crypto decrypt buffer by udp.
MM_EXPORT_DLL void mm_headset_crypto_decrypt(struct mm_headset* p,struct mm_udp* udp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length);
//////////////////////////////////////////////////////////////////////////
// fopen.
MM_EXPORT_DLL void mm_headset_fopen_socket(struct mm_headset* p);
// bind.
MM_EXPORT_DLL void mm_headset_bind(struct mm_headset* p);
// close socket.mm_headset_join before call this.
MM_EXPORT_DLL void mm_headset_close_socket(struct mm_headset* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_headset_clear(struct mm_headset* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_headset_apply_broken(struct mm_headset* p);
MM_EXPORT_DLL void mm_headset_apply_finish(struct mm_headset* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_headset_start(struct mm_headset* p);
MM_EXPORT_DLL void mm_headset_interrupt(struct mm_headset* p);
MM_EXPORT_DLL void mm_headset_shutdown(struct mm_headset* p);
MM_EXPORT_DLL void mm_headset_join(struct mm_headset* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_headset_h__