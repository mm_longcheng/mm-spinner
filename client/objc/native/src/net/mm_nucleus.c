#include "net/mm_nucleus.h"
#include "core/mm_logger.h"
#include "core/mm_alloc.h"
#include "core/mm_hash_func.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_nucleus_init(struct mm_nucleus* p)
{
	mm_nuclear_callback_init(&p->callback);
	pthread_mutex_init(&p->signal_mutex, NULL);  
	pthread_cond_init(&p->signal_cond, NULL);
	mm_spin_init(&p->arrays_locker, NULL);
	mm_spin_init(&p->locker, NULL);
	p->length = 0;
	p->arrays = NULL;
	p->poll_length = MM_NUCLEAR_POLL_LENGTH;
	p->poll_timeout = MM_NUCLEAR_IDLE_SLEEP_MSEC;// poll timeout.
	p->state = ts_closed;
}
MM_EXPORT_DLL void mm_nucleus_destroy(struct mm_nucleus* p)
{
	mm_nucleus_clear(p);
	//
	mm_nuclear_callback_destroy(&p->callback);
	pthread_mutex_destroy(&p->signal_mutex);  
	pthread_cond_destroy(&p->signal_cond);
	mm_spin_destroy(&p->arrays_locker);
	mm_spin_destroy(&p->locker);
	p->poll_length = 0;
	p->poll_timeout = 0;
	p->state = ts_closed;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_nucleus_lock(struct mm_nucleus* p)
{
	mm_spin_lock(&p->locker);
}
MM_EXPORT_DLL void mm_nucleus_unlock(struct mm_nucleus* p)
{
	mm_spin_unlock(&p->locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_nucleus_set_length(struct mm_nucleus* p, mm_uint32_t length)
{
	mm_spin_lock(&p->arrays_locker);
	if (length < p->length)
	{
		mm_uint32_t i = 0;
		struct mm_nuclear* e = NULL;
		for ( i = length; i < p->length; ++i)
		{
			e = p->arrays[i];
			mm_nuclear_lock(e);
			p->arrays[i] = NULL;
			mm_nuclear_unlock(e);
			mm_nuclear_destroy(e);
			mm_free(e);
		}
		p->arrays = (struct mm_nuclear**)mm_realloc(p->arrays,sizeof(struct mm_nuclear*) * length);
		p->length = length;
	}
	else if (length > p->length)
	{
		mm_uint32_t i = 0;
		p->arrays = (struct mm_nuclear**)mm_realloc(p->arrays,sizeof(struct mm_nuclear*) * length);
		for ( i = p->length; i < length; ++i)
		{
			p->arrays[i] = (struct mm_nuclear*)mm_malloc(sizeof(struct mm_nuclear));
			mm_nuclear_init(p->arrays[i]);
			p->arrays[i]->poll_length = p->poll_length;
			p->arrays[i]->poll_timeout = p->poll_timeout;
			p->arrays[i]->state = p->state;
			mm_nuclear_assign_callback(p->arrays[i],&p->callback);
		}
		p->length = length;
	}
	mm_spin_unlock(&p->arrays_locker);
}
MM_EXPORT_DLL mm_uint32_t mm_nucleus_get_length(struct mm_nucleus* p)
{
	return p->length;
}
MM_EXPORT_DLL void mm_nucleus_assign_callback(struct mm_nucleus* p,struct mm_nuclear_callback* cb)
{
	mm_uint32_t i = 0;
	assert(NULL != cb && "you can not assign null callback.");
	mm_spin_lock(&p->arrays_locker);
	p->callback = (*cb);
	for ( i = 0; i < p->length; ++i)
	{
		mm_nuclear_lock(p->arrays[i]);
		mm_nuclear_assign_callback(p->arrays[i],cb);
		mm_nuclear_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
}
//////////////////////////////////////////////////////////////////////////
// poll size.
MM_EXPORT_DLL size_t mm_nucleus_poll_size(struct mm_nucleus* p)
{
	size_t sz = 0;
	mm_uint32_t i = 0;
	mm_spin_lock(&p->arrays_locker);
	for ( i = 0; i < p->length; ++i)
	{
		mm_nuclear_lock(p->arrays[i]);
		sz += mm_nuclear_poll_size(p->arrays[i]);
		mm_nuclear_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
	return sz;
}
//////////////////////////////////////////////////////////////////////////
// wait for activation fd.
MM_EXPORT_DLL void mm_nucleus_poll_wait(struct mm_nucleus* p)
{
	mm_uint32_t i = 0;
	mm_spin_lock(&p->arrays_locker);
	for ( i = 0; i < p->length; ++i)
	{
		mm_nuclear_lock(p->arrays[i]);
		mm_nuclear_poll_wait(p->arrays[i]);
		mm_nuclear_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
}
//////////////////////////////////////////////////////////////////////////
// start wait thread.
MM_EXPORT_DLL void mm_nucleus_start(struct mm_nucleus* p)
{
	mm_uint32_t i = 0;
	mm_spin_lock(&p->arrays_locker);
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	for ( i = 0; i < p->length; ++i)
	{
		mm_nuclear_lock(p->arrays[i]);
		mm_nuclear_start(p->arrays[i]);
		mm_nuclear_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
}
// interrupt wait thread.
MM_EXPORT_DLL void mm_nucleus_interrupt(struct mm_nucleus* p)
{
	mm_uint32_t i = 0;
	mm_spin_lock(&p->arrays_locker);
	p->state = ts_closed;
	for ( i = 0; i < p->length; ++i)
	{
		mm_nuclear_lock(p->arrays[i]);
		mm_nuclear_interrupt(p->arrays[i]);
		mm_nuclear_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
	//
	pthread_mutex_lock(&p->signal_mutex);
	pthread_cond_signal(&p->signal_cond);
	pthread_mutex_unlock(&p->signal_mutex);
}
// shutdown wait thread.
MM_EXPORT_DLL void mm_nucleus_shutdown(struct mm_nucleus* p)
{
	mm_uint32_t i = 0;
	mm_spin_lock(&p->arrays_locker);
	p->state = ts_finish;
	for ( i = 0; i < p->length; ++i)
	{
		mm_nuclear_lock(p->arrays[i]);
		mm_nuclear_shutdown(p->arrays[i]);
		mm_nuclear_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
	//
	pthread_mutex_lock(&p->signal_mutex);
	pthread_cond_signal(&p->signal_cond);
	pthread_mutex_unlock(&p->signal_mutex);
}
// join wait thread.
MM_EXPORT_DLL void mm_nucleus_join(struct mm_nucleus* p)
{
	mm_uint32_t i = 0;
	if (ts_motion == p->state)
	{
		// we can not lock or join until cond wait all thread is shutdown.
		pthread_mutex_lock(&p->signal_mutex);
		pthread_cond_wait(&p->signal_cond,&p->signal_mutex);
		pthread_mutex_unlock(&p->signal_mutex);
	}
	//
	mm_spin_lock(&p->arrays_locker);
	for ( i = 0; i < p->length; ++i)
	{
		mm_nuclear_lock(p->arrays[i]);
		mm_nuclear_join(p->arrays[i]);
		mm_nuclear_unlock(p->arrays[i]);
	}
	mm_spin_unlock(&p->arrays_locker);
}
MM_EXPORT_DLL void mm_nucleus_clear(struct mm_nucleus* p)
{
	mm_uint32_t i = 0;
	mm_spin_lock(&p->arrays_locker);
	for ( i = 0; i < p->length; ++i)
	{
		struct mm_nuclear* e = p->arrays[i];
		mm_nuclear_lock(e);
		p->arrays[i] = NULL;
		mm_nuclear_unlock(e);
		mm_nuclear_destroy(e);
		mm_free(e);
	}
	mm_free(p->arrays);
	p->arrays = NULL;
	p->length = 0;
	mm_spin_unlock(&p->arrays_locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL mm_uint32_t mm_nucleus_hash_index(struct mm_nucleus* p, mm_socket_t fd )
{
	return mm_bit32_hash(fd) % p->length;
}
//////////////////////////////////////////////////////////////////////////
// add u into poll_fd.
MM_EXPORT_DLL void mm_nucleus_fd_add(struct mm_nucleus* p, mm_socket_t fd, void* u)
{
	mm_uint32_t idx = mm_nucleus_hash_index(p,fd);
	assert(idx >= 0 && idx < p->length && "idx is out range.");
	mm_nuclear_fd_add(p->arrays[idx],fd,u);
}
// rmv u from poll_fd.
MM_EXPORT_DLL void mm_nucleus_fd_rmv(struct mm_nucleus* p, mm_socket_t fd, void* u)
{
	mm_uint32_t idx = mm_nucleus_hash_index(p,fd);
	assert(idx >= 0 && idx < p->length && "idx is out range.");
	mm_nuclear_fd_rmv(p->arrays[idx],fd,u);
}
// mod u event from poll wait.
MM_EXPORT_DLL void mm_nucleus_fd_mod(struct mm_nucleus* p, mm_socket_t fd, void* u, int flag)
{
	mm_uint32_t idx = mm_nucleus_hash_index(p,fd);
	assert(idx >= 0 && idx < p->length && "idx is out range.");
	mm_nuclear_fd_mod(p->arrays[idx],fd,u,flag);
}
// get u from poll.
MM_EXPORT_DLL void* mm_nucleus_fd_get(struct mm_nucleus* p, mm_socket_t fd)
{
	mm_uint32_t idx = mm_nucleus_hash_index(p,fd);
	assert(idx >= 0 && idx < p->length && "idx is out range.");
	return mm_nuclear_fd_get(p->arrays[idx],fd);
}
//////////////////////////////////////////////////////////////////////////
