#ifndef __mm_net_udp_h__
#define __mm_net_udp_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_spinlock.h"
#include "core/mm_timer_task.h"

#include "net/mm_udp.h"
#include "net/mm_nuclear.h"
#include "net/mm_crypto.h"
#include "net/mm_packet.h"

#include "container/mm_holder32.h"

// net udp nonblock timeout..
#define MM_NET_UDP_NONBLOCK_TIMEOUT 20
// if bind listen failure will msleep this time.
#define MM_NET_UDP_TRY_MSLEEP_TIME 1000
// net udp one time try time.
#define MM_NET_UDP_TRYTIME 3

typedef void (*net_udp_handle)(void* obj, void* u, struct mm_packet* pack, struct sockaddr_storage* remote);
typedef void (*net_udp_broken)(void* obj);
typedef void (*net_udp_nready)(void* obj);
typedef void (*net_udp_finish)(void* obj);
struct mm_net_udp_callback
{
	net_udp_handle handle;
	net_udp_broken broken;
	net_udp_nready nready;
	net_udp_finish finish;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_net_udp_callback_init(struct mm_net_udp_callback* p);
MM_EXPORT_DLL void mm_net_udp_callback_destroy(struct mm_net_udp_callback* p);

typedef void (*net_udp_event_udp_alloc)( void* p, struct mm_udp* udp );
typedef void (*net_udp_event_udp_relax)( void* p, struct mm_udp* udp );
struct mm_net_udp_event_udp_alloc
{
	net_udp_event_udp_alloc event_udp_alloc;// event udp alloc and add holder.
	net_udp_event_udp_relax event_udp_relax;// event udp relax and rmv holder.
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_net_udp_event_udp_alloc_init(struct mm_net_udp_event_udp_alloc* p);
MM_EXPORT_DLL void mm_net_udp_event_udp_alloc_destroy(struct mm_net_udp_event_udp_alloc* p);

enum mm_net_udp_state_t
{
	net_udp_state_closed = 0,// fd is closed,connect closed.invalid at all.
	net_udp_state_motion = 1,// fd is opened,connect closed.at connecting.
	net_udp_state_finish = 2,// fd is opened,connect opened.at connected.
	net_udp_state_broken = 3,// fd is opened,connect broken.connect is broken fd not closed.
};

struct mm_net_udp
{
	struct mm_udp udp;// strong ref.
	struct mm_nuclear nuclear;// strong ref.
	struct mm_holder_u32_vpt holder;// rb tree for msg_id <--> callback.
	struct mm_net_udp_callback callback;// value ref. udp callback.
	struct mm_net_udp_event_udp_alloc event_udp_alloc;
	struct mm_crypto_callback crypto_callback;// crypto callback.
	mm_atomic_t holder_locker;
	mm_atomic_t locker;

	mm_msec_t nonblock_timeout;// udp nonblock timeout.default is MM_NET_UDP_NONBLOCK_TIMEOUT milliseconds.

	int udp_state;// mm_net_udp_state_t,default is udp_state_closed(0)

	mm_uint32_t trytimes;// try bind and listen times.default is MM_NET_UDP_TRYTIME.
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
	void* u;// user data.
};
/////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_udp_init(struct mm_net_udp* p);
MM_EXPORT_DLL void mm_net_udp_destroy(struct mm_net_udp* p);
//////////////////////////////////////////////////////////////////////////
// assign native address but not connect.
MM_EXPORT_DLL void mm_net_udp_assign_native(struct mm_net_udp* p,const char* node,mm_ushort_t port);
// assign remote address but not connect.
MM_EXPORT_DLL void mm_net_udp_assign_remote(struct mm_net_udp* p,const char* node,mm_ushort_t port);
// assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
MM_EXPORT_DLL void mm_net_udp_assign_native_storage(struct mm_net_udp* p, struct sockaddr_storage* ss_native);
// assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
MM_EXPORT_DLL void mm_net_udp_assign_remote_storage(struct mm_net_udp* p, struct sockaddr_storage* ss_remote);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_udp_assign_callback(struct mm_net_udp* p,mm_uint32_t id,net_udp_handle callback);
MM_EXPORT_DLL void mm_net_udp_assign_net_udp_callback(struct mm_net_udp* p,struct mm_net_udp_callback* net_udp_callback);
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_net_udp_assign_event_udp_alloc(struct mm_net_udp* p,struct mm_net_udp_event_udp_alloc* event_udp_alloc);
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_net_udp_assign_crypto_callback(struct mm_net_udp* p,struct mm_crypto_callback* crypto_callback);
// assign context handle.
MM_EXPORT_DLL void mm_net_udp_assign_context(struct mm_net_udp* p,void* u);
// assign nonblock_timeout.
MM_EXPORT_DLL void mm_net_udp_assign_nonblock_timeout(struct mm_net_udp* p,mm_msec_t nonblock_timeout);
// assign trytimes.
MM_EXPORT_DLL void mm_net_udp_assign_trytimes(struct mm_net_udp* p,mm_uint32_t trytimes);
//////////////////////////////////////////////////////////////////////////
// context for udp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_DLL void mm_net_udp_set_addr_context(struct mm_net_udp* p, void* u);
MM_EXPORT_DLL void* mm_net_udp_get_addr_context(struct mm_net_udp* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_udp_clear_callback_holder(struct mm_net_udp* p);
//////////////////////////////////////////////////////////////////////////
// net_udp crypto encrypt buffer by udp.
MM_EXPORT_DLL void mm_net_udp_crypto_encrypt(struct mm_net_udp* p,struct mm_udp* udp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length);
// net_udp crypto decrypt buffer by udp.
MM_EXPORT_DLL void mm_net_udp_crypto_decrypt(struct mm_net_udp* p,struct mm_udp* udp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length);
//////////////////////////////////////////////////////////////////////////
// lock to make sure the net_udp is thread safe.
MM_EXPORT_DLL void mm_net_udp_lock(struct mm_net_udp* p);
// unlock net_udp.
MM_EXPORT_DLL void mm_net_udp_unlock(struct mm_net_udp* p);
//////////////////////////////////////////////////////////////////////////
// fopen socket.
MM_EXPORT_DLL void mm_net_udp_fopen_socket(struct mm_net_udp* p);
// bind.
MM_EXPORT_DLL void mm_net_udp_bind(struct mm_net_udp* p);
// close socket.
MM_EXPORT_DLL void mm_net_udp_close_socket(struct mm_net_udp* p);
// shutdown socket.
MM_EXPORT_DLL void mm_net_udp_shutdown_socket(struct mm_net_udp* p, int opcode);
// set socket block.
MM_EXPORT_DLL void mm_net_udp_set_block(struct mm_net_udp* p, int block);
// streambuf reset.
MM_EXPORT_DLL void mm_net_udp_streambuf_reset(struct mm_net_udp* p);
//////////////////////////////////////////////////////////////////////////
// check state.not thread safe.
MM_EXPORT_DLL void mm_net_udp_check(struct mm_net_udp* p);
// get state finally.return 0 for all regular.not thread safe.
MM_EXPORT_DLL int mm_net_udp_finally_state(struct mm_net_udp* p);
//////////////////////////////////////////////////////////////////////////
// reset streambuf and fire event broken.
MM_EXPORT_DLL void mm_net_udp_apply_broken(struct mm_net_udp* p);
// reset streambuf and fire event finish.
MM_EXPORT_DLL void mm_net_udp_apply_finish(struct mm_net_udp* p);
// synchronize attach to socket.not thread safe.
MM_EXPORT_DLL void mm_net_udp_attach_socket(struct mm_net_udp* p);
// synchronize detach to socket.not thread safe.
MM_EXPORT_DLL void mm_net_udp_detach_socket(struct mm_net_udp* p);
//////////////////////////////////////////////////////////////////////////
// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_net_udp_buffer_send(struct mm_net_udp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote);
// handle send data by flush send buffer.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_net_udp_flush_send(struct mm_net_udp* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_udp_start(struct mm_net_udp* p);
MM_EXPORT_DLL void mm_net_udp_interrupt(struct mm_net_udp* p);
MM_EXPORT_DLL void mm_net_udp_shutdown(struct mm_net_udp* p);
MM_EXPORT_DLL void mm_net_udp_join(struct mm_net_udp* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_net_udp_h__
