#include "net/mm_packet.h"
#include "core/mm_alloc.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_packet_head_init(struct mm_packet_head* p)
{
	//p->mid = 0;// msg     id.
	//p->pid = 0;// proxy   id.
	//p->sid = 0;// section id.
	//p->uid = 0;// unique  id.
	mm_memset(p,0,sizeof(struct mm_packet_head));
}
MM_EXPORT_DLL void mm_packet_head_destroy(struct mm_packet_head* p)
{
	//p->mid = 0;// msg     id.
	//p->pid = 0;// proxy   id.
	//p->sid = 0;// section id.
	//p->uid = 0;// unique  id.
	mm_memset(p,0,sizeof(struct mm_packet_head));
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_packet_buff_init(struct mm_packet_buff* p)
{
	//p->length = 0;// packet buff length.
	//p->offset = 0;// packet buff offset.
	//p->buffer = 0;// packet buff buffer.
	mm_memset(p,0,sizeof(struct mm_packet_buff));
}
MM_EXPORT_DLL void mm_packet_buff_destroy(struct mm_packet_buff* p)
{
	//p->length = 0;// packet buff length.
	//p->offset = 0;// packet buff offset.
	//p->buffer = 0;// packet buff buffer.
	mm_memset(p,0,sizeof(struct mm_packet_buff));
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_packet_init(struct mm_packet* p)
{
	mm_packet_head_init(&p->phead);
	mm_packet_buff_init(&p->hbuff);
	mm_packet_buff_init(&p->bbuff);
}
MM_EXPORT_DLL void mm_packet_destroy(struct mm_packet* p)
{
	mm_packet_head_destroy(&p->phead);
	mm_packet_buff_destroy(&p->hbuff);
	mm_packet_buff_destroy(&p->bbuff);
}
//////////////////////////////////////////////////////////////////////////
// reset all to zero.
MM_EXPORT_DLL void mm_packet_reset(struct mm_packet* p)
{
	mm_memset(p,0,sizeof(struct mm_packet));
}
// use for quick zero head base.(mid,uid,sid,pid)
MM_EXPORT_DLL void mm_packet_head_base_zero(struct mm_packet* p)
{
	mm_memset(&p->phead,0,sizeof(struct mm_packet_head));
}
// use for quick copy head base r-->p.(mid,uid,sid,pid)
MM_EXPORT_DLL void mm_packet_head_base_copy(struct mm_packet* p, struct mm_packet* r)
{
	mm_memcpy(&p->phead,&r->phead,sizeof(struct mm_packet_head));
}
// use for quick decode head base.depend on hsize (mid)/(mid,uid,sid,pid)/(other).
// p : struct mm_packet*
MM_EXPORT_DLL void mm_packet_head_base_decode(struct mm_packet* p)
{
	mm_memcpy(&p->phead,p->hbuff.buffer + p->hbuff.offset,p->hbuff.length);
}
// use for quick encode head base.depend on hsize (mid)/(mid,uid,sid,pid)/(other).
// p : struct mm_packet*
MM_EXPORT_DLL void mm_packet_head_base_encode(struct mm_packet* p)
{
	mm_memcpy(p->hbuff.buffer + p->hbuff.offset,&p->phead,p->hbuff.length);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_packet_copy_realloc(struct mm_packet* p,struct mm_packet* t)
{
	assert(NULL != t->hbuff.buffer && "t->hbuff.buffer is null.");
	assert(NULL != t->bbuff.buffer && "t->bbuff.buffer is null.");
	t->hbuff.buffer = (mm_uint8_t*)mm_realloc(t->hbuff.buffer,p->hbuff.length);
	t->bbuff.buffer = (mm_uint8_t*)mm_realloc(t->bbuff.buffer,p->bbuff.length);
	mm_packet_copy_weak(p,t);
}
// copy data from p to t.will alloc the hbuff and bbuff.
MM_EXPORT_DLL void mm_packet_copy_alloc(struct mm_packet* p,struct mm_packet* t)
{
	assert(NULL == t->hbuff.buffer && "t->hbuff.buffer is not null.");
	assert(NULL == t->bbuff.buffer && "t->bbuff.buffer is not null.");
	t->hbuff.buffer = (mm_uint8_t*)mm_malloc(p->hbuff.length);
	t->bbuff.buffer = (mm_uint8_t*)mm_malloc(p->bbuff.length);
	mm_packet_copy_weak(p,t);
}
MM_EXPORT_DLL void mm_packet_copy_weak(struct mm_packet* p,struct mm_packet* t)
{
	t->hbuff.length = p->hbuff.length;
	t->bbuff.length = p->bbuff.length;
	mm_memcpy(t->hbuff.buffer + t->hbuff.offset,p->hbuff.buffer + p->hbuff.offset,p->hbuff.length);
	mm_memcpy(t->bbuff.buffer + t->bbuff.offset,p->bbuff.buffer + p->bbuff.offset,p->bbuff.length);
	mm_packet_head_base_decode(t);
}
// free copy realloc data from.
MM_EXPORT_DLL void mm_packet_free_copy_realloc(struct mm_packet* p)
{
	mm_free(p->hbuff.buffer);
	mm_free(p->bbuff.buffer);
	mm_packet_reset(p);
}
// free copy alloc data from.
MM_EXPORT_DLL void mm_packet_free_copy_alloc(struct mm_packet* p)
{
	mm_free(p->hbuff.buffer);
	mm_free(p->bbuff.buffer);
	mm_packet_reset(p);
}
MM_EXPORT_DLL void mm_packet_shadow_alloc(struct mm_packet* p)
{
	assert(NULL == p->hbuff.buffer && "p->hbuff.buffer is not null.");
	assert(NULL == p->bbuff.buffer && "p->bbuff.buffer is not null.");
	p->hbuff.buffer = (mm_uint8_t*)mm_malloc(p->hbuff.length);
	p->bbuff.buffer = (mm_uint8_t*)mm_malloc(p->bbuff.length);
}
MM_EXPORT_DLL void mm_packet_shadow_free(struct mm_packet* p)
{
	mm_free(p->hbuff.buffer);
	mm_free(p->bbuff.buffer);
}
