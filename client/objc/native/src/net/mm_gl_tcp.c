#include "net/mm_gl_tcp.h"
#include "net/mm_streambuf_packet.h"
#include "core/mm_alloc.h"
#include "core/mm_logger.h"
//////////////////////////////////////////////////////////////////////////
static void __static_gl_tcp_tcp_handle_callback(void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length);
static void __static_gl_tcp_tcp_broken_callback(void* obj);

static void __static_gl_tcp_tcp_handle_packet_callback(void* obj,struct mm_packet* pack);

static void __static_gl_tcp_net_tcp_handle_callback(void* obj, void* u, struct mm_packet* pack);
static void __static_gl_tcp_net_tcp_broken_callback(void* obj);
static void __static_gl_tcp_net_tcp_nready_callback(void* obj);
static void __static_gl_tcp_net_tcp_finish_callback(void* obj);

static int __static_gl_tcp_flush_signal_task_factor(void* obj);
static int __static_gl_tcp_flush_signal_task_handle(void* obj);
static int __static_gl_tcp_state_signal_task_factor(void* obj);
static int __static_gl_tcp_state_signal_task_handle(void* obj);
//////////////////////////////////////////////////////////////////////////
static void __static_gl_tcp_handle(void* obj, void* u, struct mm_packet* pack)
{

}
MM_EXPORT_DLL void mm_gl_tcp_callback_init(struct mm_gl_tcp_callback* p)
{
	p->handle = &__static_gl_tcp_handle;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_gl_tcp_callback_destroy(struct mm_gl_tcp_callback* p)
{
	p->handle = &__static_gl_tcp_handle;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_tcp_init(struct mm_gl_tcp* p)
{
	struct mm_tcp_callback tcp_callback;
	struct mm_net_tcp_callback net_tcp_callback;
	struct mm_signal_task_callback flush_task_callback;
	struct mm_signal_task_callback state_task_callback;

	mm_sockaddr_storage_init(&p->ss_native);
	mm_sockaddr_storage_init(&p->ss_remote);
	mm_net_tcp_init(&p->net_tcp);
	mm_mq_tcp_init(&p->queue_tcp);
	mm_gl_tcp_callback_init(&p->callback);
	mm_signal_task_init(&p->flush_task);
	mm_signal_task_init(&p->state_task);
	mm_spin_init(&p->locker, NULL);
	p->state_check_flag = gl_tcp_check_inactive;
	p->push_to_queue_flag = gl_tcp_ptq_activate;
	p->state = ts_closed;
	p->u = NULL;

	tcp_callback.handle = &__static_gl_tcp_tcp_handle_callback;
	tcp_callback.broken = &__static_gl_tcp_tcp_broken_callback;
	tcp_callback.obj = p;
	mm_tcp_assign_callback(&p->net_tcp.tcp,&tcp_callback);

	net_tcp_callback.handle = &__static_gl_tcp_net_tcp_handle_callback;
	net_tcp_callback.broken = &__static_gl_tcp_net_tcp_broken_callback;
	net_tcp_callback.nready = &__static_gl_tcp_net_tcp_nready_callback;
	net_tcp_callback.finish = &__static_gl_tcp_net_tcp_finish_callback;
	net_tcp_callback.obj = p;
	mm_net_tcp_assign_net_tcp_callback(&p->net_tcp,&net_tcp_callback);

	flush_task_callback.factor = &__static_gl_tcp_flush_signal_task_factor;
	flush_task_callback.handle = &__static_gl_tcp_flush_signal_task_handle;
	flush_task_callback.obj = p;
	mm_signal_task_assign_callback(&p->flush_task,&flush_task_callback);

	state_task_callback.factor = &__static_gl_tcp_state_signal_task_factor;
	state_task_callback.handle = &__static_gl_tcp_state_signal_task_handle;
	state_task_callback.obj = p;
	mm_signal_task_assign_callback(&p->state_task,&state_task_callback);

	mm_signal_task_assign_success_nearby(&p->flush_task,MM_GL_TCP_SEND_INTERVAL_HANDLE_TIME);
	mm_signal_task_assign_failure_nearby(&p->flush_task,MM_GL_TCP_SEND_INTERVAL_BROKEN_TIME);
	mm_signal_task_assign_success_nearby(&p->state_task,MM_GL_TCP_HANDLE_MSEC_CHECK);
	mm_signal_task_assign_failure_nearby(&p->state_task,MM_GL_TCP_BROKEN_MSEC_CHECK);
}
MM_EXPORT_DLL void mm_gl_tcp_destroy(struct mm_gl_tcp* p)
{
	mm_gl_tcp_clear_callback_holder(p);
	mm_sockaddr_storage_destroy(&p->ss_native);
	mm_sockaddr_storage_destroy(&p->ss_remote);
	mm_net_tcp_destroy(&p->net_tcp);
	mm_mq_tcp_destroy(&p->queue_tcp);
	mm_gl_tcp_callback_destroy(&p->callback);
	mm_signal_task_destroy(&p->flush_task);
	mm_signal_task_destroy(&p->state_task);
	mm_spin_destroy(&p->locker);
	p->state_check_flag = gl_tcp_check_inactive;
	p->push_to_queue_flag = gl_tcp_ptq_activate;
	p->state = ts_closed;
	p->u = NULL;
}
//////////////////////////////////////////////////////////////////////////
// assign native address but not connect.
MM_EXPORT_DLL void mm_gl_tcp_assign_native(struct mm_gl_tcp* p,const char* node,mm_ushort_t port)
{
	mm_net_tcp_assign_native(&p->net_tcp,node,port);
}
// assign remote address but not connect.
MM_EXPORT_DLL void mm_gl_tcp_assign_remote(struct mm_gl_tcp* p,const char* node,mm_ushort_t port)
{
	mm_net_tcp_assign_remote(&p->net_tcp,node,port);
}
// assign native address but not connect.target value not current real.will apply at state thread.
MM_EXPORT_DLL void mm_gl_tcp_assign_native_target(struct mm_gl_tcp* p,const char* node,mm_ushort_t port)
{
	mm_sockaddr_storage_assign(&p->ss_native, node, port);
	p->ss_remote.ss_family = p->ss_native.ss_family;
}
// assign remote address but not connect.target value not current real..will apply at state thread.
MM_EXPORT_DLL void mm_gl_tcp_assign_remote_target(struct mm_gl_tcp* p,const char* node,mm_ushort_t port)
{
	mm_sockaddr_storage_assign(&p->ss_remote, node, port);
	p->ss_native.ss_family = p->ss_remote.ss_family;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_tcp_assign_q_default_callback(struct mm_gl_tcp* p,struct mm_mq_tcp_callback* mq_tcp_callback)
{
	mm_mq_tcp_assign_queue_tcp_callback(&p->queue_tcp,mq_tcp_callback);
}
MM_EXPORT_DLL void mm_gl_tcp_assign_n_default_callback(struct mm_gl_tcp* p,struct mm_gl_tcp_callback* gl_tcp_callback)
{
	assert(NULL != gl_tcp_callback && "you can not assign null gl_tcp_callback.");
	p->callback = *gl_tcp_callback;
}
// assign queue callback.at main thread.
MM_EXPORT_DLL void mm_gl_tcp_assign_q_callback(struct mm_gl_tcp* p,mm_uint32_t id,net_tcp_handle callback)
{
	mm_mq_tcp_assign_callback(&p->queue_tcp,id,callback);
}
// assign net callback.not at main thread.
MM_EXPORT_DLL void mm_gl_tcp_assign_n_callback(struct mm_gl_tcp* p,mm_uint32_t id,net_tcp_handle callback)
{
	mm_net_tcp_assign_callback(&p->net_tcp,id,callback);
}
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_gl_tcp_assign_event_tcp_alloc(struct mm_gl_tcp* p,struct mm_net_tcp_event_tcp_alloc* event_tcp_alloc)
{
	mm_net_tcp_assign_event_tcp_alloc(&p->net_tcp,event_tcp_alloc);
}
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_gl_tcp_assign_crypto_callback(struct mm_gl_tcp* p,struct mm_crypto_callback* crypto_callback)
{
	mm_net_tcp_assign_crypto_callback(&p->net_tcp,crypto_callback);
}
// assign context handle.
MM_EXPORT_DLL void mm_gl_tcp_assign_context(struct mm_gl_tcp* p,void* u)
{
	p->u = u;
	mm_mq_tcp_assign_context(&p->queue_tcp,p->u);
	mm_net_tcp_assign_context(&p->net_tcp,p->u);
}
//////////////////////////////////////////////////////////////////////////
// context for tcp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_DLL void mm_gl_tcp_set_addr_context(struct mm_gl_tcp* p, void* u)
{
	mm_net_tcp_set_addr_context(&p->net_tcp,u);
}
MM_EXPORT_DLL void* mm_gl_tcp_get_addr_context(struct mm_gl_tcp* p)
{
	return mm_net_tcp_get_addr_context(&p->net_tcp);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_tcp_clear_callback_holder(struct mm_gl_tcp* p)
{
	mm_mq_tcp_clear_callback_holder(&p->queue_tcp);
	mm_net_tcp_clear_callback_holder(&p->net_tcp);
}
//////////////////////////////////////////////////////////////////////////
// gl_tcp crypto encrypt buffer by tcp.
MM_EXPORT_DLL void mm_gl_tcp_crypto_encrypt(struct mm_gl_tcp* p,struct mm_tcp* tcp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length)
{
	mm_net_tcp_crypto_encrypt(&p->net_tcp,tcp,buffer,offset,length);
}
// gl_tcp crypto decrypt buffer by tcp.
MM_EXPORT_DLL void mm_gl_tcp_crypto_decrypt(struct mm_gl_tcp* p,struct mm_tcp* tcp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length)
{
	mm_net_tcp_crypto_decrypt(&p->net_tcp,tcp,buffer,offset,length);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_tcp_assign_flush_handle_nearby(struct mm_gl_tcp* p,mm_msec_t milliseconds)
{
	mm_signal_task_assign_success_nearby(&p->flush_task,milliseconds);
}
MM_EXPORT_DLL void mm_gl_tcp_assign_flush_broken_nearby(struct mm_gl_tcp* p,mm_msec_t milliseconds)
{
	mm_signal_task_assign_failure_nearby(&p->flush_task,milliseconds);
}
MM_EXPORT_DLL void mm_gl_tcp_assign_state_handle_nearby(struct mm_gl_tcp* p,mm_msec_t milliseconds)
{
	mm_signal_task_assign_success_nearby(&p->state_task,milliseconds);
}
MM_EXPORT_DLL void mm_gl_tcp_assign_state_broken_nearby(struct mm_gl_tcp* p,mm_msec_t milliseconds)
{
	mm_signal_task_assign_failure_nearby(&p->state_task,milliseconds);
}
MM_EXPORT_DLL void mm_gl_tcp_assign_state_check_flag(struct mm_gl_tcp* p,mm_uint8_t flag)
{
	p->state_check_flag = flag;
}
MM_EXPORT_DLL void mm_gl_tcp_assign_push_to_queue_flag(struct mm_gl_tcp* p,mm_uint8_t flag)
{
	p->push_to_queue_flag = flag;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_tcp_assign_max_poper_number(struct mm_gl_tcp* p,size_t max_pop)
{
	mm_mq_tcp_assign_max_poper_number(&p->queue_tcp,max_pop);
}
//////////////////////////////////////////////////////////////////////////
// lock to make sure the mm_gl_tcp is thread safe.
MM_EXPORT_DLL void mm_gl_tcp_lock(struct mm_gl_tcp* p)
{
	mm_spin_lock(&p->locker);
}
// unlock net_tcp.
MM_EXPORT_DLL void mm_gl_tcp_unlock(struct mm_gl_tcp* p)
{
	mm_spin_unlock(&p->locker);
}
// lock the flush send tcp buffer.
MM_EXPORT_DLL void mm_gl_tcp_flush_lock(struct mm_gl_tcp* p)
{
	mm_tcp_lock(&p->net_tcp.tcp);
}
// unlock the flush send tcp buffer.
MM_EXPORT_DLL void mm_gl_tcp_flush_unlock(struct mm_gl_tcp* p)
{
	mm_tcp_unlock(&p->net_tcp.tcp);
}
//////////////////////////////////////////////////////////////////////////
// fopen socket.
MM_EXPORT_DLL void mm_gl_tcp_fopen_socket(struct mm_gl_tcp* p)
{
	mm_net_tcp_fopen_socket(&p->net_tcp);
}
// synchronize connect.can call this as we already open connect check thread.
MM_EXPORT_DLL void mm_gl_tcp_connect(struct mm_gl_tcp* p)
{
	mm_net_tcp_connect(&p->net_tcp);
}
// close socket.
MM_EXPORT_DLL void mm_gl_tcp_close_socket(struct mm_gl_tcp* p)
{
	mm_net_tcp_close_socket(&p->net_tcp);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_tcp_check(struct mm_gl_tcp* p)
{
	mm_net_tcp_check(&p->net_tcp);
}
// get state finally.return 0 for all regular.not thread safe.
MM_EXPORT_DLL int mm_gl_tcp_finally_state(struct mm_gl_tcp* p)
{
	return mm_net_tcp_finally_state(&p->net_tcp);
}
//////////////////////////////////////////////////////////////////////////
// synchronize attach to socket.not thread safe.
MM_EXPORT_DLL void mm_gl_tcp_attach_socket(struct mm_gl_tcp* p)
{
	mm_net_tcp_attach_socket(&p->net_tcp);
}
// synchronize detach to socket.not thread safe.
MM_EXPORT_DLL void mm_gl_tcp_detach_socket(struct mm_gl_tcp* p)
{
	mm_net_tcp_detach_socket(&p->net_tcp);
}
// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_DLL void mm_gl_tcp_thread_handle(struct mm_gl_tcp* p)
{
	mm_mq_tcp_thread_handle(&p->queue_tcp);
}
MM_EXPORT_DLL void mm_gl_tcp_push(struct mm_gl_tcp* p, void* obj, struct mm_packet* pack)
{
	mm_mq_tcp_push(&p->queue_tcp, obj, pack);
}
//////////////////////////////////////////////////////////////////////////
// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_gl_tcp_buffer_send(struct mm_gl_tcp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length)
{
	return mm_net_tcp_buffer_send(&p->net_tcp,buffer,offset,length);
}
// handle send data by flush send buffer.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_gl_tcp_flush_send(struct mm_gl_tcp* p)
{
	return mm_net_tcp_flush_send(&p->net_tcp);
}
//////////////////////////////////////////////////////////////////////////
// use for trigger the flush send thread signal.
// is useful for main thread asynchronous send message.
MM_EXPORT_DLL void mm_gl_tcp_flush_signal(struct mm_gl_tcp* p)
{
	mm_signal_task_signal(&p->flush_task);
}
// use for trigger the state check thread signal.
// is useful for main thread asynchronous state check.
MM_EXPORT_DLL void mm_gl_tcp_state_signal(struct mm_gl_tcp* p)
{
	mm_signal_task_signal(&p->state_task);
}
//////////////////////////////////////////////////////////////////////////
// start wait thread.
MM_EXPORT_DLL void mm_gl_tcp_start(struct mm_gl_tcp* p)
{
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	mm_net_tcp_start(&p->net_tcp);
	mm_signal_task_start(&p->flush_task);
	mm_signal_task_start(&p->state_task);
}
// interrupt wait thread.
MM_EXPORT_DLL void mm_gl_tcp_interrupt(struct mm_gl_tcp* p)
{
	p->state = ts_closed;
	mm_net_tcp_interrupt(&p->net_tcp);
	// 
	mm_mq_tcp_cond_not_null(&p->queue_tcp);
	mm_mq_tcp_cond_not_full(&p->queue_tcp);
	// signal the thread.
	mm_signal_task_interrupt(&p->flush_task);
	mm_signal_task_interrupt(&p->state_task);
}
// shutdown wait thread.
MM_EXPORT_DLL void mm_gl_tcp_shutdown(struct mm_gl_tcp* p)
{
	p->state = ts_finish;
	mm_net_tcp_shutdown(&p->net_tcp);
	// 
	mm_mq_tcp_cond_not_null(&p->queue_tcp);
	mm_mq_tcp_cond_not_full(&p->queue_tcp);
	// signal the thread.
	mm_signal_task_shutdown(&p->flush_task);
	mm_signal_task_shutdown(&p->state_task);
}
// join wait thread.
MM_EXPORT_DLL void mm_gl_tcp_join(struct mm_gl_tcp* p)
{
	mm_net_tcp_join(&p->net_tcp);
	// signal the thread.
	mm_signal_task_join(&p->flush_task);
	mm_signal_task_join(&p->state_task);
}
//////////////////////////////////////////////////////////////////////////
static void __static_gl_tcp_tcp_handle_callback(void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length)
{
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_gl_tcp* gl_tcp = (struct mm_gl_tcp*)(tcp->callback.obj);
	mm_net_tcp_crypto_decrypt(&gl_tcp->net_tcp,tcp,buffer,offset,length);
	mm_streambuf_packet_handle_tcp(&tcp->buff_recv, buffer, offset, length, &__static_gl_tcp_tcp_handle_packet_callback, tcp);
}
static void __static_gl_tcp_tcp_broken_callback(void* obj)
{
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_gl_tcp* gl_tcp = (struct mm_gl_tcp*)(tcp->callback.obj);
	struct mm_net_tcp* net_tcp = &gl_tcp->net_tcp;
	assert(NULL != net_tcp->callback.broken && "net_tcp->callback.broken is a null.");
	mm_net_tcp_apply_broken(net_tcp);
}
static void __static_gl_tcp_tcp_handle_packet_callback(void* obj,struct mm_packet* pack)
{
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_gl_tcp* gl_tcp = (struct mm_gl_tcp*)(tcp->callback.obj);
	struct mm_net_tcp* net_tcp = &gl_tcp->net_tcp;
	assert(NULL != net_tcp->callback.handle && "net_tcp->callback.handle is a null.");
	(*(net_tcp->callback.handle))(tcp,gl_tcp->u,pack);
}
static void __static_gl_tcp_net_tcp_handle_callback(void* obj, void* u, struct mm_packet* pack)
{
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_gl_tcp* gl_tcp = (struct mm_gl_tcp*)(tcp->callback.obj);
	struct mm_net_tcp* net_tcp = &gl_tcp->net_tcp;
	net_tcp_handle handle = NULL;
	mm_spin_lock(&net_tcp->holder_locker);
	handle = (net_tcp_handle)mm_holder_u32_vpt_get(&net_tcp->holder,pack->phead.mid);
	mm_spin_unlock(&net_tcp->holder_locker);
	if (NULL != handle)
	{
		// fire the handle event.
		(*(handle))(tcp,u,pack);
	}
	else
	{
		assert(NULL != gl_tcp->callback.handle && "gl_tcp->callback.handle is a null.");
		(*(gl_tcp->callback.handle))(tcp,u,pack);
	}
	// push to queue thread.
	if (gl_tcp_ptq_activate == gl_tcp->push_to_queue_flag)
	{
		mm_gl_tcp_push(gl_tcp, obj, pack);
	}
}
static void __static_gl_tcp_net_tcp_broken_callback(void* obj)
{
	struct mm_packet pack;
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_gl_tcp* gl_tcp = (struct mm_gl_tcp*)(tcp->callback.obj);
	// push to queue thread.
	mm_packet_init(&pack);
	mm_packet_head_base_zero(&pack);
	pack.hbuff.length = MM_MSG_COMM_HEAD_SIZE;
	pack.bbuff.length = 0;
	mm_packet_shadow_alloc(&pack);
	pack.phead.mid = tcp_mid_broken;
    mm_packet_head_base_encode(&pack);
	__static_gl_tcp_net_tcp_handle_callback(obj,gl_tcp->u,&pack);
	mm_packet_shadow_free(&pack);
	mm_packet_destroy(&pack);
	// net state signal.
	mm_gl_tcp_state_signal(gl_tcp);
}
static void __static_gl_tcp_net_tcp_nready_callback(void* obj)
{
	struct mm_packet pack;
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_gl_tcp* gl_tcp = (struct mm_gl_tcp*)(tcp->callback.obj);
	// push to queue thread.
	mm_packet_init(&pack);
	mm_packet_head_base_zero(&pack);
	pack.hbuff.length = MM_MSG_COMM_HEAD_SIZE;
	pack.bbuff.length = 0;
	mm_packet_shadow_alloc(&pack);
	pack.phead.mid = tcp_mid_nready;
    mm_packet_head_base_encode(&pack);
	__static_gl_tcp_net_tcp_handle_callback(obj,gl_tcp->u,&pack);
	mm_packet_shadow_free(&pack);
	mm_packet_destroy(&pack);
	// net state signal.
	mm_gl_tcp_state_signal(gl_tcp);
}
static void __static_gl_tcp_net_tcp_finish_callback(void* obj)
{
	struct mm_packet pack;
	struct mm_tcp* tcp = (struct mm_tcp*)(obj);
	struct mm_gl_tcp* gl_tcp = (struct mm_gl_tcp*)(tcp->callback.obj);
	// push to queue thread.
	mm_packet_init(&pack);
	mm_packet_head_base_zero(&pack);
	pack.hbuff.length = MM_MSG_COMM_HEAD_SIZE;
	pack.bbuff.length = 0;
	mm_packet_shadow_alloc(&pack);
	pack.phead.mid = tcp_mid_finish;
    mm_packet_head_base_encode(&pack);
	__static_gl_tcp_net_tcp_handle_callback(obj,gl_tcp->u,&pack);
	mm_packet_shadow_free(&pack);
	mm_packet_destroy(&pack);
	// net state signal.
	mm_gl_tcp_state_signal(gl_tcp);
}
static int __static_gl_tcp_flush_signal_task_factor(void* obj)
{
	int code = -1;
	struct mm_signal_task* signal_task = (struct mm_signal_task*)(obj);
	struct mm_gl_tcp* gl_tcp = (struct mm_gl_tcp*)(signal_task->callback.obj);
	struct mm_tcp* tcp = &gl_tcp->net_tcp.tcp;
	size_t buff_len = 0;
	mm_tcp_lock(tcp);
	buff_len = mm_streambuf_size(&tcp->buff_send);
	mm_tcp_unlock(tcp);
	code = ( 0 == buff_len ? 0 : -1 );
	return code;
}
static int __static_gl_tcp_flush_signal_task_handle(void* obj)
{
	int code = -1;
	struct mm_signal_task* signal_task = (struct mm_signal_task*)(obj);
	struct mm_gl_tcp* gl_tcp = (struct mm_gl_tcp*)(signal_task->callback.obj);
	struct mm_tcp* tcp = &gl_tcp->net_tcp.tcp;
	int real_len = 0;
	// check the state.
	if (0 == mm_gl_tcp_finally_state(gl_tcp))
	{
		// handle send data by flush send buffer.
		// 0 <  rt,means rt buffer is send,we must gbump rt size.
		// 0 == rt,means the send buffer can be full.
		// 0 >  rt,means the send process can be failure.
		mm_tcp_lock(tcp);
		real_len = mm_tcp_flush_send(tcp);
		mm_tcp_unlock(tcp);
		if ( 0 > real_len )
		{
			// the socket is broken.
			// but the send thread can not broken.
			// it is wait the reconnect and send.
			code = -1;
		}
		else
		{
			// the finally state is regular.
			// if 0 == real_len.the state is broken or the socket send buffer is full.
			// just think it is regular.
			code = 0;
		}
	}
	else
	{
		// the finally state is broken.
		// wake the state check process.
		mm_gl_tcp_state_signal(gl_tcp);
		code = -1;
	}
	return code;
}
static int __static_gl_tcp_state_signal_task_factor(void* obj)
{
	int code = -1;
	struct mm_signal_task* signal_task = (struct mm_signal_task*)(obj);
	struct mm_gl_tcp* gl_tcp = (struct mm_gl_tcp*)(signal_task->callback.obj);
	struct mm_tcp* tcp = &gl_tcp->net_tcp.tcp;
	struct mm_addr* addr = &tcp->addr;
	code = (
		0 == mm_sockaddr_storage_compare(&addr->ss_native,&gl_tcp->ss_native) && 
		0 == mm_sockaddr_storage_compare(&addr->ss_remote,&gl_tcp->ss_remote) && 
		0 == mm_gl_tcp_finally_state(gl_tcp) ) ? 0 : -1;
	return code;
}
static int __static_gl_tcp_state_signal_task_handle(void* obj)
{
	int code = -1;
	struct mm_signal_task* signal_task = (struct mm_signal_task*)(obj);
	struct mm_gl_tcp* gl_tcp = (struct mm_gl_tcp*)(signal_task->callback.obj);
	if (gl_tcp_check_activate == gl_tcp->state_check_flag)
	{
		struct mm_net_tcp* net_tcp = &gl_tcp->net_tcp;
		struct mm_tcp* tcp = &gl_tcp->net_tcp.tcp;
		struct mm_addr* addr = &tcp->addr;
		if ( 
			0 != mm_sockaddr_storage_compare(&addr->ss_native,&gl_tcp->ss_native) || 
			0 != mm_sockaddr_storage_compare(&addr->ss_remote,&gl_tcp->ss_remote) )
		{
			// if target address is not the real address,we disconnect current socket.
			mm_gl_tcp_lock(gl_tcp);
			// first disconnect current socket.
			mm_net_tcp_detach_socket(net_tcp);
			// assign the target address.
			mm_addr_assign_native_storage(addr,&gl_tcp->ss_native);
			mm_addr_assign_remote_storage(addr,&gl_tcp->ss_remote);
			mm_gl_tcp_unlock(gl_tcp);
		}
		if ( 
			0 == mm_sockaddr_storage_compare_address(&addr->ss_native,MM_ADDR_DEFAULT_NODE,MM_ADDR_DEFAULT_PORT) && 
			0 == mm_sockaddr_storage_compare_address(&addr->ss_remote,MM_ADDR_DEFAULT_NODE,MM_ADDR_DEFAULT_PORT)) 
		{
			// if the native and remote is empty.fire the broken event immediately.
			mm_gl_tcp_lock(gl_tcp);
			mm_net_tcp_apply_broken(net_tcp);
			mm_gl_tcp_unlock(gl_tcp);
		}
		else
		{
			mm_gl_tcp_lock(gl_tcp);
			mm_gl_tcp_check(gl_tcp);
			mm_gl_tcp_unlock(gl_tcp);
		}
	}
	code = ( 0 == mm_gl_tcp_finally_state(gl_tcp) ) ? 0 : -1;
	return code;
}
