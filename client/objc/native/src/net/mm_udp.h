#ifndef __mm_udp_h__
#define __mm_udp_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_streambuf.h"
#include "core/mm_socket.h"

#include "net/mm_addr.h"

enum mm_udp_mid
{
	udp_mid_broken = 0x0F000010,
	udp_mid_nready = 0x0F000011,
	udp_mid_finish = 0x0F000012,
};

//
// obj is mm_udp.
typedef void (*udp_handle)(void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote);
typedef void (*udp_broken)(void* obj);
//
struct mm_udp_callback
{
	//
	udp_handle handle;
	udp_broken broken;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_udp_callback_init(struct mm_udp_callback* p);
MM_EXPORT_DLL void mm_udp_callback_destroy(struct mm_udp_callback* p);
//
struct mm_udp
{
	struct mm_addr addr;// strong ref. udp address. 
	struct mm_streambuf buff_recv;// strong ref.
	struct mm_streambuf buff_send;// strong ref.
	struct mm_udp_callback callback;// value ref. transport callback.
	mm_uint64_t unique_id;// feedback unique_id.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_udp_init(struct mm_udp* p);
MM_EXPORT_DLL void mm_udp_destroy(struct mm_udp* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_udp_lock(struct mm_udp* p);
MM_EXPORT_DLL void mm_udp_unlock(struct mm_udp* p);
//////////////////////////////////////////////////////////////////////////
// assign addr native by ip port.
MM_EXPORT_DLL void mm_udp_assign_native(struct mm_udp* p, const char* node, mm_ushort_t port);
// assign addr remote by ip port.
MM_EXPORT_DLL void mm_udp_assign_remote(struct mm_udp* p, const char* node, mm_ushort_t port);
// assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
MM_EXPORT_DLL void mm_udp_assign_native_storage(struct mm_udp* p, struct sockaddr_storage* ss_native);
// assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
MM_EXPORT_DLL void mm_udp_assign_remote_storage(struct mm_udp* p, struct sockaddr_storage* ss_remote);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_udp_assign_callback(struct mm_udp* p,struct mm_udp_callback* cb);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_udp_set_unique_id(struct mm_udp* p, mm_uint64_t unique_id);
MM_EXPORT_DLL mm_uint64_t mm_udp_get_unique_id(struct mm_udp* p);
// context for udp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_DLL void mm_udp_set_context(struct mm_udp* p, void* u);
MM_EXPORT_DLL void* mm_udp_get_context(struct mm_udp* p);
//////////////////////////////////////////////////////////////////////////
// fopen socket.ss_native.ss_family,SOCK_DGRAM,0
MM_EXPORT_DLL void mm_udp_fopen_socket(struct mm_udp* p);
// close socket.
MM_EXPORT_DLL void mm_udp_close_socket(struct mm_udp* p);
// shutdown socket.
MM_EXPORT_DLL void mm_udp_shutdown_socket(struct mm_udp* p, int opcode);
//////////////////////////////////////////////////////////////////////////
// handle recv for buffer pool and pool max size.
MM_EXPORT_DLL void mm_udp_handle_recv(struct mm_udp* p, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length);
// handle send for buffer pool and pool max size.
MM_EXPORT_DLL void mm_udp_handle_send(struct mm_udp* p, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length);
//////////////////////////////////////////////////////////////////////////
// handle recv data from buffer and buffer length.0 success -1 failure.
MM_EXPORT_DLL int mm_udp_buffer_recv(struct mm_udp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote);
// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_udp_buffer_send(struct mm_udp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote);
//////////////////////////////////////////////////////////////////////////
// handle send data by flush send buffer.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_udp_flush_send(struct mm_udp* p);
//////////////////////////////////////////////////////////////////////////
// udp streambuf recv send reset.
MM_EXPORT_DLL void mm_udp_streambuf_reset(struct mm_udp* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_udp_h__