#ifndef __mm_mq_tcp_h__
#define __mm_mq_tcp_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_time.h"
#include "core/mm_thread.h"

#include "net/mm_packet.h"
#include "net/mm_net_tcp.h"
#include "net/mm_net_udp.h"

#include "container/mm_lock_queue.h"
#include "container/mm_holder32.h"

// max poper number at one loop.the default main thread timer frequency is 1/20 = 50 ms.
// 2ms one message handle is suitable 50/2 = 25.
#define MM_MQ_TCP_MAX_POPER_NUMBER 25

//////////////////////////////////////////////////////////////////////////
struct mm_mq_tcp_data
{
	void* obj;// weak ref for tcp handle callback.it is mm_tcp.
	struct mm_packet pack;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_tcp_data_init(struct mm_mq_tcp_data* p);
MM_EXPORT_DLL void mm_mq_tcp_data_destroy(struct mm_mq_tcp_data* p);
//////////////////////////////////////////////////////////////////////////
typedef void (*mm_mq_tcp_handle)(void* obj, void* u, struct mm_packet* pack);
struct mm_mq_tcp_callback
{
	mm_mq_tcp_handle handle;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_mq_tcp_callback_init(struct mm_mq_tcp_callback* p);
MM_EXPORT_DLL void mm_mq_tcp_callback_destroy(struct mm_mq_tcp_callback* p);
//////////////////////////////////////////////////////////////////////////
// message queue for tcp.
struct mm_mq_tcp
{
	struct mm_lock_queue lock_queue;
	struct mm_holder_u32_vpt holder;// rb tree for msg_id <--> callback.
	struct mm_mq_tcp_callback callback;// value ref. queue tcp callback.
	mm_atomic_t holder_locker;
	mm_atomic_t locker;
	size_t max_pop;// max pop to make sure current not wait long times.default is MM_MQ_TCP_MAX_POPER_NUMBER. 
	void* u;// user data.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_tcp_init(struct mm_mq_tcp* p);
MM_EXPORT_DLL void mm_mq_tcp_destroy(struct mm_mq_tcp* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_tcp_lock(struct mm_mq_tcp* p);
MM_EXPORT_DLL void mm_mq_tcp_unlock(struct mm_mq_tcp* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_tcp_assign_callback(struct mm_mq_tcp* p,mm_uint32_t id,net_tcp_handle callback);
MM_EXPORT_DLL void mm_mq_tcp_assign_queue_tcp_callback(struct mm_mq_tcp* p,struct mm_mq_tcp_callback* queue_tcp_callback);
MM_EXPORT_DLL void mm_mq_tcp_assign_max_poper_number(struct mm_mq_tcp* p,size_t max_pop);
// assign context handle.
MM_EXPORT_DLL void mm_mq_tcp_assign_context(struct mm_mq_tcp* p,void* u);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_tcp_clear_callback_holder(struct mm_mq_tcp* p);
//////////////////////////////////////////////////////////////////////////
// you can interrupt block state.but will make the size error.
// other way is push a NULL.
MM_EXPORT_DLL void mm_mq_tcp_cond_not_null(struct mm_mq_tcp* p);
MM_EXPORT_DLL void mm_mq_tcp_cond_not_full(struct mm_mq_tcp* p);
//////////////////////////////////////////////////////////////////////////
// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_DLL void mm_mq_tcp_thread_handle(struct mm_mq_tcp* p);
// delete all not handle pack at queue.
MM_EXPORT_DLL void mm_mq_tcp_dispose(struct mm_mq_tcp* p);
// push a pack and context tp queue.this function will copy alloc pack.
MM_EXPORT_DLL void mm_mq_tcp_push(struct mm_mq_tcp* p, void* obj, struct mm_packet* pack);
// pop and trigger handle.
MM_EXPORT_DLL void mm_mq_tcp_pop(struct mm_mq_tcp* p);
//////////////////////////////////////////////////////////////////////////

#include "core/mm_suffix.h"

#endif//__mm_mq_tcp_h__