#include "net/mm_sockaddr.h"
#include "core/mm_alloc.h"
#include "core/mm_string.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_sockaddr_storage_init( struct sockaddr_storage* p )
{
	mm_memset(p,0,sizeof(struct sockaddr_storage));
	p->ss_family = MM_AF_INET6;
}
MM_EXPORT_DLL void mm_sockaddr_storage_destroy( struct sockaddr_storage* p )
{
	mm_memset(p,0,sizeof(struct sockaddr_storage));
	p->ss_family = MM_AF_INET6;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_sockaddr_storage_copy( struct sockaddr_storage* p, struct sockaddr_storage* t )
{
	mm_memcpy(p,t,sizeof(struct sockaddr_storage));
}
//////////////////////////////////////////////////////////////////////////
// decode by string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_DLL void mm_sockaddr_format_decode_string( char addr_name[MM_ADDR_NAME_LENGTH], char node[MM_NODE_NAME_LENGTH], mm_ushort_t* port )
{
	size_t i = 0;
	struct mm_string cs;
	mm_string_init(&cs);
	mm_string_assigns(&cs,addr_name);
	if ( 0 < cs.l )
	{
		for ( i = cs.l - 1; 0 < i; --i)
		{
			if ( '-' == cs.s[i] )
			{
				cs.s[i] = ' ';
				break;
			}
		}
	}
	mm_sscanf(cs.s,"%s %" SCNu16 "",node,port);
	mm_string_destroy(&cs);
}
// encode to string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_DLL void mm_sockaddr_format_encode_string( char addr_name[MM_ADDR_NAME_LENGTH], char node[MM_NODE_NAME_LENGTH], mm_ushort_t* port )
{
	mm_sprintf(addr_name,"%s-%u",node,*port);
}
//////////////////////////////////////////////////////////////////////////
// family ipv4 ipv6.
MM_EXPORT_DLL int mm_sockaddr_storage_string_family( char addr_name[MM_ADDR_NAME_LENGTH] )
{
	int ss_family = MM_AF_INET6;
	size_t i = 0;
	for ( i = 0; i < MM_ADDR_NAME_LENGTH; ++i )
	{
		if ( '.' == addr_name[i] )
		{
			ss_family = MM_AF_INET4;
			break;
		}
	}
	return ss_family;
}
//////////////////////////////////////////////////////////////////////////
static void __static_sockaddr_storage_unspec_assign( struct sockaddr_storage* addr, const char* node, mm_ushort_t port )
{
	struct addrinfo* addr_info = NULL;
	char port_string[MM_PORT_NAME_LENGTH];
	mm_memset(addr,0,sizeof(struct sockaddr_storage));
	mm_sprintf(port_string,"%u",port);
	if (0 == getaddrinfo(node, port_string, NULL, &addr_info) && NULL != addr_info)
	{
		mm_memcpy(addr,addr_info->ai_addr,addr_info->ai_addrlen);
		freeaddrinfo(addr_info);
	}
}
static void __static_sockaddr_storage_inetv4_assign( struct sockaddr_storage* addr, const char* node, mm_ushort_t port )
{
	struct sockaddr_in* addr_inetv4 = (struct sockaddr_in*)(addr);
	assert(addr->ss_family == MM_AF_INET4 && "addr->ss_family == MM_AF_INET4 is invalid.");
	addr_inetv4->sin_family = addr->ss_family;
	addr_inetv4->sin_port = htons(port);
	inet_pton(MM_AF_INET4, node, &addr_inetv4->sin_addr);
}
static void __static_sockaddr_storage_inetv6_assign( struct sockaddr_storage* addr, const char* node, mm_ushort_t port )
{
	struct sockaddr_in6* addr_inetv6 = (struct sockaddr_in6*)(addr);
	assert(addr->ss_family == MM_AF_INET6 && "addr->ss_family == MM_AF_INET6 is invalid.");
	addr_inetv6->sin6_family = addr->ss_family;
	addr_inetv6->sin6_port = htons(port);
	inet_pton(MM_AF_INET6, node, &addr_inetv6->sin6_addr);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_sockaddr_storage_assign( struct sockaddr_storage* addr, const char* node, mm_ushort_t port )
{
	char addr_name[MM_ADDR_NAME_LENGTH] = {0};
	mm_strcpy(addr_name,node);
	mm_memset(addr,0,sizeof(struct sockaddr_storage));
	addr->ss_family = mm_sockaddr_storage_string_family(addr_name);
	assert(AF_UNSPEC <= addr->ss_family && addr->ss_family <= MM_AF_INET6 && "AF_UNSPEC <= addr->ss_family && addr->ss_family <= MM_AF_INET6 is invalid.");
	switch (addr->ss_family)
	{
	case MM_AF_INET4:
		__static_sockaddr_storage_inetv4_assign(addr,node,port);
		break;
	case MM_AF_INET6:
		__static_sockaddr_storage_inetv6_assign(addr,node,port);
		break;
	default:
		__static_sockaddr_storage_unspec_assign(addr,node,port);
		break;
	}
}
//////////////////////////////////////////////////////////////////////////
static void __static_sockaddr_storage_unspec_string( struct sockaddr_storage* addr, char addr_name[MM_ADDR_NAME_LENGTH] )
{
	char node_string[MM_NODE_NAME_LENGTH] = {0};
	char port_string[MM_PORT_NAME_LENGTH] = {0};
	struct addrinfo addr_info;
	mm_memset(&addr_info,0,sizeof(struct addrinfo));
	addr_info.ai_addrlen = sizeof(struct sockaddr_storage);
	addr_info.ai_addr = (struct sockaddr*)addr;
	getnameinfo(addr_info.ai_addr,addr_info.ai_addrlen,node_string,MM_NODE_NAME_LENGTH,port_string,MM_PORT_NAME_LENGTH,addr_info.ai_flags);
	mm_sprintf(addr_name,"%s-%s",node_string,port_string);
}
static void __static_sockaddr_storage_inetv4_string( struct sockaddr_storage* addr, char addr_name[MM_ADDR_NAME_LENGTH] )
{
	struct sockaddr_in* addr_inetv4 = (struct sockaddr_in*)(addr);
	char node_string[MM_NODE_NAME_LENGTH] = {0};
	assert(addr->ss_family == MM_AF_INET4 && "addr->ss_family == MM_AF_INET4 is invalid.");
	inet_ntop(MM_AF_INET4, &addr_inetv4->sin_addr, node_string, MM_NODE_NAME_LENGTH);
	mm_sprintf(addr_name,"%s-%u",node_string,ntohs(addr_inetv4->sin_port));
}
static void __static_sockaddr_storage_inetv6_string( struct sockaddr_storage* addr, char addr_name[MM_ADDR_NAME_LENGTH] )
{
	struct sockaddr_in6* addr_inetv6 = (struct sockaddr_in6*)(addr);
	char node_string[MM_NODE_NAME_LENGTH] = {0};
	assert(addr->ss_family == MM_AF_INET6 && "addr->ss_family == MM_AF_INET6 is invalid.");
	inet_ntop(MM_AF_INET6, &addr_inetv6->sin6_addr, node_string, MM_NODE_NAME_LENGTH);
	mm_sprintf(addr_name,"%s-%u",node_string,ntohs(addr_inetv6->sin6_port));
}
// 2001:0DB8:0000:0023:0008:0800:200C:417A 64 38
// 65535                                   8  5
// addr_name:
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_DLL void mm_sockaddr_storage_string( struct sockaddr_storage* addr, char addr_name[MM_ADDR_NAME_LENGTH] )
{
	assert(AF_UNSPEC <= addr->ss_family && addr->ss_family <= MM_AF_INET6 && "AF_UNSPEC <= addr->ss_family && addr->ss_family <= MM_AF_INET6 is invalid.");
	switch (addr->ss_family)
	{
	case MM_AF_INET4:
		__static_sockaddr_storage_inetv4_string(addr,addr_name);
		break;
	case MM_AF_INET6:
		__static_sockaddr_storage_inetv6_string(addr,addr_name);
		break;
	default:
		__static_sockaddr_storage_unspec_string(addr,addr_name);
		break;
	}
}
// (fd)|ip-port --> ip-port
// ipv6 (65535)|2001:0DB8:0000:0023:0008:0800:200C:417A-65535 --> 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         (65535)|192.168.111.123-65535 --> 192.168.111.123-65535
MM_EXPORT_DLL void mm_sockaddr_storage_native_remote_string( struct sockaddr_storage* native_addr, struct sockaddr_storage* remote_addr, mm_socket_t fd, char link_name[MM_LINK_NAME_LENGTH] )
{
	char addr_n_name[MM_ADDR_NAME_LENGTH];
	char addr_r_name[MM_ADDR_NAME_LENGTH];
	mm_sockaddr_storage_string(native_addr,addr_n_name);
	mm_sockaddr_storage_string(remote_addr,addr_r_name);
	mm_sprintf(link_name,"(%d)|%s-->%s",fd,addr_n_name,addr_r_name);
}
//////////////////////////////////////////////////////////////////////////
// 0 is same.
MM_EXPORT_DLL int mm_sockaddr_storage_compare( struct sockaddr_storage* addrl, struct sockaddr_storage* addrr )
{
	return mm_memcmp(addrl,addrr,sizeof(struct sockaddr_storage));
}
// 0 is same.
MM_EXPORT_DLL int mm_sockaddr_storage_compare_address( struct sockaddr_storage* addr, const char* node, mm_ushort_t port )
{
	struct sockaddr_storage addrr;
	mm_sockaddr_storage_assign(&addrr,node,port);
	return mm_sockaddr_storage_compare(addr,&addrr);
}
//////////////////////////////////////////////////////////////////////////
// decode by string.
MM_EXPORT_DLL void mm_sockaddr_storage_decode_string( struct sockaddr_storage* addr, char addr_name[MM_ADDR_NAME_LENGTH] )
{
	char node_name[MM_NODE_NAME_LENGTH] = {0};
	mm_ushort_t port = 0;
	mm_sockaddr_format_decode_string(addr_name,node_name,&port);
	mm_sockaddr_storage_assign(addr,node_name,port);
}
// encode to string.
MM_EXPORT_DLL void mm_sockaddr_storage_encode_string( struct sockaddr_storage* addr, char addr_name[MM_ADDR_NAME_LENGTH] )
{
	mm_sockaddr_storage_string(addr,addr_name);
}
//////////////////////////////////////////////////////////////////////////
// decode by string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535 =>      ::-0
// ipv4                         192.168.111.123-65535 => 0.0.0.0-0
MM_EXPORT_DLL void mm_sockaddr_storage_decode_zero_string( struct sockaddr_storage* addr, char addr_name[MM_ADDR_NAME_LENGTH] )
{
	addr->ss_family = mm_sockaddr_storage_string_family(addr_name);
	switch (addr->ss_family)
	{
	case MM_AF_INET4:
		__static_sockaddr_storage_inetv4_assign(addr,"::",0);
		break;
	case MM_AF_INET6:
		__static_sockaddr_storage_inetv6_assign(addr,"0.0.0.0",0);
		break;
	default:
		__static_sockaddr_storage_unspec_assign(addr,"0.0.0.0",0);
		break;
	}
}
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535 =>      ::-0
// ipv4                         192.168.111.123-65535 => 0.0.0.0-0
MM_EXPORT_DLL void mm_sockaddr_storage_decode_zero( struct sockaddr_storage* addr, const char* node, mm_ushort_t port )
{
	char addr_name[MM_ADDR_NAME_LENGTH] = {0};
	mm_strcpy(addr_name,node);
	addr->ss_family = mm_sockaddr_storage_string_family(addr_name);
	switch (addr->ss_family)
	{
	case MM_AF_INET4:
		__static_sockaddr_storage_inetv4_assign(addr,"::",0);
		break;
	case MM_AF_INET6:
		__static_sockaddr_storage_inetv6_assign(addr,"0.0.0.0",0);
		break;
	default:
		__static_sockaddr_storage_unspec_assign(addr,"0.0.0.0",0);
		break;
	}
}
//////////////////////////////////////////////////////////////////////////
// calculate the socket real length for this function.
// connect bind sendto
// for linux windows use the sizeof(struct sockaddr_storage) os not checking and anything correct.
// for freebsd must use the real sockaddr length.if not well case errno(EAFNOSUPPORT) Address family not supported by protocol family.
MM_EXPORT_DLL socklen_t mm_sockaddr_storage_length( struct sockaddr_storage* addr)
{
	socklen_t length = 0;
	switch (addr->ss_family)
	{
	case MM_AF_INET4:
		length = sizeof(struct sockaddr_in);
		break;
	case MM_AF_INET6:
		length = sizeof(struct sockaddr_in6);
		break;
	default:
		length = sizeof(struct sockaddr_storage);
		break;
	}
	return length;
}