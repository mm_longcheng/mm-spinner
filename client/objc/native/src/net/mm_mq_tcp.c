#include "mm_mq_tcp.h"
#include "core/mm_alloc.h"
#include "core/mm_logger.h"
#include "container/mm_list_iterator.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_tcp_data_init(struct mm_mq_tcp_data* p)
{
	p->obj = NULL;
	mm_packet_init(&p->pack);
}
MM_EXPORT_DLL void mm_mq_tcp_data_destroy(struct mm_mq_tcp_data* p)
{
	p->obj = NULL;
	mm_packet_destroy(&p->pack);
}
//////////////////////////////////////////////////////////////////////////
static void __static_mq_tcp_handle(void* obj, void* u, struct mm_packet* pack)
{

}
MM_EXPORT_DLL void mm_mq_tcp_callback_init(struct mm_mq_tcp_callback* p)
{
	p->handle = &__static_mq_tcp_handle;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_mq_tcp_callback_destroy(struct mm_mq_tcp_callback* p)
{
	p->handle = &__static_mq_tcp_handle;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_tcp_init(struct mm_mq_tcp* p)
{
	struct mm_holder_u32_vpt_alloc holder32_alloc;

	mm_lock_queue_init(&p->lock_queue);
	mm_holder_u32_vpt_init(&p->holder);
	mm_mq_tcp_callback_init(&p->callback);
	mm_spin_init(&p->holder_locker,NULL);
	mm_spin_init(&p->locker, NULL);
	p->max_pop = MM_MQ_TCP_MAX_POPER_NUMBER;
	p->u = NULL;

	holder32_alloc.alloc = &mm_holder_u32_vpt_weak_alloc;
	holder32_alloc.relax = &mm_holder_u32_vpt_weak_relax;
	holder32_alloc.obj = p;
	mm_holder_u32_vpt_assign_alloc(&p->holder,&holder32_alloc);
}
MM_EXPORT_DLL void mm_mq_tcp_destroy(struct mm_mq_tcp* p)
{
	mm_mq_tcp_clear_callback_holder(p);
	mm_mq_tcp_dispose(p);
	//
	mm_lock_queue_destroy(&p->lock_queue);
	mm_holder_u32_vpt_destroy(&p->holder);
	mm_mq_tcp_callback_destroy(&p->callback);
	mm_spin_destroy(&p->holder_locker);
	mm_spin_destroy(&p->locker);
	p->max_pop = -1;
	p->u = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_tcp_lock(struct mm_mq_tcp* p)
{
	mm_spin_lock(&p->locker);
}
MM_EXPORT_DLL void mm_mq_tcp_unlock(struct mm_mq_tcp* p)
{
	mm_spin_unlock(&p->locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_tcp_assign_callback(struct mm_mq_tcp* p,mm_uint32_t id,net_tcp_handle callback)
{
	mm_spin_lock(&p->holder_locker);
	mm_holder_u32_vpt_set(&p->holder,id,(void*)callback);
	mm_spin_unlock(&p->holder_locker);
}
MM_EXPORT_DLL void mm_mq_tcp_assign_queue_tcp_callback(struct mm_mq_tcp* p,struct mm_mq_tcp_callback* queue_tcp_callback)
{
	assert(NULL != queue_tcp_callback && "you can not assign null queue_tcp_callback.");
	p->callback = *queue_tcp_callback;
}
MM_EXPORT_DLL void mm_mq_tcp_assign_max_poper_number(struct mm_mq_tcp* p,size_t max_pop)
{
	p->max_pop = max_pop;
}
// assign context handle.
MM_EXPORT_DLL void mm_mq_tcp_assign_context(struct mm_mq_tcp* p,void* u)
{
	p->u = u;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_tcp_clear_callback_holder(struct mm_mq_tcp* p)
{
	mm_spin_lock(&p->holder_locker);
	mm_holder_u32_vpt_clear(&p->holder);
	mm_spin_unlock(&p->holder_locker);
}
//////////////////////////////////////////////////////////////////////////
// you can interrupt block state.but will make the size error.
// other way is push a NULL.
MM_EXPORT_DLL void mm_mq_tcp_cond_not_null(struct mm_mq_tcp* p)
{
	mm_lock_queue_cond_not_null(&p->lock_queue);
}
MM_EXPORT_DLL void mm_mq_tcp_cond_not_full(struct mm_mq_tcp* p)
{
	mm_lock_queue_cond_not_full(&p->lock_queue);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mq_tcp_thread_handle(struct mm_mq_tcp* p)
{
	mm_mq_tcp_pop(p);
}
MM_EXPORT_DLL void mm_mq_tcp_dispose(struct mm_mq_tcp* p)
{
	struct mm_list_head* pos = NULL;
	struct mm_list_vpt_iterator* lvp =NULL;
	struct mm_mq_tcp_data* data = NULL;
	mm_spin_lock(&p->lock_queue.list_locker);
	pos = p->lock_queue.l.next;
	while(pos != &p->lock_queue.l)
	{
		struct mm_list_head* curr = pos;
		pos = pos->next;
		lvp =(struct mm_list_vpt_iterator*)mm_list_entry(curr, struct mm_list_vpt_iterator, n);
		mm_list_del(curr);
		data = (struct mm_mq_tcp_data*)(lvp->v);
		mm_packet_free_copy_alloc(&data->pack);
		mm_mq_tcp_data_destroy(data);
		mm_free(data);
		mm_list_vpt_iterator_destroy(lvp);
		mm_free(lvp);
	}
	mm_spin_unlock(&p->lock_queue.list_locker);
}
MM_EXPORT_DLL void mm_mq_tcp_push(struct mm_mq_tcp* p, void* obj, struct mm_packet* pack)
{
	struct mm_mq_tcp_data* data = (struct mm_mq_tcp_data*)mm_malloc(sizeof(struct mm_mq_tcp_data));
	mm_mq_tcp_data_init(data);
	data->obj = obj;
	mm_packet_copy_alloc(pack,&data->pack);
	// push to message queue.
	mm_lock_queue_blpush(&p->lock_queue,data);
}
MM_EXPORT_DLL void mm_mq_tcp_pop(struct mm_mq_tcp* p)
{
	struct mm_mq_tcp_data* data = NULL;
	size_t n = 0;
	// lock queue size is lock free.and we not need check it thread safe.
	while( 0 != p->lock_queue.size && n <= p->max_pop )
	{
		data = (struct mm_mq_tcp_data*)mm_lock_queue_brpop(&p->lock_queue);
		if (NULL != data)
		{
			struct mm_packet* pack = &data->pack;
			void* obj = data->obj;
			net_tcp_handle handle = NULL;
			mm_spin_lock(&p->holder_locker);
			handle = (net_tcp_handle)mm_holder_u32_vpt_get(&p->holder,pack->phead.mid);
			mm_spin_unlock(&p->holder_locker);
			if (NULL != handle)
			{
				(*(handle))(obj,p->u,pack);
			}
			else
			{
				// if not define the handler,fire the default function.
				assert(NULL != p->callback.handle && "p->callback.handle is a null.");
				(*(p->callback.handle))(obj,p->u,pack);
			}
			mm_packet_free_copy_alloc(&data->pack);
			mm_mq_tcp_data_destroy(data);
			mm_free(data);
			data = NULL;
		}
		n++;
	}
}
//////////////////////////////////////////////////////////////////////////