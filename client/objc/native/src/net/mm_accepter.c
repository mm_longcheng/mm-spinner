#include "net/mm_accepter.h"
#include "core/mm_logger.h"
#include "core/mm_time.h"

static void __static_accepter_accept( void* obj, mm_socket_t fd, struct sockaddr_storage* remote )
{

}

MM_EXPORT_DLL void mm_accepter_callback_init(struct mm_accepter_callback* p)
{
	p->accept = &__static_accepter_accept;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_accepter_callback_destroy(struct mm_accepter_callback* p)
{
	p->accept = &__static_accepter_accept;
	p->obj = NULL;
}

// accept task.
static void* __static_accepter_accept_thread(void* _arg);
//
MM_EXPORT_DLL void mm_accepter_init(struct mm_accepter* p)
{
	mm_addr_init(&p->addr);// accepter address.
	mm_accepter_callback_init(&p->callback);
	p->backlog = MM_ACCEPTER_BACKLOG;
	p->keepalive = MM_KEEPALIVE_ACTIVATE;
	p->accepter_state = accepter_state_closed;// mm_accepter_state_t,default is accepter_state_closed(0)
	p->nonblock_timeout = MM_ACCEPTER_NONBLOCK_TIMEOUT;// tcp nonblock timeout.default is MM_ACCEPTER_NONBLOCK_TIMEOUT milliseconds.
	p->trytimes = MM_NET_ACCEPTER_TRYTIME;
	p->state = ts_closed;
}
MM_EXPORT_DLL void mm_accepter_destroy(struct mm_accepter* p)
{
	mm_addr_destroy(&p->addr);
	mm_accepter_callback_destroy(&p->callback);
	p->backlog = 0;
	p->keepalive = MM_KEEPALIVE_ACTIVATE;
	p->accepter_state = accepter_state_closed;
	p->nonblock_timeout = 0;
	p->trytimes = 0;
	p->state = ts_closed;
}
// assign native address but not connect.
MM_EXPORT_DLL void mm_accepter_assign_native(struct mm_accepter* p,const char* node,mm_ushort_t port)
{
	mm_addr_assign_native(&p->addr, node, port);
}
// assign remote address but not connect.
MM_EXPORT_DLL void mm_accepter_assign_remote(struct mm_accepter* p,const char* node,mm_ushort_t port)
{
	mm_addr_assign_remote(&p->addr, node, port);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_accepter_assign_callback(struct mm_accepter* p,struct mm_accepter_callback* cb)
{
	assert(NULL != cb && "cb is a null.");
	p->callback = (*cb);
}
//////////////////////////////////////////////////////////////////////////
// reset streambuf and fire event broken.
MM_EXPORT_DLL void mm_accepter_apply_broken(struct mm_accepter* p)
{
	p->accepter_state = accepter_state_closed;
}
// reset streambuf and fire event broken.
MM_EXPORT_DLL void mm_accepter_apply_finish(struct mm_accepter* p)
{
	p->accepter_state = accepter_state_finish;
}
// fopen.
MM_EXPORT_DLL void mm_accepter_fopen_socket(struct mm_accepter* p)
{
	struct mm_logger* g_logger = mm_logger_instance();
	char link_name[MM_LINK_NAME_LENGTH];
	int reuseaddr_flag = 1;
	struct mm_addr* addr = &p->addr;
	//
	mm_addr_fopen_socket(&p->addr,p->addr.ss_native.ss_family,SOCK_STREAM,0);
	//
	mm_addr_string(&p->addr,link_name);
	//
	if (MM_INVALID_SOCKET != p->addr.socket)
	{
		mm_logger_log_I(g_logger,"%s %d fopen_socket %s success.",__FUNCTION__,__LINE__,link_name);
	}
	else
	{
		//
		mm_logger_log_I(g_logger,"%s %d fopen_socket %s failure.",__FUNCTION__,__LINE__,link_name);
	}
	//
	//SO_REUSEADDR
	//	Indicates that the rules used in validating addresses supplied
	//	in a bind(2) call should allow reuse of local addresses.  For
	//	AF_INET sockets this means that a socket may bind, except when
	//	there is an active listening socket bound to the address.
	//	When the listening socket is bound to INADDR_ANY with a
	//	specific port then it is not possible to bind to this port for
	//	any local address.  Argument is an integer boolean flag.
	if (-1 == setsockopt(addr->socket, SOL_SOCKET, SO_REUSEADDR,(const char*)&reuseaddr_flag , sizeof(reuseaddr_flag)))
	{
		// the option for SO_REUSEADDR socket flag is optional.
		// this error is not serious.
		mm_err_t errcode = mm_errno;
		mm_logger_socket_error(g_logger, MM_LOG_INFO, errcode);
		mm_logger_log_I(g_logger,"%s %d reuseaddr %s failure",__FUNCTION__,__LINE__,link_name);
	}
	else
	{
		mm_logger_log_I(g_logger,"%s %d reuseaddr %s success",__FUNCTION__,__LINE__,link_name);
	}
}

// bind address.
MM_EXPORT_DLL void mm_accepter_bind(struct mm_accepter* p)
{
	char link_name[MM_LINK_NAME_LENGTH];
	struct mm_addr* addr = &p->addr;
	struct mm_logger* g_logger = mm_logger_instance();
	//
	mm_addr_string(addr,link_name);
	//
	if ( MM_INVALID_SOCKET != addr->socket )
	{
		mm_uint32_t _try_times = 0;
		int rt = -1;
		p->accepter_state = accepter_state_motion;
		// set socket to blocking.
		mm_addr_set_block(addr, MM_BLOCKING);
		do 
		{
			_try_times ++;
			rt = mm_addr_bind(addr);
			if (0 == rt)
			{
				// bind immediately.
				break;
			}
			if ( ts_finish == p->state )
			{
				break;
			}
			else
			{
				mm_logger_log_E(g_logger,"%s %d bind %s failure.try times:%d",__FUNCTION__,__LINE__,link_name,_try_times);
				mm_msleep(MM_ACCEPTER_TRY_MSLEEP_TIME);
			}
		} while ( 0 != rt && _try_times < p->trytimes );
		// set socket to blocking.
		mm_addr_set_block(addr, MM_BLOCKING);
		if ( 0 != rt )
		{
			mm_err_t errcode = mm_errno;
			mm_logger_log_E(g_logger,"%s %d bind %s failure.try times:%d",__FUNCTION__,__LINE__,link_name,_try_times);
			mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
			mm_accepter_apply_broken(p);
			mm_addr_close_socket(addr);
		}
		else
		{
			mm_accepter_apply_finish(p);
			mm_logger_log_I(g_logger,"%s %d %s success.try times:%d",__FUNCTION__,__LINE__,link_name,_try_times);
		}
	}
	else
	{
		mm_logger_log_E(g_logger,"%s %d bind target fd is invalid.",__FUNCTION__,__LINE__);
	}
}
// listen address.
MM_EXPORT_DLL void mm_accepter_listen(struct mm_accepter* p)
{
	char link_name[MM_LINK_NAME_LENGTH];
	struct mm_addr* addr = &p->addr;
	struct mm_logger* g_logger = mm_logger_instance();
	//
	mm_addr_string(addr,link_name);
	//
	if ( MM_INVALID_SOCKET != addr->socket )
	{
		mm_uint32_t _try_times = 0;
		int rt = -1;
		p->accepter_state = accepter_state_motion;
		// set socket to blocking.
		mm_addr_set_block(addr, MM_BLOCKING);
		do 
		{
			_try_times ++;
			rt = mm_addr_listen(addr,p->backlog);
			if (0 == rt)
			{
				// listen immediately.
				break;
			}
			if ( ts_finish == p->state )
			{
				break;
			}
			else
			{
				mm_logger_log_E(g_logger,"%s %d listen %s failure.try times:%d",__FUNCTION__,__LINE__,link_name,_try_times);
				mm_msleep(MM_ACCEPTER_TRY_MSLEEP_TIME);
			}
		} while ( 0 != rt && _try_times < p->trytimes );
		// set socket to blocking.
		mm_addr_set_block(addr, MM_BLOCKING);
		if ( 0 != rt )
		{
			mm_err_t errcode = mm_errno;
			mm_logger_log_E(g_logger,"%s %d listen %s failure.try times:%d",__FUNCTION__,__LINE__,link_name,_try_times);
			mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
			mm_accepter_apply_broken(p);
			mm_addr_close_socket(addr);
		}
		else
		{
			mm_accepter_apply_finish(p);
			mm_logger_log_I(g_logger,"%s %d listen %s success.try times:%d",__FUNCTION__,__LINE__,link_name,_try_times);
		}
	}
	else
	{
		mm_logger_log_E(g_logger,"%s %d listen target fd is invalid.",__FUNCTION__,__LINE__);
	}
}
// close socket. mm_accepter_join before call this.
MM_EXPORT_DLL void mm_accepter_close_socket(struct mm_accepter* p)
{
	mm_addr_close_socket(&p->addr);
}
// accept sync.
MM_EXPORT_DLL void mm_accepter_accept(struct mm_accepter* p)
{
	socklen_t remote_size;
	struct sockaddr_storage remote;
	mm_socket_t afd;
	char link_name[MM_LINK_NAME_LENGTH];
	struct mm_logger* g_logger = mm_logger_instance();
	// we not need assert the fd is must valid.
	// assert( MM_INVALID_SOCKET != p->addr.fd&&"you must alloc socket fd first.");
	p->state = (MM_INVALID_SOCKET == p->addr.socket || ts_finish == p->state) ? ts_closed : ts_motion;
	//
	remote_size = sizeof(struct sockaddr_storage);
	while(ts_motion == p->state)
	{
		mm_memset(&remote,0,sizeof(struct sockaddr_storage));
		afd = accept(p->addr.socket, (struct sockaddr*)(&remote), &remote_size);
		if(MM_INVALID_SOCKET == afd)
		{
			mm_err_t errcode = mm_errno;
			if (MM_EINTR == errcode)
			{
				// MM_EINTR errno:(10004) A blocking operation was interrupted.
				// this error is not serious.
				continue;
			}
			else
			{
				mm_addr_string(&p->addr,link_name);
				// at here when we want interrupt accept, will case a socket errno.
				// [errno:(53) Software caused connection abort] it is expected result.
				mm_logger_log_I(g_logger,"%s %d %s error occur.",__FUNCTION__,__LINE__,link_name);
				mm_logger_socket_error(g_logger, MM_LOG_INFO, errcode);
				p->state = ts_closed;
				p->accepter_state = accepter_state_broken;
			}
		}
		else
		{
			if ( MM_KEEPALIVE_ACTIVATE == p->keepalive )
			{
				mm_socket_keepalive(afd,p->keepalive);
			}
			//
			(*p->callback.accept)(p,afd,&remote);
			mm_sockaddr_storage_native_remote_string(&p->addr.ss_native,&remote,afd,link_name);
			mm_logger_log_I(g_logger,"%s %d %s connect.",__FUNCTION__,__LINE__, link_name);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
// start accept thread.
MM_EXPORT_DLL void mm_accepter_start(struct mm_accepter* p)
{
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	pthread_create(&p->accept_thread, NULL, &__static_accepter_accept_thread, p);
}
// interrupt accept thread.
MM_EXPORT_DLL void mm_accepter_interrupt(struct mm_accepter* p)
{
	p->state = ts_closed;
	mm_addr_shutdown_socket(&p->addr,MM_BOTH_SHUTDOWN);
	mm_addr_close_socket(&p->addr);
}
// shutdown accept thread.
MM_EXPORT_DLL void mm_accepter_shutdown(struct mm_accepter* p)
{
	p->state = ts_finish;
	mm_addr_shutdown_socket(&p->addr,MM_BOTH_SHUTDOWN);
	mm_addr_close_socket(&p->addr);
}
// join accept thread.
MM_EXPORT_DLL void mm_accepter_join(struct mm_accepter* p)
{
	pthread_join(p->accept_thread, NULL);
}
//////////////////////////////////////////////////////////////////////////
static void* __static_accepter_accept_thread(void* arg)
{
	struct mm_accepter* p = (struct mm_accepter*)(arg);
	mm_accepter_accept(p);
	return NULL;
}
