#ifndef __mm_addr_h__
#define __mm_addr_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_socket.h"
#include "core/mm_atomic.h"

#include "net/mm_sockaddr.h"

//////////////////////////////////////////////////////////////////////////
struct mm_addr
{
	struct sockaddr_storage ss_native;// native end point.
	struct sockaddr_storage ss_remote;// remote end point.
	mm_socket_t socket;// strong ref.
	mm_atomic_t locker;
	void* u;// user data.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_addr_init(struct mm_addr* p);
MM_EXPORT_DLL void mm_addr_destroy(struct mm_addr* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_addr_lock(struct mm_addr* p);
MM_EXPORT_DLL void mm_addr_unlock(struct mm_addr* p);
//////////////////////////////////////////////////////////////////////////
// alloc socket fd.
// tcp PF_INET, SOCK_STREAM, 0
// udp PF_INET, SOCK_DGRAM , 0
MM_EXPORT_DLL void mm_addr_fopen_socket(struct mm_addr* p, int domain, int type, int protocol);
// close socket fd if fd is valid.fd == -1 is call safe.
MM_EXPORT_DLL void mm_addr_close_socket(struct mm_addr* p );
// close socket fd.not set socket_t to invalid -1.
// use to fire socket fd close event.
MM_EXPORT_DLL void mm_addr_close_socket_event(struct mm_addr* p);
// shutdown socket.
MM_EXPORT_DLL void mm_addr_shutdown_socket(struct mm_addr* p, int opcode);
// assign addr native by ip port.AF_INET;after assign native will auto assign the remote ss_family.
MM_EXPORT_DLL void mm_addr_assign_native(struct mm_addr* p, const char* node, mm_ushort_t port);
// assign addr remote by ip port.AF_INET;after assign remote will auto assign the native ss_family.
MM_EXPORT_DLL void mm_addr_assign_remote(struct mm_addr* p, const char* node, mm_ushort_t port);
// assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
MM_EXPORT_DLL void mm_addr_assign_native_storage(struct mm_addr* p, struct sockaddr_storage* ss_native);
// assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
MM_EXPORT_DLL void mm_addr_assign_remote_storage(struct mm_addr* p, struct sockaddr_storage* ss_remote);
// context for tcp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_DLL void mm_addr_set_context(struct mm_addr* p, void* u);
MM_EXPORT_DLL void* mm_addr_get_context(struct mm_addr* p);
// default is blocking.MM_BLOCKING means block MM_NONBLOCK no block.
MM_EXPORT_DLL int mm_addr_set_block(struct mm_addr* p, int block);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL int mm_addr_connect(struct mm_addr* p);
// bind address.
MM_EXPORT_DLL int mm_addr_bind(struct mm_addr* p);
// listen address.
MM_EXPORT_DLL int mm_addr_listen(struct mm_addr* p, int backlog);
//////////////////////////////////////////////////////////////////////////
// we not use c style char* but use buffer and offset target the real buffer,because other language might not have pointer.
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL int mm_addr_recv(struct mm_addr* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, int flags );
MM_EXPORT_DLL int mm_addr_send(struct mm_addr* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, int flags );
MM_EXPORT_DLL int mm_addr_send_dgram(struct mm_addr* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, int flags, struct sockaddr* remote_addr, socklen_t  remote_size);
MM_EXPORT_DLL int mm_addr_recv_dgram(struct mm_addr* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, int flags, struct sockaddr* remote_addr, socklen_t* remote_size);
//////////////////////////////////////////////////////////////////////////
// (fd)|ip-port --> ip-port
// ipv6 (65535)|2001:0DB8:0000:0023:0008:0800:200C:417A-65535 --> 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         (65535)|192.168.111.123-65535 --> 192.168.111.123-65535
MM_EXPORT_DLL void mm_addr_string(struct mm_addr* p, char link_name[MM_LINK_NAME_LENGTH]);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_addr_h__