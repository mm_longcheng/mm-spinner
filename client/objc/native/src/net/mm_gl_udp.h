#ifndef __mm_gl_udp_h__
#define __mm_gl_udp_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_time.h"
#include "core/mm_thread.h"
#include "core/mm_signal_task.h"

#include "net/mm_mq_udp.h"

//////////////////////////////////////////////////////////////////////////
// gl udp connect handle check milliseconds.default is 5 second(5000 ms).
#define MM_GL_UDP_HANDLE_MSEC_CHECK 5000
// gl udp connect broken check milliseconds.default is 1 second(1000 ms).
#define MM_GL_UDP_BROKEN_MSEC_CHECK 1000

// send thread msleep interval handle time.
#define MM_GL_UDP_SEND_INTERVAL_HANDLE_TIME 0
// send thread msleep interval broken time.
#define MM_GL_UDP_SEND_INTERVAL_BROKEN_TIME 1000

enum mm_gl_udp_check_flag_t
{
	gl_udp_check_inactive = 0,// do not check the connect state.
	gl_udp_check_activate = 1,// check the connect state.
};
enum mm_gl_udp_push_to_queue_t
{
	gl_udp_ptq_inactive = 0,// not push to queue.
	gl_udp_ptq_activate = 1,// push to queue.
};
//////////////////////////////////////////////////////////////////////////
typedef void (*gl_udp_handle)(void* obj, void* u, struct mm_packet* pack, struct sockaddr_storage* remote);
struct mm_gl_udp_callback
{
	gl_udp_handle handle;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_gl_udp_callback_init(struct mm_gl_udp_callback* p);
MM_EXPORT_DLL void mm_gl_udp_callback_destroy(struct mm_gl_udp_callback* p);
//////////////////////////////////////////////////////////////////////////
struct mm_gl_udp
{
	struct sockaddr_storage ss_native;// native end point.target value not current real.
	struct sockaddr_storage ss_remote;// remote end point.target value not current real.
	struct mm_net_udp net_udp;// real net udp.
	struct mm_mq_udp queue_udp;// thread handle message queue.
	struct mm_gl_udp_callback callback;// value ref. udp callback.
	struct mm_signal_task flush_task;// flush task for flush send message buffer.
	struct mm_signal_task state_task;// state task for check state.
	mm_atomic_t locker;
	mm_uint8_t state_check_flag;// default is gl_udp_check_inactive.
	mm_uint8_t push_to_queue_flag;// default is gl_udp_ptq_activate.
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
	void* u;// user data.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_udp_init(struct mm_gl_udp* p);
MM_EXPORT_DLL void mm_gl_udp_destroy(struct mm_gl_udp* p);
//////////////////////////////////////////////////////////////////////////
// assign native address but not connect.
MM_EXPORT_DLL void mm_gl_udp_assign_native(struct mm_gl_udp* p,const char* node,mm_ushort_t port);
// assign remote address but not connect.
MM_EXPORT_DLL void mm_gl_udp_assign_remote(struct mm_gl_udp* p,const char* node,mm_ushort_t port);
// assign native address but not connect.target value not current real.will apply at state thread.
MM_EXPORT_DLL void mm_gl_udp_assign_native_target(struct mm_gl_udp* p,const char* node,mm_ushort_t port);
// assign remote address but not connect.target value not current real..will apply at state thread.
MM_EXPORT_DLL void mm_gl_udp_assign_remote_target(struct mm_gl_udp* p,const char* node,mm_ushort_t port);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_udp_assign_q_default_callback(struct mm_gl_udp* p,struct mm_mq_udp_callback* mq_udp_callback);
MM_EXPORT_DLL void mm_gl_udp_assign_n_default_callback(struct mm_gl_udp* p,struct mm_gl_udp_callback* gl_udp_callback);
// assign queue callback.at main thread.
MM_EXPORT_DLL void mm_gl_udp_assign_q_callback(struct mm_gl_udp* p,mm_uint32_t id,net_udp_handle callback);
// assign net callback.not at main thread.
MM_EXPORT_DLL void mm_gl_udp_assign_n_callback(struct mm_gl_udp* p,mm_uint32_t id,net_udp_handle callback);
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_gl_udp_assign_event_udp_alloc(struct mm_gl_udp* p,struct mm_net_udp_event_udp_alloc* event_udp_alloc);
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_gl_udp_assign_crypto_callback(struct mm_gl_udp* p,struct mm_crypto_callback* crypto_callback);
// assign context handle.
MM_EXPORT_DLL void mm_gl_udp_assign_context(struct mm_gl_udp* p,void* u);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_udp_clear_callback_holder(struct mm_gl_udp* p);
//////////////////////////////////////////////////////////////////////////
// context for udp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_DLL void mm_gl_udp_set_addr_context(struct mm_gl_udp* p, void* u);
MM_EXPORT_DLL void* mm_gl_udp_get_addr_context(struct mm_gl_udp* p);
//////////////////////////////////////////////////////////////////////////
// gl_udp crypto encrypt buffer by udp.
MM_EXPORT_DLL void mm_gl_udp_crypto_encrypt(struct mm_gl_udp* p,struct mm_udp* udp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length);
// gl_udp crypto decrypt buffer by udp.
MM_EXPORT_DLL void mm_gl_udp_crypto_decrypt(struct mm_gl_udp* p,struct mm_udp* udp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_udp_assign_flush_handle_nearby(struct mm_gl_udp* p,mm_msec_t milliseconds);
MM_EXPORT_DLL void mm_gl_udp_assign_flush_broken_nearby(struct mm_gl_udp* p,mm_msec_t milliseconds);
MM_EXPORT_DLL void mm_gl_udp_assign_state_handle_nearby(struct mm_gl_udp* p,mm_msec_t milliseconds);
MM_EXPORT_DLL void mm_gl_udp_assign_state_broken_nearby(struct mm_gl_udp* p,mm_msec_t milliseconds);
MM_EXPORT_DLL void mm_gl_udp_assign_state_check_flag(struct mm_gl_udp* p,mm_uint8_t flag);
MM_EXPORT_DLL void mm_gl_udp_assign_push_to_queue_flag(struct mm_gl_udp* p,mm_uint8_t flag);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_udp_assign_max_poper_number(struct mm_gl_udp* p,size_t max_pop);
//////////////////////////////////////////////////////////////////////////
// lock to make sure the mm_gl_udp is thread safe.
MM_EXPORT_DLL void mm_gl_udp_lock(struct mm_gl_udp* p);
// unlock net_udp.
MM_EXPORT_DLL void mm_gl_udp_unlock(struct mm_gl_udp* p);
// lock the flush send udp buffer.
MM_EXPORT_DLL void mm_gl_udp_flush_lock(struct mm_gl_udp* p);
// unlock the flush send udp buffer.
MM_EXPORT_DLL void mm_gl_udp_flush_unlock(struct mm_gl_udp* p);
//////////////////////////////////////////////////////////////////////////
// fopen socket.
MM_EXPORT_DLL void mm_gl_udp_fopen_socket(struct mm_gl_udp* p);
// bind.
MM_EXPORT_DLL void mm_gl_udp_bind(struct mm_gl_udp* p);
// close socket.
MM_EXPORT_DLL void mm_gl_udp_close_socket(struct mm_gl_udp* p);
//////////////////////////////////////////////////////////////////////////
// check connect and reconnect if disconnect.not thread safe.
MM_EXPORT_DLL void mm_gl_udp_check(struct mm_gl_udp* p);
// get state finally.return 0 for all regular.not thread safe.
MM_EXPORT_DLL int mm_gl_udp_finally_state(struct mm_gl_udp* p);
//////////////////////////////////////////////////////////////////////////
// synchronize attach to socket.not thread safe.
MM_EXPORT_DLL void mm_gl_udp_attach_socket(struct mm_gl_udp* p);
// synchronize detach to socket.not thread safe.
MM_EXPORT_DLL void mm_gl_udp_detach_socket(struct mm_gl_udp* p);
// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_DLL void mm_gl_udp_thread_handle(struct mm_gl_udp* p);
// push a pack and context tp queue.this function will copy alloc pack.
MM_EXPORT_DLL void mm_gl_udp_push(struct mm_gl_udp* p, void* obj, struct mm_packet* pack, struct sockaddr_storage* remote);
//////////////////////////////////////////////////////////////////////////
// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_gl_udp_buffer_send(struct mm_gl_udp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote);
// handle send data by flush send buffer.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_gl_udp_flush_send(struct mm_gl_udp* p);
//////////////////////////////////////////////////////////////////////////
// use for trigger the flush send thread signal.
// is useful for main thread asynchronous send message.
MM_EXPORT_DLL void mm_gl_udp_flush_signal(struct mm_gl_udp* p);
// use for trigger the state check thread signal.
// is useful for main thread asynchronous state check.
MM_EXPORT_DLL void mm_gl_udp_state_signal(struct mm_gl_udp* p);
//////////////////////////////////////////////////////////////////////////
// start wait thread.
MM_EXPORT_DLL void mm_gl_udp_start(struct mm_gl_udp* p);
// interrupt wait thread.
MM_EXPORT_DLL void mm_gl_udp_interrupt(struct mm_gl_udp* p);
// shutdown wait thread.
MM_EXPORT_DLL void mm_gl_udp_shutdown(struct mm_gl_udp* p);
// join wait thread.
MM_EXPORT_DLL void mm_gl_udp_join(struct mm_gl_udp* p);
//////////////////////////////////////////////////////////////////////////

#include "core/mm_suffix.h"

#endif//__mm_gl_udp_h__