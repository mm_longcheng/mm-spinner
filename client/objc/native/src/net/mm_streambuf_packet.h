#ifndef __mm_streambuf_packet_h__
#define __mm_streambuf_packet_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_streambuf.h"

#include "net/mm_packet.h"
//////////////////////////////////////////////////////////////////////////
// util about struct mm_streambuf and struct mm_packet.
//////////////////////////////////////////////////////////////////////////
// append pack data to streambuf.
MM_EXPORT_DLL mm_uint32_t mm_streambuf_packet_append(struct mm_streambuf* p, struct mm_packet* pack);
// overdraft pack.hbuff pack.bbuff for encode.
// will pbump streambuf.
MM_EXPORT_DLL mm_uint32_t mm_streambuf_packet_overdraft(struct mm_streambuf* p, struct mm_packet* pack);
//////////////////////////////////////////////////////////////////////////
// MM_MSG_COMM_HEAD_SIZE valid.
//////////////////////////////////////////////////////////////////////////
// if need quick transmit one transport recv data to another transport,call this.it is safe.
// assign sid quick,make sure the data must struct mm_packet format.
MM_EXPORT_DLL void mm_streambuf_packet_assign_sid(struct mm_streambuf* p, mm_uint64_t sid);
// if need quick transmit one transport recv data to another transport,call this.it is safe.
// assign pid quick,make sure the data must struct mm_packet format.
MM_EXPORT_DLL void mm_streambuf_packet_assign_pid(struct mm_streambuf* p, mm_uint32_t pid);
//////////////////////////////////////////////////////////////////////////
// packet handle tcp buffer and length for streambuf handle separate packet.
MM_EXPORT_DLL void mm_streambuf_packet_handle_tcp(struct mm_streambuf* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, packet_handle_tcp handle, void* obj);
// packet handle udp buffer and length for streambuf handle separate packet.
MM_EXPORT_DLL void mm_streambuf_packet_handle_udp(struct mm_streambuf* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, packet_handle_udp handle, void* obj, struct sockaddr_storage* remote);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_streambuf_packet_h__