#ifndef __mm_mailbox_h__
#define __mm_mailbox_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_spinlock.h"
#include "core/mm_thread.h"

#include "net/mm_accepter.h"
#include "net/mm_nucleus.h"
#include "net/mm_tcp.h"
#include "net/mm_crypto.h"
#include "net/mm_packet.h"

#include "container/mm_holder32.h"

struct mm_mailbox;
typedef void (*mailbox_traver)( struct mm_mailbox* mailbox, struct mm_tcp* tcp, void* u );

typedef void (*mailbox_handle)( void* obj, void* u, struct mm_packet* pack );
typedef void (*mailbox_broken)( void* obj );
typedef void (*mailbox_nready)( void* obj );
typedef void (*mailbox_finish)( void* obj );
struct mm_mailbox_callback
{
	mailbox_handle handle;
	mailbox_broken broken;
	mailbox_nready nready;
	mailbox_finish finish;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_mailbox_callback_init(struct mm_mailbox_callback* p);
MM_EXPORT_DLL void mm_mailbox_callback_destroy(struct mm_mailbox_callback* p);

typedef void (*mailbox_event_tcp_alloc)( void* p, struct mm_tcp* tcp );
typedef void (*mailbox_event_tcp_relax)( void* p, struct mm_tcp* tcp );
struct mm_mailbox_event_tcp_alloc
{
	mailbox_event_tcp_alloc event_tcp_alloc;// event tcp alloc and add holder.
	mailbox_event_tcp_relax event_tcp_relax;// event tcp relax and rmv holder.
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_mailbox_event_tcp_alloc_init(struct mm_mailbox_event_tcp_alloc* p);
MM_EXPORT_DLL void mm_mailbox_event_tcp_alloc_destroy(struct mm_mailbox_event_tcp_alloc* p);

struct mm_mailbox
{
	struct mm_accepter accepter;
	struct mm_nucleus nucleus;// strong ref.
	struct mm_holder_u32_vpt holder;// rb tree for msg_id <--> callback.
	struct mm_mailbox_callback callback;// value ref. tcp callback.
	struct mm_mailbox_event_tcp_alloc event_tcp_alloc;// value ref.event tcp alloc.
	struct mm_crypto_callback crypto_callback;// crypto callback.
	mm_atomic_t holder_locker;
	mm_atomic_t locker;
	void* u;// user data.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mailbox_init(struct mm_mailbox* p);
MM_EXPORT_DLL void mm_mailbox_destroy(struct mm_mailbox* p);
//////////////////////////////////////////////////////////////////////////
// assign native address but not connect.
MM_EXPORT_DLL void mm_mailbox_assign_native(struct mm_mailbox* p,const char* node,mm_ushort_t port);
// assign remote address but not connect.
MM_EXPORT_DLL void mm_mailbox_assign_remote(struct mm_mailbox* p,const char* node,mm_ushort_t port);
// assign protocol_size.
MM_EXPORT_DLL void mm_mailbox_set_length(struct mm_mailbox* p, mm_uint32_t length);
MM_EXPORT_DLL mm_uint32_t mm_mailbox_get_length(struct mm_mailbox* p);
// assign context handle.
MM_EXPORT_DLL void mm_mailbox_assign_context(struct mm_mailbox* p,void* u);
// fopen.
MM_EXPORT_DLL void mm_mailbox_fopen_socket(struct mm_mailbox* p);
// bind.
MM_EXPORT_DLL void mm_mailbox_bind(struct mm_mailbox* p);
// listen.
MM_EXPORT_DLL void mm_mailbox_listen(struct mm_mailbox* p);
// close socket. mm_mailbox_join before call this.
MM_EXPORT_DLL void mm_mailbox_close_socket(struct mm_mailbox* p);
//////////////////////////////////////////////////////////////////////////
// lock to make sure the mailbox is thread safe.
MM_EXPORT_DLL void mm_mailbox_lock(struct mm_mailbox* p);
// unlock mailbox.
MM_EXPORT_DLL void mm_mailbox_unlock(struct mm_mailbox* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mailbox_assign_callback(struct mm_mailbox* p,mm_uint32_t id,mailbox_handle callback);
//////////////////////////////////////////////////////////////////////////
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_mailbox_assign_event_tcp_alloc(struct mm_mailbox* p,struct mm_mailbox_event_tcp_alloc* event_tcp_alloc);
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_mailbox_assign_crypto_callback(struct mm_mailbox* p,struct mm_crypto_callback* crypto_callback);
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_mailbox_assign_mailbox_callback(struct mm_mailbox* p,struct mm_mailbox_callback* mailbox_callback);
MM_EXPORT_DLL void mm_mailbox_assign_tcp_alloc(struct mm_mailbox* p,struct mm_holder_u32_vpt_alloc* alloc);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mailbox_clear_callback_holder(struct mm_mailbox* p);
//////////////////////////////////////////////////////////////////////////
// mailbox crypto encrypt buffer by tcp.
MM_EXPORT_DLL void mm_mailbox_crypto_encrypt(struct mm_mailbox* p,struct mm_tcp* tcp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length);
// mailbox crypto decrypt buffer by tcp.
MM_EXPORT_DLL void mm_mailbox_crypto_decrypt(struct mm_mailbox* p,struct mm_tcp* tcp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length);
//////////////////////////////////////////////////////////////////////////
// not lock inside,lock tcp manual.this func will copy each nuclear poll fd holder,
// and traver all fd(not the tcp weak ref),use fd get the tcp for nuclear and trigger callback.
MM_EXPORT_DLL void mm_mailbox_traver(struct mm_mailbox* p,mailbox_traver func,void* u);
// poll size.
MM_EXPORT_DLL size_t mm_mailbox_poll_size(struct mm_mailbox* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mailbox_start(struct mm_mailbox* p);
MM_EXPORT_DLL void mm_mailbox_interrupt(struct mm_mailbox* p);
MM_EXPORT_DLL void mm_mailbox_shutdown(struct mm_mailbox* p);
MM_EXPORT_DLL void mm_mailbox_join(struct mm_mailbox* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL struct mm_tcp* mm_mailbox_add(struct mm_mailbox* p,mm_socket_t fd,struct sockaddr_storage* remote);
MM_EXPORT_DLL struct mm_tcp* mm_mailbox_get(struct mm_mailbox* p,mm_socket_t fd);
MM_EXPORT_DLL struct mm_tcp* mm_mailbox_get_instance(struct mm_mailbox* p,mm_socket_t fd,struct sockaddr_storage* remote);
MM_EXPORT_DLL void mm_mailbox_rmv(struct mm_mailbox* p,mm_socket_t fd);
MM_EXPORT_DLL void mm_mailbox_rmv_tcp(struct mm_mailbox* p,struct mm_tcp* tcp);
MM_EXPORT_DLL void mm_mailbox_clear(struct mm_mailbox* p);
//////////////////////////////////////////////////////////////////////////
// use this function for manual shutdown tcp.
// note: you can not use rmv or rmv_tcp for shutdown a activation tcp.
MM_EXPORT_DLL void mm_mailbox_shutdown_tcp(struct mm_mailbox* p,struct mm_tcp* tcp);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_mailbox_h__