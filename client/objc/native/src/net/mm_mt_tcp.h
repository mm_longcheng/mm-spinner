#ifndef __mm_mt_tcp_h__
#define __mm_mt_tcp_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_spinlock.h"
#include "core/mm_list.h"
#include "core/mm_time.h"
#include "core/mm_string.h"

#include "net/mm_net_tcp.h"
#include "net/mm_packet.h"

#include "container/mm_holder32.h"
#include "container/mm_lock_queue.h"

#include <pthread.h>

//////////////////////////////////////////////////////////////////////////
typedef void (*mm_mt_tcp_handle)(void* obj, void* u, struct mm_packet* pack);
typedef void (*mm_mt_tcp_broken)(void* obj);
typedef void (*mm_mt_tcp_nready)(void* obj);
typedef void (*mm_mt_tcp_finish)(void* obj);
struct mm_mt_tcp_callback
{
	mm_mt_tcp_handle handle;
	mm_mt_tcp_broken broken;
	mm_mt_tcp_nready nready;
	mm_mt_tcp_finish finish;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_mt_tcp_callback_init(struct mm_mt_tcp_callback* p);
MM_EXPORT_DLL void mm_mt_tcp_callback_destroy(struct mm_mt_tcp_callback* p);
//////////////////////////////////////////////////////////////////////////
struct mm_mt_tcp
{
	struct sockaddr_storage ss_native;// native end point.
	struct sockaddr_storage ss_remote;// remote end point.
	struct mm_list_head list;// silk strong ref list.
	struct mm_holder_u32_vpt holder;// rb tree for msg_id <--> callback.
	struct mm_mt_tcp_callback callback;// value ref. tcp callback.
	struct mm_crypto_callback crypto_callback;// crypto callback.
	struct mm_string cache_native_node;// cache ss_native node use for quick compare.
	struct mm_string cache_remote_node;// cache ss_remote node use for quick compare.
	pthread_mutex_t signal_mutex;
	pthread_cond_t signal_cond;
	mm_atomic_t list_locker;
	mm_atomic_t holder_locker;
	mm_atomic_t locker;
	pthread_key_t thread_key;// thread key.
	mm_ushort_t cache_remote_port;// cache ss_remote port use for quick compare.

	mm_msec_t nonblock_timeout;// tcp nonblock timeout.default is MM_NET_TCP_NONBLOCK_TIMEOUT milliseconds.

	mm_uint32_t trytimes;// try bind and listen times.default is -1.

	mm_uint32_t unique_id;// unique_id.
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
	void* u;// user data.
};

//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mt_tcp_init(struct mm_mt_tcp* p);
MM_EXPORT_DLL void mm_mt_tcp_destroy(struct mm_mt_tcp* p);
//////////////////////////////////////////////////////////////////////////
// assign native address but not connect.
MM_EXPORT_DLL void mm_mt_tcp_assign_native(struct mm_mt_tcp* p,const char* node,mm_ushort_t port);
// assign remote address but not connect.
MM_EXPORT_DLL void mm_mt_tcp_assign_remote(struct mm_mt_tcp* p,const char* node,mm_ushort_t port);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mt_tcp_assign_callback(struct mm_mt_tcp* p,mm_uint32_t id,net_tcp_handle callback);
MM_EXPORT_DLL void mm_mt_tcp_assign_default_callback(struct mm_mt_tcp* p,struct mm_mt_tcp_callback* mt_tcp_callback);
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_mt_tcp_assign_crypto_callback(struct mm_mt_tcp* p,struct mm_crypto_callback* crypto_callback);
// assign context handle.
MM_EXPORT_DLL void mm_mt_tcp_assign_context(struct mm_mt_tcp* p,void* u);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mt_tcp_clear_callback_holder(struct mm_mt_tcp* p);
//////////////////////////////////////////////////////////////////////////
// lock to make sure the mt_tcp is thread safe.
MM_EXPORT_DLL void mm_mt_tcp_lock(struct mm_mt_tcp* p);
// unlock mt_tcp.
MM_EXPORT_DLL void mm_mt_tcp_unlock(struct mm_mt_tcp* p);
//////////////////////////////////////////////////////////////////////////
// fopen socket.
MM_EXPORT_DLL void mm_mt_tcp_fopen_socket(struct mm_mt_tcp* p);
// synchronize connect.can call this as we already open connect check thread.
MM_EXPORT_DLL void mm_mt_tcp_connect(struct mm_mt_tcp* p);
// close socket.
MM_EXPORT_DLL void mm_mt_tcp_close_socket(struct mm_mt_tcp* p);
//////////////////////////////////////////////////////////////////////////
// check connect and reconnect if disconnect.not thread safe.
MM_EXPORT_DLL void mm_mt_tcp_check(struct mm_mt_tcp* p);
//////////////////////////////////////////////////////////////////////////
// synchronize attach to socket.not thread safe.
MM_EXPORT_DLL void mm_mt_tcp_attach_socket(struct mm_mt_tcp* p);
// synchronize detach to socket.not thread safe.
MM_EXPORT_DLL void mm_mt_tcp_detach_socket(struct mm_mt_tcp* p);
//////////////////////////////////////////////////////////////////////////
// get current thread net tcp.
// note 
MM_EXPORT_DLL struct mm_net_tcp* mm_mt_tcp_thread_instance(struct mm_mt_tcp* p);
// clear all net tcp.
MM_EXPORT_DLL void mm_mt_tcp_clear(struct mm_mt_tcp* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mt_tcp_start(struct mm_mt_tcp* p);
MM_EXPORT_DLL void mm_mt_tcp_interrupt(struct mm_mt_tcp* p);
MM_EXPORT_DLL void mm_mt_tcp_shutdown(struct mm_mt_tcp* p);
MM_EXPORT_DLL void mm_mt_tcp_join(struct mm_mt_tcp* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_mt_tcp_h__