#ifndef __mm_gl_tcp_h__
#define __mm_gl_tcp_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_time.h"
#include "core/mm_thread.h"
#include "core/mm_signal_task.h"

#include "net/mm_mq_tcp.h"

//////////////////////////////////////////////////////////////////////////
// gl tcp connect handle check milliseconds.default is 5 second(5000 ms).
#define MM_GL_TCP_HANDLE_MSEC_CHECK 5000
// gl tcp connect broken check milliseconds.default is 1 second(1000 ms).
#define MM_GL_TCP_BROKEN_MSEC_CHECK 1000

// send thread msleep interval handle time.
#define MM_GL_TCP_SEND_INTERVAL_HANDLE_TIME 0
// send thread msleep interval broken time.
#define MM_GL_TCP_SEND_INTERVAL_BROKEN_TIME 1000

enum mm_gl_tcp_check_flag_t
{
	gl_tcp_check_inactive = 0,// do not check the connect state.
	gl_tcp_check_activate = 1,// check the connect state.
};
enum mm_gl_tcp_push_to_queue_t
{
	gl_tcp_ptq_inactive = 0,// not push to queue.
	gl_tcp_ptq_activate = 1,// push to queue.
};
//////////////////////////////////////////////////////////////////////////
typedef void (*gl_tcp_handle)(void* obj, void* u,struct mm_packet* pack);
struct mm_gl_tcp_callback
{
	gl_tcp_handle handle;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_gl_tcp_callback_init(struct mm_gl_tcp_callback* p);
MM_EXPORT_DLL void mm_gl_tcp_callback_destroy(struct mm_gl_tcp_callback* p);
//////////////////////////////////////////////////////////////////////////
struct mm_gl_tcp
{
	struct sockaddr_storage ss_native;// native end point.target value not current real.
	struct sockaddr_storage ss_remote;// remote end point.target value not current real.
	struct mm_net_tcp net_tcp;// real net tcp.
	struct mm_mq_tcp queue_tcp;// thread handle message queue.
	struct mm_gl_tcp_callback callback;// value ref. tcp callback.
	struct mm_signal_task flush_task;// flush task for flush send message buffer.
	struct mm_signal_task state_task;// state task for check state.
	mm_atomic_t locker;
	mm_uint8_t state_check_flag;// default is gl_tcp_check_inactive.
	mm_uint8_t push_to_queue_flag;// default is gl_tcp_ptq_activate.
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
	void* u;// user data.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_tcp_init(struct mm_gl_tcp* p);
MM_EXPORT_DLL void mm_gl_tcp_destroy(struct mm_gl_tcp* p);
//////////////////////////////////////////////////////////////////////////
// assign native address but not connect.
MM_EXPORT_DLL void mm_gl_tcp_assign_native(struct mm_gl_tcp* p,const char* node,mm_ushort_t port);
// assign remote address but not connect.
MM_EXPORT_DLL void mm_gl_tcp_assign_remote(struct mm_gl_tcp* p,const char* node,mm_ushort_t port);
// assign native address but not connect.target value not current real.will apply at state thread.
MM_EXPORT_DLL void mm_gl_tcp_assign_native_target(struct mm_gl_tcp* p,const char* node,mm_ushort_t port);
// assign remote address but not connect.target value not current real..will apply at state thread.
MM_EXPORT_DLL void mm_gl_tcp_assign_remote_target(struct mm_gl_tcp* p,const char* node,mm_ushort_t port);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_tcp_assign_q_default_callback(struct mm_gl_tcp* p,struct mm_mq_tcp_callback* mq_tcp_callback);
MM_EXPORT_DLL void mm_gl_tcp_assign_n_default_callback(struct mm_gl_tcp* p,struct mm_gl_tcp_callback* gl_tcp_callback);
// assign queue callback.at main thread.
MM_EXPORT_DLL void mm_gl_tcp_assign_q_callback(struct mm_gl_tcp* p,mm_uint32_t id,net_tcp_handle callback);
// assign net callback.not at main thread.
MM_EXPORT_DLL void mm_gl_tcp_assign_n_callback(struct mm_gl_tcp* p,mm_uint32_t id,net_tcp_handle callback);
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_gl_tcp_assign_event_tcp_alloc(struct mm_gl_tcp* p,struct mm_net_tcp_event_tcp_alloc* event_tcp_alloc);
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_gl_tcp_assign_crypto_callback(struct mm_gl_tcp* p,struct mm_crypto_callback* crypto_callback);
// assign context handle.
MM_EXPORT_DLL void mm_gl_tcp_assign_context(struct mm_gl_tcp* p,void* u);
//////////////////////////////////////////////////////////////////////////
// context for tcp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_DLL void mm_gl_tcp_set_addr_context(struct mm_gl_tcp* p, void* u);
MM_EXPORT_DLL void* mm_gl_tcp_get_addr_context(struct mm_gl_tcp* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_tcp_clear_callback_holder(struct mm_gl_tcp* p);
//////////////////////////////////////////////////////////////////////////
// set crypto handle.
MM_EXPORT_DLL void mm_gl_tcp_set_crypto(struct mm_gl_tcp* p,void* u);
// get crypto handle.
MM_EXPORT_DLL void* mm_gl_tcp_get_crypto(struct mm_gl_tcp* p);
//////////////////////////////////////////////////////////////////////////
// gl_tcp crypto encrypt buffer by tcp.
MM_EXPORT_DLL void mm_gl_tcp_crypto_encrypt(struct mm_gl_tcp* p,struct mm_tcp* tcp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length);
// gl_tcp crypto decrypt buffer by tcp.
MM_EXPORT_DLL void mm_gl_tcp_crypto_decrypt(struct mm_gl_tcp* p,struct mm_tcp* tcp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_tcp_assign_flush_handle_nearby(struct mm_gl_tcp* p,mm_msec_t milliseconds);
MM_EXPORT_DLL void mm_gl_tcp_assign_flush_broken_nearby(struct mm_gl_tcp* p,mm_msec_t milliseconds);
MM_EXPORT_DLL void mm_gl_tcp_assign_state_handle_nearby(struct mm_gl_tcp* p,mm_msec_t milliseconds);
MM_EXPORT_DLL void mm_gl_tcp_assign_state_broken_nearby(struct mm_gl_tcp* p,mm_msec_t milliseconds);
MM_EXPORT_DLL void mm_gl_tcp_assign_state_check_flag(struct mm_gl_tcp* p,mm_uint8_t flag);
MM_EXPORT_DLL void mm_gl_tcp_assign_push_to_queue_flag(struct mm_gl_tcp* p,mm_uint8_t flag);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gl_tcp_assign_max_poper_number(struct mm_gl_tcp* p,size_t max_pop);
//////////////////////////////////////////////////////////////////////////
// lock to make sure the mm_gl_tcp is thread safe.
MM_EXPORT_DLL void mm_gl_tcp_lock(struct mm_gl_tcp* p);
// unlock net_tcp.
MM_EXPORT_DLL void mm_gl_tcp_unlock(struct mm_gl_tcp* p);
// lock the flush send tcp buffer.
MM_EXPORT_DLL void mm_gl_tcp_flush_lock(struct mm_gl_tcp* p);
// unlock the flush send tcp buffer.
MM_EXPORT_DLL void mm_gl_tcp_flush_unlock(struct mm_gl_tcp* p);
//////////////////////////////////////////////////////////////////////////
// fopen socket.
MM_EXPORT_DLL void mm_gl_tcp_fopen_socket(struct mm_gl_tcp* p);
// synchronize connect.can call this as we already open connect check thread.
MM_EXPORT_DLL void mm_gl_tcp_connect(struct mm_gl_tcp* p);
// close socket.
MM_EXPORT_DLL void mm_gl_tcp_close_socket(struct mm_gl_tcp* p);
//////////////////////////////////////////////////////////////////////////
// check connect and reconnect if disconnect.not thread safe.
MM_EXPORT_DLL void mm_gl_tcp_check(struct mm_gl_tcp* p);
// get state finally.return 0 for all regular.not thread safe.
MM_EXPORT_DLL int mm_gl_tcp_finally_state(struct mm_gl_tcp* p);
//////////////////////////////////////////////////////////////////////////
// synchronize attach to socket.not thread safe.
MM_EXPORT_DLL void mm_gl_tcp_attach_socket(struct mm_gl_tcp* p);
// synchronize detach to socket.not thread safe.
MM_EXPORT_DLL void mm_gl_tcp_detach_socket(struct mm_gl_tcp* p);
// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_DLL void mm_gl_tcp_thread_handle(struct mm_gl_tcp* p);
// push a pack and context tp queue.this function will copy alloc pack.
MM_EXPORT_DLL void mm_gl_tcp_push(struct mm_gl_tcp* p, void* obj, struct mm_packet* pack);
//////////////////////////////////////////////////////////////////////////
// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_gl_tcp_buffer_send(struct mm_gl_tcp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length);
// handle send data by flush send buffer.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_gl_tcp_flush_send(struct mm_gl_tcp* p);
//////////////////////////////////////////////////////////////////////////
// use for trigger the flush send thread signal.
// is useful for main thread asynchronous send message.
MM_EXPORT_DLL void mm_gl_tcp_flush_signal(struct mm_gl_tcp* p);
// use for trigger the state check thread signal.
// is useful for main thread asynchronous state check.
MM_EXPORT_DLL void mm_gl_tcp_state_signal(struct mm_gl_tcp* p);
//////////////////////////////////////////////////////////////////////////
// start wait thread.
MM_EXPORT_DLL void mm_gl_tcp_start(struct mm_gl_tcp* p);
// interrupt wait thread.
MM_EXPORT_DLL void mm_gl_tcp_interrupt(struct mm_gl_tcp* p);
// shutdown wait thread.
MM_EXPORT_DLL void mm_gl_tcp_shutdown(struct mm_gl_tcp* p);
// join wait thread.
MM_EXPORT_DLL void mm_gl_tcp_join(struct mm_gl_tcp* p);
//////////////////////////////////////////////////////////////////////////

#include "core/mm_suffix.h"

#endif//__mm_gl_tcp_h__