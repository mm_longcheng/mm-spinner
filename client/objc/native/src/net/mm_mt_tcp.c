#include "net/mm_mt_tcp.h"
#include "core/mm_alloc.h"
#include "container/mm_list_iterator.h"
//////////////////////////////////////////////////////////////////////////
static void __static_mt_tcp_net_tcp_handle(void* obj, void* u, struct mm_packet* pack);
static void __static_mt_tcp_net_tcp_broken(void* obj);
static void __static_mt_tcp_net_tcp_nready(void* obj);
static void __static_mt_tcp_net_tcp_finish(void* obj);
//////////////////////////////////////////////////////////////////////////
static void __static_mt_tcp_handle(void* obj, void* u, struct mm_packet* pack)
{

}
static void __static_mt_tcp_broken(void* obj)
{

}
static void __static_mt_tcp_nready(void* obj)
{

}
static void __static_mt_tcp_finish(void* obj)
{

}
MM_EXPORT_DLL void mm_mt_tcp_callback_init(struct mm_mt_tcp_callback* p)
{
	p->handle = &__static_mt_tcp_handle;
	p->broken = &__static_mt_tcp_broken;
	p->nready = &__static_mt_tcp_nready;
	p->finish = &__static_mt_tcp_finish;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_mt_tcp_callback_destroy(struct mm_mt_tcp_callback* p)
{
	p->handle = &__static_mt_tcp_handle;
	p->broken = &__static_mt_tcp_broken;
	p->nready = &__static_mt_tcp_nready;
	p->finish = &__static_mt_tcp_finish;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mt_tcp_init(struct mm_mt_tcp* p)
{
	struct mm_holder_u32_vpt_alloc holder32_alloc;

	mm_memset(&p->ss_native,0,sizeof(struct sockaddr_storage));
	mm_memset(&p->ss_remote,0,sizeof(struct sockaddr_storage));
	MM_LIST_INIT_HEAD(&p->list);
	mm_holder_u32_vpt_init(&p->holder);
	mm_mt_tcp_callback_init(&p->callback);
	mm_crypto_callback_init(&p->crypto_callback);
	mm_string_init(&p->cache_native_node);
	mm_string_init(&p->cache_remote_node);
	pthread_mutex_init(&p->signal_mutex, NULL);  
	pthread_cond_init(&p->signal_cond, NULL);
	mm_spin_init(&p->list_locker,NULL);
	mm_spin_init(&p->holder_locker,NULL);
	mm_spin_init(&p->locker,NULL);
	pthread_key_create(&p->thread_key, NULL);
	p->cache_remote_port = 0;

	p->nonblock_timeout = MM_NET_TCP_NONBLOCK_TIMEOUT;// tcp nonblock timeout.default is MM_NET_TCP_NONBLOCK_TIMEOUT milliseconds.
	p->trytimes = -1;// try bind and listen times.default is -1.
	p->unique_id = 0;// unique_id.
	p->state = ts_closed;// mm_thread_state_t,default is ts_closed(0)

	holder32_alloc.alloc = &mm_holder_u32_vpt_weak_alloc;
	holder32_alloc.relax = &mm_holder_u32_vpt_weak_relax;
	holder32_alloc.obj = p;
	mm_holder_u32_vpt_assign_alloc(&p->holder,&holder32_alloc);
}
MM_EXPORT_DLL void mm_mt_tcp_destroy(struct mm_mt_tcp* p)
{
	mm_mt_tcp_clear_callback_holder(p);
	mm_mt_tcp_clear(p);
	//
	mm_memset(&p->ss_native,0,sizeof(struct sockaddr_storage));
	mm_memset(&p->ss_remote,0,sizeof(struct sockaddr_storage));
	MM_LIST_INIT_HEAD(&p->list);
	mm_holder_u32_vpt_destroy(&p->holder);
	mm_mt_tcp_callback_destroy(&p->callback);
	mm_crypto_callback_destroy(&p->crypto_callback);
	mm_string_destroy(&p->cache_native_node);
	mm_string_destroy(&p->cache_remote_node);
	pthread_mutex_destroy(&p->signal_mutex);  
	pthread_cond_destroy(&p->signal_cond);
	mm_spin_destroy(&p->list_locker);
	mm_spin_destroy(&p->holder_locker);
	mm_spin_destroy(&p->locker);
	pthread_key_delete(p->thread_key);
	p->cache_remote_port = 0;

	p->nonblock_timeout = 0;
	p->trytimes = 0;
	p->unique_id = 0;
	p->state = ts_closed;
}
//////////////////////////////////////////////////////////////////////////
// assign native address but not connect.
MM_EXPORT_DLL void mm_mt_tcp_assign_native(struct mm_mt_tcp* p,const char* node,mm_ushort_t port)
{
	struct mm_list_head* pos = NULL;
	mm_spin_lock(&p->list_locker);
	mm_sockaddr_storage_assign(&p->ss_native, node, port);
	mm_list_for_each(pos,&p->list)
	{
		struct mm_list_vpt_iterator* e =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
		struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(e->v);
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_assign_native(net_tcp, node, port);
		mm_net_tcp_unlock(net_tcp);
	}
	mm_spin_unlock(&p->list_locker);
}
// assign remote address but not connect.
MM_EXPORT_DLL void mm_mt_tcp_assign_remote(struct mm_mt_tcp* p,const char* node,mm_ushort_t port)
{
	struct mm_list_head* pos = NULL;
	mm_spin_lock(&p->list_locker);
	mm_sockaddr_storage_assign(&p->ss_remote, node, port);
	mm_list_for_each(pos,&p->list)
	{
		struct mm_list_vpt_iterator* e =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
		struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(e->v);
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_assign_remote(net_tcp, node, port);
		mm_net_tcp_unlock(net_tcp);
	}
	mm_spin_unlock(&p->list_locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mt_tcp_assign_callback(struct mm_mt_tcp* p,mm_uint32_t id,net_tcp_handle callback)
{
	// we not need assign all net tcp instance,but use default and hander by p->holder.
	mm_spin_lock(&p->holder_locker);
	mm_holder_u32_vpt_set(&p->holder,id,(void*)callback);
	mm_spin_unlock(&p->holder_locker);
}
MM_EXPORT_DLL void mm_mt_tcp_assign_default_callback(struct mm_mt_tcp* p,struct mm_mt_tcp_callback* mt_tcp_callback)
{
	assert(NULL != mt_tcp_callback && "you can not assign null mt_tcp_callback.");
	p->callback = *mt_tcp_callback;
}
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_mt_tcp_assign_crypto_callback(struct mm_mt_tcp* p,struct mm_crypto_callback* crypto_callback)
{
	struct mm_list_head* pos = NULL;
	mm_spin_lock(&p->list_locker);
	assert(NULL != crypto_callback && "you can not assign null crypto_callback.");
	p->crypto_callback = *crypto_callback;
	mm_list_for_each(pos,&p->list)
	{
		struct mm_list_vpt_iterator* e =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
		struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(e->v);
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_assign_crypto_callback(net_tcp, &p->crypto_callback);
		mm_net_tcp_unlock(net_tcp);
	}
	mm_spin_unlock(&p->list_locker);
}
// assign context handle.
MM_EXPORT_DLL void mm_mt_tcp_assign_context(struct mm_mt_tcp* p,void* u)
{
	struct mm_list_head* pos = NULL;
	mm_spin_lock(&p->list_locker);
	p->u = u;
	mm_list_for_each(pos,&p->list)
	{
		struct mm_list_vpt_iterator* e =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
		struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(e->v);
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_assign_context(net_tcp, p->u);
		mm_net_tcp_unlock(net_tcp);
	}
	mm_spin_unlock(&p->list_locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mt_tcp_clear_callback_holder(struct mm_mt_tcp* p)
{
	mm_spin_lock(&p->holder_locker);
	mm_holder_u32_vpt_clear(&p->holder);
	mm_spin_unlock(&p->holder_locker);
}
//////////////////////////////////////////////////////////////////////////
// lock to make sure the mt_tcp is thread safe.
MM_EXPORT_DLL void mm_mt_tcp_lock(struct mm_mt_tcp* p)
{
	mm_spin_lock(&p->locker);
}
// unlock mt_tcp.
MM_EXPORT_DLL void mm_mt_tcp_unlock(struct mm_mt_tcp* p)
{
	mm_spin_unlock(&p->locker);
}
//////////////////////////////////////////////////////////////////////////
// fopen socket.
MM_EXPORT_DLL void mm_mt_tcp_fopen_socket(struct mm_mt_tcp* p)
{
	struct mm_list_head* pos = NULL;
	mm_spin_lock(&p->list_locker);
	mm_list_for_each(pos,&p->list)
	{
		struct mm_list_vpt_iterator* e =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
		struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(e->v);
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_fopen_socket(net_tcp);
		mm_net_tcp_unlock(net_tcp);
	}
	mm_spin_unlock(&p->list_locker);
}
// synchronize connect.can call this as we already open connect check thread.
MM_EXPORT_DLL void mm_mt_tcp_connect(struct mm_mt_tcp* p)
{
	struct mm_list_head* pos = NULL;
	mm_spin_lock(&p->list_locker);
	mm_list_for_each(pos,&p->list)
	{
		struct mm_list_vpt_iterator* e =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
		struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(e->v);
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_connect(net_tcp);
		mm_net_tcp_unlock(net_tcp);
	}
	mm_spin_unlock(&p->list_locker);
}
// close socket.
MM_EXPORT_DLL void mm_mt_tcp_close_socket(struct mm_mt_tcp* p)
{
	struct mm_list_head* pos = NULL;
	mm_spin_lock(&p->list_locker);
	mm_list_for_each(pos,&p->list)
	{
		struct mm_list_vpt_iterator* e =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
		struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(e->v);
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_close_socket(net_tcp);
		mm_net_tcp_unlock(net_tcp);
	}
	mm_spin_unlock(&p->list_locker);
}
//////////////////////////////////////////////////////////////////////////
// check connect and reconnect if disconnect.not thread safe.
MM_EXPORT_DLL void mm_mt_tcp_check(struct mm_mt_tcp* p)
{
	struct mm_list_head* pos = NULL;
	mm_spin_lock(&p->list_locker);
	mm_list_for_each(pos,&p->list)
	{
		struct mm_list_vpt_iterator* e =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
		struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(e->v);
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_check(net_tcp);
		mm_net_tcp_unlock(net_tcp);
	}
	mm_spin_unlock(&p->list_locker);
}
//////////////////////////////////////////////////////////////////////////
// synchronize attach to socket.not thread safe.
MM_EXPORT_DLL void mm_mt_tcp_attach_socket(struct mm_mt_tcp* p)
{
	struct mm_list_head* pos = NULL;
	mm_spin_lock(&p->list_locker);
	mm_list_for_each(pos,&p->list)
	{
		struct mm_list_vpt_iterator* e =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
		struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(e->v);
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_attach_socket(net_tcp);
		mm_net_tcp_unlock(net_tcp);
	}
	mm_spin_unlock(&p->list_locker);
}
// synchronize detach to socket.not thread safe.
MM_EXPORT_DLL void mm_mt_tcp_detach_socket(struct mm_mt_tcp* p)
{
	struct mm_list_head* pos = NULL;
	mm_spin_lock(&p->list_locker);
	mm_list_for_each(pos,&p->list)
	{
		struct mm_list_vpt_iterator* e =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
		struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(e->v);
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_detach_socket(net_tcp);
		mm_net_tcp_unlock(net_tcp);
	}
	mm_spin_unlock(&p->list_locker);
}
//////////////////////////////////////////////////////////////////////////
// get current thread net tcp.
MM_EXPORT_DLL struct mm_net_tcp* mm_mt_tcp_thread_instance(struct mm_mt_tcp* p)
{
	struct mm_net_tcp* net_tcp = NULL;
	struct mm_list_vpt_iterator* lvp  = NULL;
	lvp = (struct mm_list_vpt_iterator*)pthread_getspecific(p->thread_key);
	if (NULL == lvp)
	{
		struct mm_net_tcp_callback net_tcp_callback;
		lvp = (struct mm_list_vpt_iterator*)mm_malloc(sizeof(struct mm_list_vpt_iterator));
		mm_list_vpt_iterator_init(lvp);
		pthread_setspecific(p->thread_key, lvp);
		net_tcp = (struct mm_net_tcp*)mm_malloc(sizeof(struct mm_net_tcp));
		mm_net_tcp_init(net_tcp);
		mm_net_tcp_assign_native_storage(net_tcp,&p->ss_native);
		mm_net_tcp_assign_remote_storage(net_tcp,&p->ss_remote);
		mm_net_tcp_assign_context(net_tcp,p->u);

		net_tcp_callback.handle = &__static_mt_tcp_net_tcp_handle;
		net_tcp_callback.broken = &__static_mt_tcp_net_tcp_broken;
		net_tcp_callback.nready = &__static_mt_tcp_net_tcp_nready;
		net_tcp_callback.finish = &__static_mt_tcp_net_tcp_finish;
		net_tcp_callback.obj = p;
		mm_net_tcp_assign_net_tcp_callback(net_tcp,&net_tcp_callback);

		mm_net_tcp_assign_crypto_callback(net_tcp,&p->crypto_callback);
		//
		if ( ts_motion == p->state )
		{
			// if current route is in motion we start the new net tcp .
			mm_net_tcp_start(net_tcp);
		}
		else
		{
			// if current route is not motion,we assign the same state.
			net_tcp->state = p->state;
		}
		//
		lvp->v = net_tcp;
		mm_spin_lock(&p->list_locker);
		mm_list_add(&lvp->n, &p->list);
		mm_spin_unlock(&p->list_locker);
	}
	else
	{
		net_tcp = (struct mm_net_tcp*)(lvp->v);
	}
	return net_tcp;
}
// clear all net tcp.
MM_EXPORT_DLL void mm_mt_tcp_clear(struct mm_mt_tcp* p)
{
	struct mm_list_head* pos = NULL;
	struct mm_list_vpt_iterator* lvp =NULL;
	struct mm_net_tcp* net_tcp = NULL;
	mm_spin_lock(&p->list_locker);
	pos = p->list.next;
	while(pos != &p->list)
	{
		struct mm_list_head* curr = pos;
		pos = pos->next;
		lvp =(struct mm_list_vpt_iterator*)mm_list_entry(curr, struct mm_list_vpt_iterator, n);
		mm_list_del(curr);
		net_tcp = (struct mm_net_tcp*)(lvp->v);
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_detach_socket(net_tcp);
		mm_net_tcp_unlock(net_tcp);
		mm_net_tcp_destroy(net_tcp);
		mm_free(net_tcp);
		mm_list_vpt_iterator_destroy(lvp);
		mm_free(lvp);
	}
	mm_spin_unlock(&p->list_locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mt_tcp_start(struct mm_mt_tcp* p)
{
	struct mm_list_head* pos = NULL;
	mm_spin_lock(&p->list_locker);
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	mm_list_for_each(pos,&p->list)
	{
		struct mm_list_vpt_iterator* e =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
		struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(e->v);
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_start(net_tcp);
		mm_net_tcp_unlock(net_tcp);
	}
	mm_spin_unlock(&p->list_locker);
}
MM_EXPORT_DLL void mm_mt_tcp_interrupt(struct mm_mt_tcp* p)
{
	struct mm_list_head* pos = NULL;
	mm_spin_lock(&p->list_locker);
	p->state = ts_closed;
	mm_list_for_each(pos,&p->list)
	{
		struct mm_list_vpt_iterator* e =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
		struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(e->v);
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_interrupt(net_tcp);
		mm_net_tcp_unlock(net_tcp);
	}
	mm_spin_unlock(&p->list_locker);
	//
	pthread_mutex_lock(&p->signal_mutex);
	pthread_cond_signal(&p->signal_cond);
	pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mm_mt_tcp_shutdown(struct mm_mt_tcp* p)
{
	struct mm_list_head* pos = NULL;
	mm_spin_lock(&p->list_locker);
	p->state = ts_finish;
	mm_list_for_each(pos,&p->list)
	{
		struct mm_list_vpt_iterator* e =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
		struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(e->v);
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_shutdown(net_tcp);
		mm_net_tcp_unlock(net_tcp);
	}
	mm_spin_unlock(&p->list_locker);
	//
	pthread_mutex_lock(&p->signal_mutex);
	pthread_cond_signal(&p->signal_cond);
	pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mm_mt_tcp_join(struct mm_mt_tcp* p)
{
	struct mm_list_head* pos = NULL;
	if (ts_motion == p->state)
	{
		// we can not lock or join until cond wait all thread is shutdown.
		pthread_mutex_lock(&p->signal_mutex);
		pthread_cond_wait(&p->signal_cond,&p->signal_mutex);
		pthread_mutex_unlock(&p->signal_mutex);
	}
	//
	mm_spin_lock(&p->list_locker);
	mm_list_for_each(pos,&p->list)
	{
		struct mm_list_vpt_iterator* e =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
		struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(e->v);
		mm_net_tcp_lock(net_tcp);
		mm_net_tcp_join(net_tcp);
		mm_net_tcp_unlock(net_tcp);
	}
	mm_spin_unlock(&p->list_locker);
}
//////////////////////////////////////////////////////////////////////////
static void __static_mt_tcp_net_tcp_handle(void* obj, void* u, struct mm_packet* pack)
{
	struct mm_tcp* tcp = (struct mm_tcp*)obj;
	struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(tcp->callback.obj);
	struct mm_mt_tcp* mt_tcp = (struct mm_mt_tcp*)(net_tcp->callback.obj);
	net_tcp_handle handle = NULL;
	mm_spin_lock(&mt_tcp->holder_locker);
	handle = (net_tcp_handle)mm_holder_u32_vpt_get(&mt_tcp->holder,pack->phead.mid);
	mm_spin_unlock(&mt_tcp->holder_locker);
	if (NULL != handle)
	{
		// fire the handle event.
		(*(handle))(tcp,u,pack);
	}
	else
	{
		assert(NULL != mt_tcp->callback.handle && "mt_tcp->callback.handle is a null.");
		(*(mt_tcp->callback.handle))(tcp,u,pack);
	}
}
static void __static_mt_tcp_net_tcp_broken(void* obj)
{
	struct mm_tcp* tcp = (struct mm_tcp*)obj;
	struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(tcp->callback.obj);
	struct mm_mt_tcp* mt_tcp = (struct mm_mt_tcp*)(net_tcp->callback.obj);
	assert(NULL != mt_tcp->callback.broken && "mt_tcp->callback.broken is a null.");
	(*(mt_tcp->callback.broken))(tcp);
}
static void __static_mt_tcp_net_tcp_nready(void* obj)
{
	struct mm_tcp* tcp = (struct mm_tcp*)obj;
	struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(tcp->callback.obj);
	struct mm_mt_tcp* mt_tcp = (struct mm_mt_tcp*)(net_tcp->callback.obj);
	assert(NULL != mt_tcp->callback.nready && "mt_tcp->callback.nready is a null.");
	(*(mt_tcp->callback.nready))(tcp);
}
static void __static_mt_tcp_net_tcp_finish(void* obj)
{
	struct mm_tcp* tcp = (struct mm_tcp*)obj;
	struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(tcp->callback.obj);
	struct mm_mt_tcp* mt_tcp = (struct mm_mt_tcp*)(net_tcp->callback.obj);
	assert(NULL != mt_tcp->callback.finish && "mt_tcp->callback.finish is a null.");
	(*(mt_tcp->callback.finish))(tcp);
}
//////////////////////////////////////////////////////////////////////////
