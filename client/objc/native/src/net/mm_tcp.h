#ifndef __mm_tcp_h__
#define __mm_tcp_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_streambuf.h"
#include "core/mm_socket.h"

#include "net/mm_addr.h"

enum mm_tcp_mid
{
	tcp_mid_broken = 0x0F000000,
	tcp_mid_nready = 0x0F000001,
	tcp_mid_finish = 0x0F000002,
};

//
// obj is mm_tcp.
typedef void (*tcp_handle)( void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length );
typedef void (*tcp_broken)( void* obj );
//
struct mm_tcp_callback
{
	tcp_handle handle;
	tcp_broken broken;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_tcp_callback_init(struct mm_tcp_callback* p);
MM_EXPORT_DLL void mm_tcp_callback_destroy(struct mm_tcp_callback* p);
//
struct mm_tcp
{
	struct mm_addr addr;// strong ref. tcp address. 
	struct mm_streambuf buff_recv;// strong ref.
	struct mm_streambuf buff_send;// strong ref.
	struct mm_tcp_callback callback;// value ref. transport callback.
	mm_uint64_t unique_id;// feedback unique_id.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_tcp_init(struct mm_tcp* p);
MM_EXPORT_DLL void mm_tcp_destroy(struct mm_tcp* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_tcp_lock(struct mm_tcp* p);
MM_EXPORT_DLL void mm_tcp_unlock(struct mm_tcp* p);
//////////////////////////////////////////////////////////////////////////
// assign addr native by ip port.
MM_EXPORT_DLL void mm_tcp_assign_native(struct mm_tcp* p, const char* node, mm_ushort_t port);
// assign addr remote by ip port.
MM_EXPORT_DLL void mm_tcp_assign_remote(struct mm_tcp* p, const char* node, mm_ushort_t port);
// assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
MM_EXPORT_DLL void mm_tcp_assign_native_storage(struct mm_tcp* p, struct sockaddr_storage* ss_native);
// assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
MM_EXPORT_DLL void mm_tcp_assign_remote_storage(struct mm_tcp* p, struct sockaddr_storage* ss_remote);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_tcp_assign_callback(struct mm_tcp* p,struct mm_tcp_callback* cb);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_tcp_set_unique_id(struct mm_tcp* p, mm_uint64_t unique_id);
MM_EXPORT_DLL mm_uint64_t mm_tcp_get_unique_id(struct mm_tcp* p);
// context for tcp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_DLL void mm_tcp_set_context(struct mm_tcp* p, void* u);
MM_EXPORT_DLL void* mm_tcp_get_context(struct mm_tcp* p);
//////////////////////////////////////////////////////////////////////////
// fopen socket.ss_remote.ss_family,SOCK_STREAM,0 
MM_EXPORT_DLL void mm_tcp_fopen_socket(struct mm_tcp* p);
// close socket.
MM_EXPORT_DLL void mm_tcp_close_socket(struct mm_tcp* p);
// shutdown socket.
MM_EXPORT_DLL void mm_tcp_shutdown_socket(struct mm_tcp* p, int opcode);
//////////////////////////////////////////////////////////////////////////
// handle recv for buffer pool and pool max size.
MM_EXPORT_DLL void mm_tcp_handle_recv(struct mm_tcp* p, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length);
// handle send for buffer pool and pool max size.
MM_EXPORT_DLL void mm_tcp_handle_send(struct mm_tcp* p, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length);
//////////////////////////////////////////////////////////////////////////
// handle recv data from buffer and buffer length.0 success -1 failure.
MM_EXPORT_DLL int mm_tcp_buffer_recv(struct mm_tcp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length);
// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_tcp_buffer_send(struct mm_tcp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length);
//////////////////////////////////////////////////////////////////////////
// handle send data by flush send buffer.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_tcp_flush_send(struct mm_tcp* p);
//////////////////////////////////////////////////////////////////////////
// tcp streambuf recv send reset.
MM_EXPORT_DLL void mm_tcp_streambuf_reset(struct mm_tcp* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_tcp_h__