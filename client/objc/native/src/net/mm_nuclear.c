#include "net/mm_nuclear.h"
#include "core/mm_logger.h"
#include "core/mm_string.h"
#include "core/mm_errno.h"

static void* __static_nuclear_poll_wait_thread(void* _arg);
//////////////////////////////////////////////////////////////////////////
static void __static_nuclear_handle_recv(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length)
{

}
static void __static_nuclear_handle_send(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length)
{

}
MM_EXPORT_DLL void mm_nuclear_callback_init(struct mm_nuclear_callback* p)
{
	p->handle_recv = &__static_nuclear_handle_recv;
	p->handle_send = &__static_nuclear_handle_send;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_nuclear_callback_destroy(struct mm_nuclear_callback* p)
{
	p->handle_recv = &__static_nuclear_handle_recv;
	p->handle_send = &__static_nuclear_handle_send;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_nuclear_init(struct mm_nuclear* p)
{
	struct mm_holder_u32_vpt_alloc holder32_alloc;

	mm_holder_u32_vpt_init(&p->holder);
	mm_nuclear_callback_init(&p->callback);
	mm_poll_init(&p->poll);// poller.
	pthread_mutex_init(&p->cond_locker, NULL);
	pthread_cond_init(&p->cond_not_null, NULL);
	pthread_cond_init(&p->cond_not_full, NULL);
	mm_spin_init(&p->locker, NULL);
	p->poll_length = MM_NUCLEAR_POLL_LENGTH;// poll length.
	p->poll_timeout = MM_NUCLEAR_IDLE_SLEEP_MSEC;// poll timeout.
	p->size = 0;// addr size.
	p->max_size = -1;// -1 is default limit size_t value.
	p->state = ts_closed;

	holder32_alloc.alloc = &mm_holder_u32_vpt_weak_alloc;
	holder32_alloc.relax = &mm_holder_u32_vpt_weak_relax;
	holder32_alloc.obj = p;
	mm_holder_u32_vpt_assign_alloc(&p->holder,&holder32_alloc);
}
MM_EXPORT_DLL void mm_nuclear_destroy(struct mm_nuclear* p)
{
	mm_holder_u32_vpt_destroy(&p->holder);
	mm_nuclear_callback_destroy(&p->callback);
	mm_poll_destroy(&p->poll);
	pthread_mutex_destroy(&p->cond_locker);
	pthread_cond_destroy(&p->cond_not_null);
	pthread_cond_destroy(&p->cond_not_full);
	mm_spin_destroy(&p->locker);

	p->poll_length = 0;
	p->poll_timeout = 0;
	p->size = 0;
	p->max_size = 0;
	p->state = ts_closed;
}
// lock to make sure the knot is thread safe.
MM_EXPORT_DLL void mm_nuclear_lock(struct mm_nuclear* p)
{
	mm_spin_lock(&p->locker);
}
// unlock knot.
MM_EXPORT_DLL void mm_nuclear_unlock(struct mm_nuclear* p)
{
	mm_spin_unlock(&p->locker);
}
MM_EXPORT_DLL void mm_nuclear_assign_callback(struct mm_nuclear* p,struct mm_nuclear_callback* callback)
{
	assert(NULL != callback && "you can not assign null callback.");
	p->callback = (*callback);
}
//////////////////////////////////////////////////////////////////////////
// poll size.
MM_EXPORT_DLL size_t mm_nuclear_poll_size(struct mm_nuclear* p)
{
	return p->size;
}
//////////////////////////////////////////////////////////////////////////
// wait for activation fd.
MM_EXPORT_DLL void mm_nuclear_poll_wait(struct mm_nuclear* p)
{
	struct mm_poll_event* pe = NULL;
	mm_uint8_t buffer[MM_NUCLEAR_PAGE_SIZE];
	mm_int_t real_len = 0;
	int i = 0;
	struct mm_logger* g_logger = mm_logger_instance();
	mm_poll_set_length(&p->poll, p->poll_length);
	//
	while( ts_motion == p->state )
	{
		// the first quick size checking.
		if ( 0 == p->size )
		{
			pthread_mutex_lock(&p->cond_locker);
			// double lock checking.
			// because the first quick size checking is not thread safe.
			// here we lock and check the size once again.
			if (0 == p->size)
			{
				mm_logger_log_I(g_logger,"%s %d %p wait.",__FUNCTION__,__LINE__,p);
				pthread_cond_wait(&p->cond_not_null,&p->cond_locker);
				mm_logger_log_I(g_logger,"%s %d %p wake.",__FUNCTION__,__LINE__,p);
			}
			pthread_mutex_unlock(&p->cond_locker);
			continue;
		}
		real_len = mm_poll_wait_event(&p->poll,p->poll_timeout);
		if ( 0 == real_len )
		{
			continue;
		}
		else if( -1 == real_len )
		{
			mm_err_t errcode = mm_errno;
			if (MM_ENOTSOCK == errcode)
			{
				mm_logger_log_E(g_logger,"%s %d %p error occur.",__FUNCTION__,__LINE__,p);
				mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
				break;
			}
			else
			{
				mm_logger_log_E(g_logger,"%s %d %p error occur.",__FUNCTION__,__LINE__,p);
				mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
				continue;
			}
		}
		for ( i = 0; i < real_len ; ++i )
		{
			pe = &p->poll.pe[i];
			assert(NULL != pe->s && "pe->s is a null.");
			//
			if (pe->mask & MM_PE_READABLE)
			{
				(*p->callback.handle_recv)(p,pe->s,buffer,0,MM_NUCLEAR_PAGE_SIZE);
			}
			if (pe->mask & MM_PE_WRITABLE)
			{
				(*p->callback.handle_send)(p,pe->s,buffer,0,MM_NUCLEAR_PAGE_SIZE);
			}
		}
	}
}
//////////////////////////////////////////////////////////////////////////
// start wait thread.
MM_EXPORT_DLL void mm_nuclear_start(struct mm_nuclear* p)
{
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	pthread_create(&p->poll_thread, NULL, &__static_nuclear_poll_wait_thread, p);
}
// interrupt wait thread.
MM_EXPORT_DLL void mm_nuclear_interrupt(struct mm_nuclear* p)
{
	p->state = ts_closed;
	pthread_mutex_lock(&p->cond_locker);
	pthread_cond_signal(&p->cond_not_null);
	pthread_cond_signal(&p->cond_not_full);
	pthread_mutex_unlock(&p->cond_locker);
}
// shutdown wait thread.
MM_EXPORT_DLL void mm_nuclear_shutdown(struct mm_nuclear* p)
{
	p->state = ts_finish;
	pthread_mutex_lock(&p->cond_locker);
	pthread_cond_signal(&p->cond_not_null);
	pthread_cond_signal(&p->cond_not_full);
	pthread_mutex_unlock(&p->cond_locker);
}
// join wait thread.
MM_EXPORT_DLL void mm_nuclear_join(struct mm_nuclear* p)
{
	pthread_join(p->poll_thread, NULL);
}
//////////////////////////////////////////////////////////////////////////
// add u into poll_fd.you must add rmv in pairs.
MM_EXPORT_DLL void mm_nuclear_fd_add(struct mm_nuclear* p, mm_socket_t fd, void* u)
{
	do 
	{
		if (MM_INVALID_SOCKET == fd)
		{
			// the socket is invalid.do nothing.
			break;
		}
		//
		if ( p->max_size <= p->size )
		{
			// pthread_cond_wait(&p->cond_not_null,&p->locker);
			// nothing need remove and not need cond wait at all.
			struct mm_logger* g_logger = mm_logger_instance();
			mm_logger_log_T(g_logger,"%s %d we can not fd add into full poll.",__FUNCTION__,__LINE__);
			break;
		}
		mm_poll_add_event(&p->poll,fd,MM_PE_READABLE,u);
		// add addr to rbt.
		pthread_mutex_lock(&p->cond_locker);
		mm_holder_u32_vpt_set(&p->holder,fd,u);
		p->size ++;
		pthread_cond_signal(&p->cond_not_null);
		pthread_mutex_unlock(&p->cond_locker);
	} while (0);
}
// rmv u from poll_fd.you must add rmv in pairs.
MM_EXPORT_DLL void mm_nuclear_fd_rmv(struct mm_nuclear* p, mm_socket_t fd, void* u)
{
	do 
	{
		if (MM_INVALID_SOCKET == fd)
		{
			// the socket is invalid.do nothing.
			break;
		}
		//
		if ( 0 >= p->size )
		{
			// pthread_cond_wait(&p->cond_not_full,&p->locker);
			// nothing need remove and not need cond wait at all.
			struct mm_logger* g_logger = mm_logger_instance();
			mm_logger_log_T(g_logger,"%s %d we can not fd rmv from null poll.",__FUNCTION__,__LINE__);
			break;
		}
		mm_poll_del_event( &p->poll, fd, MM_PE_READABLE | MM_PE_WRITABLE, u );
		// rmv addr from rbt.
		pthread_mutex_lock(&p->cond_locker);
		mm_holder_u32_vpt_rmv(&p->holder,fd);
		p->size --;
		pthread_cond_signal(&p->cond_not_full);
		pthread_mutex_unlock(&p->cond_locker);
	} while (0);
}
// mod u event from poll wait.
MM_EXPORT_DLL void mm_nuclear_fd_mod(struct mm_nuclear* p, mm_socket_t fd, void* u, int flag)
{
	mm_poll_mod_event(&p->poll,fd,flag,u);
}
// get u event from poll wait.
MM_EXPORT_DLL void* mm_nuclear_fd_get(struct mm_nuclear* p, mm_socket_t fd)
{
	void* e = NULL;
	pthread_mutex_lock(&p->cond_locker);
	e = mm_holder_u32_vpt_get(&p->holder,fd);
	pthread_mutex_unlock(&p->cond_locker);
	return e;
}
//////////////////////////////////////////////////////////////////////////
// add u into poll_fd.not cond and locker.you must lock it outside manual.
MM_EXPORT_DLL void mm_nuclear_fd_add_poll(struct mm_nuclear* p, mm_socket_t fd, void* u)
{
	mm_poll_add_event(&p->poll,fd,MM_PE_READABLE,u);
	mm_holder_u32_vpt_set(&p->holder,fd,u);
	p->size ++;
}
// rmv u from poll_fd..not cond and locker.you must lock it outside manual.
MM_EXPORT_DLL void mm_nuclear_fd_rmv_poll(struct mm_nuclear* p, mm_socket_t fd, void* u)
{
	mm_poll_del_event( &p->poll, fd, MM_PE_READABLE | MM_PE_WRITABLE, u );
	mm_holder_u32_vpt_rmv(&p->holder,fd);
	p->size --;
}
// signal.
MM_EXPORT_DLL void mm_nuclear_fd_poll_signal(struct mm_nuclear* p)
{
	pthread_mutex_lock(&p->cond_locker);
	pthread_cond_signal(&p->cond_not_null);
	pthread_cond_signal(&p->cond_not_full);
	pthread_mutex_unlock(&p->cond_locker);
}
//////////////////////////////////////////////////////////////////////////
// this function is used for copy current fd holder.
MM_EXPORT_DLL void mm_nuclear_fd_copy_holder(struct mm_nuclear* p,struct mm_holder_u32_vpt* holder)
{
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	// clear the holder first.
	mm_holder_u32_vpt_clear(holder);
	//
	pthread_mutex_lock(&p->cond_locker);
	n = mm_rb_first(&p->holder.rbt);
	while(NULL != n)
	{
		it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n, struct mm_holder_u32_vpt_iterator, n);
		n = mm_rb_next(n);
		mm_holder_u32_vpt_set(holder,it->k,it->v);
	}
	pthread_mutex_unlock(&p->cond_locker);
}
//////////////////////////////////////////////////////////////////////////
static void* __static_nuclear_poll_wait_thread(void* arg)
{
	struct mm_nuclear* p = (struct mm_nuclear*)(arg);
	mm_nuclear_poll_wait(p);
	return NULL;
}