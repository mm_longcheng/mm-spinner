#ifndef __mm_packet_h__
#define __mm_packet_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

// Ethernet len[46~1500] MTU 20 
// udp head  8 1500 - 20 -  8 = 1472
// tcp head 20 1500 - 20 - 20 = 1460
// we define packet page size to 1024.
#define MM_PACKET_PAGE_SIZE 1024

// we not use typedef a new name for this type.
// because uint16 uint32 is common type when cross language.

// msg_size    type is mm_uint16_t
// msg_id      type is mm_uint32_t
// proxy_id    type is mm_uint32_t
// session_id  type is mm_uint64_t
// unique_id   type is mm_uint64_t
// msg_head    type is mm_uint32_t = 2 * msg_size

// msg pack struct.
// |-mm_uint16_t-|-mm_uint16_t-|---char*---|---char*---|

// use this quick decode encode.
// |--------mm_uint32_t--------|---char*---|---char*---|

// |-------------|-------------------------|-----------|
// |--msg_size---|--head_size--|-head_data-|-body_data-|
// |---------head_data---------|-head_data-|-body_data-|

// package_size = 2 * sizeof(mm_msg_size_t) + head_size + body_size

#define MM_MSG_SIZE_BIT_NUM 16
#define MM_MSG_HEAD_H_MASK 0xFFFF0000 // hight mask
#define MM_MSG_HEAD_L_MASK 0x0000FFFF // low   mask

// the hsize must use sizeof(T),make sure the package byte aligned.

// msg base head size.
// mm_uint32_t   mid;// msg     id.
// byte aligned this struct sizeof is 4.
#define MM_MSG_BASE_HEAD_SIZE 4
// mm_uint32_t   mid;// msg     id.
// mm_uint32_t   pid;// proxy   id.
// mm_uint64_t   sid;// section id.
// mm_uint64_t   uid;// unique  id.
// byte aligned this struct sizeof is 24.
#define MM_MSG_COMM_HEAD_SIZE 24

struct mm_packet_head
{
	// msg base head.include into head data.
	mm_uint32_t   mid;// msg     id.
	mm_uint32_t   pid;// proxy   id.
	mm_uint64_t   sid;// section id.
	mm_uint64_t   uid;// unique  id.
};
MM_EXPORT_DLL void mm_packet_head_init(struct mm_packet_head* p);
MM_EXPORT_DLL void mm_packet_head_destroy(struct mm_packet_head* p);

struct mm_packet_buff
{
	mm_uint32_t length;// packet buff length.
	mm_uint32_t offset;// packet buff offset.
	mm_uint8_t* buffer;// packet buff buffer.
};
MM_EXPORT_DLL void mm_packet_buff_init(struct mm_packet_buff* p);
MM_EXPORT_DLL void mm_packet_buff_destroy(struct mm_packet_buff* p);

// packet struct.
struct mm_packet
{
	struct mm_packet_head phead;// packet head.
	struct mm_packet_buff hbuff;// buffer for head.
	struct mm_packet_buff bbuff;// buffer for body.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_packet_init(struct mm_packet* p);
MM_EXPORT_DLL void mm_packet_destroy(struct mm_packet* p);
//////////////////////////////////////////////////////////////////////////
// reset all to zero.
MM_EXPORT_DLL void mm_packet_reset(struct mm_packet* p);
// use for quick zero head base.(mid,uid,sid,pid)
MM_EXPORT_DLL void mm_packet_head_base_zero(struct mm_packet* p);
// use for quick copy head base r-->p.(mid,uid,sid,pid)
MM_EXPORT_DLL void mm_packet_head_base_copy(struct mm_packet* p, struct mm_packet* r);
// use for quick decode head base.depend on hsize (mid)/(mid,uid,sid,pid)/(other).
// p : struct mm_packet*
MM_EXPORT_DLL void mm_packet_head_base_decode(struct mm_packet* p);// mm_memcpy((p),(p)->hbuff,(p)->hsize)
// use for quick encode head base.depend on hsize (mid)/(mid,uid,sid,pid)/(other).
// p : struct mm_packet*
MM_EXPORT_DLL void mm_packet_head_base_encode(struct mm_packet* p);// mm_memcpy((p)->hbuff,(p),(p)->hsize)
//////////////////////////////////////////////////////////////////////////
// copy data from p to t.will realloc the hbuff and bbuff.
MM_EXPORT_DLL void mm_packet_copy_realloc(struct mm_packet* p, struct mm_packet* t);
// copy data from p to t.will alloc the hbuff and bbuff.
MM_EXPORT_DLL void mm_packet_copy_alloc(struct mm_packet* p, struct mm_packet* t);
// copy data from p to t.not realloc the hbuff and bbuff.
MM_EXPORT_DLL void mm_packet_copy_weak(struct mm_packet* p, struct mm_packet* t);
// free copy realloc data from.
MM_EXPORT_DLL void mm_packet_free_copy_realloc(struct mm_packet* p);
// free copy alloc data from.
MM_EXPORT_DLL void mm_packet_free_copy_alloc(struct mm_packet* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_packet_shadow_alloc(struct mm_packet* p);
MM_EXPORT_DLL void mm_packet_shadow_free(struct mm_packet* p);
//////////////////////////////////////////////////////////////////////////
// packet handle function type.
typedef void (*packet_handle_tcp)(void* obj, struct mm_packet* pack);
typedef void (*packet_signal_tcp)(void* obj);
// packet handle function type.
typedef void (*packet_handle_udp)(void* obj, struct mm_packet* pack, struct sockaddr_storage* remote);
typedef void (*packet_signal_udp)(void* obj);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_packet_h__