#ifndef __mm_accepter_h__
#define __mm_accepter_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_thread.h"
#include "core/mm_time.h"

#include "net/mm_addr.h"

#include <pthread.h>

// listen backlog default 5;
#define MM_ACCEPTER_BACKLOG 5
// if bind listen failure will msleep this time.
#define MM_ACCEPTER_TRY_MSLEEP_TIME 1000
// net accept nonblock timeout..
#define MM_ACCEPTER_NONBLOCK_TIMEOUT 1000
// net accept one time try time.
#define MM_NET_ACCEPTER_TRYTIME -1

typedef void (*accepter_accept)( void* obj, mm_socket_t fd, struct sockaddr_storage* remote );
struct mm_accepter_callback
{
	accepter_accept accept;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_accepter_callback_init(struct mm_accepter_callback* p);
MM_EXPORT_DLL void mm_accepter_callback_destroy(struct mm_accepter_callback* p);

enum mm_accepter_state_t
{
	accepter_state_closed = 0,// fd is closed,connect closed.invalid at all.
	accepter_state_motion = 1,// fd is opened,connect closed.at connecting.
	accepter_state_finish = 2,// fd is opened,connect opened.at connected.
	accepter_state_broken = 3,// fd is opened,connect broken.connect is broken fd not closed.
};
// we not use select timeout for accept interrupt.
// we just use close listen socket fd and trigger a socket errno 
// [errno:(53) Software caused connection abort] to interrupt accept.
// this mode will make this class simpleness and negative effects receivability.
struct mm_accepter
{
	struct mm_addr addr;// strong ref. accepter address.
	struct mm_accepter_callback callback;// value ref. accepter callback.
	mm_int_t backlog;// listen backlog,default MM_ACCEPTER_BACKLOG.
	pthread_t accept_thread;// thread.
	int keepalive;// accepter_keepalive_activate true accepter_keepalive_inactive false keepalive the accept fd.default is accepter_keepalive_activate.
	mm_msec_t nonblock_timeout;// tcp nonblock timeout.default is MM_ACCEPTER_NONBLOCK_TIMEOUT milliseconds.
	int accepter_state;// mm_accepter_state_t,default is accepter_state_closed(0)
	mm_uint32_t trytimes;// try bind and listen times.default is -1.
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_accepter_init(struct mm_accepter* p);
MM_EXPORT_DLL void mm_accepter_destroy(struct mm_accepter* p);
//////////////////////////////////////////////////////////////////////////
// assign native address but not connect.
MM_EXPORT_DLL void mm_accepter_assign_native(struct mm_accepter* p,const char* node,mm_ushort_t port);
// assign remote address but not connect.
MM_EXPORT_DLL void mm_accepter_assign_remote(struct mm_accepter* p,const char* node,mm_ushort_t port);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_accepter_assign_callback(struct mm_accepter* p,struct mm_accepter_callback* cb);
//////////////////////////////////////////////////////////////////////////
// reset streambuf and fire event broken.
MM_EXPORT_DLL void mm_accepter_apply_broken(struct mm_accepter* p);
// reset streambuf and fire event broken.
MM_EXPORT_DLL void mm_accepter_apply_finish(struct mm_accepter* p);
//////////////////////////////////////////////////////////////////////////
// fopen socket.ss_native.ss_family,SOCK_STREAM,0
MM_EXPORT_DLL void mm_accepter_fopen_socket(struct mm_accepter* p);
// bind address.
MM_EXPORT_DLL void mm_accepter_bind(struct mm_accepter* p);
// listen address.
MM_EXPORT_DLL void mm_accepter_listen(struct mm_accepter* p);
// accept sync.
MM_EXPORT_DLL void mm_accepter_accept(struct mm_accepter* p);
// close socket. mm_accepter_join before call this.
MM_EXPORT_DLL void mm_accepter_close_socket(struct mm_accepter* p);
//////////////////////////////////////////////////////////////////////////
// start accept thread.
MM_EXPORT_DLL void mm_accepter_start(struct mm_accepter* p);
// interrupt accept thread.
MM_EXPORT_DLL void mm_accepter_interrupt(struct mm_accepter* p);
// shutdown accept thread.
MM_EXPORT_DLL void mm_accepter_shutdown(struct mm_accepter* p);
// join accept thread.
MM_EXPORT_DLL void mm_accepter_join(struct mm_accepter* p);
//////////////////////////////////////////////////////////////////////////

#include "core/mm_suffix.h"

#endif//__mm_accepter_h__
