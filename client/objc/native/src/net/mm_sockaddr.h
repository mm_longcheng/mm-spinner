#ifndef __mm_sockaddr_h__
#define __mm_sockaddr_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_socket.h"

//////////////////////////////////////////////////////////////////////////
#define MM_ADDR_DEFAULT_NODE "::"
#define MM_ADDR_DEFAULT_PORT 0
//////////////////////////////////////////////////////////////////////////
#define MM_NODE_NAME_LENGTH 64
#define MM_PORT_NAME_LENGTH 8
#define MM_SOCK_NAME_LENGTH 16
#define MM_ADDR_NAME_LENGTH 64
#define MM_LINK_NAME_LENGTH 128
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_sockaddr_storage_init( struct sockaddr_storage* p );
MM_EXPORT_DLL void mm_sockaddr_storage_destroy( struct sockaddr_storage* p );
//////////////////////////////////////////////////////////////////////////
// copy t to p.
MM_EXPORT_DLL void mm_sockaddr_storage_copy( struct sockaddr_storage* p, struct sockaddr_storage* t );
//////////////////////////////////////////////////////////////////////////
// decode by string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_DLL void mm_sockaddr_format_decode_string( char addr_name[MM_ADDR_NAME_LENGTH], char node[MM_NODE_NAME_LENGTH], mm_ushort_t* port );
// encode to string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_DLL void mm_sockaddr_format_encode_string( char addr_name[MM_ADDR_NAME_LENGTH], char node[MM_NODE_NAME_LENGTH], mm_ushort_t* port );
//////////////////////////////////////////////////////////////////////////
// family only support ipv4 ipv6.
MM_EXPORT_DLL int mm_sockaddr_storage_string_family( char addr_name[MM_ADDR_NAME_LENGTH] );
//////////////////////////////////////////////////////////////////////////
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_DLL void mm_sockaddr_storage_assign( struct sockaddr_storage* addr, const char* node, mm_ushort_t port );
//////////////////////////////////////////////////////////////////////////
// 2001:0DB8:0000:0023:0008:0800:200C:417A 64 38
// 65535                                   8  5
// addr_name:
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_DLL void mm_sockaddr_storage_string( struct sockaddr_storage* addr, char addr_name[MM_ADDR_NAME_LENGTH] );
// (fd)|ip-port --> ip-port
// ipv6 (65535)|2001:0DB8:0000:0023:0008:0800:200C:417A-65535 --> 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         (65535)|192.168.111.123-65535 --> 192.168.111.123-65535
MM_EXPORT_DLL void mm_sockaddr_storage_native_remote_string( struct sockaddr_storage* native_addr, struct sockaddr_storage* remote_addr, mm_socket_t fd, char link_name[MM_LINK_NAME_LENGTH] );
//////////////////////////////////////////////////////////////////////////
// 0 is same.
MM_EXPORT_DLL int mm_sockaddr_storage_compare( struct sockaddr_storage* addrl, struct sockaddr_storage* addrr );
// 0 is same.
MM_EXPORT_DLL int mm_sockaddr_storage_compare_address( struct sockaddr_storage* addr, const char* node, mm_ushort_t port );
//////////////////////////////////////////////////////////////////////////
// decode by string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_DLL void mm_sockaddr_storage_decode_string( struct sockaddr_storage* addr, char addr_name[MM_ADDR_NAME_LENGTH] );
// encode to string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_DLL void mm_sockaddr_storage_encode_string( struct sockaddr_storage* addr, char addr_name[MM_ADDR_NAME_LENGTH] );
//////////////////////////////////////////////////////////////////////////
// decode by string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535 =>      ::-0
// ipv4                         192.168.111.123-65535 => 0.0.0.0-0
MM_EXPORT_DLL void mm_sockaddr_storage_decode_zero_string( struct sockaddr_storage* addr, char addr_name[MM_ADDR_NAME_LENGTH] );
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535 =>      ::-0
// ipv4                         192.168.111.123-65535 => 0.0.0.0-0
MM_EXPORT_DLL void mm_sockaddr_storage_decode_zero( struct sockaddr_storage* addr, const char* node, mm_ushort_t port );
//////////////////////////////////////////////////////////////////////////
// calculate the socket real length for this function.
// connect bind sendto
// for linux windows use the sizeof(struct sockaddr_storage) os not checking and anything correct.
// for freebsd must use the real sockaddr length.if not well case errno(EAFNOSUPPORT) Address family not supported by protocol family.
MM_EXPORT_DLL socklen_t mm_sockaddr_storage_length( struct sockaddr_storage* addr);
//////////////////////////////////////////////////////////////////////////

#include "core/mm_suffix.h"

#endif//__mm_sockaddr_h__