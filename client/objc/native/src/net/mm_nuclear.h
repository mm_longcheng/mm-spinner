#ifndef __mm_nuclear_h__
#define __mm_nuclear_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_thread.h"
#include "core/mm_spinlock.h"

#include "poller/mm_poll.h"

#include "container/mm_holder32.h"

#include <pthread.h>

// nuclear default page size.
#define MM_NUCLEAR_PAGE_SIZE 1024
// nuclear default poll length.
#define MM_NUCLEAR_POLL_LENGTH 256
// nuclear default poll idle sleep milliseconds.
#define MM_NUCLEAR_IDLE_SLEEP_MSEC 200


typedef void (*nuclear_handle_recv)(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length);
typedef void (*nuclear_handle_send)(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length);
struct mm_nuclear_callback
{
	nuclear_handle_recv handle_recv;
	nuclear_handle_send handle_send;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_nuclear_callback_init(struct mm_nuclear_callback* p);
MM_EXPORT_DLL void mm_nuclear_callback_destroy(struct mm_nuclear_callback* p);

struct mm_nuclear
{
	struct mm_holder_u32_vpt holder;// rb tree for fd <--> addr.
	//
	struct mm_nuclear_callback callback;
	//
	struct mm_poll poll;// poller.
	//
	pthread_t poll_thread;
	pthread_mutex_t cond_locker;
	pthread_cond_t cond_not_null;
	pthread_cond_t cond_not_full;
	mm_atomic_t locker;
	// set poll len for mm_poll_wait.default is MM_NUCLEAR_POLL_LEN.set it before wait,or join_wait after.
	int poll_length;// poll length.
	// set poll timeout interval.-1 is forever.default is MM_NUCLEAR_IDLE_SLEEP_MSEC.
	// if forever,you must resolve break block state while poll wait,use pipeline,or other way.
	mm_msec_t poll_timeout;// poll timeout.
	size_t size;// addr size.
	size_t max_size;// max addr size.default is -1;
	//
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_nuclear_init(struct mm_nuclear* p);
MM_EXPORT_DLL void mm_nuclear_destroy(struct mm_nuclear* p);
//////////////////////////////////////////////////////////////////////////
// lock to make sure the knot is thread safe.
MM_EXPORT_DLL void mm_nuclear_lock(struct mm_nuclear* p);
// unlock knot.
MM_EXPORT_DLL void mm_nuclear_unlock(struct mm_nuclear* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_nuclear_assign_callback(struct mm_nuclear* p,struct mm_nuclear_callback* callback);
//////////////////////////////////////////////////////////////////////////
// poll size.
MM_EXPORT_DLL size_t mm_nuclear_poll_size(struct mm_nuclear* p);
//////////////////////////////////////////////////////////////////////////
// wait for activation fd.
MM_EXPORT_DLL void mm_nuclear_poll_wait(struct mm_nuclear* p);
//////////////////////////////////////////////////////////////////////////
// start wait thread.
MM_EXPORT_DLL void mm_nuclear_start(struct mm_nuclear* p);
// interrupt wait thread.
MM_EXPORT_DLL void mm_nuclear_interrupt(struct mm_nuclear* p);
// shutdown wait thread.
MM_EXPORT_DLL void mm_nuclear_shutdown(struct mm_nuclear* p);
// join wait thread.
MM_EXPORT_DLL void mm_nuclear_join(struct mm_nuclear* p);
//////////////////////////////////////////////////////////////////////////
// add u into poll_fd.you must add rmv in pairs.
MM_EXPORT_DLL void mm_nuclear_fd_add(struct mm_nuclear* p, mm_socket_t fd, void* u);
// rmv u from poll_fd.you must add rmv in pairs.
MM_EXPORT_DLL void mm_nuclear_fd_rmv(struct mm_nuclear* p, mm_socket_t fd, void* u);
// mod u event from poll wait.
MM_EXPORT_DLL void mm_nuclear_fd_mod(struct mm_nuclear* p, mm_socket_t fd, void* u, int flag);
// get u event from poll wait.
MM_EXPORT_DLL void* mm_nuclear_fd_get(struct mm_nuclear* p, mm_socket_t fd);
//////////////////////////////////////////////////////////////////////////
// add u into poll_fd.not cond and locker.you must lock it outside manual.
MM_EXPORT_DLL void mm_nuclear_fd_add_poll(struct mm_nuclear* p, mm_socket_t fd, void* u);
// rmv u from poll_fd.not cond and locker.you must lock it outside manual.
MM_EXPORT_DLL void mm_nuclear_fd_rmv_poll(struct mm_nuclear* p, mm_socket_t fd, void* u);
// signal.
MM_EXPORT_DLL void mm_nuclear_fd_poll_signal(struct mm_nuclear* p);
//////////////////////////////////////////////////////////////////////////
// this function is used for copy current fd holder.
MM_EXPORT_DLL void mm_nuclear_fd_copy_holder(struct mm_nuclear* p,struct mm_holder_u32_vpt* holder);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_nuclear_h__