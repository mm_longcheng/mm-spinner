#ifndef __mm_buffer_packet_h__
#define __mm_buffer_packet_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_string.h"
#include "net/mm_packet.h"
//////////////////////////////////////////////////////////////////////////
// packet handle tcp buffer and length for string handle only one packet.
MM_EXPORT_DLL void mm_buffer_packet_handle_tcp(mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length,packet_handle_tcp handle, void* obj);
MM_EXPORT_DLL void mm_buffer_packet_handle_udp(mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length,packet_handle_udp handle, void* obj, struct sockaddr_storage* remote);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_buffer_packet_h__