#ifndef __mm_mt_contact_h__
#define __mm_mt_contact_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_spinlock.h"

#include "net/mm_mt_tcp.h"

#include "container/mm_holder32.h"

//////////////////////////////////////////////////////////////////////////
typedef void (*mt_contact_event_mt_tcp_alloc)( void* p, struct mm_mt_tcp* mt_tcp );
typedef void (*mt_contact_event_mt_tcp_relax)( void* p, struct mm_mt_tcp* mt_tcp );
struct mm_mt_contact_event_mt_tcp_alloc
{
	mt_contact_event_mt_tcp_alloc event_mt_tcp_alloc;// event mt_tcp alloc and add holder.
	mt_contact_event_mt_tcp_relax event_mt_tcp_relax;// event mt_tcp relax and rmv holder.
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_mt_contact_event_mt_tcp_alloc_init(struct mm_mt_contact_event_mt_tcp_alloc* p);
MM_EXPORT_DLL void mm_mt_contact_event_mt_tcp_alloc_destroy(struct mm_mt_contact_event_mt_tcp_alloc* p);
//////////////////////////////////////////////////////////////////////////
struct mm_mt_contact
{
	struct mm_mt_contact_event_mt_tcp_alloc event_mt_tcp_alloc;
	struct mm_crypto_callback crypto_callback;// crypto callback.
	struct mm_holder_u32_vpt holder;
	pthread_mutex_t signal_mutex;
	pthread_cond_t signal_cond;
	mm_atomic_t holder_locker;
	mm_atomic_t instance_locker;// the locker for only mm_mt_contact_get_instance process thread safe.
	mm_atomic_t locker;
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
	void* u;// user data.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mt_contact_init(struct mm_mt_contact* p);
MM_EXPORT_DLL void mm_mt_contact_destroy(struct mm_mt_contact* p);
//////////////////////////////////////////////////////////////////////////
// lock to make sure the mt_contact is thread safe.
MM_EXPORT_DLL void mm_mt_contact_lock(struct mm_mt_contact* p);
// unlock mt_contact.
MM_EXPORT_DLL void mm_mt_contact_unlock(struct mm_mt_contact* p);
//////////////////////////////////////////////////////////////////////////
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_mt_contact_assign_event_mt_tcp_alloc(struct mm_mt_contact* p,struct mm_mt_contact_event_mt_tcp_alloc* event_mt_tcp_alloc);
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_mt_contact_assign_crypto_callback(struct mm_mt_contact* p,struct mm_crypto_callback* crypto_callback);
// assign context handle.
MM_EXPORT_DLL void mm_mt_contact_assign_context(struct mm_mt_contact* p,void* u);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mt_contact_check(struct mm_mt_contact* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_mt_contact_start(struct mm_mt_contact* p);
MM_EXPORT_DLL void mm_mt_contact_interrupt(struct mm_mt_contact* p);
MM_EXPORT_DLL void mm_mt_contact_shutdown(struct mm_mt_contact* p);
MM_EXPORT_DLL void mm_mt_contact_join(struct mm_mt_contact* p);
//////////////////////////////////////////////////////////////////////////
// this func is all thread safe,not need lock.
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL struct mm_mt_tcp* mm_mt_contact_add(struct mm_mt_contact* p,int unique_id,const char* node_n,const char* node_r,mm_ushort_t port);
MM_EXPORT_DLL void mm_mt_contact_rmv(struct mm_mt_contact* p,int sid);
MM_EXPORT_DLL struct mm_mt_tcp* mm_mt_contact_get(struct mm_mt_contact* p,int unique_id);
MM_EXPORT_DLL struct mm_mt_tcp* mm_mt_contact_get_instance(struct mm_mt_contact* p,int unique_id,const char* node_n,const char* node_r,mm_ushort_t port);
MM_EXPORT_DLL void mm_mt_contact_clear(struct mm_mt_contact* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_mt_contact_h__