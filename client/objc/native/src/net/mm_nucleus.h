#ifndef __mm_nucleus_h__
#define __mm_nucleus_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_thread.h"

#include "net/mm_nuclear.h"

struct mm_nucleus
{
	struct mm_nuclear_callback callback;
	pthread_mutex_t signal_mutex;
	pthread_cond_t signal_cond;
	mm_atomic_t arrays_locker;
	mm_atomic_t locker;
	mm_uint32_t length;// length. default is 0.
	struct mm_nuclear** arrays;// array pointer.
	// set poll len for mm_poll_wait.default is MM_NUCLEAR_POLL_LEN.set it before wait,or join_wait after.
	int poll_length;// poll length.
	// set poll timeout interval.-1 is forever.default is MM_NUCLEAR_IDLE_SLEEP_MSEC.
	// if forever,you must resolve break block state while poll wait,use pipeline,or other way.
	mm_msec_t poll_timeout;// poll timeout.
	//
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_nucleus_init(struct mm_nucleus* p);
MM_EXPORT_DLL void mm_nucleus_destroy(struct mm_nucleus* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_nucleus_lock(struct mm_nucleus* p);
MM_EXPORT_DLL void mm_nucleus_unlock(struct mm_nucleus* p);
//////////////////////////////////////////////////////////////////////////
// must join before set length.
MM_EXPORT_DLL void mm_nucleus_set_length(struct mm_nucleus* p, mm_uint32_t length);
MM_EXPORT_DLL mm_uint32_t mm_nucleus_get_length(struct mm_nucleus* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_nucleus_assign_callback(struct mm_nucleus* p,struct mm_nuclear_callback* cb);
//////////////////////////////////////////////////////////////////////////
// poll size.
MM_EXPORT_DLL size_t mm_nucleus_poll_size(struct mm_nucleus* p);
//////////////////////////////////////////////////////////////////////////
// wait for activation fd.
MM_EXPORT_DLL void mm_nucleus_poll_wait(struct mm_nucleus* p);
//////////////////////////////////////////////////////////////////////////
// start wait thread.
MM_EXPORT_DLL void mm_nucleus_start(struct mm_nucleus* p);
// interrupt wait thread.
MM_EXPORT_DLL void mm_nucleus_interrupt(struct mm_nucleus* p);
// shutdown wait thread.
MM_EXPORT_DLL void mm_nucleus_shutdown(struct mm_nucleus* p);
// join wait thread.
MM_EXPORT_DLL void mm_nucleus_join(struct mm_nucleus* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_nucleus_clear(struct mm_nucleus* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL mm_uint32_t mm_nucleus_hash_index(struct mm_nucleus* p, mm_socket_t fd );
//////////////////////////////////////////////////////////////////////////
// add u into poll_fd.
MM_EXPORT_DLL void mm_nucleus_fd_add(struct mm_nucleus* p, mm_socket_t fd, void* u);
// rmv u from poll_fd.
MM_EXPORT_DLL void mm_nucleus_fd_rmv(struct mm_nucleus* p, mm_socket_t fd, void* u);
// mod u event from poll wait.
MM_EXPORT_DLL void mm_nucleus_fd_mod(struct mm_nucleus* p, mm_socket_t fd, void* u, int flag);
// get u from poll.
MM_EXPORT_DLL void* mm_nucleus_fd_get(struct mm_nucleus* p, mm_socket_t fd);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_nucleus_h__