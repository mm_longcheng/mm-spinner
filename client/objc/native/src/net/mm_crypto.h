#ifndef __mm_crypto_h__
#define __mm_crypto_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

typedef void (*crypto_encrypt)(void* p,void* e,mm_uint8_t* buffer_i,mm_uint32_t offset_i,mm_uint8_t* buffer_o,mm_uint32_t offset_o,mm_uint32_t length);
typedef void (*crypto_decrypt)(void* p,void* e,mm_uint8_t* buffer_i,mm_uint32_t offset_i,mm_uint8_t* buffer_o,mm_uint32_t offset_o,mm_uint32_t length);
struct mm_crypto_callback
{
	crypto_encrypt encrypt;// event encrypt.
	crypto_decrypt decrypt;// event decrypt.
	void* obj;// weak ref. user data for callback.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_crypto_callback_init(struct mm_crypto_callback* p);
MM_EXPORT_DLL void mm_crypto_callback_destroy(struct mm_crypto_callback* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_mailbox_h__