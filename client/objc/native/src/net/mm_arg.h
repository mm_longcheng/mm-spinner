#ifndef __mm_arg_h__
#define __mm_arg_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_string.h"
// application cmd arg.
struct mm_arg
{
	int argc;
	char** argv;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_arg_init(struct mm_arg* p);
MM_EXPORT_DLL void mm_arg_destroy(struct mm_arg* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_arg_print(struct mm_arg* p);
MM_EXPORT_DLL void mm_arg_logger(struct mm_arg* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_arg_h__