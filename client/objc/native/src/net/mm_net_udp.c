#include "net/mm_net_udp.h"
#include "net/mm_streambuf_packet.h"
#include "core/mm_alloc.h"
#include "core/mm_logger.h"
/////////////////////////////////////////////////////////////
static void __static_net_udp_udp_handle_callback(void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote);
static void __static_net_udp_udp_broken_callback(void* obj);

static void __static_net_udp_nuclear_handle_recv(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length);
static void __static_net_udp_nuclear_handle_send(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length);

static void __static_net_udp_udp_handle_packet_callback(void* obj,struct mm_packet* pack, struct sockaddr_storage* remote);
/////////////////////////////////////////////////////////////
static void __static_net_udp_handle(void* obj, void* u, struct mm_packet* pack, struct sockaddr_storage* remote)
{

}
static void __static_net_udp_broken(void* obj)
{

}
static void __static_net_udp_nready(void* obj)
{

}
static void __static_net_udp_finish(void* obj)
{

}
MM_EXPORT_DLL void mm_net_udp_callback_init(struct mm_net_udp_callback* p)
{
	p->handle = &__static_net_udp_handle;
	p->broken = &__static_net_udp_broken;
	p->nready = &__static_net_udp_nready;
	p->finish = &__static_net_udp_finish;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_net_udp_callback_destroy(struct mm_net_udp_callback* p)
{
	p->handle = &__static_net_udp_handle;
	p->broken = &__static_net_udp_broken;
	p->nready = &__static_net_udp_nready;
	p->finish = &__static_net_udp_finish;
	p->obj = NULL;
}
/////////////////////////////////////////////////////////////
static void __static_net_udp_event_udp_alloc( void* p, struct mm_udp* udp )
{

}
static void __static_net_udp_event_udp_relax( void* p, struct mm_udp* udp )
{

}
MM_EXPORT_DLL void mm_net_udp_event_udp_alloc_init(struct mm_net_udp_event_udp_alloc* p)
{
	p->event_udp_alloc = &__static_net_udp_event_udp_alloc;
	p->event_udp_relax = &__static_net_udp_event_udp_relax;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_net_udp_event_udp_alloc_destroy(struct mm_net_udp_event_udp_alloc* p)
{
	p->event_udp_alloc = &__static_net_udp_event_udp_alloc;
	p->event_udp_relax = &__static_net_udp_event_udp_relax;
	p->obj = NULL;
}
/////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_udp_init(struct mm_net_udp* p)
{
	struct mm_holder_u32_vpt_alloc holder32_alloc;
	struct mm_udp_callback udp_callback;
	struct mm_nuclear_callback nuclear_callback;

	mm_udp_init(&p->udp);
	mm_nuclear_init(&p->nuclear);
	mm_holder_u32_vpt_init(&p->holder);
	mm_net_udp_callback_init(&p->callback);
	mm_net_udp_event_udp_alloc_init(&p->event_udp_alloc);
	mm_crypto_callback_init(&p->crypto_callback);
	mm_spin_init(&p->holder_locker, NULL);
	mm_spin_init(&p->locker, NULL);
	p->nonblock_timeout = MM_NET_UDP_NONBLOCK_TIMEOUT;// udp nonblock timeout.default is MM_NET_UDP_NONBLOCK_TIMEOUT milliseconds.
	p->udp_state = net_udp_state_closed;
	p->trytimes = MM_NET_UDP_TRYTIME;
	p->state = ts_closed;
	p->u = NULL;

	udp_callback.handle = &__static_net_udp_udp_handle_callback;
	udp_callback.broken = &__static_net_udp_udp_broken_callback;
	udp_callback.obj = p;
	mm_udp_assign_callback(&p->udp,&udp_callback);

	nuclear_callback.handle_recv = &__static_net_udp_nuclear_handle_recv;
	nuclear_callback.handle_send = &__static_net_udp_nuclear_handle_send;
	nuclear_callback.obj = p;
	mm_nuclear_assign_callback(&p->nuclear,&nuclear_callback);

	holder32_alloc.alloc = &mm_holder_u32_vpt_weak_alloc;
	holder32_alloc.relax = &mm_holder_u32_vpt_weak_relax;
	holder32_alloc.obj = p;
	mm_holder_u32_vpt_assign_alloc(&p->holder,&holder32_alloc);
}
MM_EXPORT_DLL void mm_net_udp_destroy(struct mm_net_udp* p)
{
	mm_net_udp_clear_callback_holder(p);
	mm_net_udp_detach_socket(p);
	//
	mm_udp_destroy(&p->udp);
	mm_nuclear_destroy(&p->nuclear);
	mm_holder_u32_vpt_destroy(&p->holder);
	mm_net_udp_callback_destroy(&p->callback);
	mm_net_udp_event_udp_alloc_destroy(&p->event_udp_alloc);
	mm_crypto_callback_destroy(&p->crypto_callback);
	mm_spin_destroy(&p->holder_locker);
	mm_spin_destroy(&p->locker);
	p->nonblock_timeout = 0;
	p->udp_state = net_udp_state_closed;
	p->trytimes = 0;
	p->state = ts_closed;
	p->u = NULL;
}
/////////////////////////////////////////////////////////////
// assign native address but not connect.
MM_EXPORT_DLL void mm_net_udp_assign_native(struct mm_net_udp* p,const char* node,mm_ushort_t port)
{
	mm_udp_assign_native(&p->udp, node, port);
}
// assign remote address but not connect.
MM_EXPORT_DLL void mm_net_udp_assign_remote(struct mm_net_udp* p,const char* node,mm_ushort_t port)
{
	mm_udp_assign_remote(&p->udp, node, port);
}
// assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
MM_EXPORT_DLL void mm_net_udp_assign_native_storage(struct mm_net_udp* p, struct sockaddr_storage* ss_native)
{
	mm_udp_assign_native_storage(&p->udp, ss_native);
}
// assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
MM_EXPORT_DLL void mm_net_udp_assign_remote_storage(struct mm_net_udp* p, struct sockaddr_storage* ss_remote)
{
	mm_udp_assign_remote_storage(&p->udp, ss_remote);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_udp_assign_callback(struct mm_net_udp* p,mm_uint32_t id,net_udp_handle callback)
{
	mm_spin_lock(&p->holder_locker);
	mm_holder_u32_vpt_set(&p->holder,id,(void*)callback);
	mm_spin_unlock(&p->holder_locker);
}
MM_EXPORT_DLL void mm_net_udp_assign_net_udp_callback(struct mm_net_udp* p,struct mm_net_udp_callback* net_udp_callback)
{
	assert(NULL != net_udp_callback && "you can not assign null net_udp_callback.");
	p->callback = *net_udp_callback;
}
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_net_udp_assign_event_udp_alloc(struct mm_net_udp* p,struct mm_net_udp_event_udp_alloc* event_udp_alloc)
{
	assert(NULL != event_udp_alloc && "you can not assign null event_udp_alloc.");
	p->event_udp_alloc = *event_udp_alloc;
}
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_net_udp_assign_crypto_callback(struct mm_net_udp* p,struct mm_crypto_callback* crypto_callback)
{
	assert(NULL != crypto_callback && "you can not assign null crypto_callback.");
	p->crypto_callback = *crypto_callback;
}
// assign context handle.
MM_EXPORT_DLL void mm_net_udp_assign_context(struct mm_net_udp* p,void* u)
{
	p->u = u;
}
// assign nonblock_timeout.
MM_EXPORT_DLL void mm_net_udp_assign_nonblock_timeout(struct mm_net_udp* p,mm_msec_t nonblock_timeout)
{
	p->nonblock_timeout = nonblock_timeout;
}
// assign trytimes.
MM_EXPORT_DLL void mm_net_udp_assign_trytimes(struct mm_net_udp* p,mm_uint32_t trytimes)
{
	p->trytimes = trytimes;
}
//////////////////////////////////////////////////////////////////////////
// context for udp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_DLL void mm_net_udp_set_addr_context(struct mm_net_udp* p, void* u)
{
	mm_udp_set_context(&p->udp,u);
}
MM_EXPORT_DLL void* mm_net_udp_get_addr_context(struct mm_net_udp* p)
{
	return mm_udp_get_context(&p->udp);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_udp_clear_callback_holder(struct mm_net_udp* p)
{
	mm_spin_lock(&p->holder_locker);
	mm_holder_u32_vpt_clear(&p->holder);
	mm_spin_unlock(&p->holder_locker);
}
//////////////////////////////////////////////////////////////////////////
// net_udp crypto encrypt buffer by udp.
MM_EXPORT_DLL void mm_net_udp_crypto_encrypt(struct mm_net_udp* p,struct mm_udp* udp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length)
{
	(*(p->crypto_callback.encrypt))(p,udp,buffer,offset,buffer,offset,length);
}
// net_udp crypto decrypt buffer by udp.
MM_EXPORT_DLL void mm_net_udp_crypto_decrypt(struct mm_net_udp* p,struct mm_udp* udp,mm_uint8_t* buffer,mm_uint32_t offset,mm_uint32_t length)
{
	(*(p->crypto_callback.decrypt))(p,udp,buffer,offset,buffer,offset,length);
}
//////////////////////////////////////////////////////////////////////////
// lock to make sure the net_udp is thread safe.
MM_EXPORT_DLL void mm_net_udp_lock(struct mm_net_udp* p)
{
	mm_spin_lock(&p->locker);
}
// unlock net_udp.
MM_EXPORT_DLL void mm_net_udp_unlock(struct mm_net_udp* p)
{
	mm_spin_unlock(&p->locker);
}
//////////////////////////////////////////////////////////////////////////
// fopen socket.
MM_EXPORT_DLL void mm_net_udp_fopen_socket(struct mm_net_udp* p)
{
	mm_udp_fopen_socket(&p->udp);
	//mm_nuclear_fd_add(&p->nuclear,p->udp.addr.socket,&p->udp);
}
// bind.
MM_EXPORT_DLL void mm_net_udp_bind(struct mm_net_udp* p)
{
	char link_name[MM_LINK_NAME_LENGTH];
	struct mm_addr* addr = &p->udp.addr;
	struct mm_logger* g_logger = mm_logger_instance();
	//
	mm_addr_string(addr,link_name);
	//
	if ( MM_INVALID_SOCKET != addr->socket )
	{
		mm_uint32_t _try_times = 0;
		int rt = -1;
		p->udp_state = net_udp_state_motion;
		// set socket to blocking.
		mm_addr_set_block(addr, MM_BLOCKING);
		do 
		{
			_try_times ++;
			rt = mm_addr_bind(addr);
			if (0 == rt)
			{
				// bind immediately.
				break;
			}
			if ( ts_finish == p->state )
			{
				break;
			}
			else
			{
				mm_logger_log_E(g_logger,"%s %d bind %s failure.try times:%d",__FUNCTION__,__LINE__,link_name,_try_times);
				mm_msleep(MM_NET_UDP_TRY_MSLEEP_TIME);
			}
		} while ( 0 != rt && _try_times < p->trytimes );
		// set socket to not block.
		mm_addr_set_block(addr, MM_NONBLOCK);
		if ( 0 != rt )
		{
			mm_err_t errcode = mm_errno;
			mm_logger_log_E(g_logger,"%s %d bind %s failure.try times:%d",__FUNCTION__,__LINE__,link_name,_try_times);
			mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
			mm_net_udp_apply_broken(p);
			mm_addr_close_socket(addr);
		}
		else
		{
			mm_net_udp_apply_finish(p);
			mm_logger_log_I(g_logger,"%s %d %s success.try times:%d",__FUNCTION__,__LINE__,link_name,_try_times);
		}
	}
	else
	{
		mm_logger_log_E(g_logger,"%s %d bind target fd is invalid.",__FUNCTION__,__LINE__);
	}
}

// close socket.
MM_EXPORT_DLL void mm_net_udp_close_socket(struct mm_net_udp* p)
{
	//mm_nuclear_fd_rmv(&p->nuclear,p->udp.addr.socket,&p->udp);
	mm_udp_close_socket(&p->udp);
}
// shutdown socket.
MM_EXPORT_DLL void mm_net_udp_shutdown_socket(struct mm_net_udp* p, int opcode)
{
	mm_addr_shutdown_socket(&p->udp.addr,opcode);
}
// set socket block.
MM_EXPORT_DLL void mm_net_udp_set_block(struct mm_net_udp* p, int block)
{
	mm_addr_set_block(&p->udp.addr, block);
}
// streambuf reset.
MM_EXPORT_DLL void mm_net_udp_streambuf_reset(struct mm_net_udp* p)
{
	mm_udp_streambuf_reset(&p->udp);
}
//////////////////////////////////////////////////////////////////////////
// check state.not thread safe.
MM_EXPORT_DLL void mm_net_udp_check(struct mm_net_udp* p)
{
	if ( 0 != mm_net_udp_finally_state(p) )
	{
		// we first disconnect this old connect.
		mm_net_udp_detach_socket(p);
		// fopen a socket.
		mm_net_udp_fopen_socket(p);
		// net tcp connect.
		mm_net_udp_bind(p);
	}
}
// get state finally.return 0 for all regular.not thread safe.
MM_EXPORT_DLL int mm_net_udp_finally_state(struct mm_net_udp* p)
{
	return ( MM_INVALID_SOCKET != p->udp.addr.socket && net_udp_state_finish == p->udp_state ) ? 0 : -1;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_udp_apply_broken(struct mm_net_udp* p)
{
	p->udp_state = net_udp_state_closed;
	mm_udp_streambuf_reset(&p->udp);
	(*p->callback.broken)(&p->udp);
	mm_nuclear_fd_rmv(&p->nuclear,p->udp.addr.socket,&p->udp);
}
MM_EXPORT_DLL void mm_net_udp_apply_finish(struct mm_net_udp* p)
{
	p->udp_state = net_udp_state_finish;
	mm_udp_streambuf_reset(&p->udp);
	mm_nuclear_fd_add(&p->nuclear,p->udp.addr.socket,&p->udp);
	(*p->callback.finish)(&p->udp);
}
// synchronize attach to socket.not thread safe.
MM_EXPORT_DLL void mm_net_udp_attach_socket(struct mm_net_udp* p)
{
	mm_net_udp_bind(p);
}
// synchronize detach to socket.not thread safe.
MM_EXPORT_DLL void mm_net_udp_detach_socket(struct mm_net_udp* p)
{
	struct mm_udp* udp = &p->udp;
	if ( MM_INVALID_SOCKET != udp->addr.socket)
	{
		// set flag value.
		p->udp_state = net_udp_state_closed;
		// set block state.
		mm_net_udp_set_block(p, MM_BLOCKING);
		// shutdown socket first.
		mm_net_udp_shutdown_socket(p,MM_BOTH_SHUTDOWN);
		// close socket.
		mm_net_udp_close_socket(p);
		// buffer is ivalid.
		mm_net_udp_streambuf_reset(p);
	}
}
//////////////////////////////////////////////////////////////////////////
// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_net_udp_buffer_send(struct mm_net_udp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote)
{
	int rt = -1;
	if ( net_udp_state_finish == p->udp_state && MM_INVALID_SOCKET != p->udp.addr.socket )
	{
		rt = mm_udp_buffer_send(&p->udp,buffer,offset,length,remote);
		// update the udp state,because the mm_udp_buffer_send can not fire broken event immediately.
		p->udp_state = -1 == rt ? net_udp_state_broken : p->udp_state;
	}
	else
	{
		// net udp nready.fire event.
		(*p->callback.nready)(&p->udp);
	}
	return rt;
}
// handle send data by flush send buffer.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_DLL int mm_net_udp_flush_send(struct mm_net_udp* p)
{
	int rt = -1;
	if ( net_udp_state_finish == p->udp_state && MM_INVALID_SOCKET != p->udp.addr.socket )
	{
		rt = mm_udp_flush_send(&p->udp);
		// update the udp state,because the mm_udp_buffer_send can not fire broken event immediately.
		p->udp_state = -1 == rt ? net_udp_state_broken : p->udp_state;
	}
	else
	{
		// net udp nready.fire event.
		(*p->callback.nready)(&p->udp);
	}
	return rt;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_udp_start(struct mm_net_udp* p)
{
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	mm_nuclear_start(&p->nuclear);
	(*(p->event_udp_alloc.event_udp_alloc))(p,&p->udp);
}
MM_EXPORT_DLL void mm_net_udp_interrupt(struct mm_net_udp* p)
{
	p->state = ts_closed;
	mm_nuclear_interrupt(&p->nuclear);
}
MM_EXPORT_DLL void mm_net_udp_shutdown(struct mm_net_udp* p)
{
	p->state = ts_finish;
	mm_nuclear_shutdown(&p->nuclear);
}
MM_EXPORT_DLL void mm_net_udp_join(struct mm_net_udp* p)
{
	mm_nuclear_join(&p->nuclear);
	(*(p->event_udp_alloc.event_udp_relax))(p,&p->udp);
}
//////////////////////////////////////////////////////////////////////////
static void __static_net_udp_udp_handle_callback(void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote)
{
	struct mm_udp* udp = (struct mm_udp*)(obj);
	struct mm_net_udp* net_udp = (struct mm_net_udp*)(udp->callback.obj);
	mm_net_udp_crypto_decrypt(net_udp,udp,buffer,offset,length);
	mm_streambuf_packet_handle_udp(&udp->buff_recv, buffer, offset, length, &__static_net_udp_udp_handle_packet_callback, udp, remote);
}
static void __static_net_udp_udp_broken_callback(void* obj)
{
	struct mm_udp* udp = (struct mm_udp*)(obj);
	struct mm_net_udp* net_udp = (struct mm_net_udp*)(udp->callback.obj);
	assert(NULL != net_udp->callback.broken && "net_udp->broken is a null.");
	mm_net_udp_apply_broken(net_udp);
}

static void __static_net_udp_nuclear_handle_recv(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length)
{
	struct mm_udp* udp = (struct mm_udp*)u;
	mm_udp_handle_recv(udp,buffer,offset,max_length);
}
static void __static_net_udp_nuclear_handle_send(void* obj, void* u, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length)
{
	struct mm_udp* udp = (struct mm_udp*)u;
	mm_udp_handle_send(udp,buffer,offset,max_length);
}

static void __static_net_udp_udp_handle_packet_callback(void* obj,struct mm_packet* pack, struct sockaddr_storage* remote)
{
	struct mm_udp* udp = (struct mm_udp*)(obj);
	struct mm_net_udp* net_udp = (struct mm_net_udp*)(udp->callback.obj);
	net_udp_handle handle = NULL;
	mm_spin_lock(&net_udp->holder_locker);
	handle = (net_udp_handle)mm_holder_u32_vpt_get(&net_udp->holder,pack->phead.mid);
	mm_spin_unlock(&net_udp->holder_locker);
	if (NULL != handle)
	{
		// fire the handle event.
		(*(handle))(udp,net_udp->u,pack,remote);
	}
	else
	{
		assert(NULL != net_udp->callback.handle && "net_udp->handle is a null.");
		(*(net_udp->callback.handle))(udp,net_udp->u,pack,remote);
	}
}
//////////////////////////////////////////////////////////////////////////
