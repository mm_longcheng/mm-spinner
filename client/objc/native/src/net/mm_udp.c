#include "net/mm_udp.h"
#include "core/mm_logger.h"
#include "core/mm_errno.h"
#include "core/mm_alloc.h"
/////////////////////////////////////////////////////////////
static void __static_udp_handle( void* obj, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote )
{

}
static void __static_udp_broken( void* obj )
{

}
/////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_udp_callback_init(struct mm_udp_callback* p)
{
	p->handle = &__static_udp_handle;
	p->broken = &__static_udp_broken;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_udp_callback_destroy(struct mm_udp_callback* p)
{
	p->handle = &__static_udp_handle;
	p->broken = &__static_udp_broken;
	p->obj = NULL;
}
/////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_udp_init(struct mm_udp* p)
{
	mm_addr_init(&p->addr);// transport address.
	mm_streambuf_init(&p->buff_recv);
	mm_streambuf_init(&p->buff_send);
	mm_udp_callback_init(&p->callback);
	p->unique_id = 0;
}
MM_EXPORT_DLL void mm_udp_destroy(struct mm_udp* p)
{
	mm_addr_shutdown_socket(&p->addr, MM_BOTH_SHUTDOWN);
	mm_addr_destroy(&p->addr);
	mm_streambuf_destroy(&p->buff_recv);
	mm_streambuf_destroy(&p->buff_send);
	mm_udp_callback_destroy(&p->callback);
	p->unique_id = 0;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_udp_lock(struct mm_udp* p)
{
	mm_addr_lock(&p->addr);
}
MM_EXPORT_DLL void mm_udp_unlock(struct mm_udp* p)
{
	mm_addr_unlock(&p->addr);
}
//////////////////////////////////////////////////////////////////////////
// assign addr native by ip port.
MM_EXPORT_DLL void mm_udp_assign_native(struct mm_udp* p, const char* node, mm_ushort_t port)
{
	mm_addr_assign_native(&p->addr, node, port);
}
// assign addr remote by ip port.
MM_EXPORT_DLL void mm_udp_assign_remote(struct mm_udp* p, const char* node, mm_ushort_t port)
{
	mm_addr_assign_remote(&p->addr, node, port);
}
// assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
MM_EXPORT_DLL void mm_udp_assign_native_storage(struct mm_udp* p, struct sockaddr_storage* ss_native)
{
	mm_addr_assign_native_storage(&p->addr, ss_native);
}
// assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
MM_EXPORT_DLL void mm_udp_assign_remote_storage(struct mm_udp* p, struct sockaddr_storage* ss_remote)
{
	mm_addr_assign_remote_storage(&p->addr, ss_remote);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_udp_assign_callback(struct mm_udp* p,struct mm_udp_callback* cb)
{
	assert(NULL != cb && "cb is a null.");
	p->callback = (*cb);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_udp_set_unique_id(struct mm_udp* p, mm_uint64_t unique_id)
{
	p->unique_id = unique_id;
}
MM_EXPORT_DLL mm_uint64_t mm_udp_get_unique_id(struct mm_udp* p)
{
	return p->unique_id;
}
// context for udp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_DLL void mm_udp_set_context(struct mm_udp* p, void* u)
{
	mm_addr_set_context(&p->addr,u);
}
MM_EXPORT_DLL void* mm_udp_get_context(struct mm_udp* p)
{
	return mm_addr_get_context(&p->addr);
}
//////////////////////////////////////////////////////////////////////////
// fopen socket.
MM_EXPORT_DLL void mm_udp_fopen_socket(struct mm_udp* p)
{
	char link_name[MM_LINK_NAME_LENGTH];
	struct mm_logger* g_logger = mm_logger_instance();
	//
	mm_addr_fopen_socket(&p->addr,p->addr.ss_native.ss_family,SOCK_DGRAM,0);
	//
	mm_addr_string(&p->addr,link_name);
	//
	if (MM_INVALID_SOCKET != p->addr.socket)
	{
		mm_logger_log_I(g_logger,"%s %d fopen_socket %s success.",__FUNCTION__,__LINE__,link_name);
	}
	else
	{
		//
		mm_logger_log_E(g_logger,"%s %d fopen_socket %s failure.",__FUNCTION__,__LINE__,link_name);
	}
}
// close socket.
MM_EXPORT_DLL void mm_udp_close_socket(struct mm_udp* p)
{
	mm_addr_close_socket(&p->addr);
}
// shutdown socket.
MM_EXPORT_DLL void mm_udp_shutdown_socket(struct mm_udp* p, int opcode)
{
	mm_addr_shutdown_socket(&p->addr,opcode);
}
//////////////////////////////////////////////////////////////////////////
// handle recv for buffer pool and pool max size.
MM_EXPORT_DLL void mm_udp_handle_recv(struct mm_udp* p, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length)
{
	socklen_t remote_size;
	struct sockaddr_storage remote;
	int real_len = 0;
	char link_name[MM_LINK_NAME_LENGTH];
	//
	remote_size = sizeof(struct sockaddr_storage);
	// we not need assert the fd is must valid.
	assert(p->callback.handle && p->callback.broken && "callback.handle or callback.broken is invalid.");
	//
	do
	{
		mm_memset(buffer + offset,0,max_length - offset);
		real_len = mm_addr_recv_dgram(&p->addr, buffer, offset, (mm_uint32_t)max_length, 0, (struct sockaddr*)(&remote), &remote_size);
		if ( -1 == real_len )
		{
			mm_err_t errcode = mm_errno;
			//-MM_EBADF
			//-MM_ENOTSOCK
			//-MM_EAGAIN
			//-MM_ENOBUFS
			//-MM_ENOMEM
			//-MM_EINVAL

			// errno:(10022) An invalid argument was supplied.
			// when we not sendto but recvfrom will case this error.
			// this error is not serious.
			if ( MM_EINVAL == errcode )
			{
				// sleep_msec for waiting for something sendto remote.
				break;
			}
			else if ( MM_ECONNRESET == errcode )
			{
				// An existing connection was forcibly closed by the remote host.
				// when the remote connection not open will case this error.
				break;
			}
			else if ( MM_EAGAIN == errcode )
			{
				// MM_EAGAIN EWOULDBLOCK
				// is timeout if setsocketopt SO_RCVTIMEO.
				break;
			}
			else
			{
				struct mm_logger* g_logger = mm_logger_instance();
				mm_addr_string(&p->addr, link_name);
				// at here when we want interrupt accept, will case a socket errno.
				// [errno:(53) Software caused connection abort] it is expected result.
				mm_logger_log_I(g_logger, "%s %d %s error occur.", __FUNCTION__, __LINE__, link_name);
				mm_logger_socket_error(g_logger, MM_LOG_INFO, errcode);
				// (*(p->callback.broken))(p);
				break;
			}
		}
		else if(0 == real_len)
		{
			mm_err_t errcode = mm_errno;
			// if this recv operate 0 == real_len,means the recv buffer is full or target socket is closed.
			// so we must use poll for checking the io event make sure read event is fire.
			// at here send length is 0 will fire the broken event.
			struct mm_logger* g_logger = mm_logger_instance();
			mm_addr_string(&p->addr, link_name);
			// the other side close the socket.
			mm_logger_log_I(g_logger, "%s %d %s error occur.", __FUNCTION__, __LINE__, link_name);
			mm_logger_socket_error(g_logger, MM_LOG_INFO, errcode);
			// (*(p->callback.broken))(p);
			break;
		}
		//if( real_len != max_length )
		//{
		//	mm_udp_buffer_recv(p, buffer, offset, real_len, &remote);
		//	break;
		//}
		//else
		//{
		//	mm_udp_buffer_recv(p, buffer, offset, real_len, &remote);
		//}
		// udp not like tcp socket just recv until the udp socket is empty.
		mm_udp_buffer_recv(p, buffer, offset, real_len, &remote);
	} while (1);
}
// handle send for buffer pool and pool max size.
MM_EXPORT_DLL void mm_udp_handle_send(struct mm_udp* p, mm_uint8_t* buffer, mm_uint32_t offset, size_t max_length)
{
	// do nothing.
}
//////////////////////////////////////////////////////////////////////////
// handle recv for buffer pool and pool max size.
MM_EXPORT_DLL int mm_udp_buffer_recv(struct mm_udp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote)
{
	assert(p->callback.handle&&"callback.handle is invalid.");
	(*(p->callback.handle))(p, buffer, offset, length, remote);
	return 0;
}
// handle send for buffer pool and pool max size.
MM_EXPORT_DLL int mm_udp_buffer_send(struct mm_udp* p, mm_uint8_t* buffer, mm_uint32_t offset, mm_uint32_t length, struct sockaddr_storage* remote)
{
	int real_len = 0;
	int send_len = 0;
	do 
	{
		if (0 == length)
		{
			// nothing for send break immediately.
			break;
		}
		real_len = mm_addr_send_dgram(&p->addr, buffer, offset, (int)length, 0, (struct sockaddr*)remote, mm_sockaddr_storage_length(remote));
		if ( -1 == real_len )
		{
			mm_err_t errcode = mm_errno;
			if ( MM_EAGAIN == errcode )
			{
				// MM_EAGAIN errno:(10035) A non-blocking socket operation could not be completed immediately.
				// MM_EAGAIN is well.
				// send_len will be 0.
				send_len = 0;
				break;
			}
			else
			{
				char link_name[MM_LINK_NAME_LENGTH];
				// if udp send error we logger it out.
				struct mm_logger* g_logger = mm_logger_instance();
				mm_addr_string(&p->addr, link_name);
				// error occur.
				mm_logger_log_I(g_logger, "%s %d %s error occur.", __FUNCTION__, __LINE__, link_name);
				mm_logger_socket_error(g_logger, MM_LOG_INFO, errcode);
				send_len = -1;
				break;
			}
		}
		else
		{
			send_len = real_len;
			break;
		}
	} while (1);
	return send_len;
}

// handle send data by flush send buffer.
MM_EXPORT_DLL int mm_udp_flush_send(struct mm_udp* p)
{
	size_t sz = mm_streambuf_size(&p->buff_send);
	// use self streambuf.
	mm_udp_buffer_send(p, p->buff_send.buff, (mm_uint32_t)p->buff_send.gptr, (mm_uint32_t)sz, &p->addr.ss_remote);
	// we not need check why udp send error.just gbump the buffer.
	mm_streambuf_gbump(&p->buff_send, sz);
	return (int)sz;
}
// udp streambuf recv send reset.
MM_EXPORT_DLL void mm_udp_streambuf_reset(struct mm_udp* p)
{
	mm_streambuf_reset(&p->buff_recv);
	mm_streambuf_reset(&p->buff_send);
}
