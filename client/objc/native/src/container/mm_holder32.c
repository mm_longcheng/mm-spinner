#include "mm_holder32.h"
#include "core/mm_alloc.h"
#include "core/mm_hash_func.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_u32_iterator_init(struct mm_holder_u32_u32_iterator* p)
{
	p->k = 0;
	p->v = 0;
	p->n.rb_right = NULL;
	p->n.rb_left = NULL;
}
MM_EXPORT_DLL void mm_holder_u32_u32_iterator_destroy(struct mm_holder_u32_u32_iterator* p)
{
	p->k = 0;
	p->v = 0;
	p->n.rb_right = NULL;
	p->n.rb_left = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_u32_init(struct mm_holder_u32_u32* p)
{
	p->rbt.rb_node = NULL;
	p->size = 0;
}
MM_EXPORT_DLL void mm_holder_u32_u32_destroy(struct mm_holder_u32_u32* p)
{
	mm_holder_u32_u32_clear(p);
	p->rbt.rb_node = NULL;
	p->size = 0;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL mm_uint32_t* mm_holder_u32_u32_add(struct mm_holder_u32_u32* p,mm_uint32_t k)
{
	struct mm_rb_node **newe = NULL;
	struct mm_rb_node *parent = NULL;
	struct mm_holder_u32_u32_iterator* it = NULL;
	mm_sint32_t result = 0;
	newe = &(p->rbt.rb_node);
	/* Figure out where to put new node */
	while (NULL != *newe)
	{
		it = mm_container_of(*newe, struct mm_holder_u32_u32_iterator, n);
		result = mm_holder_u32_u32_cmp_func(it->k,k);
		parent = *newe;
		if (result < 0)
			newe = &((*newe)->rb_left);
		else if (result > 0)
			newe = &((*newe)->rb_right);
		else
			return NULL;
	}
	it = (struct mm_holder_u32_u32_iterator*)mm_malloc(sizeof(struct mm_holder_u32_u32_iterator));
	mm_holder_u32_u32_iterator_init(it);
	it->k = k;
	it->v = 0;
	/* Add new node and rebalance tree. */
	mm_rb_link_node(&it->n, parent, newe);
	mm_rb_insert_color(&it->n, &p->rbt);
	p->size ++;
	return &it->v;
}
MM_EXPORT_DLL void mm_holder_u32_u32_rmv(struct mm_holder_u32_u32* p,mm_uint32_t k)
{
	struct mm_holder_u32_u32_iterator* it = NULL;
	mm_sint32_t result = 0;
	struct mm_rb_node* node = p->rbt.rb_node;
	while (NULL != node)
	{
		it = mm_container_of(node, struct mm_holder_u32_u32_iterator, n);
		result = mm_holder_u32_u32_cmp_func(it->k,k);
		if (result < 0)
			node = node->rb_left;
		else if (result > 0)
			node = node->rb_right;
		else
		{
			mm_holder_u32_u32_erase(p,it);
			// break.
			return;
		}
	}
}
MM_EXPORT_DLL void mm_holder_u32_u32_erase(struct mm_holder_u32_u32* p,struct mm_holder_u32_u32_iterator* it)
{
	mm_rb_erase(&it->n, &p->rbt);
	mm_holder_u32_u32_iterator_destroy(it);
	mm_free(it);
	p->size --;
}
MM_EXPORT_DLL mm_uint32_t* mm_holder_u32_u32_get_instance(struct mm_holder_u32_u32* p,mm_uint32_t k)
{
	mm_uint32_t* v = (mm_uint32_t*)mm_holder_u32_u32_get(p,k);
	if (NULL == v)
	{
		v = mm_holder_u32_u32_add(p,k);
	}
	return v;
}
MM_EXPORT_DLL mm_uint32_t* mm_holder_u32_u32_get(struct mm_holder_u32_u32* p,mm_uint32_t k)
{
	struct mm_holder_u32_u32_iterator* it = mm_holder_u32_u32_get_iterator(p,k);
	if (NULL != it) { return &it->v; }
	return NULL;
}
MM_EXPORT_DLL struct mm_holder_u32_u32_iterator* mm_holder_u32_u32_get_iterator(struct mm_holder_u32_u32* p,mm_uint32_t k)
{
	struct mm_holder_u32_u32_iterator* it = NULL;
	mm_sint32_t result = 0;
	struct mm_rb_node* node = p->rbt.rb_node;
	while (NULL != node)
	{
		it = mm_container_of(node, struct mm_holder_u32_u32_iterator, n);
		result = mm_holder_u32_u32_cmp_func(it->k,k);
		if (result < 0)
			node = node->rb_left;
		else if (result > 0)
			node = node->rb_right;
		else
			return it;
	}
	return NULL;
}
MM_EXPORT_DLL void mm_holder_u32_u32_clear(struct mm_holder_u32_u32* p)
{
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_u32_iterator* it = NULL;
	//
	n = mm_rb_first(&p->rbt);
	while(NULL != n)
	{
		it = (struct mm_holder_u32_u32_iterator*)mm_rb_entry(n, struct mm_holder_u32_u32_iterator, n);
		n = mm_rb_next(n);
		mm_holder_u32_u32_erase(p,it);
	}
}
MM_EXPORT_DLL size_t mm_holder_u32_u32_size(struct mm_holder_u32_u32* p)
{
	return p->size;
}
MM_EXPORT_DLL void mm_holder_u32_u32_set(struct mm_holder_u32_u32* p,mm_uint32_t k,mm_uint32_t v)
{
	struct mm_rb_node **newe = NULL;
	struct mm_rb_node *parent = NULL;
	struct mm_holder_u32_u32_iterator* it = NULL;
	mm_sint32_t result = 0;
	newe = &(p->rbt.rb_node);
	/* Figure out where to put new node */
	while (NULL != *newe)
	{
		it = mm_container_of(*newe, struct mm_holder_u32_u32_iterator, n);
		result = mm_holder_u32_u32_cmp_func(it->k,k);
		parent = *newe;
		if (result < 0)
			newe = &((*newe)->rb_left);
		else if (result > 0)
			newe = &((*newe)->rb_right);
		else
		{
			it->v = v;
			return;
		}
	}
	it = (struct mm_holder_u32_u32_iterator*)mm_malloc(sizeof(struct mm_holder_u32_u32_iterator));
	mm_holder_u32_u32_iterator_init(it);
	it->k = k;
	it->v = v;
	/* Add new node and rebalance tree. */
	mm_rb_link_node(&it->n, parent, newe);
	mm_rb_insert_color(&it->n, &p->rbt);
	it->v = v;
	p->size ++;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_u64_iterator_init(struct mm_holder_u32_u64_iterator* p)
{
	p->k = 0;
	p->v = 0;
	p->n.rb_right = NULL;
	p->n.rb_left = NULL;
}
MM_EXPORT_DLL void mm_holder_u32_u64_iterator_destroy(struct mm_holder_u32_u64_iterator* p)
{
	p->k = 0;
	p->v = 0;
	p->n.rb_right = NULL;
	p->n.rb_left = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_u64_init(struct mm_holder_u32_u64* p)
{
	p->rbt.rb_node = NULL;
	p->size = 0;
}
MM_EXPORT_DLL void mm_holder_u32_u64_destroy(struct mm_holder_u32_u64* p)
{
	mm_holder_u32_u64_clear(p);
	p->rbt.rb_node = NULL;
	p->size = 0;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL mm_uint64_t* mm_holder_u32_u64_add(struct mm_holder_u32_u64* p,mm_uint32_t k)
{
	struct mm_rb_node **newe = NULL;
	struct mm_rb_node *parent = NULL;
	struct mm_holder_u32_u64_iterator* it = NULL;
	mm_sint32_t result = 0;
	newe = &(p->rbt.rb_node);
	/* Figure out where to put new node */
	while (NULL != *newe)
	{
		it = mm_container_of(*newe, struct mm_holder_u32_u64_iterator, n);
		result = mm_holder_u32_u64_cmp_func(it->k,k);
		parent = *newe;
		if (result < 0)
			newe = &((*newe)->rb_left);
		else if (result > 0)
			newe = &((*newe)->rb_right);
		else
			return NULL;
	}
	it = (struct mm_holder_u32_u64_iterator*)mm_malloc(sizeof(struct mm_holder_u32_u64_iterator));
	mm_holder_u32_u64_iterator_init(it);
	it->k = k;
	it->v = 0;
	/* Add new node and rebalance tree. */
	mm_rb_link_node(&it->n, parent, newe);
	mm_rb_insert_color(&it->n, &p->rbt);
	p->size ++;
	return &it->v;
}
MM_EXPORT_DLL void mm_holder_u32_u64_rmv(struct mm_holder_u32_u64* p,mm_uint32_t k)
{
	struct mm_holder_u32_u64_iterator* it = NULL;
	mm_sint32_t result = 0;
	struct mm_rb_node* node = p->rbt.rb_node;
	while (NULL != node)
	{
		it = mm_container_of(node, struct mm_holder_u32_u64_iterator, n);
		result = mm_holder_u32_u64_cmp_func(it->k,k);
		if (result < 0)
			node = node->rb_left;
		else if (result > 0)
			node = node->rb_right;
		else
		{
			mm_holder_u32_u64_erase(p,it);
			// break.
			return;
		}
	}
}
MM_EXPORT_DLL void mm_holder_u32_u64_erase(struct mm_holder_u32_u64* p,struct mm_holder_u32_u64_iterator* it)
{
	mm_rb_erase(&it->n, &p->rbt);
	mm_holder_u32_u64_iterator_destroy(it);
	mm_free(it);
	p->size --;
}
MM_EXPORT_DLL mm_uint64_t* mm_holder_u32_u64_get_instance(struct mm_holder_u32_u64* p,mm_uint32_t k)
{
	mm_uint64_t* v = (mm_uint64_t*)mm_holder_u32_u64_get(p,k);
	if (NULL == v)
	{
		v = mm_holder_u32_u64_add(p,k);
	}
	return v;
}
MM_EXPORT_DLL mm_uint64_t* mm_holder_u32_u64_get(struct mm_holder_u32_u64* p,mm_uint32_t k)
{
	struct mm_holder_u32_u64_iterator* it = mm_holder_u32_u64_get_iterator(p,k);
	if (NULL != it) { return &it->v; }
	return NULL;
}
MM_EXPORT_DLL struct mm_holder_u32_u64_iterator* mm_holder_u32_u64_get_iterator(struct mm_holder_u32_u64* p,mm_uint32_t k)
{
	struct mm_holder_u32_u64_iterator* it = NULL;
	mm_sint32_t result = 0;
	struct mm_rb_node* node = p->rbt.rb_node;
	while (NULL != node)
	{
		it = mm_container_of(node, struct mm_holder_u32_u64_iterator, n);
		result = mm_holder_u32_u64_cmp_func(it->k,k);
		if (result < 0)
			node = node->rb_left;
		else if (result > 0)
			node = node->rb_right;
		else
			return it;
	}
	return NULL;
}
MM_EXPORT_DLL void mm_holder_u32_u64_clear(struct mm_holder_u32_u64* p)
{
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_u64_iterator* it = NULL;
	//
	n = mm_rb_first(&p->rbt);
	while(NULL != n)
	{
		it = (struct mm_holder_u32_u64_iterator*)mm_rb_entry(n, struct mm_holder_u32_u64_iterator, n);
		n = mm_rb_next(n);
		mm_holder_u32_u64_erase(p,it);
	}
}
MM_EXPORT_DLL size_t mm_holder_u32_u64_size(struct mm_holder_u32_u64* p)
{
	return p->size;
}
MM_EXPORT_DLL void mm_holder_u32_u64_set(struct mm_holder_u32_u64* p,mm_uint32_t k,mm_uint64_t v)
{
	struct mm_rb_node **newe = NULL;
	struct mm_rb_node *parent = NULL;
	struct mm_holder_u32_u64_iterator* it = NULL;
	mm_sint32_t result = 0;
	newe = &(p->rbt.rb_node);
	/* Figure out where to put new node */
	while (NULL != *newe)
	{
		it = mm_container_of(*newe, struct mm_holder_u32_u64_iterator, n);
		result = mm_holder_u32_u64_cmp_func(it->k,k);
		parent = *newe;
		if (result < 0)
			newe = &((*newe)->rb_left);
		else if (result > 0)
			newe = &((*newe)->rb_right);
		else
		{
			it->v = v;
			return;
		}
	}
	it = (struct mm_holder_u32_u64_iterator*)mm_malloc(sizeof(struct mm_holder_u32_u64_iterator));
	mm_holder_u32_u64_iterator_init(it);
	it->k = k;
	it->v = v;
	/* Add new node and rebalance tree. */
	mm_rb_link_node(&it->n, parent, newe);
	mm_rb_insert_color(&it->n, &p->rbt);
	it->v = v;
	p->size ++;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_vpt_iterator_init(struct mm_holder_u32_vpt_iterator* p)
{
	p->k = 0;
	p->v = NULL;
	p->n.rb_right = NULL;
	p->n.rb_left = NULL;
}
MM_EXPORT_DLL void mm_holder_u32_vpt_iterator_destroy(struct mm_holder_u32_vpt_iterator* p)
{
	p->k = 0;
	p->v = NULL;
	p->n.rb_right = NULL;
	p->n.rb_left = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_vpt_alloc_init(struct mm_holder_u32_vpt_alloc* p)
{
	p->alloc = &mm_holder_u32_vpt_weak_alloc;
	p->relax = &mm_holder_u32_vpt_weak_relax;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_holder_u32_vpt_alloc_destroy(struct mm_holder_u32_vpt_alloc* p)
{
	p->alloc = &mm_holder_u32_vpt_weak_alloc;
	p->relax = &mm_holder_u32_vpt_weak_relax;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_vpt_init(struct mm_holder_u32_vpt* p)
{
	p->rbt.rb_node = NULL;
	mm_holder_u32_vpt_alloc_init(&p->alloc);
	p->size = 0;
}
MM_EXPORT_DLL void mm_holder_u32_vpt_destroy(struct mm_holder_u32_vpt* p)
{
	mm_holder_u32_vpt_clear(p);
	p->rbt.rb_node = NULL;
	mm_holder_u32_vpt_alloc_destroy(&p->alloc);
	p->size = 0;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_vpt_assign_alloc(struct mm_holder_u32_vpt* p,struct mm_holder_u32_vpt_alloc* alloc)
{
	assert(alloc&&"you can not assign null alloc.");
	p->alloc = *alloc;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void* mm_holder_u32_vpt_add(struct mm_holder_u32_vpt* p,mm_uint32_t k)
{
	struct mm_rb_node **newe = NULL;
	struct mm_rb_node *parent = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	mm_sint32_t result = 0;
	newe = &(p->rbt.rb_node);
	/* Figure out where to put new node */
	while (NULL != *newe)
	{
		it = mm_container_of(*newe, struct mm_holder_u32_vpt_iterator, n);
		result = mm_holder_u32_vpt_cmp_func(it->k,k);
		parent = *newe;
		if (result < 0)
			newe = &((*newe)->rb_left);
		else if (result > 0)
			newe = &((*newe)->rb_right);
		else
			return NULL;
	}
	it = (struct mm_holder_u32_vpt_iterator*)mm_malloc(sizeof(struct mm_holder_u32_vpt_iterator));
	mm_holder_u32_vpt_iterator_init(it);
	it->k = k;
	it->v = p->alloc.alloc(p,k);
	/* Add new node and rebalance tree. */
	mm_rb_link_node(&it->n, parent, newe);
	mm_rb_insert_color(&it->n, &p->rbt);
	p->size ++;
	return it->v;
}
MM_EXPORT_DLL void mm_holder_u32_vpt_rmv(struct mm_holder_u32_vpt* p,mm_uint32_t k)
{
	struct mm_holder_u32_vpt_iterator* it = NULL;
	mm_sint32_t result = 0;
	struct mm_rb_node* node = p->rbt.rb_node;
	while (NULL != node)
	{
		it = mm_container_of(node, struct mm_holder_u32_vpt_iterator, n);
		result = mm_holder_u32_vpt_cmp_func(it->k,k);
		if (result < 0)
			node = node->rb_left;
		else if (result > 0)
			node = node->rb_right;
		else
		{
			mm_holder_u32_vpt_erase(p,it);
			// break.
			return;
		}
	}
}
MM_EXPORT_DLL void mm_holder_u32_vpt_erase(struct mm_holder_u32_vpt* p,struct mm_holder_u32_vpt_iterator* it)
{
	mm_rb_erase(&it->n, &p->rbt);
	p->alloc.relax(p,it->k,it->v);
	mm_holder_u32_vpt_iterator_destroy(it);
	mm_free(it);
	p->size --;
}
MM_EXPORT_DLL void* mm_holder_u32_vpt_get_instance(struct mm_holder_u32_vpt* p,mm_uint32_t k)
{
	void* v = mm_holder_u32_vpt_get(p,k);
	if (NULL == v)
	{
		v = mm_holder_u32_vpt_add(p,k);
	}
	return v;
}
MM_EXPORT_DLL void* mm_holder_u32_vpt_get(struct mm_holder_u32_vpt* p,mm_uint32_t k)
{
	struct mm_holder_u32_vpt_iterator* it = mm_holder_u32_vpt_get_iterator(p,k);
	if (NULL != it) { return it->v; }
	return NULL;
}
MM_EXPORT_DLL struct mm_holder_u32_vpt_iterator* mm_holder_u32_vpt_get_iterator(struct mm_holder_u32_vpt* p,mm_uint32_t k)
{
	struct mm_holder_u32_vpt_iterator* it = NULL;
	mm_sint32_t result = 0;
	struct mm_rb_node* node = p->rbt.rb_node;
	while (NULL != node)
	{
		it = mm_container_of(node, struct mm_holder_u32_vpt_iterator, n);
		result = mm_holder_u32_vpt_cmp_func(it->k,k);
		if (result < 0)
			node = node->rb_left;
		else if (result > 0)
			node = node->rb_right;
		else
			return it;
	}
	return NULL;
}
MM_EXPORT_DLL void mm_holder_u32_vpt_clear(struct mm_holder_u32_vpt* p)
{
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	//
	n = mm_rb_first(&p->rbt);
	while(NULL != n)
	{
		it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n, struct mm_holder_u32_vpt_iterator, n);
		n = mm_rb_next(n);
		mm_holder_u32_vpt_erase(p,it);
	}
}
MM_EXPORT_DLL size_t mm_holder_u32_vpt_size(struct mm_holder_u32_vpt* p)
{
	return p->size;
}
MM_EXPORT_DLL void mm_holder_u32_vpt_set(struct mm_holder_u32_vpt* p,mm_uint32_t k,void* v)
{
	struct mm_rb_node **newe = NULL;
	struct mm_rb_node *parent = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	mm_sint32_t result = 0;
	newe = &(p->rbt.rb_node);
	/* Figure out where to put new node */
	while (NULL != *newe)
	{
		it = mm_container_of(*newe, struct mm_holder_u32_vpt_iterator, n);
		result = mm_holder_u32_vpt_cmp_func(it->k,k);
		parent = *newe;
		if (result < 0)
			newe = &((*newe)->rb_left);
		else if (result > 0)
			newe = &((*newe)->rb_right);
		else
		{
			it->v = v;
			return;
		}
	}
	it = (struct mm_holder_u32_vpt_iterator*)mm_malloc(sizeof(struct mm_holder_u32_vpt_iterator));
	mm_holder_u32_vpt_iterator_init(it);
	it->k = k;
	it->v = p->alloc.alloc(p,k);
	/* Add new node and rebalance tree. */
	mm_rb_link_node(&it->n, parent, newe);
	mm_rb_insert_color(&it->n, &p->rbt);
	it->v = v;
	p->size ++;
}
MM_EXPORT_DLL void* mm_holder_u32_vpt_weak_alloc(struct mm_holder_u32_vpt* p,mm_uint32_t k)
{
	return NULL;
}
MM_EXPORT_DLL void* mm_holder_u32_vpt_weak_relax(struct mm_holder_u32_vpt* p,mm_uint32_t k,void* v)
{
	return v;
}
//////////////////////////////////////////////////////////////////////////
