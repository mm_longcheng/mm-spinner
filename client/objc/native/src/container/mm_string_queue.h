#ifndef __mm_string_queue_h__
#define __mm_string_queue_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_list.h"
#include "core/mm_string.h"

//////////////////////////////////////////////////////////////////////////
struct mm_list_sp
{
	struct mm_string v;
	struct mm_list_head n;
};
MM_EXPORT_DLL void mm_list_sp_init(struct mm_list_sp* p);
MM_EXPORT_DLL void mm_list_sp_destroy(struct mm_list_sp* p);
//
struct mm_string_queue
{
	struct mm_list_head l;// strong ref for mm_list_sp.
};
MM_EXPORT_DLL void mm_string_queue_init(struct mm_string_queue* p);
MM_EXPORT_DLL void mm_string_queue_destroy(struct mm_string_queue* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_string_queue_lpush(struct mm_string_queue* p,const char* buff,size_t size);
MM_EXPORT_DLL void mm_string_queue_lpop(struct mm_string_queue* p,struct mm_string* e);
MM_EXPORT_DLL void mm_string_queue_rpush(struct mm_string_queue* p,const char* buff,size_t size);
MM_EXPORT_DLL void mm_string_queue_rpop(struct mm_string_queue* p,struct mm_string* e);
MM_EXPORT_DLL void mm_string_queue_clear(struct mm_string_queue* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_string_queue_h__