#ifndef __mm_list_iterator_h__
#define __mm_list_iterator_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

#include "core/mm_list.h"

//////////////////////////////////////////////////////////////////////////
struct mm_list_u32_iterator
{
	mm_uint32_t v;
	struct mm_list_head n;
};
MM_EXPORT_DLL void mm_list_u32_iterator_init(struct mm_list_u32_iterator* p);
MM_EXPORT_DLL void mm_list_u32_iterator_destroy(struct mm_list_u32_iterator* p);
struct mm_list_u64_iterator
{
	mm_uint64_t v;
	struct mm_list_head n;
};
MM_EXPORT_DLL void mm_list_u64_iterator_init(struct mm_list_u64_iterator* p);
MM_EXPORT_DLL void mm_list_u64_iterator_destroy(struct mm_list_u64_iterator* p);
struct mm_list_f32_iterator
{
	mm_float32_t v;
	struct mm_list_head n;
};
MM_EXPORT_DLL void mm_list_f32_iterator_init(struct mm_list_f32_iterator* p);
MM_EXPORT_DLL void mm_list_f32_iterator_destroy(struct mm_list_f32_iterator* p);
struct mm_list_f64_iterator
{
	mm_float64_t v;
	struct mm_list_head n;
};
MM_EXPORT_DLL void mm_list_f64_iterator_init(struct mm_list_f64_iterator* p);
MM_EXPORT_DLL void mm_list_f64_iterator_destroy(struct mm_list_f64_iterator* p);
struct mm_list_vpt_iterator
{
	void* v;
	struct mm_list_head n;
};
MM_EXPORT_DLL void mm_list_vpt_iterator_init(struct mm_list_vpt_iterator* p);
MM_EXPORT_DLL void mm_list_vpt_iterator_destroy(struct mm_list_vpt_iterator* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_list_iterator_h__