#ifndef __mm_holder32_h__
#define __mm_holder32_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_rbtree.h"

//////////////////////////////////////////////////////////////////////////
// mm_holder_u32_u32
//////////////////////////////////////////////////////////////////////////
static mm_inline mm_sint32_t mm_holder_u32_u32_cmp_func(mm_uint32_t lhs,mm_uint32_t rhs)
{
	return lhs - rhs;
}

struct mm_holder_u32_u32_iterator
{
	mm_uint32_t k;
	mm_uint32_t v;
	struct mm_rb_node n;
};
MM_EXPORT_DLL void mm_holder_u32_u32_iterator_init(struct mm_holder_u32_u32_iterator* p);
MM_EXPORT_DLL void mm_holder_u32_u32_iterator_destroy(struct mm_holder_u32_u32_iterator* p);

struct mm_holder_u32_u32
{
	struct mm_rb_root rbt;// rb tree for k <--> v.
	size_t size;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_u32_init(struct mm_holder_u32_u32* p);
MM_EXPORT_DLL void mm_holder_u32_u32_destroy(struct mm_holder_u32_u32* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL mm_uint32_t* mm_holder_u32_u32_add(struct mm_holder_u32_u32* p,mm_uint32_t k);
MM_EXPORT_DLL void mm_holder_u32_u32_rmv(struct mm_holder_u32_u32* p,mm_uint32_t k);
MM_EXPORT_DLL void mm_holder_u32_u32_erase(struct mm_holder_u32_u32* p,struct mm_holder_u32_u32_iterator* it);
MM_EXPORT_DLL mm_uint32_t* mm_holder_u32_u32_get_instance(struct mm_holder_u32_u32* p,mm_uint32_t k);
MM_EXPORT_DLL mm_uint32_t* mm_holder_u32_u32_get(struct mm_holder_u32_u32* p,mm_uint32_t k);
MM_EXPORT_DLL struct mm_holder_u32_u32_iterator* mm_holder_u32_u32_get_iterator(struct mm_holder_u32_u32* p,mm_uint32_t k);
MM_EXPORT_DLL void mm_holder_u32_u32_clear(struct mm_holder_u32_u32* p);
MM_EXPORT_DLL size_t mm_holder_u32_u32_size(struct mm_holder_u32_u32* p);
//////////////////////////////////////////////////////////////////////////
// weak ref can use this.
MM_EXPORT_DLL void mm_holder_u32_u32_set(struct mm_holder_u32_u32* p,mm_uint32_t k,mm_uint32_t v);
//////////////////////////////////////////////////////////////////////////
// mm_holder_u32_u64
//////////////////////////////////////////////////////////////////////////
static mm_inline mm_sint32_t mm_holder_u32_u64_cmp_func(mm_uint32_t lhs,mm_uint32_t rhs)
{
	return lhs - rhs;
}

struct mm_holder_u32_u64_iterator
{
	mm_uint32_t k;
	mm_uint64_t v;
	struct mm_rb_node n;
};
MM_EXPORT_DLL void mm_holder_u32_u64_iterator_init(struct mm_holder_u32_u64_iterator* p);
MM_EXPORT_DLL void mm_holder_u32_u64_iterator_destroy(struct mm_holder_u32_u64_iterator* p);

struct mm_holder_u32_u64
{
	struct mm_rb_root rbt;// rb tree for k <--> v.
	size_t size;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_u64_init(struct mm_holder_u32_u64* p);
MM_EXPORT_DLL void mm_holder_u32_u64_destroy(struct mm_holder_u32_u64* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL mm_uint64_t* mm_holder_u32_u64_add(struct mm_holder_u32_u64* p,mm_uint32_t k);
MM_EXPORT_DLL void mm_holder_u32_u64_rmv(struct mm_holder_u32_u64* p,mm_uint32_t k);
MM_EXPORT_DLL void mm_holder_u32_u64_erase(struct mm_holder_u32_u64* p,struct mm_holder_u32_u64_iterator* it);
MM_EXPORT_DLL mm_uint64_t* mm_holder_u32_u64_get_instance(struct mm_holder_u32_u64* p,mm_uint32_t k);
MM_EXPORT_DLL mm_uint64_t* mm_holder_u32_u64_get(struct mm_holder_u32_u64* p,mm_uint32_t k);
MM_EXPORT_DLL struct mm_holder_u32_u64_iterator* mm_holder_u32_u64_get_iterator(struct mm_holder_u32_u64* p,mm_uint32_t k);
MM_EXPORT_DLL void mm_holder_u32_u64_clear(struct mm_holder_u32_u64* p);
MM_EXPORT_DLL size_t mm_holder_u32_u64_size(struct mm_holder_u32_u64* p);
//////////////////////////////////////////////////////////////////////////
// weak ref can use this.
MM_EXPORT_DLL void mm_holder_u32_u64_set(struct mm_holder_u32_u64* p,mm_uint32_t k,mm_uint64_t v);
//////////////////////////////////////////////////////////////////////////
// mm_holder_u32_vpt
//////////////////////////////////////////////////////////////////////////
static mm_inline mm_sint32_t mm_holder_u32_vpt_cmp_func(mm_uint32_t lhs,mm_uint32_t rhs)
{
	return lhs - rhs;
}

struct mm_holder_u32_vpt_iterator
{
	mm_uint32_t k;
	void* v;
	struct mm_rb_node n;
};
MM_EXPORT_DLL void mm_holder_u32_vpt_iterator_init(struct mm_holder_u32_vpt_iterator* p);
MM_EXPORT_DLL void mm_holder_u32_vpt_iterator_destroy(struct mm_holder_u32_vpt_iterator* p);

struct mm_holder_u32_vpt;

typedef void* (*holder_u32_vpt_alloc)(struct mm_holder_u32_vpt* p,mm_uint32_t k);
typedef void* (*holder_u32_vpt_relax)(struct mm_holder_u32_vpt* p,mm_uint32_t k,void* v);
struct mm_holder_u32_vpt_alloc
{
	holder_u32_vpt_alloc alloc;
	holder_u32_vpt_relax relax;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_holder_u32_vpt_alloc_init(struct mm_holder_u32_vpt_alloc* p);
MM_EXPORT_DLL void mm_holder_u32_vpt_alloc_destroy(struct mm_holder_u32_vpt_alloc* p);

struct mm_holder_u32_vpt
{
	struct mm_rb_root rbt;// rb tree for k <--> v.
	struct mm_holder_u32_vpt_alloc alloc;
	size_t size;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_vpt_init(struct mm_holder_u32_vpt* p);
MM_EXPORT_DLL void mm_holder_u32_vpt_destroy(struct mm_holder_u32_vpt* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_vpt_assign_alloc(struct mm_holder_u32_vpt* p,struct mm_holder_u32_vpt_alloc* alloc);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void* mm_holder_u32_vpt_add(struct mm_holder_u32_vpt* p,mm_uint32_t k);
MM_EXPORT_DLL void mm_holder_u32_vpt_rmv(struct mm_holder_u32_vpt* p,mm_uint32_t k);
MM_EXPORT_DLL void mm_holder_u32_vpt_erase(struct mm_holder_u32_vpt* p,struct mm_holder_u32_vpt_iterator* it);
MM_EXPORT_DLL void* mm_holder_u32_vpt_get_instance(struct mm_holder_u32_vpt* p,mm_uint32_t k);
MM_EXPORT_DLL void* mm_holder_u32_vpt_get(struct mm_holder_u32_vpt* p,mm_uint32_t k);
MM_EXPORT_DLL struct mm_holder_u32_vpt_iterator* mm_holder_u32_vpt_get_iterator(struct mm_holder_u32_vpt* p,mm_uint32_t k);
MM_EXPORT_DLL void mm_holder_u32_vpt_clear(struct mm_holder_u32_vpt* p);
MM_EXPORT_DLL size_t mm_holder_u32_vpt_size(struct mm_holder_u32_vpt* p);
//////////////////////////////////////////////////////////////////////////
// weak ref can use this.
MM_EXPORT_DLL void mm_holder_u32_vpt_set(struct mm_holder_u32_vpt* p,mm_uint32_t k,void* v);
MM_EXPORT_DLL void* mm_holder_u32_vpt_weak_alloc(struct mm_holder_u32_vpt* p,mm_uint32_t k);
MM_EXPORT_DLL void* mm_holder_u32_vpt_weak_relax(struct mm_holder_u32_vpt* p,mm_uint32_t k,void* v);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_holder32_h__