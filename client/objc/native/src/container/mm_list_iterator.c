#include "mm_list_iterator.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_list_u32_iterator_init(struct mm_list_u32_iterator* p)
{
	p->v = 0;
	MM_LIST_INIT_HEAD(&p->n);
}
MM_EXPORT_DLL void mm_list_u32_iterator_destroy(struct mm_list_u32_iterator* p)
{
	p->v = 0;
	MM_LIST_INIT_HEAD(&p->n);
}

MM_EXPORT_DLL void mm_list_u64_iterator_init(struct mm_list_u64_iterator* p)
{
	p->v = 0;
	MM_LIST_INIT_HEAD(&p->n);
}
MM_EXPORT_DLL void mm_list_u64_iterator_destroy(struct mm_list_u64_iterator* p)
{
	p->v = 0;
	MM_LIST_INIT_HEAD(&p->n);
}

MM_EXPORT_DLL void mm_list_f32_iterator_init(struct mm_list_f32_iterator* p)
{
	p->v = 0;
	MM_LIST_INIT_HEAD(&p->n);
}
MM_EXPORT_DLL void mm_list_f32_iterator_destroy(struct mm_list_f32_iterator* p)
{
	p->v = 0;
	MM_LIST_INIT_HEAD(&p->n);
}

MM_EXPORT_DLL void mm_list_f64_iterator_init(struct mm_list_f64_iterator* p)
{
	p->v = 0;
	MM_LIST_INIT_HEAD(&p->n);
}
MM_EXPORT_DLL void mm_list_f64_iterator_destroy(struct mm_list_f64_iterator* p)
{
	p->v = 0;
	MM_LIST_INIT_HEAD(&p->n);
}

MM_EXPORT_DLL void mm_list_vpt_iterator_init(struct mm_list_vpt_iterator* p)
{
	p->v = NULL;
	MM_LIST_INIT_HEAD(&p->n);
}
MM_EXPORT_DLL void mm_list_vpt_iterator_destroy(struct mm_list_vpt_iterator* p)
{
	p->v = NULL;
	MM_LIST_INIT_HEAD(&p->n);
}
