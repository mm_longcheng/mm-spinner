#ifndef __mm_lock_queue_h__
#define __mm_lock_queue_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_platform.h"
#include "core/mm_types.h"
#include "core/mm_spinlock.h"

#include "core/mm_list.h"
#include <pthread.h>

// weak ref lock queue.
struct mm_lock_queue
{
	struct mm_list_head l;
	pthread_mutex_t cond_locker;
	pthread_cond_t cond_not_null;
	pthread_cond_t cond_not_full;
	mm_atomic_t list_locker;
	mm_atomic_t locker;
	size_t size;// current size.
	size_t max_size;// max transport size.default is -1;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_lock_queue_init(struct mm_lock_queue* p);
MM_EXPORT_DLL void mm_lock_queue_destroy(struct mm_lock_queue* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_lock_queue_lock(struct mm_lock_queue* p);
MM_EXPORT_DLL void mm_lock_queue_unlock(struct mm_lock_queue* p);
//////////////////////////////////////////////////////////////////////////
// you can interrupt block state.but will make the size error.
// other way is push a NULL.
// Note: you can not lock it manual.
MM_EXPORT_DLL void mm_lock_queue_cond_not_null(struct mm_lock_queue* p);
MM_EXPORT_DLL void mm_lock_queue_cond_not_full(struct mm_lock_queue* p);
//////////////////////////////////////////////////////////////////////////
// this function is single thread.you might lock you self.
MM_EXPORT_DLL void mm_lock_queue_lpush(struct mm_lock_queue* p,void* e);
MM_EXPORT_DLL void* mm_lock_queue_lpop(struct mm_lock_queue* p);
MM_EXPORT_DLL void mm_lock_queue_rpush(struct mm_lock_queue* p,void* e);
MM_EXPORT_DLL void* mm_lock_queue_rpop(struct mm_lock_queue* p);
//////////////////////////////////////////////////////////////////////////
// block function.will lock.
MM_EXPORT_DLL void mm_lock_queue_blpush(struct mm_lock_queue* p,void* e);
MM_EXPORT_DLL void* mm_lock_queue_blpop(struct mm_lock_queue* p);
MM_EXPORT_DLL void mm_lock_queue_brpush(struct mm_lock_queue* p,void* e);
MM_EXPORT_DLL void* mm_lock_queue_brpop(struct mm_lock_queue* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_lock_queue_h__