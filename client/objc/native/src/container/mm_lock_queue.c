#include "mm_lock_queue.h"
#include "core/mm_alloc.h"
#include "container/mm_list_iterator.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_lock_queue_init(struct mm_lock_queue* p)
{
	MM_LIST_INIT_HEAD(&p->l);
	pthread_mutex_init(&p->cond_locker, NULL);
	pthread_cond_init(&p->cond_not_null, NULL);
	pthread_cond_init(&p->cond_not_full, NULL);
	mm_spin_init(&p->list_locker,NULL);
	mm_spin_init(&p->locker,NULL);
	p->size = 0;
	p->max_size = -1;// -1 is default limit size_t value.
}
MM_EXPORT_DLL void mm_lock_queue_destroy(struct mm_lock_queue* p)
{
	MM_LIST_INIT_HEAD(&p->l);
	pthread_mutex_destroy(&p->cond_locker);
	pthread_cond_destroy(&p->cond_not_null);
	pthread_cond_destroy(&p->cond_not_full);
	mm_spin_destroy(&p->list_locker);
	mm_spin_destroy(&p->locker);
	p->size = 0;
	p->max_size = -1;// -1 is default limit size_t value.
}
MM_EXPORT_DLL void mm_lock_queue_lock(struct mm_lock_queue* p)
{
	mm_spin_lock(&p->locker);
}
MM_EXPORT_DLL void mm_lock_queue_unlock(struct mm_lock_queue* p)
{
	mm_spin_unlock(&p->locker);
}
MM_EXPORT_DLL void mm_lock_queue_cond_not_null(struct mm_lock_queue* p)
{
	pthread_mutex_lock(&p->cond_locker);
	pthread_cond_signal(&p->cond_not_null);
	pthread_mutex_unlock(&p->cond_locker);
}
MM_EXPORT_DLL void mm_lock_queue_cond_not_full(struct mm_lock_queue* p)
{
	pthread_mutex_lock(&p->cond_locker);
	pthread_cond_signal(&p->cond_not_full);
	pthread_mutex_unlock(&p->cond_locker);
}
MM_EXPORT_DLL void mm_lock_queue_lpush(struct mm_lock_queue* p,void* e)
{
	struct mm_list_vpt_iterator* it = (struct mm_list_vpt_iterator*)mm_malloc(sizeof(struct mm_list_vpt_iterator));
	mm_list_vpt_iterator_init(it);
	it->v = e;
	mm_spin_lock(&p->list_locker);
	mm_list_add(&it->n, &p->l);
	p->size ++;
	mm_spin_unlock(&p->list_locker);
}
MM_EXPORT_DLL void* mm_lock_queue_lpop(struct mm_lock_queue* p)
{
	void* v = NULL;
	struct mm_list_head* pos = p->l.next;
	struct mm_list_vpt_iterator* it =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
	mm_spin_lock(&p->list_locker);
	mm_list_del(pos);
	p->size --;
	mm_spin_unlock(&p->list_locker);
	v = it->v;
	mm_list_vpt_iterator_destroy(it);
	mm_free(it);
	//
	return v;
}
MM_EXPORT_DLL void mm_lock_queue_rpush(struct mm_lock_queue* p,void* e)
{
	struct mm_list_vpt_iterator* it = (struct mm_list_vpt_iterator*)mm_malloc(sizeof(struct mm_list_vpt_iterator));
	mm_list_vpt_iterator_init(it);
	it->v = e;
	mm_spin_lock(&p->list_locker);
	mm_list_add_tail(&it->n, &p->l);
	p->size ++;
	mm_spin_unlock(&p->list_locker);
	//
}
MM_EXPORT_DLL void* mm_lock_queue_rpop(struct mm_lock_queue* p)
{
	void* v = NULL;
	struct mm_list_head* pos = p->l.prev;
	struct mm_list_vpt_iterator* it =(struct mm_list_vpt_iterator*)mm_list_entry(pos, struct mm_list_vpt_iterator, n);
	mm_spin_lock(&p->list_locker);
	mm_list_del(pos);
	p->size --;
	mm_spin_unlock(&p->list_locker);
	v = it->v;
	mm_list_vpt_iterator_destroy(it);
	mm_free(it);
	//
	return v;
}
MM_EXPORT_DLL void mm_lock_queue_blpush(struct mm_lock_queue* p,void* e)
{
	pthread_mutex_lock(&p->cond_locker);
	if ( p->max_size <= p->size )
	{
		pthread_cond_wait(&p->cond_not_full,&p->cond_locker);
	}
	//
	mm_lock_queue_lpush(p,e);
	// conn map from 0 to 1,change to not empty,we signal to msg loop.
	if ( 1 == p->size)
	{
		pthread_cond_signal(&p->cond_not_null);
	}
	pthread_mutex_unlock(&p->cond_locker);
}
MM_EXPORT_DLL void* mm_lock_queue_blpop(struct mm_lock_queue* p)
{
	void* v = NULL;
	pthread_mutex_lock(&p->cond_locker);
	if ( 0 >= p->size )
	{
		pthread_cond_wait(&p->cond_not_null,&p->cond_locker);
	}
	//
	v = mm_lock_queue_lpop(p);
	//
	if ( p->max_size - 1 == p->size )
	{
		pthread_cond_signal(&p->cond_not_full);
	}
	pthread_mutex_unlock(&p->cond_locker);
	return v;
}
MM_EXPORT_DLL void mm_lock_queue_brpush(struct mm_lock_queue* p,void* e)
{
	pthread_mutex_lock(&p->cond_locker);
	if ( p->max_size <= p->size )
	{
		pthread_cond_wait(&p->cond_not_full,&p->cond_locker);
	}
	//
	mm_lock_queue_rpush(p,e);
	// conn map from 0 to 1,change to not empty,we signal to msg loop.
	if ( 1 == p->size)
	{
		pthread_cond_signal(&p->cond_not_null);
	}
	pthread_mutex_unlock(&p->cond_locker);
}
MM_EXPORT_DLL void* mm_lock_queue_brpop(struct mm_lock_queue* p)
{
	void* v = NULL;
	pthread_mutex_lock(&p->cond_locker);
	if ( 0 >= p->size )
	{
		pthread_cond_wait(&p->cond_not_null,&p->cond_locker);
	}
	//
	v = mm_lock_queue_rpop(p);
	//
	if ( p->max_size - 1 == p->size )
	{
		pthread_cond_signal(&p->cond_not_full);
	}
	pthread_mutex_unlock(&p->cond_locker);
	return v;
}
