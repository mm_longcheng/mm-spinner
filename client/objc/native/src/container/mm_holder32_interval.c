#include "mm_holder32_interval.h"
#include "core/mm_alloc.h"
#include "core/mm_hash_func.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_interval_iterator_init(struct mm_holder_u32_interval_iterator* p)
{
	p->l = 0;
	p->r = 0;
	p->v = NULL;
	p->n.rb_right = NULL;
	p->n.rb_left = NULL;
}
MM_EXPORT_DLL void mm_holder_u32_interval_iterator_destroy(struct mm_holder_u32_interval_iterator* p)
{
	p->l = 0;
	p->r = 0;
	p->v = NULL;
	p->n.rb_right = NULL;
	p->n.rb_left = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_interval_alloc_init(struct mm_holder_u32_interval_alloc* p)
{
	p->alloc = &mm_holder_u32_interval_weak_alloc;
	p->relax = &mm_holder_u32_interval_weak_relax;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_holder_u32_interval_alloc_destroy(struct mm_holder_u32_interval_alloc* p)
{
	p->alloc = &mm_holder_u32_interval_weak_alloc;
	p->relax = &mm_holder_u32_interval_weak_relax;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_interval_init(struct mm_holder_u32_interval* p)
{
	p->rbt.rb_node = NULL;
	mm_holder_u32_interval_alloc_init(&p->alloc);
	p->size = 0;
}
MM_EXPORT_DLL void mm_holder_u32_interval_destroy(struct mm_holder_u32_interval* p)
{
	mm_holder_u32_interval_clear(p);
	p->rbt.rb_node = NULL;
	mm_holder_u32_interval_alloc_destroy(&p->alloc);
	p->size = 0;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_interval_assign_alloc(struct mm_holder_u32_interval* p,struct mm_holder_u32_interval_alloc* alloc)
{
	assert(alloc&&"you can not assign null alloc.");
	p->alloc = *alloc;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void* mm_holder_u32_interval_add(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r)
{
	struct mm_rb_node **newe = NULL;
	struct mm_rb_node *parent = NULL;
	struct mm_holder_u32_interval_iterator* it = NULL;
	mm_sint32_t result = 0;
	newe = &(p->rbt.rb_node);
	/* Figure out where to put new node */
	while (NULL != *newe)
	{
		it = mm_container_of(*newe, struct mm_holder_u32_interval_iterator, n);
		result = mm_holder_u32_interval_cmp_func(it->l,it->r,l,r);
		parent = *newe;
		if (result < 0)
			newe = &((*newe)->rb_left);
		else if (result > 0)
			newe = &((*newe)->rb_right);
		else
			return NULL;
	}
	it = (struct mm_holder_u32_interval_iterator*)mm_malloc(sizeof(struct mm_holder_u32_interval_iterator));
	mm_holder_u32_interval_iterator_init(it);
	it->l = l;
	it->r = r;
	it->v = p->alloc.alloc(p,l,r);
	/* Add new node and rebalance tree. */
	mm_rb_link_node(&it->n, parent, newe);
	mm_rb_insert_color(&it->n, &p->rbt);
	p->size ++;
	return it->v;
}
MM_EXPORT_DLL void mm_holder_u32_interval_rmv(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r)
{
	struct mm_holder_u32_interval_iterator* it = NULL;
	mm_sint32_t result = 0;
	struct mm_rb_node* node = p->rbt.rb_node;
	while (NULL != node)
	{
		it = mm_container_of(node, struct mm_holder_u32_interval_iterator, n);
		result = mm_holder_u32_interval_cmp_func(it->l,it->r,l,r);
		if (result < 0)
			node = node->rb_left;
		else if (result > 0)
			node = node->rb_right;
		else
		{
			mm_holder_u32_interval_erase(p,it);
			// break.
			return;
		}
	}
}
MM_EXPORT_DLL void mm_holder_u32_interval_erase(struct mm_holder_u32_interval* p,struct mm_holder_u32_interval_iterator* it)
{
	mm_rb_erase(&it->n, &p->rbt);
	p->alloc.relax(p,it->l,it->r,it->v);
	mm_holder_u32_interval_iterator_destroy(it);
	mm_free(it);
	p->size --;
}
MM_EXPORT_DLL void* mm_holder_u32_interval_get_instance(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r)
{
	void* v = mm_holder_u32_interval_get(p,l,r);
	if (NULL == v)
	{
		v = mm_holder_u32_interval_add(p,l,r);
	}
	return v;
}
MM_EXPORT_DLL void* mm_holder_u32_interval_get(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r)
{
	struct mm_holder_u32_interval_iterator* it = mm_holder_u32_interval_get_iterator(p,l,r);
	if (NULL != it) { return it->v; }
	return NULL;
}
MM_EXPORT_DLL struct mm_holder_u32_interval_iterator* mm_holder_u32_interval_get_iterator(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r)
{
	struct mm_holder_u32_interval_iterator* it = NULL;
	mm_sint32_t result = 0;
	struct mm_rb_node* node = p->rbt.rb_node;
	while (NULL != node)
	{
		it = mm_container_of(node, struct mm_holder_u32_interval_iterator, n);
		result = mm_holder_u32_interval_cmp_func(it->l,it->r,l,r);
		if (result < 0)
			node = node->rb_left;
		else if (result > 0)
			node = node->rb_right;
		else
			return it;
	}
	return NULL;
}
MM_EXPORT_DLL void mm_holder_u32_interval_clear(struct mm_holder_u32_interval* p)
{
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_interval_iterator* it = NULL;
	//
	n = mm_rb_first(&p->rbt);
	while(NULL != n)
	{
		it = (struct mm_holder_u32_interval_iterator*)mm_rb_entry(n, struct mm_holder_u32_interval_iterator, n);
		n = mm_rb_next(n);
		mm_holder_u32_interval_erase(p,it);
	}
}
MM_EXPORT_DLL size_t mm_holder_u32_interval_size(struct mm_holder_u32_interval* p)
{
	return p->size;
}
MM_EXPORT_DLL void mm_holder_u32_interval_set(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r,void* v)
{
	struct mm_rb_node **newe = NULL;
	struct mm_rb_node *parent = NULL;
	struct mm_holder_u32_interval_iterator* it = NULL;
	mm_sint32_t result = 0;
	newe = &(p->rbt.rb_node);
	/* Figure out where to put new node */
	while (NULL != *newe)
	{
		it = mm_container_of(*newe, struct mm_holder_u32_interval_iterator, n);
		result = mm_holder_u32_interval_cmp_func(it->l,it->r,l,r);
		parent = *newe;
		if (result < 0)
			newe = &((*newe)->rb_left);
		else if (result > 0)
			newe = &((*newe)->rb_right);
		else
		{
			it->v = v;
			return;
		}
	}
	it = (struct mm_holder_u32_interval_iterator*)mm_malloc(sizeof(struct mm_holder_u32_interval_iterator));
	mm_holder_u32_interval_iterator_init(it);
	it->l = l;
	it->r = r;
	it->v = p->alloc.alloc(p,l,r);
	/* Add new node and rebalance tree. */
	mm_rb_link_node(&it->n, parent, newe);
	mm_rb_insert_color(&it->n, &p->rbt);
	it->v = v;
	p->size ++;
}
MM_EXPORT_DLL void* mm_holder_u32_interval_weak_alloc(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r)
{
	return NULL;
}
MM_EXPORT_DLL void* mm_holder_u32_interval_weak_relax(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r,void* v)
{
	return v;
}
//////////////////////////////////////////////////////////////////////////
