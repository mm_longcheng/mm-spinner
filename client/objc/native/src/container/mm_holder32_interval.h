#ifndef __mm_holder32_interval_h__
#define __mm_holder32_interval_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_rbtree.h"

//////////////////////////////////////////////////////////////////////////
// mm_holder_u32_interval
//////////////////////////////////////////////////////////////////////////
static mm_inline mm_sint32_t mm_holder_u32_interval_cmp_func(mm_uint32_t ll,mm_uint32_t lr,mm_uint32_t rl,mm_uint32_t rr)
{
	if (ll <= rl && rr <= lr)
	{
		return 0;
	}
	else if (ll > rr)
	{
		return ll - rr;
	}
	else
	{
		return lr - rl;
	}
}

struct mm_holder_u32_interval_iterator
{
	mm_uint32_t l;
	mm_uint32_t r;
	void* v;
	struct mm_rb_node n;
};
MM_EXPORT_DLL void mm_holder_u32_interval_iterator_init(struct mm_holder_u32_interval_iterator* p);
MM_EXPORT_DLL void mm_holder_u32_interval_iterator_destroy(struct mm_holder_u32_interval_iterator* p);

struct mm_holder_u32_interval;

typedef void* (*holder_u32_interval_alloc)(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r);
typedef void* (*holder_u32_interval_relax)(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r,void* v);
struct mm_holder_u32_interval_alloc
{
	holder_u32_interval_alloc alloc;
	holder_u32_interval_relax relax;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_holder_u32_interval_alloc_init(struct mm_holder_u32_interval_alloc* p);
MM_EXPORT_DLL void mm_holder_u32_interval_alloc_destroy(struct mm_holder_u32_interval_alloc* p);

struct mm_holder_u32_interval
{
	struct mm_rb_root rbt;// rb tree for k <--> v.
	struct mm_holder_u32_interval_alloc alloc;
	size_t size;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_interval_init(struct mm_holder_u32_interval* p);
MM_EXPORT_DLL void mm_holder_u32_interval_destroy(struct mm_holder_u32_interval* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_holder_u32_interval_assign_alloc(struct mm_holder_u32_interval* p,struct mm_holder_u32_interval_alloc* alloc);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void* mm_holder_u32_interval_add(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r);
MM_EXPORT_DLL void mm_holder_u32_interval_rmv(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r);
MM_EXPORT_DLL void mm_holder_u32_interval_erase(struct mm_holder_u32_interval* p,struct mm_holder_u32_interval_iterator* it);
MM_EXPORT_DLL void* mm_holder_u32_interval_get_instance(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r);
MM_EXPORT_DLL void* mm_holder_u32_interval_get(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r);
MM_EXPORT_DLL struct mm_holder_u32_interval_iterator* mm_holder_u32_interval_get_iterator(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r);
MM_EXPORT_DLL void mm_holder_u32_interval_clear(struct mm_holder_u32_interval* p);
MM_EXPORT_DLL size_t mm_holder_u32_interval_size(struct mm_holder_u32_interval* p);
//////////////////////////////////////////////////////////////////////////
// weak ref can use this.
MM_EXPORT_DLL void mm_holder_u32_interval_set(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r,void* v);
MM_EXPORT_DLL void* mm_holder_u32_interval_weak_alloc(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r);
MM_EXPORT_DLL void* mm_holder_u32_interval_weak_relax(struct mm_holder_u32_interval* p,mm_uint32_t l,mm_uint32_t r,void* v);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_holder32_interval_h__