#include "mm_string_queue.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_list_sp_init(struct mm_list_sp* p)
{
    mm_string_init(&p->v);
    MM_LIST_INIT_HEAD(&p->n);
}
MM_EXPORT_DLL void mm_list_sp_destroy(struct mm_list_sp* p)
{
    mm_string_destroy(&p->v);
    MM_LIST_INIT_HEAD(&p->n);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_string_queue_init(struct mm_string_queue* p)
{
    MM_LIST_INIT_HEAD(&p->l);
}
MM_EXPORT_DLL void mm_string_queue_destroy(struct mm_string_queue* p)
{
    mm_string_queue_clear(p);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_string_queue_lpush(struct mm_string_queue* p,const char* buff,size_t size)
{
    struct mm_list_sp* it = (struct mm_list_sp*)mm_malloc(sizeof(struct mm_list_sp));
    mm_list_sp_init(it);
    mm_string_assignsn(&it->v,buff,size);
    mm_list_add(&it->n, &p->l);
}
MM_EXPORT_DLL void mm_string_queue_lpop(struct mm_string_queue* p,struct mm_string* e)
{
    struct mm_list_head* pos = p->l.next;
    struct mm_list_sp* it =(struct mm_list_sp*)mm_list_entry(pos, struct mm_list_sp, n);
    mm_list_del(pos);
    mm_string_assignsn(e,it->v.s,it->v.l);
    mm_list_sp_destroy(it);
    mm_free(it);
}
MM_EXPORT_DLL void mm_string_queue_rpush(struct mm_string_queue* p,const char* buff,size_t size)
{
    struct mm_list_sp* it = (struct mm_list_sp*)mm_malloc(sizeof(struct mm_list_sp));
    mm_list_sp_init(it);
    mm_string_assignsn(&it->v,buff,size);
    mm_list_add_tail(&it->n, &p->l);
}
MM_EXPORT_DLL void mm_string_queue_rpop(struct mm_string_queue* p,struct mm_string* e)
{
    struct mm_list_head* pos = p->l.prev;
    struct mm_list_sp* it =(struct mm_list_sp*)mm_list_entry(pos, struct mm_list_sp, n);
    mm_list_del(pos);
    mm_string_assignsn(e,it->v.s,it->v.l);
    mm_list_sp_destroy(it);
    mm_free(it);
}
MM_EXPORT_DLL void mm_string_queue_clear(struct mm_string_queue* p)
{
    struct mm_list_head* pos = NULL;
	struct mm_list_sp* it = NULL;
    pos = p->l.next;
    while(pos != &p->l)
    {
        struct mm_list_head* curr = pos;
        pos = pos->next;
        it = (struct mm_list_sp*)mm_list_entry(curr,struct mm_list_sp,n);
        mm_list_del(curr);
        mm_list_sp_destroy(it);
        mm_free(it);
    }
    MM_LIST_INIT_HEAD(&p->l);
}
//////////////////////////////////////////////////////////////////////////