#include <intrin.h>

#define MM_HAVE_ATOMIC_OPS  1

typedef mm_sint32_t                     mm_atomic_int_t;
typedef mm_uint32_t                    mm_atomic_uint_t;
typedef volatile mm_atomic_uint_t  mm_atomic_t;
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)


static mm_inline mm_atomic_uint_t
	mm_atomic_cmp_set(mm_atomic_t *lock, mm_atomic_uint_t old,
	mm_atomic_uint_t set)
{
#if defined(__ICC)      //x86 version
	return _InterlockedCompareExchange((void*)lock, set, old) == old;
#elif defined(__ECC)    //IA-64 version
	return _InterlockedCompareExchange((void*)lock, set, old) == old;
#elif defined(__ICL) || defined(_MSC_VER)
	return _InterlockedCompareExchange((volatile long*)(lock), set, old) == old;
#else
	return 0;
#endif
}


static mm_inline mm_atomic_int_t
	mm_atomic_fetch_add(mm_atomic_t *value, mm_atomic_int_t add)
{
#if defined(__ICC)      //x86 version
	return _InterlockedExchangeAdd((void*)value, add);
#elif defined(__ECC)    //IA-64 version
	return _InterlockedExchangeAdd((void*)value, add);
#elif defined(__ICL) || defined(_MSC_VER)
	return _InterlockedExchangeAdd((volatile long*)(value), add);
#else
	return 0;
#endif
}

#define mm_memory_barrier() MemoryBarrier()
#if ( __i386__ || __i386 || __amd64__ || __amd64 )
#define mm_cpu_pause() __asm__ ("pause")
#else
#define mm_cpu_pause()
#endif
