
/*
 * Copyright (C) Igor Sysoev
 * Copyright (C) Nginx, Inc.
 */


/*
 * "casa   [r1] 0x80, r2, r0"  and
 * "casxa  [r1] 0x80, r2, r0"  do the following:
 *
 *     if ([r1] == r2) {
 *         swap(r0, [r1]);
 *     } else {
 *         r0 = [r1];
 *     }
 *
 * so "r0 == r2" means that the operation was successful.
 *
 *
 * The "r" means the general register.
 * The "+r" means the general register used for both input and output.
 */


#if (MM_PTR_SIZE == 4)
#define MM_CASA  "casa"
#else
#define MM_CASA  "casxa"
#endif


static mm_inline mm_atomic_uint_t
mm_atomic_cmp_set(mm_atomic_t *lock, mm_atomic_uint_t old,
    mm_atomic_uint_t set)
{
    __asm__ volatile (

    MM_CASA " [%1] 0x80, %2, %0"

    : "+r" (set) : "r" (lock), "r" (old) : "memory");

    return (set == old);
}


static mm_inline mm_atomic_int_t
mm_atomic_fetch_add(mm_atomic_t *value, mm_atomic_int_t add)
{
    mm_atomic_uint_t  old, res;

    old = *value;

    for ( ;; ) {

        res = old + add;

        __asm__ volatile (

        MM_CASA " [%1] 0x80, %2, %0"

        : "+r" (res) : "r" (value), "r" (old) : "memory");

        if (res == old) {
            return res;
        }

        old = res;
    }
}


#if (MM_SMP)
#define mm_memory_barrier()                                                  \
            __asm__ volatile (                                                \
            "membar #LoadLoad | #LoadStore | #StoreStore | #StoreLoad"        \
            ::: "memory")
#else
#define mm_memory_barrier()   __asm__ volatile ("" ::: "memory")
#endif

#define mm_cpu_pause()
