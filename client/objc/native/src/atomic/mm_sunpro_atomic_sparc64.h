
/*
 * Copyright (C) Igor Sysoev
 * Copyright (C) Nginx, Inc.
 */


#if (MM_PTR_SIZE == 4)
#define MM_CASA  mm_casa
#else
#define MM_CASA  mm_casxa
#endif


mm_atomic_uint_t
mm_casa(mm_atomic_uint_t set, mm_atomic_uint_t old, mm_atomic_t *lock);

mm_atomic_uint_t
mm_casxa(mm_atomic_uint_t set, mm_atomic_uint_t old, mm_atomic_t *lock);

/* the code in src/os/unix/mm_sunpro_sparc64.il */


static mm_inline mm_atomic_uint_t
mm_atomic_cmp_set(mm_atomic_t *lock, mm_atomic_uint_t old,
    mm_atomic_uint_t set)
{
    set = MM_CASA(set, old, lock);

    return (set == old);
}


static mm_inline mm_atomic_int_t
mm_atomic_fetch_add(mm_atomic_t *value, mm_atomic_int_t add)
{
    mm_atomic_uint_t  old, res;

    old = *value;

    for ( ;; ) {

        res = old + add;

        res = MM_CASA(res, old, value);

        if (res == old) {
            return res;
        }

        old = res;
    }
}


#define mm_memory_barrier()                                                  \
        __asm (".volatile");                                                 \
        __asm ("membar #LoadLoad | #LoadStore | #StoreStore | #StoreLoad");  \
        __asm (".nonvolatile")

#define mm_cpu_pause()
