#ifndef __mm_config_platform_h__
#define __mm_config_platform_h__

#include "core/mm_prefix.h"

#include "core/mm_platform.h"
// sign current platform.
#define MM_LINUX 1

#include "core/mm_auto_headers.h"

#ifndef _GNU_SOURCE
#define _GNU_SOURCE             /* pread(), pwrite(), gethostname() */
#endif

#define _FILE_OFFSET_BITS  64

#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdarg.h>
#include <stddef.h>             /* offsetof() */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <pwd.h>
#include <grp.h>
#include <dirent.h>
// #include <glob.h>
#include <sys/vfs.h>            /* statfs() */

#include <sys/uio.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sched.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>        /* TCP_NODELAY, TCP_CORK */
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/un.h>

#include <time.h>               /* tzset() */
#include <malloc.h>             /* memalign() */
#include <limits.h>             /* IOV_MAX */
#include <sys/ioctl.h>
// #include <crypt.h>
#include <sys/utsname.h>        /* uname() */


#include <core/mm_auto_config.h>


#if (MM_HAVE_POSIX_SEM)
#include <semaphore.h>
#endif


#if (MM_HAVE_SYS_PRCTL_H)
#include <sys/prctl.h>
#endif


#if (MM_HAVE_SENDFILE64)
#include <sys/sendfile.h>
#else
extern ssize_t sendfile(int s, int fd, int32_t *offset, size_t size);
#define MM_SENDFILE_LIMIT  0x80000000
#endif


#if (MM_HAVE_POLL)
#include <poll.h>
#endif


#if (MM_HAVE_EPOLL)
#include <sys/epoll.h>
#endif


#if (MM_HAVE_SYS_EVENTFD_H)
#include <sys/eventfd.h>
#endif
#include <sys/syscall.h>
#if (MM_HAVE_FILE_AIO)
#include <linux/aio_abi.h>
typedef struct iocb  mm_aiocb_t;
#endif


#define MM_LISTEN_BACKLOG        511


#ifndef MM_HAVE_SO_SNDLOWAT
/* setsockopt(SO_SNDLOWAT) returns ENOPROTOOPT */
#define MM_HAVE_SO_SNDLOWAT         0
#endif


#ifndef MM_HAVE_INHERITED_NONBLOCK
#define MM_HAVE_INHERITED_NONBLOCK  0
#endif


#define MM_HAVE_OS_SPECIFIC_INIT    1
#define mm_debug_init()


extern char **environ;
//////////////////////////////////////////////////////////////////////////
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
//////////////////////////////////////////////////////////////////////////

#include "core/mm_suffix.h"

#endif//__mm_config_platform_h__