#ifndef __mm_timer_task_h__
#define __mm_timer_task_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_time.h"
#include "core/mm_thread.h"
#include "core/mm_socket.h"

#include <pthread.h>

// timer task default nearby time milliseconds.
#define MM_TIME_TASK_NEARBY_MSEC 200

// windows pthread pthread_cond_timedwait working terrible.
// linux   select  can not wake up for shutdown.
struct mm_timer_task;
typedef void (*timer_task_handle)(struct mm_timer_task* p);
struct mm_timer_task_callback
{
	timer_task_handle handle;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_timer_task_callback_init(struct mm_timer_task_callback* p);
MM_EXPORT_DLL void mm_timer_task_callback_destroy(struct mm_timer_task_callback* p);

struct mm_timer_task
{
	void* impl;// impl for time wait.strong ref.
	pthread_t task_thread;// check thread.
	struct mm_timer_task_callback callback;
	mm_msec_t nearby_time;// nearby_time milliseconds for each time wait.default is MM_TIME_TASK_NEARBY_MSEC ms.
	mm_msec_t msec_delay;// delay milliseconds before first callback.default is 0 ms.
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_timer_task_init(struct mm_timer_task* p);
MM_EXPORT_DLL void mm_timer_task_destroy(struct mm_timer_task* p);
//////////////////////////////////////////////////////////////////////////
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_timer_task_assign_callback(struct mm_timer_task* p,struct mm_timer_task_callback* callback);
MM_EXPORT_DLL void mm_timer_task_assign_nearby_time(struct mm_timer_task* p,mm_msec_t nearby_time);
//////////////////////////////////////////////////////////////////////////
// sync.
MM_EXPORT_DLL void mm_timer_task_handle(struct mm_timer_task* p);
//////////////////////////////////////////////////////////////////////////
// start thread.
MM_EXPORT_DLL void mm_timer_task_start(struct mm_timer_task* p);
// interrupt thread.
MM_EXPORT_DLL void mm_timer_task_interrupt(struct mm_timer_task* p);
// shutdown thread.
MM_EXPORT_DLL void mm_timer_task_shutdown(struct mm_timer_task* p);
// join thread.
MM_EXPORT_DLL void mm_timer_task_join(struct mm_timer_task* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL const char* mm_timer_task_impl_version(struct mm_timer_task* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_timer_task_h__