
#include "core/mm_prefix.h"

#ifndef MM_HAVE_UNISTD_H
#define MM_HAVE_UNISTD_H  1
#endif


#ifndef MM_HAVE_INTTYPES_H
#define MM_HAVE_INTTYPES_H  1
#endif


#ifndef MM_HAVE_LIMITS_H
#define MM_HAVE_LIMITS_H  1
#endif


#ifndef MM_HAVE_SYS_PARAM_H
#define MM_HAVE_SYS_PARAM_H  1
#endif


#ifndef MM_HAVE_SYS_MOUNT_H
#define MM_HAVE_SYS_MOUNT_H  1
#endif


#ifndef MM_HAVE_SYS_STATVFS_H
#define MM_HAVE_SYS_STATVFS_H  1
#endif


#ifndef MM_HAVE_CRYPT_H
#define MM_HAVE_CRYPT_H  1
#endif


#ifndef MM_LINUX
#define MM_LINUX  1
#endif


#ifndef MM_HAVE_SYS_PRCTL_H
#define MM_HAVE_SYS_PRCTL_H  1
#endif


#ifndef MM_HAVE_SYS_VFS_H
#define MM_HAVE_SYS_VFS_H  1
#endif

#include "core/mm_suffix.h"