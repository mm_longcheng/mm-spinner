#ifndef __mm_poll_h__
#define __mm_poll_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

#include "core/mm_socket.h"
#include "core/mm_time.h"

#define MM_PE_NONEABLE 0
#define MM_PE_READABLE 1
#define MM_PE_WRITABLE 2

struct mm_poll_event
{
	void* s;
	int mask;// mask for MM_PE_NONEABLE | MM_PE_READABLE | MM_PE_WRITABLE
};

struct mm_poll
{
	mm_uint32_t ev_length;
	struct mm_poll_event* pe;
	void* impl;
};
MM_EXPORT_DLL void mm_poll_init(struct mm_poll* p);
MM_EXPORT_DLL void mm_poll_destroy(struct mm_poll* p);
// poll ev length default is 256.set it before mm_poll_wait,or mm_poll_wait after.
MM_EXPORT_DLL void mm_poll_set_length(struct mm_poll* p, mm_uint32_t length);
MM_EXPORT_DLL mm_uint32_t mm_poll_get_length(struct mm_poll* p);
MM_EXPORT_DLL mm_int_t mm_poll_add_event(struct mm_poll* p, mm_socket_t fd, int mask, void* ud);
MM_EXPORT_DLL mm_int_t mm_poll_mod_event(struct mm_poll* p, mm_socket_t fd, int mask, void* ud);
MM_EXPORT_DLL mm_int_t mm_poll_del_event(struct mm_poll* p, mm_socket_t fd, int mask, void* ud);
MM_EXPORT_DLL mm_int_t mm_poll_wait_event(struct mm_poll* p,mm_msec_t milliseconds);

MM_EXPORT_DLL const mm_char_t* mm_poll_api();

#include "core/mm_suffix.h"


#endif//__mm_poll_h__