#include "core/mm_time.h"


/*
 * FreeBSD does not test /etc/localtime change, however, we can workaround it
 * by calling tzset() with TZ and then without TZ to update timezone.
 * The trick should work since FreeBSD 2.1.0.
 *
 * Linux does not test /etc/localtime change in localtime(),
 * but may stat("/etc/localtime") several times in every strftime(),
 * therefore we use it to update timezone.
 *
 * Solaris does not test /etc/TIMEZONE change too and no workaround available.
 */

MM_EXPORT_DLL void
mm_timezone_update(void)
{
#if (MM_FREEBSD)

    if (getenv("TZ")) {
        return;
    }

    putenv("TZ=UTC");

    tzset();

    unsetenv("TZ");

    tzset();

#elif (MM_LINUX)
    time_t      s;
    struct tm  *t;
    char        buf[4];

    s = time(0);

    t = localtime(&s);

    strftime(buf, 4, "%H", t);

#endif
}


MM_EXPORT_DLL void
mm_localtime_r(const time_t* s, mm_tm_t *tm)
{
#if (MM_HAVE_LOCALTIME_R)
    (void) localtime_r(s, tm);

#else
    mm_tm_t  *t;

    t = localtime(s);
    *tm = *t;

#endif

    tm->mm_tm_mon++;
    tm->mm_tm_year += 1900;
}


MM_EXPORT_DLL void
mm_libc_localtime(const time_t* s, struct tm *tm)
{
#if (MM_HAVE_LOCALTIME_R)
    (void) localtime_r(s, tm);

#else
    struct tm  *t;

    t = localtime(s);
    *tm = *t;

#endif
}


MM_EXPORT_DLL void
mm_libc_gmtime(const time_t* s, struct tm *tm)
{
#if (MM_HAVE_LOCALTIME_R)
    (void) gmtime_r(s, tm);

#else
    struct tm  *t;

    t = gmtime(s);
    *tm = *t;

#endif
}

MM_EXPORT_DLL void 
mm_usleep(mm_ulong_t microseconds)
{
	struct timespec request, remaining;
	request.tv_sec = microseconds / MM_USEC_PER_SEC;
	request.tv_nsec = 1000 * (microseconds % MM_USEC_PER_SEC);
	while (nanosleep (&request, &remaining) == -1 && errno == EINTR)
		request = remaining;
}

/* NB NB NB NB This returns GMT!!!!!!!!!! */
MM_EXPORT_DLL mm_time_t 
mm_time_now(void)
{
    struct timeval tv;
    mm_gettimeofday(&tv, NULL);
    return tv.tv_sec * MM_USEC_PER_SEC + tv.tv_usec;
}