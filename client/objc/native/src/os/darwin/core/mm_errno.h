#ifndef __mm_errno_h__
#define __mm_errno_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"


typedef int               mm_err_t;

#define MM_EPERM         EPERM
#define MM_ENOENT        ENOENT
#define MM_ENOPATH       ENOENT
#define MM_ESRCH         ESRCH
#define MM_EINTR         EINTR
#define MM_ECHILD        ECHILD
#define MM_ENOMEM        ENOMEM
#define MM_EACCES        EACCES
#define MM_EBUSY         EBUSY
#define MM_EEXIST        EEXIST
#define MM_EXDEV         EXDEV
#define MM_ENOTDIR       ENOTDIR
#define MM_EISDIR        EISDIR
#define MM_EINVAL        EINVAL
#define MM_ENFILE        ENFILE
#define MM_EMFILE        EMFILE
#define MM_ENOSPC        ENOSPC
#define MM_EPIPE         EPIPE
#define MM_EINPROGRESS   EINPROGRESS
#define MM_ENOPROTOOPT   ENOPROTOOPT
#define MM_EOPNOTSUPP    EOPNOTSUPP
#define MM_EADDRINUSE    EADDRINUSE
#define MM_ECONNABORTED  ECONNABORTED
#define MM_ECONNRESET    ECONNRESET
#define MM_ENOTCONN      ENOTCONN
#define MM_ENOTSOCK		 ENOTSOCK
#define MM_ENOBUFS		 ENOBUFS
#define MM_ETIMEDOUT     ETIMEDOUT
#define MM_ECONNREFUSED  ECONNREFUSED
#define MM_ENAMETOOLONG  ENAMETOOLONG
#define MM_ENETDOWN      ENETDOWN
#define MM_ENETUNREACH   ENETUNREACH
#define MM_EHOSTDOWN     EHOSTDOWN
#define MM_EHOSTUNREACH  EHOSTUNREACH
#define MM_ENOSYS        ENOSYS
#define MM_ECANCELED     ECANCELED
#define MM_EILSEQ        EILSEQ
#define MM_ENOMOREFILES  0
#define MM_ELOOP         ELOOP
#define MM_EBADF         EBADF

#if (MM_HAVE_OPENAT)
#define MM_EMLINK        EMLINK
#endif

#if (__hpux__)
#define MM_EAGAIN        EWOULDBLOCK
#else
#define MM_EAGAIN        EAGAIN
#endif


#define mm_errno                  errno
#define mm_socket_errno           errno
#define mm_set_errno(err)         errno = err
#define mm_set_socket_errno(err)  errno = err


MM_EXPORT_DLL mm_uchar_t* mm_strerror(mm_err_t err, mm_uchar_t* errstr, size_t size);
MM_EXPORT_DLL mm_int_t mm_strerror_init(void);
MM_EXPORT_DLL mm_int_t mm_strerror_destroy(void);

#include "core/mm_suffix.h"

#endif//__mm_errno_h__
