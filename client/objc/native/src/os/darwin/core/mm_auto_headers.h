
#include "core/mm_prefix.h"

#ifndef MM_HAVE_UNISTD_H
#define MM_HAVE_UNISTD_H  1
#endif


#ifndef MM_HAVE_INTTYPES_H
#define MM_HAVE_INTTYPES_H  1
#endif


#ifndef MM_HAVE_LIMITS_H
#define MM_HAVE_LIMITS_H  1
#endif


#ifndef MM_HAVE_SYS_FILIO_H
#define MM_HAVE_SYS_FILIO_H  1
#endif


#ifndef MM_HAVE_SYS_PARAM_H
#define MM_HAVE_SYS_PARAM_H  1
#endif


#ifndef MM_HAVE_SYS_MOUNT_H
#define MM_HAVE_SYS_MOUNT_H  1
#endif


#ifndef MM_HAVE_SYS_STATVFS_H
#define MM_HAVE_SYS_STATVFS_H  1
#endif


#ifndef MM_DARWIN
#define MM_DARWIN  1
#endif

#include "core/mm_suffix.h"