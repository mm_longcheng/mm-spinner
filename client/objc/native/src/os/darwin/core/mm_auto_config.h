#ifndef __mm_auto_config_h__
#define __mm_auto_config_h__

#include "core/mm_prefix.h"

// some platform macro definition.
#ifndef MM_COMPILER_NAME
#define MM_COMPILER_NAME  "clang 6.0 (clang-600.0.57) (based on LLVM 3.5svn)"
#endif


#ifndef MM_HAVE_GCC_ATOMIC
#define MM_HAVE_GCC_ATOMIC  1
#endif


#ifndef MM_HAVE_C99_VARIADIC_MACROS
#define MM_HAVE_C99_VARIADIC_MACROS  1
#endif


#ifndef MM_HAVE_GCC_VARIADIC_MACROS
#define MM_HAVE_GCC_VARIADIC_MACROS  1
#endif


#ifndef MM_HAVE_KQUEUE
#define MM_HAVE_KQUEUE  1
#endif


#ifndef MM_HAVE_CLEAR_EVENT
#define MM_HAVE_CLEAR_EVENT  1
#endif


#ifndef MM_HAVE_TIMER_EVENT
#define MM_HAVE_TIMER_EVENT  1
#endif


#ifndef MM_HAVE_SENDFILE
#define MM_HAVE_SENDFILE  1
#endif


#ifndef MM_DARWIN_ATOMIC
#define MM_DARWIN_ATOMIC  1
#endif


#ifndef MM_HAVE_NONALIGNED
#define MM_HAVE_NONALIGNED  1
#endif


#ifndef MM_CPU_CACHE_LINE
#define MM_CPU_CACHE_LINE  64
#endif


#define MM_KQUEUE_UDATA_T  (void *)


#ifndef MM_HAVE_F_NOCACHE
#define MM_HAVE_F_NOCACHE  1
#endif


#ifndef MM_HAVE_STATFS
#define MM_HAVE_STATFS  1
#endif


#ifndef MM_HAVE_STATVFS
#define MM_HAVE_STATVFS  1
#endif


#ifndef MM_HAVE_SCHED_YIELD
#define MM_HAVE_SCHED_YIELD  1
#endif


#ifndef MM_HAVE_REUSEPORT
#define MM_HAVE_REUSEPORT  1
#endif


#ifndef MM_HAVE_UNIX_DOMAIN
#define MM_HAVE_UNIX_DOMAIN  1
#endif


#ifndef MM_PTR_SIZE
#define MM_PTR_SIZE  8
#endif


#ifndef MM_SIG_ATOMIC_T_SIZE
#define MM_SIG_ATOMIC_T_SIZE  4
#endif


#ifndef MM_HAVE_LITTLE_ENDIAN
#define MM_HAVE_LITTLE_ENDIAN  1
#endif


#ifndef MM_MAX_SIZE_T_VALUE
#define MM_MAX_SIZE_T_VALUE  9223372036854775807LL
#endif


#ifndef MM_SIZE_T_LEN
#define MM_SIZE_T_LEN  (sizeof("-9223372036854775808") - 1)
#endif


#ifndef MM_MAX_OFF_T_VALUE
#define MM_MAX_OFF_T_VALUE  9223372036854775807LL
#endif


#ifndef MM_OFF_T_LEN
#define MM_OFF_T_LEN  (sizeof("-9223372036854775808") - 1)
#endif


#ifndef MM_TIME_T_SIZE
#define MM_TIME_T_SIZE  8
#endif


#ifndef MM_TIME_T_LEN
#define MM_TIME_T_LEN  (sizeof("-9223372036854775808") - 1)
#endif


#ifndef MM_MAX_TIME_T_VALUE
#define MM_MAX_TIME_T_VALUE  9223372036854775807LL
#endif


#ifndef MM_HAVE_PREAD
#define MM_HAVE_PREAD  1
#endif


#ifndef MM_HAVE_PWRITE
#define MM_HAVE_PWRITE  1
#endif


#ifndef MM_SYS_NERR
#define MM_SYS_NERR  107
#endif


#ifndef MM_HAVE_LOCALTIME_R
#define MM_HAVE_LOCALTIME_R  1
#endif


#ifndef MM_HAVE_POSIX_MEMALIGN
#define MM_HAVE_POSIX_MEMALIGN  1
#endif


#ifndef MM_HAVE_MAP_ANON
#define MM_HAVE_MAP_ANON  1
#endif


#ifndef MM_HAVE_SYSVSHM
#define MM_HAVE_SYSVSHM  1
#endif


#ifndef MM_HAVE_MSGHDR_MSG_CONTROL
#define MM_HAVE_MSGHDR_MSG_CONTROL  1
#endif


#ifndef MM_HAVE_FIONBIO
#define MM_HAVE_FIONBIO  1
#endif


#ifndef MM_HAVE_GMTOFF
#define MM_HAVE_GMTOFF  1
#endif


#ifndef MM_HAVE_D_NAMLEN
#define MM_HAVE_D_NAMLEN  1
#endif


#ifndef MM_HAVE_D_TYPE
#define MM_HAVE_D_TYPE  1
#endif


#ifndef MM_HAVE_SC_NPROCESSORS_ONLN
#define MM_HAVE_SC_NPROCESSORS_ONLN  1
#endif


#ifndef MM_HAVE_OPENAT
#define MM_HAVE_OPENAT  1
#endif


#ifndef MM_HAVE_GETADDRINFO
#define MM_HAVE_GETADDRINFO  1
#endif


#ifndef MM_HTTP_CACHE
#define MM_HTTP_CACHE  1
#endif


#ifndef MM_HTTP_GZIP
#define MM_HTTP_GZIP  1
#endif


#ifndef MM_HTTP_SSI
#define MM_HTTP_SSI  1
#endif


#ifndef MM_CRYPT
#define MM_CRYPT  1
#endif


#ifndef MM_HTTP_X_FORWARDED_FOR
#define MM_HTTP_X_FORWARDED_FOR  1
#endif


#ifndef MM_HTTP_X_FORWARDED_FOR
#define MM_HTTP_X_FORWARDED_FOR  1
#endif


#ifndef MM_HTTP_UPSTREAM_ZONE
#define MM_HTTP_UPSTREAM_ZONE  1
#endif


#ifndef MM_OPENSSL_MD5
#define MM_OPENSSL_MD5  1
#endif


#ifndef MM_HAVE_OPENSSL_MD5_H
#define MM_HAVE_OPENSSL_MD5_H  1
#endif


#ifndef MM_HAVE_MD5
#define MM_HAVE_MD5  1
#endif


#ifndef MM_HAVE_SHA1
#define MM_HAVE_SHA1  1
#endif


#ifndef MM_HAVE_OPENSSL_SHA1_H
#define MM_HAVE_OPENSSL_SHA1_H  1
#endif


#ifndef MM_ZLIB
#define MM_ZLIB  1
#endif


#ifndef MM_PREFIX
#define MM_PREFIX  "/usr/local/nginx/"
#endif


#ifndef MM_CONF_PREFIX
#define MM_CONF_PREFIX  "conf/"
#endif


#ifndef MM_SBIN_PATH
#define MM_SBIN_PATH  "sbin/nginx"
#endif


#ifndef MM_CONF_PATH
#define MM_CONF_PATH  "conf/nginx.conf"
#endif


#ifndef MM_PID_PATH
#define MM_PID_PATH  "logs/nginx.pid"
#endif


#ifndef MM_LOCK_PATH
#define MM_LOCK_PATH  "logs/nginx.lock"
#endif


#ifndef MM_ERROR_LOG_PATH
#define MM_ERROR_LOG_PATH  "logs/error.log"
#endif


#ifndef MM_HTTP_LOG_PATH
#define MM_HTTP_LOG_PATH  "logs/access.log"
#endif


#ifndef MM_HTTP_CLIENT_TEMP_PATH
#define MM_HTTP_CLIENT_TEMP_PATH  "client_body_temp"
#endif


#ifndef MM_HTTP_PROXY_TEMP_PATH
#define MM_HTTP_PROXY_TEMP_PATH  "proxy_temp"
#endif


#ifndef MM_HTTP_FASTCGI_TEMP_PATH
#define MM_HTTP_FASTCGI_TEMP_PATH  "fastcgi_temp"
#endif


#ifndef MM_HTTP_UWSGI_TEMP_PATH
#define MM_HTTP_UWSGI_TEMP_PATH  "uwsgi_temp"
#endif


#ifndef MM_HTTP_SCGI_TEMP_PATH
#define MM_HTTP_SCGI_TEMP_PATH  "scgi_temp"
#endif


#ifndef MM_SUPPRESS_WARN
#define MM_SUPPRESS_WARN  1
#endif


#ifndef MM_SMP
#define MM_SMP  1
#endif


#ifndef MM_USER
#define MM_USER  "nobody"
#endif


#ifndef MM_GROUP
#define MM_GROUP  "nobody"
#endif

#include "core/mm_suffix.h"
//
#endif//__mm_auto_config_h__