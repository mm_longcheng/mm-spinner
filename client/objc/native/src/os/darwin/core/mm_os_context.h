#ifndef __mm_os_context_h__
#define __mm_os_context_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

#include "core/mm_logger.h"
#include "core/mm_cpuinfo.h"

struct mm_os_context
{
	struct mm_string name;
	struct mm_cpuinfo cpuinfo;
	mm_uint32_t cacheline_size;
	mm_uint32_t cores;
	mm_uint32_t pagesize;
	mm_uint32_t pagesize_shift;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL extern struct mm_os_context* mm_os_context_instance();
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_os_context_init(struct mm_os_context* p);
MM_EXPORT_DLL void mm_os_context_destroy(struct mm_os_context* p);
//
MM_EXPORT_DLL void mm_os_context_perform(struct mm_os_context* p);
//
MM_EXPORT_DLL void mm_os_context_logger_console(const char* section,const char* time_stamp_string, int lvl,const char* message);

#include "core/mm_suffix.h"

#endif//__mm_os_context_h__