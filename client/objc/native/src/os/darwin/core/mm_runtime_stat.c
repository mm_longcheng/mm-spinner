#include "mm_runtime_stat.h"
#include "core/mm_logger.h"
///////////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_runtime_loadavg_init(struct mm_runtime_loadavg* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_loadavg));
}
MM_EXPORT_DLL void mm_runtime_loadavg_destroy(struct mm_runtime_loadavg* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_loadavg));
}
MM_EXPORT_DLL void mm_runtime_loadavg_perform(struct mm_runtime_loadavg* p)
{
	// struct loadavg {
	//         fixpt_t ldavg[3];
	//         long    fscale;
	// };
	struct loadavg _loadavg;
	size_t size = sizeof(struct loadavg);
	static const int arg_n = 2;
	int mib[arg_n];
	mib[0] = CTL_VM;
	mib[1] = VM_LOADAVG;
	sysctl(mib,arg_n, &_loadavg, &size, NULL, 0);
	p->lavg_01 = _loadavg.ldavg[0] * 1.0f / _loadavg.fscale;
	p->lavg_05 = _loadavg.ldavg[1] * 1.0f / _loadavg.fscale;
	p->lavg_15 = _loadavg.ldavg[2] * 1.0f / _loadavg.fscale;
}
///////////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_runtime_machine_init(struct mm_runtime_machine* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_machine));
}
MM_EXPORT_DLL void mm_runtime_machine_destroy(struct mm_runtime_machine* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_machine));
}
MM_EXPORT_DLL void mm_runtime_machine_perform(struct mm_runtime_machine* p)
{
	mm_runtime_machine_perform_cpu(p);
	mm_runtime_machine_perform_mem(p);
}
MM_EXPORT_DLL void mm_runtime_machine_perform_cpu(struct mm_runtime_machine* p)
{
	p->cores = (mm_uint32_t)sysconf(_SC_NPROCESSORS_ONLN);
}
MM_EXPORT_DLL void mm_runtime_machine_perform_mem(struct mm_runtime_machine* p)
{
	{
		uint64_t hw_memsize = 0;
		size_t size = sizeof(uint64_t);
		static const int arg_n = 2;
		int mib[arg_n];
		mib[0] = CTL_HW;
		mib[1] = HW_MEMSIZE;
		sysctl(mib,arg_n, &hw_memsize, &size, NULL, 0);
		p->mem_total = hw_memsize;
	}
	// {
	// 	uint64_t vm_free_target = 0;
	// 	size_t size = sizeof(uint64_t);
	// 	static const int arg_n = 2;
	// 	int mib[arg_n];
	// 	mib[0] = CTL_VM;
	// 	mib[1] = VM_V_FREE_TARGET;
	// 	sysctl(mib,arg_n, &vm_free_target, &size, NULL, 0);
	// 	p->mem_free = vm_free_target;
	// }
}
///////////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_runtime_process_init(struct mm_runtime_process* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_process));
}
MM_EXPORT_DLL void mm_runtime_process_destroy(struct mm_runtime_process* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_process));
}
MM_EXPORT_DLL void mm_runtime_process_perform(struct mm_runtime_process* p)
{
	mm_runtime_process_perform_pid(p);
	mm_runtime_process_perform_cpu(p);
	mm_runtime_process_perform_mem(p);
	mm_runtime_process_perform_io(p);
}
MM_EXPORT_DLL void mm_runtime_process_perform_pid(struct mm_runtime_process* p)
{
	p->pid = getpid();
}
MM_EXPORT_DLL void mm_runtime_process_perform_cpu(struct mm_runtime_process* p)
{
	struct kinfo_proc proc;
	size_t size = sizeof(struct kinfo_proc);
	static const int arg_n = 4;
	int mib[arg_n];
	mib[0] = CTL_KERN;
	mib[1] = KERN_PROC;
	mib[2] = KERN_PROC_PID;
	mib[3] = (int)p->pid;
	sysctl(mib,arg_n, &proc, &size, NULL, 0);
    
    // u_quad_t p_uticks;		/* Statclock hits in user mode. */
    // u_quad_t p_sticks;		/* Statclock hits in system mode. */
    // u_quad_t p_iticks;		/* Statclock hits processing intr. */
    
    p->cpu_utime = proc.kp_proc.p_uticks;
    p->cpu_stime = proc.kp_proc.p_sticks;
    p->cpu_cutime = 0;
    p->cpu_cstime = 0;

}
MM_EXPORT_DLL void mm_runtime_process_perform_mem(struct mm_runtime_process* p)
{

}
MM_EXPORT_DLL void mm_runtime_process_perform_io(struct mm_runtime_process* p)
{

}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_runtime_stat_init( struct mm_runtime_stat* p )
{
	p->pid = 0;
	p->cores = 0;
	p->vm_rss = 0;
	p->rchar = 0;
	p->wchar = 0;
	p->cpu_sys = 0;
	p->cpu_pro = 0;
	p->mem_pro = 0;
	p->timecode_o = 0;
	p->timecode_n = 0;
	p->sampling = 0;
	mm_memset(&p->tv_o,0,sizeof(struct timeval));
	mm_memset(&p->tv_n,0,sizeof(struct timeval));
	mm_runtime_loadavg_init(&p->loadavg_o);
	mm_runtime_loadavg_init(&p->loadavg_n);
	mm_runtime_machine_init(&p->machine_o);
	mm_runtime_machine_init(&p->machine_n);
	mm_runtime_process_init(&p->process_o);
	mm_runtime_process_init(&p->process_n);
}

MM_EXPORT_DLL void mm_runtime_stat_destroy( struct mm_runtime_stat* p )
{
	p->pid = 0;
	p->cores = 0;
	p->vm_rss = 0;
	p->rchar = 0;
	p->wchar = 0;
	p->cpu_sys = 0;
	p->cpu_pro = 0;
	p->mem_pro = 0;
	p->timecode_o = 0;
	p->timecode_n = 0;
	p->sampling = 0;
	mm_memset(&p->tv_o,0,sizeof(struct timeval));
	mm_memset(&p->tv_n,0,sizeof(struct timeval));
	mm_runtime_loadavg_destroy(&p->loadavg_o);
	mm_runtime_loadavg_destroy(&p->loadavg_n);
	mm_runtime_machine_destroy(&p->machine_o);
	mm_runtime_machine_destroy(&p->machine_n);
	mm_runtime_process_destroy(&p->process_o);
	mm_runtime_process_destroy(&p->process_n);
}

MM_EXPORT_DLL void mm_runtime_stat_update( struct mm_runtime_stat* p )
{
	mm_uint64_t b_utime,b_stime,b_cutime,b_cstime;
	mm_uint64_t b_user,b_nice,b_system,b_idle,a,b,c;
	//////////////////////////////////////////////////////////////////////////
	mm_gettimeofday(&p->tv_n,NULL);
	p->timecode_n = p->tv_n.tv_sec * 1000 + (p->tv_n.tv_usec / 1000);
	p->sampling = p->timecode_n - p->timecode_o;
	//
	mm_runtime_loadavg_perform(&p->loadavg_n);
	mm_runtime_machine_perform(&p->machine_n);
	mm_runtime_process_perform(&p->process_n);
	//////////////////////////////////////////////////////////////////////////
	p->pid = p->process_n.pid;
	p->vm_rss = p->process_n.vm_rss;
	p->rchar = p->process_n.rchar;
	p->wchar = p->process_n.wchar;
	p->cores = p->machine_n.cores;
	//
	b_utime = p->process_n.cpu_utime - p->process_o.cpu_utime;
	b_stime = p->process_n.cpu_stime - p->process_o.cpu_stime;
	b_cutime = p->process_n.cpu_cutime - p->process_o.cpu_cutime;
	b_cstime = p->process_n.cpu_cstime - p->process_o.cpu_cstime;
	a = b_utime + b_stime + b_cutime + b_cstime;

	b_user = p->machine_n.cpu_user - p->machine_o.cpu_user;
	b_nice = p->machine_n.cpu_nice - p->machine_o.cpu_nice;
	b_system = p->machine_n.cpu_system - p->machine_o.cpu_system;
	b_idle = p->machine_n.cpu_idle - p->machine_o.cpu_idle;
	b = b_user + b_nice + b_system + b_idle;
	c = b_user + b_nice + b_system;

	p->cpu_sys = ( 0 == c && 0 == b_idle ) ? 0 : c * 100.0f /( c + b_idle );
	p->cpu_pro = ( 0 == b ) ? 0 : a * 100.0f / b;
	p->mem_pro = p->vm_rss * 100.0f / p->machine_o.mem_total;
	//////////////////////////////////////////////////////////////////////////
	mm_memcpy(&p->loadavg_o,&p->loadavg_n,sizeof(struct mm_runtime_loadavg));
	mm_memcpy(&p->machine_o,&p->machine_n,sizeof(struct mm_runtime_machine));
	mm_memcpy(&p->process_o,&p->process_n,sizeof(struct mm_runtime_process));
	p->timecode_o = p->timecode_n;
}
//////////////////////////////////////////////////////////////////////////