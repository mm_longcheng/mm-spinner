#ifndef __mm_process_h__
#define __mm_process_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

#if (MM_HAVE_SCHED_YIELD)
#define mm_sched_yield()  sched_yield()
#else
#define mm_sched_yield()  usleep(1)
#endif

#include "core/mm_suffix.h"

#endif//__mm_process_h__