#include "mm_timer_task.h"
#include "core/mm_errno.h"
#include "core/mm_logger.h"
#include "core/mm_time_cache.h"
//////////////////////////////////////////////////////////////////////////
// timer task impl pthread version.
struct mm_timer_task_pthread
{
	pthread_mutex_t mutex;
	pthread_cond_t cond;
};
MM_EXPORT_DLL void mm_timer_task_pthread_init(struct mm_timer_task_pthread* p)
{
	pthread_mutex_init(&p->mutex,NULL);
	pthread_cond_init(&p->cond,NULL);
}
MM_EXPORT_DLL void mm_timer_task_pthread_destroy(struct mm_timer_task_pthread* p)
{
	pthread_mutex_destroy(&p->mutex);
	pthread_cond_destroy(&p->cond);
}
//////////////////////////////////////////////////////////////////////////
static void* __static_timer_task_handle_thread(void* _arg);
//////////////////////////////////////////////////////////////////////////
static void __static_timer_task_handle(struct mm_timer_task* p)
{

}
MM_EXPORT_DLL void mm_timer_task_callback_init(struct mm_timer_task_callback* p)
{
	p->handle = &__static_timer_task_handle;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_timer_task_callback_destroy(struct mm_timer_task_callback* p)
{
	p->handle = &__static_timer_task_handle;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_timer_task_init(struct mm_timer_task* p)
{
	p->impl = mm_malloc(sizeof(struct mm_timer_task_pthread));// impl for time wait.strong ref.
	mm_timer_task_pthread_init((struct mm_timer_task_pthread*)p->impl);
	mm_timer_task_callback_init(&p->callback);
	p->nearby_time = MM_TIME_TASK_NEARBY_MSEC;// nearby_time milliseconds for each time wait.default is MM_TIME_TASK_NEARBY_MSEC ms.
	p->msec_delay = 0;// delay milliseconds before first callback.default is 0 ms.
	p->state = ts_closed;// mm_thread_state_t,default is ts_closed(0)
}
MM_EXPORT_DLL void mm_timer_task_destroy(struct mm_timer_task* p)
{
	mm_timer_task_pthread_destroy((struct mm_timer_task_pthread*)p->impl);
	mm_free((struct mm_timer_task_select*)p->impl);
	p->impl = NULL;// impl for time wait.strong ref.
	mm_timer_task_callback_destroy(&p->callback);
	p->nearby_time = 0;// nearby_time milliseconds for each time wait.default is 0 ms.
	p->msec_delay = 0;// delay milliseconds before first callback.default is 0 ms.
	p->state = ts_closed;// mm_thread_state_t,default is ts_closed(0)
}
//////////////////////////////////////////////////////////////////////////
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_timer_task_assign_callback(struct mm_timer_task* p,struct mm_timer_task_callback* callback)
{
	assert(NULL != callback && "you can not assign null callback.");
	p->callback = *callback;
}
MM_EXPORT_DLL void mm_timer_task_assign_nearby_time(struct mm_timer_task* p,mm_msec_t nearby_time)
{
	p->nearby_time = nearby_time;
}
//////////////////////////////////////////////////////////////////////////
// sync.
MM_EXPORT_DLL void mm_timer_task_handle(struct mm_timer_task* p)
{
	struct mm_timer_task_pthread* _impl = (struct mm_timer_task_pthread*)p->impl;
	//////////////////////////////////////////////////////////////////////////
	struct timeval  ntime;
	struct timespec otime;
	int rt = 0;
	assert(p->callback.handle&&"p->callback.handle is a null.");
	if ( 0 < p->msec_delay )
	{
		mm_msleep(p->msec_delay);
	}
	while( ts_motion == p->state )
	{
		// timewait nearby.
		rt = mm_timedwait_nearby(&_impl->cond, &_impl->mutex, &ntime, &otime, p->nearby_time);
		// check the timewait for callback.
		if (0 == p->nearby_time || 0 == rt || ETIMEDOUT == rt )
		{
			(*(p->callback.handle))(p);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
// start thread.
MM_EXPORT_DLL void mm_timer_task_start(struct mm_timer_task* p)
{
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	pthread_create(&p->task_thread, NULL, &__static_timer_task_handle_thread, p);
}
// interrupt thread.
MM_EXPORT_DLL void mm_timer_task_interrupt(struct mm_timer_task* p)
{
	struct mm_timer_task_pthread* _impl = (struct mm_timer_task_pthread*)p->impl;
	if ( NULL != _impl )
	{
		//////////////////////////////////////////////////////////////////////////
		p->state = ts_closed;
		//////////////////////////////////////////////////////////////////////////
		pthread_mutex_lock(&_impl->mutex);
		pthread_cond_signal(&_impl->cond);
		pthread_mutex_unlock(&_impl->mutex);
	}
}
// shutdown thread.
MM_EXPORT_DLL void mm_timer_task_shutdown(struct mm_timer_task* p)
{
	struct mm_timer_task_pthread* _impl = (struct mm_timer_task_pthread*)p->impl;
	if ( NULL != _impl )
	{
		//////////////////////////////////////////////////////////////////////////
		p->state = ts_finish;
		//////////////////////////////////////////////////////////////////////////
		pthread_mutex_lock(&_impl->mutex);
		pthread_cond_signal(&_impl->cond);
		pthread_mutex_unlock(&_impl->mutex);
	}
}
// join thread.
MM_EXPORT_DLL void mm_timer_task_join(struct mm_timer_task* p)
{
	pthread_join(p->task_thread, NULL);
}
MM_EXPORT_DLL const char* mm_timer_task_impl_version(struct mm_timer_task* p)
{
	return "mm_timer_task_pthread";
}
//////////////////////////////////////////////////////////////////////////
static void* __static_timer_task_handle_thread(void* _arg)
{
	struct mm_timer_task* p = (struct mm_timer_task*)(_arg);
	mm_timer_task_handle(p);
	return NULL;
}