#ifndef __mm_errno_h__
#define __mm_errno_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

typedef DWORD                      mm_err_t;

#define mm_errno                  GetLastError()
#define mm_set_errno(err)         SetLastError(err)
#define mm_socket_errno           WSAGetLastError()
#define mm_set_socket_errno(err)  WSASetLastError(err)

#define MM_EPERM                  ERROR_ACCESS_DENIED
#define MM_ENOENT                 ERROR_FILE_NOT_FOUND
#define MM_ENOPATH                ERROR_PATH_NOT_FOUND
#define MM_ENOMEM                 ERROR_NOT_ENOUGH_MEMORY
#define MM_EACCES                 ERROR_ACCESS_DENIED
/* it's seems that ERROR_FILE_EXISTS is not appropriate error code */
#define MM_EEXIST                 ERROR_ALREADY_EXISTS
/*
 * could not found cross volume directory move error code,
 * so use ERROR_WRONG_DISK as stub one
 */
#define MM_EXDEV                  ERROR_WRONG_DISK
#define MM_ENOTDIR                ERROR_PATH_NOT_FOUND
#define MM_EISDIR                 ERROR_CANNOT_MAKE
#define MM_ENOSPC                 ERROR_DISK_FULL
#define MM_EPIPE                  EPIPE
#define MM_EINTR				  WSAEINTR
#define MM_EAGAIN                 WSAEWOULDBLOCK
#define MM_EINPROGRESS            WSAEINPROGRESS
#define MM_ENOPROTOOPT            WSAENOPROTOOPT
#define MM_EOPNOTSUPP             WSAEOPNOTSUPP
#define MM_EADDRINUSE             WSAEADDRINUSE
#define MM_ECONNABORTED           WSAECONNABORTED
#define MM_ECONNRESET             WSAECONNRESET
#define MM_ENOTCONN               WSAENOTCONN
#define MM_ENOTSOCK				  WSAENOTSOCK
#define MM_ENOBUFS				  WSAENOBUFS
#define MM_ETIMEDOUT              WSAETIMEDOUT
#define MM_ECONNREFUSED           WSAECONNREFUSED
#define MM_ENAMETOOLONG           ERROR_BAD_PATHNAME
#define MM_ENETDOWN               WSAENETDOWN
#define MM_ENETUNREACH            WSAENETUNREACH
#define MM_EHOSTDOWN              WSAEHOSTDOWN
#define MM_EHOSTUNREACH           WSAEHOSTUNREACH
#define MM_ENOMOREFILES           ERROR_NO_MORE_FILES
#define MM_EILSEQ                 ERROR_NO_UNICODE_TRANSLATION
#define MM_ELOOP                  0
#define MM_EBADF                  WSAEBADF

#define MM_EALREADY               WSAEALREADY
#define MM_EINVAL                 WSAEINVAL
#define MM_EMFILE                 WSAEMFILE
#define MM_ENFILE                 WSAEMFILE


MM_EXPORT_DLL mm_uchar_t *mm_strerror(mm_err_t err, mm_uchar_t *errstr, size_t size);
MM_EXPORT_DLL mm_int_t mm_strerror_init(void);
MM_EXPORT_DLL mm_int_t mm_strerror_destroy(void);

#include "core/mm_suffix.h"

#endif//__mm_errno_h__
