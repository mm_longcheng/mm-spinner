#ifndef __mm_alloc_h__
#define __mm_alloc_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include <stdlib.h>
#include <string.h>

struct mm_logger;

#define mm_malloc        malloc
#define mm_realloc       realloc
#define mm_free          free

#define mm_calloc        calloc

#define mm_memzero(buf, n)       (void) memset(buf, 0, n)
#define mm_memset(buf, c, n)     (void) memset(buf, c, n)
#define mm_memcpy(dst, src, n)   (void) memcpy(dst, src, n)
#define mm_cpymem(dst, src, n)   (((mm_uchar_t *) memcpy(dst, src, n)) + (n))
#define mm_memmove(dst, src, n)   (void) memmove(dst, src, n)
#define mm_movemem(dst, src, n)   (((mm_uchar_t *) memmove(dst, src, n)) + (n))
#define mm_memcmp(s1, s2, n)  memcmp((const char *) s1, (const char *) s2, n)

#define mm_aligned_malloc _aligned_malloc
#define mm_aligned_free _aligned_free

#include "core/mm_suffix.h"

#endif//__mm_alloc_h__
