#include "core/mm_errno.h"
#include "core/mm_string.h"
#include "container/mm_holder32.h"

static struct mm_holder_u32_vpt g_sys_errlist;

static void __static_sys_errlist_clear(struct mm_holder_u32_vpt* p)
{
	struct mm_rb_node* n = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	struct mm_string* e = NULL;
	n = mm_rb_first(&g_sys_errlist.rbt);
	while(n)
	{
		it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n, struct mm_holder_u32_vpt_iterator, n);
		n = mm_rb_next(n);
		e = (struct mm_string*)(it->v);
		mm_string_destroy(e);
		mm_free(e);
	}
}

static mm_uchar_t* __static_strerror(mm_err_t err, mm_uchar_t* errstr, size_t size)
{
    mm_uint_t          len = 0;
    static mm_ulong_t  lang = MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US);

    if (size == 0) 
    {
        return errstr;
    }

    len = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, err, lang, (char *) errstr, size, NULL);

    if (len == 0 && lang && GetLastError() == ERROR_RESOURCE_LANG_NOT_FOUND) 
    {

        /*
         * Try to use English messages first and fallback to a language,
         * based on locale: non-English Windows have no English messages
         * at all.  This way allows to use English messages at least on
         * Windows with MUI.
         */

        lang = 0;

        len = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, err, lang, (char *) errstr, size, NULL);
    }

    if (len == 0) 
    {
        mm_snprintf((char*)errstr, size - 1,"FormatMessage() error:(%d)", GetLastError());
		return errstr;
    }

    /* remove ".\r\n\0" */
    while (errstr[len] == '\0' || errstr[len] == CR || errstr[len] == LF || errstr[len] == '.')
    {
		errstr[len] = '\0';
        --len;
    }

    return &errstr[++len];
}

MM_EXPORT_DLL mm_uchar_t* mm_strerror(mm_err_t err, mm_uchar_t* errstr, size_t size)
{
	struct mm_string* err_string = (struct mm_string*)mm_holder_u32_vpt_get(&g_sys_errlist,err);
	if ( NULL == err_string )
	{
		err_string = (struct mm_string*)mm_malloc(sizeof(struct mm_string));
		mm_string_init(err_string);
		__static_strerror(err,errstr,size);
		mm_string_assigns(err_string,(char*)errstr);
		mm_holder_u32_vpt_set(&g_sys_errlist,err,err_string);
	}
	else
	{
		mm_memcpy(errstr,err_string->s,err_string->l);
	}
	return errstr;
}


MM_EXPORT_DLL mm_int_t mm_strerror_init(void)
{
	struct mm_holder_u32_vpt_alloc holder32_alloc;

	mm_holder_u32_vpt_init(&g_sys_errlist);

	holder32_alloc.alloc = &mm_holder_u32_vpt_weak_alloc;
	holder32_alloc.relax = &mm_holder_u32_vpt_weak_relax;
	holder32_alloc.obj = NULL;
	mm_holder_u32_vpt_assign_alloc(&g_sys_errlist,&holder32_alloc);

    return MM_OK;
}
MM_EXPORT_DLL mm_int_t mm_strerror_destroy(void)
{
	// clear first.
	__static_sys_errlist_clear(&g_sys_errlist);
	//
	mm_holder_u32_vpt_destroy(&g_sys_errlist);
    return MM_OK;
}
