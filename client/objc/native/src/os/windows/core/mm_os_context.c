#include "core/mm_os_context.h"
#include "core/mm_alloc.h"
#include "core/mm_socket.h"
#include "core/mm_logger.h"
#include "core/mm_errno.h"
#include "core/mm_cpuinfo.h"

static void* __static_os_context_thread(void* _arg);
static int __static_win_startup_socket();
static int __static_win_cleanup_socket();

static int is_socket_startup = 0;
static int __static_win_startup_socket()
{
	WORD wVersionRequested;
	WSADATA wsaData;
	int err; 
	struct mm_logger* g_logger = mm_logger_instance();

	if (is_socket_startup == 1)
	{
		return 0;
	}

	wVersionRequested = MAKEWORD(1,1);

	err = WSAStartup(wVersionRequested, &wsaData);
	if( err != 0)
	{
		mm_err_t errcode = mm_errno;
		mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
		mm_logger_log_E(g_logger,"__static_win_startup_socket %d can not start socket.",__LINE__);
		return -1;
	}

	if (LOBYTE(wsaData.wVersion) != 1 ||
		HIBYTE(wsaData.wVersion) != 1)
	{
		mm_err_t errcode = mm_errno;
		WSACleanup();
		mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
		mm_logger_log_E(g_logger,"__static_win_startup_socket %d socket protocol version error.",__LINE__);
		return -2;
	}
	mm_logger_log_I(g_logger,"__static_win_startup_socket %d socket startup succeed.",__LINE__);
	is_socket_startup = 1;
	return 0;
}

static int __static_win_cleanup_socket()
{
	struct mm_logger* g_logger = mm_logger_instance();
	if (is_socket_startup == 0)
	{
		return 0;
	}
	WSACleanup();
	mm_logger_log_I(g_logger,"__static_win_cleanup_socket %d socket cleanup succeed.",__LINE__);
	is_socket_startup = 0;
	return 0;
}
static void mm_os_context_perform_cacheline_size(struct mm_os_context* p)
{
    if (mm_strcmp(p->cpuinfo.vendor.s, "GenuineIntel") == 0) 
    {
    	uint32_t cpu_eax = p->cpuinfo.level >> 32;
    	uint32_t model;
        switch ((cpu_eax & 0xf00) >> 8) 
        {
        /* Pentium */
        case 5:
            p->cacheline_size = 32;
            break;

        /* Pentium Pro, II, III */
        case 6:
            p->cacheline_size = 32;

            model = ((cpu_eax & 0xf0000) >> 8) | (cpu_eax & 0xf0);

            if (model >= 0xd0) 
            {
                /* Intel Core, Core 2, Atom */
                p->cacheline_size = 64;
            }
            break;

        /*
         * Pentium 4, although its cache line size is 64 bytes,
         * it prefetches up to two cache lines during memory read
         */
        case 15:
            p->cacheline_size = 128;
            break;
        }

    }
    else if (mm_strcmp(p->cpuinfo.vendor.s, "AuthenticAMD") == 0) 
    {
        p->cacheline_size = 64;
    }
}

static struct mm_os_context g_os_context;
MM_EXPORT_DLL struct mm_os_context* mm_os_context_instance()
{
	return &g_os_context;
}
MM_EXPORT_DLL void mm_os_context_init(struct mm_os_context* p)
{
	mm_string_init(&p->name);
	mm_cpuinfo_init(&p->cpuinfo);
	p->cacheline_size = 64;
	p->cores = 1;
	p->pagesize = 4096;
	p->pagesize_shift = 12;
	//
	mm_strerror_init();
	mm_string_assigns(&p->name,"windows");
	__static_win_startup_socket();
}
MM_EXPORT_DLL void mm_os_context_destroy(struct mm_os_context* p)
{
	__static_win_cleanup_socket();
	mm_string_destroy(&p->name);
	mm_strerror_destroy();
	mm_cpuinfo_destroy(&p->cpuinfo);
}
//
MM_EXPORT_DLL void mm_os_context_perform(struct mm_os_context* p)
{
	mm_uint_t n;
	SYSTEM_INFO  si;
    struct mm_logger* g_logger = mm_logger_instance();
	GetSystemInfo(&si);
	p->pagesize = si.dwPageSize;
	p->cores = si.dwNumberOfProcessors;
	p->cacheline_size = MM_CPU_CACHE_LINE;
	mm_cpuinfo_perform(&p->cpuinfo);
	for (n = p->pagesize; n >>= 1; p->pagesize_shift++) { /* void */ }
	mm_os_context_perform_cacheline_size(p);
	//
    mm_logger_log_I(g_logger,"name           :%s",p->name.s);
    mm_logger_log_I(g_logger,"cpuinfo.vendor :%s",p->cpuinfo.vendor.s);
    mm_logger_log_I(g_logger,"cpuinfo.brand  :%s",p->cpuinfo.brand.s);
    mm_logger_log_I(g_logger,"cpuinfo.level  :%" PRIu64,p->cpuinfo.level);
    mm_logger_log_I(g_logger,"cpuinfo.flags  :%" PRIu64,p->cpuinfo.flags);
    mm_logger_log_I(g_logger,"cacheline_size :%u",p->cacheline_size);
    mm_logger_log_I(g_logger,"cores          :%u",p->cores);
    mm_logger_log_I(g_logger,"pagesize       :%u",p->pagesize);
    mm_logger_log_I(g_logger,"pagesize_shift :%u",p->pagesize_shift);
}
MM_EXPORT_DLL void mm_os_context_logger_console(const char* section,const char* time_stamp_string, int lvl,const char* message)
{
	// [1992/01/26 09:13:14-520 8 V ]
	const struct mm_level_mark* mark = mm_logger_level_mark(lvl);
	mm_printf("%s %d %s %s %s\n",time_stamp_string,lvl,mark->m.s,section,message);
}