#ifndef __mm_socket_h__
#define __mm_socket_h__

#include "core/mm_prefix.h"

#include "core/mm_config.h"

#define MM_READ_SHUTDOWN SD_RECEIVE
#define MM_SEND_SHUTDOWN SD_SEND
#define MM_BOTH_SHUTDOWN SD_BOTH

typedef SOCKET  mm_socket_t;
typedef int     socklen_t;


#define mm_socket(af, type, proto)                                          \
    WSASocket(af, type, proto, NULL, 0, WSA_FLAG_OVERLAPPED)

#define mm_socket_n        "WSASocket()"

MM_EXPORT_DLL int mm_nonblocking(mm_socket_t s);
MM_EXPORT_DLL int mm_blocking(mm_socket_t s);

#define mm_nonblocking_n   "ioctlsocket(FIONBIO)"
#define mm_blocking_n      "ioctlsocket(!FIONBIO)"

#define mm_shutdown_socket    shutdown
#define mm_shutdown_socket_n  "shutdown()"

#define mm_close_socket    closesocket
#define mm_close_socket_n  "closesocket()"


#ifndef WSAID_ACCEPTEX

typedef BOOL (PASCAL FAR * LPFN_ACCEPTEX)(
    IN SOCKET sListenSocket,
    IN SOCKET sAcceptSocket,
    IN PVOID lpOutputBuffer,
    IN DWORD dwReceiveDataLength,
    IN DWORD dwLocalAddressLength,
    IN DWORD dwRemoteAddressLength,
    OUT LPDWORD lpdwBytesReceived,
    IN LPOVERLAPPED lpOverlapped
    );

#define WSAID_ACCEPTEX                                                       \
    {0xb5367df1,0xcbac,0x11cf,{0x95,0xca,0x00,0x80,0x5f,0x48,0xa1,0x92}}

#endif


#ifndef WSAID_GETACCEPTEXSOCKADDRS

typedef VOID (PASCAL FAR * LPFN_GETACCEPTEXSOCKADDRS)(
    IN PVOID lpOutputBuffer,
    IN DWORD dwReceiveDataLength,
    IN DWORD dwLocalAddressLength,
    IN DWORD dwRemoteAddressLength,
    OUT struct sockaddr **LocalSockaddr,
    OUT LPINT LocalSockaddrLength,
    OUT struct sockaddr **RemoteSockaddr,
    OUT LPINT RemoteSockaddrLength
    );

#define WSAID_GETACCEPTEXSOCKADDRS                                           \
        {0xb5367df2,0xcbac,0x11cf,{0x95,0xca,0x00,0x80,0x5f,0x48,0xa1,0x92}}

#endif


#ifndef WSAID_TRANSMITFILE

#ifndef TF_DISCONNECT

#define TF_DISCONNECT           1
#define TF_REUSE_SOCKET         2
#define TF_WRITE_BEHIND         4
#define TF_USE_DEFAULT_WORKER   0
#define TF_USE_SYSTEM_THREAD    16
#define TF_USE_KERNEL_APC       32

typedef struct _TRANSMIT_FILE_BUFFERS {
    LPVOID Head;
    DWORD HeadLength;
    LPVOID Tail;
    DWORD TailLength;
} TRANSMIT_FILE_BUFFERS, *PTRANSMIT_FILE_BUFFERS, FAR *LPTRANSMIT_FILE_BUFFERS;

#endif

typedef BOOL (PASCAL FAR * LPFN_TRANSMITFILE)(
    IN SOCKET hSocket,
    IN HANDLE hFile,
    IN DWORD nNumberOfBytesToWrite,
    IN DWORD nNumberOfBytesPerSend,
    IN LPOVERLAPPED lpOverlapped,
    IN LPTRANSMIT_FILE_BUFFERS lpTransmitBuffers,
    IN DWORD dwReserved
    );

#define WSAID_TRANSMITFILE                                                   \
    {0xb5367df0,0xcbac,0x11cf,{0x95,0xca,0x00,0x80,0x5f,0x48,0xa1,0x92}}

#endif


#ifndef WSAID_TRANSMITPACKETS

/* OpenWatcom has a swapped TP_ELEMENT_FILE and TP_ELEMENT_MEMORY definition */

#ifndef TP_ELEMENT_FILE

#ifdef _MSC_VER
#pragma warning(disable:4201) /* Nonstandard extension, nameless struct/union */
#endif

typedef struct _TRANSMIT_PACKETS_ELEMENT {
    ULONG dwElFlags;
#define TP_ELEMENT_MEMORY   1
#define TP_ELEMENT_FILE     2
#define TP_ELEMENT_EOP      4
    ULONG cLength;
    union {
        struct {
            LARGE_INTEGER nFileOffset;
            HANDLE        hFile;
        };
        PVOID             pBuffer;
    };
} TRANSMIT_PACKETS_ELEMENT, *PTRANSMIT_PACKETS_ELEMENT,
    FAR *LPTRANSMIT_PACKETS_ELEMENT;

#ifdef _MSC_VER
#pragma warning(default:4201)
#endif

#endif

typedef BOOL (PASCAL FAR * LPFN_TRANSMITPACKETS) (
    SOCKET hSocket,
    TRANSMIT_PACKETS_ELEMENT *lpPacketArray,
    DWORD nElementCount,
    DWORD nSendSize,
    LPOVERLAPPED lpOverlapped,
    DWORD dwFlags
    );

#define WSAID_TRANSMITPACKETS                                                \
    {0xd9689da0,0x1f90,0x11d3,{0x99,0x71,0x00,0xc0,0x4f,0x68,0xc8,0x76}}

#endif


#ifndef WSAID_CONNECTEX

typedef BOOL (PASCAL FAR * LPFN_CONNECTEX) (
    IN SOCKET s,
    IN const struct sockaddr FAR *name,
    IN int namelen,
    IN PVOID lpSendBuffer OPTIONAL,
    IN DWORD dwSendDataLength,
    OUT LPDWORD lpdwBytesSent,
    IN LPOVERLAPPED lpOverlapped
    );

#define WSAID_CONNECTEX \
    {0x25a207b9,0xddf3,0x4660,{0x8e,0xe9,0x76,0xe5,0x8c,0x74,0x06,0x3e}}

#endif


#ifndef WSAID_DISCONNECTEX

typedef BOOL (PASCAL FAR * LPFN_DISCONNECTEX) (
    IN SOCKET s,
    IN LPOVERLAPPED lpOverlapped,
    IN DWORD  dwFlags,
    IN DWORD  dwReserved
    );

#define WSAID_DISCONNECTEX                                                   \
    {0x7fda2e11,0x8630,0x436f,{0xa0,0x31,0xf5,0x36,0xa6,0xee,0xc1,0x57}}

#endif


extern LPFN_ACCEPTEX              mm_acceptex;
extern LPFN_GETACCEPTEXSOCKADDRS  mm_getacceptexsockaddrs;
extern LPFN_TRANSMITFILE          mm_transmitfile;
extern LPFN_TRANSMITPACKETS       mm_transmitpackets;
extern LPFN_CONNECTEX             mm_connectex;
extern LPFN_DISCONNECTEX          mm_disconnectex;


MM_EXPORT_DLL int mm_tcp_push(mm_socket_t s);
#define mm_tcp_push_n            "tcp_push()"

// only open the flag.
// 1 true 0 false.
// 0(MM_OK) success -1(MM_ERROR) failure.
MM_EXPORT_DLL int mm_socket_keepalive(mm_socket_t s,int flag);

// tcp_keepidle  firt keepalive trigger    ms.
// tcp_keepintvl keepalive packet interval ms.
// tcp_keepcnt   try times                 n.
// 0(MM_OK) success -1(MM_ERROR) failure.
MM_EXPORT_DLL int mm_socket_keepalive_ctl(mm_socket_t s,int tcp_keepidle,int tcp_keepintvl,int tcp_keepcnt);

#define MM_INVALID_SOCKET INVALID_SOCKET

#define MM_NONBLOCK 0
#define MM_BLOCKING 1

#define MM_AF_INET4 AF_INET
#define MM_AF_INET6 AF_INET6

// not keepalive.
#define MM_KEEPALIVE_INACTIVE 0
// mod keepalive.
#define MM_KEEPALIVE_ACTIVATE 1

#include "core/mm_suffix.h"

#endif//__mm_socket_h__
