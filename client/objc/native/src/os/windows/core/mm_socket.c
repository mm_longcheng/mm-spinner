#include "core/mm_socket.h"
#include "core/mm_logger.h"
#include <Mstcpip.h>

MM_EXPORT_DLL int
mm_nonblocking(mm_socket_t s)
{
    unsigned long  nb = 1;

    return ioctlsocket(s, FIONBIO, &nb);
}


MM_EXPORT_DLL int
mm_blocking(mm_socket_t s)
{
    unsigned long  nb = 0;

    return ioctlsocket(s, FIONBIO, &nb);
}


MM_EXPORT_DLL int
mm_tcp_push(mm_socket_t s)
{
    return 0;
}

MM_EXPORT_DLL int mm_socket_keepalive(mm_socket_t s,int flag)
{
	// at windows BOOL int 0 FALSE 1 TRUE same for our define.
	if ( SOCKET_ERROR == setsockopt(s, SOL_SOCKET, SO_KEEPALIVE, (char*)&flag, sizeof(flag)) )
	{
		mm_err_t errcode = mm_errno;
		struct mm_logger* g_logger = mm_logger_instance();
		mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
		mm_logger_log_E(g_logger,"%s %d can not keepalive fd:%d",__FUNCTION__,__LINE__,s);
		return MM_ERROR;
	}
	return MM_OK;
}

MM_EXPORT_DLL int mm_socket_keepalive_ctl(mm_socket_t s,int tcp_keepidle,int tcp_keepintvl,int tcp_keepcnt)
{
	// set KeepAlive parameter  
	struct tcp_keepalive alive_in;  
	struct tcp_keepalive alive_out;  
	unsigned long _return = 0;  
	alive_in.keepalivetime      = tcp_keepidle;
	alive_in.keepaliveinterval  = tcp_keepintvl;
	alive_in.onoff              = TRUE;  

	if ( SOCKET_ERROR == WSAIoctl(s, SIO_KEEPALIVE_VALS, &alive_in, sizeof(alive_in),  
		&alive_out, sizeof(alive_out), &_return, NULL, NULL) )
	{  
		mm_err_t errcode = mm_errno;
		struct mm_logger* g_logger = mm_logger_instance();
		mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
		mm_logger_log_E(g_logger,"%s %d can not keepalive ctl fd:%d",__FUNCTION__,__LINE__,s);
		return MM_ERROR;  
	}
	return MM_OK;
}