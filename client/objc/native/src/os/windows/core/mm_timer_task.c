#include "mm_timer_task.h"
#include "core/mm_errno.h"
#include "core/mm_logger.h"
//////////////////////////////////////////////////////////////////////////
// timer task impl select version.
struct mm_timer_task_select
{
	mm_socket_t fd;// timer select fd.
};
MM_EXPORT_DLL void mm_timer_task_select_init(struct mm_timer_task_select* p)
{
	p->fd = -1;
}
MM_EXPORT_DLL void mm_timer_task_select_destroy(struct mm_timer_task_select* p)
{
	if ( -1 != p->fd )
	{
		mm_shutdown_socket(p->fd,MM_BOTH_SHUTDOWN);
		mm_close_socket(p->fd);
	}
	p->fd = -1;
}
//////////////////////////////////////////////////////////////////////////
static void* __static_timer_task_handle_thread(void* _arg);
//////////////////////////////////////////////////////////////////////////
static void __static_timer_task_handle(struct mm_timer_task* p)
{

}
MM_EXPORT_DLL void mm_timer_task_callback_init(struct mm_timer_task_callback* p)
{
	p->handle = &__static_timer_task_handle;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_timer_task_callback_destroy(struct mm_timer_task_callback* p)
{
	p->handle = &__static_timer_task_handle;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_timer_task_init(struct mm_timer_task* p)
{
	p->impl = mm_malloc(sizeof(struct mm_timer_task_select));// impl for time wait.strong ref.
	mm_timer_task_select_init((struct mm_timer_task_select*)p->impl);
	mm_timer_task_callback_init(&p->callback);
	p->nearby_time = MM_TIME_TASK_NEARBY_MSEC;// nearby_time milliseconds for each time wait.default is 0 ms.
	p->msec_delay = 0;// delay milliseconds before first callback.default is 0 ms.
	p->state = ts_closed;// mm_thread_state_t,default is ts_closed(0)
}
MM_EXPORT_DLL void mm_timer_task_destroy(struct mm_timer_task* p)
{
	mm_timer_task_select_destroy((struct mm_timer_task_select*)p->impl);
	mm_free((struct mm_timer_task_select*)p->impl);
	p->impl = NULL;// impl for time wait.strong ref.
	mm_timer_task_callback_destroy(&p->callback);
	p->nearby_time = 0;// nearby_time milliseconds for each time wait.default is 0 ms.
	p->msec_delay = 0;// delay milliseconds before first callback.default is 0 ms.
	p->state = ts_closed;// mm_thread_state_t,default is ts_closed(0)
}
//////////////////////////////////////////////////////////////////////////
// do not assign when holder have some elem.
MM_EXPORT_DLL void mm_timer_task_assign_callback(struct mm_timer_task* p,struct mm_timer_task_callback* callback)
{
	assert(NULL != callback && "you can not assign null callback.");
	p->callback = *callback;
}
MM_EXPORT_DLL void mm_timer_task_assign_nearby_time(struct mm_timer_task* p,mm_msec_t nearby_time)
{
	p->nearby_time = nearby_time;
}
//////////////////////////////////////////////////////////////////////////
// sync.
MM_EXPORT_DLL void mm_timer_task_handle(struct mm_timer_task* p)
{
	struct mm_timer_task_select* _impl = (struct mm_timer_task_select*)p->impl;
	//////////////////////////////////////////////////////////////////////////
	struct timeval otime;
	int rt = 0;
	int _a, _b;
	fd_set impl_r_fd_set;
	fd_set impl_w_fd_set;
	fd_set impl_e_fd_set;
	assert(p->callback.handle&&"p->callback.handle is a null.");
	FD_ZERO(&impl_r_fd_set);
	FD_ZERO(&impl_w_fd_set);
	FD_ZERO(&impl_e_fd_set);
	FD_SET(_impl->fd, &impl_r_fd_set);
	//FD_SET(p->fd, &impl_w_fd_set);
	FD_SET(_impl->fd, &impl_e_fd_set);
	if ( 0 < p->msec_delay )
	{
		mm_msleep(p->msec_delay);
	}
	while( ts_motion == p->state )
	{
		if ( 0 < p->nearby_time )
		{
			// FD_SET is slow,we just copy it.
			fd_set work_r_fd_set = impl_r_fd_set;
			fd_set work_w_fd_set = impl_w_fd_set;
			fd_set work_e_fd_set = impl_e_fd_set;

			_a = p->nearby_time / 1000;
			_b = p->nearby_time % 1000;

			otime.tv_sec  = (long) (_a);
			otime.tv_usec = (long) (_b * 1000);

			rt = select(_impl->fd + 1, &work_r_fd_set, &work_w_fd_set, &work_e_fd_set, &otime);
		}
		if (-1 == rt)
		{
			struct mm_logger* g_logger = mm_logger_instance();
			mm_err_t errcode = mm_errno;
			mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
		}
		if (0 == p->nearby_time || 0 <= rt )
		{
			(*(p->callback.handle))(p);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
// start thread.
MM_EXPORT_DLL void mm_timer_task_start(struct mm_timer_task* p)
{
	struct mm_timer_task_select* _impl = (struct mm_timer_task_select*)p->impl;
	//////////////////////////////////////////////////////////////////////////
	_impl->fd = mm_socket(AF_INET, SOCK_DGRAM, 0);
	// no blocking the control fd.
	mm_nonblocking(_impl->fd);
	//////////////////////////////////////////////////////////////////////////
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	pthread_create(&p->task_thread, NULL, &__static_timer_task_handle_thread, p);
}
// interrupt thread.
MM_EXPORT_DLL void mm_timer_task_interrupt(struct mm_timer_task* p)
{
	struct mm_timer_task_select* _impl = (struct mm_timer_task_select*)p->impl;
	if (NULL != _impl)
	{
		//////////////////////////////////////////////////////////////////////////
		p->state = ts_closed;
		//////////////////////////////////////////////////////////////////////////
		mm_shutdown_socket(_impl->fd,MM_BOTH_SHUTDOWN);
		mm_close_socket(_impl->fd);
	}
}
// shutdown thread.
MM_EXPORT_DLL void mm_timer_task_shutdown(struct mm_timer_task* p)
{
	struct mm_timer_task_select* _impl = (struct mm_timer_task_select*)p->impl;
	if (NULL != _impl)
	{
		//////////////////////////////////////////////////////////////////////////
		p->state = ts_finish;
		//////////////////////////////////////////////////////////////////////////
		mm_shutdown_socket(_impl->fd,MM_BOTH_SHUTDOWN);
		mm_close_socket(_impl->fd);
	}
}
// join thread.
MM_EXPORT_DLL void mm_timer_task_join(struct mm_timer_task* p)
{
	pthread_join(p->task_thread, NULL);
}
MM_EXPORT_DLL const char* mm_timer_task_impl_version( struct mm_timer_task* p )
{
	return "mm_timer_task_select";
}
//////////////////////////////////////////////////////////////////////////
static void* __static_timer_task_handle_thread(void* _arg)
{
	struct mm_timer_task* p = (struct mm_timer_task*)(_arg);
	mm_timer_task_handle(p);
	return NULL;
}

