#include "mm_runtime_stat.h"
#include "core/mm_logger.h"
#include "Psapi.h"
///////////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_runtime_loadavg_init(struct mm_runtime_loadavg* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_loadavg));
}
MM_EXPORT_DLL void mm_runtime_loadavg_destroy(struct mm_runtime_loadavg* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_loadavg));
}
MM_EXPORT_DLL void mm_runtime_loadavg_perform(struct mm_runtime_loadavg* p)
{

}
///////////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_runtime_machine_init(struct mm_runtime_machine* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_machine));
}
MM_EXPORT_DLL void mm_runtime_machine_destroy(struct mm_runtime_machine* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_machine));
}
MM_EXPORT_DLL void mm_runtime_machine_perform(struct mm_runtime_machine* p)
{
	mm_runtime_machine_perform_cpu(p);
	mm_runtime_machine_perform_mem(p);
}
MM_EXPORT_DLL void mm_runtime_machine_perform_cpu(struct mm_runtime_machine* p)
{
	FILETIME idleTime,kernelTime,userTime;
	LARGE_INTEGER lg_idleTime,lg_kernelTime,lg_userTime;

	SYSTEM_INFO si;

	GetSystemTimes( &idleTime, &kernelTime, &userTime );
	lg_idleTime.HighPart = idleTime.dwHighDateTime;
	lg_idleTime.LowPart = idleTime.dwLowDateTime;

	lg_kernelTime.HighPart = kernelTime.dwHighDateTime;
	lg_kernelTime.LowPart = kernelTime.dwLowDateTime;

	lg_userTime.HighPart = userTime.dwHighDateTime;
	lg_userTime.LowPart = userTime.dwLowDateTime;

	p->cpu_idle = lg_idleTime.QuadPart;
	p->cpu_system = lg_kernelTime.QuadPart;
	p->cpu_user = lg_userTime.QuadPart;

	// get cpu cores.
	GetSystemInfo(&si);
	p->cores = si.dwNumberOfProcessors;
}
MM_EXPORT_DLL void mm_runtime_machine_perform_mem(struct mm_runtime_machine* p)
{
	MEMORYSTATUSEX statex;
	statex.dwLength = sizeof(statex);
	GlobalMemoryStatusEx (&statex);
	p->mem_total = statex.ullTotalPhys;
	p->mem_free = statex.ullAvailPhys;
	p->mem_buff = p->mem_total - p->mem_free;
	p->mem_cache = 0;
}
///////////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_runtime_process_init(struct mm_runtime_process* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_process));
}
MM_EXPORT_DLL void mm_runtime_process_destroy(struct mm_runtime_process* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_process));
}
MM_EXPORT_DLL void mm_runtime_process_perform(struct mm_runtime_process* p)
{
	mm_runtime_process_perform_pid(p);
	mm_runtime_process_perform_cpu(p);
	mm_runtime_process_perform_mem(p);
	mm_runtime_process_perform_io(p);
}
MM_EXPORT_DLL void mm_runtime_process_perform_pid(struct mm_runtime_process* p)
{
	p->pid = GetCurrentProcessId();
}
MM_EXPORT_DLL void mm_runtime_process_perform_cpu(struct mm_runtime_process* p)
{
	FILETIME CreateTime, ExitTime, KernelTime,UserTime;
	LARGE_INTEGER lgKernelTime,lgUserTime;

	HANDLE hd = OpenProcess(PROCESS_QUERY_INFORMATION,FALSE,(DWORD)p->pid);
	GetProcessTimes(hd, &CreateTime, &ExitTime, &KernelTime, &UserTime);

	lgKernelTime.HighPart = KernelTime.dwHighDateTime;
	lgKernelTime.LowPart = KernelTime.dwLowDateTime;

	lgUserTime.HighPart = UserTime.dwHighDateTime;
	lgUserTime.LowPart = UserTime.dwLowDateTime;

	p->cpu_utime = lgUserTime.QuadPart;
	p->cpu_stime = lgKernelTime.QuadPart;
	p->cpu_cutime = 0;
	p->cpu_cstime = 0;
}
MM_EXPORT_DLL void mm_runtime_process_perform_mem(struct mm_runtime_process* p)
{
	HANDLE hd = OpenProcess(PROCESS_QUERY_INFORMATION,FALSE,(DWORD)p->pid);
	PROCESS_MEMORY_COUNTERS pmc;
	GetProcessMemoryInfo( hd, &pmc, sizeof(PROCESS_MEMORY_COUNTERS));
	p->vm_rss = pmc.WorkingSetSize;
}
MM_EXPORT_DLL void mm_runtime_process_perform_io(struct mm_runtime_process* p)
{
	IO_COUNTERS info;
	HANDLE hd = OpenProcess(PROCESS_QUERY_INFORMATION,FALSE,(DWORD)p->pid);
	GetProcessIoCounters(hd,&info);
	p->rchar = info.ReadTransferCount;
	p->wchar = info.WriteTransferCount;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_runtime_stat_init( struct mm_runtime_stat* p )
{
	p->pid = 0;
	p->cores = 0;
	p->vm_rss = 0;
	p->rchar = 0;
	p->wchar = 0;
	p->cpu_sys = 0;
	p->cpu_pro = 0;
	p->mem_pro = 0;
	p->timecode_o = 0;
	p->timecode_n = 0;
	p->sampling = 0;
	mm_memset(&p->tv_o,0,sizeof(struct timeval));
	mm_memset(&p->tv_n,0,sizeof(struct timeval));
	mm_runtime_loadavg_init(&p->loadavg_o);
	mm_runtime_loadavg_init(&p->loadavg_n);
	mm_runtime_machine_init(&p->machine_o);
	mm_runtime_machine_init(&p->machine_n);
	mm_runtime_process_init(&p->process_o);
	mm_runtime_process_init(&p->process_n);
}

MM_EXPORT_DLL void mm_runtime_stat_destroy( struct mm_runtime_stat* p )
{
	p->pid = 0;
	p->cores = 0;
	p->vm_rss = 0;
	p->rchar = 0;
	p->wchar = 0;
	p->cpu_sys = 0;
	p->cpu_pro = 0;
	p->mem_pro = 0;
	p->timecode_o = 0;
	p->timecode_n = 0;
	p->sampling = 0;
	mm_memset(&p->tv_o,0,sizeof(struct timeval));
	mm_memset(&p->tv_n,0,sizeof(struct timeval));
	mm_runtime_loadavg_destroy(&p->loadavg_o);
	mm_runtime_loadavg_destroy(&p->loadavg_n);
	mm_runtime_machine_destroy(&p->machine_o);
	mm_runtime_machine_destroy(&p->machine_n);
	mm_runtime_process_destroy(&p->process_o);
	mm_runtime_process_destroy(&p->process_n);
}

MM_EXPORT_DLL void mm_runtime_stat_update( struct mm_runtime_stat* p )
{
	mm_uint64_t b_utime,b_stime,b_cutime,b_cstime;
	mm_uint64_t b_user,b_nice,b_system,b_idle,a,b,c;
	//////////////////////////////////////////////////////////////////////////
	mm_gettimeofday(&p->tv_n,NULL);
	p->timecode_n = p->tv_n.tv_sec * 1000 + (p->tv_n.tv_usec / 1000);
	p->sampling = p->timecode_n - p->timecode_o;
	//
	mm_runtime_loadavg_perform(&p->loadavg_n);
	mm_runtime_machine_perform(&p->machine_n);
	mm_runtime_process_perform(&p->process_n);
	//////////////////////////////////////////////////////////////////////////
	p->pid = p->process_n.pid;
	p->vm_rss = p->process_n.vm_rss;
	p->rchar = p->process_n.rchar;
	p->wchar = p->process_n.wchar;
	p->cores = p->machine_n.cores;
	//
	b_utime = p->process_n.cpu_utime - p->process_o.cpu_utime;
	b_stime = p->process_n.cpu_stime - p->process_o.cpu_stime;
	b_cutime = p->process_n.cpu_cutime - p->process_o.cpu_cutime;
	b_cstime = p->process_n.cpu_cstime - p->process_o.cpu_cstime;
	a = b_utime + b_stime + b_cutime + b_cstime;

	b_user = p->machine_n.cpu_user - p->machine_o.cpu_user;
	b_nice = p->machine_n.cpu_nice - p->machine_o.cpu_nice;
	b_system = p->machine_n.cpu_system - p->machine_o.cpu_system;
	b_idle = p->machine_n.cpu_idle - p->machine_o.cpu_idle;
	b = b_user + b_nice + b_system + b_idle;
	c = b_user + b_nice + b_system;

	p->cpu_sys = ( 0 == c && 0 == b_idle ) ? 0 : c * 100.0f /( c + b_idle );
	p->cpu_pro = ( 0 == b ) ? 0 : a * 100.0f / b;
	p->mem_pro = p->vm_rss * 100.0f / p->machine_o.mem_total;
	//////////////////////////////////////////////////////////////////////////
	mm_memcpy(&p->loadavg_o,&p->loadavg_n,sizeof(struct mm_runtime_loadavg));
	mm_memcpy(&p->machine_o,&p->machine_n,sizeof(struct mm_runtime_machine));
	mm_memcpy(&p->process_o,&p->process_n,sizeof(struct mm_runtime_process));
	p->timecode_o = p->timecode_n;
}
//////////////////////////////////////////////////////////////////////////