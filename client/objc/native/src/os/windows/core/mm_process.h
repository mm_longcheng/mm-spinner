#ifndef __mm_process_h__
#define __mm_process_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

#define mm_sched_yield()   SwitchToThread()

#include "core/mm_suffix.h"

#endif//__mm_process_h__