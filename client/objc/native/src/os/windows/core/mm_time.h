#ifndef __mm_time_h__
#define __mm_time_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

typedef mm_uint_t      	mm_msec_t;
typedef mm_int_t  		mm_msec_int_t;

typedef mm_sint64_t 	mm_time_t;

/** intervals for I/O timeouts, in microseconds */
typedef mm_sint64_t 	mm_interval_time_t;
/** short interval for I/O timeouts, in microseconds */
typedef mm_sint32_t 	mm_short_interval_time_t;

typedef struct tm             mm_tm_t;

#define mm_tm_sec            tm_sec
#define mm_tm_min            tm_min
#define mm_tm_hour           tm_hour
#define mm_tm_mday           tm_mday
#define mm_tm_mon            tm_mon
#define mm_tm_year           tm_year
#define mm_tm_wday           tm_wday
#define mm_tm_isdst          tm_isdst

#define mm_tm_sec_t          int
#define mm_tm_min_t          int
#define mm_tm_hour_t         int
#define mm_tm_mday_t         int
#define mm_tm_mon_t          int
#define mm_tm_year_t         int
#define mm_tm_wday_t         int
//typedef FILETIME              mm_mtime_t;
#ifndef suseconds_t
#define suseconds_t long
#endif//suseconds_t
// struct timeval
#define mm_timeval_ssec_t    time_t
#define mm_timeval_usec_t    suseconds_t

#define mm_msleep Sleep
#define mm_ssleep(ms)        (void) Sleep((ms) * 1000)

#define MM_HAVE_GETTIMEZONE  1
MM_EXPORT_DLL mm_long_t mm_gettimezone(void);

#define mm_localtime localtime
#define  mm_timezone_update()
MM_EXPORT_DLL void mm_localtime_r(const time_t* s, mm_tm_t *tm);
MM_EXPORT_DLL mm_long_t mm_gettimezone(void);
MM_EXPORT_DLL void mm_libc_localtime(const time_t* s, struct tm *tm);
MM_EXPORT_DLL void mm_libc_gmtime(const time_t* s, struct tm *tm);
MM_EXPORT_DLL int mm_gettimeofday(struct timeval *tp, struct timezone *tz);
MM_EXPORT_DLL void mm_usleep(mm_ulong_t microseconds);
MM_EXPORT_DLL mm_time_t mm_time_now(void);

#define MM_TIMER_INFINITE  (mm_msec_t) -1
#define MM_TIMER_LAZY_DELAY  300

#define MM_MSEC_PER_SEC 1000
#define MM_USEC_PER_SEC 1000000
#define MM_NSEC_PER_SEC 1000000000

/** @return mm_time_t as a second */
#define mm_time_sec(time) ((time) / MM_USEC_PER_SEC)

/** @return mm_time_t as a usec */
#define mm_time_usec(time) ((time) % MM_USEC_PER_SEC)

/** @return mm_time_t as a msec */
#define mm_time_msec(time) (((time) / 1000) % 1000)

/** @return mm_time_t as a msec */
#define mm_time_as_msec(time) ((time) / 1000)

/** @return milliseconds as an mm_time_t */
#define mm_time_from_msec(msec) ((mm_time_t)(msec) * 1000)

/** @return seconds as an mm_time_t */
#define mm_time_from_sec(sec) ((mm_time_t)(sec) * MM_USEC_PER_SEC)

/** @return a second and usec combination as an mm_time_t */
#define mm_time_make(sec, usec) ((mm_time_t)(sec) * MM_USEC_PER_SEC \
                                + (mm_time_t)(usec))

static const mm_uint32_t 	MM_SECONDS_MIN 		= 60;		// 60
static const mm_uint32_t 	MM_SECONDS_HOUR 	= 3600;		// 60 * 60
static const mm_uint32_t 	MM_SECONDS_DAY 		= 86400;	// 24 * 60 * 60;
static const mm_uint32_t 	MM_SECONDS_WEEK 	= 604800;	//  7 * 24 * 60 * 60;
static const mm_uint32_t 	MM_SECONDS_MONTH 	= 2592000;	// 30 * 24 * 60 * 60;

#include "core/mm_suffix.h"

#endif//__mm_time_h__
