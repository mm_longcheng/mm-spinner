#ifndef __mm_auto_config_h__
#define __mm_auto_config_h__

#include "core/mm_prefix.h"

// some platform macro definition.
//
#ifndef MM_CPU_CACHE_LINE
#define MM_CPU_CACHE_LINE  64
#endif

#include "core/mm_suffix.h"

#endif//__mm_auto_config_h__