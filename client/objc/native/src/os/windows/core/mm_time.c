#include "core/mm_time.h"

//MM_EXPORT_DLL int
//mm_gettimeofday(struct timeval *tp, struct timezone *tz)
//{
//	// Note: some broken versions only have 8 trailing zero's, the correct epoch has 9 trailing zero's
//	static const uint64_t EPOCH = ((uint64_t) 116444736000000000ULL);
//
//	SYSTEMTIME  system_time;
//	FILETIME    file_time;
//	uint64_t    time;
//
//	GetSystemTime( &system_time );
//	SystemTimeToFileTime( &system_time, &file_time );
//	time =  ((uint64_t)file_time.dwLowDateTime )      ;
//	time += ((uint64_t)file_time.dwHighDateTime) << 32;
//
//	tp->tv_sec  = (long) ((time - EPOCH) / 10000000L);
//	tp->tv_usec = (long) (system_time.wMilliseconds * 1000);
//	return 0;
//}

//MM_EXPORT_DLL int
//mm_gettimeofday(struct timeval *tp, struct timezone *tz)
//{
//	time_t clock;
//	struct tm tm;
//	SYSTEMTIME wtm;
//	GetLocalTime(&wtm);
//	tm.tm_year      = wtm.wYear - 1900;
//	tm.tm_mon       = wtm.wMonth - 1;
//	tm.tm_mday      = wtm.wDay;
//	tm.tm_hour      = wtm.wHour;
//	tm.tm_min       = wtm.wMinute;
//	tm.tm_sec       = wtm.wSecond;
//	tm. tm_isdst    = -1;
//	clock = mktime(&tm);
//	tp->tv_sec = (long)clock;
//	tp->tv_usec = wtm.wMilliseconds * 1000;
//	return 0;
//}

MM_EXPORT_DLL int
mm_gettimeofday(struct timeval *tp, struct timezone *tz)
{
    uint64_t  intervals;
    FILETIME  ft;

    GetSystemTimeAsFileTime(&ft);

    /*
     * A file time is a 64-bit value that represents the number
     * of 100-nanosecond intervals that have elapsed since
     * January 1, 1601 12:00 A.M. UTC.
     *
     * Between January 1, 1970 (Epoch) and January 1, 1601 there were
     * 134744 days,
     * 11644473600 seconds or
     * 11644473600,000,000,0 100-nanosecond intervals.
     *
     * See also MSKB Q167296.
     */

    intervals = ((uint64_t) ft.dwHighDateTime << 32) | ft.dwLowDateTime;
    intervals -= 116444736000000000;

    tp->tv_sec = (long) (intervals / 10000000);
    tp->tv_usec = (long) ((intervals % 10000000) / 10);
	return 0;
}

MM_EXPORT_DLL void
mm_localtime_r(const time_t* s, mm_tm_t *tm)
{
#if (MM_HAVE_LOCALTIME_R)
	(void) localtime_r(s, tm);

#else
    mm_tm_t  *t;

    t = localtime(s);
    *tm = *t;

#endif

    tm->mm_tm_mon++;
    tm->mm_tm_year += 1900;
}

MM_EXPORT_DLL void
mm_libc_localtime(const time_t* s, struct tm *tm)
{
    struct tm  *t;

    t = localtime(s);
    *tm = *t;
}


MM_EXPORT_DLL void
mm_libc_gmtime(const time_t* s, struct tm *tm)
{
    struct tm  *t;

    t = gmtime(s);
    *tm = *t;
}


MM_EXPORT_DLL mm_long_t
mm_gettimezone(void)
{
    u_long                 n;
    TIME_ZONE_INFORMATION  tz;

    n = GetTimeZoneInformation(&tz);

    switch (n) {

    case TIME_ZONE_ID_UNKNOWN:
        return -tz.Bias;

    case TIME_ZONE_ID_STANDARD:
        return -(tz.Bias + tz.StandardBias);

    case TIME_ZONE_ID_DAYLIGHT:
        return -(tz.Bias + tz.DaylightBias);

    default: /* TIME_ZONE_ID_INVALID */
        return 0;
    }
}

MM_EXPORT_DLL void 
mm_usleep(mm_ulong_t microseconds)
{
	Sleep (microseconds / 1000);
}

/* Return micro-seconds since the Unix epoch (jan. 1, 1970) UTC */
MM_EXPORT_DLL mm_time_t 
mm_time_now(void)
{
    struct timeval tv;
    mm_gettimeofday(&tv, NULL);
    return tv.tv_sec * MM_USEC_PER_SEC + tv.tv_usec;
}