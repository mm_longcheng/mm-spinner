#include "poller/mm_poll.h"
#include "core/mm_alloc.h"
#include "core/mm_string.h"
#include "core/mm_math.h"
#include "core/mm_errno.h"
#include "core/mm_logger.h"

#include "container/mm_holder32.h"

static void* mm_holder32_poll_event_alloc(struct mm_holder_u32_vpt* p,mm_uint32_t k)
{
	struct mm_poll_event* e = (struct mm_poll_event*)mm_malloc(sizeof(struct mm_poll_event));
	// mm_poll_event_init(e);
	return e;
}
static void* mm_holder32_poll_event_relax(struct mm_holder_u32_vpt* p,mm_uint32_t k,void* v)
{
	struct mm_poll_event* e = (struct mm_poll_event*)(v);
	// mm_poll_event_destroy(e);
	mm_free(e);
	return v;
}

struct mm_poll_select
{
	struct mm_holder_u32_vpt holder;
	fd_set sr;// read
	fd_set sw;// write
	mm_socket_t max_fd;
};
static void mm_poll_select_init(struct mm_poll_select* p)
{
	struct mm_holder_u32_vpt_alloc holder32_alloc;

	mm_holder_u32_vpt_init(&p->holder);
	FD_ZERO(&p->sr);
	FD_ZERO(&p->sw);
	p->max_fd = 0;

	holder32_alloc.alloc = &mm_holder32_poll_event_alloc;
	holder32_alloc.relax = &mm_holder32_poll_event_relax;
	holder32_alloc.obj = p;
	mm_holder_u32_vpt_assign_alloc(&p->holder,&holder32_alloc);
}
static void mm_poll_select_destroy(struct mm_poll_select* p)
{
	mm_holder_u32_vpt_destroy(&p->holder);
	FD_ZERO(&p->sr);
	FD_ZERO(&p->sw);
	p->max_fd = 0;
}
static void mm_poll_select_check_update_maxfd(struct mm_poll_select* p,mm_socket_t fd)
{
	if (fd == p->max_fd)
	{
		struct mm_rb_node* n = NULL;
		struct mm_holder_u32_vpt_iterator* it = NULL;
		p->max_fd = 0;
		for (n = mm_rb_first(&p->holder.rbt); n; n = mm_rb_next(n))
		{
			it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n,struct mm_holder_u32_vpt_iterator,n);
			p->max_fd = mm_max(p->max_fd,(mm_socket_t)it->k);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL const mm_char_t* mm_poll_api()
{
	return "select";
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_poll_init(struct mm_poll* p)
{
	struct mm_poll_select* _impl = NULL;
	p->ev_length = 256;
	p->pe = (struct mm_poll_event*)mm_malloc(sizeof(struct mm_poll_event) * p->ev_length);
	_impl = (struct mm_poll_select*)mm_malloc(sizeof(struct mm_poll_select));
	mm_poll_select_init(_impl);
	p->impl = _impl;
}
MM_EXPORT_DLL void mm_poll_destroy(struct mm_poll* p)
{
	struct mm_poll_select* _impl = (struct mm_poll_select*)(p->impl);
	mm_poll_select_destroy(_impl);
	mm_free(p->pe);
	mm_free(_impl);
}
MM_EXPORT_DLL void mm_poll_set_length(struct mm_poll* p, mm_uint32_t length)
{
	if (p->ev_length != length)
	{
		p->ev_length = length;
		p->pe = (struct mm_poll_event*)mm_realloc(p->pe,sizeof(struct mm_poll_event) * p->ev_length);
	}
}
MM_EXPORT_DLL mm_uint32_t mm_poll_get_length(struct mm_poll* p)
{
	return p->ev_length;
}
//int select(int maxfdp,fd_set *readfds,fd_set *writefds,fd_set *errorfds,struct timeval *timeout);
//////////////////////////////////////////////////////////////////////////
/*
Anfd_set is a fixed size buffer. Executing FD_CLR() or FD_SET() with a value of fd that is 
negative or is equal to or larger than FD_SETSIZE will result in undefined behavior. Moreover, 
POSIX requires fd to be a valid file descriptor.

if fd is larger than FD_SETSIZE it will rollback and cover old value.
*/
MM_EXPORT_DLL mm_int_t mm_poll_add_event(struct mm_poll* p, mm_socket_t fd, int mask, void* ud)
{
	struct mm_poll_event* e = NULL;
	struct mm_poll_select* _impl = (struct mm_poll_select*)(p->impl);
	struct mm_holder_u32_vpt_iterator* it = mm_holder_u32_vpt_get_iterator(&_impl->holder,fd);
	assert(_impl->holder.size + 1 < FD_SETSIZE && " fd is larger than FD_SETSIZE it will rollback and cover old value.");
	if (NULL == it)
	{
		// if it is NULL the e must NULL.
		e = (struct mm_poll_event*)mm_holder_u32_vpt_add(&_impl->holder,fd);
	}
	else
	{
		// if it is not NULL the e must not NULL.
		e = (struct mm_poll_event*)(it->v);
	}
	e->s = ud;
	e->mask = 0;
	if (mask & MM_PE_READABLE)
	{
		e->mask |= MM_PE_READABLE;
		FD_SET(fd, &_impl->sr);
	}
	if (mask & MM_PE_WRITABLE)
	{
		e->mask |= MM_PE_WRITABLE;
		FD_SET(fd, &_impl->sw);
	}
	_impl->max_fd = _impl->max_fd > fd ? _impl->max_fd : fd;
	return 0;
}
MM_EXPORT_DLL mm_int_t mm_poll_mod_event(struct mm_poll* p, mm_socket_t fd, int mask, void* ud)
{
	struct mm_poll_event* e = NULL;
	struct mm_poll_select* _impl = (struct mm_poll_select*)(p->impl);
	struct mm_holder_u32_vpt_iterator* it = mm_holder_u32_vpt_get_iterator(&_impl->holder,fd);
	if (NULL != it)
	{
		// if it is not NULL the e must not NULL.
		e = (struct mm_poll_event*)(it->v);
		e->s = ud;
		if (mask & MM_PE_READABLE)
		{
			if (!(e->mask & MM_PE_READABLE)) FD_SET(fd, &_impl->sr);
		}
		else
		{
			if (e->mask & MM_PE_READABLE) FD_CLR(fd, &_impl->sr);
		}
		if (mask & MM_PE_WRITABLE)
		{
			if (!(e->mask & MM_PE_WRITABLE)) FD_SET(fd, &_impl->sw);
		}
		else
		{
			if (e->mask & MM_PE_WRITABLE) FD_SET(fd, &_impl->sw);
		}
		e->mask = mask;
		return 0;
	}
	return -1;
}
MM_EXPORT_DLL mm_int_t mm_poll_del_event(struct mm_poll* p, mm_socket_t fd, int mask, void* ud)
{
	struct mm_poll_event* e = NULL;
	struct mm_poll_select* _impl = (struct mm_poll_select*)(p->impl);
	struct mm_holder_u32_vpt_iterator* it = mm_holder_u32_vpt_get_iterator(&_impl->holder,fd);
	if (NULL != it)
	{
		// if it is not NULL the e must not NULL.
		e = (struct mm_poll_event*)(it->v);
		if ((MM_PE_READABLE | MM_PE_WRITABLE) == mask)
		{
			FD_CLR(fd, &_impl->sw);
			FD_CLR(fd, &_impl->sr);
			mm_holder_u32_vpt_erase(&_impl->holder,it);
			mm_poll_select_check_update_maxfd(_impl,fd);
			return 0;
		}
		else
		{
			if (mask & MM_PE_READABLE)
			{
				e->mask &= ~(1<<MM_PE_READABLE);
				FD_CLR(fd, &_impl->sr);
			}
			if (mask & MM_PE_WRITABLE)
			{
				e->mask &= ~(1<<MM_PE_WRITABLE);
				FD_CLR(fd, &_impl->sw);
			}
			if(!(MM_PE_READABLE & e->mask || MM_PE_WRITABLE & e->mask))
			{
				mm_holder_u32_vpt_erase(&_impl->holder,it);
				mm_poll_select_check_update_maxfd(_impl,fd);
			}
			return 0;
		}
	}
	return -1;
}
MM_EXPORT_DLL mm_int_t mm_poll_wait_event(struct mm_poll* p, mm_msec_t milliseconds)
{
	fd_set work_r_fd_set;
	fd_set work_w_fd_set;
	//fd_set work_e_fd_set;

	struct timeval  tv; 
	struct timeval* tp = NULL;

	int ready = 0;
	mm_uint32_t find_number = 0;
	//
	struct mm_rb_node* n = NULL;
	struct mm_poll_event pe;
	struct mm_poll_event* ue = NULL;
	struct mm_holder_u32_vpt_iterator* it = NULL;
	//
	struct mm_poll_select* _impl = (struct mm_poll_select*)(p->impl);
	// quick checking.
	if ( 0 == _impl->holder.size )
	{
		return 0;
	}

	tp = &tv;

	if ( MM_TIMER_INFINITE == milliseconds )
	{
		// when select fd_set add or remove we have no way to re select,
		// so if timeout is forever.we use do while for simulate.
		// here we just 200ms for timeout.
		tv.tv_sec  = (mm_timeval_ssec_t) 0;// (200 / 1000);
		tv.tv_usec = (mm_timeval_usec_t) 200000;// ((200 % 1000) * 1000);
		do 
		{
			work_r_fd_set = _impl->sr;
			work_w_fd_set = _impl->sw;
			//work_e_fd_set = _fd_read->m_fd_set;

			ready = select(_impl->max_fd + 1, &work_r_fd_set, &work_w_fd_set, NULL, tp);
		} while ( 0 == ready );
	}
	else
	{
		tv.tv_sec  = (mm_timeval_ssec_t) ( milliseconds / 1000);
		tv.tv_usec = (mm_timeval_usec_t) ((milliseconds % 1000) * 1000);

		work_r_fd_set = _impl->sr;
		work_w_fd_set = _impl->sw;
		//work_e_fd_set = _fd_read->m_fd_set;

		// int select(int maxfdp,fd_set *readfds,fd_set *writefds,fd_set *errorfds,struct timeval *timeout);
		//ready = select(_fd_read->m_max_fd + 1, &work_r_fd_set, &work_w_fd_set, &work_e_fd_set, tp);
		ready = select(_impl->max_fd + 1, &work_r_fd_set, &work_w_fd_set, NULL, tp);
	}

	if (ready == 0) 
	{
		if (milliseconds != MM_TIMER_INFINITE)
		{
			return MM_OK;
		}
		return MM_ERROR;
	}
	else if( -1 == ready )
	{
		// not socket .
		mm_err_t _errcode = mm_errno;
		if (_errcode == MM_ECONNABORTED || _errcode == MM_EAGAIN)
		{
			// An operation on a socket could not be performed because the system 
			// lacked sufficient buffer space or because a queue was full.
			return 0;
		}
		else
		{
			// error handler.
			return -1;
		}
		return 0;
	}
	//
	mm_memset(p->pe,0,sizeof(struct mm_poll_event) * p->ev_length);
	for (n = mm_rb_first(&_impl->holder.rbt); n; n = mm_rb_next(n))
	{
		it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n,struct mm_holder_u32_vpt_iterator,n);
		if ( 0 != it->k )
		{
			ue = (struct mm_poll_event*)(it->v);
			pe.s = ue->s;
			pe.mask = 0;
			if (FD_ISSET(it->k, &work_r_fd_set)) pe.mask |= MM_PE_READABLE;
			if (FD_ISSET(it->k, &work_w_fd_set)) pe.mask |= MM_PE_WRITABLE;
			if (MM_PE_NONEABLE != pe.mask)
			{
				p->pe[find_number] = pe;
				find_number ++;
			}
			if (find_number>=p->ev_length)
			{
				break;
			}
		}
	}
	return find_number;
}