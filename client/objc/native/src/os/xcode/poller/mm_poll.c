#include "poller/mm_poll.h"
#include "core/mm_alloc.h"
#include "core/mm_time.h"
#include "core/mm_string.h"
//////////////////////////////////////////////////////////////////////////
struct mm_poll_kqueue
{
	mm_socket_t pfd;
	struct kevent* ev;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL const mm_char_t* mm_poll_api()
{
	return "kqueue";
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_poll_init(struct mm_poll* p)
{
	struct mm_poll_kqueue* _impl = NULL;
	p->ev_length = 256;
	p->pe = (struct mm_poll_event*)mm_malloc(sizeof(struct mm_poll_event) * p->ev_length);
	_impl = (struct mm_poll_kqueue*)mm_malloc(sizeof(struct mm_poll_kqueue));
	_impl->pfd = kqueue();
	_impl->ev = (struct kevent*)mm_malloc(sizeof(struct kevent) * p->ev_length);
	p->impl = _impl;
}
MM_EXPORT_DLL void mm_poll_destroy(struct mm_poll* p)
{
	struct mm_poll_kqueue* _impl = (struct mm_poll_kqueue*)(p->impl);
	mm_close_socket(_impl->pfd);
	mm_free(_impl->ev);
	mm_free(p->pe);
	mm_free(_impl);
}
MM_EXPORT_DLL void mm_poll_set_length(struct mm_poll* p, mm_uint32_t length)
{
	if (p->ev_length != length)
	{
		struct mm_poll_kqueue* _impl = (struct mm_poll_kqueue*)(p->impl);
		p->ev_length = length;
		p->pe = (struct mm_poll_event*)mm_realloc(p->pe,sizeof(struct mm_poll_event) * p->ev_length);
		_impl->ev = (struct kevent*)mm_realloc(_impl->ev,sizeof(struct kevent) * p->ev_length);
	}
}
MM_EXPORT_DLL mm_uint32_t mm_poll_get_length(struct mm_poll* p)
{
	return p->ev_length;
}
MM_EXPORT_DLL mm_int_t mm_poll_add_event(struct mm_poll* p, mm_socket_t fd, int mask, void* ud)
{
	struct mm_poll_kqueue* _impl = (struct mm_poll_kqueue*)(p->impl);
	struct kevent ke;
	if (mask & MM_PE_READABLE)
	{
		EV_SET(&ke, fd, EVFILT_READ , EV_ADD | EV_ENABLE, 0, 0, ud);
		if (kevent(_impl->pfd, &ke, 1, NULL, 0, NULL) == -1) return -1;
	}
	if (mask & MM_PE_WRITABLE)
	{
		EV_SET(&ke, fd, EVFILT_WRITE, EV_ADD | EV_ENABLE, 0, 0, ud);
		if (kevent(_impl->pfd, &ke, 1, NULL, 0, NULL) == -1) return -1;
	}
	return 0;
}
MM_EXPORT_DLL mm_int_t mm_poll_mod_event(struct mm_poll* p, mm_socket_t fd, int mask, void* ud)
{
	struct mm_poll_kqueue* _impl = (struct mm_poll_kqueue*)(p->impl);
	struct kevent ke;
	int ffr = mask & MM_PE_READABLE ? EV_ENABLE : EV_DISABLE;
	int ffw = mask & MM_PE_WRITABLE ? EV_ENABLE : EV_DISABLE;
	//
	EV_SET(&ke, fd, EVFILT_READ , ffr, 0, 0, ud);
	if (kevent(_impl->pfd, &ke, 1, NULL, 0, NULL) == -1) return -1;
	//
	EV_SET(&ke, fd, EVFILT_WRITE, ffw, 0, 0, ud);
	if (kevent(_impl->pfd, &ke, 1, NULL, 0, NULL) == -1) return -1;
	return 0;
}
MM_EXPORT_DLL mm_int_t mm_poll_del_event(struct mm_poll* p, mm_socket_t fd, int mask, void* ud)
{
	struct mm_poll_kqueue* _impl = (struct mm_poll_kqueue*)(p->impl);
	struct kevent ke;
	if (mask & MM_PE_READABLE)
	{
		EV_SET(&ke, fd, EVFILT_READ , EV_DELETE, 0, 0, ud);
		if (kevent(_impl->pfd, &ke, 1, NULL, 0, NULL) == -1) return -1;
	}
	if (mask & MM_PE_WRITABLE)
	{
		EV_SET(&ke, fd, EVFILT_WRITE, EV_DELETE, 0, 0, ud);
		if (kevent(_impl->pfd, &ke, 1, NULL, 0, NULL) == -1) return -1;
	}
	return 0;
}

MM_EXPORT_DLL mm_int_t mm_poll_wait_event(struct mm_poll* p, mm_msec_t milliseconds)
{
	//
	struct timespec  tv;
	struct timespec* tp;
	//
	int n = 0;
	int i;
	//
	struct mm_poll_kqueue* _impl = (struct mm_poll_kqueue*)(p->impl);
	//
	if (milliseconds == MM_TIMER_INFINITE) 
	{
		tp = NULL;
	} 
	else
	{
		tv.tv_sec  = (mm_timeval_ssec_t) ( milliseconds / 1000);
		tv.tv_nsec = (mm_timeval_usec_t) ((milliseconds % 1000) * 1000000);
		tp = &tv;
	}
	n = kevent(_impl->pfd, NULL, 0, _impl->ev, p->ev_length, tp);

	for (i = 0; i < n; i++) 
	{
		int mask = 0;
		struct kevent* ee = &(_impl->ev[i]);

		if (ee->filter & EVFILT_READ)  mask |= MM_PE_READABLE;
		if (ee->filter & EVFILT_WRITE) mask |= MM_PE_WRITABLE;

		p->pe[i].s = ee->udata;
		p->pe[i].mask = mask;
	}
	return n;
}