#ifndef __mm_config_platform_h__
#define __mm_config_platform_h__

#include "core/mm_prefix.h"

#include "core/mm_platform.h"
// sign current platform.
#define MM_DARWIN 1

#include "core/mm_auto_headers.h"

#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <inttypes.h>
#include <stdarg.h>
#include <stddef.h>             /* offsetof() */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <pwd.h>
#include <grp.h>
#include <dirent.h>
#include <glob.h>
#include <sys/mount.h>          /* statfs() */

#include <sys/filio.h>          /* FIONBIO */
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sched.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>        /* TCP_NODELAY */
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/un.h>

#include <sys/sysctl.h>
#include <xlocale.h>


#ifndef IOV_MAX
#define IOV_MAX   64
#endif


#include <core/mm_auto_config.h>


#if (MM_HAVE_POSIX_SEM)
#include <semaphore.h>
#endif


#if (MM_HAVE_POLL)
#include <poll.h>
#endif


#if (MM_HAVE_KQUEUE)
#include <sys/event.h>
#endif


#define MM_LISTEN_BACKLOG  -1


#ifndef MM_HAVE_INHERITED_NONBLOCK
#define MM_HAVE_INHERITED_NONBLOCK  1
#endif


#ifndef MM_HAVE_CASELESS_FILESYSTEM
#define MM_HAVE_CASELESS_FILESYSTEM  1
#endif


#define MM_HAVE_OS_SPECIFIC_INIT    1
#define MM_HAVE_DEBUG_MALLOC        1


extern char **environ;
//////////////////////////////////////////////////////////////////////////
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
//////////////////////////////////////////////////////////////////////////

#include "core/mm_suffix.h"

#endif//__mm_config_platform_h__