#ifndef __mm_runtime_stat_h__
#define __mm_runtime_stat_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_time.h"

#define MM_PROC_MAX_LENGTH 1024

enum MM_PROCESS_STATE
{
	MM_PS_RU = 'R',// R:runnign, 
	MM_PS_SS = 'S',// S:sleeping (TASK_INTERRUPTIBLE), 
	MM_PS_DD = 'D',// D:disk sleep (TASK_UNINTERRUPTIBLE), 
	MM_PS_TS = 'T',// T:stopped, 
	MM_PS_TT = 't',// t:tracing stop,
	MM_PS_ZZ = 'Z',// Z:zombie, 
	MM_PS_XD = 'X',// X:dead
};
struct mm_runtime_loadavg
{
	mm_float32_t lavg_01;
	mm_float32_t lavg_05;
	mm_float32_t lavg_15;
	mm_uint32_t nr_running;
	mm_uint32_t nr_threads;
	mm_uint64_t last_pid;
};
MM_EXPORT_DLL void mm_runtime_loadavg_init(struct mm_runtime_loadavg* p);
MM_EXPORT_DLL void mm_runtime_loadavg_destroy(struct mm_runtime_loadavg* p);
MM_EXPORT_DLL void mm_runtime_loadavg_perform(struct mm_runtime_loadavg* p);
// global.
struct mm_runtime_machine
{
	// cpu
	mm_uint64_t cpu_user;
	mm_uint64_t cpu_nice;
	mm_uint64_t cpu_system;
	mm_uint64_t cpu_idle;
	// mem
	mm_uint64_t mem_total;// kb
	mm_uint64_t mem_free;// kb
	mm_uint64_t mem_buff;// kb
	mm_uint64_t mem_cache;// kb
	//
	mm_uint32_t cores;
};
MM_EXPORT_DLL void mm_runtime_machine_init(struct mm_runtime_machine* p);
MM_EXPORT_DLL void mm_runtime_machine_destroy(struct mm_runtime_machine* p);
MM_EXPORT_DLL void mm_runtime_machine_perform(struct mm_runtime_machine* p);
MM_EXPORT_DLL void mm_runtime_machine_perform_cpu(struct mm_runtime_machine* p);
MM_EXPORT_DLL void mm_runtime_machine_perform_mem(struct mm_runtime_machine* p);

// process
struct mm_runtime_process
{
	// pid
	mm_uint64_t pid;
	// state
	mm_uint8_t state;// MM_PROCESS_STATE
	// cpu
	mm_uint64_t cpu_utime;
	mm_uint64_t cpu_stime;
	mm_uint64_t cpu_cutime;
	mm_uint64_t cpu_cstime;
	// thread
	mm_uint32_t threads;
	// mem
	mm_uint64_t vm_size;
	mm_uint64_t vm_rss;
	mm_uint64_t vm_data;
	mm_uint64_t vm_stk;
	// io
	mm_uint64_t rchar;
	mm_uint64_t wchar;
};
MM_EXPORT_DLL void mm_runtime_process_init(struct mm_runtime_process* p);
MM_EXPORT_DLL void mm_runtime_process_destroy(struct mm_runtime_process* p);
MM_EXPORT_DLL void mm_runtime_process_perform(struct mm_runtime_process* p);
MM_EXPORT_DLL void mm_runtime_process_perform_pid(struct mm_runtime_process* p);
MM_EXPORT_DLL void mm_runtime_process_perform_cpu(struct mm_runtime_process* p);
MM_EXPORT_DLL void mm_runtime_process_perform_mem(struct mm_runtime_process* p);
MM_EXPORT_DLL void mm_runtime_process_perform_io(struct mm_runtime_process* p);
//////////////////////////////////////////////////////////////////////////
struct mm_runtime_stat
{
	mm_uint64_t pid;
	mm_uint32_t cores;
	mm_uint64_t vm_rss;
	mm_uint64_t rchar;
	mm_uint64_t wchar;
	//
	mm_float32_t cpu_sys;
	mm_float32_t cpu_pro;
	mm_float32_t mem_pro;
	//
	mm_uint64_t timecode_o;
	mm_uint64_t timecode_n;
	mm_uint64_t sampling;// sampling time ms
	struct timeval tv_o;
	struct timeval tv_n;
	struct mm_runtime_loadavg loadavg_o;
	struct mm_runtime_loadavg loadavg_n;
	struct mm_runtime_machine machine_o;
	struct mm_runtime_machine machine_n;
	struct mm_runtime_process process_o;
	struct mm_runtime_process process_n;
};
MM_EXPORT_DLL void mm_runtime_stat_init(struct mm_runtime_stat* p);
MM_EXPORT_DLL void mm_runtime_stat_destroy(struct mm_runtime_stat* p);
MM_EXPORT_DLL void mm_runtime_stat_update(struct mm_runtime_stat* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_runtime_stat_h__