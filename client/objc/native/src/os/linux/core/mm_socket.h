#ifndef __mm_socket_h__
#define __mm_socket_h__

#include "core/mm_prefix.h"

#include "core/mm_config.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define MM_READ_SHUTDOWN SHUT_RD
#define MM_SEND_SHUTDOWN SHUT_WR
#define MM_BOTH_SHUTDOWN SHUT_RDWR

typedef int  mm_socket_t;

#define mm_socket          socket
#define mm_socket_n        "socket()"


#if (MM_HAVE_FIONBIO)

MM_EXPORT_DLL int mm_nonblocking(mm_socket_t s);
MM_EXPORT_DLL int mm_blocking(mm_socket_t s);

#define mm_nonblocking_n   "ioctl(FIONBIO)"
#define mm_blocking_n      "ioctl(!FIONBIO)"

#else

#define mm_nonblocking(s)  fcntl(s, F_SETFL, fcntl(s, F_GETFL) | O_NONBLOCK)
#define mm_nonblocking_n   "fcntl(O_NONBLOCK)"

#define mm_blocking(s)     fcntl(s, F_SETFL, fcntl(s, F_GETFL) & ~O_NONBLOCK)
#define mm_blocking_n      "fcntl(!O_NONBLOCK)"

#endif

MM_EXPORT_DLL int mm_tcp_nopush(mm_socket_t s);
MM_EXPORT_DLL int mm_tcp_push(mm_socket_t s);

#if (MM_LINUX)

#define mm_tcp_nopush_n   "setsockopt(TCP_CORK)"
#define mm_tcp_push_n     "setsockopt(!TCP_CORK)"

#else

#define mm_tcp_nopush_n   "setsockopt(TCP_NOPUSH)"
#define mm_tcp_push_n     "setsockopt(!TCP_NOPUSH)"

#endif


#define mm_shutdown_socket    shutdown
#define mm_shutdown_socket_n  "shutdown()"

#define mm_close_socket    close
#define mm_close_socket_n  "close() socket"

// only open the flag.
// 1 true 0 false.
// 0(MM_OK) success -1(MM_ERROR) failure.
MM_EXPORT_DLL int mm_socket_keepalive(mm_socket_t s,int flag);

// tcp_keepidle  firt keepalive trigger    ms.
// tcp_keepintvl keepalive packet interval ms.
// tcp_keepcnt   try times                 n.
// 0(MM_OK) success -1(MM_ERROR) failure.
MM_EXPORT_DLL int mm_socket_keepalive_ctl(mm_socket_t s,int tcp_keepidle,int tcp_keepintvl,int tcp_keepcnt);

#define MM_INVALID_SOCKET (-1)

#define MM_NONBLOCK 0
#define MM_BLOCKING 1

#define MM_AF_INET4 AF_INET
#define MM_AF_INET6 AF_INET6

// not keepalive.
#define MM_KEEPALIVE_INACTIVE 0
// mod keepalive.
#define MM_KEEPALIVE_ACTIVATE 1

#include "core/mm_suffix.h"

#endif//__mm_socket_h__
