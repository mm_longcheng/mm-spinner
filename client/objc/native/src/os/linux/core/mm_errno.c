#include "core/mm_errno.h"
#include "core/mm_string.h"
#include "core/mm_math.h"
#include "core/mm_logger.h"


/*
 * The strerror() messages are copied because:
 *
 * 1) strerror() and strerror_r() functions are not Async-Signal-Safe,
 *    therefore, they cannot be used in signal handlers;
 *
 * 2) a direct sys_errlist[] array may be used instead of these functions,
 *    but Linux linker warns about its usage:
 *
 * warning: `sys_errlist' is deprecated; use `strerror' or `strerror_r' instead
 * warning: `sys_nerr' is deprecated; use `strerror' or `strerror_r' instead
 *
 *    causing false bug reports.
 */


static struct mm_string*  mm_sys_errlist;
static struct mm_string   mm_unknown_error = mm_string_make("Unknown error");


MM_EXPORT_DLL mm_uchar_t*
mm_strerror(mm_err_t err, mm_uchar_t *errstr, size_t size)
{
    struct mm_string* msg;

    msg = ((mm_uint_t) err < MM_SYS_NERR) ? &mm_sys_errlist[err]:
                                              &mm_unknown_error;
    size = mm_min(size, msg->l);

    return mm_cpymem(errstr, msg->s, size);
}

MM_EXPORT_DLL mm_int_t
mm_strerror_init(void)
{
    char* msg;
    size_t len;
    mm_err_t err;

    /*
     * mm_strerror() is not ready to work at this stage, therefore,
     * malloc() is used and possible errors are logged using strerror().
     */

    len = MM_SYS_NERR * sizeof(struct mm_string);

    mm_sys_errlist = (struct mm_string*)mm_malloc(len);
    if ( NULL != mm_sys_errlist ) 
    {
        for (err = 0; err < MM_SYS_NERR; err++) 
        {
            msg = strerror(err);
            len = mm_strlen(msg);
            struct mm_string* e = &mm_sys_errlist[err];
            mm_string_init(e);
            mm_string_assigns(e,msg);
        }
        return MM_OK;
    }
    else
    {
        err = errno;
        mm_log_E("malloc(%u) failed (%d: %s)", (unsigned int)len, err, strerror(err));
    }
    return MM_ERROR;
}
MM_EXPORT_DLL mm_int_t mm_strerror_destroy(void)
{
    if (mm_sys_errlist)
    {
        mm_err_t err;
        for (err = 0; err < MM_SYS_NERR; err++) 
        {
            struct mm_string* e = &mm_sys_errlist[err];
            mm_string_destroy(e);
        }
    }
    return MM_OK;
}