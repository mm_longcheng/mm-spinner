#include "mm_runtime_stat.h"
#include "core/mm_logger.h"
///////////////////////////////////////////////////////////////////////////////
const char* __seek_items(const char* buffer,int ie)
{
	int i;
	int count = 0;
	assert(buffer);
	const char* p = buffer;
	int len = strlen(buffer);
	if ( ie <= 0)
	{
		return p;
	}
	for (i=0; i<len; i++)
	{
		if (' ' == *p)
		{
			count++;
			if (count == ie)
			{
				p++;
				break;
			}
		}
		p++;
	}
	return p;
}
///////////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_runtime_loadavg_init(struct mm_runtime_loadavg* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_loadavg));
}
MM_EXPORT_DLL void mm_runtime_loadavg_destroy(struct mm_runtime_loadavg* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_loadavg));
}
MM_EXPORT_DLL void mm_runtime_loadavg_perform(struct mm_runtime_loadavg* p)
{
	FILE* f = fopen("/proc/loadavg","rb");
	if (f)
	{
		char line[MM_PROC_MAX_LENGTH] = {0};
		if ( NULL != fgets(line,MM_PROC_MAX_LENGTH,f) )
		{
			sscanf(line,"%f %f %f %d/%d %" PRIu64
				,&p->lavg_01
				,&p->lavg_05
				,&p->lavg_15
				,&p->nr_running
				,&p->nr_threads
				,&p->last_pid);
		}
		fclose(f);
	}
}
///////////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_runtime_machine_init(struct mm_runtime_machine* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_machine));
}
MM_EXPORT_DLL void mm_runtime_machine_destroy(struct mm_runtime_machine* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_machine));
}
MM_EXPORT_DLL void mm_runtime_machine_perform(struct mm_runtime_machine* p)
{
	mm_runtime_machine_perform_cpu(p);
	mm_runtime_machine_perform_mem(p);
}
MM_EXPORT_DLL void mm_runtime_machine_perform_cpu(struct mm_runtime_machine* p)
{
	FILE* f = fopen("/proc/stat","rb");
	if (f)
	{
		char line[MM_PROC_MAX_LENGTH] = {0};
		if ( NULL != fgets(line,MM_PROC_MAX_LENGTH,f) )
		{
			sscanf(line,"%*s %" PRIu64 " %" PRIu64 " %" PRIu64 " %" PRIu64
				,&p->cpu_user
				,&p->cpu_nice
				,&p->cpu_system
				,&p->cpu_idle);
		}
		fclose(f);
	}
	p->cores = (mm_uint32_t)sysconf(_SC_NPROCESSORS_ONLN);
}
MM_EXPORT_DLL void mm_runtime_machine_perform_mem(struct mm_runtime_machine* p)
{
	FILE* f = fopen("/proc/meminfo","rb");
	if (f)
	{
		char line[MM_PROC_MAX_LENGTH] = {0};
		if ( NULL != fgets(line,MM_PROC_MAX_LENGTH,f) )
		{
			sscanf(line,"%*s %" PRIu64,&p->mem_total);
		}
		if ( NULL != fgets(line,MM_PROC_MAX_LENGTH,f) )
		{
			sscanf(line,"%*s %" PRIu64,&p->mem_free);
		}
		if ( NULL != fgets(line,MM_PROC_MAX_LENGTH,f) )
		{
			sscanf(line,"%*s %" PRIu64,&p->mem_buff);
		}
		if ( NULL != fgets(line,MM_PROC_MAX_LENGTH,f) )
		{
			sscanf(line,"%*s %" PRIu64,&p->mem_cache);
		}
		fclose(f);
	}
}
///////////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_runtime_process_init(struct mm_runtime_process* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_process));
}
MM_EXPORT_DLL void mm_runtime_process_destroy(struct mm_runtime_process* p)
{
	mm_memset(p,0,sizeof(struct mm_runtime_process));
}
MM_EXPORT_DLL void mm_runtime_process_perform(struct mm_runtime_process* p)
{
	mm_runtime_process_perform_pid(p);
	mm_runtime_process_perform_cpu(p);
	mm_runtime_process_perform_mem(p);
	mm_runtime_process_perform_io(p);
}
MM_EXPORT_DLL void mm_runtime_process_perform_pid(struct mm_runtime_process* p)
{
	p->pid = getpid();
}
MM_EXPORT_DLL void mm_runtime_process_perform_cpu(struct mm_runtime_process* p)
{
	char cmd[128];
	mm_sprintf(cmd,"/proc/%llu/stat",(mm_ullong_t)p->pid);
	FILE* f = fopen(cmd,"rb");
	if (f)
	{
		char line[MM_PROC_MAX_LENGTH] = {0};
		if ( NULL != fgets(line,MM_PROC_MAX_LENGTH,f) )
		{
			const char* b = __seek_items(line,1);
			sscanf(b,"%*s %c",&p->state);
			//get data in line 14 15 16 17
			const char* q = __seek_items(b,12);
			sscanf(q,"%" PRIu64 " %" PRIu64 " %" PRIu64 " %" PRIu64 ""
				,&p->cpu_utime
				,&p->cpu_stime
				,&p->cpu_cutime
				,&p->cpu_cstime);
			//get data in line 20
			const char* t = __seek_items(q,6);
			sscanf(t,"%d",&p->threads);
		}
		fclose(f);
	}
}
MM_EXPORT_DLL void mm_runtime_process_perform_mem(struct mm_runtime_process* p)
{
	char cmd[128];
	mm_sprintf(cmd,"/proc/%" PRIu64 "/status",p->pid);
	FILE* f = fopen(cmd,"rb");
	if (f)
	{
		int i = 0;
		char line[MM_PROC_MAX_LENGTH] = {0};
		//read before line 12
		for (i=0;i<12;i++)
		{
			fgets(line,MM_PROC_MAX_LENGTH,f);
		}
		if ( NULL != fgets(line,MM_PROC_MAX_LENGTH,f) )
		{
			sscanf(line,"%*s %" PRIu64,&p->vm_size);
		}
		//read before line 4
		for (i=0;i<2;i++)
		{
			fgets(line,MM_PROC_MAX_LENGTH,f);
		}
		if ( NULL != fgets(line,MM_PROC_MAX_LENGTH,f) )
		{
			sscanf(line,"%*s %" PRIu64,&p->vm_rss);
		}
		if ( NULL != fgets(line,MM_PROC_MAX_LENGTH,f) )
		{
			sscanf(line,"%*s %" PRIu64,&p->vm_data);
		}
		if ( NULL != fgets(line,MM_PROC_MAX_LENGTH,f) )
		{
			sscanf(line,"%*s %" PRIu64,&p->vm_stk);
		}
		fclose(f);
	}
}
MM_EXPORT_DLL void mm_runtime_process_perform_io(struct mm_runtime_process* p)
{
	char cmd[128];
	mm_sprintf(cmd,"/proc/%" PRIu64 "/io",p->pid);
	FILE* f = fopen(cmd,"rb");
	if (f)
	{
		char line[MM_PROC_MAX_LENGTH] = {0};
		if ( NULL != fgets(line,MM_PROC_MAX_LENGTH,f) )
		{
			sscanf(line,"%*s %" PRIu64,&p->rchar);
		}
		if ( NULL != fgets(line,MM_PROC_MAX_LENGTH,f) )
		{
			sscanf(line,"%*s %" PRIu64,&p->wchar);
		}
		fclose(f);
	}
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_runtime_stat_init( struct mm_runtime_stat* p )
{
	p->pid = 0;
	p->cores = 0;
	p->vm_rss = 0;
	p->rchar = 0;
	p->wchar = 0;
	p->cpu_sys = 0;
	p->cpu_pro = 0;
	p->mem_pro = 0;
	p->timecode_o = 0;
	p->timecode_n = 0;
	p->sampling = 0;
	mm_memset(&p->tv_o,0,sizeof(struct timeval));
	mm_memset(&p->tv_n,0,sizeof(struct timeval));
	mm_runtime_loadavg_init(&p->loadavg_o);
	mm_runtime_loadavg_init(&p->loadavg_n);
	mm_runtime_machine_init(&p->machine_o);
	mm_runtime_machine_init(&p->machine_n);
	mm_runtime_process_init(&p->process_o);
	mm_runtime_process_init(&p->process_n);
}

MM_EXPORT_DLL void mm_runtime_stat_destroy( struct mm_runtime_stat* p )
{
	p->pid = 0;
	p->cores = 0;
	p->vm_rss = 0;
	p->rchar = 0;
	p->wchar = 0;
	p->cpu_sys = 0;
	p->cpu_pro = 0;
	p->mem_pro = 0;
	p->timecode_o = 0;
	p->timecode_n = 0;
	p->sampling = 0;
	mm_memset(&p->tv_o,0,sizeof(struct timeval));
	mm_memset(&p->tv_n,0,sizeof(struct timeval));
	mm_runtime_loadavg_destroy(&p->loadavg_o);
	mm_runtime_loadavg_destroy(&p->loadavg_n);
	mm_runtime_machine_destroy(&p->machine_o);
	mm_runtime_machine_destroy(&p->machine_n);
	mm_runtime_process_destroy(&p->process_o);
	mm_runtime_process_destroy(&p->process_n);
}

MM_EXPORT_DLL void mm_runtime_stat_update( struct mm_runtime_stat* p )
{
	mm_uint64_t b_utime,b_stime,b_cutime,b_cstime;
	mm_uint64_t b_user,b_nice,b_system,b_idle,a,b,c;
	//////////////////////////////////////////////////////////////////////////
	mm_gettimeofday(&p->tv_n,NULL);
	p->timecode_n = p->tv_n.tv_sec * 1000 + (p->tv_n.tv_usec / 1000);
	p->sampling = p->timecode_n - p->timecode_o;
	//
	mm_runtime_loadavg_perform(&p->loadavg_n);
	mm_runtime_machine_perform(&p->machine_n);
	mm_runtime_process_perform(&p->process_n);
	//////////////////////////////////////////////////////////////////////////
	p->pid = p->process_n.pid;
	p->vm_rss = p->process_n.vm_rss;
	p->rchar = p->process_n.rchar;
	p->wchar = p->process_n.wchar;
	p->cores = p->machine_n.cores;
	//
	b_utime = p->process_n.cpu_utime - p->process_o.cpu_utime;
	b_stime = p->process_n.cpu_stime - p->process_o.cpu_stime;
	b_cutime = p->process_n.cpu_cutime - p->process_o.cpu_cutime;
	b_cstime = p->process_n.cpu_cstime - p->process_o.cpu_cstime;
	a = b_utime + b_stime + b_cutime + b_cstime;

	b_user = p->machine_n.cpu_user - p->machine_o.cpu_user;
	b_nice = p->machine_n.cpu_nice - p->machine_o.cpu_nice;
	b_system = p->machine_n.cpu_system - p->machine_o.cpu_system;
	b_idle = p->machine_n.cpu_idle - p->machine_o.cpu_idle;
	b = b_user + b_nice + b_system + b_idle;
	c = b_user + b_nice + b_system;

	p->cpu_sys = ( 0 == c && 0 == b_idle ) ? 0 : c * 100.0f /( c + b_idle );
	p->cpu_pro = ( 0 == b ) ? 0 : a * 100.0f / b;
	p->mem_pro = p->vm_rss * 100.0f / p->machine_o.mem_total;
	//////////////////////////////////////////////////////////////////////////
	mm_memcpy(&p->loadavg_o,&p->loadavg_n,sizeof(struct mm_runtime_loadavg));
	mm_memcpy(&p->machine_o,&p->machine_n,sizeof(struct mm_runtime_machine));
	mm_memcpy(&p->process_o,&p->process_n,sizeof(struct mm_runtime_process));
	p->timecode_o = p->timecode_n;
}
//////////////////////////////////////////////////////////////////////////