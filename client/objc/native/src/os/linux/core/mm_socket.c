#include "core/mm_socket.h"
#include "core/mm_logger.h"

/*
 * ioctl(FIONBIO) sets a non-blocking mode with the single syscall
 * while fcntl(F_SETFL, O_NONBLOCK) needs to learn the current state
 * using fcntl(F_GETFL).
 *
 * ioctl() and fcntl() are syscalls at least in FreeBSD 2.x, Linux 2.2
 * and Solaris 7.
 *
 * ioctl() in Linux 2.4 and 2.6 uses BKL, however, fcntl(F_SETFL) uses it too.
 */


#if (MM_HAVE_FIONBIO)

MM_EXPORT_DLL int
mm_nonblocking(mm_socket_t s)
{
    int  nb;

    nb = 1;

    return ioctl(s, FIONBIO, &nb);
}


MM_EXPORT_DLL int
mm_blocking(mm_socket_t s)
{
    int  nb;

    nb = 0;

    return ioctl(s, FIONBIO, &nb);
}

#endif


#if (MM_FREEBSD)

MM_EXPORT_DLL int
mm_tcp_nopush(mm_socket_t s)
{
    int  tcp_nopush;

    tcp_nopush = 1;

    return setsockopt(s, IPPROTO_TCP, TCP_NOPUSH,
                      (const void *) &tcp_nopush, sizeof(int));
}


MM_EXPORT_DLL int
mm_tcp_push(mm_socket_t s)
{
    int  tcp_nopush;

    tcp_nopush = 0;

    return setsockopt(s, IPPROTO_TCP, TCP_NOPUSH,
                      (const void *) &tcp_nopush, sizeof(int));
}

#elif (MM_LINUX)


MM_EXPORT_DLL int
mm_tcp_nopush(mm_socket_t s)
{
    int  cork;

    cork = 1;

    return setsockopt(s, IPPROTO_TCP, TCP_CORK,
                      (const void *) &cork, sizeof(int));
}


MM_EXPORT_DLL int
mm_tcp_push(mm_socket_t s)
{
    int  cork;

    cork = 0;

    return setsockopt(s, IPPROTO_TCP, TCP_CORK,
                      (const void *) &cork, sizeof(int));
}

#else

MM_EXPORT_DLL int
mm_tcp_nopush(mm_socket_t s)
{
    return 0;
}


MM_EXPORT_DLL int
mm_tcp_push(mm_socket_t s)
{
    return 0;
}

#endif

MM_EXPORT_DLL int mm_socket_keepalive(mm_socket_t s,int flag)
{
    if ( 0 != setsockopt(s, SOL_SOCKET, SO_KEEPALIVE, (char*)&flag, sizeof(flag)) )
    {
        mm_err_t errcode = mm_errno;
        struct mm_logger* g_logger = mm_logger_instance();
        mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
        mm_logger_log_E(g_logger,"%s %d can not keepalive fd:%d",__FUNCTION__,__LINE__,s);
        return MM_ERROR;  
    }
    return MM_OK;
}

MM_EXPORT_DLL int mm_socket_keepalive_ctl(mm_socket_t s,int tcp_keepidle,int tcp_keepintvl,int tcp_keepcnt)
{
    if ( 0 != setsockopt(s, SOL_TCP, TCP_KEEPIDLE, (void *)&tcp_keepidle, sizeof(tcp_keepidle)) )
    {
        mm_err_t errcode = mm_errno;
        struct mm_logger* g_logger = mm_logger_instance();
        mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
        mm_logger_log_E(g_logger,"%s %d can not keepalive ctl idle fd:%d",__FUNCTION__,__LINE__,s);
        return MM_ERROR;  
    }
    if ( 0 != setsockopt(s, SOL_TCP, TCP_KEEPINTVL, (void *)&tcp_keepintvl, sizeof(tcp_keepintvl)) )
    {
        mm_err_t errcode = mm_errno;
        struct mm_logger* g_logger = mm_logger_instance();
        mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
        mm_logger_log_E(g_logger,"%s %d can not keepalive ctl intvl fd:%d",__FUNCTION__,__LINE__,s);
        return MM_ERROR;  
    }
    if ( 0 != setsockopt(s, SOL_TCP, TCP_KEEPCNT, (void *)&tcp_keepcnt, sizeof(tcp_keepcnt)) )
    {
        mm_err_t errcode = mm_errno;
        struct mm_logger* g_logger = mm_logger_instance();
        mm_logger_socket_error(g_logger, MM_LOG_ERROR, errcode);
        mm_logger_log_E(g_logger,"%s %d can not keepalive ctl cnt fd:%d",__FUNCTION__,__LINE__,s);
        return MM_ERROR;  
    }
    return MM_OK;
}