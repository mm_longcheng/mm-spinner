#include "poller/mm_poll.h"
#include "core/mm_alloc.h"
#include "core/mm_string.h"
//////////////////////////////////////////////////////////////////////////
struct mm_poll_epoll
{
	mm_socket_t pfd;
	struct epoll_event* ev;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL const mm_char_t* mm_poll_api()
{
	return "epoll";
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_poll_init(struct mm_poll* p)
{
	struct mm_poll_epoll* _impl = NULL;
	p->ev_length = 256;
	p->pe = (struct mm_poll_event*)mm_malloc(sizeof(struct mm_poll_event) * p->ev_length);
	_impl = (struct mm_poll_epoll*)mm_malloc(sizeof(struct mm_poll_epoll));
	_impl->pfd = epoll_create(1024);
	_impl->ev = (struct epoll_event*)mm_malloc(sizeof(struct epoll_event) * p->ev_length);
	p->impl = _impl;
}
MM_EXPORT_DLL void mm_poll_destroy(struct mm_poll* p)
{
	struct mm_poll_epoll* _impl = (struct mm_poll_epoll*)(p->impl);
	mm_close_socket(_impl->pfd);
	mm_free(_impl->ev);
	mm_free(p->pe);
	mm_free(_impl);
}
MM_EXPORT_DLL void mm_poll_set_length(struct mm_poll* p, mm_uint32_t length)
{
	if (p->ev_length != length)
	{
		struct mm_poll_epoll* _impl = (struct mm_poll_epoll*)(p->impl);
		p->ev_length = length;
		p->pe = (struct mm_poll_event*)mm_realloc(p->pe,sizeof(struct mm_poll_event) * p->ev_length);
		_impl->ev = (struct epoll_event*)mm_realloc(_impl->ev,sizeof(struct epoll_event) * p->ev_length);
	}
}
MM_EXPORT_DLL mm_uint32_t mm_poll_get_length(struct mm_poll* p)
{
	return p->ev_length;
}
MM_EXPORT_DLL mm_int_t mm_poll_add_event(struct mm_poll* p, mm_socket_t fd, int mask, void* ud)
{
	struct mm_poll_epoll* _impl = (struct mm_poll_epoll*)(p->impl);
	struct epoll_event ev = {0, {0}};
	if (mask & MM_PE_READABLE) ev.events |= EPOLLIN;
	if (mask & MM_PE_WRITABLE) ev.events |= EPOLLOUT;
	ev.events |= EPOLLET;
	ev.data.ptr = ud;
	if ( epoll_ctl(_impl->pfd, EPOLL_CTL_ADD, fd, &ev) == -1 ) return -1;
	return 0;
}
MM_EXPORT_DLL mm_int_t mm_poll_mod_event(struct mm_poll* p, mm_socket_t fd, int mask, void* ud)
{
	struct mm_poll_epoll* _impl = (struct mm_poll_epoll*)(p->impl);
	struct epoll_event ev = {0, {0}};
	if (mask & MM_PE_READABLE) ev.events |= EPOLLIN;
	if (mask & MM_PE_WRITABLE) ev.events |= EPOLLOUT;
	ev.events |= EPOLLET;
	ev.data.ptr = ud;
	if ( epoll_ctl(_impl->pfd, EPOLL_CTL_MOD, fd, &ev) == -1 ) return -1;
	return 0;
}
MM_EXPORT_DLL mm_int_t mm_poll_del_event(struct mm_poll* p, mm_socket_t fd, int mask, void* ud)
{
	struct mm_poll_epoll* _impl = (struct mm_poll_epoll*)(p->impl);
	struct epoll_event ev = {0, {0}};
    mask = (MM_PE_READABLE | MM_PE_WRITABLE) & (~mask);
    ev.events = 0;
    if (mask & MM_PE_READABLE) ev.events |= EPOLLIN;
    if (mask & MM_PE_WRITABLE) ev.events |= EPOLLOUT;
	ev.events |= EPOLLET;
	ev.data.ptr = ud;
    if (mask != MM_PE_NONEABLE)
	{
		if ( epoll_ctl(_impl->pfd, EPOLL_CTL_MOD, fd, &ev) == -1 ) return -1;
    }
	else
	{
        /* Note, Kernel < 2.6.9 requires a non null event pointer even for
         * EPOLL_CTL_DEL. */
        if ( epoll_ctl(_impl->pfd, EPOLL_CTL_DEL, fd, &ev) == -1 ) return -1;
    }
	return 0;
}
MM_EXPORT_DLL mm_int_t mm_poll_wait_event(struct mm_poll* p, mm_msec_t milliseconds)
{
	// struct epoll_event ev[len];
	int n = 0;
	int i;
	//
	struct mm_poll_epoll* _impl = (struct mm_poll_epoll*)(p->impl);
	//
	mm_memset(p->pe,0,sizeof(struct mm_poll_event) * p->ev_length);
	n = epoll_wait(_impl->pfd , _impl->ev, p->ev_length, milliseconds);
	
	for (i = 0;i < n; i++) 
	{
		int mask = 0;
		struct epoll_event* ee = &(_impl->ev[i]);

		if (ee->events & EPOLLIN)  mask |= MM_PE_READABLE;
		if (ee->events & EPOLLOUT) mask |= MM_PE_WRITABLE;
		if (ee->events & EPOLLERR) mask |= MM_PE_WRITABLE;
		if (ee->events & EPOLLHUP) mask |= MM_PE_WRITABLE;

		p->pe[i].s = ee->data.ptr;
		p->pe[i].mask = mask;
	}
	return n;
}