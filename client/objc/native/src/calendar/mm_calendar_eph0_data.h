﻿#ifndef __mm_calendar_eph0_data_h__
#define __mm_calendar_eph0_data_h__
#include "core/mm_platform.h"
#include "core/mm_prefix.h"
#include "calendar/mm_calendar_const.h"
#include "calendar/mm_calendar_vector.h"
//=================================deltat T计算=====================================
//==================================================================================
// TD - UT1 计算表
MM_EXPORT_DLL extern const struct cs_arr1 cs_dt_at;
//=================================岁差计算=========================================
//==================================================================================
#define cs_preceTabType_length 3
#define cs_preceTab_w1 4
#define cs_preceTab_w2 6
#define cs_preceTab_w3 6
#define cs_preceTab_h 12
//
MM_EXPORT_DLL extern const unsigned int cs_preceTab_wtab[cs_preceTabType_length];
//IAU1976岁差表
MM_EXPORT_DLL extern const struct cs_arr1 cs_preceTab_IAU1976;// 12x4
//IAU2000岁差表
MM_EXPORT_DLL extern const struct cs_arr1 cs_preceTab_IAU2000;// 12x6
//P03岁差表
MM_EXPORT_DLL extern const struct cs_arr1 cs_preceTab_P03;    // 12x6

MM_EXPORT_DLL extern const struct cs_arr2 cs_preceTab;
//=================================章动计算=========================================
//==================================================================================
//IAU2000B章动序列
#define cs_nuTab_w 11
#define cs_nuTab_h 77
MM_EXPORT_DLL extern const struct cs_arr1 cs_nuTab;// 11x77

#define cs_nutB_w  5
#define cs_nutB_h 10
//中精度章动计算表
MM_EXPORT_DLL extern const struct cs_arr1 cs_nutB;// 5x10
//=================================星历数据=========================================
//==================================================================================

/********************************
8行星星历数据表,及数据表的计算
********************************/
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0_0;
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0_1;
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0_2;
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0_3;
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0_4;
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0_5;
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0_6;
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0_7;
// 
MM_EXPORT_DLL extern const struct cs_arr2 cs_XL0;
/********************************
冥王星星历数据表
********************************/
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0Pluto_0;
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0Pluto_1;
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0Pluto_2;
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0Pluto_3;
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0Pluto_4;
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0Pluto_5;
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0Pluto_6;
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0Pluto_7;
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0Pluto_8;

MM_EXPORT_DLL extern const struct cs_arr2 cs_XL0Pluto;
/********************************
月亮星历数据表
********************************/
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL1_00;//ML0
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL1_01;//ML1
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL1_02;//ML2
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL1_03;//ML3

MM_EXPORT_DLL extern const struct cs_arr1 cs_XL1_10;//MB0
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL1_11;//MB1
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL1_12;//MB2

MM_EXPORT_DLL extern const struct cs_arr1 cs_XL1_20;//MR0
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL1_21;//MR1
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL1_22;//MR2
//
MM_EXPORT_DLL extern const struct cs_arr2 cs_XL1_g0;// g0
MM_EXPORT_DLL extern const struct cs_arr2 cs_XL1_g1;// g1
MM_EXPORT_DLL extern const struct cs_arr2 cs_XL1_g2;// g2
//
//各坐标均是余弦项,各列单位:角秒,1,1,1e-4,1e-8,1e-8
MM_EXPORT_DLL extern const struct cs_arr3 cs_XL1;
//=================================行星星历=========================================
//==================================================================================
#define cs_XL0_xzb_w 3
#define cs_XL0_xzb_h 7
//行星星历修正表
MM_EXPORT_DLL extern const struct cs_arr1 cs_XL0_xzb;//3x7

#include "core/mm_suffix.h"

#endif//__mm_calendar_eph0_data_h__