﻿#ifndef __mm_calendar_lunar_h__
#define __mm_calendar_lunar_h__
#include "core/mm_platform.h"
#include "core/mm_prefix.h"
#include "calendar/mm_calendar_type.h"
#include "calendar/mm_calendar_vector.h"

//年月日
struct cs_lunar_nyr
{
	int y;//年
	int m;//月
	int d;//日
};
//月
struct cs_lunar_yue
{
	int d0;		//月首的J2000.0起算的儒略日数
	int dn;		//本月的天数
	int week0;	//月首的星期
	int weeki;	//本日所在的周序号
	int weekN;	//本月的总周数
};
//天干地支
struct cs_lunar_tgdz
{
	char g;// 干
	char z;// 支
};
//干支四柱
struct cs_lunar_gzsz
{
	struct cs_lunar_tgdz gz_jn; //干支纪年
	struct cs_lunar_tgdz gz_jy; //干支纪月
	struct cs_lunar_tgdz gz_jr; //干支纪日
	struct cs_lunar_tgdz gz_js; //干支纪时
	cs_real gz_zty;	            //干支真太阳时
};

//命理八字计算。jd为格林尼治UT(J2000起算),J为本地经度,返回在物件re中
MM_EXPORT_DLL void cs_lunar_mingLiBaZi(cs_real jd,cs_real J,struct cs_lunar_gzsz* re);
//回历计算,J2000.0起算的儒略日数
MM_EXPORT_DLL void cs_lunar_getHuiLi(cs_real d0,struct cs_lunar_nyr* re);
//精气
MM_EXPORT_DLL cs_real cs_lunar_qi_accurate(cs_real W);
//精朔
MM_EXPORT_DLL cs_real cs_lunar_so_accurate(cs_real W);
//精气
MM_EXPORT_DLL cs_real cs_lunar_qi_accurate2(cs_real jd);
//精朔
MM_EXPORT_DLL cs_real cs_lunar_so_accurate2(cs_real jd);

#include "core/mm_suffix.h"

#endif//__mm_calendar_lunar_h__