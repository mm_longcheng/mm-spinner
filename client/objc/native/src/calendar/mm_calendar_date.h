﻿#ifndef __mm_calendar_date_h__
#define __mm_calendar_date_h__
#include "core/mm_platform.h"
#include "core/mm_prefix.h"
#include "core/mm_string.h"
#include "calendar/mm_calendar_type.h"
//=================================日期计算--=======================================
//==================================================================================
// we not use int but prefer cs_real, because we have some like floor(D/30.601) algorithm.
struct cs_date
{
	cs_real Y;// 年
	cs_real M;// 月
	cs_real D;// 日
	cs_real h;// 时
	cs_real m;// 分
	cs_real s;// 秒
};
MM_EXPORT_DLL void cs_date_init(struct cs_date* p);
MM_EXPORT_DLL void cs_date_init_arg(struct cs_date* p,cs_real _Y,cs_real _M,cs_real _D,cs_real _h,cs_real _m,cs_real _s);
//公历转儒略日
MM_EXPORT_DLL cs_real cs_date_toJD(struct cs_date* p);
//儒略日数转公历
MM_EXPORT_DLL void cs_date_setFromJD(struct cs_date* p,cs_real jd);
//日期转为串
MM_EXPORT_DLL void cs_date_DD2str(struct cs_date* p,struct mm_string* str);
//////////////////////////////////////////////////////////////////////////
//提取jd中的时间(去除日期)
MM_EXPORT_DLL void cs_date_timeStr(cs_real jd,struct mm_string* str);
//星期计算
MM_EXPORT_DLL int cs_date_getWeek(cs_real jd);
//求y年m月的第n个星期w的儒略日数
MM_EXPORT_DLL cs_real cs_date_nnweek(cs_real y,cs_real m,cs_real n,cs_real w);
//公历转儒略日
MM_EXPORT_DLL cs_real cs_date_JD(cs_real y,cs_real m,cs_real d);
//JD转为串
MM_EXPORT_DLL void cs_date_JD2str(cs_real jd,struct mm_string* str);

#include "core/mm_suffix.h"

#endif//__mm_calendar_date_h__