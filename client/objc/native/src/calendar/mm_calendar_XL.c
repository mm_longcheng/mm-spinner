﻿#include "calendar/mm_calendar_XL.h"
#include "calendar/mm_calendar_eph0.h"
#include <math.h>

//日月星历基本函数类
//=====================
//星历函数(日月球面坐标计算)
//地球经度计算,返回Date分点黄经,传入世纪数、取项数
MM_EXPORT_DLL cs_real cs_XL_E_Lon(cs_real t,cs_real n)
{
	return cs_XL0_calc(0,0, t,n);
}
//月球经度计算,返回Date分点黄经,传入世纪数,n是项数比例
MM_EXPORT_DLL cs_real cs_XL_M_Lon(cs_real t,cs_real n)
{
	return cs_XL1_calc(0,t,n);
}
//=========================
//地球速度,t是世纪数,误差小于万分3
MM_EXPORT_DLL cs_real cs_XL_E_v(cs_real t)
{
	cs_real f=628.307585*t;
	return 628.332 +21 *sin(1.527+f) +0.44 *sin(1.48+f*2) 
		+0.129*sin(5.82+f)*t +0.00055*sin(4.21+f)*t*t;
}
//月球速度计算,传入世经数
MM_EXPORT_DLL cs_real cs_XL_M_v(cs_real t)
{
	cs_real v = 8399.71 - 914*sin( 0.7848 + 8328.691425*t + 0.0001523*t*t ); //误差小于5%
	//误差小于0.3%
	v-= 179*sin( 2.543  +15542.7543*t )+
		160*sin( 0.1874 + 7214.0629*t )+
		62 *sin( 3.14   +16657.3828*t )+
		34 *sin( 4.827  +16866.9323*t )+
		22 *sin( 4.9    +23871.4457*t )+
		12 *sin( 2.59   +14914.4523*t )+
		7  *sin( 0.23   + 6585.7609*t )+
		5  *sin( 0.9    +25195.624 *t )+
		5  *sin( 2.32   - 7700.3895*t )+
		5  *sin( 3.88   + 8956.9934*t )+
		5  *sin( 0.49   + 7771.3771*t );
	return v;
}
//=========================
//月日视黄经的差值
MM_EXPORT_DLL cs_real cs_XL_MS_aLon(cs_real t,cs_real Mn,cs_real Sn)
{
	return cs_XL_M_Lon(t,Mn) + cs_gxc_moonLon(t) - ( cs_XL_E_Lon(t,Sn) + cs_gxc_sunLon(t) + cs_PI );
}
//太阳视黄经
//busy!!
MM_EXPORT_DLL cs_real cs_XL_S_aLon(cs_real t,cs_real n)
{
	return cs_XL_E_Lon(t,n) + cs_nutationLon2(t) + cs_gxc_sunLon(t) + cs_PI; //注意，这里的章动计算很耗时
}
//=========================
//已知地球真黄经求时间
MM_EXPORT_DLL cs_real cs_XL_E_Lon_t(cs_real W)
{
	cs_real t = 0;
	cs_real v = 628.3319653318;
	t  = ( W - 1.75347           )/v; v = cs_XL_E_v(t);   //v的精度0.03%，详见原文
	t += ( W - cs_XL_E_Lon(t,10) )/v; v = cs_XL_E_v(t);   //再算一次v有助于提高精度,不算也可以
	t += ( W - cs_XL_E_Lon(t,-1) )/v;
	return t;
}
//已知真月球黄经求时间
MM_EXPORT_DLL cs_real cs_XL_M_Lon_t(cs_real W)
{
	cs_real t = 0;
	cs_real v = 8399.70911033384;
	t  = ( W - 3.81034         )/v;
	t += ( W - cs_XL_M_Lon(t,3 ) )/v; v = cs_XL_M_v(t);  //v的精度0.5%，详见原文
	t += ( W - cs_XL_M_Lon(t,20) )/v;
	t += ( W - cs_XL_M_Lon(t,-1) )/v;
	return t;
}
//已知月日视黄经差求时间
MM_EXPORT_DLL cs_real cs_XL_MS_aLon_t(cs_real W)
{
	cs_real t = 0;
	cs_real v = 7771.37714500204;
	t  = ( W + 1.08472                )/v;
	t += ( W - cs_XL_MS_aLon(t, 3, 3) )/v; v = cs_XL_M_v(t)-cs_XL_E_v(t);  //v的精度0.5%，详见原文
	t += ( W - cs_XL_MS_aLon(t,20,10) )/v;
	t += ( W - cs_XL_MS_aLon(t,-1,60) )/v;
	return t;
}
//已知太阳视黄经反求时间
MM_EXPORT_DLL cs_real cs_XL_S_aLon_t(cs_real W)
{
	cs_real t = 0;
	cs_real v = 628.3319653318;
	t  = ( W - 1.75347 - cs_PI    )/v; v = cs_XL_E_v(t); //v的精度0.03%，详见原文
	t += ( W - cs_XL_S_aLon(t,10) )/v; v = cs_XL_E_v(t); //再算一次v有助于提高精度,不算也可以
	t += ( W - cs_XL_S_aLon(t,-1) )/v;
	return t;
}
//已知月日视黄经差求时间,高速低精度,误差不超过40秒
MM_EXPORT_DLL cs_real cs_XL_MS_aLon_t1(cs_real W)
{ 
	cs_real t = 0;
	cs_real v = 7771.37714500204;
	t  = ( W + 1.08472                )/v;
	t += ( W - cs_XL_MS_aLon(t, 3, 3) )/v; v = cs_XL_M_v(t)-cs_XL_E_v(t);  //v的精度0.5%，详见原文
	t += ( W - cs_XL_MS_aLon(t,50,20) )/v;
	return t;
}
//已知太阳视黄经反求时间,高速低精度,最大误差不超过50秒,平均误差15秒
MM_EXPORT_DLL cs_real cs_XL_S_aLon_t1(cs_real W)
{
	cs_real t = 0;
	cs_real v= 628.3319653318;
	t  = ( W - 1.75347 - cs_PI   )/v; v = 628.332 + 21*sin( 1.527+628.307585*t );
	t += ( W - cs_XL_S_aLon(t,3) )/v;
	t += ( W - cs_XL_S_aLon(t,40))/v;
	return t;
}
//已知月日视黄经差求时间,高速低精度,误差不超过600秒(只验算了几千年)
MM_EXPORT_DLL cs_real cs_XL_MS_aLon_t2(cs_real W)
{
	cs_real t = 0,L = 0,t2 = 0;
	cs_real v = 7771.37714500204;
	t  = ( W + 1.08472 )/v;
	t2 = t*t;
	t -= ( -0.00003309*t2 + 0.10976*cos( 0.784758 + 8328.6914246*t + 0.000152292*t2 ) + 0.02224 *cos( 0.18740  + 7214.0628654*t - 0.00021848 *t2 ) - 0.03342 *cos( 4.669257 + 628.307585*t ) )/v;
	L = cs_XL_M_Lon(t,20) - (4.8950632+ 628.3319653318*t + 0.000005297*t*t + 0.0334166*cos(4.669257+628.307585*t) + 0.0002061*cos(2.67823+628.307585*t)*t + 0.000349*cos(4.6261+1256.61517*t) - 20.5/cs_rad);
	v = 7771.38 - 914*sin( 0.7848 + 8328.691425*t + 0.0001523*t*t ) - 179*sin( 2.543 + 15542.7543*t ) - 160*sin( 0.1874 + 7214.0629*t );
	t += ( W - L )/v;
	return t;
}
//已知太阳视黄经反求时间,高速低精度,最大误差不超过600秒
MM_EXPORT_DLL cs_real cs_XL_S_aLon_t2(cs_real W)
{
	cs_real t = 0;
	cs_real v = 628.3319653318;
	t =  ( W - 1.75347 - cs_PI )/v;
	t -= ( 0.000005297*t*t + 0.0334166 * cos( 4.669257 + 628.307585*t) + 0.0002061 * cos( 2.67823  + 628.307585*t)*t )/v;
	t += ( W - cs_XL_E_Lon(t,8) - cs_PI + (20.5+17.2*sin(2.1824-33.75705*t))/cs_rad )/v;
	return t;
}
//月亮被照亮部分的比例
MM_EXPORT_DLL cs_real cs_XL_moonIll(cs_real t)
{
	cs_real t2 = t*t,t3 = t2*t,t4 = t3*t;
	cs_real D,M,m,a;
	cs_real dm = cs_D2R;
	D = (297.8502042 + 445267.1115168*t - 0.0016300*t2 + t3/545868 - t4/113065000)*dm; //日月平距角
	M = (357.5291092 +  35999.0502909*t - 0.0001536*t2 + t3/24490000             )*dm; //太阳平近点
	m = (134.9634114 + 477198.8676313*t + 0.0089970*t2 + t3/69699 - t4/14712000  )*dm; //月亮平近点
	a = cs_PI - D + (-6.289*sin(m) +2.100*sin(M) -1.274*sin(D*2-m) -0.658*sin(D*2) -0.214*sin(m*2) -0.110*sin(D))*dm;
	return (1+cos(a))/2;
}
//转入地平纬度及地月质心距离,返回站心视半径(角秒)
MM_EXPORT_DLL cs_real cs_XL_moonRad(cs_real r,cs_real h)
{
	return cs_sMoon/r*(1+sin(h)*cs_rEar/r);
}
//求月亮近点时间和距离,t为儒略世纪数力学时
MM_EXPORT_DLL struct cs_vector3 cs_XL_moonMinR(cs_real t,cs_real _min)
{
	cs_real a = 27.55454988/36525;
	cs_real b = 0;
	cs_real r1,r2,r3,dt;
	struct cs_vector3 re;
	if(_min) {b = -10.3302/36525;} else {b = 3.4471/36525;}
	t = b + a * floor((t-b)/a+0.5); //平近(远)点时间
	//初算二次
	dt=1.0/36525;
	r1 = cs_XL1_calc( 2, t-dt, 10 );
	r2 = cs_XL1_calc( 2, t,    10 );
	r3 = cs_XL1_calc( 2, t+dt, 10 );
	t += (r1-r3)/(r1+r3-2*r2)*dt/2;
	dt=0.5/36525;
	r1 = cs_XL1_calc( 2, t-dt, 20 );
	r2 = cs_XL1_calc( 2, t,    20 );
	r3 = cs_XL1_calc( 2, t+dt, 20 );
	t += (r1-r3)/(r1+r3-2*r2)*dt/2;
	//精算
	dt = 1200.0/86400/36525;
	r1 = cs_XL1_calc( 2, t-dt, -1 );
	r2 = cs_XL1_calc( 2, t,    -1 );
	r3 = cs_XL1_calc( 2, t+dt, -1 );
	t += (r1-r3)/(r1+r3-2*r2)*dt/2;
	r2+= (r1-r3)/(r1+r3-2*r2)*(r3-r1)/8;
	re.x = t;
	re.y = r2;
	re.z = 0;
	return re;
}
//月亮升交点
MM_EXPORT_DLL struct cs_vector3 cs_XL_moonNode(cs_real t,cs_real asc)
{
	cs_real a = 27.21222082/36525;
	cs_real b = 0;
	cs_real w,v,w2,dt;
	struct cs_vector3 re;
	if(asc) {b = 21.0/36525;} else {b = 35/36525;}
	t = b + a * floor((t-b)/a+0.5); //平升(降)交点时间
	dt = 0.5 /36525; w = cs_XL1_calc( 1, t, 10 ); w2 = cs_XL1_calc( 1, t+dt, 10 ); v = (w2-w)/dt; t-=w/v;
	dt = 0.05/36525; w = cs_XL1_calc( 1, t, 40 ); w2 = cs_XL1_calc( 1, t+dt, 40 ); v = (w2-w)/dt; t-=w/v;
	w = cs_XL1_calc( 1, t, -1 );  t-=w/v;
	re.x = t;
	re.y = cs_XL1_calc( 0, t, -1 );
	re.z = 0;
	return re;
}
//地球近远点
MM_EXPORT_DLL struct cs_vector3 cs_XL_earthMinR(cs_real t,cs_real _min)
{
	cs_real a =365.25963586/36525;
	cs_real b = 0;
	cs_real r1,r2,r3,dt;
	struct cs_vector3 re;
	if(_min) {b = 1.7/36525;} else {b = 184.5/36525;}
	t = b + a * floor((t-b)/a+0.5); //平近(远)点时间
	//初算二次
	dt=3.0/36525;
	r1 = cs_XL0_calc( 0,2, t-dt, 10 );
	r2 = cs_XL0_calc( 0,2, t,    10 );
	r3 = cs_XL0_calc( 0,2, t+dt, 10 );
	t += (r1-r3)/(r1+r3-2*r2)*dt/2; //误差几个小时
	dt=0.2/36525;
	r1 = cs_XL0_calc( 0,2, t-dt, 80 );
	r2 = cs_XL0_calc( 0,2, t,    80 );
	r3 = cs_XL0_calc( 0,2, t+dt, 80 );
	t += (r1-r3)/(r1+r3-2*r2)*dt/2; //误差几分钟
	//精算
	dt = 0.01/36525;
	r1 = cs_XL0_calc( 0,2, t-dt, -1 );
	r2 = cs_XL0_calc( 0,2, t,    -1 );
	r3 = cs_XL0_calc( 0,2, t+dt, -1 );
	t += (r1-r3)/(r1+r3-2*r2)*dt/2; //误差小于秒
	r2+= (r1-r3)/(r1+r3-2*r2)*(r3-r1)/8;
	re.x = t;
	re.y = r2;
	re.z = 0;
	return re;
}
