﻿#include "calendar/mm_calendar_eph0.h"
#include <assert.h>
//==================================================================================
//经纬转(时分秒/度分秒)
MM_EXPORT_DLL struct cs_vector3 cs_jw2hdms(const struct cs_jw* v)
{
	struct cs_vector3 r;
	r.x = v->j;
	r.y = floor(v->w);
	r.z = (v->w - r.y) * 60;
	return r;
}
//时分秒转弧度
MM_EXPORT_DLL cs_real cs_hms2rad(const struct cs_vector3* v)
{
	return (v->x*3600+v->y*60+v->z*1)/cs_rad*15;
}
//度分秒转弧度
MM_EXPORT_DLL cs_real cs_dms2rad(const struct cs_vector3* v)
{
	return (v->x*3600+v->y*60+v->z*1)/cs_rad;
}
//=================================数学工具=========================================
//==================================================================================
//对超过0-2PI的角度转为0-2PI
MM_EXPORT_DLL cs_real cs_rad2mrad(cs_real v)
{
	v=fmod(v,cs_pi2);//v % (2*Math.PI);
	if(v<0) return v+cs_pi2;
	return v;
}
//对超过-PI到PI的角度转为-PI到PI
MM_EXPORT_DLL cs_real cs_rad2rrad(cs_real v)
{
	v=fmod(v,cs_pi2);//v % (2*Math.PI);
	if(v<=-cs_PI) return v+cs_pi2;
	if(v>  cs_PI) return v-cs_pi2;
	return v;
}
//临界余数(a与最近的整倍数b相差的距离)
MM_EXPORT_DLL cs_real cs_mod2(cs_real a,cs_real b)
{
	cs_real c=a/b;
	c -= floor(c);
	if(c>0.5) c-=1;
	return c*b;
}
//球面转直角坐标
MM_EXPORT_DLL struct cs_vector3 cs_llr2xyz(const struct cs_vector3* JW)
{
	struct cs_vector3 r;
	r.x=JW->z*cos(JW->y)*cos(JW->x);
	r.y=JW->z*cos(JW->y)*sin(JW->x);
	r.z=JW->z*sin(JW->y);
	return r;
}
//直角坐标转球
MM_EXPORT_DLL struct cs_vector3 cs_xyz2llr(const struct cs_vector3* xyz)
{
	struct cs_vector3 r;
	r.z = sqrt(xyz->x*xyz->x+xyz->y*xyz->y+xyz->z*xyz->z);
	r.y = asin(xyz->z/r.z);
	r.x = cs_rad2mrad( atan2(xyz->y,xyz->x) );
	return r;
}
//球面坐标旋转
MM_EXPORT_DLL struct cs_vector3 cs_llrConv(const struct cs_vector3* JW,cs_real E)
{
	//黄道赤道坐标变换,赤到黄E取负
	struct cs_vector3 r;
	cs_real sJ=sin(JW->x);
	cs_real sE=sin(E);
	cs_real cE=cos(E);
	r.x=atan2( sJ*cE - tan(JW->y)*sE, cos(JW->x) );
	r.y=asin ( cE*sin(JW->y) + sE*cos(JW->y)*sJ  );
	r.z=JW->z;
	r.x=cs_rad2mrad(r.x);
	return r;
}
//赤道坐标转为地平坐标
//转到相对于地平赤道分点的赤道坐标
MM_EXPORT_DLL struct cs_vector3 cs_CD2DP(const struct cs_vector3* z,cs_real L,cs_real fa,cs_real gst)
{
	struct cs_vector3 a = *z;
	a.x = z->x+cs_pi_2-gst-L;
	a = cs_llrConv( &a, cs_pi_2-fa );
	a.x = cs_rad2mrad( cs_pi_2-a.x );
	return a;
}
//求角度差
MM_EXPORT_DLL cs_real cs_j1_j2(cs_real J1,cs_real W1,cs_real J2,cs_real W2)
{
	cs_real dJ=cs_rad2rrad(J1-J2);
	cs_real dW=W1-W2;
	if(fabs(dJ)<0.001 && fabs(dW)<0.001)
	{
		dJ*=cos((W1+W2)/2);
		return sqrt(dJ*dJ+dW*dW);
	}
	return acos( sin(W1)*sin(W2) + cos(W1)*cos(W2)*cos(dJ) );
}
//日心球面转地心球面,Z星体球面坐标,A地球球面坐标
//本含数是通用的球面坐标中心平移函数,行星计算中将反复使用
MM_EXPORT_DLL struct cs_vector3 cs_h2g(struct cs_vector3 z,struct cs_vector3 a)
{
	a = cs_llr2xyz(&a); //地球
	z = cs_llr2xyz(&z); //星体
	z.x-=a.x; z.y-=a.y; z.z-=a.z;
	return cs_xyz2llr(&z);
}
//视差角(不是视差)
MM_EXPORT_DLL cs_real cs_shiChaJ(cs_real gst,cs_real L,cs_real fa,cs_real J,cs_real W)
{
	cs_real H=gst+L-J; //天体的时角
	return cs_rad2mrad( atan2(sin(H) , tan(fa)*cos(W) - sin(W)*cos(H)) );
}
//=================================deltat T计算=====================================
//==================================================================================
//二次曲线外推
MM_EXPORT_DLL cs_real cs_dt_ext(cs_real y,cs_real jsd)
{
	cs_real dy=(y-1820)/100; 
	return -20+jsd*dy*dy;
}
//计算世界时与原子时之差,传入年
MM_EXPORT_DLL cs_real cs_dt_calc(cs_real y)
{
	unsigned int i = 0;
	const cs_real* d = cs_dt_at.d;
	cs_real t1,t2,t3;
	cs_real y0=d[cs_dt_at.len-2]; //表中最后一年
	cs_real t0=d[cs_dt_at.len-1]; //表中最后一年的deltatT
	if(y>=y0)
	{
		cs_real v = 0,dv = 0,jsd = 31;
		//sjd是y1年之后的加速度估计。瑞士星历表jsd=31,NASA网站jsd=32,skmap的jsd=29
		if(y>y0+100) {return cs_dt_ext(y,jsd);}
		v = cs_dt_ext(y, jsd);       //二次曲线外推
		dv= cs_dt_ext(y0,jsd) - t0;  //ye年的二次外推与te的差
		return v - dv*(y0+100-y)/100;
	}
	for(i=0;i<cs_dt_at.len;i+=5) {if(y<d[i+5]) break;}
	t1=(y-d[i])/(d[i+5]-d[i])*10, t2=t1*t1, t3=t2*t1;
	return d[i+1] +d[i+2]*t1 +d[i+3]*t2 +d[i+4]*t3;
}
//传入儒略日(J2000起算),计算TD-UT(单位:日)
MM_EXPORT_DLL cs_real cs_dt_T(cs_real t)
{
	return cs_dt_calc(t/365.2425+2000)/86400.0;
}
//=================================岁差计算=========================================
//==================================================================================

//t是儒略世纪数,sc是岁差量名称,mx是模型
//mx :
// 0 IAU1976
// 1 IAU2000
// 2 P03
//"fi w  P  Q  E  x  pi II p  th Z  z "
// 0  1  2  3  4  5  6  7  8  9  10 11
MM_EXPORT_DLL cs_real cs_prece(cs_real t,unsigned int sc,unsigned int mx)
{
	unsigned int i = 0,n = cs_preceTab_wtab[mx];
	cs_real tn=1,c=0;
	const struct cs_arr1* p = &(cs_preceTab.d[mx]);
	assert(mx<cs_preceTabType_length);
	assert(sc<cs_preceTab_h);
	for(i=0;i<n;i++,tn*=t) {c+=p->d[sc*n+i]*tn;}
	return c/cs_rad;
}
//返回P03黄赤交角,t是世纪数
MM_EXPORT_DLL cs_real cs_hcjj(cs_real t)
{
	cs_real t2=t*t, t3=t2*t,t4=t3*t,t5=t4*t;
	return (84381.4060 -46.836769*t -0.0001831*t2 +0.00200340*t3 -5.76e-7*t4 -4.34e-8*t5)/cs_rad;
}
//=================================岁差旋转=========================================
//==================================================================================
//J2000赤道转Date赤道
MM_EXPORT_DLL struct cs_vector3 cs_CDllr_J2D(cs_real t,const struct cs_vector3* llr,unsigned int mx)
{
	//"fi w  P  Q  E  x  pi II p  th Z  z "
	// 0  1  2  3  4  5  6  7  8  9  10 11
	//Real Z = prece(t,'Z', mx) + llr[0];
	//Real z = prece(t,'z', mx);
	//Real th= prece(t,'th',mx);
	cs_real Z,z,th,cosW,sinW,cosH,sinH,A,B,C;
	struct cs_vector3 r;
	Z = cs_prece(t,cspsc_Z , mx) + llr->x;
	z = cs_prece(t,cspsc_z , mx);
	th= cs_prece(t,cspsc_th,mx);
	cosW = cos(llr->y);cosH = cos(th);
	sinW = sin(llr->y);sinH = sin(th);
	A = cosW*sin(Z);
	B = cosH*cosW*cos(Z) - sinH*sinW;
	C = sinH*cosW*cos(Z) + cosH*sinW;
	r.x = cs_rad2mrad( atan2(A,B)+z );
	r.y = asin(C);
	r.z = llr->z;
	return r;
}
//Date赤道转J2000赤道
MM_EXPORT_DLL struct cs_vector3 cs_CDllr_D2J(cs_real t,const struct cs_vector3* llr,unsigned int mx)
{
	//"fi w  P  Q  E  x  pi II p  th Z  z "
	// 0  1  2  3  4  5  6  7  8  9  10 11
	//Real Z = -prece(t,'z', mx) + llr[0];
	//Real z = -prece(t,'Z', mx);
	//Real th= -prece(t,'th',mx);
	cs_real Z,z,th,cosW,sinW,cosH,sinH,A,B,C;
	struct cs_vector3 r;
	Z = -cs_prece(t,cspsc_z , mx) + llr->x;
	z = -cs_prece(t,cspsc_Z , mx);
	th= -cs_prece(t,cspsc_th,mx);
	cosW = cos(llr->y);cosH = cos(th);
	sinW = sin(llr->y);sinH = sin(th);
	A = cosW*sin(Z);
	B = cosH*cosW*cos(Z) - sinH*sinW;
	C = sinH*cosW*cos(Z) + cosH*sinW;
	r.x = cs_rad2mrad( atan2(A,B)+z );
	r.y = asin(C);
	r.z = llr->z;
	return r;
}
//黄道球面坐标_J2000转Date分点,t为儒略世纪数
//J2000黄道旋转到Date黄道(球面对球面),也可直接由利用球面旋转函数计算,但交角接近为0时精度很低
MM_EXPORT_DLL struct cs_vector3 cs_HDllr_J2D(cs_real t,const struct cs_vector3* llr,unsigned int mx)
{
	struct cs_vector3 r = *llr;
	//"fi w  P  Q  E  x  pi II p  th Z  z "
	// 0  1  2  3  4  5  6  7  8  9  10 11
	//r[0]+=prece(t,'fi',mx); r=llrConv(r, prece(t,'w',mx));
	//r[0]-=prece(t,'x', mx); r=llrConv(r,-prece(t,'E',mx));
	r.x+=cs_prece(t,cspsc_fi,mx); r=cs_llrConv(&r, cs_prece(t,cspsc_w,mx));
	r.x-=cs_prece(t,cspsc_x ,mx); r=cs_llrConv(&r,-cs_prece(t,cspsc_E,mx));
	return r;
}
//黄道球面坐标_Date分点转J2000,t为儒略世纪数
MM_EXPORT_DLL struct cs_vector3 cs_HDllr_D2J(cs_real t,const struct cs_vector3* llr,unsigned int mx)
{
	struct cs_vector3 r = *llr;
	//"fi w  P  Q  E  x  pi II p  th Z  z "
	// 0  1  2  3  4  5  6  7  8  9  10 11
	//r=llrConv(r, prece(t,'E',mx));  r[0]+=prece(t,'x', mx);
	//r=llrConv(r,-prece(t,'w',mx));  r[0]-=prece(t,'fi',mx);
	r=cs_llrConv(&r, cs_prece(t,cspsc_E,mx));  r.x+=cs_prece(t,cspsc_x , mx);
	r=cs_llrConv(&r,-cs_prece(t,cspsc_w,mx));  r.x-=cs_prece(t,cspsc_fi,mx);
	r.x=cs_rad2mrad(r.x);
	return r;
}
//=================================章动计算=========================================
//==================================================================================
//章动计算,t是J2000.0起算的儒略世纪数,zq表示只计算周期大于zq(天)的项
MM_EXPORT_DLL struct cs_vector3 cs_nutation(cs_real t,cs_real zq)
{
	struct cs_vector3 r;
	unsigned int i = 0;
	cs_real t2=t*t,t3=t2*t,t4=t3*t;//t的二、三、四次方
	cs_real dL=0,dE=0;
	cs_real c = 0,q = 0;
	const cs_real* B = cs_nuTab.d;
	cs_real l,l1,F,D,Om;
	l = 485868.249036 + 1717915923.2178*t + 31.8792*t2 + 0.051635*t3 - 0.00024470*t4;
	l1=1287104.79305  +  129596581.0481*t -  0.5532*t2 - 0.000136*t3 - 0.00001149*t4;
	F = 335779.526232 + 1739527262.8478*t - 12.7512*t2 - 0.001037*t3 + 0.00000417*t4;
	D =1072260.70369  + 1602961601.2090*t -  6.3706*t2 + 0.006593*t3 - 0.00003169*t4;
	Om= 450160.398036 -    6962890.5431*t +  7.4722*t2 + 0.007702*t3 - 0.00005939*t4;
	//周期项取和计算
	for(i=0;i<cs_nuTab.len;i+=cs_nuTab_w)
	{ 
		c = (B[i]*l + B[i+1]*l1 + B[i+2]*F +B[i+3]*D +B[i+4]*Om)/cs_rad;
		//只算周期大于zq天的项
		if(zq)
		{ 
			q = 36526*2*cs_PI*cs_rad/(1717915923.2178*B[i] + 
				129596581.0481*B[i+1] + 1739527262.8478*B[i+2] + 
				1602961601.2090*B[i+3] + 6962890.5431*B[i+4]);
			if(q<zq) {continue;}
		}
		dL += (B[i+5]+B[i+6]*t) * sin(c) + B[i+7]  * cos(c);
		dE += (B[i+8]+B[i+9]*t) * cos(c) + B[i+10] * sin(c);
	}
	//返回IAU2000B章动值, dL是黄经章动,dE是交角章动
	r.x = dL/=10000000*cs_rad;
	r.x = dE/=10000000*cs_rad;
	r.x = 0;
	return r;
}
//本函数计算赤经章动及赤纬章动
MM_EXPORT_DLL struct cs_vector3 cs_CDnutation(const struct cs_vector3* z,cs_real E,cs_real dL,cs_real dE)
{
	struct cs_vector3 r = *z;
	r.x += ( cos(E) + sin(E)*sin(z->x)*tan(z->y) )*dL - cos(z->x)*tan(z->y)*dE; //赤经章动
	r.y += sin(E)*cos(z->x)*dL + sin(z->x)*dE; //赤纬章动
	r.x = cs_rad2mrad(r.x);
	return r;
}
//中精度章动计算,t是世纪数
MM_EXPORT_DLL struct cs_vector3 cs_nutation2(cs_real t)
{
	unsigned int i = 0;
	cs_real c = 0,a = 0,t2 = t*t,dL = 0,dE = 0;
	const cs_real* B = cs_nutB.d;
	struct cs_vector3 r;
	for(i=0;i<cs_nutB.len;i+=cs_nutB_w)
	{
		c = B[i]+B[i+1]*t+B[i+2]*t2;
		if(i==0) {a=-1.742*t;} else {a=0;}
		dL+=(B[i+3]+a)*sin(c);
		dE+= B[i+4]   *cos(c);
	}
	//黄经章动,交角章动
	r.x = dL/100/cs_rad;
	r.y = dE/100/cs_rad;
	r.z = 0;
	return r;
}
//只计算黄经章动
MM_EXPORT_DLL cs_real cs_nutationLon2(cs_real t)
{
	unsigned int i = 0;
	cs_real a = 0,t2 = t*t,dL = 0;
	const cs_real* B = cs_nutB.d;
	for(i=0;i<cs_nutB.len;i+=cs_nutB_w)
	{
		if(i==0) {a=-1.742*t;} else {a=0;}
		dL += (B[i+3]+a) * sin( B[i]+B[i+1]*t+B[i+2]*t2 );
	}
	return dL/100/cs_rad;
}
//=================================蒙气改正=========================================
//==================================================================================
//大气折射,h是真高度
MM_EXPORT_DLL cs_real cs_MQC (cs_real h)
{
	return  0.0002967/tan( h +0.003138/( h+0.08919) );
}
//大气折射,ho是视高度
MM_EXPORT_DLL cs_real cs_MQC2(cs_real ho)
{
	return -0.0002909/tan( ho+0.002227/(ho+0.07679) );
}
//=================================视差改正=========================================
//==================================================================================
//视差修正
//z赤道坐标,fa地理纬度,H时角,high海拔(千米)
MM_EXPORT_DLL void cs_parallax( struct cs_vector3* z,cs_real H,cs_real fa,cs_real high)
{
	cs_real r0,x0,y0,z0;
	cs_real f=cs_ba;
	cs_real dw=1,u=0,g=0;
	struct cs_vector3 s;
	if(z->z<500) {dw=cs_AU;}
	z->z*=dw;
	u=atan(f*tan(fa));
	g=z->x+H;
	r0 = cs_rEar*cos(u)  + high*cos(fa); //站点与地地心向径的赤道投影长度
	z0 = cs_rEar*sin(u)*f+ high*sin(fa); //站点与地地心向径的轴向投影长度
	x0 = r0*cos(g);
	y0 = r0*sin(g);

	s = cs_llr2xyz(z);
	s.x-=x0  ; s.y-=y0  ; s.z-=z0;
	s = cs_xyz2llr(&s);
	*z = s;
	z->z =s.z/dw;
}
//////////////////////////////////////////////////////////////////////////
//=================================行星星历=========================================
//==================================================================================
//xt星体,zn坐标号,t儒略世纪数,n计算项数
//转为儒略千年数
MM_EXPORT_DLL cs_real cs_XL0_calc(unsigned int xt,unsigned int zn,cs_real t,cs_real n)
{
	unsigned int i = 0,j = 0;
	cs_real v = 0,tn=1,c = 0;
	const struct cs_arr1* FD=&(cs_XL0.d[xt]);
	const cs_real* F=FD->d;
	cs_real n0 = 0,n1 = 0,n2 = 0,N = 0,N0 = 0;
	unsigned int pn=zn*6+1;
	t/=10; 
	N0 = F[pn+1]-F[pn]; //N0序列总数
	for(i=0;i<6;i++,tn*=t)
	{
		n1=F[pn+i];
		n2=F[pn+1+i];
		n0=n2-n1;
		if(!n0) {continue;}
		if(n<0) {N=n2;}  //确定项数
		else 
		{ 
			N = floor(3*n*n0/N0+0.5)+n1;
			if(i)		{N+=3;}  
			if(N > n2) 	{N=n2;}
		}
		for(j=(unsigned int)(n1),c=0;j<N;j+=3) {c += F[j]*cos(F[j+1] +t*F[j+2]);}
		v += c*tn;
	}
	v/=F[0];
	//地球
	if(xt==0)
	{ 
		cs_real t2=t*t,t3=t2*t;//千年数的各次方
		if(zn==0) {v += (-0.0728 -2.7702*t -1.1019*t2 -0.0996*t3) / cs_rad;}
		if(zn==1) {v += (+0.0000 +0.0004*t +0.0004*t2 -0.0026*t3) / cs_rad;}
		if(zn==2) {v += (-0.0020 +0.0044*t +0.0213*t2 -0.0250*t3) / 1000000;}
	}
	else
	{ 
		//其它行星
		cs_real dv = cs_XL0_xzb.d[ (xt-1)*3+zn ];
		if(zn==0) {v += -3*t/cs_rad;}
		if(zn==2) {v += dv/1000000;}
		else      {v += dv/cs_rad;}
	}
	return v;
}
//返回冥王星J2000直角坐标
MM_EXPORT_DLL struct cs_vector3 cs_pluto_coord(cs_real t)
{
	struct cs_vector3 r;
	unsigned int i = 0,j = 0;
	cs_real c0=cs_PI/180/100000;
	cs_real x = -1+2*(t*36525+1825394.5)/2185000;
	cs_real T = t/100000000;
	for(i=0;i<cs_XL0Pluto.len;i++)
	{
		const struct cs_arr1* ob=&(cs_XL0Pluto.d[i]);
		size_t N = ob->len;
		cs_real v = 0;
		cs_real* rref = 0;
		for(j=0;j<N;j+=3) {v += ob->d[j] * sin( ob->d[j+1]*T + ob->d[j+2]*c0 );}
		if(i%3==1) {v*=x;}
		if(i%3==2) {v*=x*x;}
		//r[ floor(i/3) ] += v/100000000;
		rref = cs_vector3_at(&r,(unsigned int)(floor(i/3)));
		*rref += v/100000000;
	}
	r.x +=  9.922274 + 0.154154*x;
	r.y += 10.016090 + 0.064073*x;
	r.z += -3.947474 - 0.042746*x;
	return r;
}
//xt星体,T儒略世纪数,TD
MM_EXPORT_DLL struct cs_vector3 cs_p_coord(int xt,cs_real t,cs_real n1,cs_real n2,cs_real n3)
{
	struct cs_vector3 z;
    // init vector3 z.
    z.x=0;z.y=0;z.z=0;
	if(xt<8)
	{
		z.x=cs_XL0_calc(xt,0, t,n1);
		z.y=cs_XL0_calc(xt,1, t,n2);
		z.z=cs_XL0_calc(xt,2, t,n3);
	}
	if(xt==8)
	{ 
		//冥王星
		z = cs_pluto_coord(t);
		z = cs_xyz2llr(&z);
		//z = HDllr_J2D(t,z,'P03');
		z = cs_HDllr_J2D(t,&z,cspmx_P03);
	}
	if(xt==9)
	{ 
		//太阳
		z.x=0;z.y=0;z.z=0;
	}
	return z;
}
//返回地球坐标,t为世纪数
MM_EXPORT_DLL struct cs_vector3 cs_e_coord(cs_real t,cs_real n1,cs_real n2,cs_real n3)
{
	struct cs_vector3 re;
	re.x= cs_XL0_calc( 0,0, t, n1);
	re.y= cs_XL0_calc( 0,1, t, n2);
	re.z= cs_XL0_calc( 0,2, t, n3);
	return re;
}
//=================================月亮星历--=======================================
//==================================================================================
//计算月亮
MM_EXPORT_DLL cs_real cs_XL1_calc(unsigned int zn,cs_real t,cs_real n)
{
	const struct cs_arr2* ob=&(cs_XL1.d[zn]);
	unsigned int i = 0,j = 0;
	cs_real N = 0,v = 0,tn = 1,c = 0;
	cs_real t2=t*t,t3=t2*t,t4=t3*t,t5=t4*t;
	cs_real tx=t-10;
	const struct cs_arr1* obpd = &(ob->d[0]);
	if(zn==0)
	{
		//月球平黄经(弧度)
		v += (3.81034409 + 8399.684730072*t -3.319e-05*t2 + 3.11e-08*t3 - 2.033e-10*t4)*cs_rad;
		//岁差(角秒)
		v += 5028.792262*t + 1.1124406*t2 + 0.00007699*t3 - 0.000023479*t4 -0.0000000178*t5;  
		//对公元3000年至公元5000年的拟合,最大误差小于10角秒
		if(tx>0) {v += -0.866 +1.43*tx +0.054*tx*tx;} 
	}
	t2/=1e4;
	t3/=1e8;
	t4/=1e8;
	n*=6; if(n<0) {n = obpd->len;}
	for(i=0;i<ob->len;i++,tn*=t)
	{
		const struct cs_arr1* F = &(ob->d[i]);
		N = floor( n*F->len/obpd->len+0.5 );  if(i) {N+=6;}  if(N >= F->len) {N=F->len;}
		for(j=0,c=0;j<N;j+=6) {c+=F->d[j]*cos(F->d[j+1] +t*F->d[j+2] +t2*F->d[j+3] +t3*F->d[j+4] +t4*F->d[j+5]);}
		v += c*tn;
	}
	if(zn!=2) {v/=cs_rad;}
	return v;
}
//返回月球坐标,n1,n2,n3为各坐标所取的项数
MM_EXPORT_DLL struct cs_vector3 cs_m_coord(cs_real t,cs_real n1,cs_real n2,cs_real n3)
{
	struct cs_vector3 re;
	re.x = cs_XL1_calc( 0, t, n1 );
	re.y = cs_XL1_calc( 1, t, n2 );
	re.z = cs_XL1_calc( 2, t, n3 );
	return re;
}
//=============================一些天文基本问题=====================================
//==================================================================================
//返回朔日的编号,jd应在朔日附近，允许误差数天
MM_EXPORT_DLL cs_real cs_suoN(cs_real jd)
{
	return floor( (jd+8)/29.5306 );
}
//太阳光行差,t是世纪数
MM_EXPORT_DLL cs_real cs_gxc_sunLon(cs_real t)
{
	cs_real v =-0.043126+ 628.301955*t -0.000002732*t*t; //平近点角
	cs_real e = 0.016708634-0.000042037*t-0.0000001267*t*t;
	return  ( -20.49552 * (1+e*cos(v)) )/cs_rad; //黄经光行差
}
//黄纬光行差
MM_EXPORT_DLL cs_real cs_gxc_sunLat(cs_real t)
{
	return 0;
}
//月球经度光行差,误差0.07"
MM_EXPORT_DLL cs_real cs_gxc_moonLon(cs_real t)
{
	return -3.4E-6;
}
//月球纬度光行差,误差0.006"
MM_EXPORT_DLL cs_real cs_gxc_moonLat(cs_real t)
{
	return 0.063*sin(0.057+8433.4662*t+0.000064*t*t)/cs_rad;
}
//传入T是2000年首起算的日数(UT),dt是deltatT(日),精度要求不高时dt可取值为0
//返回格林尼治平恒星时(不含赤经章动及非多项式部分),即格林尼治子午圈的平春风点起算的赤经
MM_EXPORT_DLL cs_real cs_pGST(cs_real T,cs_real dt)
{
	cs_real t=(T+dt)/36525;
	cs_real t2=t*t,t3=t2*t,t4=t3*t;
	return cs_pi2*(0.7790572732640 + 1.00273781191135448*T) //T是UT,下一行的t是力学时(世纪数)
		+ (0.014506 + 4612.15739966*t + 1.39667721*t2 - 0.00009344*t3 + 0.00001882*t4)/cs_rad;
}
//传入力学时J2000起算日数，返回平恒星时
MM_EXPORT_DLL cs_real cs_pGST2(cs_real jd)
{
	cs_real dt=cs_dt_T(jd);
	return cs_pGST(jd-dt,dt);
}
//太阳升降计算。jd儒略日(须接近L当地平午UT)，L地理经度，fa地理纬度，sj=-1升,sj=1降
MM_EXPORT_DLL cs_real cs_sunShengJ(cs_real jd,cs_real L,cs_real fa,cs_real sj)
{
	unsigned int i = 0;
	jd=floor(jd+0.5)-L/cs_pi2;
	for(i=0;i<2;i++)
	{
		cs_real T=jd/36525;
		cs_real E=(84381.4060 -46.836769*T)/cs_rad; //黄赤交角
		cs_real t = T+(32*(T+1.8)*(T+1.8)-20)/86400/36525;  //儒略世纪年数,力学时
		cs_real J = (
			48950621.66 + 6283319653.318*t + 53*t*t - 994 +
			334166 * cos( 4.669257+  628.307585*t) + 
			3489   * cos( 4.6261  + 1256.61517*t ) +
			2060.6 * cos( 2.67823 +  628.307585*t) * t)/10000000;
		cs_real sinJ=sin(J);cs_real cosJ=cos(J); //太阳黄经以及它的正余弦值
		//恒星时(子午圈位置)
		cs_real gst = 
			(0.7790572732640 + 1.00273781191135448*jd)*cs_pi2 + 
			(0.014506 + 4612.15739966*T + 1.39667721*T*T)/cs_rad; 
		cs_real A = atan2( sinJ*cos(E), cosJ );  //太阳赤经
		cs_real D = asin ( sin(E)*sinJ );        //太阳赤纬
		cs_real cosH0 = (sin(-50*60/cs_rad) - sin(fa)*sin(D) ) / ( cos(fa)*cos(D) );
		if(fabs(cosH0)>=1) {return 0;} //太阳在地平线上的cos(时角)计算
		jd += cs_rad2rrad(sj*acos(cosH0) - (gst+L-A) )/6.28; //(升降时角-太阳时角)/太阳速度
	}
	return jd; //反回格林尼治UT
}
//时差计算(高精度),t力学时儒略世纪数
MM_EXPORT_DLL cs_real cs_pty_zty(cs_real t)
{
	struct cs_vector3 z;
	cs_real E,dE,dL;
	cs_real t2=t*t,t3=t2*t,t4=t3*t,t5=t4*t;
	cs_real L = ( 1753470142 + 628331965331.8*t + 5296.74*t2 + 0.432*t3 - 0.1124*t4 - 0.00009*t5 )/1000000000 + cs_PI -20.5/cs_rad;

	//Real f;
	dL= -17.2*sin(2.1824-33.75705*t)/cs_rad; //黄经章
	dE=   9.2*cos(2.1824-33.75705*t)/cs_rad; //交角章
	E = cs_hcjj(t)+dE; //真黄赤交角

	//地球坐标
	z.x=cs_XL0_calc(0,0,t,50)+cs_PI+cs_gxc_sunLon(t)+dL;
	z.y=-( 2796*cos(3.1987+8433.46616*t) +1016*cos(5.4225+550.75532*t) +804*cos(3.88  +522.3694*t) )/1000000000;

	z = cs_llrConv( &z, E ); //z太阳地心赤道坐标
	z.x-=dL*cos(E);

	L = cs_rad2rrad(L-z.x);
	return L/cs_pi2; //单位是周(天)
}
//时差计算(低精度),误差约在1秒以内,t力学时儒略世纪数
MM_EXPORT_DLL cs_real cs_pty_zty2(cs_real t)
{
	struct cs_vector3 z;
	cs_real L = ( 1753470142 + 628331965331.8*t + 5296.74*t*t )/1000000000 + cs_PI;
	cs_real E = (84381.4088 -46.836051*t)/cs_rad;
	z.x=cs_XL0_calc(0,0,t,5)+cs_PI, z.y=0; //地球坐标
	z = cs_llrConv( &z, E ); //z太阳地心赤道坐标
	L = cs_rad2rrad(L-z.x);
	return L/cs_pi2; //单位是周(天)
}
