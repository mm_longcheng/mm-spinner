﻿#ifndef __mm_calendar_XL_h__
#define __mm_calendar_XL_h__
#include "core/mm_platform.h"
#include "core/mm_prefix.h"
#include "calendar/mm_calendar_type.h"
#include "calendar/mm_calendar_vector.h"

//物件XL : 日月黄道平分点坐标、视坐标、速度、已知经度反求时间等方面的计算
//日月星历基本函数类
//=====================
//星历函数(日月球面坐标计算)
//地球经度计算,返回Date分点黄经,传入世纪数、取项数
MM_EXPORT_DLL cs_real cs_XL_E_Lon(cs_real t,cs_real n);
//月球经度计算,返回Date分点黄经,传入世纪数,n是项数比例
MM_EXPORT_DLL cs_real cs_XL_M_Lon(cs_real t,cs_real n);   
//=========================
//地球速度,t是世纪数,误差小于万分3
MM_EXPORT_DLL cs_real cs_XL_E_v(cs_real t);
//月球速度计算,传入世经数
MM_EXPORT_DLL cs_real cs_XL_M_v(cs_real t);
//=========================
//月日视黄经的差值
MM_EXPORT_DLL cs_real cs_XL_MS_aLon(cs_real t,cs_real Mn,cs_real Sn);
//太阳视黄经
//busy!!
MM_EXPORT_DLL cs_real cs_XL_S_aLon(cs_real t,cs_real n);
//=========================
//已知地球真黄经求时间
MM_EXPORT_DLL cs_real cs_XL_E_Lon_t(cs_real W);
//已知真月球黄经求时间
MM_EXPORT_DLL cs_real cs_XL_M_Lon_t(cs_real W);
//已知月日视黄经差求时间
MM_EXPORT_DLL cs_real cs_XL_MS_aLon_t(cs_real W);
//已知太阳视黄经反求时间
MM_EXPORT_DLL cs_real cs_XL_S_aLon_t(cs_real W);
//已知月日视黄经差求时间,高速低精度,误差不超过40秒
MM_EXPORT_DLL cs_real cs_XL_MS_aLon_t1(cs_real W);
//已知太阳视黄经反求时间,高速低精度,最大误差不超过50秒,平均误差15秒
MM_EXPORT_DLL cs_real cs_XL_S_aLon_t1(cs_real W);
//已知月日视黄经差求时间,高速低精度,误差不超过600秒(只验算了几千年)
MM_EXPORT_DLL cs_real cs_XL_MS_aLon_t2(cs_real W);
//已知太阳视黄经反求时间,高速低精度,最大误差不超过600秒
MM_EXPORT_DLL cs_real cs_XL_S_aLon_t2(cs_real W);
//月亮被照亮部分的比例
MM_EXPORT_DLL cs_real cs_XL_moonIll(cs_real t);
//转入地平纬度及地月质心距离,返回站心视半径(角秒)
MM_EXPORT_DLL cs_real cs_XL_moonRad(cs_real r,cs_real h);
//求月亮近点时间和距离,t为儒略世纪数力学时
MM_EXPORT_DLL struct cs_vector3 cs_XL_moonMinR(cs_real t,cs_real _min);
//月亮升交点
MM_EXPORT_DLL struct cs_vector3 cs_XL_moonNode(cs_real t,cs_real asc);
//地球近远点
MM_EXPORT_DLL struct cs_vector3 cs_XL_earthMinR(cs_real t,cs_real _min);

#include "core/mm_suffix.h"

#endif//__mm_calendar_XL_h__