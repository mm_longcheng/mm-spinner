﻿#include "calendar/mm_calendar_i2s.h"
const char* cs_xx_name_[] = 
{
	"地球","水星","金星","火星","木星","土星","天王星","海王星","冥王星",
};
MM_EXPORT_DLL const struct cs_arrs cs_xx_name = {cs_array_size(cs_xx_name_),cs_xx_name_,};
// 星期
const char* cs_Week_[]=
{
	"日","一","二","三","四","五","六","七",
};
MM_EXPORT_DLL const struct cs_arrs cs_Week = {cs_array_size(cs_Week_),cs_Week_,};
// 数字
const char* cs_Number_[]=
{
	"零","一","二","三","四","五","六","七","八","九","十",
};
MM_EXPORT_DLL const struct cs_arrs cs_Number = {cs_array_size(cs_Number_),cs_Number_,};
// 干
const char* cs_Gan_[]=
{
	"甲","乙","丙","丁","戊","己","庚","辛","壬","癸",
};
MM_EXPORT_DLL const struct cs_arrs cs_Gan = {cs_array_size(cs_Gan_),cs_Gan_,};
// 支
const char* cs_Zhi_[]=
{
	"子","丑","寅","卯","辰","巳","午","未","申","酉","戌","亥",
};
MM_EXPORT_DLL const struct cs_arrs cs_Zhi = {cs_array_size(cs_Zhi_),cs_Zhi_,};
// 生肖
const char* cs_ShX_[]=
{
	"鼠","牛","虎","兔","龙","蛇","马","羊","猴","鸡","狗","猪",
};
MM_EXPORT_DLL const struct cs_arrs cs_ShX = {cs_array_size(cs_ShX_),cs_ShX_,};
// 星座
const char* cs_XiZ_[]=
{
	"摩羯","水瓶","双鱼","白羊","金牛","双子","巨蟹","狮子","处女","天秤","天蝎","射手",
};
MM_EXPORT_DLL const struct cs_arrs cs_XiZ = {cs_array_size(cs_XiZ_),cs_XiZ_,};
// 月相名称表
const char* cs_yxmc_[]=
{
	"朔","上弦","望","下弦",
};
MM_EXPORT_DLL const struct cs_arrs cs_yxmc = {cs_array_size(cs_yxmc_),cs_yxmc_,};
// 节气名称表
const char* cs_jqmc_[]=
{
	"冬至","小寒","大寒","立春","雨水","惊蛰","春分","清明","谷雨","立夏","小满","芒种",
	"夏至","小暑","大暑","立秋","处暑","白露","秋分","寒露","霜降","立冬","小雪","大雪",
};
MM_EXPORT_DLL const struct cs_arrs cs_jqmc = {cs_array_size(cs_jqmc_),cs_jqmc_,};
// 月名称,建寅
const char* cs_ymc_[]=
{
	"十一","十二","正","二","三","四","五","六","七","八","九","十",
};
MM_EXPORT_DLL const struct cs_arrs cs_ymc = {cs_array_size(cs_ymc_),cs_ymc_,};
// 日名称,初一
const char* cs_rmc_[]=
{
	"初一","初二","初三","初四","初五","初六","初七","初八","初九","初十",
	"十一","十二","十三","十四","十五","十六","十七","十八","十九","二十",
	"廿一","廿二","廿三","廿四","廿五","廿六","廿七","廿八","廿九","三十",
	"卅一",
};
MM_EXPORT_DLL const struct cs_arrs cs_rmc = {cs_array_size(cs_rmc_),cs_rmc_,};