﻿#ifndef __mm_calendar_vector_h__
#define __mm_calendar_vector_h__
#include "core/mm_platform.h"
#include "core/mm_prefix.h"
#include "calendar/mm_calendar_type.h"

struct cs_vector3
{
	cs_real x;
	cs_real y;
	cs_real z;
};
MM_INLINE static cs_real* cs_vector3_at(struct cs_vector3* p,unsigned int i)
{
	return &p->x+i;
}
struct cs_vector4
{
	cs_real x;
	cs_real y;
	cs_real z;
	cs_real w;
};
// cs_real array.
struct cs_arr1
{
	size_t len;
	const cs_real* d;
};
// struct cs_arr1 array.
struct cs_arr2
{
	size_t len;
	const struct cs_arr1* d;
};
// struct cs_arr2 array.
struct cs_arr3
{
	size_t len;
	const struct cs_arr2* d;
};
// const char* array.
struct cs_arrs
{
	size_t len;
	const char** d;
};
// const int array.
struct cs_arri
{
	size_t len;
	const int* d;
};
//get count from array like int arr[].
#define cs_array_size( a )  ( sizeof( (a) ) / sizeof( (a[0]) ) )

#include "core/mm_suffix.h"

#endif//__mm_calendar_vector_h__