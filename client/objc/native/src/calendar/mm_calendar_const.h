﻿#ifndef __mm_calendar_const_h__
#define __mm_calendar_const_h__
#include "core/mm_platform.h"
#include "core/mm_prefix.h"
#include "calendar/mm_calendar_type.h"
#define _USE_MATH_DEFINES
#include <math.h>

// const define.
#define cs_PI    M_PI             //pi
#define cs_rEar  6378.1366        //地球赤道半径(千米)
#define cs_ba    0.99664719       //地球极赤半径比
#define cs_AU    1.49597870691e8  //天文单位长度(千米)
#define cs_GS    299792.458       //光速(行米/秒)
#define cs_J2000 2451545          //2000年的儒略日
#define cs_k     0.2725076        //月亮与地球的半径比(用于半影计算)
#define cs_k2    0.2722810        //月亮与地球的半径比(用于本影计算)
#define cs_k0    109.1222         //太阳与地球的半径比(对应959.64)
#define cs_sSun  959.64           //用于太阳视半径计算
// algorithm const.
static const cs_real cs_R2D		= 180/cs_PI;			 //- 180/pi Radian to Degrees
static const cs_real cs_D2R		= cs_PI/180;			 //- pi/180 Degrees to Radian
//=================================天文常数=========================================
//==================================================================================
static const cs_real cs_rEarA= 0.99834*cs_rEar; //平均半径
static const cs_real cs_ba2 = cs_ba*cs_ba; //地球极赤半径比的平方
static const cs_real cs_sinP = cs_rEar/cs_AU;   //sin(太阳视差)
static const cs_real cs_Agx = cs_AU/cs_GS/86400/36525; //每天文单位的光行时间(儒略世纪)
static const cs_real cs_xxHH[8]={116,584,780,399,378,370,367,367,}; //行星会合周期
static const cs_real cs_rad  = 180*3600/cs_PI; //每弧度的角秒数
static const cs_real cs_radd = 180/cs_PI; //每弧度的度数
static const cs_real cs_pi2  = cs_PI*2;
static const cs_real cs_pi_2 = cs_PI/2;
// cs_sMoon = cs_k*cs_rEar*1.0000036*rad
static const cs_real cs_sMoon = cs_k*cs_rEar*1.0000036*(180*3600/cs_PI);  //用于月亮视半径计算
// cs_sMoon2= cs_k2*cs_rEar*1.0000036*rad
static const cs_real cs_sMoon2= cs_k2*cs_rEar*1.0000036*(180*3600/cs_PI); //用于月亮视半径计算

#include "core/mm_suffix.h"

#endif//__mm_calendar_const_h__