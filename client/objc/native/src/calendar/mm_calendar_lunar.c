﻿#include "calendar/mm_calendar_lunar.h"
#include "calendar/mm_calendar_eph0.h"
#include "calendar/mm_calendar_XL.h"
#include <math.h>

//命理八字计算。jd为格林尼治UT(J2000起算),J为本地经度,返回在物件ob中
MM_EXPORT_DLL void cs_lunar_mingLiBaZi(cs_real jd,cs_real J,struct cs_lunar_gzsz* re)
{ 
	int v = 0;
	cs_real D,SC;
	cs_real jd2 = jd + cs_dt_T(jd); //力学时
	cs_real w = cs_XL_S_aLon( jd2/36525.0, -1 ); //此刻太阳视黄经
	cs_real k = floor( (w/cs_pi2*360+45+15*360)/30 ); //1984年立春起算的节气数(不含中气)
	jd += cs_pty_zty2(jd2/36525.0)+J/cs_PI/2; //本地真太阳时(使用低精度算法计算时差)
	re->gz_zty = jd;

	jd += 13.0/24; //转为前一日23点起算(原jd为本日中午12点起算)
	D = floor(jd);SC = floor( (jd-D)*12 ); //日数与时辰

	v = (int)floor(k/12+6000000)   ; re->gz_jn.g = v%10;re->gz_jn.z = v%12;
	v = (int)(k+2+60000000)        ; re->gz_jy.g = v%10;re->gz_jy.z = v%12;
	v = (int)(D - 6 + 9000000)     ; re->gz_jr.g = v%10;re->gz_jr.z = v%12;
	v = (int)((D-1)*12+90000000+SC); re->gz_js.g = v%10;re->gz_js.z = v%12;
}
//回历计算,J2000.0起算的儒略日数
MM_EXPORT_DLL void cs_lunar_getHuiLi(cs_real d0,struct cs_lunar_nyr* re)
{ 
	//以下算法使用Excel测试得到,测试时主要关心年临界与月临界
	cs_real z,y,m,d;
	d = d0 + 503105;          z = floor( d/10631 );       //10631为一周期(30年)
	d -= z*10631;             y = floor((d+0.5)/354.366); //加0.5的作用是保证闰年正确(一周中的闰年是第2,5,7,10,13,16,18,21,24,26,29年)
	d -= floor(y*354.366+0.5);m = floor((d+0.11)/29.51);  //分子加0.11,分母加0.01的作用是第354或355天的的月分保持为12月(m=11)
	d -= floor(m*29.5+0.5);
	re->y = (int)(z*30+y+1);
	re->m = (int)(m+1);
	re->d = (int)(d+1);
}
//精气
MM_EXPORT_DLL cs_real cs_lunar_qi_accurate(cs_real W)
{ 
	cs_real t=cs_XL_S_aLon_t(W)*36525;  return t - cs_dt_T(t) + 8/24; 
}
//精朔
MM_EXPORT_DLL cs_real cs_lunar_so_accurate(cs_real W)
{ 
	cs_real t=cs_XL_MS_aLon_t(W)*36525; return t - cs_dt_T(t) + 8/24; 
}
//精气
MM_EXPORT_DLL cs_real cs_lunar_qi_accurate2(cs_real jd)
{ 
	cs_real d = cs_PI/12;
	cs_real w= floor((jd+293)/365.2422*24) * d;
	cs_real a= cs_lunar_qi_accurate ( w );
	if(a-jd>5 ) return cs_lunar_qi_accurate ( w-d );
	if(a-jd<-5) return cs_lunar_qi_accurate ( w+d );
	return a;
}
//精朔
MM_EXPORT_DLL cs_real cs_lunar_so_accurate2(cs_real jd)
{ 
	return cs_lunar_so_accurate ( floor((jd+8)/29.5306) * cs_PI*2 );
}