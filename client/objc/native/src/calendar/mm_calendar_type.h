﻿#ifndef __mm_calendar_type_h__
#define __mm_calendar_type_h__
#include "core/mm_platform.h"
#include "core/mm_prefix.h"
#include <stddef.h>
// calendar system.
typedef double cs_real;
// 经纬
struct cs_jw
{
	cs_real j;    // 经(时分)
	cs_real w;    // 纬(度分)
};

#include "core/mm_suffix.h"

#endif//__mm_calendar_type_h__