﻿#include "calendar/mm_calendar_date.h"
#include <math.h>

MM_EXPORT_DLL void cs_date_init(struct cs_date* p)
{
	p->Y = 2000;
	p->M = 1;
	p->D = 1;
	p->h = 12;
	p->m = 0;
	p->s = 0;
}
MM_EXPORT_DLL void cs_date_init_arg(struct cs_date* p,cs_real _Y,cs_real _M,cs_real _D,cs_real _h,cs_real _m,cs_real _s)
{
	p->Y = _Y;
	p->M = _M;
	p->D = _D;
	p->h = _h;
	p->m = _m;
	p->s = _s;
}
//公历转儒略日
MM_EXPORT_DLL cs_real cs_date_toJD(struct cs_date* p)
{
	return cs_date_JD( p->Y, p->M, p->D+((p->s/60+p->m)/60+p->h)/24 );
}
//儒略日数转公历
MM_EXPORT_DLL void cs_date_setFromJD(struct cs_date* p,cs_real jd)
{
	cs_real c = 0;  //取得日数的整数部份A及小数部分F
	cs_real D,F;
	D=floor(jd+0.5);
	F=jd+0.5-D;
	if(D>=2299161) {c=floor((D-1867216.25)/36524.25);D+=1+c-floor(c/4);}
	D += 1524;               p->Y = floor((D-122.1)/365.25);//年数
	D -= floor(365.25*p->Y); p->M = floor(D/30.601); //月数
	D -= floor(30.601*p->M); p->D = D; //日数
	if(p->M>13){p->M -= 13; p->Y -= 4715;}
	else       {p->M -= 1 ; p->Y -= 4716;}
	//日的小数转为时分秒
	F*=24; p->h=floor(F); F-=p->h;
	F*=60; p->m=floor(F); F-=p->m;
	F*=60; p->s=F;
}
//日期转为串
MM_EXPORT_DLL void cs_date_DD2str(struct cs_date* p,struct mm_string* str)
{
	//[ 2000-01-01 12:00:00]
	mm_string_clear(str);
	mm_string_sprintf(str,"%5d-%02d-%02d %02d:%02d:%02d"
		,(int)p->Y,(int)p->M,(int)p->D
		,(int)p->h,(int)p->m,(int)p->s);
}
//////////////////////////////////////////////////////////////////////////
//提取jd中的时间(去除日期)
MM_EXPORT_DLL void cs_date_timeStr(cs_real jd,struct mm_string* str)
{
	//[12:00:00]
	cs_real h,m,s;
	jd+=0.5; jd = (jd - floor(jd));
	s=floor(jd*86400+0.5);
	h=floor(s/3600); s-=h*3600;
	m=floor(s/60);   s-=m*60;
	mm_string_clear(str);
	mm_string_sprintf(str,"%02d:%02d:%02d",(int)h,(int)m,(int)s);
}
//星期计算
MM_EXPORT_DLL int cs_date_getWeek(cs_real jd)
{
	return ((int)(floor(jd+1.5+7000000))) % 7;
}
//求y年m月的第n个星期w的儒略日数
MM_EXPORT_DLL cs_real cs_date_nnweek(cs_real y,cs_real m,cs_real n,cs_real w)
{
	cs_real jd,w0,r;
	jd = cs_date_JD(y,m,1.5); //月首儒略日
	w0 = ((int)(floor(jd+1+7000000))) % 7; //月首的星期
	r  = jd-w0+7*n+w;    //jd-w0+7*n是和n个星期0,起算下本月第一行的星期日(可能落在上一月)。加w后为第n个星期w
	if(w>=w0) r-=7; //第1个星期w可能落在上个月,造成多算1周,所以考虑减1周
	if(n==5)
	{
		m++; if(m>12) {m=1; y++;}   //下个月
		if(r>=cs_date_JD(y,m,1.5)) {r-=7;} //r跑到下个月则减1周
	}
	return r;
}
//公历转儒略日
MM_EXPORT_DLL cs_real cs_date_JD(cs_real y,cs_real m,cs_real d)
{
	cs_real n=0,G=0;
	//判断是否为格里高利历日1582*372+10*31+15
	if(y*372+m*31+floor(d)>=588829) {G=1; }
	if(m<=2) {m+=12; y--;}
	//加百年闰
	if(G) {n=floor(y/100); n=2-n+floor(n/4);} 
	return floor(365.25*(y+4716)) + floor(30.6001*(m+1))+d+n - 1524.5;
}
//JD转为串
MM_EXPORT_DLL void cs_date_JD2str(cs_real jd,struct mm_string* str)
{
	struct cs_date r;
	cs_date_setFromJD(&r,jd);
	cs_date_DD2str( &r, str );
}
