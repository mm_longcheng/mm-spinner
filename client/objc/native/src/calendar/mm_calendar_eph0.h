﻿#ifndef __mm_calendar_eph0_h__
#define __mm_calendar_eph0_h__
#include "core/mm_platform.h"
#include "core/mm_prefix.h"
#include "calendar/mm_calendar_const.h"
#include "calendar/mm_calendar_vector.h"
#include "calendar/mm_calendar_eph0_data.h"
//==================================================================================
//经纬转(时分秒/度分秒)
MM_EXPORT_DLL struct cs_vector3 cs_jw2hdms(const struct cs_jw* v);
//时分秒转弧度
MM_EXPORT_DLL cs_real cs_hms2rad(const struct cs_vector3* v);
//度分秒转弧度
MM_EXPORT_DLL cs_real cs_dms2rad(const struct cs_vector3* v);
//=================================数学工具=========================================
//==================================================================================
//对超过0-2PI的角度转为0-2PI
MM_EXPORT_DLL cs_real cs_rad2mrad(cs_real v);
//对超过-PI到PI的角度转为-PI到PI
MM_EXPORT_DLL cs_real cs_rad2rrad(cs_real v);
//临界余数(a与最近的整倍数b相差的距离)
MM_EXPORT_DLL cs_real cs_mod2(cs_real a,cs_real b);
//球面转直角坐标
MM_EXPORT_DLL struct cs_vector3 cs_llr2xyz(const struct cs_vector3* JW);
//直角坐标转球
MM_EXPORT_DLL struct cs_vector3 cs_xyz2llr(const struct cs_vector3* xyz);
//球面坐标旋转
MM_EXPORT_DLL struct cs_vector3 cs_llrConv(const struct cs_vector3* JW,cs_real E);
//赤道坐标转为地平坐标
//转到相对于地平赤道分点的赤道坐标
MM_EXPORT_DLL struct cs_vector3 cs_CD2DP(const struct cs_vector3* z,cs_real L,cs_real fa,cs_real gst);
//求角度差
MM_EXPORT_DLL cs_real cs_j1_j2(cs_real J1,cs_real W1,cs_real J2,cs_real W2);
//日心球面转地心球面,Z星体球面坐标,A地球球面坐标
//本含数是通用的球面坐标中心平移函数,行星计算中将反复使用
MM_EXPORT_DLL struct cs_vector3 cs_h2g(struct cs_vector3 z,struct cs_vector3 a);
//视差角(不是视差)
MM_EXPORT_DLL cs_real cs_shiChaJ(cs_real gst,cs_real L,cs_real fa,cs_real J,cs_real W);
//=================================deltat T计算=====================================
//==================================================================================
//二次曲线外推
MM_EXPORT_DLL cs_real cs_dt_ext(cs_real y,cs_real jsd); 
//计算世界时与原子时之差,传入年
MM_EXPORT_DLL cs_real cs_dt_calc(cs_real y);
//传入儒略日(J2000起算),计算TD-UT(单位:日)
MM_EXPORT_DLL cs_real cs_dt_T(cs_real t);  
//=================================岁差计算=========================================
//==================================================================================
enum cs_prece_mx
{
	cspmx_IAU1976 = 0,
	cspmx_IAU2000 = 1,
	cspmx_P03     = 2,
};
enum cs_prece_sc
{
	cspsc_fi  = 0,
	cspsc_w   = 1,
	cspsc_P   = 2,
	cspsc_Q   = 3,
	cspsc_E   = 4,
	cspsc_x   = 5,
	cspsc_pi  = 6,
	cspsc_II  = 7,
	cspsc_p   = 8,
	cspsc_th  = 9,
	cspsc_Z   = 10,
	cspsc_z   = 11,
};
//t是儒略世纪数,sc是岁差量名称,mx是模型
//mx :
// 0 IAU1976
// 1 IAU2000
// 2 P03
//"fi w  P  Q  E  x  pi II p  th Z  z "
// 0  1  2  3  4  5  6  7  8  9  10 11
MM_EXPORT_DLL cs_real cs_prece(cs_real t,unsigned int sc,unsigned int mx);
//返回P03黄赤交角,t是世纪数
MM_EXPORT_DLL cs_real cs_hcjj(cs_real t);
//=================================岁差旋转=========================================
//==================================================================================
//J2000赤道转Date赤道
MM_EXPORT_DLL struct cs_vector3 cs_CDllr_J2D(cs_real t,const struct cs_vector3* llr,unsigned int mx);
//Date赤道转J2000赤道
MM_EXPORT_DLL struct cs_vector3 cs_CDllr_D2J(cs_real t,const struct cs_vector3* llr,unsigned int mx);
//黄道球面坐标_J2000转Date分点,t为儒略世纪数
//J2000黄道旋转到Date黄道(球面对球面),也可直接由利用球面旋转函数计算,但交角接近为0时精度很低
MM_EXPORT_DLL struct cs_vector3 cs_HDllr_J2D(cs_real t,const struct cs_vector3* llr,unsigned int mx);
//黄道球面坐标_Date分点转J2000,t为儒略世纪数
MM_EXPORT_DLL struct cs_vector3 cs_HDllr_D2J(cs_real t,const struct cs_vector3* llr,unsigned int mx);
//=================================章动计算=========================================
//==================================================================================
//章动计算,t是J2000.0起算的儒略世纪数,zq表示只计算周期大于zq(天)的项
MM_EXPORT_DLL struct cs_vector3 cs_nutation(cs_real t,cs_real zq);
//本函数计算赤经章动及赤纬章动
MM_EXPORT_DLL struct cs_vector3 cs_CDnutation(const struct cs_vector3* z,cs_real E,cs_real dL,cs_real dE);

//中精度章动计算,t是世纪数
MM_EXPORT_DLL struct cs_vector3 cs_nutation2(cs_real t);
//只计算黄经章动
MM_EXPORT_DLL cs_real cs_nutationLon2(cs_real t);
//=================================蒙气改正=========================================
//==================================================================================
//大气折射,h是真高度
MM_EXPORT_DLL cs_real cs_MQC (cs_real h); 
//大气折射,ho是视高度
MM_EXPORT_DLL cs_real cs_MQC2(cs_real ho); 
//=================================视差改正=========================================
//==================================================================================
//视差修正
//z赤道坐标,fa地理纬度,H时角,high海拔(千米)
MM_EXPORT_DLL void cs_parallax( struct cs_vector3* z,cs_real H,cs_real fa,cs_real high);
//=================================星历数据=========================================
//==================================================================================

/********************************
月亮星历数据表
********************************/
//以下是月球黄经周期项及泊松项,精度0.5角秒,平均误差0.8角秒,近期精度更高一些
/******
1.黄经精度(角秒):0.5
2.黄纬精度(角秒):0.5
3.距离精度(千米):1
4.J2000.0时间最大偏移(世纪数):30
5.小数点增补位数:3

误差检测
1.测试点数N；2<N<10001，如500 :2300
2.偏移量rnd；0<=rnd<=1，如0.3 :0.2
最大      平均
经  0.468217  0.080642 (角秒)
纬  0.346452  0.071540 (角秒)
距  0.724653  0.153055 (千米)
实际最大误差值大约为平均误差的6倍。
******/
//=================================行星星历=========================================
//==================================================================================
//xt星体,zn坐标号,t儒略世纪数,n计算项数
//转为儒略千年数
MM_EXPORT_DLL cs_real cs_XL0_calc(unsigned int xt,unsigned int zn,cs_real t,cs_real n);
//返回冥王星J2000直角坐标
MM_EXPORT_DLL struct cs_vector3 cs_pluto_coord(cs_real t);
//xt星体,T儒略世纪数,TD
MM_EXPORT_DLL struct cs_vector3 cs_p_coord(int xt,cs_real t,cs_real n1,cs_real n2,cs_real n3);
//返回地球坐标,t为世纪数
MM_EXPORT_DLL struct cs_vector3 cs_e_coord(cs_real t,cs_real n1,cs_real n2,cs_real n3);
//=================================月亮星历--=======================================
//==================================================================================
//计算月亮
MM_EXPORT_DLL cs_real cs_XL1_calc(unsigned int zn,cs_real t,cs_real n);;
//返回月球坐标,n1,n2,n3为各坐标所取的项数
MM_EXPORT_DLL struct cs_vector3 cs_m_coord(cs_real t,cs_real n1,cs_real n2,cs_real n3);
//=============================一些天文基本问题=====================================
//==================================================================================
//返回朔日的编号,jd应在朔日附近，允许误差数天
MM_EXPORT_DLL cs_real cs_suoN(cs_real jd); 
//太阳光行差,t是世纪数
MM_EXPORT_DLL cs_real cs_gxc_sunLon(cs_real t);
//黄纬光行差
MM_EXPORT_DLL cs_real cs_gxc_sunLat(cs_real t); 
//月球经度光行差,误差0.07"
MM_EXPORT_DLL cs_real cs_gxc_moonLon(cs_real t); 
//月球纬度光行差,误差0.006"
MM_EXPORT_DLL cs_real cs_gxc_moonLat(cs_real t); 

//传入T是2000年首起算的日数(UT),dt是deltatT(日),精度要求不高时dt可取值为0
//返回格林尼治平恒星时(不含赤经章动及非多项式部分),即格林尼治子午圈的平春风点起算的赤经
MM_EXPORT_DLL cs_real cs_pGST(cs_real T,cs_real dt);
//传入力学时J2000起算日数，返回平恒星时
MM_EXPORT_DLL cs_real cs_pGST2(cs_real jd);
//太阳升降计算。jd儒略日(须接近L当地平午UT)，L地理经度，fa地理纬度，sj=-1升,sj=1降
MM_EXPORT_DLL cs_real cs_sunShengJ(cs_real jd,cs_real L,cs_real fa,cs_real sj);
//时差计算(高精度),t力学时儒略世纪数
MM_EXPORT_DLL cs_real cs_pty_zty(cs_real t);
//时差计算(低精度),误差约在1秒以内,t力学时儒略世纪数
MM_EXPORT_DLL cs_real cs_pty_zty2(cs_real t);

#include "core/mm_suffix.h"

#endif//__mm_calendar_eph0_h__