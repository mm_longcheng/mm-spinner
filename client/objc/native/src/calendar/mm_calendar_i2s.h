﻿#ifndef __mm_calendar_i2s_h__
#define __mm_calendar_i2s_h__
#include "core/mm_platform.h"
#include "core/mm_prefix.h"
#include "calendar/mm_calendar_vector.h"

// calendar int to string
// common name mapping.
// const string.
// 行星名
MM_EXPORT_DLL extern const struct cs_arrs cs_xx_name;
// 星期
MM_EXPORT_DLL extern const struct cs_arrs cs_Week;
// 中文数字
MM_EXPORT_DLL extern const struct cs_arrs cs_Number;
// 干
MM_EXPORT_DLL extern const struct cs_arrs cs_Gan;
// 支
MM_EXPORT_DLL extern const struct cs_arrs cs_Zhi;
// 生肖
MM_EXPORT_DLL extern const struct cs_arrs cs_ShX;
// 星座
MM_EXPORT_DLL extern const struct cs_arrs cs_XiZ;
// 月相名称表
MM_EXPORT_DLL extern const struct cs_arrs cs_yxmc;
// 节气名称表
MM_EXPORT_DLL extern const struct cs_arrs cs_jqmc;
// 月名称,建寅
MM_EXPORT_DLL extern const struct cs_arrs cs_ymc;
// 日名称,初一
MM_EXPORT_DLL extern const struct cs_arrs cs_rmc;

#include "core/mm_suffix.h"

#endif//__mm_calendar_i2s_h__