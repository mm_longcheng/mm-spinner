#ifndef __mm_math_h__
#define __mm_math_h__

#include "core/mm_prefix.h"

#include "core/mm_config.h"
#include "core/mm_core.h"

#define mm_abs(value)       (((value) >= 0) ? (value) : - (value))
#define mm_max(val1, val2)  (((val1) < (val2)) ? (val2) : (val1))
#define mm_min(val1, val2)  (((val1) > (val2)) ? (val2) : (val1))

#include "core/mm_suffix.h"

#endif//__mm_math_h__