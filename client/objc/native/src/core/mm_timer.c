#include "mm_timer.h"
#include "core/mm_timeval.h"
#include "core/mm_spinlock.h"
#include "core/mm_alloc.h"
#include "core/mm_limit.h"

static void __static_timer_heap_handle(struct mm_timer_heap* timer_heap, struct mm_timer_entry* entry)
{

}

MM_EXPORT_DLL void mm_timer_heap_callback_init(struct mm_timer_heap_callback* p)
{
	p->handle = &__static_timer_heap_handle;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_timer_heap_callback_destroy(struct mm_timer_heap_callback* p)
{
	p->handle = &__static_timer_heap_handle;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_timer_entry_init(struct mm_timer_entry* p)
{
	mm_timer_heap_callback_init(&p->callback);
	mm_timeval_init(&p->timer_value);
	mm_spin_init(&p->locker,NULL);
	p->period = 0;
	p->id = 0;
	p->timer_id = MM_TIMER_ID_INVALID;
}
MM_EXPORT_DLL void mm_timer_entry_destroy(struct mm_timer_entry* p)
{
	mm_timer_heap_callback_destroy(&p->callback);
	mm_timeval_destroy(&p->timer_value);
	mm_spin_destroy(&p->locker);
	p->period = 0;
	p->id = 0;
	p->timer_id = 0;
}
MM_EXPORT_DLL int mm_timer_entry_compare(struct mm_timer_entry* p,struct mm_timer_entry* q)
{
	return 0 != mm_timeval_less(&p->timer_value,&q->timer_value) ? 1 : 0;
}
//////////////////////////////////////////////////////////////////////////
static void __static_timer_heap_copy_node( struct mm_timer_heap* p, size_t slot, struct mm_timer_entry* moved_node )
{
	// Insert <moved_node> into its new location in the heap.
	p->heap[slot] = moved_node;

	// Update the corresponding slot in the parallel <ids_> array.
	p->timer_ids[moved_node->timer_id] = (int)slot;
}

static int __static_timer_heap_pop_freelist(struct mm_timer_heap* p)
{
	// We need to truncate this to <int> for backwards compatibility.
	int new_id = p->timer_ids_freelist;

	// The freelist values in the <timer_ids_> are negative, so we need
	// to negate them to get the next freelist "pointer."
	p->timer_ids_freelist = -p->timer_ids[p->timer_ids_freelist];

	return new_id;
}

static void __static_timer_heap_push_freelist (struct mm_timer_heap* p, int old_id)
{
	// The freelist values in the <timer_ids_> are negative, so we need
	// to negate them to get the next freelist "pointer."
	p->timer_ids[old_id] = -p->timer_ids_freelist;
	p->timer_ids_freelist = old_id;
}

static void __static_timer_heap_reheap_down(struct mm_timer_heap* p, struct mm_timer_entry* moved_node, size_t slot, size_t child)
{
	// Restore the heap property after a deletion.    
	while (child < p->cur_size)
	{
		// Choose the smaller of the two children.
		if (child + 1 < p->cur_size && 0 != mm_timer_entry_compare(p->heap[child + 1], p->heap[child]))
		{
			child++;
		}

		// Perform a <copy> if the child has a larger timeout value than
		// the <moved_node>.
		if (0 != mm_timer_entry_compare(p->heap[child], moved_node))
		{
			__static_timer_heap_copy_node( p, slot, p->heap[child]);
			slot = child;
			child = MM_HEAP_L(child);
		}
		else
		{
			// We've found our location in the heap.
			break;
		}
	}
	__static_timer_heap_copy_node( p, slot, moved_node);
}

static void __static_timer_heap_reheap_up(struct mm_timer_heap* p, struct mm_timer_entry* moved_node, size_t slot, size_t parent)
{
	// Restore the heap property after an insertion.    
	while (slot > 0)
	{
		// If the parent node is greater than the <moved_node> we need
		// to copy it down.
		if (0 != mm_timer_entry_compare(moved_node, p->heap[parent]))
		{
			__static_timer_heap_copy_node(p, slot, p->heap[parent]);
			slot = parent;
			parent = MM_HEAP_P(slot);
		}
		else
		{
			break;
		}
	}

	// Insert the new node into its proper resting place in the heap and
	// update the corresponding slot in the parallel <timer_ids> array.
	__static_timer_heap_copy_node(p, slot, moved_node);
}


static struct mm_timer_entry* __static_timer_heap_remove_node(struct mm_timer_heap* p, size_t slot)
{
	struct mm_timer_entry* removed_node = p->heap[slot];

	// Return this timer id to the freelist.
	__static_timer_heap_push_freelist( p, removed_node->timer_id );

	// Decrement the size of the heap by one since we're removing the
	// "slot"th node.
	p->cur_size--;

	// Set the ID
	removed_node->timer_id = MM_TIMER_ID_INVALID;

	// Only try to reheapify if we're not deleting the last entry.    
	if (slot < p->cur_size)
	{
		size_t parent;
		struct mm_timer_entry* moved_node = p->heap[p->cur_size];

		// Move the end node to the location being removed and update
		// the corresponding slot in the parallel <timer_ids> array.
		__static_timer_heap_copy_node( p, slot, moved_node);

		// If the <moved_node->time_value_> is great than or equal its
		// parent it needs be moved down the heap.
		parent = MM_HEAP_P(slot);

		if (0 == mm_timer_entry_compare(moved_node, p->heap[parent]))
		{
			__static_timer_heap_reheap_down( p, moved_node, slot, MM_HEAP_L(slot));
		}
		else
		{
			__static_timer_heap_reheap_up( p, moved_node, slot, parent);
		}
	}

	return removed_node;
}

static void __static_timer_heap_grow_heap(struct mm_timer_heap* p)
{
	// All the containers will double in size from max_size_
	size_t new_size = p->max_size + MM_TIMER_HEAP_PAGE;
	int* new_ids = NULL;
	struct mm_timer_entry** new_heap = NULL;
	struct mm_timer_entry* new_heap_entry = NULL;
	size_t i = 0;

	// First grow the heap itself.
	new_heap = (struct mm_timer_entry**)mm_malloc(sizeof(struct mm_timer_entry*) * new_size);
	mm_memset(new_heap,0,sizeof(struct mm_timer_entry*) * new_size);
	memcpy(new_heap, p->heap, p->max_size * sizeof(struct mm_timer_entry*));
	mm_free(p->heap);
	p->heap = new_heap;

	// Grow the array of heap entry.
	new_heap_entry = (struct mm_timer_entry*)mm_malloc(sizeof(struct mm_timer_entry) * new_size);
	mm_memset(new_heap_entry,0,sizeof(struct mm_timer_entry*) * new_size);
	memcpy(new_heap_entry, p->heap_entry, p->max_size * sizeof(struct mm_timer_entry));
	mm_free(p->heap_entry);
	p->heap_entry = new_heap_entry;

	// Grow the array of timer ids.
	new_ids = (int*)mm_malloc(new_size * sizeof(int));
	mm_memset(new_ids,0,sizeof(int) * new_size);
	memcpy(new_ids, p->timer_ids, p->max_size * sizeof(int));
	mm_free(p->timer_ids);
	p->timer_ids = new_ids;

	// And add the new elements to the end of the "freelist".
	for (i = p->max_size; i < new_size; i++)
	{
		struct mm_timer_entry* entry = &p->heap_entry[i];
		mm_timer_entry_init(entry);
		p->timer_ids[i] = -((mm_timer_id) (i + 1));
	}
	p->max_size = new_size;
}

static void __static_timer_heap_insert_node(struct mm_timer_heap* p, struct mm_timer_entry* new_node)
{
	if (p->cur_size >= p->max_size)
	{
		__static_timer_heap_grow_heap(p);
	}
	__static_timer_heap_reheap_up( p, new_node, p->cur_size, MM_HEAP_P(p->cur_size));
	p->cur_size++;
}

static void __static_timer_heap_entry_array_alloc(struct mm_timer_heap* p)
{
	size_t i = 0;
	for (i = 0; i < p->max_size; ++i)
	{
		struct mm_timer_entry* entry = &p->heap_entry[i];
		mm_timer_entry_init(entry);
		p->timer_ids[i] = -((int)(i + 1));
	}
}
static void __static_timer_heap_entry_array_relax(struct mm_timer_heap* p)
{
	size_t i = 0;
	for (i = 0; i < p->max_size; ++i)
	{
		struct mm_timer_entry* entry = &p->heap_entry[i];
		mm_timer_entry_destroy(entry);
	}
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_timer_heap_init(struct mm_timer_heap* p)
{
	mm_timer_heap_callback_init(&p->callback);
	mm_spin_init(&p->locker,NULL);
	p->max_size = MM_TIMER_HEAP_PAGE;
	p->cur_size = 0;
	p->max_entries_per_poll = MM_TIMER_HEAP_MAX_POLL_TIMEOUT;
	p->timer_ids_freelist = 0;
	p->heap = (struct mm_timer_entry**)mm_malloc(sizeof(struct mm_timer_entry*) * p->max_size);
	p->heap_entry = (struct mm_timer_entry*)mm_malloc(sizeof(struct mm_timer_entry) * p->max_size);
	p->timer_ids = (mm_timer_id*)mm_malloc(sizeof(mm_timer_id) * p->max_size);
	//
	__static_timer_heap_entry_array_alloc(p);
}
MM_EXPORT_DLL void mm_timer_heap_destroy(struct mm_timer_heap* p)
{
	__static_timer_heap_entry_array_relax(p);
	//
	mm_timer_heap_callback_destroy(&p->callback);
	mm_spin_destroy(&p->locker);
	p->max_size = 0;
	p->cur_size = 0;
	p->max_entries_per_poll = 0;
	p->timer_ids_freelist = 0;
	mm_free(p->heap);
	mm_free(p->heap_entry);
	mm_free(p->timer_ids);
	p->heap = NULL;
	p->heap_entry = NULL;
	p->timer_ids = NULL;
}
MM_EXPORT_DLL void mm_timer_heap_lock(struct mm_timer_heap* p)
{
	mm_spin_lock(&p->locker);
}
MM_EXPORT_DLL void mm_timer_heap_unlock(struct mm_timer_heap* p)
{
	mm_spin_unlock(&p->locker);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL struct mm_timer_entry* mm_timer_heap_schedule_timeval(struct mm_timer_heap* p, struct timeval* delay, timer_heap_handle handle)
{
	struct timeval expires;
	mm_timer_id timer_id = 0;
	struct mm_timer_entry* entry = NULL;

	mm_gettimeofday(&expires,NULL);
	mm_timeval_add(&expires, delay);

	mm_timer_heap_lock(p);

	if (p->cur_size >= p->max_size)
	{
		__static_timer_heap_grow_heap(p);
	}
	timer_id = __static_timer_heap_pop_freelist(p);
	if (0 <= timer_id)
	{
		entry = &p->heap_entry[timer_id];
		// Obtain the next unique sequence number.
		// Set the entry
		entry->timer_id = timer_id;
		entry->timer_value = expires;
		__static_timer_heap_insert_node( p, entry);
	}
	mm_timer_heap_unlock(p);

	return entry;
}
// schedule a timer entry for delay and period.if 0 == period just call once.
MM_EXPORT_DLL struct mm_timer_entry* mm_timer_heap_schedule(struct mm_timer_heap* p, mm_msec_t delay, mm_msec_t period, timer_heap_handle handle, void* u)
{
	struct mm_timer_entry* entry = NULL;
	struct timeval tv_period;
	tv_period.tv_sec  = (delay / MM_MSEC_PER_SEC);
	tv_period.tv_usec = (delay % MM_MSEC_PER_SEC) * 1000;
	entry = mm_timer_heap_schedule_timeval(p, &tv_period, handle);
	entry->callback.handle = handle;
	entry->callback.obj = u;
	entry->period = period;
	return entry;
}
MM_EXPORT_DLL void mm_timer_heap_cancel(struct mm_timer_heap* p, struct mm_timer_entry* entry)
{
	mm_timer_id timer_node_slot = 0;

	mm_timer_heap_lock(p);
	// Check to see if the timer_id is out of range
	if (entry->timer_id < 0 || (size_t)entry->timer_id > p->max_size) 
	{
		entry->timer_id = MM_TIMER_ID_INVALID;
	}

	timer_node_slot = p->timer_ids[entry->timer_id];

	if (timer_node_slot < 0) 
	{
		// Check to see if timer_id is still valid.
		entry->timer_id = MM_TIMER_ID_INVALID;
	}

	if (entry == p->heap[timer_node_slot])
	{
		__static_timer_heap_remove_node( p, timer_node_slot);

		// Call the close hook.
		(*(p->callback.handle))(p, entry);
	}
	mm_timer_heap_unlock(p);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL mm_uint32_t mm_timer_heap_poll(struct mm_timer_heap* p, struct timeval* next_delay)
{
	struct timeval now;
	mm_uint32_t count = 0;

	mm_timer_heap_lock(p);
	do 
	{
		if (0 == p->cur_size && NULL != next_delay)
		{
			next_delay->tv_sec = next_delay->tv_usec = MM_UINT32_MAX;
			break;
		}

		count = 0;
		mm_gettimeofday(&now,NULL);

		while (0 != p->cur_size && mm_timeval_less_or_equal(&p->heap[0]->timer_value, &now) && count < p->max_entries_per_poll ) 
		{
			struct mm_timer_entry* node = __static_timer_heap_remove_node(p, 0);

			++count;

			mm_timer_heap_unlock(p);

			(*(node->callback.handle))(p, node);

			if (0 != node->period)
			{
				mm_timer_heap_schedule(p,node->period,node->period,node->callback.handle,node->callback.obj);
			}

			mm_timer_heap_lock(p);
		}
		if (0 != p->cur_size && NULL != next_delay) 
		{
			*next_delay = p->heap[0]->timer_value;
			mm_timeval_sub(next_delay, &now);
			// if the next delay time less then 0.we assign it to 0.
			if ( next_delay->tv_sec * MM_USEC_PER_SEC + next_delay->tv_usec < 0 )
			{
				next_delay->tv_sec = next_delay->tv_usec = 0;
			}
		} 
		else if (NULL != next_delay) 
		{
			next_delay->tv_sec = next_delay->tv_usec = MM_UINT32_MAX;
		}
	} while (0);
	mm_timer_heap_unlock(p);
	return count;
}
MM_EXPORT_DLL void mm_timer_heap_earliest_time(struct mm_timer_heap* p, struct timeval* next_delay)
{
	if (p->cur_size == 0)
	{
		next_delay->tv_sec  = MM_UINT32_MAX;
		next_delay->tv_usec = MM_UINT32_MAX;
	}
	else
	{
		struct timeval now;
		mm_gettimeofday(&now,NULL);
		mm_timer_heap_lock(p);
		*next_delay = p->heap[0]->timer_value;
		mm_timer_heap_unlock(p);
		mm_timeval_sub(next_delay, &now);
	}
}
//////////////////////////////////////////////////////////////////////////
static void __static_timer_task_timer_handle(struct mm_timer_task* p)
{
	struct mm_timer* timer = (struct mm_timer*)(p->callback.obj);
	struct timeval next_delay;
	int nearby_time = 0;
	mm_timer_heap_poll(&timer->timer_heap,&next_delay);
	nearby_time = next_delay.tv_sec * MM_MSEC_PER_SEC + next_delay.tv_usec / 1000;
	// [0,MM_TIME_TASK_NEARBY_MSEC]
	nearby_time = 0 > nearby_time ? 0 : nearby_time;
	nearby_time = MM_TIMER_TASK_NEARBY_MSEC < nearby_time ? MM_TIMER_TASK_NEARBY_MSEC : nearby_time;
	mm_timer_task_assign_nearby_time(&timer->timer_task,nearby_time);
}
MM_EXPORT_DLL void mm_timer_init(struct mm_timer* p)
{
	struct mm_timer_task_callback callback;

	mm_timer_heap_init(&p->timer_heap);
	mm_timer_task_init(&p->timer_task);

	callback.handle = &__static_timer_task_timer_handle;
	callback.obj = p;
	mm_timer_task_assign_callback(&p->timer_task,&callback);
}
MM_EXPORT_DLL void mm_timer_destroy(struct mm_timer* p)
{
	mm_timer_heap_destroy(&p->timer_heap);
	mm_timer_task_destroy(&p->timer_task);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_timer_lock(struct mm_timer* p)
{
	mm_timer_heap_lock(&p->timer_heap);
}
MM_EXPORT_DLL void mm_timer_unlock(struct mm_timer* p)
{
	mm_timer_heap_unlock(&p->timer_heap);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL struct mm_timer_entry* mm_timer_schedule(struct mm_timer* p, mm_msec_t delay, mm_msec_t period, timer_heap_handle handle, void* u)
{
	struct timeval next_delay;
	int nearby_time = 0;
	struct mm_timer_entry* entry = mm_timer_heap_schedule(&p->timer_heap,delay,period,handle,u);
	mm_timer_heap_earliest_time(&p->timer_heap,&next_delay);
	nearby_time = next_delay.tv_sec * MM_MSEC_PER_SEC + next_delay.tv_usec / 1000;
	// [0,MM_TIME_TASK_NEARBY_MSEC]
	nearby_time = 0 > nearby_time ? 0 : nearby_time;
	nearby_time = MM_TIMER_TASK_NEARBY_MSEC < nearby_time ? MM_TIMER_TASK_NEARBY_MSEC : nearby_time;
	mm_timer_task_assign_nearby_time(&p->timer_task,nearby_time);
	return entry;
}
MM_EXPORT_DLL void mm_timer_cancel(struct mm_timer* p, struct mm_timer_entry* entry)
{
	mm_timer_heap_cancel(&p->timer_heap, entry);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_timer_start(struct mm_timer* p)
{
	mm_timer_task_start(&p->timer_task);
}
MM_EXPORT_DLL void mm_timer_interrupt(struct mm_timer* p)
{
	mm_timer_task_interrupt(&p->timer_task);
}
MM_EXPORT_DLL void mm_timer_shutdown(struct mm_timer* p)
{
	mm_timer_task_shutdown(&p->timer_task);
}
MM_EXPORT_DLL void mm_timer_join(struct mm_timer* p)
{
	mm_timer_task_join(&p->timer_task);
}
//////////////////////////////////////////////////////////////////////////