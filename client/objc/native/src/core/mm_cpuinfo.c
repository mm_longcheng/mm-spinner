#include "core/mm_cpuinfo.h"
#include "core/mm_alloc.h"
#include "core/mm_string.h"
#include "core/mm_types.h"
//////////////////////////////////////////////////////////////////////////
#if MM_COMPILER == MM_COMPILER_MSVC
#	include <excpt.h>      // For SEH values
#	if _MSC_VER >= 1400
#		include <intrin.h>
#	endif
#elif (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG) && MM_PLATFORM != MM_PLATFORM_NACL
#	include <signal.h>
#	include <setjmp.h>

#	if MM_PLATFORM == MM_PLATFORM_ANDROID
#		include <cpu-features.h>
#	elif MM_CPU == MM_CPU_ARM 
#		include <sys/sysctl.h>
#		if __MACH__
#			include <mach/machine.h>
#			ifndef CPU_SUBTYPE_ARM64_V8
#				define CPU_SUBTYPE_ARM64_V8 ((cpu_subtype_t) 1)
#			endif
#			ifndef CPU_SUBTYPE_ARM_V8
#				define CPU_SUBTYPE_ARM_V8 ((cpu_subtype_t) 13)
#			endif
#		endif
#	endif
#endif
//////////////////////////////////////////////////////////////////////////
#if (( __i386__ || __amd64__ ) && ( __GNUC__ || __INTEL_COMPILER ))


static mm_inline void mm_cpuid(uint32_t i, uint32_t *buf);


#if ( __i386__ )

static mm_inline void
mm_cpuid(uint32_t i, uint32_t *buf)
{

    /*
     * we could not use %ebx as output parameter if gcc builds PIC,
     * and we could not save %ebx on stack, because %esp is used,
     * when the -fomit-frame-pointer optimization is specified.
     */

    __asm__ (

    "    mov    %%ebx, %%esi;  "

    "    cpuid;                "
    "    mov    %%eax, (%1);   "
    "    mov    %%ebx, 4(%1);  "
    "    mov    %%edx, 8(%1);  "
    "    mov    %%ecx, 12(%1); "

    "    mov    %%esi, %%ebx;  "

    : : "a" (i), "D" (buf) : "ecx", "edx", "esi", "memory" );
}


#else /* __amd64__ */


static mm_inline void
mm_cpuid(uint32_t i, uint32_t *buf)
{
    uint32_t  eax, ebx, ecx, edx;

    __asm__ (

        "cpuid"

    : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx) : "a" (i) );

    buf[0] = eax;
    buf[1] = ebx;
    buf[2] = edx;
    buf[3] = ecx;
}


#endif

#elif MM_CPU == MM_CPU_X86
// Performs CPUID instruction with 'query', fill the results, and return value of eax.
static void mm_cpuid(uint32_t query, uint32_t *buf)
{
#if MM_COMPILER ==MM_COMPILER_MSVC
#if _MSC_VER >= 1400 
	__cpuid((int*)buf, query);
#else
	mm_uint32_t  _eax, _ebx, _ecx, _edx;
	__asm
	{
		mov     eax, query
		cpuid
		mov     _eax, eax
		mov     _ebx, ebx
		mov     _edx, edx
		mov     _ecx, ecx
		// Return values in eax, no return statement requirement here for VC.
	}
	buf[0] = _eax;
	buf[1] = _ebx;
	buf[2] = _edx;
	buf[3] = _ecx;
#endif
#elif (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG) && MM_PLATFORM != MM_PLATFORM_NACL && MM_PLATFORM != MM_PLATFORM_EMSCRIPTEN
#if MM_ARCH_TYPE == MM_ARCHITECTURE_64
	mm_uint32_t  _eax, _ebx, _ecx, _edx;
	__asm__
		(
		"cpuid": "=a" (_eax), "=b" (_ebx), "=c" (_ecx), "=d" (_edx) : "a" (query)
		);
	buf[0] = _eax;
	buf[1] = _ebx;
	buf[2] = _edx;
	buf[3] = _ecx;
#else
	mm_uint32_t  _eax, _ebx, _ecx, _edx;
	__asm__
		(
		"pushl  %%ebx           \n\t"
		"cpuid                  \n\t"
		"movl   %%ebx, %%edi    \n\t"
		"popl   %%ebx           \n\t"
		: "=a" (_eax), "=D" (_ebx), "=c" (_ecx), "=d" (_edx)
		: "a" (query)
		);
	buf[0] = _eax;
	buf[1] = _ebx;
	buf[2] = _edx;
	buf[3] = _ecx;
#endif // MM_ARCHITECTURE_64

#else
	// TODO: Supports other compiler
	
#endif
}

#else

static mm_inline void mm_cpuid(uint32_t i, uint32_t *buf)
{
	buf[0] = 0;
	buf[1] = 0;
	buf[2] = 0;
	buf[3] = 0;
}

#endif

//////////////////////////////////////////////////////////////////////////
static void mm_cpuinfo_vendor(struct mm_cpuinfo* p)
{
	uint32_t cpuinfo[4];
	char vendor[13] = {0};
	mm_cpuid(0,cpuinfo);
	mm_memcpy(vendor, &cpuinfo[1], 12);
	mm_string_assigns(&p->vendor,vendor);
}
static void mm_cpuinfo_brand(struct mm_cpuinfo* p)
{
	mm_uint32_t i = 0;
	uint32_t cpuinfo[4];
	// 0x80000002--0x80000004
	const mm_uint32_t BRANDID = 0x80000002;
	char name[49] = {0}; // 48 char
	for ( i = 0; i < 3; i++)
	{
		mm_cpuid(BRANDID + i,cpuinfo);
		mm_memcpy(name + i*16, &cpuinfo[0], 16);
	}
	mm_string_assigns(&p->brand,name);
}
static void mm_cpuinfo_flags(struct mm_cpuinfo* p)
{
	uint32_t cpuinfo[4];
	mm_cpuid(1,cpuinfo);
    mm_memcpy(&p->level, &cpuinfo[0],8);
    mm_memcpy(&p->flags, &cpuinfo[2],8);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_cpuinfo_init(struct mm_cpuinfo* p)
{
	mm_string_init(&p->vendor);
	mm_string_init(&p->brand);
    p->level = 0;
    p->flags = 0;
}
MM_EXPORT_DLL void mm_cpuinfo_destroy(struct mm_cpuinfo* p)
{
	mm_string_destroy(&p->vendor);
	mm_string_destroy(&p->brand);
}
MM_EXPORT_DLL void mm_cpuinfo_perform(struct mm_cpuinfo* p)
{
	mm_cpuinfo_vendor(p);
	mm_cpuinfo_brand(p);
	mm_cpuinfo_flags(p);
}