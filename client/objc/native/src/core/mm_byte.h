#ifndef __mm_byte_h__
#define __mm_byte_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

// byte is a signed char type.
typedef mm_uchar_t mm_byte_t;
//////////////////////////////////////////////////////////////////////////
// ref for byte data.
struct mm_byte_ref
{
	size_t offset;// offset for byte.
	mm_byte_t* buffer;// buffer for byte.
};
MM_EXPORT_DLL void mm_byte_ref_init(struct mm_byte_ref* p);
MM_EXPORT_DLL void mm_byte_ref_destroy(struct mm_byte_ref* p);
//////////////////////////////////////////////////////////////////////////
// byte data.
struct mm_byte_data
{
	size_t length;// length for byte.
	mm_byte_t* buffer;// buffer for byte.
};
MM_EXPORT_DLL void mm_byte_data_init(struct mm_byte_data* p);
MM_EXPORT_DLL void mm_byte_data_destroy(struct mm_byte_data* p);
//////////////////////////////////////////////////////////////////////////
// |----l---|----r---|
// |-uint32-|-uint32-|
// little endian
// assemble
static mm_inline mm_uint64_t mm_byte_uint64_a(mm_uint64_t l,mm_uint64_t r)
{
	return r << 32 | l;
}
static mm_inline mm_uint32_t mm_byte_uint64_l(mm_uint64_t v)
{
	return (mm_uint32_t)(v & 0x00000000FFFFFFFF);
}
static mm_inline mm_uint32_t mm_byte_uint64_r(mm_uint64_t v)
{
	return (mm_uint32_t)(v >> 32);
}
// |----l---|----r---|
// |-uint16-|-uint16-|
// little endian
// assemble
static mm_inline mm_uint32_t mm_byte_uint32_a(mm_uint32_t l,mm_uint32_t r)
{
	return r << 16 | l;
}
static mm_inline mm_uint16_t mm_byte_uint32_l(mm_uint32_t v)
{
	return (mm_uint16_t)(v & 0x0000FFFF);
}
static mm_inline mm_uint16_t mm_byte_uint32_r(mm_uint32_t v)
{
	return (mm_uint16_t)(v >> 16);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_int16_encode_bytes( mm_uint8_t* buffer, int offset, mm_uint16_t i );
MM_EXPORT_DLL mm_uint16_t mm_int16_decode_bytes( mm_uint8_t* buffer, int offset );
MM_EXPORT_DLL void mm_int32_encode_bytes( mm_uint8_t* buffer, int offset, int i );
MM_EXPORT_DLL mm_uint32_t mm_int32_decode_bytes( mm_uint8_t* buffer, int offset );
MM_EXPORT_DLL void mm_int64_encode_bytes( mm_uint8_t* buffer, int offset, mm_uint64_t i );
MM_EXPORT_DLL mm_uint64_t mm_int64_decode_bytes( mm_uint8_t* buffer, int offset );
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_byte_h__