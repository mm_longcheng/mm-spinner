#include "mm_byte.h"

MM_EXPORT_DLL void mm_byte_ref_init(struct mm_byte_ref* p)
{
	p->offset = 0;
	p->buffer = NULL;
}
MM_EXPORT_DLL void mm_byte_ref_destroy(struct mm_byte_ref* p)
{
	p->offset = 0;
	p->buffer = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_byte_data_init(struct mm_byte_data* p)
{
	p->length = 0;
	p->buffer = NULL;
}
MM_EXPORT_DLL void mm_byte_data_destroy(struct mm_byte_data* p)
{
	p->length = 0;
	p->buffer = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_int16_encode_bytes( mm_uint8_t* buffer, int offset, mm_uint16_t i )
{
	buffer[offset + 1] = (mm_uint8_t)((i >>  8) & 0xFF);
	buffer[offset + 0] = (mm_uint8_t)((i >>  0) & 0xFF);
}

MM_EXPORT_DLL mm_uint16_t mm_int16_decode_bytes( mm_uint8_t* buffer, int offset )
{
	mm_uint16_t value = 0;
	value |= ((mm_uint16_t)(buffer[offset + 1] & 0xFF)) <<  8;
	value |= ((mm_uint16_t)(buffer[offset + 0] & 0xFF)) <<  0;
	return value;
}
MM_EXPORT_DLL void mm_int32_encode_bytes( mm_uint8_t* buffer, int offset, int i )
{
	buffer[offset + 3] = (mm_uint8_t)((i >> 24) & 0xFF);
	buffer[offset + 2] = (mm_uint8_t)((i >> 16) & 0xFF);
	buffer[offset + 1] = (mm_uint8_t)((i >>  8) & 0xFF);
	buffer[offset + 0] = (mm_uint8_t)((i >>  0) & 0xFF);
}

MM_EXPORT_DLL mm_uint32_t mm_int32_decode_bytes( mm_uint8_t* buffer, int offset )
{
	mm_uint32_t value = 0;
	value |= ((mm_uint32_t)(buffer[offset + 3] & 0xFF)) << 24;
	value |= ((mm_uint32_t)(buffer[offset + 2] & 0xFF)) << 16;
	value |= ((mm_uint32_t)(buffer[offset + 1] & 0xFF)) <<  8;
	value |= ((mm_uint32_t)(buffer[offset + 0] & 0xFF)) <<  0;
	return value;
}
MM_EXPORT_DLL void mm_int64_encode_bytes( mm_uint8_t* buffer, int offset, mm_uint64_t i )
{
	buffer[offset + 7] = (mm_uint8_t)((i >> 56) & 0xFF);
	buffer[offset + 6] = (mm_uint8_t)((i >> 48) & 0xFF);
	buffer[offset + 5] = (mm_uint8_t)((i >> 40) & 0xFF);
	buffer[offset + 4] = (mm_uint8_t)((i >> 32) & 0xFF);
	buffer[offset + 3] = (mm_uint8_t)((i >> 24) & 0xFF);
	buffer[offset + 2] = (mm_uint8_t)((i >> 16) & 0xFF);
	buffer[offset + 1] = (mm_uint8_t)((i >>  8) & 0xFF);
	buffer[offset + 0] = (mm_uint8_t)((i >>  0) & 0xFF);
}

MM_EXPORT_DLL mm_uint64_t mm_int64_decode_bytes( mm_uint8_t* buffer, int offset )
{
	mm_uint64_t value = 0;
	value |= ((mm_uint64_t)(buffer[offset + 7] & 0xFF)) << 56;
	value |= ((mm_uint64_t)(buffer[offset + 6] & 0xFF)) << 48;
	value |= ((mm_uint64_t)(buffer[offset + 5] & 0xFF)) << 40;
	value |= ((mm_uint64_t)(buffer[offset + 4] & 0xFF)) << 32;
	value |= ((mm_uint64_t)(buffer[offset + 3] & 0xFF)) << 24;
	value |= ((mm_uint64_t)(buffer[offset + 2] & 0xFF)) << 16;
	value |= ((mm_uint64_t)(buffer[offset + 1] & 0xFF)) <<  8;
	value |= ((mm_uint64_t)(buffer[offset + 0] & 0xFF)) <<  0;
	return value;
}
