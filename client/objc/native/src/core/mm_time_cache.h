#ifndef __mm_time_cache_h__
#define __mm_time_cache_h__
//
#include "core/mm_prefix.h"

#include "core/mm_core.h"

#include "core/mm_time.h"
#include "core/mm_string.h"
#include "core/mm_spinlock.h"
#include "core/mm_thread.h"

#include <pthread.h>

#define MM_TIME_SLOTS   64
// time cache update default is 100ms
#define MM_TIME_UPDATE 100

struct mm_time_info
{
	time_t     sec;
	mm_uint_t  msec;
	mm_int_t   gmtoff;
};
MM_EXPORT_DLL void mm_time_info_init(struct mm_time_info* p);
MM_EXPORT_DLL void mm_time_info_destroy(struct mm_time_info* p);

static const char log_time_string_template[] = "1992/01/26 05:20:14-001";
static const size_t log_time_string_length = sizeof(log_time_string_template);
struct mm_time_cache
{
	struct mm_time_info cached_time[MM_TIME_SLOTS];
	char cached_string_log_time[MM_TIME_SLOTS][sizeof(log_time_string_template)];
	struct mm_time_info start;// the struct start init time info.
	mm_atomic_t locker;
	pthread_t time_thread;
	mm_uint64_t msec_current;// current milliseconds.
	mm_msec_t msec_sleep;// sleep milliseconds.default is MM_TIME_UPDATE.when interrupt will sleep until awake.
	mm_uint_t slot;
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
	struct mm_time_info* cache;// weak ref,now cache time.
	const char* string_log_time;// weak ref,log time string cache.log_time_string_template
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL extern struct mm_time_cache* mm_time_cache_instance();
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_time_cache_init(struct mm_time_cache* p);
MM_EXPORT_DLL void mm_time_cache_destroy(struct mm_time_cache* p);
//
MM_EXPORT_DLL void mm_time_cache_update(struct mm_time_cache* p);
//
MM_EXPORT_DLL void mm_time_cache_start(struct mm_time_cache* p);
MM_EXPORT_DLL void mm_time_cache_interrupt(struct mm_time_cache* p);
MM_EXPORT_DLL void mm_time_cache_shutdown(struct mm_time_cache* p);
MM_EXPORT_DLL void mm_time_cache_join(struct mm_time_cache* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_gmtime(time_t t, mm_tm_t *tp);
MM_EXPORT_DLL void mm_time_tm(struct mm_time_info* ti, mm_tm_t *tp);
MM_EXPORT_DLL void mm_time_tm_const(const struct mm_time_info* ti, mm_tm_t *tp);
MM_EXPORT_DLL void mm_time_string(struct mm_time_info* tp, char ts[sizeof(log_time_string_template)]);
MM_EXPORT_DLL void mm_time_string_const(const struct mm_time_info* tp, char ts[sizeof(log_time_string_template)]);
// _offset second is dep for 1970/1/1 8:0:0 - 8 hour is 1970/1/1 0:0:0
// _timecode second is time code now 
MM_EXPORT_DLL time_t mm_get_duration_from_dawn_by_offset(time_t _timecode, time_t _offset );
// timecode is unix time.
MM_EXPORT_DLL time_t mm_get_duration_from_week(time_t _timecode );
// _offset second is dep for 1970/1/1 8:0:0 - 8 hour is 1970/1/1 0:0:0
// _timecode second is time code now 
MM_EXPORT_DLL int mm_get_day_flip(time_t _timecode ,time_t _offset );
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL int mm_timedwait_nearby(pthread_cond_t* signal_cond, pthread_mutex_t* signal_mutex, struct timeval* tv_now, struct timespec* ts_outtime, mm_msec_t _nearby_time);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_time_cache_h__