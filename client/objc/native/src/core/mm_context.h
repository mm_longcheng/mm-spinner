#ifndef __mm_context_h__
#define __mm_context_h__
//
#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_thread.h"

struct mm_context
{
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL extern struct mm_context* mm_context_instance();
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_context_init(struct mm_context* p);
MM_EXPORT_DLL void mm_context_destroy(struct mm_context* p);
//////////////////////////////////////////////////////////////////////////
// task thread for mm_logger_manager and mm_os_context.
MM_EXPORT_DLL void mm_context_start(struct mm_context* p);
MM_EXPORT_DLL void mm_context_interrupt(struct mm_context* p);
MM_EXPORT_DLL void mm_context_shutdown(struct mm_context* p);
MM_EXPORT_DLL void mm_context_join(struct mm_context* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_context_h__