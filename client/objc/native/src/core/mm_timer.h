#ifndef __mm_timer_h__
#define __mm_timer_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_time.h"
#include "core/mm_thread.h"
#include "core/mm_atomic.h"
#include "core/mm_timer_task.h"

#include <pthread.h>

//////////////////////////////////////////////////////////////////////////
#define MM_HEAP_P(X)	(X == 0 ? 0 : (((X) - 1) / 2))
#define MM_HEAP_L(X)	(((X)+(X))+1)
#define MM_HEAP_R(X)	(((X)+(X))+2)
//////////////////////////////////////////////////////////////////////////
#define MM_TIMER_ID_INVALID -1
#define MM_TIMER_HEAP_MAX_POLL_TIMEOUT 25
#define MM_TIMER_HEAP_PAGE 64
#define MM_TIMER_TASK_NEARBY_MSEC 20000

// timer for time schedule.

/**
 * The type for internal timer ID.
 */
typedef int mm_timer_id;

/**
 * The type for timer heap.
 */
struct mm_timer_heap;

/** 
 * Forward declaration for pj_timer_entry. 
 */
struct mm_timer_entry;

/**
 * The type of callback function to be called by timer scheduler when a timer
 * has expired.
 *
 * @param timer_heap    The timer heap.
 * @param entry         Timer entry which timer's has expired.
 */
typedef void (*timer_heap_handle)(struct mm_timer_heap* timer_heap, struct mm_timer_entry* entry);
struct mm_timer_heap_callback
{
	timer_heap_handle handle;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_timer_heap_callback_init(struct mm_timer_heap_callback* p);
MM_EXPORT_DLL void mm_timer_heap_callback_destroy(struct mm_timer_heap_callback* p);

/**
 * This structure represents an entry to the timer.
 */
struct mm_timer_entry
{
    /** 
     * Callback to be called when the timer expires. 
     */
    struct mm_timer_heap_callback callback;

    /** 
     * The future time when the timer expires, which the value is updated
     * by timer heap when the timer is scheduled.
     */
	struct timeval timer_value;

    /** 
     * The future time when the timer expires, will repeat.
	 * if 0 == period will only fire one time after delay.
	 * default is 0.
     */
	mm_msec_t period;

    /**
     * Internal: the lock used by this entry.
     */
	mm_atomic_t locker;

    /** 
     * Arbitrary ID assigned by the user/owner of this entry. 
     * Applications can use this ID to distinguish multiple
     * timer entries that share the same callback and user_data.
     */
    int id;

    /** 
     * Internal unique timer ID, which is assigned by the timer heap. 
     * Application should not touch this ID.
	 * default is MM_TIMER_ID_INVALID.
     */
    mm_timer_id timer_id;
};
MM_EXPORT_DLL void mm_timer_entry_init(struct mm_timer_entry* p);
MM_EXPORT_DLL void mm_timer_entry_destroy(struct mm_timer_entry* p);
// compare p q.less return none zero.
MM_EXPORT_DLL int mm_timer_entry_compare(struct mm_timer_entry* p,struct mm_timer_entry* q);

/**
 * The implementation of timer heap.
 */
struct mm_timer_heap
{
    /** Callback to be called when a timer expires. */
	struct mm_timer_heap_callback callback;

    /** Lock object. */
    mm_atomic_t locker;

    /** Maximum size of the heap.default is MM_TIMER_HEAP_POLL_PAGE */
    size_t max_size;

    /** Current size of the heap. */
    size_t cur_size;

    /** Max timed out entries to process per poll. 
	 * default is MM_TIMER_HEAP_MAX_POLL_TIMEOUT.
	 */
    mm_uint32_t max_entries_per_poll;

    /**
     * "Pointer" to the first element in the freelist contained within
     * the <timer_ids_> array, which is organized as a stack.
     */
    mm_timer_id timer_ids_freelist;

    /**
     * Current contents of the Heap, which is organized as a "heap" of
     * mm_timer_entry's.  In this context, a heap is a "partially
     * ordered, almost complete" binary tree, which is stored in an
     * array.
     */
    struct mm_timer_entry** heap;

    /**
     * mm_timer_entry storage.
     */
    struct mm_timer_entry* heap_entry;

    /**
     * An array of "pointers" that allows each mm_timer_entry in the
     * <heap_> to be located in O(1) time.  Basically, <timer_id_[i]>
     * contains the slot in the <heap_> array where an pj_timer_entry
     * with timer id <i> resides.  Thus, the timer id passed back from
     * <schedule_entry> is really an slot into the <timer_ids> array.  The
     * <timer_ids_> array serves two purposes: negative values are
     * treated as "pointers" for the <freelist_>, whereas positive
     * values are treated as "pointers" into the <heap_> array.
     */
    mm_timer_id *timer_ids;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_timer_heap_init(struct mm_timer_heap* p);
MM_EXPORT_DLL void mm_timer_heap_destroy(struct mm_timer_heap* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_timer_heap_lock(struct mm_timer_heap* p);
MM_EXPORT_DLL void mm_timer_heap_unlock(struct mm_timer_heap* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL struct mm_timer_entry* mm_timer_heap_schedule_timeval(struct mm_timer_heap* p, struct timeval* delay, timer_heap_handle handle);
// schedule a timer entry for delay and period.if 0 == period just call once.
MM_EXPORT_DLL struct mm_timer_entry* mm_timer_heap_schedule(struct mm_timer_heap* p, mm_msec_t delay, mm_msec_t period, timer_heap_handle handle, void* u);
MM_EXPORT_DLL void mm_timer_heap_cancel(struct mm_timer_heap* p, struct mm_timer_entry* entry);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL mm_uint32_t mm_timer_heap_poll(struct mm_timer_heap* p, struct timeval* next_delay);
MM_EXPORT_DLL void mm_timer_heap_earliest_time(struct mm_timer_heap* p, struct timeval* next_delay);
//////////////////////////////////////////////////////////////////////////
struct mm_timer
{
	struct mm_timer_heap timer_heap;
	struct mm_timer_task timer_task;
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_timer_init(struct mm_timer* p);
MM_EXPORT_DLL void mm_timer_destroy(struct mm_timer* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_timer_lock(struct mm_timer* p);
MM_EXPORT_DLL void mm_timer_unlock(struct mm_timer* p);
//////////////////////////////////////////////////////////////////////////
// schedule a timer entry for delay and period.if 0 == period just call once.
MM_EXPORT_DLL struct mm_timer_entry* mm_timer_schedule(struct mm_timer* p, mm_msec_t delay, mm_msec_t period, timer_heap_handle handle, void* u);
MM_EXPORT_DLL void mm_timer_cancel(struct mm_timer* p, struct mm_timer_entry* entry);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_timer_start(struct mm_timer* p);
MM_EXPORT_DLL void mm_timer_interrupt(struct mm_timer* p);
MM_EXPORT_DLL void mm_timer_shutdown(struct mm_timer* p);
MM_EXPORT_DLL void mm_timer_join(struct mm_timer* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_timer_h__