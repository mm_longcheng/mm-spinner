#ifndef __mm_list_h__
#define __mm_list_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

struct mm_list_head
{
	struct mm_list_head *next, *prev;
};

struct mm_hlist_node
{
	struct mm_hlist_node *next, **pprev;
};

struct mm_hlist_head
{
	struct mm_hlist_node *first;
};

/*
 * These are non-NULL pointers that will result in page faults
 * under normal circumstances, used to verify that nobody uses
 * non-initialized list entries.
 */
#define MM_LIST_POISON1  ((void *) (0x00100100))
#define MM_LIST_POISON2  ((void *) (0x00200200))

/*
 * Simple doubly linked list implementation.
 *
 * Some of the internal functions ("__xxx") are useful when
 * manipulating whole lists rather than single entries, as
 * sometimes we already know the next/prev entries and we can
 * generate better code by using them directly rather than
 * using the generic single-entry routines.
 */

#define MM_LIST_HEAD_INIT(name) { &(name), &(name) }

#define MM_LIST_HEAD(name) \
	struct mm_list_head name = MM_LIST_HEAD_INIT(name)

static mm_inline void MM_LIST_INIT_HEAD(struct mm_list_head *list)
{
	list->next = list;
	list->prev = list;
}

/*
 * Insert a new entry between two known consecutive entries.
 *
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 */
static mm_inline void __mm_list_add(struct mm_list_head *newe,
			      struct mm_list_head *prev,
			      struct mm_list_head *next)
{
	next->prev = newe;
	newe->next = next;
	newe->prev = prev;
	prev->next = newe;
}

/**
 * mm_list_add - add a new entry
 * @newe: new entry to be added
 * @head: list head to add it after
 *
 * Insert a new entry after the specified head.
 * This is good for implementing stacks.
 */
static mm_inline void mm_list_add(struct mm_list_head *newe, struct mm_list_head *head)
{
	__mm_list_add(newe, head, head->next);
}


/**
 * mm_list_add_tail - add a new entry
 * @newe: new entry to be added
 * @head: list head to add it before
 *
 * Insert a new entry before the specified head.
 * This is useful for implementing queues.
 */
static mm_inline void mm_list_add_tail(struct mm_list_head *newe, struct mm_list_head *head)
{
	__mm_list_add(newe, head->prev, head);
}

/*
 * Delete a list entry by making the prev/next entries
 * point to each other.
 *
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 */
static mm_inline void __mm_list_del(struct mm_list_head * prev, struct mm_list_head * next)
{
	next->prev = prev;
	prev->next = next;
}

/**
 * mm_list_del - deletes entry from list.
 * @entry: the element to delete from the list.
 * Note: mm_list_empty() on entry does not return true after this, the entry is
 * in an undefined state.
 */
static mm_inline void __mm_list_del_entry(struct mm_list_head *entry)
{
	__mm_list_del(entry->prev, entry->next);
}

static mm_inline void mm_list_del(struct mm_list_head *entry)
{
	__mm_list_del(entry->prev, entry->next);
	entry->next = (struct mm_list_head *)MM_LIST_POISON1;
	entry->prev = (struct mm_list_head *)MM_LIST_POISON2;
}

/**
 * mm_list_replace - replace old entry by new one
 * @olde : the element to be replaced
 * @newe : the new element to insert
 *
 * If @old was empty, it will be overwritten.
 */
static mm_inline void mm_list_replace(struct mm_list_head *olde,
				struct mm_list_head *newe)
{
	newe->next = olde->next;
	newe->next->prev = newe;
	newe->prev = olde->prev;
	newe->prev->next = newe;
}

static mm_inline void mm_list_replace_init(struct mm_list_head *olde,
					struct mm_list_head *newe)
{
	mm_list_replace(olde, newe);
	MM_LIST_INIT_HEAD(olde);
}

/**
 * mm_list_del_init - deletes entry from list and reinitialize it.
 * @entry: the element to delete from the list.
 */
static mm_inline void mm_list_del_init(struct mm_list_head *entry)
{
	__mm_list_del_entry(entry);
	MM_LIST_INIT_HEAD(entry);
}

/**
 * mm_list_move - delete from one list and add as another's head
 * @list: the entry to move
 * @head: the head that will precede our entry
 */
static mm_inline void mm_list_move(struct mm_list_head *list, struct mm_list_head *head)
{
	__mm_list_del_entry(list);
	mm_list_add(list, head);
}

/**
 * mm_list_move_tail - delete from one list and add as another's tail
 * @list: the entry to move
 * @head: the head that will follow our entry
 */
static mm_inline void mm_list_move_tail(struct mm_list_head *list,
				  struct mm_list_head *head)
{
	__mm_list_del_entry(list);
	mm_list_add_tail(list, head);
}

/**
 * mm_list_is_last - tests whether @list is the last entry in list @head
 * @list: the entry to test
 * @head: the head of the list
 */
static mm_inline int mm_list_is_last(const struct mm_list_head *list,
				const struct mm_list_head *head)
{
	return list->next == head;
}

/**
 * mm_list_empty - tests whether a list is empty
 * @head: the list to test.
 */
static mm_inline int mm_list_empty(const struct mm_list_head *head)
{
	return head->next == head;
}

/**
 * mm_list_empty_careful - tests whether a list is empty and not being modified
 * @head: the list to test
 *
 * Description:
 * tests whether a list is empty _and_ checks that no other CPU might be
 * in the process of modifying either member (next or prev)
 *
 * NOTE: using mm_list_empty_careful() without synchronization
 * can only be safe if the only activity that can happen
 * to the list entry is mm_list_del_init(). Eg. it cannot be used
 * if another CPU could re-list_add() it.
 */
static mm_inline int mm_list_empty_careful(const struct mm_list_head *head)
{
	struct mm_list_head *next = head->next;
	return (next == head) && (next == head->prev);
}

/**
 * mm_list_rotate_left - rotate the list to the left
 * @head: the head of the list
 */
static mm_inline void mm_list_rotate_left(struct mm_list_head *head)
{
	struct mm_list_head *first;

	if (!mm_list_empty(head)) {
		first = head->next;
		mm_list_move_tail(first, head);
	}
}

/**
 * mm_list_is_singular - tests whether a list has just one entry.
 * @head: the list to test.
 */
static mm_inline int mm_list_is_singular(const struct mm_list_head *head)
{
	return !mm_list_empty(head) && (head->next == head->prev);
}

static mm_inline void __mm_list_cut_position(struct mm_list_head *list,
		struct mm_list_head *head, struct mm_list_head *entry)
{
	struct mm_list_head *new_first = entry->next;
	list->next = head->next;
	list->next->prev = list;
	list->prev = entry;
	entry->next = list;
	head->next = new_first;
	new_first->prev = head;
}

/**
 * mm_list_cut_position - cut a list into two
 * @list: a new list to add all removed entries
 * @head: a list with entries
 * @entry: an entry within head, could be the head itself
 *	and if so we won't cut the list
 *
 * This helper moves the initial part of @head, up to and
 * including @entry, from @head to @list. You should
 * pass on @entry an element you know is on @head. @list
 * should be an empty list or a list you do not care about
 * losing its data.
 *
 */
static mm_inline void mm_list_cut_position(struct mm_list_head *list,
		struct mm_list_head *head, struct mm_list_head *entry)
{
	if (mm_list_empty(head))
		return;
	if (mm_list_is_singular(head) &&
		(head->next != entry && head != entry))
		return;
	if (entry == head)
		MM_LIST_INIT_HEAD(list);
	else
		__mm_list_cut_position(list, head, entry);
}

static mm_inline void __mm_list_splice(const struct mm_list_head *list,
				 struct mm_list_head *prev,
				 struct mm_list_head *next)
{
	struct mm_list_head *first = list->next;
	struct mm_list_head *last = list->prev;

	first->prev = prev;
	prev->next = first;

	last->next = next;
	next->prev = last;
}

/**
 * mm_list_splice - join two lists, this is designed for stacks
 * @list: the new list to add.
 * @head: the place to add it in the first list.
 */
static mm_inline void mm_list_splice(const struct mm_list_head *list,
				struct mm_list_head *head)
{
	if (!mm_list_empty(list))
		__mm_list_splice(list, head, head->next);
}

/**
 * mm_list_splice_tail - join two lists, each list being a queue
 * @list: the new list to add.
 * @head: the place to add it in the first list.
 */
static mm_inline void mm_list_splice_tail(struct mm_list_head *list,
				struct mm_list_head *head)
{
	if (!mm_list_empty(list))
		__mm_list_splice(list, head->prev, head);
}

/**
 * mm_list_splice_init - join two lists and reinitialise the emptied list.
 * @list: the new list to add.
 * @head: the place to add it in the first list.
 *
 * The list at @list is reinitialised
 */
static mm_inline void list_splice_init(struct mm_list_head *list,
				    struct mm_list_head *head)
{
	if (!mm_list_empty(list)) {
		__mm_list_splice(list, head, head->next);
		MM_LIST_INIT_HEAD(list);
	}
}

/**
 * mm_list_splice_tail_init - join two lists and reinitialise the emptied list
 * @list: the new list to add.
 * @head: the place to add it in the first list.
 *
 * Each of the lists is a queue.
 * The list at @list is reinitialised
 */
static mm_inline void mm_list_splice_tail_init(struct mm_list_head *list,
					 struct mm_list_head *head)
{
	if (!mm_list_empty(list)) {
		__mm_list_splice(list, head->prev, head);
		MM_LIST_INIT_HEAD(list);
	}
}

/**
 * mm_list_entry - get the struct for this entry
 * @ptr:	the &struct mm_list_head pointer.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the list_head within the struct.
 */
#define mm_list_entry(ptr, type, member) \
	mm_container_of(ptr, type, member)

/**
 * mm_list_first_entry - get the first element from a list
 * @ptr:	the list head to take the element from.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the list_head within the struct.
 *
 * Note, that list is expected to be not empty.
 */
#define mm_list_first_entry(ptr, type, member) \
	mm_list_entry((ptr)->next, type, member)

/**
 * mm_list_last_entry - get the last element from a list
 * @ptr:	the list head to take the element from.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the list_head within the struct.
 *
 * Note, that list is expected to be not empty.
 */
#define mm_list_last_entry(ptr, type, member) \
	mm_list_entry((ptr)->prev, type, member)

/**
 * mm_list_first_entry_or_null - get the first element from a list
 * @ptr:	the list head to take the element from.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the list_head within the struct.
 *
 * Note that if the list is empty, it returns NULL.
 */
#define mm_list_first_entry_or_null(ptr, type, member) \
	(!mm_list_empty(ptr) ? mm_list_first_entry(ptr, type, member) : NULL)

/**
 * mm_list_next_entry - get the next element in list
 * @pos:	the type * to cursor
 * @member:	the name of the list_head within the struct.
 */
#define mm_list_next_entry(pos, member) \
	mm_list_entry((pos)->member.next, typeof(*(pos)), member)

/**
 * mm_list_prev_entry - get the prev element in list
 * @pos:	the type * to cursor
 * @member:	the name of the list_head within the struct.
 */
#define mm_list_prev_entry(pos, member) \
	mm_list_entry((pos)->member.prev, typeof(*(pos)), member)

/**
 * mm_list_for_each	-	iterate over a list
 * @pos:	the &struct mm_list_head to use as a loop cursor.
 * @head:	the head for your list.
 */
#define mm_list_for_each(pos, head) \
	for (pos = (head)->next; pos != (head); pos = pos->next)

/**
 * mm_list_for_each_prev	-	iterate over a list backwards
 * @pos:	the &struct mm_list_head to use as a loop cursor.
 * @head:	the head for your list.
 */
#define mm_list_for_each_prev(pos, head) \
	for (pos = (head)->prev; pos != (head); pos = pos->prev)

/**
 * mm_list_for_each_safe - iterate over a list safe against removal of list entry
 * @pos:	the &struct mm_list_head to use as a loop cursor.
 * @n:		another &struct mm_list_head to use as temporary storage
 * @head:	the head for your list.
 */
#define mm_list_for_each_safe(pos, n, head) \
	for (pos = (head)->next, n = pos->next; pos != (head); \
		pos = n, n = pos->next)

/**
 * mm_list_for_each_prev_safe - iterate over a list backwards safe against removal of list entry
 * @pos:	the &struct mm_list_head to use as a loop cursor.
 * @n:		another &struct mm_list_head to use as temporary storage
 * @head:	the head for your list.
 */
#define mm_list_for_each_prev_safe(pos, n, head) \
	for (pos = (head)->prev, n = pos->prev; \
	     pos != (head); \
	     pos = n, n = pos->prev)

/**
 * mm_list_for_each_entry	-	iterate over list of given type
 * @pos:	the type * to use as a loop cursor.
 * @head:	the head for your list.
 * @member:	the name of the list_head within the struct.
 */
#define mm_list_for_each_entry(pos, head, member)				\
	for (pos = mm_list_first_entry(head, typeof(*pos), member);	\
	     &pos->member != (head);					\
	     pos = mm_list_next_entry(pos, member))

/**
 * mm_list_for_each_entry_reverse - iterate backwards over list of given type.
 * @pos:	the type * to use as a loop cursor.
 * @head:	the head for your list.
 * @member:	the name of the list_head within the struct.
 */
#define mm_list_for_each_entry_reverse(pos, head, member)			\
	for (pos = mm_list_last_entry(head, typeof(*pos), member);		\
	     &pos->member != (head); 					\
	     pos = mm_list_prev_entry(pos, member))

/**
 * mm_list_prepare_entry - prepare a pos entry for use in mm_list_for_each_entry_continue()
 * @pos:	the type * to use as a start point
 * @head:	the head of the list
 * @member:	the name of the list_head within the struct.
 *
 * Prepares a pos entry for use as a start point in mm_list_for_each_entry_continue().
 */
#define mm_list_prepare_entry(pos, head, member) \
	((pos) ? : mm_list_entry(head, typeof(*pos), member))

/**
 * mm_list_for_each_entry_continue - continue iteration over list of given type
 * @pos:	the type * to use as a loop cursor.
 * @head:	the head for your list.
 * @member:	the name of the list_head within the struct.
 *
 * Continue to iterate over list of given type, continuing after
 * the current position.
 */
#define mm_list_for_each_entry_continue(pos, head, member) 		\
	for (pos = mm_list_next_entry(pos, member);			\
	     &pos->member != (head);					\
	     pos = mm_list_next_entry(pos, member))

/**
 * mm_list_for_each_entry_continue_reverse - iterate backwards from the given point
 * @pos:	the type * to use as a loop cursor.
 * @head:	the head for your list.
 * @member:	the name of the list_head within the struct.
 *
 * Start to iterate over list of given type backwards, continuing after
 * the current position.
 */
#define mm_list_for_each_entry_continue_reverse(pos, head, member)		\
	for (pos = mm_list_prev_entry(pos, member);			\
	     &pos->member != (head);					\
	     pos = mm_list_prev_entry(pos, member))

/**
 * mm_list_for_each_entry_from - iterate over list of given type from the current point
 * @pos:	the type * to use as a loop cursor.
 * @head:	the head for your list.
 * @member:	the name of the list_head within the struct.
 *
 * Iterate over list of given type, continuing from current position.
 */
#define mm_list_for_each_entry_from(pos, head, member) 			\
	for (; &pos->member != (head);					\
	     pos = mm_list_next_entry(pos, member))

/**
 * mm_list_for_each_entry_safe - iterate over list of given type safe against removal of list entry
 * @pos:	the type * to use as a loop cursor.
 * @n:		another type * to use as temporary storage
 * @head:	the head for your list.
 * @member:	the name of the list_head within the struct.
 */
#define mm_list_for_each_entry_safe(pos, n, head, member)			\
	for (pos = mm_list_first_entry(head, typeof(*pos), member),	\
		n = mm_list_next_entry(pos, member);			\
	     &pos->member != (head); 					\
	     pos = n, n = mm_list_next_entry(n, member))

/**
 * mm_list_for_each_entry_safe_continue - continue list iteration safe against removal
 * @pos:	the type * to use as a loop cursor.
 * @n:		another type * to use as temporary storage
 * @head:	the head for your list.
 * @member:	the name of the list_head within the struct.
 *
 * Iterate over list of given type, continuing after current point,
 * safe against removal of list entry.
 */
#define mm_list_for_each_entry_safe_continue(pos, n, head, member) 		\
	for (pos = mm_list_next_entry(pos, member), 				\
		n = mm_list_next_entry(pos, member);				\
	     &pos->member != (head);						\
	     pos = n, n = mm_list_next_entry(n, member))

/**
 * mm_list_for_each_entry_safe_from - iterate over list from current point safe against removal
 * @pos:	the type * to use as a loop cursor.
 * @n:		another type * to use as temporary storage
 * @head:	the head for your list.
 * @member:	the name of the list_head within the struct.
 *
 * Iterate over list of given type from current point, safe against
 * removal of list entry.
 */
#define mm_list_for_each_entry_safe_from(pos, n, head, member) 			\
	for (n = mm_list_next_entry(pos, member);					\
	     &pos->member != (head);						\
	     pos = n, n = mm_list_next_entry(n, member))

/**
 * mm_list_for_each_entry_safe_reverse - iterate backwards over list safe against removal
 * @pos:	the type * to use as a loop cursor.
 * @n:		another type * to use as temporary storage
 * @head:	the head for your list.
 * @member:	the name of the list_head within the struct.
 *
 * Iterate backwards over list of given type, safe against removal
 * of list entry.
 */
#define mm_list_for_each_entry_safe_reverse(pos, n, head, member)		\
	for (pos = mm_list_last_entry(head, typeof(*pos), member),		\
		n = mm_list_prev_entry(pos, member);			\
	     &pos->member != (head); 					\
	     pos = n, n = mm_list_prev_entry(n, member))

/**
 * mm_list_safe_reset_next - reset a stale mm_list_for_each_entry_safe loop
 * @pos:	the loop cursor used in the mm_list_for_each_entry_safe loop
 * @n:		temporary storage used in mm_list_for_each_entry_safe
 * @member:	the name of the list_head within the struct.
 *
 * mm_list_safe_reset_next is not safe to use in general if the list may be
 * modified concurrently (eg. the lock is dropped in the loop body). An
 * exception to this is if the cursor element (pos) is pinned in the list,
 * and mm_list_safe_reset_next is called after re-taking the lock and before
 * completing the current iteration of the loop body.
 */
#define mm_list_safe_reset_next(pos, n, member)				\
	n = mm_list_next_entry(pos, member)

/*
 * Double linked lists with a single pointer list head.
 * Mostly useful for hash tables where the two pointer list head is
 * too wasteful.
 * You lose the ability to access the tail in O(1).
 */

#define MM_HLIST_HEAD_INIT { .first = NULL }
#define MM_HLIST_HEAD(name) struct mm_hlist_head name = {  .first = NULL }
#define MM_HLIST_INIT_HEAD(ptr) ((ptr)->first = NULL)
static mm_inline void MM_HLIST_INIT_NODE(struct mm_hlist_node *h)
{
	h->next = NULL;
	h->pprev = NULL;
}

static mm_inline int mm_hlist_unhashed(const struct mm_hlist_node *h)
{
	return !h->pprev;
}

static mm_inline int mm_hlist_empty(const struct mm_hlist_head *h)
{
	return !h->first;
}

static mm_inline void __mm_hlist_del(struct mm_hlist_node *n)
{
	struct mm_hlist_node *next = n->next;
	struct mm_hlist_node **pprev = n->pprev;
	*pprev = next;
	if (next)
		next->pprev = pprev;
}

static mm_inline void mm_hlist_del(struct mm_hlist_node *n)
{
	__mm_hlist_del(n);
	n->next = (struct mm_hlist_node *)MM_LIST_POISON1;
	n->pprev = (struct mm_hlist_node **)MM_LIST_POISON2;
}

static mm_inline void mm_hlist_del_init(struct mm_hlist_node *n)
{
	if (!mm_hlist_unhashed(n)) {
		__mm_hlist_del(n);
		MM_HLIST_INIT_NODE(n);
	}
}

static mm_inline void mm_hlist_add_head(struct mm_hlist_node *n, struct mm_hlist_head *h)
{
	struct mm_hlist_node *first = h->first;
	n->next = first;
	if (first)
		first->pprev = &n->next;
	h->first = n;
	n->pprev = &h->first;
}

/* next must be != NULL */
static mm_inline void mm_hlist_add_before(struct mm_hlist_node *n,
					struct mm_hlist_node *next)
{
	n->pprev = next->pprev;
	n->next = next;
	next->pprev = &n->next;
	*(n->pprev) = n;
}

static mm_inline void mm_hlist_add_behind(struct mm_hlist_node *n,
				    struct mm_hlist_node *prev)
{
	n->next = prev->next;
	prev->next = n;
	n->pprev = &prev->next;

	if (n->next)
		n->next->pprev  = &n->next;
}

/* after that we'll appear to be on some hlist and mm_hlist_del will work */
static mm_inline void mm_hlist_add_fake(struct mm_hlist_node *n)
{
	n->pprev = &n->next;
}

/*
 * Move a list from one list head to another. Fixup the pprev
 * reference of the first entry if it exists.
 */
static mm_inline void mm_hlist_move_list(struct mm_hlist_head *olde,
				   struct mm_hlist_head *newe)
{
	newe->first = olde->first;
	if (newe->first)
		newe->first->pprev = &newe->first;
	olde->first = NULL;
}

#define mm_hlist_entry(ptr, type, member) mm_container_of(ptr,type,member)

#define mm_hlist_for_each(pos, head) \
	for (pos = (head)->first; pos ; pos = pos->next)

#define mm_hlist_for_each_safe(pos, n, head) \
	for (pos = (head)->first; pos && ({ n = pos->next; 1; }); \
	     pos = n)

#define mm_hlist_entry_safe(ptr, type, member) \
	({ typeof(ptr) ____ptr = (ptr); \
	   ____ptr ? mm_hlist_entry(____ptr, type, member) : NULL; \
	})

/**
 * mm_hlist_for_each_entry	- iterate over list of given type
 * @pos:	the type * to use as a loop cursor.
 * @head:	the head for your list.
 * @member:	the name of the struct mm_hlist_node within the struct.
 */
#define mm_hlist_for_each_entry(pos, head, member)				\
	for (pos = mm_hlist_entry_safe((head)->first, typeof(*(pos)), member);\
	     pos;							\
	     pos = mm_hlist_entry_safe((pos)->member.next, typeof(*(pos)), member))

/**
 * mm_hlist_for_each_entry_continue - iterate over a hlist continuing after current point
 * @pos:	the type * to use as a loop cursor.
 * @member:	the name of the struct mm_hlist_node within the struct.
 */
#define mm_hlist_for_each_entry_continue(pos, member)			\
	for (pos = mm_hlist_entry_safe((pos)->member.next, typeof(*(pos)), member);\
	     pos;							\
	     pos = mm_hlist_entry_safe((pos)->member.next, typeof(*(pos)), member))

/**
 * mm_hlist_for_each_entry_from - iterate over a hlist continuing from current point
 * @pos:	the type * to use as a loop cursor.
 * @member:	the name of the struct mm_hlist_node within the struct.
 */
#define mm_hlist_for_each_entry_from(pos, member)				\
	for (; pos;							\
	     pos = mm_hlist_entry_safe((pos)->member.next, typeof(*(pos)), member))

/**
 * mm_hlist_for_each_entry_safe - iterate over list of given type safe against removal of list entry
 * @pos:	the type * to use as a loop cursor.
 * @n:		another &struct mm_hlist_node to use as temporary storage
 * @head:	the head for your list.
 * @member:	the name of the struct mm_hlist_node within the struct.
 */
#define mm_hlist_for_each_entry_safe(pos, n, head, member) 		\
	for (pos = mm_hlist_entry_safe((head)->first, typeof(*pos), member);\
	     pos && ({ n = pos->member.next; 1; });			\
	     pos = mm_hlist_entry_safe(n, typeof(*pos), member))

#include "core/mm_suffix.h"

#endif//__mm_list_h__
