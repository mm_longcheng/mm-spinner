#ifndef __mm_bit_h__
#define __mm_bit_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

static mm_inline mm_uint64_t mm_popcount64(mm_uint64_t y) // standard popcount; from wikipedia
{
	y -= ((y >> 1) & 0x5555555555555555ull);
	y = (y & 0x3333333333333333ull) + (y >> 2 & 0x3333333333333333ull);
	return ((y + (y >> 4)) & 0xf0f0f0f0f0f0f0full) * 0x101010101010101ull >> 56;
}

static mm_inline mm_uint64_t mm_DNAcount64(mm_uint64_t y, int c) // count #A/C/G/T from a 2-bit encoded integer; from BWA
{
	// reduce nucleotide counting to bits counting
	y = ((c&2)? y : ~y) >> 1 & ((c&1)? y : ~y) & 0x5555555555555555ull;
	// count the number of 1s in y
	y = (y & 0x3333333333333333ull) + (y >> 2 & 0x3333333333333333ull);
	return ((y + (y >> 4)) & 0xf0f0f0f0f0f0f0full) * 0x101010101010101ull >> 56;
}

#ifndef mm_roundup32 // round a 32-bit integer to the next closet integer; from "bit twiddling hacks"
#define mm_roundup32(x) (--(x), (x)|=(x)>>1, (x)|=(x)>>2, (x)|=(x)>>4, (x)|=(x)>>8, (x)|=(x)>>16, ++(x))
#endif

#ifndef mm_swap
#define mm_swap(a, b) (((a) ^= (b)), ((b) ^= (a)), ((a) ^= (b))) // from "bit twiddling hacks"
#endif

#include "core/mm_suffix.h"

#endif//__mm_bit_h__
