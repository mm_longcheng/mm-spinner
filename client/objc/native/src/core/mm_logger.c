#include "core/mm_logger.h"
#include "core/mm_alloc.h"

static struct mm_level_mark const_level_mark[] = 
{
	{mm_string_make("U"),mm_string_make("unknow"),},
	{mm_string_make("F"),mm_string_make("fatal"),},
	{mm_string_make("C"),mm_string_make("crit"),},
	{mm_string_make("E"),mm_string_make("error"),},
	{mm_string_make("A"),mm_string_make("alert"),},
	{mm_string_make("W"),mm_string_make("warning"),},
	{mm_string_make("N"),mm_string_make("notice"),},
	{mm_string_make("I"),mm_string_make("info"),},
	{mm_string_make("T"),mm_string_make("trace"),},
	{mm_string_make("D"),mm_string_make("debug"),},
	{mm_string_make("V"),mm_string_make("verbose"),},
};

MM_EXPORT_DLL const struct mm_level_mark* mm_logger_level_mark(int lvl)
{
	if (MM_LOG_FATAL <= lvl && lvl <= MM_LOG_VERBOSE)
	{
		return &const_level_mark[lvl];
	}
	else
	{
		return &const_level_mark[MM_LOG_UNKNOW];
	}
}
static struct mm_logger g_logger = { &mm_logger_callback_printf,NULL, };
MM_EXPORT_DLL struct mm_logger* mm_logger_instance()
{
	return &g_logger;
}

MM_EXPORT_DLL void mm_logger_init(struct mm_logger* p)
{
	p->callback = &mm_logger_callback_printf;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_logger_destroy(struct mm_logger* p)
{
	p->callback = &mm_logger_callback_printf;
	p->obj = NULL;
}
// assign callback.
MM_EXPORT_DLL void mm_logger_assign_callback(struct mm_logger* p, logger_callback callback, void* obj)
{
	p->callback = callback;
	p->obj = obj;
}
MM_EXPORT_DLL void mm_logger_message(struct mm_logger* p,mm_uint32_t lvl,const char* fmt, ...)
{
	struct mm_string message_buffer;
	va_list ap;
	mm_string_init(&message_buffer);
	va_start(ap, fmt);
	mm_string_vsprintf(&message_buffer, fmt, ap);
	va_end(ap);
	if (NULL != p && NULL != p->callback)
	{
		(*(p->callback))(p,lvl,message_buffer.s);
	}
	else
	{
		mm_logger_callback_printf(p,lvl,message_buffer.s);
	}
	mm_string_destroy(&message_buffer);
}
// default callback.
MM_EXPORT_DLL void mm_logger_callback_printf(struct mm_logger* p, mm_uint32_t lvl, const char* message)
{
	mm_printf("%s\n",message);
}
MM_EXPORT_DLL void mm_logger_socket_error(struct mm_logger* p, mm_uint32_t lvl, mm_err_t errcode)
{
	if (0 != errcode)
	{
		unsigned char error_info[SOCKET_ERROR_BUFFER_SIZE] = {0};
		mm_strerror(errcode, error_info, SOCKET_ERROR_BUFFER_SIZE);
		mm_logger_message(p, lvl, "errno:(%d) %s",errcode,error_info);
	}
}