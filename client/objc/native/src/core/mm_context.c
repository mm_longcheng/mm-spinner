#include "core/mm_context.h"
#include "core/mm_os_context.h"
#include "core/mm_logger_manager.h"

//////////////////////////////////////////////////////////////////////////
static struct mm_context g_context;
MM_EXPORT_DLL struct mm_context* mm_context_instance()
{
	return &g_context;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_context_init(struct mm_context* p)
{
	struct mm_logger_manager* g_logger_manager = mm_logger_manager_instance();
	struct mm_os_context* g_os_context = mm_os_context_instance();
	mm_logger_manager_init(g_logger_manager);
	mm_os_context_init(g_os_context);
	p->state = ts_closed;
}
MM_EXPORT_DLL void mm_context_destroy(struct mm_context* p)
{
	struct mm_os_context* g_os_context = mm_os_context_instance();
	struct mm_logger_manager* g_logger_manager = mm_logger_manager_instance();
	mm_os_context_destroy(g_os_context);
	mm_logger_manager_destroy(g_logger_manager);
	p->state = ts_closed;
}
//
MM_EXPORT_DLL void mm_context_start(struct mm_context* p)
{
	struct mm_logger_manager* g_logger_manager = mm_logger_manager_instance();
	struct mm_os_context* g_os_context = mm_os_context_instance();
	mm_logger_manager_start(g_logger_manager);
	mm_os_context_perform(g_os_context);
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
}
MM_EXPORT_DLL void mm_context_interrupt(struct mm_context* p)
{
	struct mm_logger_manager* g_logger_manager = mm_logger_manager_instance();
	mm_logger_manager_interrupt(g_logger_manager);
	p->state = ts_closed;
}
MM_EXPORT_DLL void mm_context_shutdown(struct mm_context* p)
{
	struct mm_logger_manager* g_logger_manager = mm_logger_manager_instance();
	mm_logger_manager_shutdown(g_logger_manager);
	p->state = ts_finish;
}
MM_EXPORT_DLL void mm_context_join(struct mm_context* p)
{
	struct mm_logger_manager* g_logger_manager = mm_logger_manager_instance();
	mm_logger_manager_join(g_logger_manager);
}
//////////////////////////////////////////////////////////////////////////
