#ifndef __mm_timeval_h__
#define __mm_timeval_h__
//
#include "core/mm_prefix.h"

#include "core/mm_core.h"
//////////////////////////////////////////////////////////////////////////
/**
 * This function will init timeval.
 *
 * @param p    The time value to normalize.
 * @return     void.
 * @hideinitializer
 */
MM_EXPORT_DLL void mm_timeval_init(struct timeval* p);

/**
 * This function will destroy timeval.
 *
 * @param p    The time value to normalize.
 * @return     void.
 * @hideinitializer
 */
MM_EXPORT_DLL void mm_timeval_destroy(struct timeval* p);

/**
 * This function will normalize timeval.
 *
 * @param p    The time value to normalize.
 * @return     void.
 * @hideinitializer
 */
MM_EXPORT_DLL void mm_timeval_normalize(struct timeval* p);

/**
 * This function will check if \a t1 is equal to \a t2.
 *
 * @param p    The first time value to compare.
 * @param q    The second time value to compare.
 * @return      Non-zero if both time values are equal.
 * @hideinitializer
 */
static mm_inline int mm_timeval_equal(struct timeval* p, struct timeval* q)
{
	return (p->tv_sec == q->tv_sec && p->tv_usec == q->tv_usec);
}

/**
 * This macro will check if \a t1 is greater than \a t2
 *
 * @param p    The first time value to compare.
 * @param q    The second time value to compare.
 * @return      Non-zero if t1 is greater than t2.
 * @hideinitializer
 */
static mm_inline int mm_timeval_greater(struct timeval* p, struct timeval* q)
{
	return (p->tv_sec > q->tv_sec || (p->tv_sec == q->tv_sec && p->tv_usec > q->tv_usec));
}

/**
 * This macro will check if \a t1 is greater than or equal to \a t2
 *
 * @param p    The first time value to compare.
 * @param q    The second time value to compare.
 * @return      Non-zero if t1 is greater than or equal to t2.
 * @hideinitializer
 */
static mm_inline int mm_timeval_greater_or_equal(struct timeval* p, struct timeval* q)
{
	return mm_timeval_greater(p,q) || mm_timeval_equal(p,q);
}

/**
 * This macro will check if \a t1 is less than \a t2
 *
 * @param p    The first time value to compare.
 * @param q    The second time value to compare.
 * @return      Non-zero if t1 is less than t2.
 * @hideinitializer
 */
static mm_inline int mm_timeval_less(struct timeval* p, struct timeval* q)
{
	return !mm_timeval_greater_or_equal(p,q);
}

/**
 * This macro will check if \a t1 is less than or equal to \a t2.
 *
 * @param p    The first time value to compare.
 * @param q    The second time value to compare.
 * @return      Non-zero if t1 is less than or equal to t2.
 * @hideinitializer
 */
static mm_inline int mm_timeval_less_or_equal(struct timeval* p, struct timeval* q)
{
	return !mm_timeval_greater(p,q);
}

/**
 * Add \a t2 to \a t1 and store the result in \a t1. Effectively
 *
 * this macro will expand as: (\a t1 += \a t2).
 * @param p    The time value to add.
 * @param q    The time value to be added to \a t1.
 * @hideinitializer
 */
static mm_inline void mm_timeval_add(struct timeval* p, struct timeval* q)
{
	p->tv_sec += q->tv_sec;
	p->tv_usec += q->tv_usec;
	mm_timeval_normalize(p);
}

/**
 * Substract \a t2 from \a t1 and store the result in \a t1. Effectively
 * this macro will expand as (\a t1 -= \a t2).
 *
 * @param p    The time value to subsctract.
 * @param q    The time value to be substracted from \a t1.
 * @hideinitializer
 */
static mm_inline void mm_timeval_sub(struct timeval* p, struct timeval* q)
{
	p->tv_sec -= q->tv_sec;
	p->tv_usec -= q->tv_usec;
	mm_timeval_normalize(p);
}
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_timeval_h__