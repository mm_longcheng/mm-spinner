#ifndef __mm_signal_task_h__
#define __mm_signal_task_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_time.h"
#include "core/mm_thread.h"

#include <pthread.h>

// signal task default success nearby time milliseconds.
#define MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC 0
// signal task default failure nearby time milliseconds.
#define MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC 200

//////////////////////////////////////////////////////////////////////////
typedef int (*signal_task_factor)(void* obj);
typedef int (*signal_task_handle)(void* obj);
struct mm_signal_task_callback
{
	signal_task_factor factor;// factor for condition wait.return code for 0 is fire event -1 for condition wait.
	signal_task_handle handle;// handle for fire event.return code result 0 for success -1 for failure.
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_signal_task_callback_init(struct mm_signal_task_callback* p);
MM_EXPORT_DLL void mm_signal_task_callback_destroy(struct mm_signal_task_callback* p);
//////////////////////////////////////////////////////////////////////////
struct mm_signal_task
{
	struct mm_signal_task_callback callback;
	pthread_t signal_thread;
	pthread_mutex_t signal_mutex;
	pthread_cond_t signal_cond;
	mm_msec_t msec_success_nearby;// nearby_time milliseconds for each time wait.default is MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC ms.
	mm_msec_t msec_failure_nearby;// nearby_time milliseconds for each time wait.default is MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC ms.
	mm_msec_t msec_delay;// delay milliseconds before first callback.default is 0 ms.
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_signal_task_init(struct mm_signal_task* p);
MM_EXPORT_DLL void mm_signal_task_destroy(struct mm_signal_task* p);
//////////////////////////////////////////////////////////////////////////
// lock to make sure the knot is thread safe.
MM_EXPORT_DLL void mm_signal_task_lock(struct mm_signal_task* p);
// unlock knot.
MM_EXPORT_DLL void mm_signal_task_unlock(struct mm_signal_task* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_signal_task_assign_callback(struct mm_signal_task* p,struct mm_signal_task_callback* cb);
MM_EXPORT_DLL void mm_signal_task_assign_success_nearby(struct mm_signal_task* p,mm_msec_t nearby);
MM_EXPORT_DLL void mm_signal_task_assign_failure_nearby(struct mm_signal_task* p,mm_msec_t nearby);
//////////////////////////////////////////////////////////////////////////
// wait.
MM_EXPORT_DLL void mm_signal_task_signal_wait(struct mm_signal_task* p);
// signal.
MM_EXPORT_DLL void mm_signal_task_signal(struct mm_signal_task* p);
//////////////////////////////////////////////////////////////////////////
// start wait thread.
MM_EXPORT_DLL void mm_signal_task_start(struct mm_signal_task* p);
// interrupt wait thread.
MM_EXPORT_DLL void mm_signal_task_interrupt(struct mm_signal_task* p);
// shutdown wait thread.
MM_EXPORT_DLL void mm_signal_task_shutdown(struct mm_signal_task* p);
// join wait thread.
MM_EXPORT_DLL void mm_signal_task_join(struct mm_signal_task* p);
//////////////////////////////////////////////////////////////////////////

#include "core/mm_suffix.h"

#endif//__mm_signal_task_h__
