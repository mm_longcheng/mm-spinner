#ifndef __mm_dynlib_h__
#define __mm_dynlib_h__

#include "core/mm_prefix.h"
#include "core/mm_core.h"

#if MM_PLATFORM == MM_PLATFORM_WIN32
#   if defined(_MSC_VER)
#       pragma warning(disable : 4552)  // warning: operator has no effect; expected operator with side-effect
#   endif
#   ifndef WIN32_LEAN_AND_MEAN
#   define WIN32_LEAN_AND_MEAN
#   endif//WIN32_LEAN_AND_MEAN
#   include <windows.h>
#    define MM_DYNLIB_HANDLE hInstance
#    define MM_DYNLIB_LOAD( a ) LoadLibraryEx( a, NULL, 0 ) // we can not use LOAD_WITH_ALTERED_SEARCH_PATH with relative paths
#    define MM_DYNLIB_GETSYM( a, b ) GetProcAddress( a, b )
#    define MM_DYNLIB_UNLOAD( a ) !FreeLibrary( a )

struct HINSTANCE__;
typedef struct HINSTANCE__* hInstance;

#elif MM_PLATFORM == MM_PLATFORM_WINRT
#  undef min
#  undef max
#  if defined( __MINGW32__ )
#    include <unistd.h>
#  endif
#    define MM_DYNLIB_HANDLE hInstance
#    define MM_DYNLIB_LOAD( a ) LoadPackagedLibrary( UTFString(a).asWStr_c_str(), 0 )
#    define MM_DYNLIB_GETSYM( a, b ) GetProcAddress( a, b )
#    define MM_DYNLIB_UNLOAD( a ) !FreeLibrary( a )

struct HINSTANCE__;
typedef struct HINSTANCE__* hInstance;

#elif MM_PLATFORM == MM_PLATFORM_LINUX || MM_PLATFORM == MM_PLATFORM_ANDROID || MM_PLATFORM == MM_PLATFORM_NACL || MM_PLATFORM == MM_PLATFORM_FLASHCC
extern "C" 
{
#   include <unistd.h>
#   include <dlfcn.h>
}
#    define MM_DYNLIB_HANDLE void*
#    define MM_DYNLIB_LOAD( a ) dlopen( a, RTLD_LAZY | RTLD_GLOBAL)
#    define MM_DYNLIB_GETSYM( a, b ) dlsym( a, b )
#    define MM_DYNLIB_UNLOAD( a ) dlclose( a )

#elif MM_PLATFORM == MM_PLATFORM_APPLE || MM_PLATFORM == MM_PLATFORM_APPLE_IOS
extern "C" 
{
#   include <unistd.h>
#   include <sys/param.h>
#   include <CoreFoundation/CoreFoundation.h>
#   include <dlfcn.h>
}
#    define MM_DYNLIB_HANDLE void*
#    define MM_DYNLIB_LOAD( a ) dlopen( a, RTLD_LAZY | RTLD_GLOBAL)
#    define FRAMEWORK_LOAD( a ) mac_loadFramework( a )
#    define MM_DYNLIB_GETSYM( a, b ) dlsym( a, b )
#    define MM_DYNLIB_UNLOAD( a ) dlclose( a )

#endif

#include "core/mm_suffix.h"

#endif//__mm_dynlib_h__
