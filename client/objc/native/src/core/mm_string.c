#include "core/mm_string.h"
#ifndef va_copy 
# ifdef __va_copy 
# define va_copy(DEST,SRC) __va_copy((DEST),(SRC)) 
# else 
# define va_copy(DEST, SRC) memcpy((&DEST), (&SRC), sizeof(va_list)) 
# endif 
#endif
// return empty char*.
static char __static_string_empty[1] = {0};
MM_EXPORT_DLL char* mm_string_empty()
{
	return __static_string_empty;
}

MM_EXPORT_DLL int mm_string_vsprintf( struct mm_string* p, const char *fmt, va_list ap )
{
	va_list args;
	int l;
	do 
	{
		// This line does not work with glibc 2.0. See `man snprintf'.
		// if buffer size is 0.l will a real len.
		va_copy(args, ap);
		l = mm_vsnprintf(p->s + p->l, p->m - p->l, fmt, args);
		va_end(args);
		if ( 0 < l && (size_t)(l + 1) > p->m - p->l ) 
		{
			if ( 0 != mm_string_realloc(p,p->l + l + 2) )
			{
				return EOF;
			}
			// write again.
			va_copy(args, ap);
			l = mm_vsnprintf(p->s + p->l, p->m - p->l, fmt, args);
			va_end(args);
		}
		if ( 0 > l )
		{
			// if -1 == l,we realloc m*2 buffer size.and retry.
			if ( 0 != mm_string_realloc(p,p->m * 2) )
			{
				return EOF;
			}
		}
	} while ( 0 > l );
	p->l += l;
	return l;
}

MM_EXPORT_DLL int mm_string_sprintf( struct mm_string* p, const char *fmt, ... )
{
	va_list ap;
	int l;
	va_start(ap, fmt);
	l = mm_string_vsprintf(p, fmt, ap);
	va_end(ap);
	return l;
}

MM_EXPORT_DLL char *mm_strtok(const char *str, const char *sep, struct mm_string_tokaux *aux)
{
	const char *p, *start;
	if (sep) { // set up the table
		if (str == 0 && (aux->tab[0]&1)) return 0; // no need to set up if we have finished
		aux->finished = 0;
		if (sep[1]) {
			aux->sep = -1;
			aux->tab[0] = aux->tab[1] = aux->tab[2] = aux->tab[3] = 0;
			for (p = sep; *p; ++p) aux->tab[*p>>6] |= 1ull<<(*p&0x3f);
		} else aux->sep = sep[0];
	}
	if (aux->finished) return 0;
	else if (str) aux->p = str - 1, aux->finished = 0;
	if (aux->sep < 0) {
		for (p = start = aux->p + 1; *p; ++p)
			if (aux->tab[*p>>6]>>(*p&0x3f)&1) break;
	} else {
		for (p = start = aux->p + 1; *p; ++p)
			if (*p == aux->sep) break;
	}
	aux->p = p; // end of token
	if (*p == 0) aux->finished = 1; // no more tokens
	return (char*)start;
}

// s MUST BE a null terminated string; l = mm_strlen(s)
MM_EXPORT_DLL int mm_split_core(char *s, int delimiter, int *_max, int **_offsets)
{
	int i, n, max, last_char, last_start, *offsets, l;
	n = 0; max = *_max; offsets = *_offsets;
	l = (int)mm_strlen(s);
	
#define __mm_split_aux do {						\
		if (_offsets) {						\
			s[i] = 0;					\
			if (n == max) {					\
				int *tmp;				\
				max = max? max<<1 : 2;			\
				if ((tmp = (int*)mm_realloc(offsets, sizeof(int) * max))) {  \
					offsets = tmp;			\
				} else	{				\
					mm_free(offsets);			\
					*_offsets = NULL;		\
					return 0;			\
				}					\
			}						\
			offsets[n++] = last_start;			\
		} else ++n;						\
	} while (0)

	for (i = 0, last_char = last_start = 0; i <= l; ++i) {
		if (delimiter == 0) {
			if (mm_isspace(s[i]) || s[i] == 0) {
				if (mm_isgraph(last_char)) __mm_split_aux; // the end of a field
			} else {
				if (mm_isspace(last_char) || last_char == 0) last_start = i;
			}
		} else {
			if (s[i] == delimiter || s[i] == 0) {
				if (last_char != 0 && last_char != delimiter) __mm_split_aux; // the end of a field
			} else {
				if (last_char == delimiter || last_char == 0) last_start = i;
			}
		}
		last_char = s[i];
	}
	*_max = max; *_offsets = offsets;
	return n;
}
MM_EXPORT_DLL void mm_string_init(struct mm_string* p)
{
	p->l = 0;
	p->m = MM_STRING_DEFAULT_MAX_SIZE;
	p->s = (char*)mm_malloc(p->m);
	mm_memset(p->s,0,p->m);
}
MM_EXPORT_DLL void mm_string_destroy(struct mm_string* p)
{
	mm_free(p->s);
}
MM_EXPORT_DLL int mm_string_assign(struct mm_string* p,struct mm_string* s)
{
	return mm_string_assignsn(p,s->s,s->l);
}
MM_EXPORT_DLL int mm_string_assigns( struct mm_string* p, const char* s )
{
	return mm_string_assignsn(p,s,mm_strlen(s));
}
MM_EXPORT_DLL int mm_string_assignsn( struct mm_string* p, const char* s, size_t l )
{
	if (l + 1 > p->m) 
	{
		if ( 0 != mm_string_realloc(p,l + 1) ) 
		{
			return EOF;
		}
	}
	mm_memcpy(p->s, s, l);
	p->l = l;
	p->s[p->l] = 0;
	return (int)l;
}
MM_EXPORT_DLL int mm_string_getline( struct mm_string* p, mm_string_gets_func* fgets_func, void* fp )
{
	size_t l0 = p->l;

	while (p->l == l0 || p->s[p->l-1] != '\n') 
	{
		if (p->m - p->l < 200)
		{
			if ( 0 != mm_string_realloc(p,p->m + 200) )
			{
				return EOF;
			}
		}
		if (fgets_func(p->s + p->l, (int)(p->m - p->l), fp) == NULL) break;
		p->l += mm_strlen(p->s + p->l);
	}

	if (p->l == l0) return EOF;

	if (p->l > l0 && p->s[p->l-1] == '\n') 
	{
		p->l--;
		if (p->l > l0 && p->s[p->l-1] == '\r') p->l--;
	}
	p->s[p->l] = '\0';
	return 0;
}

/**********************
 * Boyer-Moore search *
 **********************/

// reference: http://www-igm.univ-mlv.fr/~lecroq/string/node14.html
static int *mm_string_bm_prep(const mm_uchar_t *pat, int m)
{
	int i, *suff, *prep, *bmGs, *bmBc;
	prep = (int*)mm_calloc(m + 256, sizeof(int));
	bmGs = prep; bmBc = prep + m;
	{ // preBmBc()
		for (i = 0; i < 256; ++i) bmBc[i] = m;
		for (i = 0; i < m - 1; ++i) bmBc[pat[i]] = m - i - 1;
	}
	suff = (int*)mm_calloc(m, sizeof(int));
	{ // suffixes()
		int f = 0, g;
		suff[m - 1] = m;
		g = m - 1;
		for (i = m - 2; i >= 0; --i) 
		{
			if (i > g && suff[i + m - 1 - f] < i - g)
			{
				suff[i] = suff[i + m - 1 - f];
			}
			else 
			{
				if (i < g) g = i;
				f = i;
				while (g >= 0 && pat[g] == pat[g + m - 1 - f]) --g;
				suff[i] = f - g;
			}
		}
	}
	{ 
		// preBmGs()
		int j = 0;
		for (i = 0; i < m; ++i) bmGs[i] = m;
		for (i = m - 1; i >= 0; --i)
		{
			if (suff[i] == i + 1)
			{
				for (; j < m - 1 - i; ++j)
				{
					if (bmGs[j] == m)
					{
						bmGs[j] = m - 1 - i;
					}
				}
			}
		}
		for (i = 0; i <= m - 2; ++i)
		{
			bmGs[m - 1 - suff[i]] = m - 1 - i;
		}
	}
	mm_free(suff);
	return prep;
}

MM_EXPORT_DLL void *mm_memmem(const void *_str, int n, const void *_pat, int m, int **_prep)
{
	int i, j, *prep = 0, *bmGs, *bmBc;
	const mm_uchar_t *str, *pat;
	str = (const mm_uchar_t*)_str; pat = (const mm_uchar_t*)_pat;
	prep = (_prep == 0 || *_prep == 0)? mm_string_bm_prep(pat, m) : *_prep;
	if (_prep && *_prep == 0) *_prep = prep;
	bmGs = prep; bmBc = prep + m;
	j = 0;
	while (j <= n - m) 
	{
		for (i = m - 1; i >= 0 && pat[i] == str[i+j]; --i);
		if (i >= 0) 
		{
			int max = bmBc[str[i+j]] - m + 1 + i;
			if (max < bmGs[i]) max = bmGs[i];
			j += max;
		} 
		else 
		{
			return (void*)(str + j);
		}
	}
	if (_prep == 0) mm_free(prep);
	return 0;
}

MM_EXPORT_DLL char *mm_strstr(const char *str, const char *pat, int **_prep)
{
	return (char*)mm_memmem(str, (int)mm_strlen(str), pat, (int)mm_strlen(pat), _prep);
}

MM_EXPORT_DLL char *mm_strnstr(const char *str, const char *pat, int n, int **_prep)
{
	return (char*)mm_memmem(str, n, pat, (int)mm_strlen(pat), _prep);
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL char* mm_string_c_str( struct mm_string* p )
{
	return p->s;
}

MM_EXPORT_DLL size_t mm_string_size( struct mm_string* p )
{
	return p->l;
}
MM_EXPORT_DLL int mm_string_puts( struct mm_string* p,const char* s )
{
	return mm_string_putsn(p, s, (int)mm_strlen(s));
}

MM_EXPORT_DLL int mm_string_realloc( struct mm_string* p, size_t size )
{
	if (p->m < size) 
	{
		char* s = NULL;
		p->m = size;
		mm_roundup32(p->m);
		s = (char*)mm_realloc(p->s, p->m);
		if (NULL != s)
		{
			p->s = s;
		}
		else
		{
			return EOF;
		}
	}
	return 0;
}
MM_EXPORT_DLL int mm_string_resize( struct mm_string* p, size_t size )
{
	int rt = mm_string_realloc(p,size);
	if (0 == rt)
	{
		p->l = size;
	}
	return rt;
}
MM_EXPORT_DLL void mm_string_clear( struct mm_string* p )
{
	p->l = 0;
}

// Give ownership of the underlying buffer away to something else (making
// that something else responsible for freeing it), leaving the struct mm_string
// empty and ready to be used again, or ready to go out of scope without
// needing  free(str.s)  to prevent a memory leak.
MM_EXPORT_DLL char *mm_string_release( struct mm_string* p )
{
	char* ss = p->s;
	p->l = p->m = 0;
	p->s = NULL;
	return ss;
}

MM_EXPORT_DLL int mm_string_putsn( struct mm_string* p,const char* s, int l )
{
	if (p->l + l + 1 >= p->m) 
	{
		if ( 0 != mm_string_realloc(p,p->l + l + 2) )
		{
			return EOF;
		}
	}
	mm_memcpy(p->s + p->l, s, l);
	p->l += l;
	p->s[p->l] = 0;
	return l;
}

MM_EXPORT_DLL int mm_string_putc( struct mm_string* p, int c )
{
	if (p->l + 1 >= p->m) 
	{
		if ( 0 != mm_string_realloc(p,p->l + 2) )
		{
			return EOF;
		}
	}
	p->s[p->l++] = c;
	p->s[p->l] = 0;
	return c;
}

MM_EXPORT_DLL int mm_string_putc_( struct mm_string* p, int c )
{
	if (p->l + 1 > p->m) 
	{
		if ( 0 != mm_string_realloc(p,p->l + 1) )
		{
			return EOF;
		}
	}
	p->s[p->l++] = c;
	return 1;
}

MM_EXPORT_DLL int mm_string_putsn_( struct mm_string* p, const void* s, int l )
{
	if (p->l + l > p->m) 
	{
		if ( 0 != mm_string_realloc(p,p->l + l) )
		{
			return EOF;
		}
	}
	mm_memcpy(p->s + p->l, s, l);
	p->l += l;
	return l;
}

MM_EXPORT_DLL int mm_string_putw( struct mm_string* p, int c )
{
	char buf[16];
	int i, l = 0;
	unsigned int x = c;
	// if (c < 0) x = -x; warning c4146.
	if (c < 0) x = -c;
	do { buf[l++] = x%10 + '0'; x /= 10; } while (x > 0);
	if (c < 0) buf[l++] = '-';
	if (p->l + l + 1 >= p->m) 
	{
		if ( 0 != mm_string_realloc(p,p->l + l + 2) )
		{
			return EOF;
		}
	}
	for (i = l - 1; i >= 0; --i) p->s[p->l++] = buf[i];
	p->s[p->l] = 0;
	return 0;
}

MM_EXPORT_DLL int mm_string_putuw( struct mm_string* p, unsigned c )
{
	char buf[16];
	int l, i;
	unsigned x;
	if (c == 0) return mm_string_putc(p, '0');
	for (l = 0, x = c; x > 0; x /= 10) buf[l++] = x%10 + '0';
	if (p->l + l + 1 >= p->m) 
	{
		if ( 0 != mm_string_realloc(p,p->l + l + 2) )
		{
			return EOF;
		}
	}
	for (i = l - 1; i >= 0; --i) p->s[p->l++] = buf[i];
	p->s[p->l] = 0;
	return 0;
}

MM_EXPORT_DLL int mm_string_putl( struct mm_string* p, long c )
{
	char buf[32];
	int i, l = 0;
	unsigned long x = c;
	// if (c < 0) x = -x; warning c4146.
	if (c < 0) x = -c;
	do { buf[l++] = x%10 + '0'; x /= 10; } while (x > 0);
	if (c < 0) buf[l++] = '-';
	if (p->l + l + 1 >= p->m) 
	{
		if ( 0 != mm_string_realloc(p,p->l + l + 2) )
		{
			return EOF;
		}
	}
	for (i = l - 1; i >= 0; --i) p->s[p->l++] = buf[i];
	p->s[p->l] = 0;
	return 0;
}

/*
 * Returns 's' split by delimiter, with *n being the number of components;
 *         NULL on failue.
 */
MM_EXPORT_DLL int* mm_string_split( struct mm_string* p, int delimiter, int* n )
{
	int max = 0, *offsets = 0;
	*n = mm_split_core(p->s, delimiter, &max, &offsets);
	return offsets;
}
// return 0 is equal.
MM_EXPORT_DLL int mm_string_compare( struct mm_string* p, struct mm_string* t )
{
	return ( p->l != t->l || 0 != mm_strcmp( p->s, t->s ) ) ? 1 : 0;
}
// return 0 is equal.
MM_EXPORT_DLL int mm_string_compare_c_str( struct mm_string* p, const char* t )
{
	return ( p->l != mm_strlen(t) || 0 != mm_strcmp( p->s, t ) ) ? 1 : 0;
}
//////////////////////////////////////////////////////////////////////////
/***********************
 * The main() function *
 ***********************/

#ifdef KSTRING_MAIN
#include <stdio.h>
int main()
{
	struct mm_string *s;
	int *fields, n, i;
	struct mm_stringokaux_t aux;
	char *p;
	s = (struct mm_string*)mm_calloc(1, sizeof(struct mm_string));
	// test ksprintf()
	mm_sprintf(s, " abcdefg:    %d ", 100);
	mm_printf("'%s'\n", s->s);
	// test ksplit()
	fields = mm_string_split(s, 0, &n);
	for (i = 0; i < n; ++i)
		mm_printf("field[%d] = '%s'\n", i, s->s + fields[i]);
	// test kstrtok()
	s->l = 0;
	for (p = mm_strtok("ab:cde:fg/hij::k", ":/", &aux); p; p = mm_strtok(0, 0, &aux)) {
		mm_string_putsn(s, p, aux.p - p);
		mm_string_putc(s, '\n');
	}
	mm_printf("%s", s->s);
	// mm_free
	mm_free(s->s); mm_free(s); mm_free(fields);

	{
		static char *str = "abcdefgcdgcagtcakcdcd";
		static char *pat = "cd";
		char *ret, *s = str;
		int *prep = 0;
		while ((ret = mm_strstr(s, pat, &prep)) != 0) {
			mm_printf("match: %s\n", ret);
			s = ret + prep[0];
		}
		mm_free(prep);
	}
	return 0;
}
#endif
