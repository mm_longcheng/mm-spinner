#ifndef __mm_spinlock_h__
#define __mm_spinlock_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_atomic.h"

MM_EXPORT_DLL void mm_spin_lock_impl(mm_atomic_t *lock, mm_atomic_int_t value, mm_uint_t spin);

#define mm_spin_init(lock,spin) (*lock = 0)
#define mm_spin_destroy(lock) (*lock = 0)
#define mm_spin_lock(lock) mm_spin_lock_impl(lock,1,1000)
#define mm_spin_trylock(lock) (*(lock) == 0 && mm_atomic_cmp_set(lock, 0, 1))
#define mm_spin_unlock(lock) (*(lock) = 0)

#include "core/mm_suffix.h"

#endif//__mm_spinlock_h__