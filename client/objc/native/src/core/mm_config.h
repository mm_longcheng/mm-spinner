#ifndef __mm_config_h__
#define __mm_config_h__

#include "core/mm_prefix.h"

#include "core/mm_config_platform.h"
#include "core/mm_platform.h"
#include "core/mm_types.h"

// #define MM_WIN32 1
// #define MM_LINUX 2
// #define MM_APPLE 3
// #define MM_APPLE_IOS 4
// #define MM_ANDROID 5
// #define MM_NACL 6
// #define MM_WINRT 7
// #define MM_FLASHCC 8
// #define MM_FREEBSD 9

#ifndef MM_HAVE_SO_SNDLOWAT
#define MM_HAVE_SO_SNDLOWAT     1
#endif

#if !(MM_WIN32)

#define mm_signal_helper(n)     SIG##n
#define mm_signal_value(n)      mm_signal_helper(n)

#define mm_random               random

/* TODO: #ifndef */
#define MM_SHUTDOWN_SIGNAL      QUIT
#define MM_TERMINATE_SIGNAL     TERM
#define MM_NOACCEPT_SIGNAL      WINCH
#define MM_RECONFIGURE_SIGNAL   HUP

#if (MM_LINUXTHREADS)
#define MM_REOPEN_SIGNAL        INFO
#define MM_CHANGEBIN_SIGNAL     XCPU
#else
#define MM_REOPEN_SIGNAL        USR1
#define MM_CHANGEBIN_SIGNAL     USR2
#endif

#define mm_cdecl
#define mm_libc_cdecl

#endif

#define MM_DEBUG MM_DEBUG_MODE

#define MM_INT32_LEN   (sizeof("-2147483648") - 1)
#define MM_INT64_LEN   (sizeof("-9223372036854775808") - 1)

#ifndef MM_ALIGNMENT
#define MM_ALIGNMENT   sizeof(unsigned long)    /* platform word */
#endif

#if (MM_PTR_SIZE == 4)
#define MM_INT_T_LEN		MM_INT32_LEN
#define MM_MAX_INT_T_VALUE  2147483647

#else
#define MM_INT_T_LEN		MM_INT64_LEN
#define MM_MAX_INT_T_VALUE  9223372036854775807
#endif

#define mm_align(d, a)     (((d) + (a - 1)) & ~(a - 1))
#define mm_align_ptr(p, a)                                                   \
	(mm_uchar_t *) (((uintptr_t) (p) + ((uintptr_t) a - 1)) & ~((uintptr_t) a - 1))

#define mm_abort       abort

#define  MM_SUCCESS    0
#define  MM_UNKNOWN    -1

#define  MM_OK          0
#define  MM_ERROR      -1
#define  MM_AGAIN      -2
#define  MM_BUSY       -3
#define  MM_DONE       -4
#define  MM_DECLINED   -5
#define  MM_ABORT      -6

#define LF     (mm_uchar_t) '\n'
#define CR     (mm_uchar_t) '\r'
#define CRLF   "\r\n"

/* TODO: platform specific: array[MM_INVALID_ARRAY_INDEX] must cause SIGSEGV */
#define MM_INVALID_ARRAY_INDEX 0x80000000

/* TODO: auto_conf: ngx_inline   inline __inline __inline__ */
#ifndef mm_inline
#define mm_inline      inline
#endif

#ifndef INADDR_NONE  /* Solaris */
#define INADDR_NONE  ((unsigned int) -1)
#endif

#ifdef MAXHOSTNAMELEN
#define MM_MAXHOSTNAMELEN  MAXHOSTNAMELEN
#else
#define MM_MAXHOSTNAMELEN  256
#endif


#if ((__GNU__ == 2) && (__GNUC_MINOR__ < 8))
#define MM_MAX_UINT32_VALUE  (mm_uint32_t) 0xffffffffLL
#else
#define MM_MAX_UINT32_VALUE  (mm_uint32_t) 0xffffffff
#endif

#define MM_MAX_INT32_VALUE   (mm_uint32_t) 0x7fffffff

typedef pid_t mm_pid_t;
//
#ifndef mm_unused
#if (defined __clang__ && __clang_major__ >= 3) || (defined __GNUC__ && __GNUC__ >= 3)
#define mm_unused __attribute__ ((__unused__))
#else
#define mm_unused
#endif
#endif /* mm_unused */

static const char MM_EOF_VALUE = EOF;

#include "core/mm_suffix.h"

#endif//__mm_config_h__