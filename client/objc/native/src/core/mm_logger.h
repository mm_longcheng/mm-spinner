#ifndef __mm_logger_h__
#define __mm_logger_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

#include "core/mm_string.h"
#include "core/mm_errno.h"

#include <pthread.h>

// [1992/01/26 09:13:14-520 8 V ]

typedef enum
{
	MM_LOG_UNKNOW   = 0,
	MM_LOG_FATAL    = 1,
	MM_LOG_CRIT     = 2,
	MM_LOG_ERROR    = 3,
	MM_LOG_ALERT    = 4,
	MM_LOG_WARNING  = 5,
	MM_LOG_NOTICE   = 6,
	MM_LOG_INFO     = 7,
	MM_LOG_TRACE    = 8,
	MM_LOG_DEBUG    = 9,
	MM_LOG_VERBOSE  =10,
}mm_logger_level_t;

struct mm_level_mark
{
	struct mm_string m;// mark
	struct mm_string n;// name
};

MM_EXPORT_DLL const struct mm_level_mark* mm_logger_level_mark(int lvl);

struct mm_logger;

typedef void (*logger_callback)(struct mm_logger* p, mm_uint32_t lvl, const char* message);

struct mm_logger
{
	logger_callback callback;
	void* obj;//weak ref for obj.
};
// this length is recommend size,actually not limit here.
#define MM_MAX_LOGGER_LENGTH 1024
#define MM_MAX_LOGGER_FILE_NAME_LENGTH 512
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL extern struct mm_logger* mm_logger_instance();
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_logger_init(struct mm_logger* p);
MM_EXPORT_DLL void mm_logger_destroy(struct mm_logger* p);
// assign callback.
MM_EXPORT_DLL void mm_logger_assign_callback(struct mm_logger* p, logger_callback callback, void* obj);

MM_EXPORT_DLL void mm_logger_message(struct mm_logger* p,mm_uint32_t lvl,const char* fmt, ...) MM_STRING_ATTR_PRINTF(3,4);
// default callback.
MM_EXPORT_DLL void mm_logger_callback_printf(struct mm_logger* p, mm_uint32_t lvl, const char* message);
//
#define mm_logger_log_U(p,...) mm_logger_message(p,MM_LOG_UNKNOW  , __VA_ARGS__)
#define mm_logger_log_F(p,...) mm_logger_message(p,MM_LOG_FATAL   , __VA_ARGS__)
#define mm_logger_log_C(p,...) mm_logger_message(p,MM_LOG_CRIT    , __VA_ARGS__)
#define mm_logger_log_E(p,...) mm_logger_message(p,MM_LOG_ERROR   , __VA_ARGS__)
#define mm_logger_log_A(p,...) mm_logger_message(p,MM_LOG_ALERT   , __VA_ARGS__)
#define mm_logger_log_W(p,...) mm_logger_message(p,MM_LOG_WARNING , __VA_ARGS__)
#define mm_logger_log_N(p,...) mm_logger_message(p,MM_LOG_NOTICE  , __VA_ARGS__)
#define mm_logger_log_I(p,...) mm_logger_message(p,MM_LOG_INFO    , __VA_ARGS__)
#define mm_logger_log_T(p,...) mm_logger_message(p,MM_LOG_TRACE   , __VA_ARGS__)
#define mm_logger_log_D(p,...) mm_logger_message(p,MM_LOG_DEBUG   , __VA_ARGS__)
#define mm_logger_log_V(p,...) mm_logger_message(p,MM_LOG_VERBOSE , __VA_ARGS__)

#define mm_assert assert
//////////////////////////////////////////////////////////////////////////
// init the mm_logger_instance before use this global macro.
#define mm_log_init() mm_logger_init(mm_logger_instance())
#define mm_log_destroy() mm_logger_destroy(mm_logger_instance())
#define mm_log_U(...) mm_logger_log_U(mm_logger_instance(), __VA_ARGS__)
#define mm_log_F(...) mm_logger_log_F(mm_logger_instance(), __VA_ARGS__)
#define mm_log_C(...) mm_logger_log_C(mm_logger_instance(), __VA_ARGS__)
#define mm_log_E(...) mm_logger_log_E(mm_logger_instance(), __VA_ARGS__)
#define mm_log_A(...) mm_logger_log_A(mm_logger_instance(), __VA_ARGS__)
#define mm_log_W(...) mm_logger_log_W(mm_logger_instance(), __VA_ARGS__)
#define mm_log_N(...) mm_logger_log_N(mm_logger_instance(), __VA_ARGS__)
#define mm_log_I(...) mm_logger_log_I(mm_logger_instance(), __VA_ARGS__)
#define mm_log_T(...) mm_logger_log_T(mm_logger_instance(), __VA_ARGS__)
#define mm_log_D(...) mm_logger_log_D(mm_logger_instance(), __VA_ARGS__)
#define mm_log_V(...) mm_logger_log_V(mm_logger_instance(), __VA_ARGS__)
//////////////////////////////////////////////////////////////////////////
#define SOCKET_ERROR_BUFFER_SIZE 1024
//
MM_EXPORT_DLL void mm_logger_socket_error(struct mm_logger* p, mm_uint32_t lvl, mm_err_t errcode);

#include "core/mm_suffix.h"

#endif//__mm_logger_h__