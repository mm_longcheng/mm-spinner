
/*
 * Copyright (C) Igor Sysoev
 * Copyright (C) Nginx, Inc.
 */
#include "core/mm_spinlock.h"
#include "core/mm_os_context.h"
#include "core/mm_process.h"

MM_EXPORT_DLL void mm_spin_lock_impl(mm_atomic_t *lock, mm_atomic_int_t value, mm_uint_t spin)
{

#if (MM_HAVE_ATOMIC_OPS)

    mm_uint_t  i, n;

    struct mm_os_context* g_os_context = mm_os_context_instance();
    for ( ;; ) 
    {
        if (*lock == 0 && mm_atomic_cmp_set(lock, 0, value)) 
        {
            return;
        }
        if (g_os_context->cores > 1) 
        {
            for (n = 1; n < spin; n <<= 1) 
            {
                for (i = 0; i < n; i++) 
                {
                    mm_cpu_pause();
                }
                if (*lock == 0 && mm_atomic_cmp_set(lock, 0, value)) 
                {
                    return;
                }
            }
        }

        mm_sched_yield();
    }

#else

#if (MM_THREADS)

#error mm_spin_lock_impl() or mm_atomic_cmp_set() are not defined !

#endif

#endif

}
