#ifndef __mm_limit_h__
#define __mm_limit_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
//
static const mm_uchar_t 	MM_CHAR_BIT 	= CHAR_BIT;
//
static const mm_char_t 		MM_CHAR_MAX 	= CHAR_MAX;
static const mm_char_t 		MM_CHAR_MIN 	= CHAR_MIN;
///////////////////////////////////////////////////////////
static const mm_schar_t 	MM_SCHAR_MAX 	= SCHAR_MAX;
static const mm_schar_t 	MM_SCHAR_MIN 	= SCHAR_MIN;

static const mm_uchar_t 	MM_UCHAR_MAX 	= UCHAR_MAX;
static const mm_uchar_t 	MM_UCHAR_MIN 	= 0;
//
static const mm_sshort_t 	MM_SSHRT_MAX 	= SHRT_MAX;
static const mm_sshort_t 	MM_SSHRT_MIN 	= SHRT_MIN;

static const mm_ushort_t 	MM_USHRT_MAX 	= USHRT_MAX;
static const mm_ushort_t 	MM_USHRT_MIN 	= 0;
//
static const mm_sint_t 		MM_SINT_MAX 	= INT_MAX;
static const mm_sint_t 		MM_SINT_MIN 	= INT_MIN;

static const mm_uint_t 		MM_UINT_MAX 	= UINT_MAX;
static const mm_uint_t 		MM_UINT_MIN 	= 0;
//
static const mm_slong_t 	MM_SLONG_MAX 	= LONG_MAX;
static const mm_slong_t 	MM_SLONG_MIN 	= LONG_MIN;

static const mm_ulong_t 	MM_ULONG_MAX 	= ULONG_MAX;
static const mm_ulong_t 	MM_ULONG_MIN 	= 0;
//
static const mm_sllong_t 	MM_SLLONG_MAX 	= LLONG_MAX;
static const mm_sllong_t 	MM_SLLONG_MIN 	= LLONG_MIN;

static const mm_ullong_t 	MM_ULLONG_MAX 	= ULLONG_MAX;
static const mm_ullong_t 	MM_ULLONG_MIN 	= 0;
///////////////////////////////////////////////////////////
static const mm_sint8_t 	MM_SINT8_MAX 	= (2^7)-1;
static const mm_sint8_t 	MM_SINT8_MIN 	= 1-(2^7);

static const mm_uint8_t 	MM_UINT8_MAX 	= (2^8)-1;
static const mm_uint8_t 	MM_UINT8_MIN 	= 0;
//
static const mm_sint16_t 	MM_SINT16_MAX 	= (2^15)-1;
static const mm_sint16_t 	MM_SINT16_MIN 	= 1-(2^15);

static const mm_uint16_t 	MM_UINT16_MAX 	= (2^16)-1;
static const mm_uint16_t 	MM_UINT16_MIN 	= 0;
//
static const mm_sint32_t 	MM_SINT32_MAX 	= (2^31)-1;
static const mm_sint32_t 	MM_SINT32_MIN 	= 1-(2^31);

static const mm_uint32_t 	MM_UINT32_MAX 	= (2^32)-1;
static const mm_uint32_t 	MM_UINT32_MIN 	= 0;
//
static const mm_sint64_t 	MM_SINT64_MAX 	= (2^63)-1;
static const mm_sint64_t 	MM_SINT64_MIN 	= 1-(2^63);

static const mm_uint64_t 	MM_UINT64_MAX 	= (2^64)-1;
static const mm_uint64_t 	MM_UINT64_MIN 	= 0;
//
#include "core/mm_suffix.h"

#endif//__mm_limit_h__