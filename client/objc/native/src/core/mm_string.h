#ifndef __mm_string_h__
#define __mm_string_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

#include "core/mm_bit.h"
#include "core/mm_alloc.h"


#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ > 4)
#	define MM_STRING_ATTR_PRINTF(fmt, arg) __attribute__((__format__ (__printf__, fmt, arg)))
#else
#	define MM_STRING_ATTR_PRINTF(fmt, arg)
#endif

#define mm_strlen strlen
#define mm_strcpy strcpy
#define mm_printf printf
#define mm_sscanf sscanf

#if (MM_WIN32)
#	define mm_vsnprintf vsnprintf
#	define mm_snprintf _snprintf
#else
#	define mm_vsnprintf vsnprintf
#	define mm_snprintf snprintf
#endif

#define mm_vsprintf vsprintf
#define mm_sprintf sprintf

#define mm_isspace isspace
#define mm_isgraph isgraph

#define mm_strcmp strcmp
#define mm_strncmp strncmp
/* struct mm_string is a simple non-opaque type whose fields are likely to be
 * used directly by user code (but see also mm_string_str() and mm_string_len() below).
 * A struct mm_string object is initialised by either of
 *       struct mm_string str = { 0, 0, NULL };
 *       struct mm_string str; ...; str.l = str.m = 0; str.s = NULL;
 * and either ownership of the underlying buffer should be given away before
 * the object disappears (see mm_string_release() below) or the struct mm_string should be
 * destroyed with  free(str.s);  */
struct mm_string 
{
	size_t l;// string length.
	size_t m;// memory length.
	char *s;//
};

struct mm_string_tokaux
{
	uint64_t tab[4];
	int sep, finished;
	const char *p; // end of the current token
};
// char* empty string.is global const do not try change it.
MM_EXPORT_DLL char* mm_string_empty();
// string default size.
#define MM_STRING_DEFAULT_MAX_SIZE 16
// string null.
#define mm_string_null     { 0, 0, NULL}
#define mm_string_make(str)     { sizeof(str) - 1, sizeof(str) - 1, (char*) str }
///
MM_EXPORT_DLL int mm_string_vsprintf( struct mm_string* p, const char *fmt, va_list ap ) MM_STRING_ATTR_PRINTF(2,0);
MM_EXPORT_DLL int mm_string_sprintf( struct mm_string* p, const char *fmt, ... ) MM_STRING_ATTR_PRINTF(2,3);
MM_EXPORT_DLL int mm_split_core(char *s, int delimiter, int *_max, int **_offsets);
MM_EXPORT_DLL char *mm_strstr(const char *str, const char *pat, int **_prep);
MM_EXPORT_DLL char *mm_strnstr(const char *str, const char *pat, int n, int **_prep);
MM_EXPORT_DLL void *mm_memmem(const void *_str, int n, const void *_pat, int m, int **_prep);

/* mm_strtok() is similar to strtok_r() except that str is not
	* modified and both str and sep can be NULL. For efficiency, it is
	* actually recommended to set both to NULL in the subsequent calls
	* if sep is not changed. */
MM_EXPORT_DLL char *mm_strtok(const char *str, const char *sep, struct mm_string_tokaux *aux);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_string_init( struct mm_string* p );
MM_EXPORT_DLL void mm_string_destroy( struct mm_string* p );
MM_EXPORT_DLL int mm_string_assign( struct mm_string* p, struct mm_string* s);
MM_EXPORT_DLL int mm_string_assigns( struct mm_string* p, const char* s );
MM_EXPORT_DLL int mm_string_assignsn( struct mm_string* p, const char* s, size_t l );
//////////////////////////////////////////////////////////////////////////
/* mm_getline() uses the supplied fgets()-like function to read a "\n"-
	* or "\r\n"-terminated line from fp.  The line read is appended to the
	* kstring without its terminator and 0 is returned; EOF is returned at
	* EOF or on error (determined by querying fp, as per fgets()). */
typedef char *(mm_string_gets_func)(char *, int, void *);
MM_EXPORT_DLL int mm_string_getline( struct mm_string* p, mm_string_gets_func* fgets_func, void* fp );

MM_EXPORT_DLL char* mm_string_c_str( struct mm_string* p );
MM_EXPORT_DLL size_t mm_string_size( struct mm_string* p );

MM_EXPORT_DLL int mm_string_puts( struct mm_string* p,const char* s );
// put the cursor to origin.
#define mm_string_append mm_string_puts

MM_EXPORT_DLL int mm_string_realloc( struct mm_string* p, size_t size );
MM_EXPORT_DLL int mm_string_resize( struct mm_string* p, size_t size );
// put the cursor to origin.
MM_EXPORT_DLL void mm_string_clear( struct mm_string* p );
// Give ownership of the underlying buffer away to something else (making
// that something else responsible for freeing it), leaving the struct mm_string
// empty and ready to be used again, or ready to go out of scope without
// needing  free(str.s)  to prevent a memory leak.
MM_EXPORT_DLL char* mm_string_release( struct mm_string* p );

MM_EXPORT_DLL int mm_string_putsn( struct mm_string* p,const char* s, int l );

MM_EXPORT_DLL int mm_string_putc( struct mm_string* p, int c );

MM_EXPORT_DLL int mm_string_putc_( struct mm_string* p, int c );

MM_EXPORT_DLL int mm_string_putsn_( struct mm_string* p, const void* s, int l );

MM_EXPORT_DLL int mm_string_putw( struct mm_string* p, int c );

MM_EXPORT_DLL int mm_string_putuw( struct mm_string* p, unsigned c );

MM_EXPORT_DLL int mm_string_putl( struct mm_string* p, long c );
/*
 * Returns 's' split by delimiter, with *n being the number of components;
 *         NULL on failue.
 */
MM_EXPORT_DLL int* mm_string_split( struct mm_string* p, int delimiter, int* n );
// return 0 is equal.
MM_EXPORT_DLL int mm_string_compare( struct mm_string* p, struct mm_string* t );
// return 0 is equal.
MM_EXPORT_DLL int mm_string_compare_c_str( struct mm_string* p, const char* t );

#define mm_tolower(c)      (mm_uchar_t) ((c >= 'A' && c <= 'Z') ? (c |  0x20) : c)
#define mm_toupper(c)      (mm_uchar_t) ((c >= 'a' && c <= 'z') ? (c & ~0x20) : c)

#include "core/mm_suffix.h"

#endif//__mm_string_h__
