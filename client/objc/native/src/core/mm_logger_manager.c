﻿#include "core/mm_logger_manager.h"
#include "core/mm_os_context.h"
#include "core/mm_time_cache.h"

//////////////////////////////////////////////////////////////////////////
static void __static_logger_manager_logger_callback(struct mm_logger* p, mm_uint32_t lvl, const char* message);
static void* __static_logger_manager_thread(void* arg);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_logger_manager_callback_init(struct mm_logger_manager_callback* p)
{
	p->console_printf = &mm_os_context_logger_console;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_logger_manager_callback_destroy(struct mm_logger_manager_callback* p)
{
	p->console_printf = &mm_os_context_logger_console;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
static struct mm_logger_manager g_logger_manager;
MM_EXPORT_DLL struct mm_logger_manager* mm_logger_manager_instance()
{
	return &g_logger_manager;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_logger_manager_init(struct mm_logger_manager* p)
{
	struct mm_logger* g_logger = mm_logger_instance();
	//
	p->logger_impl = g_logger;
	p->stream = NULL;
	mm_string_init(&p->logger_section);
	mm_string_init(&p->logger_path);
	mm_string_init(&p->file_name);
	mm_string_init(&p->current_file_prefix);
	mm_string_init(&p->current_file_name);
	mm_logger_manager_callback_init(&p->callback);

	mm_spin_init(&p->locker,NULL);
	mm_spin_init(&p->logger_mutex,NULL);

	pthread_mutex_init(&p->signal_mutex, NULL);  
	pthread_cond_init(&p->signal_cond, NULL);

	p->rolling_index = 100;
	p->current_index = 0;
	p->file_size = 300 * 1024 * 1024;
	//
	p->logger_level = MM_LOG_VERBOSE;
	p->check_file_size_time = 5000;
	//
	p->is_rolling = 1;
	p->is_console = 0;
	p->is_immediately = 0;
	p->state = ts_closed;
	//
	mm_string_assigns(&p->logger_section,"mm");
	mm_string_assigns(&p->logger_path,"log");
	mm_string_assigns(&p->file_name,"mm");
	//
	mm_logger_assign_callback(p->logger_impl,&__static_logger_manager_logger_callback,p);
}
MM_EXPORT_DLL void mm_logger_manager_destroy(struct mm_logger_manager* p)
{
	struct mm_logger* g_logger = mm_logger_instance();
	//
	mm_logger_assign_callback(p->logger_impl,&mm_logger_callback_printf,p);
	//
	mm_logger_manager_flush(p);
	mm_logger_manager_close(p);
	//
	p->logger_impl = g_logger;
	p->stream = NULL;
	mm_string_destroy(&p->logger_section);
	mm_string_destroy(&p->logger_path);
	mm_string_destroy(&p->file_name);
	mm_string_destroy(&p->current_file_prefix);
	mm_string_destroy(&p->current_file_name);
	mm_logger_manager_callback_destroy(&p->callback);

	mm_spin_destroy(&p->locker);
	mm_spin_destroy(&p->logger_mutex);

	pthread_mutex_destroy(&p->signal_mutex);  
	pthread_cond_destroy(&p->signal_cond);
	//
	p->rolling_index = 100;
	p->current_index = 0;
	p->file_size = 300 * 1024 * 1024;
	//
	p->logger_level = MM_LOG_VERBOSE;
	p->check_file_size_time = 5000;
	//
	p->is_rolling = 1;
	p->is_console = 0;
	p->is_immediately = 0;
	p->state = ts_closed;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_logger_manager_lock(struct mm_logger_manager* p)
{
	mm_spin_lock(&p->locker);
}
MM_EXPORT_DLL void mm_logger_manager_unlock(struct mm_logger_manager* p)
{
	mm_spin_unlock(&p->locker);
}
//////////////////////////////////////////////////////////////////////////
// assign callback.
MM_EXPORT_DLL void mm_logger_manager_assign_callback(struct mm_logger_manager* p, struct mm_logger_manager_callback* callback)
{
	assert(NULL != callback && "you can not assign null callback.");
	p->callback = *callback;
}
// assign logger instance.default is mm_logger_instance.
MM_EXPORT_DLL void mm_logger_manager_assign_logger(struct mm_logger_manager* p, struct mm_logger* logger)
{
	assert(NULL != logger && "logger is a null.");
	if (NULL != p->logger_impl)
	{
		mm_logger_assign_callback(p->logger_impl,&mm_logger_callback_printf,p);
	}
	p->logger_impl = logger;
	if (NULL != p->logger_impl)
	{
		mm_logger_assign_callback(p->logger_impl,&__static_logger_manager_logger_callback,p);
	}
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_logger_manager_open(struct mm_logger_manager* p)
{
	p->current_index = 0;
	mm_logger_manager_generate_current_file_prefix(p);
	mm_logger_manager_generate_current_file_name(p);
	p->stream = fopen(p->current_file_name.s,"wb");
}
MM_EXPORT_DLL void mm_logger_manager_close(struct mm_logger_manager* p)
{
	if (NULL != p->stream)
	{
		fclose(p->stream);
		p->stream = NULL;
	}
}
MM_EXPORT_DLL void mm_logger_manager_flush(struct mm_logger_manager* p)
{
	if (NULL != p->stream)
	{
		fflush(p->stream);
	}
}
MM_EXPORT_DLL void mm_logger_manager_start(struct mm_logger_manager* p)
{
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	pthread_create(&p->file_size_thread, NULL, &__static_logger_manager_thread, p);
}
MM_EXPORT_DLL void mm_logger_manager_interrupt(struct mm_logger_manager* p)
{
	p->state = ts_closed;
	pthread_mutex_lock(&p->signal_mutex);  
	pthread_cond_signal(&p->signal_cond);  
	pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mm_logger_manager_shutdown(struct mm_logger_manager* p)
{
	p->state = ts_finish;
	pthread_mutex_lock(&p->signal_mutex);  
	pthread_cond_signal(&p->signal_cond);  
	pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mm_logger_manager_join(struct mm_logger_manager* p)
{
	pthread_join(p->file_size_thread, NULL);
}
MM_EXPORT_DLL void mm_logger_manager_check_file_size(struct mm_logger_manager* p)
{
	if (NULL != p->stream)
	{
		if ( (size_t)ftell(p->stream) >= p->file_size)
		{
			p->current_index ++;
			p->current_index = p->current_index > p->rolling_index ? 0 : p->current_index;
			//
			fflush(p->stream);
			fclose(p->stream);
			p->stream = NULL;

			mm_logger_manager_generate_current_file_name(p);
			p->stream = fopen(p->current_file_name.s,"wb");
		}
	}
}
MM_EXPORT_DLL void mm_logger_manager_generate_current_file_name(struct mm_logger_manager* p)
{
	char file_name[MM_MAX_LOGGER_FILE_NAME_LENGTH] = {0};
	char fmt[64] = {0};
	unsigned int w = 0;
	size_t n = p->rolling_index;
	while(n!=0){ n = n / 10; w++; }
	mm_sprintf(fmt, "%%s_%%0%dd.log",w);
	// log/mm_20160217_113233_105_000.log
	mm_snprintf(file_name, MM_MAX_LOGGER_FILE_NAME_LENGTH - 1, fmt,p->current_file_prefix.s,p->current_index);
	mm_string_assigns(&p->current_file_name,file_name);
}
MM_EXPORT_DLL void mm_logger_manager_generate_current_file_prefix(struct mm_logger_manager* p)
{
	char file_name[MM_MAX_LOGGER_FILE_NAME_LENGTH] = {0};
	// log/mm_20160217_113233_105
	// [19920126_091314_520]
	char time_path[64];
	mm_tm_t tm;
	struct timeval tv;
	struct mm_time_info tp;
	mm_gettimeofday(&tv,NULL);
	tp.sec  = tv.tv_sec;
	tp.msec = tv.tv_usec / 1000;
	mm_time_tm(&tp,&tm);
	mm_sprintf(time_path, "%4d%02d%02d_%02d%02d%02d_%03d",
		tm.mm_tm_year, tm.mm_tm_mon ,
		tm.mm_tm_mday, tm.mm_tm_hour,
		tm.mm_tm_min , tm.mm_tm_sec ,tp.msec);
	mm_snprintf(file_name, MM_MAX_LOGGER_FILE_NAME_LENGTH - 1, "%s/%s_%s",p->logger_path.s,p->file_name.s,time_path);
	mm_string_assigns(&p->current_file_prefix,file_name);
}
MM_EXPORT_DLL void mm_logger_manager_logger_section(struct mm_logger* p, const char* section, mm_uint32_t lvl, const char* message)
{
	struct timeval tv;
	struct mm_time_info tp;
	char time_stamp_string[sizeof(log_time_string_template)];
	struct mm_logger_manager* lm = (struct mm_logger_manager*)(p->obj);
	if ( NULL == lm || lvl > lm->logger_level )
	{
		// logger filter.
		return;
	}
	mm_spin_lock(&lm->logger_mutex);
	//
	mm_gettimeofday(&tv,NULL);
	tp.sec  = tv.tv_sec;
	tp.msec = tv.tv_usec / 1000;
	mm_time_string(&tp, time_stamp_string);
	// logger to file.
	if (lm->stream)
	{
		char log_head_suffix[8];
		// [1992/01/26 09:13:14-520 8 V ]
		const struct mm_level_mark* mark = mm_logger_level_mark((mm_logger_level_t)lvl);
		// [1992/01/26 09:13:14-520]
		fputs(time_stamp_string,lm->stream);
		// [ 8 V ]
		mm_sprintf(log_head_suffix," %d %s ",lvl,mark->m.s);
		fputs(log_head_suffix,lm->stream);
		fputs(section,lm->stream);
		fputc(' ',lm->stream);
		fputs(message,lm->stream);
		fputc('\n',lm->stream);
	}
	if ( 1 == lm->is_console )
	{
		// logger to console.
		(*(lm->callback.console_printf))(section,time_stamp_string,lvl,message);
	}
	if ( 1 == lm->is_immediately )
	{
		mm_logger_manager_flush(lm);
	}
	mm_spin_unlock(&lm->logger_mutex);
}
static void __static_logger_manager_logger_callback(struct mm_logger* p, mm_uint32_t lvl, const char* message)
{
	struct mm_logger_manager* lm = (struct mm_logger_manager*)(p->obj);
	mm_logger_manager_logger_section(p,lm->logger_section.s,lvl,message);
}
static void* __static_logger_manager_thread(void* arg)
{
	struct timeval  ntime;
	struct timespec otime;
	mm_msec_t _nearby_time = 0;
	//
	struct mm_logger_manager* p = (struct mm_logger_manager*)(arg);
	//
	while( ts_motion == p->state )
	{
		mm_spin_lock(&p->logger_mutex);
		mm_logger_manager_check_file_size(p);
		mm_logger_manager_flush(p);
		mm_spin_unlock(&p->logger_mutex);
		//
		_nearby_time = p->check_file_size_time;
		// timewait nearby.
		mm_timedwait_nearby(&p->signal_cond, &p->signal_mutex, &ntime, &otime, _nearby_time);
	}
	return NULL;
}