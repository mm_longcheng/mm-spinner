#include "core/mm_time_cache.h"
#include "core/mm_alloc.h"
#include "core/mm_math.h"
#include "core/mm_logger.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_time_info_init(struct mm_time_info* p)
{
	p->sec = 0;
	p->msec = 0;
	p->gmtoff = 0;
}
MM_EXPORT_DLL void mm_time_info_destroy(struct mm_time_info* p)
{
	p->sec = 0;
	p->msec = 0;
	p->gmtoff = 0;
}
//////////////////////////////////////////////////////////////////////////
static void* __static_time_cache_thread(void* arg);

static struct mm_time_cache g_time_cache;
MM_EXPORT_DLL struct mm_time_cache* mm_time_cache_instance()
{
	return &g_time_cache;
}
MM_EXPORT_DLL void mm_time_cache_init(struct mm_time_cache* p)
{
	mm_memset(p->cached_time,0,sizeof(struct mm_time_info) * MM_TIME_SLOTS);
	mm_memset(p->cached_string_log_time,0,sizeof(mm_char_t) * MM_TIME_SLOTS * log_time_string_length);
	mm_time_info_init(&p->start);
	mm_spin_init(&p->locker,NULL);
	p->msec_current = 0;
	p->msec_sleep = MM_TIME_UPDATE;
	p->slot = 0;
	p->state = ts_closed;
	p->cache = &p->cached_time[0];
	p->string_log_time = &(p->cached_string_log_time[p->slot][0]);
	//
	mm_time_cache_update(p);
	//
	p->start = *p->cache;
}
MM_EXPORT_DLL void mm_time_cache_destroy(struct mm_time_cache* p)
{
	mm_memset(p->cached_time,0,sizeof(struct mm_time_info) * MM_TIME_SLOTS);
	mm_memset(p->cached_string_log_time,0,sizeof(mm_char_t) * MM_TIME_SLOTS * log_time_string_length);
	mm_time_info_destroy(&p->start);
	mm_spin_destroy(&p->locker);
	p->msec_current = 0;
	p->msec_sleep = MM_TIME_UPDATE;
	p->slot = 0;
	p->state = ts_closed;
	p->cache = &p->cached_time[0];
	p->string_log_time = &(p->cached_string_log_time[p->slot][0]);
}
//
MM_EXPORT_DLL void mm_time_cache_update(struct mm_time_cache* p)
{
	char* p0 = NULL;
	time_t sec = 0;
	mm_uint_t msec = 0;
	struct mm_time_info* tp = NULL;
	struct timeval tv;

	mm_spin_lock(&p->locker);

	mm_gettimeofday(&tv,NULL);

	sec = tv.tv_sec;
	msec = tv.tv_usec / 1000;

	p->msec_current = (mm_uint64_t) sec * 1000 + msec;

	p->slot++;
	p->slot = MM_TIME_SLOTS == p->slot ? 0 : p->slot;

	tp = &p->cached_time[p->slot];

	tp->sec = sec;
	tp->msec = msec;

	p0 = &(p->cached_string_log_time[p->slot][0]);

	mm_time_string(tp, p0);

	mm_memory_barrier();

	p->string_log_time = p0;
	p->cache = tp;

	mm_spin_unlock(&p->locker);
}
//
MM_EXPORT_DLL void mm_time_cache_start(struct mm_time_cache* p)
{
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	pthread_create(&p->time_thread, NULL, &__static_time_cache_thread, p);
}
MM_EXPORT_DLL void mm_time_cache_interrupt(struct mm_time_cache* p)
{
	p->state = ts_closed;
}
MM_EXPORT_DLL void mm_time_cache_shutdown(struct mm_time_cache* p)
{
	p->state = ts_finish;
}
MM_EXPORT_DLL void mm_time_cache_join(struct mm_time_cache* p)
{
	pthread_join(p->time_thread, NULL);
}
static void* __static_time_cache_thread(void* arg)
{
	struct mm_time_cache* p = (struct mm_time_cache*)arg;
	while( ts_motion == p->state )
	{
		mm_time_cache_update(p);
		mm_msleep(p->msec_sleep);
	}
	return NULL;
}
MM_EXPORT_DLL void mm_gmtime(time_t t, mm_tm_t *tp)
{
    mm_int_t   yday;
    mm_uint_t  n, sec, min, hour, mday, mon, year, wday, days, leap;

    /* the calculation is valid for positive time_t only */

    n = (mm_uint_t) t;

    days = n / 86400;

    /* January 1, 1970 was Thursday */

    wday = (4 + days) % 7;

    n %= 86400;
    hour = n / 3600;
    n %= 3600;
    min = n / 60;
    sec = n % 60;

    /*
     * the algorithm based on Gauss' formula,
     * see src/http/mm_http_parse_time.c
     */

    /* days since March 1, 1 BC */
    days = days - (31 + 28) + 719527;

    /*
     * The "days" should be adjusted to 1 only, however, some March 1st's go
     * to previous year, so we adjust them to 2.  This causes also shift of the
     * last February days to next year, but we catch the case when "yday"
     * becomes negative.
     */

    year = (days + 2) * 400 / (365 * 400 + 100 - 4 + 1);

    yday = days - (365 * year + year / 4 - year / 100 + year / 400);

    if (yday < 0) 
	{
        leap = (year % 4 == 0) && (year % 100 || (year % 400 == 0));
        yday = 365 + leap + yday;
        year--;
    }

    /*
     * The empirical formula that maps "yday" to month.
     * There are at least 10 variants, some of them are:
     *     mon = (yday + 31) * 15 / 459
     *     mon = (yday + 31) * 17 / 520
     *     mon = (yday + 31) * 20 / 612
     */

    mon = (yday + 31) * 10 / 306;

    /* the Gauss' formula that evaluates days before the month */

    mday = yday - (367 * mon / 12 - 30) + 1;

    if (yday >= 306) 
	{

        year++;
        mon -= 10;

        /*
         * there is no "yday" in Win32 SYSTEMTIME
         *
         * yday -= 306;
         */

    } 
	else 
	{

        mon += 2;

        /*
         * there is no "yday" in Win32 SYSTEMTIME
         *
         * yday += 31 + 28 + leap;
         */
    }

    tp->mm_tm_sec  = (mm_tm_sec_t ) sec;
    tp->mm_tm_min  = (mm_tm_min_t ) min;
    tp->mm_tm_hour = (mm_tm_hour_t) hour;
    tp->mm_tm_mday = (mm_tm_mday_t) mday;
    tp->mm_tm_mon  = (mm_tm_mon_t ) mon;
    tp->mm_tm_year = (mm_tm_year_t) year;
    tp->mm_tm_wday = (mm_tm_wday_t) wday;
}
MM_EXPORT_DLL void mm_time_tm(struct mm_time_info* ti, mm_tm_t *tp)
{
#if (MM_HAVE_GETTIMEZONE)

	ti->gmtoff = mm_gettimezone();
	mm_gmtime(ti->sec + ti->gmtoff * 60, tp);

#elif (MM_HAVE_GMTOFF)

	mm_localtime_r(&ti->sec, tp);
	ti->gmtoff = (mm_int_t) (tp->mm_tm_gmtoff / 60);

#else

	mm_localtime(&ti->sec, tp);
	ti->gmtoff = mm_timezone(tp->mm_tm_isdst);

#endif
}
MM_EXPORT_DLL void mm_time_tm_const(const struct mm_time_info* ti, mm_tm_t *tp)
{
#if (MM_HAVE_GETTIMEZONE)

	// ti->gmtoff = mm_gettimezone();
	mm_gmtime(ti->sec + ti->gmtoff * 60, tp);

#elif (MM_HAVE_GMTOFF)

	mm_localtime_r(&ti->sec, tp);
	// ti->gmtoff = (mm_int_t) (tp->mm_tm_gmtoff / 60);

#else

	mm_localtime(&ti->sec, tp);
	// ti->gmtoff = mm_timezone(tp->mm_tm_isdst);

#endif
}
MM_EXPORT_DLL void mm_time_string(struct mm_time_info* tp, char ts[sizeof(log_time_string_template)])
{
	mm_tm_t tm;
	mm_time_tm(tp,&tm);
	mm_sprintf(ts, "%4d/%02d/%02d %02d:%02d:%02d-%03d",
		tm.mm_tm_year, tm.mm_tm_mon ,
		tm.mm_tm_mday, tm.mm_tm_hour,
		tm.mm_tm_min , tm.mm_tm_sec ,tp->msec);
}
MM_EXPORT_DLL void mm_time_string_const(const struct mm_time_info* tp, char ts[sizeof(log_time_string_template)])
{
	mm_tm_t tm;
	mm_time_tm_const(tp,&tm);
	mm_sprintf(ts, "%4d/%02d/%02d %02d:%02d:%02d-%03d",
		tm.mm_tm_year, tm.mm_tm_mon ,
		tm.mm_tm_mday, tm.mm_tm_hour,
		tm.mm_tm_min , tm.mm_tm_sec ,tp->msec);
}
MM_EXPORT_DLL time_t mm_get_duration_from_dawn_by_offset(time_t _timecode, time_t _offset )
{
   time_t _fx  = _timecode + MM_SECONDS_HOUR * 8;
   time_t _dt  = _fx - _offset;
   time_t _day  = _dt / MM_SECONDS_DAY;
   time_t _cur  = _day * MM_SECONDS_DAY;
   time_t _rt = _dt - _cur;
   return _rt;
}
MM_EXPORT_DLL time_t mm_get_duration_from_week(time_t _timecode )
{
	struct tm _tw;
	time_t _wd;
	_tw = *mm_localtime(&_timecode);

	_tw.tm_hour = 0;
	_tw.tm_min = 0;
	_tw.tm_sec = 0;
	_tw.tm_mday = _tw.tm_mday - _tw.tm_wday;

	_wd = mktime(&_tw);
	return _timecode - _wd;
}
MM_EXPORT_DLL int mm_get_day_flip(time_t _timecode ,time_t _offset )
{
   time_t _fx = _timecode + MM_SECONDS_HOUR * 8;
   time_t _dt = _fx - _offset;// second
   time_t _day = _dt / MM_SECONDS_DAY;
   return (int)_day;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL int mm_timedwait_nearby(pthread_cond_t* signal_cond, pthread_mutex_t* signal_mutex, struct timeval* ntime, struct timespec* otime, mm_msec_t _nearby_time)
{
	int rt = 0;
	// some os pthread timedwait impl precision is low.
	// MM_MSEC_PER_SEC check can avoid some precision problem
	// but will extend the application shutdown time.
	if ( 0 == _nearby_time )
	{
		// next loop immediately.
		rt = 0;
	}
	else if( MM_MSEC_PER_SEC < _nearby_time )
	{
		// timedwait a while.
		int _a = _nearby_time / 1000;
		int _b = _nearby_time % 1000;
		mm_gettimeofday(ntime, NULL);
		otime->tv_sec  = (ntime->tv_sec  + _a        );
		otime->tv_nsec = (ntime->tv_usec + _b * 1000 ) * 1000;
		otime->tv_sec += otime->tv_nsec / MM_NSEC_PER_SEC;
		otime->tv_nsec = otime->tv_nsec % MM_NSEC_PER_SEC;
		//
		pthread_mutex_lock(signal_mutex);
		rt = pthread_cond_timedwait(signal_cond,signal_mutex,otime);
		pthread_mutex_unlock(signal_mutex);
	}
	else
	{
		// msleep a while.
		mm_msleep(_nearby_time);
		rt = 0;
	}
	return rt;
}
