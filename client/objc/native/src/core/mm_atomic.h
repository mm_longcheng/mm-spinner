#ifndef __mm_atomic_h__
#define __mm_atomic_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

#if (MM_HAVE_LIBATOMIC)

#define AO_REQUIRE_CAS
#include <atomic_ops.h>

#define MM_HAVE_ATOMIC_OPS  1

typedef long                        mm_atomic_int_t;
typedef AO_t                        mm_atomic_uint_t;
typedef volatile mm_atomic_uint_t  mm_atomic_t;

#if (MM_PTR_SIZE == 8)
#define MM_ATOMIC_T_LEN            (sizeof("-9223372036854775808") - 1)
#else
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)
#endif

#define mm_atomic_cmp_set(lock, old, new)                                    \
    AO_compare_and_swap(lock, old, new)
#define mm_atomic_fetch_add(value, add)                                      \
    AO_fetch_and_add(value, add)
#define mm_memory_barrier()        AO_nop()
#define mm_cpu_pause()


#elif (MM_DARWIN_ATOMIC)

#if defined(__MAC_OS_X_VERSION_MIN_REQUIRED) && __MAC_OS_X_VERSION_MIN_REQUIRED < 101200
/*
 * use Darwin 8 atomic(3) and barrier(3) operations
 * optimized at run-time for UP and SMP
 */

#include <libkern/OSAtomic.h>

/* "bool" conflicts with perl's CORE/handy.h */
#if 0
#undef bool
#endif


#define MM_HAVE_ATOMIC_OPS  1

#if (MM_PTR_SIZE == 8)

typedef mm_sint64_t                     mm_atomic_int_t;
typedef mm_uint64_t                    mm_atomic_uint_t;
#define MM_ATOMIC_T_LEN            (sizeof("-9223372036854775808") - 1)

#define mm_atomic_cmp_set(lock, old, new)                                    \
    OSAtomicCompareAndSwap64Barrier(old, new, (mm_sint64_t *) lock)

#define mm_atomic_fetch_add(value, add)                                      \
    (OSAtomicAdd64(add, (mm_sint64_t *) value) - add)

#else

typedef mm_sint32_t                     mm_atomic_int_t;
typedef mm_uint32_t                    mm_atomic_uint_t;
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)

#define mm_atomic_cmp_set(lock, old, new)                                    \
    OSAtomicCompareAndSwap32Barrier(old, new, (mm_sint32_t *) lock)

#define mm_atomic_fetch_add(value, add)                                      \
    (OSAtomicAdd32(add, (mm_sint32_t *) value) - add)

#endif

#define mm_memory_barrier()        OSMemoryBarrier()

#define mm_cpu_pause()

typedef volatile mm_atomic_uint_t  mm_atomic_t;

#else
/* GCC 4.1 builtin atomic operations */

#define MM_HAVE_ATOMIC_OPS  1

typedef long                        mm_atomic_int_t;
typedef unsigned long               mm_atomic_uint_t;

#if (MM_PTR_SIZE == 8)
#define MM_ATOMIC_T_LEN            (sizeof("-9223372036854775808") - 1)
#else
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)
#endif

typedef volatile mm_atomic_uint_t  mm_atomic_t;


#define mm_atomic_cmp_set(lock, old, set)                                    \
    __sync_bool_compare_and_swap(lock, old, set)

#define mm_atomic_fetch_add(value, add)                                      \
    __sync_fetch_and_add(value, add)

#define mm_memory_barrier()        __sync_synchronize()

#if ( __i386__ || __i386 || __amd64__ || __amd64 )
#define mm_cpu_pause()             __asm__ ("pause")
#else
#define mm_cpu_pause()
#endif

#endif

#elif (MM_HAVE_GCC_ATOMIC)

/* GCC 4.1 builtin atomic operations */

#define MM_HAVE_ATOMIC_OPS  1

typedef long                        mm_atomic_int_t;
typedef unsigned long               mm_atomic_uint_t;

#if (MM_PTR_SIZE == 8)
#define MM_ATOMIC_T_LEN            (sizeof("-9223372036854775808") - 1)
#else
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)
#endif

typedef volatile mm_atomic_uint_t  mm_atomic_t;


#define mm_atomic_cmp_set(lock, old, set)                                    \
    __sync_bool_compare_and_swap(lock, old, set)

#define mm_atomic_fetch_add(value, add)                                      \
    __sync_fetch_and_add(value, add)

#define mm_memory_barrier()        __sync_synchronize()

#if ( __i386__ || __i386 || __amd64__ || __amd64 )
#define mm_cpu_pause()             __asm__ ("pause")
#else
#define mm_cpu_pause()
#endif


#elif ( __i386__ || __i386 )

typedef mm_sint32_t                     mm_atomic_int_t;
typedef mm_uint32_t                    mm_atomic_uint_t;
typedef volatile mm_atomic_uint_t  mm_atomic_t;
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)


#if ( __SUNPRO_C )

#define MM_HAVE_ATOMIC_OPS  1

mm_atomic_uint_t
mm_atomic_cmp_set(mm_atomic_t *lock, mm_atomic_uint_t old,
    mm_atomic_uint_t set);

mm_atomic_int_t
mm_atomic_fetch_add(mm_atomic_t *value, mm_atomic_int_t add);

/*
 * Sun Studio 12 exits with segmentation fault on '__asm ("pause")',
 * so mm_cpu_pause is declared in src/os/unix/mm_sunpro_x86.il
 */

void
mm_cpu_pause(void);

/* the code in src/os/unix/mm_sunpro_x86.il */

#define mm_memory_barrier()        __asm (".volatile"); __asm (".nonvolatile")


#else /* ( __GNUC__ || __INTEL_COMPILER ) */

#define MM_HAVE_ATOMIC_OPS  1

#include "atomic/mm_gcc_atomic_x86.h"

#endif


#elif ( __amd64__ || __amd64 )

typedef mm_sint64_t                     mm_atomic_int_t;
typedef mm_uint64_t                    mm_atomic_uint_t;
typedef volatile mm_atomic_uint_t  mm_atomic_t;
#define MM_ATOMIC_T_LEN            (sizeof("-9223372036854775808") - 1)


#if ( __SUNPRO_C )

#define MM_HAVE_ATOMIC_OPS  1

mm_atomic_uint_t
mm_atomic_cmp_set(mm_atomic_t *lock, mm_atomic_uint_t old,
    mm_atomic_uint_t set);

mm_atomic_int_t
mm_atomic_fetch_add(mm_atomic_t *value, mm_atomic_int_t add);

/*
 * Sun Studio 12 exits with segmentation fault on '__asm ("pause")',
 * so mm_cpu_pause is declared in src/os/unix/mm_sunpro_amd64.il
 */

void
mm_cpu_pause(void);

/* the code in src/os/unix/mm_sunpro_amd64.il */

#define mm_memory_barrier()        __asm (".volatile"); __asm (".nonvolatile")


#else /* ( __GNUC__ || __INTEL_COMPILER ) */

#define MM_HAVE_ATOMIC_OPS  1

#include "atomic/mm_gcc_atomic_amd64.h"

#endif


#elif ( __sparc__ || __sparc || __sparcv9 )

#if (MM_PTR_SIZE == 8)

typedef mm_sint64_t                     mm_atomic_int_t;
typedef mm_uint64_t                    mm_atomic_uint_t;
#define MM_ATOMIC_T_LEN            (sizeof("-9223372036854775808") - 1)

#else

typedef mm_sint32_t                     mm_atomic_int_t;
typedef mm_uint32_t                    mm_atomic_uint_t;
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)

#endif

typedef volatile mm_atomic_uint_t  mm_atomic_t;


#if ( __SUNPRO_C )

#define MM_HAVE_ATOMIC_OPS  1

#include "atomic/mm_sunpro_atomic_sparc64.h"


#else /* ( __GNUC__ || __INTEL_COMPILER ) */

#define MM_HAVE_ATOMIC_OPS  1

#include "atomic/mm_gcc_atomic_sparc64.h"

#endif


#elif ( __powerpc__ || __POWERPC__ )

#define MM_HAVE_ATOMIC_OPS  1

#if (MM_PTR_SIZE == 8)

typedef mm_sint64_t                     mm_atomic_int_t;
typedef mm_uint64_t                    mm_atomic_uint_t;
#define MM_ATOMIC_T_LEN            (sizeof("-9223372036854775808") - 1)

#else

typedef mm_sint32_t                     mm_atomic_int_t;
typedef mm_uint32_t                    mm_atomic_uint_t;
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)

#endif

typedef volatile mm_atomic_uint_t  mm_atomic_t;


#include "atomic/mm_gcc_atomic_ppc.h"

#elif _MSC_VER

#include "atomic/mm_msvs_atomic.h"

#endif


#if !(MM_HAVE_ATOMIC_OPS)

#define MM_HAVE_ATOMIC_OPS  0

typedef mm_sint32_t                     mm_atomic_int_t;
typedef mm_uint32_t                    mm_atomic_uint_t;
typedef volatile mm_atomic_uint_t  mm_atomic_t;
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)


static mm_inline mm_atomic_uint_t
mm_atomic_cmp_set(mm_atomic_t *lock, mm_atomic_uint_t old,
     mm_atomic_uint_t set)
{
     if (*lock == old) {
         *lock = set;
         return 1;
     }

     return 0;
}


static mm_inline mm_atomic_int_t
mm_atomic_fetch_add(mm_atomic_t *value, mm_atomic_int_t add)
{
     mm_atomic_int_t  old;

     old = *value;
     *value += add;

     return old;
}

#define mm_memory_barrier()
#define mm_cpu_pause()

#endif

#include "core/mm_suffix.h"

#endif//__mm_atomic_h__