#include "mm_signal_task.h"
#include "core/mm_time_cache.h"

static void* __static_signal_task_poll_wait_thread(void* _arg);
//////////////////////////////////////////////////////////////////////////
static int __static_signal_task_factor(void* obj)
{
	return -1;
}
static int __static_signal_task_handle(void* obj)
{
	return 0;
}
MM_EXPORT_DLL void mm_signal_task_callback_init(struct mm_signal_task_callback* p)
{
	p->factor = &__static_signal_task_factor;
	p->handle = &__static_signal_task_handle;
	p->obj = NULL;
}
MM_EXPORT_DLL void mm_signal_task_callback_destroy(struct mm_signal_task_callback* p)
{
	p->factor = &__static_signal_task_factor;
	p->handle = &__static_signal_task_handle;
	p->obj = NULL;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_signal_task_init(struct mm_signal_task* p)
{
	mm_signal_task_callback_init(&p->callback);
	pthread_mutex_init(&p->signal_mutex, NULL);
	pthread_cond_init(&p->signal_cond, NULL);
	p->msec_success_nearby = MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC;
	p->msec_failure_nearby = MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC;
	p->msec_delay = 0;
	p->state = ts_closed;
}
MM_EXPORT_DLL void mm_signal_task_destroy(struct mm_signal_task* p)
{
	mm_signal_task_callback_init(&p->callback);
	pthread_mutex_destroy(&p->signal_mutex);
	pthread_cond_destroy(&p->signal_cond);
	p->msec_success_nearby = MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC;
	p->msec_failure_nearby = MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC;
	p->msec_delay = 0;
	p->state = ts_closed;
}
// lock to make sure the knot is thread safe.
MM_EXPORT_DLL void mm_signal_task_lock(struct mm_signal_task* p)
{
	pthread_mutex_lock(&p->signal_mutex);
}
// unlock knot.
MM_EXPORT_DLL void mm_signal_task_unlock(struct mm_signal_task* p)
{
	pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mm_signal_task_assign_callback(struct mm_signal_task* p,struct mm_signal_task_callback* callback)
{
	assert(NULL != callback && "you can not assign null callback.");
	p->callback = (*callback);
}
MM_EXPORT_DLL void mm_signal_task_assign_success_nearby(struct mm_signal_task* p,mm_msec_t nearby)
{
	p->msec_success_nearby = nearby;
}
MM_EXPORT_DLL void mm_signal_task_assign_failure_nearby(struct mm_signal_task* p,mm_msec_t nearby)
{
	p->msec_failure_nearby = nearby;
}
//////////////////////////////////////////////////////////////////////////
// wait.
MM_EXPORT_DLL void mm_signal_task_signal_wait(struct mm_signal_task* p)
{
	int code = -1;
	struct timeval  ntime;
	struct timespec otime;
	mm_msec_t _nearby_time = 0;
	assert(NULL != p->callback.factor && "p->callback.factor is a null.");
	assert(NULL != p->callback.handle && "p->callback.handle is a null.");
	if ( 0 < p->msec_delay )
	{
		mm_msleep(p->msec_delay);
	}
	while( ts_motion == p->state )
	{
		// factor
		code = (*(p->callback.factor))(p);
		if (0 == code)
		{
			pthread_mutex_lock(&p->signal_mutex);
			pthread_cond_wait(&p->signal_cond,&p->signal_mutex);
			pthread_mutex_unlock(&p->signal_mutex);
			// the condition is timeout invalid.we enter a new process.
			// we must make sure the code == 0.
			continue;
		}
		// fire event.
		code = (*(p->callback.handle))(p);
		_nearby_time = 0 == code ? p->msec_success_nearby : p->msec_failure_nearby;
		// timewait nearby.
		mm_timedwait_nearby(&p->signal_cond, &p->signal_mutex, &ntime, &otime, _nearby_time);
	}
}
// signal.
MM_EXPORT_DLL void mm_signal_task_signal(struct mm_signal_task* p)
{
	pthread_mutex_lock(&p->signal_mutex);
	pthread_cond_signal(&p->signal_cond);
	pthread_mutex_unlock(&p->signal_mutex);
}
//////////////////////////////////////////////////////////////////////////
// start wait thread.
MM_EXPORT_DLL void mm_signal_task_start(struct mm_signal_task* p)
{
	p->state = ts_finish == p->state ? ts_closed : ts_motion;
	pthread_create(&p->signal_thread, NULL, &__static_signal_task_poll_wait_thread, p);
}
// interrupt wait thread.
MM_EXPORT_DLL void mm_signal_task_interrupt(struct mm_signal_task* p)
{
	p->state = ts_closed;
	mm_signal_task_signal(p);
}
// shutdown wait thread.
MM_EXPORT_DLL void mm_signal_task_shutdown(struct mm_signal_task* p)
{
	p->state = ts_finish;
	mm_signal_task_signal(p);
}
// join wait thread.
MM_EXPORT_DLL void mm_signal_task_join(struct mm_signal_task* p)
{
	pthread_join(p->signal_thread, NULL);
}
//////////////////////////////////////////////////////////////////////////
static void* __static_signal_task_poll_wait_thread(void* _arg)
{
	struct mm_signal_task* signal_task = (struct mm_signal_task*)(_arg);
	mm_signal_task_signal_wait(signal_task);
	return NULL;
}
