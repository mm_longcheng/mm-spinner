#include "core/mm_timeval.h"
#include "core/mm_time.h"
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_timeval_init(struct timeval* p)
{
	p->tv_sec  = 0;
	p->tv_usec = 0;
}
MM_EXPORT_DLL void mm_timeval_destroy(struct timeval* p)
{
	p->tv_sec  = 0;
	p->tv_usec = 0;
}
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_timeval_normalize(struct timeval* p)
{
	p->tv_sec += (p->tv_usec / MM_USEC_PER_SEC);
	p->tv_usec = (p->tv_usec % MM_USEC_PER_SEC);
	// make sure the 0 < tv_usec,if p->tv_sec * MM_USEC_PER_SEC + p->tv_usec > 0;
	if ( 0 < p->tv_sec && 0 > p->tv_usec )
	{
		p->tv_sec --;
		p->tv_usec += MM_USEC_PER_SEC;
	}
}
