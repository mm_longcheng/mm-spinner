#ifndef __mm_atoi_h__
#define __mm_atoi_h__
#include "core/mm_prefix.h"

#include "core/mm_platform.h"
#include "core/mm_types.h"

MM_EXPORT_DLL mm_sint32_t mm_atoi32(const char* n);
MM_EXPORT_DLL mm_sint64_t mm_atoi64(const char* n);

#include "core/mm_suffix.h"

#endif//__mm_atoi_h__