#ifndef __mm_cpuinfo_h__
#define __mm_cpuinfo_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_string.h"

/* Initial CPU stuff to set.
*/
#define MM_CPU_UNKNOWN    0
#define MM_CPU_X86        1
#define MM_CPU_PPC        2
#define MM_CPU_ARM        3
#define MM_CPU_MIPS       4

/* Find CPU type
*/
#if (defined(_MSC_VER) && (defined(_M_IX86) || defined(_M_X64))) || \
    (defined(__GNUC__) && (defined(__i386__) || defined(__x86_64__)))
#   define MM_CPU MM_CPU_X86

#elif MM_PLATFORM == MM_PLATFORM_APPLE && defined(__BIG_ENDIAN__)
#   define MM_CPU MM_CPU_PPC
#elif MM_PLATFORM == MM_PLATFORM_APPLE
#   define MM_CPU MM_CPU_X86
#elif MM_PLATFORM == MM_PLATFORM_APPLE_IOS && (defined(__i386__) || defined(__x86_64__))
#   define MM_CPU MM_CPU_X86
#elif defined(__arm__) || defined(_M_ARM) || defined(__arm64__) || defined(__aarch64__)
#   define MM_CPU MM_CPU_ARM
#elif defined(__mips64) || defined(__mips64_)
#   define MM_CPU MM_CPU_MIPS
#else
#   define MM_CPU MM_CPU_UNKNOWN
#endif

/* Find how to declare aligned variable.
*/
#if MM_COMPILER == MM_COMPILER_MSVC
#   define MM_ALIGNED_DECL(type, var, alignment)  __declspec(align(alignment)) type var

#elif (MM_COMPILER == MM_COMPILER_GNUC) || (MM_COMPILER == MM_COMPILER_CLANG)
#   define MM_ALIGNED_DECL(type, var, alignment)  type var __attribute__((__aligned__(alignment)))

#else
#   define MM_ALIGNED_DECL(type, var, alignment)  type var
#endif

/** Find perfect alignment (should supports SIMD alignment if SIMD available)
*/
#if MM_CPU == MM_CPU_X86
#   define MM_SIMD_ALIGNMENT  16

#else
#   define MM_SIMD_ALIGNMENT  16
#endif

/* Declare variable aligned to SIMD alignment.
*/
#define MM_SIMD_ALIGNED_DECL(type, var)   MM_ALIGNED_DECL(type, var, MM_SIMD_ALIGNMENT)

/* Define whether or not MM compiled with SSE supports.
*/
#if   MM_DOUBLE_PRECISION == 0 && MM_CPU == MM_CPU_X86 && MM_COMPILER == MM_COMPILER_MSVC && \
    MM_PLATFORM != MM_PLATFORM_NACL
#   define __MM_HAVE_SSE  1
#elif MM_DOUBLE_PRECISION == 0 && MM_CPU == MM_CPU_X86 && (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG) && \
      MM_PLATFORM != MM_PLATFORM_APPLE_IOS && MM_PLATFORM != MM_PLATFORM_NACL
#   define __MM_HAVE_SSE  1
#endif

/* Define whether or not MM compiled with VFP support.
 */
#if MM_DOUBLE_PRECISION == 0 && MM_CPU == MM_CPU_ARM && (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG) && defined(__ARM_ARCH_6K__) && defined(__VFP_FP__)
#   define __MM_HAVE_VFP  1
#endif

/* Define whether or not MM compiled with NEON support.
 */
#if MM_DOUBLE_PRECISION == 0 && MM_CPU == MM_CPU_ARM && (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG) && defined(__ARM_ARCH_7A__) && defined(__ARM_NEON__)
#   define __MM_HAVE_NEON  1
#endif

/* Define whether or not MM compiled with MSA support.
 */
#if MM_DOUBLE_PRECISION == 0 && MM_CPU == MM_CPU_MIPS && (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG) && defined(__mips_msa)
#   define __MM_HAVE_MSA  1
#endif

#ifndef __MM_HAVE_SSE
#   define __MM_HAVE_SSE  0
#endif

#ifndef __MM_HAVE_VFP
#   define __MM_HAVE_VFP  0
#endif

#ifndef __MM_HAVE_NEON
#   define __MM_HAVE_NEON  0
#endif

#ifndef __MM_HAVE_MSA
#   define __MM_HAVE_MSA  0
#endif

struct mm_cpuinfo
{
	struct mm_string vendor;
	struct mm_string brand;
	mm_uint64_t level;
	mm_uint64_t flags;
};
MM_EXPORT_DLL void mm_cpuinfo_init(struct mm_cpuinfo* p);
MM_EXPORT_DLL void mm_cpuinfo_destroy(struct mm_cpuinfo* p);
//
MM_EXPORT_DLL void mm_cpuinfo_perform(struct mm_cpuinfo* p);
//
#include "core/mm_suffix.h"

#endif//__mm_cpuinfo_h__
