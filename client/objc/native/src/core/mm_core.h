#ifndef __mm_core_h__
#define __mm_core_h__

#include "core/mm_prefix.h"

#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdarg.h>
#include <stdlib.h>
#include <ctype.h>
#include <limits.h>
#include <setjmp.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <locale.h>
#include <signal.h>
#include <stdio.h>
#include <time.h>
//////////////////////////////////////////////////////////////////////////
#include "core/mm_config.h"
//////////////////////////////////////////////////////////////////////////
#define mm_container_of(ptr, type, member) (type *)((char*)(ptr) - offsetof(type, member))
//////////////////////////////////////////////////////////////////////////
#define MM_STACK_MAGIC	0xdeadbeef

#define MM_REPEAT_BYTE(x)	((~0ul / 0xff) * (x))

#define MM_ARRAY_SIZE( a )  ( sizeof( (a) ) / sizeof( (a[0]) ) )
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"
//
#endif//__mm_core_h__