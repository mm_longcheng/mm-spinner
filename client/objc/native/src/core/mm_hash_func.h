#ifndef __mm_hash_func_h__
#define __mm_hash_func_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

MM_EXPORT_DLL mm_uint32_t mm_murmur_hash2(mm_uchar_t *data, size_t len);
//////////////////////////////////////////////////////////////////////////
// come from libfastcommon
#define MM_CRC32_XINIT 0xFFFFFFFF		/* initial value */
#define MM_CRC32_XOROT 0xFFFFFFFF		/* final xor value */

MM_EXPORT_DLL int mm_rs_hash(const void *key, const int key_len);

MM_EXPORT_DLL int mm_js_hash(const void *key, const int key_len);
MM_EXPORT_DLL int mm_js_hash_ex(const void *key, const int key_len, const int init_value);

MM_EXPORT_DLL int mm_pjw_hash(const void *key, const int key_len);
MM_EXPORT_DLL int mm_pjw_hash_ex(const void *key, const int key_len, const int init_value);

MM_EXPORT_DLL int mm_elf_hash(const void *key, const int key_len);
MM_EXPORT_DLL int mm_elf_hash_ex(const void *key, const int key_len, const int init_value);

MM_EXPORT_DLL int mm_bkdr_hash(const void *key, const int key_len);
MM_EXPORT_DLL int mm_bkdr_hash_ex(const void *key, const int key_len, const int init_value);

MM_EXPORT_DLL int mm_sdbm_hash(const void *key, const int key_len);
MM_EXPORT_DLL int mm_sdbm_hash_ex(const void *key, const int key_len, const int init_value);

MM_EXPORT_DLL int mm_time33_hash(const void *key, const int key_len);
MM_EXPORT_DLL int mm_time33_hash_ex(const void *key, const int key_len, const int init_value);

MM_EXPORT_DLL int mm_djb_hash(const void *key, const int key_len);
MM_EXPORT_DLL int mm_djb_hash_ex(const void *key, const int key_len, const int init_value);

MM_EXPORT_DLL int mm_ap_hash(const void *key, const int key_len);
MM_EXPORT_DLL int mm_ap_hash_ex(const void *key, const int key_len, const int init_value);

MM_EXPORT_DLL int mm_calc_hashnr (const void* key, const int key_len);

MM_EXPORT_DLL int mm_calc_hashnr1(const void* key, const int key_len);
MM_EXPORT_DLL int mm_calc_hashnr1_ex(const void* key, const int key_len, const int init_value);

MM_EXPORT_DLL int mm_simple_hash(const void* key, const int key_len);
MM_EXPORT_DLL int mm_simple_hash_ex(const void* key, const int key_len, const int init_value);

MM_EXPORT_DLL int mm_crc32(void *key, const int key_len);
MM_EXPORT_DLL int mm_crc32_ex(void *key, const int key_len, const int init_value);

#define CRC32_FINAL(crc)  (crc ^ MM_CRC32_XOROT)
//////////////////////////////////////////////////////////////////////////
#define mm_bit32_hash(key) ((key) >> 1 | (key) << 31)
#define mm_bit64_hash(key) ((key) >> 1 | (key) << 63)
MM_EXPORT_DLL int mm_wang_hash(int key);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_hash_func_h__