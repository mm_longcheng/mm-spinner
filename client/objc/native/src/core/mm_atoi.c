#include "core/mm_atoi.h"
#include <stdlib.h>

#if (MM_PLATFORM_WIN32 != MM_PLATFORM)
#	define _atoi64(val)     strtoll(val, NULL, 10)
#endif

MM_EXPORT_DLL mm_sint32_t mm_atoi32(const char* n)
{
	if (n)
		return atoi(n);
	else
		return 0;
}
MM_EXPORT_DLL mm_sint64_t mm_atoi64(const char* n)
{
	if (n)
		return _atoi64(n);
	else
		return 0;
}