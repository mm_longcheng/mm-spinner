/*
  Red Black Trees
  (C) 1999  Andrea Arcangeli <andrea@suse.de>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  linux/include/linux/rbtree.h

  To use rbtrees you'll have to implement your own insert and search cores.
  This will avoid us to use callbacks and to drop drammatically performances.
  I know it's not the cleaner way,  but in C (not in C++) to get
  performances and genericity...

  See Documentation/rbtree.txt for documentation and samples.
*/

#ifndef	__mm_rbtree_h__
#define	__mm_rbtree_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"

#if MM_WIN32
#pragma pack(push)
#pragma pack(4)
struct mm_rb_node {
	unsigned long  __rb_parent_color;
	struct mm_rb_node *rb_right;
	struct mm_rb_node *rb_left;
}; // __attribute__((aligned(sizeof(long))));
#pragma pack(pop)
#else
struct mm_rb_node {
	unsigned long  __rb_parent_color;
	struct mm_rb_node *rb_right;
	struct mm_rb_node *rb_left;
} __attribute__((aligned(sizeof(long))));
/* The alignment might seem pointless, but allegedly CRIS needs it */
#endif

struct mm_rb_root {
	struct mm_rb_node *rb_node;
};


#define mm_rb_parent(r)   ((struct mm_rb_node *)((r)->__rb_parent_color & ~3))

#define MM_RB_ROOT	(struct mm_rb_root) { NULL, }
#define	mm_rb_entry(ptr, type, member) mm_container_of(ptr, type, member)

#define MM_RB_EMPTY_ROOT(root)  ((root)->rb_node == NULL)

/* 'empty' nodes are nodes that are known not to be inserted in an rbree */
#define MM_RB_EMPTY_NODE(node)  \
	((node)->__rb_parent_color == (unsigned long)(node))
#define MM_RB_CLEAR_NODE(node)  \
	((node)->__rb_parent_color = (unsigned long)(node))


MM_EXPORT_DLL extern void mm_rb_insert_color(struct mm_rb_node *, struct mm_rb_root *);
MM_EXPORT_DLL extern void mm_rb_erase(struct mm_rb_node *, struct mm_rb_root *);


/* Find logical next and previous nodes in a tree */
MM_EXPORT_DLL extern struct mm_rb_node *mm_rb_next(const struct mm_rb_node *);
MM_EXPORT_DLL extern struct mm_rb_node *mm_rb_prev(const struct mm_rb_node *);
MM_EXPORT_DLL extern struct mm_rb_node *mm_rb_first(const struct mm_rb_root *);
MM_EXPORT_DLL extern struct mm_rb_node *mm_rb_last(const struct mm_rb_root *);

/* Postorder iteration - always visit the parent after its children */
MM_EXPORT_DLL extern struct mm_rb_node *mm_rb_first_postorder(const struct mm_rb_root *);
MM_EXPORT_DLL extern struct mm_rb_node *mm_rb_next_postorder(const struct mm_rb_node *);

/* Fast replacement of a single node without remove/rebalance/add/rebalance */
MM_EXPORT_DLL extern void mm_rb_replace_node(struct mm_rb_node *victim, struct mm_rb_node *newn, 
			    struct mm_rb_root *root);

static mm_inline void mm_rb_link_node(struct mm_rb_node * node, struct mm_rb_node * parent,
				struct mm_rb_node ** rb_link)
{
	node->__rb_parent_color = (unsigned long)parent;
	node->rb_left = node->rb_right = NULL;

	*rb_link = node;
}

#define mm_rb_entry_safe(ptr, type, member) \
	({ typeof(ptr) ____ptr = (ptr); \
	   ____ptr ? mm_rb_entry(____ptr, type, member) : NULL; \
	})

/**
 * mm_rbtree_postorder_for_each_entry_safe - iterate over rb_root in post order of
 * given type safe against removal of rb_node entry
 *
 * @pos:	the 'type *' to use as a loop cursor.
 * @n:		another 'type *' to use as temporary storage
 * @root:	'mm_rb_root *' of the rbtree.
 * @field:	the name of the mm_rb_node field within 'type'.
 */
#define mm_rbtree_postorder_for_each_entry_safe(pos, n, root, field) \
	for (pos = mm_rb_entry_safe(mm_rb_first_postorder(root), typeof(*pos), field); \
	     pos && ({ n = mm_rb_entry_safe(mm_rb_next_postorder(&pos->field), \
			typeof(*pos), field); 1; }); \
	     pos = n)

#include "core/mm_suffix.h"

#endif//__mm_rbtree_h__
