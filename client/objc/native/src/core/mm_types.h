#ifndef __mm_types_h__
#define __mm_types_h__

#include "core/mm_prefix.h"

#include <stdint.h>

// byte		char			short			int				long			long long 		指针
// 16	 	8byte|1char		16byte|2char	16byte|2char	32byte|4char	64byte|8char 	16byte|2char
// 32	 	8byte|1char		16byte|2char	32byte|4char	32byte|4char	64byte|8char 	32byte|4char
// 64	 	8byte|1char		16byte|2char	32byte|4char	64byte|8char	64byte|8char 	64byte|8char

//
typedef float				mm_scalar_t;
typedef double				mm_real_t;
//
typedef float				mm_float32_t;
typedef double				mm_float64_t;

typedef signed char			mm_schar_t;
typedef unsigned char		mm_uchar_t;

typedef signed short		mm_sshort_t;
typedef unsigned short		mm_ushort_t;

typedef signed int			mm_sint_t;
typedef unsigned int		mm_uint_t;

typedef signed long 		mm_slong_t;
typedef unsigned long 		mm_ulong_t;

typedef signed long long 	mm_sllong_t;
typedef unsigned long long	mm_ullong_t;
// 
typedef int8_t				mm_sint8_t;
typedef uint8_t				mm_uint8_t;

typedef int16_t				mm_sint16_t;
typedef uint16_t			mm_uint16_t;

typedef int32_t				mm_sint32_t;
typedef uint32_t			mm_uint32_t;

typedef int64_t				mm_sint64_t;
typedef uint64_t			mm_uint64_t;
//
typedef char				mm_char_t;
typedef short				mm_short_t;
typedef int					mm_int_t;
typedef long				mm_long_t;
typedef long long			mm_llong_t;
//
typedef int 				mm_status_t;

#include "core/mm_suffix.h"
//
#endif//__mm_types_h__