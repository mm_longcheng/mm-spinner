/*
  Red Black Trees
  (C) 1999  Andrea Arcangeli <andrea@suse.de>
  (C) 2002  David Woodhouse <dwmw2@infradead.org>
  (C) 2012  Michel Lespinasse <walken@google.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  linux/include/linux/rbtree_augmented.h
*/

#ifndef __mm_rbtree_augmented_h__
#define __mm_rbtree_augmented_h__

// #include <linux/compiler.h>
// #include <linux/rbtree.h>
#include "core/mm_prefix.h"

#include "core/mm_core.h"

#include "core/mm_rbtree.h"

/*
 * Please note - only struct mm_rb_augment_callbacks and the prototypes for
 * mm_rb_insert_augmented() and mm_rb_erase_augmented() are intended to be public.
 * The rest are implementation details you are not expected to depend on.
 *
 * See Documentation/rbtree.txt for documentation and samples.
 */

struct mm_rb_augment_callbacks {
	void (*propagate)(struct mm_rb_node *node, struct mm_rb_node *stop);
	void (*copy)(struct mm_rb_node *old_node, struct mm_rb_node *new_node);
	void (*rotate)(struct mm_rb_node *old_node, struct mm_rb_node *new_node);
};

MM_EXPORT_DLL extern void __mm_rb_insert_augmented(struct mm_rb_node *node, struct mm_rb_root *root,
	void (*augment_rotate)(struct mm_rb_node *old_node, struct mm_rb_node *new_node));
/*
 * Fixup the mm_rbtree and update the augmented information when rebalancing.
 *
 * On insertion, the user must update the augmented information on the path
 * leading to the inserted node, then call mm_rb_link_node() as usual and
 * mm_rb_augment_inserted() instead of the usual mm_rb_insert_color() call.
 * If mm_rb_augment_inserted() rebalances the rbtree, it will callback into
 * a user provided function to update the augmented information on the
 * affected subtrees.
 */
static mm_inline void
mm_rb_insert_augmented(struct mm_rb_node *node, struct mm_rb_root *root,
		    const struct mm_rb_augment_callbacks *augment)
{
	__mm_rb_insert_augmented(node, root, augment->rotate);
}

#define MM_RB_DECLARE_CALLBACKS(rbstatic, rbname, rbstruct, rbfield,	\
			     rbtype, rbaugmented, rbcompute)		\
static inline void							\
rbname ## _propagate(struct mm_rb_node *rb, struct mm_rb_node *stop)		\
{									\
	while (rb != stop) {						\
		rbstruct *node = mm_rb_entry(rb, rbstruct, rbfield);	\
		rbtype augmented = rbcompute(node);			\
		if (node->rbaugmented == augmented)			\
			break;						\
		node->rbaugmented = augmented;				\
		rb = mm_rb_parent(&node->rbfield);				\
	}								\
}									\
static inline void							\
rbname ## _copy(struct mm_rb_node *rb_old, struct mm_rb_node *rb_new)		\
{									\
	rbstruct *old = mm_rb_entry(rb_old, rbstruct, rbfield);		\
	rbstruct *new = mm_rb_entry(rb_new, rbstruct, rbfield);		\
	new->rbaugmented = old->rbaugmented;				\
}									\
static void								\
rbname ## _rotate(struct mm_rb_node *rb_old, struct mm_rb_node *rb_new)	\
{									\
	rbstruct *old = mm_rb_entry(rb_old, rbstruct, rbfield);		\
	rbstruct *new = mm_rb_entry(rb_new, rbstruct, rbfield);		\
	new->rbaugmented = old->rbaugmented;				\
	old->rbaugmented = rbcompute(old);				\
}									\
rbstatic const struct mm_rb_augment_callbacks rbname = {			\
	rbname ## _propagate, rbname ## _copy, rbname ## _rotate	\
};


#define	MM_RB_RED		0
#define	MM_RB_BLACK	1

#define __mm_rb_parent(pc)    ((struct mm_rb_node *)(pc & ~3))

#define __mm_rb_color(pc)     ((pc) & 1)
#define __mm_rb_is_black(pc)  __mm_rb_color(pc)
#define __mm_rb_is_red(pc)    (!__mm_rb_color(pc))
#define mm_rb_color(rb)       __mm_rb_color((rb)->__rb_parent_color)
#define mm_rb_is_red(rb)      __mm_rb_is_red((rb)->__rb_parent_color)
#define mm_rb_is_black(rb)    __mm_rb_is_black((rb)->__rb_parent_color)

static mm_inline void mm_rb_set_parent(struct mm_rb_node *rb, struct mm_rb_node *p)
{
	rb->__rb_parent_color = mm_rb_color(rb) | (unsigned long)p;
}

static mm_inline void mm_rb_set_parent_color(struct mm_rb_node *rb,
				       struct mm_rb_node *p, int color)
{
	rb->__rb_parent_color = (unsigned long)p | color;
}

static mm_inline void
__mm_rb_change_child(struct mm_rb_node *old_node, struct mm_rb_node *new_node,
		  struct mm_rb_node *parent, struct mm_rb_root *root)
{
	if (parent) {
		if (parent->rb_left == old_node)
			parent->rb_left = new_node;
		else
			parent->rb_right = new_node;
	} else
		root->rb_node = new_node;
}

MM_EXPORT_DLL extern void __mm_rb_erase_color(struct mm_rb_node *parent, struct mm_rb_root *root,
	void (*augment_rotate)(struct mm_rb_node *old_node, struct mm_rb_node *new_node));

static MM_FORCEINLINE struct mm_rb_node *
__mm_rb_erase_augmented(struct mm_rb_node *node, struct mm_rb_root *root,
		     const struct mm_rb_augment_callbacks *augment)
{
	struct mm_rb_node *child = node->rb_right, *tmp = node->rb_left;
	struct mm_rb_node *parent, *rebalance;
	unsigned long pc;

	if (!tmp) {
		/*
		 * Case 1: node to erase has no more than 1 child (easy!)
		 *
		 * Note that if there is one child it must be red due to 5)
		 * and node must be black due to 4). We adjust colors locally
		 * so as to bypass __mm_rb_erase_color() later on.
		 */
		pc = node->__rb_parent_color;
		parent = __mm_rb_parent(pc);
		__mm_rb_change_child(node, child, parent, root);
		if (child) {
			child->__rb_parent_color = pc;
			rebalance = NULL;
		} else
			rebalance = __mm_rb_is_black(pc) ? parent : NULL;
		tmp = parent;
	} else if (!child) {
		/* Still case 1, but this time the child is node->rb_left */
		tmp->__rb_parent_color = pc = node->__rb_parent_color;
		parent = __mm_rb_parent(pc);
		__mm_rb_change_child(node, tmp, parent, root);
		rebalance = NULL;
		tmp = parent;
	} else {
		struct mm_rb_node *successor = child, *child2;
		tmp = child->rb_left;
		if (!tmp) {
			/*
			 * Case 2: node's successor is its right child
			 *
			 *    (n)          (s)
			 *    / \          / \
			 *  (x) (s)  ->  (x) (c)
			 *        \
			 *        (c)
			 */
			parent = successor;
			child2 = successor->rb_right;
			augment->copy(node, successor);
		} else {
			/*
			 * Case 3: node's successor is leftmost under
			 * node's right child subtree
			 *
			 *    (n)          (s)
			 *    / \          / \
			 *  (x) (y)  ->  (x) (y)
			 *      /            /
			 *    (p)          (p)
			 *    /            /
			 *  (s)          (c)
			 *    \
			 *    (c)
			 */
			do {
				parent = successor;
				successor = tmp;
				tmp = tmp->rb_left;
			} while (tmp);
			parent->rb_left = child2 = successor->rb_right;
			successor->rb_right = child;
			mm_rb_set_parent(child, successor);
			augment->copy(node, successor);
			augment->propagate(parent, successor);
		}

		successor->rb_left = tmp = node->rb_left;
		mm_rb_set_parent(tmp, successor);

		pc = node->__rb_parent_color;
		tmp = __mm_rb_parent(pc);
		__mm_rb_change_child(node, successor, tmp, root);
		if (child2) {
			successor->__rb_parent_color = pc;
			mm_rb_set_parent_color(child2, parent, MM_RB_BLACK);
			rebalance = NULL;
		} else {
			unsigned long pc2 = successor->__rb_parent_color;
			successor->__rb_parent_color = pc;
			rebalance = __mm_rb_is_black(pc2) ? parent : NULL;
		}
		tmp = successor;
	}

	augment->propagate(tmp, NULL);
	return rebalance;
}

static MM_FORCEINLINE void
mm_rb_erase_augmented(struct mm_rb_node *node, struct mm_rb_root *root,
		   const struct mm_rb_augment_callbacks *augment)
{
	struct mm_rb_node *rebalance = __mm_rb_erase_augmented(node, root, augment);
	if (rebalance)
		__mm_rb_erase_color(rebalance, root, augment->rotate);
}

#include "core/mm_suffix.h"

#endif//__mm_rbtree_augmented_h__
