#include "core/mm_streambuf.h"
#include "core/mm_alloc.h"

// realloc size (nsize+1024*1024*n,n<32) will case a malloc and memcpy.
MM_EXPORT_DLL void mm_streambuf_init(struct mm_streambuf* p)
{
	// setp
	p->pptr = 0;
	// setg
	p->gptr = 0;
	//
	p->size = MM_STREAMBUF_PAGE_SIZE;
	p->buff = (mm_uint8_t*)mm_malloc(p->size);
}
MM_EXPORT_DLL void mm_streambuf_destroy(struct mm_streambuf* p)
{
	// setp
	p->pptr = 0;
	// setg
	p->gptr = 0;
	//
	mm_free(p->buff);
	//
	p->size = 0;
	p->buff = NULL;
}
MM_EXPORT_DLL void mm_streambuf_copy(struct mm_streambuf* p, struct mm_streambuf* q)
{
	mm_uint8_t* buff = (mm_uint8_t*)mm_malloc(q->size);
	mm_memcpy(buff,q->buff,q->size);
	mm_free(p->buff);
	p->buff = buff;
	p->size = q->size;
	// setp
	p->pptr = q->pptr;
	// setg
	p->gptr = q->gptr;
}
MM_EXPORT_DLL void mm_streambuf_addsize(struct mm_streambuf* p, size_t size)
{
	mm_uint8_t* buff = (mm_uint8_t*)mm_malloc(p->size + size);
	mm_memcpy(buff,p->buff,p->size);
	mm_free(p->buff);
	p->buff = buff;
	p->size += size;
}
MM_EXPORT_DLL void mm_streambuf_removeget(struct mm_streambuf* p)
{
	size_t rsize = p->gptr;
	if (0 < rsize)
	{
		size_t wr = p->pptr;
		mm_memmove(p->buff, p->buff + rsize, p->pptr - p->gptr);
		wr -= rsize;
		// setp
		p->pptr = wr;
		// setg
		p->gptr = 0;
	}
}
MM_EXPORT_DLL void mm_streambuf_removeget_size(struct mm_streambuf* p, size_t rsize)
{
	size_t rdmax = 0;
	mm_streambuf_clearget(p);
	rdmax = p->pptr - p->gptr;
	if (rdmax < rsize)
	{
		rsize = rdmax;
	}
	if (0 < rsize)
	{
		size_t wr = p->pptr;
		mm_memmove(p->buff, p->buff + rsize, p->pptr - p->gptr);
		wr -= rsize;
		// setp
		p->pptr = wr;
		// setg
		p->gptr = 0;
	}
}
MM_EXPORT_DLL void mm_streambuf_clearget(struct mm_streambuf* p)
{
	// setg
	p->gptr = 0;
}

MM_EXPORT_DLL void mm_streambuf_removeput(struct mm_streambuf* p)
{
	size_t wsize = p->pptr;
	if (0 < wsize)
	{
		size_t rd = p->gptr;
		mm_memmove(p->buff, p->buff + wsize, p->pptr - p->gptr);
		rd -= wsize;
		// setp
		p->pptr = 0;
		// setg
		p->gptr = rd;
	}
}
MM_EXPORT_DLL void mm_streambuf_removeput_size(struct mm_streambuf* p, size_t wsize)
{
	size_t wrmax = 0;
	mm_streambuf_clearput(p);
	wrmax = p->pptr - p->gptr;
	if (wrmax < wsize)
	{
		wsize = wrmax;
	}
	if (0 < wsize)
	{
		size_t rd = p->gptr;
		mm_memmove(p->buff, p->buff + wsize, p->pptr - p->gptr);
		rd -= wsize;
		// setp
		p->pptr = 0;
		// setg
		p->gptr = rd;
	}
}
MM_EXPORT_DLL void mm_streambuf_clearput(struct mm_streambuf* p)
{
	// setp
	p->pptr = 0;
}

MM_EXPORT_DLL size_t mm_streambuf_getsize(struct mm_streambuf* p)
{
	return p->gptr;	
}
MM_EXPORT_DLL size_t mm_streambuf_putsize(struct mm_streambuf* p)
{
	return p->pptr;	
}
MM_EXPORT_DLL size_t mm_streambuf_size(struct mm_streambuf* p)
{
	return p->pptr - p->gptr;
}

// set get pointer to new pointer.
MM_EXPORT_DLL void mm_streambuf_setg_ptr(struct mm_streambuf* p, size_t new_ptr)
{
	// setg
	p->gptr = new_ptr;
}
// set put pointer to new pointer.
MM_EXPORT_DLL void mm_streambuf_setp_ptr(struct mm_streambuf* p, size_t new_ptr)
{
	// setp
	p->pptr = new_ptr;
}
MM_EXPORT_DLL void mm_streambuf_reset(struct mm_streambuf* p)
{
	// setp
	p->pptr = 0;
	// setg
	p->gptr = 0;
}
// get get pointer(offset).
MM_EXPORT_DLL size_t mm_streambuf_gptr(struct mm_streambuf* p)
{
	return p->gptr;
}
// get put pointer(offset).
MM_EXPORT_DLL size_t mm_streambuf_pptr(struct mm_streambuf* p)
{
	return p->pptr;
}

MM_EXPORT_DLL size_t mm_streambuf_sgetn(struct mm_streambuf* p, mm_uint8_t* s, size_t o, size_t n)
{
	while( p->gptr + n > p->size )
	{
		mm_streambuf_underflow(p);
	}
	mm_memcpy(s + o, p->buff + p->gptr, n);
	p->gptr += n;
	return n;
}

MM_EXPORT_DLL size_t mm_streambuf_sputn(struct mm_streambuf* p, mm_uint8_t* s, size_t o, size_t n)
{
	while( p->pptr + n > p->size )
	{
		mm_streambuf_overflow(p);
	}
	mm_memcpy(p->buff + p->pptr, s + o, n);
	p->pptr += n;
	return n;
}

MM_EXPORT_DLL void mm_streambuf_gbump(struct mm_streambuf* p, size_t n)
{
	p->gptr += n;
}
MM_EXPORT_DLL void mm_streambuf_pbump(struct mm_streambuf* p, size_t n)
{
	p->pptr += n;
}

MM_EXPORT_DLL void mm_streambuf_underflow(struct mm_streambuf* p)
{
	if(0 < p->gptr)
	{
		// if gptr have g data.we try remove for free space.
		mm_streambuf_removeget(p);
	}
	else
	{
		// if gptr no data we just add page size.
		mm_streambuf_addsize(p,MM_STREAMBUF_PAGE_SIZE);
	}
}
MM_EXPORT_DLL void mm_streambuf_overflow(struct mm_streambuf* p)
{
	mm_streambuf_addsize(p,MM_STREAMBUF_PAGE_SIZE);
}
// if p->gptr >= MM_STREAMBUF_PAGE_SIZE we realloc a thin memory.ohter we just remove get.make sure the p->pptr + n <= p->size
MM_EXPORT_DLL void mm_streambuf_try_decrease(struct mm_streambuf* p, size_t n)
{
	size_t cursz = p->pptr - p->gptr;
	size_t fresz = p->size - cursz;
	// free size > n and MM_STREAMBUF_PAGE_SIZE < fresz - n.
	if ( fresz > n && MM_STREAMBUF_PAGE_SIZE < fresz - n )
	{
		// the surplus size for decrease.
		size_t ssize = fresz - n;
		// memmove size is <  MM_STREAMBUF_PAGE_SIZE.
		// decrease
		mm_uint8_t* buff = NULL;
		// use memove to remove the already get buffer.
		size_t wr = p->pptr;
		p->size -= (ssize / MM_STREAMBUF_PAGE_SIZE) * MM_STREAMBUF_PAGE_SIZE;
		buff = (mm_uint8_t*)mm_malloc(p->size);
		mm_memcpy(buff, p->buff + p->gptr, cursz);
		mm_free(p->buff);
		p->buff = buff;
		wr -= p->gptr;
		// setp
		p->pptr = wr;
		// setg
		p->gptr = 0;
	}
	else
	{
		// use memove to remove the already get buffer.
		mm_streambuf_removeget(p);
	}
}
// check overflow when need write size n.
MM_EXPORT_DLL void mm_streambuf_try_increase(struct mm_streambuf* p, size_t n)
{
	if ( p->pptr + n > p->size )
	{
		// the increase process is here.
		// assert( p->pptr + n > p->size && "(p->pptr + n > p->size) is invalid.");
		// size_t dsz = p->pptr + n - p->size;
		// assert( 0 < dsz );
		// size_t asz = dsz / MM_STREAMBUF_PAGE_SIZE;
		// size_t bsz = dsz % MM_STREAMBUF_PAGE_SIZE;
		// assert( 0 != bsz );
		// size_t nsz = ( asz + ( 0 == bsz ? 0 : 1 ) );
		// assert( 0 < nsz );
		// mm_streambuf_addsize(p, nsz * MM_STREAMBUF_PAGE_SIZE);

		// the quick process is here.
		size_t dsz = p->pptr + n - p->size;
		size_t asz = dsz / MM_STREAMBUF_PAGE_SIZE;
		size_t nsz = asz + 1;
		mm_streambuf_addsize(p, nsz * MM_STREAMBUF_PAGE_SIZE);
	}
}
// aligned streambuf memory if need sputn n size buffer before.
MM_EXPORT_DLL void mm_streambuf_aligned_memory(struct mm_streambuf* p, size_t n)
{
	// if current pptr + n not enough for target n.we make a aligned memory.
	if ( p->pptr + n > p->size )
	{
		size_t cursz = p->pptr - p->gptr;
		// the free size is enough and memmove size is <  MM_STREAMBUF_PAGE_SIZE.
		if ( p->size - cursz >= n && MM_STREAMBUF_PAGE_SIZE > cursz)
		{
			mm_streambuf_try_decrease(p, n);
		} 
		else
		{
			mm_streambuf_try_increase(p, n);
		}
	}
}