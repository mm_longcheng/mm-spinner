﻿#ifndef __mm_logger_manager_h__
#define __mm_logger_manager_h__
#include "core/mm_core.h"
#include "core/mm_time.h"
#include "core/mm_logger.h"
#include "core/mm_prefix.h"
#include "core/mm_thread.h"
#include "core/mm_atomic.h"
#include <stdlib.h>
#include <pthread.h>

typedef void (*logger_manager_console_printf)(const char* section,const char* time_stamp_string, int lvl,const char* message);
struct mm_logger_manager_callback
{
	logger_manager_console_printf console_printf;
	void* obj;// weak ref. user data for callback.
};
MM_EXPORT_DLL void mm_logger_manager_callback_init(struct mm_logger_manager_callback* p);
MM_EXPORT_DLL void mm_logger_manager_callback_destroy(struct mm_logger_manager_callback* p);

struct mm_logger_manager
{
	struct mm_logger* logger_impl;// default is mm_logger_instance.
	FILE* stream;// strong ref.
	struct mm_string logger_section;// default "mm"
	struct mm_string logger_path;// default "log"
	struct mm_string file_name;// default "mm"
	struct mm_string current_file_prefix;
	struct mm_string current_file_name;
	struct mm_logger_manager_callback callback;// default is os console printf.
	//
	mm_atomic_t locker;
	mm_atomic_t logger_mutex;
	pthread_mutex_t signal_mutex;
	pthread_cond_t signal_cond;
	pthread_t file_size_thread;// thread for check file size.
	//
	size_t rolling_index;// default 100.
	size_t current_index;// default 0.
	size_t file_size;// default 300*1024*1024(300M).
	//
	mm_uint_t logger_level;// default LOG_VERBOSE.
	mm_msec_t check_file_size_time;// default 5000ms.
	// we not use byte field,macosx compile different.
	mm_sint8_t is_rolling;// default is rolling(1)
	mm_sint8_t is_console;// default is not console(0)
	mm_sint8_t is_immediately;// default is not(0)
	mm_sint8_t state;// mm_thread_state_t,default is ts_closed(0)
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL extern struct mm_logger_manager* mm_logger_manager_instance();
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_logger_manager_init(struct mm_logger_manager* p);
MM_EXPORT_DLL void mm_logger_manager_destroy(struct mm_logger_manager* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_logger_manager_lock(struct mm_logger_manager* p);
MM_EXPORT_DLL void mm_logger_manager_unlock(struct mm_logger_manager* p);
//////////////////////////////////////////////////////////////////////////
// assign callback.
MM_EXPORT_DLL void mm_logger_manager_assign_callback(struct mm_logger_manager* p, struct mm_logger_manager_callback* callback);
// assign logger instance.default is mm_logger_instance.
MM_EXPORT_DLL void mm_logger_manager_assign_logger(struct mm_logger_manager* p, struct mm_logger* logger);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_logger_manager_open(struct mm_logger_manager* p);
MM_EXPORT_DLL void mm_logger_manager_close(struct mm_logger_manager* p);
MM_EXPORT_DLL void mm_logger_manager_flush(struct mm_logger_manager* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_logger_manager_check_file_size(struct mm_logger_manager* p);
MM_EXPORT_DLL void mm_logger_manager_generate_current_file_name(struct mm_logger_manager* p);
MM_EXPORT_DLL void mm_logger_manager_generate_current_file_prefix(struct mm_logger_manager* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_logger_manager_start(struct mm_logger_manager* p);
MM_EXPORT_DLL void mm_logger_manager_interrupt(struct mm_logger_manager* p);
MM_EXPORT_DLL void mm_logger_manager_shutdown(struct mm_logger_manager* p);
MM_EXPORT_DLL void mm_logger_manager_join(struct mm_logger_manager* p);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_logger_manager_logger_section(struct mm_logger* p, const char* section, mm_uint32_t lvl, const char* message);
//////////////////////////////////////////////////////////////////////////

#include "core/mm_suffix.h"

#endif//__mm_logger_manager_h__
