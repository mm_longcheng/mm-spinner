#ifndef __mm_streambuf_h__
#define __mm_streambuf_h__

#include "core/mm_prefix.h"

#include "core/mm_core.h"
#include "core/mm_platform.h"
#include "core/mm_types.h"
#include <stdio.h>

// streambuf page size.default is 1024.
#define MM_STREAMBUF_PAGE_SIZE 1024

// output sequence (put)
//    +++++-------
//    |    |      |
//    0  pptr   size
///////////////////////
// input  sequence (get)
//    +++++-------
//    |    |      |
//    0  gptr   size
struct mm_streambuf
{
	// output sequence (put)
	size_t pptr;
	// input  sequence (get)
	size_t gptr;

	// max size for this streambuf.default is MM_STREAMBUF_PAGE_SIZE = 1024
	size_t size;
	mm_uint8_t* buff;// buffer for real data.
};
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_streambuf_init(struct mm_streambuf* p);
MM_EXPORT_DLL void mm_streambuf_destroy(struct mm_streambuf* p);
//////////////////////////////////////////////////////////////////////////
// copy q to p.
MM_EXPORT_DLL void mm_streambuf_copy(struct mm_streambuf* p, struct mm_streambuf* q);
// add max streambuf size.not checking overflow,use it when overflow only.
MM_EXPORT_DLL void mm_streambuf_addsize(struct mm_streambuf* p, size_t size);

MM_EXPORT_DLL void mm_streambuf_removeget(struct mm_streambuf* p);
MM_EXPORT_DLL void mm_streambuf_removeget_size(struct mm_streambuf* p, size_t rsize);
MM_EXPORT_DLL void mm_streambuf_clearget(struct mm_streambuf* p);

MM_EXPORT_DLL void mm_streambuf_removeput(struct mm_streambuf* p);
MM_EXPORT_DLL void mm_streambuf_removeput_size(struct mm_streambuf* p, size_t wsize);
MM_EXPORT_DLL void mm_streambuf_clearput(struct mm_streambuf* p);

MM_EXPORT_DLL size_t mm_streambuf_getsize(struct mm_streambuf* p);
MM_EXPORT_DLL size_t mm_streambuf_putsize(struct mm_streambuf* p);

// size for input and output interval.
MM_EXPORT_DLL size_t mm_streambuf_size(struct mm_streambuf* p);

// set get pointer to new pointer.
MM_EXPORT_DLL void mm_streambuf_setg_ptr(struct mm_streambuf* p, size_t new_ptr);
// set put pointer to new pointer.
MM_EXPORT_DLL void mm_streambuf_setp_ptr(struct mm_streambuf* p, size_t new_ptr);
// set get and put pointer to zero.
MM_EXPORT_DLL void mm_streambuf_reset(struct mm_streambuf* p);

// get get pointer(offset).
MM_EXPORT_DLL size_t mm_streambuf_gptr(struct mm_streambuf* p);
// get put pointer(offset).
MM_EXPORT_DLL size_t mm_streambuf_pptr(struct mm_streambuf* p);
//////////////////////////////////////////////////////////////////////////
// get data s + offset length is n.and gbump n.
MM_EXPORT_DLL size_t mm_streambuf_sgetn(struct mm_streambuf* p, mm_uint8_t* s, size_t o, size_t n);
// put data s + offset length is n.and pbump n.
MM_EXPORT_DLL size_t mm_streambuf_sputn(struct mm_streambuf* p, mm_uint8_t* s, size_t o, size_t n);
// gptr += n
MM_EXPORT_DLL void mm_streambuf_gbump(struct mm_streambuf* p, size_t n);
// pptr += n
MM_EXPORT_DLL void mm_streambuf_pbump(struct mm_streambuf* p, size_t n);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_streambuf_underflow(struct mm_streambuf* p);
MM_EXPORT_DLL void mm_streambuf_overflow(struct mm_streambuf* p);
//////////////////////////////////////////////////////////////////////////
// if p->gptr >= MM_STREAMBUF_PAGE_SIZE we realloc a thin memory.ohter we just remove get.make sure the p->pptr + n <= p->size
MM_EXPORT_DLL void mm_streambuf_try_decrease(struct mm_streambuf* p, size_t n);
// check overflow when need write size n.
MM_EXPORT_DLL void mm_streambuf_try_increase(struct mm_streambuf* p, size_t n);
// aligned streambuf memory if need sputn n size buffer before.
MM_EXPORT_DLL void mm_streambuf_aligned_memory(struct mm_streambuf* p, size_t n);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_streambuf_h__