LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

include $(LOCAL_PATH)/mm_core_static.mk
include $(LOCAL_PATH)/mm_net_static.mk
include $(LOCAL_PATH)/mm_calendar_static.mk

include $(LOCAL_PATH)/mm_core_shared.mk
include $(LOCAL_PATH)/mm_net_shared.mk
include $(LOCAL_PATH)/mm_calendar_shared.mk