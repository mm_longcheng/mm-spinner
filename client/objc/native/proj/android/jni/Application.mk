APP_ABI := armeabi-v7a

APP_STL := gnustl_shared 

APP_MODULES += mm_core_static
APP_MODULES += mm_net_static
APP_MODULES += mm_calendar_static

APP_MODULES += mm_core_shared
APP_MODULES += mm_net_shared
APP_MODULES += mm_calendar_shared