LOCAL_PATH := $(call my-dir)
########################################################################
# include $(mm_clear_vars.mk)  
include $(CLEAR_VARS)  
MY_COMMON_FLAGS :=
MY_FILES_PATH :=
MY_FILTER_OUT :=
MY_FILES_SUFFIX :=
########################################################################
LOCAL_MODULE := mm_core_shared

LOCAL_MODULE_FILENAME := libmm_core

MY_COMMON_FLAGS += -Wall
########################################################################
# include $(mm_ldlibs.mk)
MY_COMMON_FLAGS += -fPIC -D__ANDROID__
LOCAL_CFLAGS   += $(MY_COMMON_FLAGS) 
LOCAL_CXXFLAGS += $(MY_COMMON_FLAGS) -std=c++11 -x c++ -fexceptions -frtti 
LOCAL_CPPFLAGS += $(MY_COMMON_FLAGS) -std=c++11 -x c++ -fexceptions -frtti

LOCAL_LDLIBS	+= -Llibs/$(APP_ABI)
LOCAL_LDLIBS	+= -Lobj/local/$(APP_ABI)

LOCAL_LDLIBS	+= -landroid -lc -lm -llog
LOCAL_LDLIBS  	+= -shared -fPIC -rdynamic -ldl
########################################################################
LOCAL_LDLIBS    += 

LOCAL_SHARED_LIBRARIES += 

LOCAL_STATIC_LIBRARIES += cpufeatures

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/os/android
LOCAL_C_INCLUDES += $(ANDROID_NDK)/sources/cpufeatures 

#  
# config self source file path ,suffix.
MY_FILES_PATH += $(LOCAL_PATH)/../../../src/atomic
MY_FILES_PATH += $(LOCAL_PATH)/../../../src/core
MY_FILES_PATH += $(LOCAL_PATH)/../../../src/os/android
MY_FILES_PATH += $(LOCAL_PATH)/../../../src/container
MY_FILES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_FILTER_OUT += ../../proj.android%
MY_FILTER_OUT += ../../../src/os/darwin%
MY_FILTER_OUT += ../../../src/os/linux%
MY_FILTER_OUT += ../../../src/os/windows%

MY_FILES_SUFFIX += %.cpp %.c %.cc
########################################################################
# include $(mm_rwildcard.mk)
# traverse all file function.
rwildcard=$(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))

# get all source file.
MY_ALL_FILES := $(foreach src_path,$(MY_FILES_PATH), $(call rwildcard,$(src_path),*.*) ) 
MY_ALL_FILES := $(MY_ALL_FILES:$(MY_CPP_PATH)/./%=$(MY_CPP_PATH)%)
MY_SRC_LIST  := $(filter $(MY_FILES_SUFFIX),$(MY_ALL_FILES)) 
MY_SRC_LIST  := $(MY_SRC_LIST:$(LOCAL_PATH)/%=%)
MY_SRC_LIST  := $(filter-out $(MY_FILTER_OUT),$(MY_SRC_LIST)) 

# traverse all include path.
# MY_ALL_DIRS := $(dir $(foreach src_path,$(MY_FILES_PATH), $(call rwildcard,$(src_path),*/) ) )
# remove duplicate
# MY_ALL_DIRS := $(call sort,$(MY_ALL_DIRS))
 
# assign to NDK system.
LOCAL_SRC_FILES  += $(MY_SRC_LIST)
# LOCAL_C_INCLUDES += $(MY_ALL_DIRS)

# $(info $(LOCAL_SRC_FILES))
# $(info $(LOCAL_C_INCLUDES))          
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################
SWAP_PATH := $(LOCAL_PATH)
#
$(call import-module,android/cpufeatures) 
########################################################################
LOCAL_PATH := $(SWAP_PATH)