//
//  ViewController.h
//  mm_native
//
//  Created by longer on 2017/6/28.
//  Copyright © 2017年 longer. All rights reserved.
//

#import <Foundation/Foundation.h>

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
#import <Protobuf/GPBProtocolBuffers.h>
#else
#import "GPBProtocolBuffers.h"
#endif

#import "core/mm_core.h"
#import "net/mm_mailbox.h"
#import "net/mm_headset.h"
#import "net/mm_gl_tcp.h"
#import "net/mm_gl_udp.h"

// max length for cut out.
#define MM_MAX_MESSAGE_LOGGER_LENGTH 1024
// min lenght for not return.
#define MM_MIN_MESSAGE_LOGGER_LENGTH 64

@interface mm_protobuf : NSObject

+(void)logger_append_packet :(NSMutableString*)value pack:(struct mm_packet*) pack;
+(void)logger_append_message :(NSMutableString*)value message:(GPBMessage*) message;
// logger append packet and message to string value.
+(void)logger_append_packet_message :(NSMutableString*)value
                                pack:(struct mm_packet*) pack
                             message:(GPBMessage*) message;
//////////////////////////////////////////////////////////////////////////
// decode message form mm_packet.
// return code.
//   -1 is failure.
//    0 is success.
+(int)decode_message:(struct mm_packet*) pack
             message:(GPBMessage*) message;
// encode message message --> mm_packet.
// return code.
//   -1 is failure.
//    0 is success.
+(int)encode_message:(struct mm_packet*) pack
             message:(GPBMessage*) message;
// encode message message --> mm_packet.
// return code.
//   -1 is failure.
//    0 is success.
+(int)encode_message_streambuf_packet:(struct mm_streambuf*) streambuf
                              message:(GPBMessage*) message
                                 pack:(struct mm_packet*) pack
                                  mid:(mm_uint32_t) mid
                                  sid:(mm_uint64_t) sid
                                  uid:(mm_uint64_t) uid;

+(int)n_tcp_append_rs :(struct mm_mailbox*) mailbox
                   tcp:(struct mm_tcp*) tcp
                   mid:(mm_uint32_t) mid
                rs_msg:(GPBMessage*) rs_msg
               rq_pack:(struct mm_packet*) rq_pack
               rs_pack:(struct mm_packet*) rs_pack;

+(int)n_udp_append_rs :(struct mm_headset*) headset
                   tcp:(struct mm_udp*) udp
                   mid:(mm_uint32_t) mid
                rs_msg:(GPBMessage*) rs_msg
               rq_pack:(struct mm_packet*) rq_pack
               rs_pack:(struct mm_packet*) rs_pack;

+(int)q_tcp_append_rq :(struct mm_net_tcp*) net_tcp
                   uid:(mm_uint64_t) uid
                   mid:(mm_uint32_t) mid
				rq_msg:(GPBMessage*) rq_msg
               rq_pack:(struct mm_packet*) rq_pack;

+(int)q_udp_append_rq :(struct mm_net_udp*) net_udp
                   uid:(mm_uint64_t) uid
                   mid:(mm_uint32_t) mid
				rq_msg:(GPBMessage*) rq_msg
               rq_pack:(struct mm_packet*) rq_pack;

+(int)n_tcp_append_nt :(struct mm_mailbox*) mailbox
                   tcp:(struct mm_tcp*) tcp
                   uid:(mm_uint64_t) uid
                   mid:(mm_uint32_t) mid
                nt_msg:(GPBMessage*) nt_msg
               nt_pack:(struct mm_packet*) nt_pack;

// proxy append
// net_tcp t_tcp is target.rq_pack rq pack pr_pack proxy pack.
+(int)p_tcp_append_rq :(struct mm_net_tcp*) net_tcp
                 t_tcp:(struct mm_tcp*) t_tcp
                   pid:(mm_uint32_t) pid
                   sid:(mm_uint64_t) sid
               rq_pack:(struct mm_packet*) rq_pack
               pr_pack:(struct mm_packet*) pr_pack;

// proxy append
// mailbox t_tcp is target.rs_pack rs pack pr_pack proxy pack.
+(int)p_tcp_append_rq :(struct mm_mailbox*) mailbox
                 t_tcp:(struct mm_tcp*) t_tcp
               rs_pack:(struct mm_packet*) rs_pack
               pr_pack:(struct mm_packet*) pr_pack;

// lock it outside manual.use for only mm_mailbox send.
+(int)n_tcp_flush_send :(struct mm_tcp*) tcp;
// lock it outside manual.use for only mm_net_udp send.
+(int)n_udp_flush_send :(struct mm_udp*) udp;
// lock it outside manual.use for client.
+(int)n_net_tcp_flush_send :(struct mm_net_tcp*) net_tcp;
// lock it outside manual.use for client.
+(int)n_net_udp_flush_send :(struct mm_net_udp*) net_udp;

+(int)n_udp_buffer_send_reset :(struct mm_udp*) udp;
+(int)n_udp_buffer_send_flush :(struct mm_udp*) udp 
						remote:(struct sockaddr_storage*) remote;


// thread safe encode and append message.
+(int)q_tcp_flush_message_append :(struct mm_gl_tcp*) gl_tcp
                              uid:(mm_uint64_t) uid
                              mid:(mm_uint32_t) mid
                           rq_msg:(GPBMessage*) rq_msg
                          rq_pack:(struct mm_packet*) rq_pack;

+(int)q_udp_flush_message_append :(struct mm_gl_udp*) gl_udp
                              mid:(mm_uint32_t) mid
                           rq_msg:(GPBMessage*) rq_msg
                          rq_pack:(struct mm_packet*) rq_pack;
// signal queue tcp flush.
+(int)q_tcp_flush_signal :(struct mm_gl_tcp*) gl_tcp;
// signal queue udp flush.
+(int)q_udp_flush_signal :(struct mm_gl_udp*) gl_udp;

@end
