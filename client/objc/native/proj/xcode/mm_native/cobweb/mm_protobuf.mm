//
//  ViewController.m
//  mm_native
//
//  Created by longer on 2017/6/28.
//  Copyright © 2017年 longer. All rights reserved.
//

#import "mm_protobuf.h"
#import "core/mm_alloc.h"
#import "core/mm_logger.h"
#import "net/mm_packet.h"
#import "net/mm_streambuf_packet.h"


@interface mm_protobuf ()

@end

@implementation mm_protobuf

+(void)logger_append_packet :(NSMutableString*)value pack:(struct mm_packet*) pack
{
    [value appendFormat:@"uid:%" PRIu64 " mid:0x%08X",pack->phead.uid,pack->phead.mid];
}
+(void)logger_append_message :(NSMutableString*)value message:(GPBMessage*) message
{
    size_t bsz = 0;
    size_t ssz = 0;
    NSString* message_string = [[NSString alloc] init];
    //
    message_string = [message_string stringByAppendingFormat:@"%@",message];
    bsz = message.serializedSize;
    ssz = message_string.length;
    if (MM_MIN_MESSAGE_LOGGER_LENGTH >= ssz)
    {
        // actually we not need '\n' any where.
        // if need look pretty.shut down this line.
        message_string = [message_string stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    }
    // append binary size and string size.
    [value appendFormat:@" [%" PRIuPTR "][%" PRIuPTR "]:= ",bsz,ssz];
    // if message string is to long.we cut it.
    if(MM_MAX_MESSAGE_LOGGER_LENGTH < ssz)
    {
        NSString* message_string_sub = [message_string substringToIndex:MM_MAX_MESSAGE_LOGGER_LENGTH];
        [value appendFormat:@"%@",message_string_sub];
        [value appendFormat:@"..."];
    }
    else
    {
        [value appendFormat:@"%@",message_string];
    }
    message_string = nil;
}
// logger append packet and message to string value.
+(void)logger_append_packet_message :(NSMutableString*)value
                                pack:(struct mm_packet*) pack
                             message:(GPBMessage*) message
{
    [mm_protobuf logger_append_packet :value pack:pack];
    [value appendFormat:@" "];
    [mm_protobuf logger_append_message :value message:message];
}
//////////////////////////////////////////////////////////////////////////
// decode message form mm_packet.
// return code.
//   -1 is failure.
//    0 is success.
+(int)decode_message:(struct mm_packet*) pack message:(GPBMessage*) message
{
    int rt = -1;
    NSData* data = [NSData dataWithBytesNoCopy:(void*)(pack->bbuff.buffer + pack->bbuff.offset)
                                        length:pack->bbuff.length
                                  freeWhenDone:NO];
    @try
    {
        [message mergeFromData:data extensionRegistry:nil];
        rt = 0;
    }
    @catch (NSException *exception)
    {
        // real Exception.
        struct mm_logger* g_logger = mm_logger_instance();
        const char* message = [exception.reason cStringUsingEncoding:NSASCIIStringEncoding];
        mm_logger_log_E(g_logger, "%s %d exception:%s", __FUNCTION__, __LINE__, message);
    }
    return rt;
}
// encode message message --> mm_packet.
// return code.
//   -1 is failure.
//    0 is success.
+(int)encode_message:(struct mm_packet*) pack
             message:(GPBMessage*) message
{
    int rt = -1;
    @try
    {
        NSData* _data = [message data];
        if (nil != _data)
        {
            mm_memcpy((void*)(pack->bbuff.buffer + pack->bbuff.offset), _data.bytes, pack->bbuff.length);
            rt = 0;
        }
    }
    @catch (NSException *exception)
    {
        // real Exception.
        struct mm_logger* g_logger = mm_logger_instance();
        const char* message = [exception.reason cStringUsingEncoding:NSASCIIStringEncoding];
        mm_logger_log_E(g_logger, "%s %d exception:%s", __FUNCTION__, __LINE__, message);
    }
    return rt;    
}
// encode message message --> mm_packet.
// return code.
//   -1 is failure.
//    0 is success.
+(int)encode_message_streambuf_packet:(struct mm_streambuf*) streambuf
                              message:(GPBMessage*) message
                                 pack:(struct mm_packet*) pack
                                  mid:(mm_uint32_t) mid
                                  sid:(mm_uint64_t) sid
                                  uid:(mm_uint64_t) uid
{
    int rt = -1;
    pack->hbuff.length = MM_MSG_COMM_HEAD_SIZE;
    pack->bbuff.length = message->ByteSizeLong();
    mm_streambuf_packet_overdraft(streambuf, pack);
    mm_packet_head_base_zero(pack);
    pack->phead.mid = mid;
    pack->phead.sid = sid;
    pack->phead.uid = uid;
    mm_packet_head_base_encode(pack);
    rt = [mm_protobuf encode_message: rs_pack message: rs_msg];
    return rt;
}

+(int)n_tcp_append_rs :(struct mm_mailbox*) mailbox
                   tcp:(struct mm_tcp*) tcp
                   mid:(mm_uint32_t) mid
				rs_msg:(GPBMessage*) rs_msg
               rq_pack:(struct mm_packet*) rq_pack
               rs_pack:(struct mm_packet*) rs_pack
{
    int rt = -1;
    rs_pack->hbuff.length = rq_pack->hbuff.length;
    rs_pack->bbuff.length = (mm_uint32_t)[rs_msg serializedSize];
    mm_streambuf_packet_overdraft(&tcp->buff_send, rs_pack);
    mm_packet_head_base_copy(rs_pack,rq_pack);
    rs_pack->phead.mid = mid;
    mm_packet_head_base_encode(rs_pack);
    rt = [mm_protobuf encode_message: rs_pack message: rs_msg];
    mm_mailbox_crypto_encrypt(mailbox,tcp,rs_pack->bbuff.buffer, rs_pack->bbuff.offset, rs_pack->bbuff.length);
    return rt;
}
+(int)n_udp_append_rs :(struct mm_headset*) headset
                   tcp:(struct mm_udp*) udp
                   mid:(mm_uint32_t) mid
				rs_msg:(GPBMessage*) rs_msg
               rq_pack:(struct mm_packet*) rq_pack
               rs_pack:(struct mm_packet*) rs_pack
{
    int rt = -1;
    rs_pack->hbuff.length = rq_pack->hbuff.length;
    rs_pack->bbuff.length = (mm_uint32_t)[rs_msg serializedSize];
    mm_streambuf_packet_overdraft(&udp->buff_send, rs_pack);
    mm_packet_head_base_copy(rs_pack,rq_pack);
    rs_pack->phead.mid = mid;
    mm_packet_head_base_encode(rs_pack);
    rt = [mm_protobuf encode_message: rs_pack message: rs_msg];
    mm_headset_crypto_encrypt(headset,udp,rs_pack->bbuff.buffer, rs_pack->bbuff.offset, rs_pack->bbuff.length);
    return rt;
}
+(int)q_tcp_append_rq :(struct mm_net_tcp*) net_tcp
                   uid:(mm_uint64_t) uid
                   mid:(mm_uint32_t) mid
                rq_msg:(GPBMessage*) rq_msg
               rq_pack:(struct mm_packet*) rq_pack
{
    int rt = -1;
    mm_tcp* tcp = &net_tcp->tcp;
    rq_pack->hbuff.length = MM_MSG_COMM_HEAD_SIZE;
    rq_pack->bbuff.length = (mm_uint32_t)[rq_msg serializedSize];
    mm_streambuf_packet_overdraft(&tcp->buff_send, rq_pack);
    mm_packet_head_base_zero(rq_pack);
    rq_pack->phead.mid = mid;
    rq_pack->phead.uid = uid;
    mm_packet_head_base_encode(rq_pack);
    rt = [mm_protobuf encode_message: rq_pack message: rq_msg];
    mm_net_tcp_crypto_encrypt(net_tcp,tcp,rq_pack->bbuff.buffer, rq_pack->bbuff.offset, rq_pack->bbuff.length);
    return rt;
}
+(int)q_udp_append_rq :(struct mm_net_udp*) net_udp
                   uid:(mm_uint64_t) uid
                   mid:(mm_uint32_t) mid
                rq_msg:(GPBMessage*) rq_msg
               rq_pack:(struct mm_packet*) rq_pack
{
    int rt = -1;
    mm_udp* udp = &net_udp->udp;
    rq_pack->hbuff.length = MM_MSG_COMM_HEAD_SIZE;
    rq_pack->bbuff.length = (mm_uint32_t)[rq_msg serializedSize];
    mm_streambuf_packet_overdraft(&udp->buff_send, rq_pack);
    mm_packet_head_base_zero(rq_pack);
    rq_pack->phead.mid = mid;
    rq_pack->phead.uid = uid;
    mm_packet_head_base_encode(rq_pack);
    rt = [mm_protobuf encode_message: rq_pack message: rq_msg];
    mm_net_udp_crypto_encrypt(net_udp,udp,rq_pack->bbuff.buffer, rq_pack->bbuff.offset, rq_pack->bbuff.length);
    return rt;
}
+(int)n_tcp_append_nt :(struct mm_mailbox*) mailbox
                   tcp:(struct mm_tcp*) tcp
                   uid:(mm_uint64_t) uid
                   mid:(mm_uint32_t) mid
                nt_msg:(GPBMessage*) nt_msg
               nt_pack:(struct mm_packet*) nt_pack
{
    int rt = -1;
    nt_pack->hbuff.length = MM_MSG_COMM_HEAD_SIZE;
    nt_pack->bbuff.length = (mm_uint32_t)[nt_msg serializedSize];
    mm_streambuf_packet_overdraft(&tcp->buff_send, nt_pack);
    mm_packet_head_base_zero(nt_pack);
    nt_pack->phead.mid = mid;
    nt_pack->phead.uid = uid;
    mm_packet_head_base_encode(nt_pack);
    rt = [mm_protobuf encode_message: nt_pack message: nt_msg];
    mm_mailbox_crypto_encrypt(mailbox,tcp,nt_pack->bbuff.buffer, nt_pack->bbuff.offset, nt_pack->bbuff.length);
    return rt;
}
// proxy append
// net_tcp t_tcp is target.rq_pack rq pack pr_pack proxy pack.
+(int)p_tcp_append_rq :(struct mm_net_tcp*) net_tcp
                 t_tcp:(struct mm_tcp*) t_tcp
                   pid:(mm_uint32_t) pid
                   sid:(mm_uint64_t) sid
               rq_pack:(struct mm_packet*) rq_pack
               pr_pack:(struct mm_packet*) pr_pack
{
    pr_pack->hbuff.length = rq_pack->hbuff.length;
    pr_pack->bbuff.length = rq_pack->bbuff.length;
    mm_streambuf_packet_overdraft(&t_tcp->buff_send, pr_pack);
    mm_packet_head_base_copy(pr_pack,rq_pack);
    pr_pack->phead.pid = pid;
    pr_pack->phead.sid = sid;
    mm_packet_head_base_encode(pr_pack);
    mm_memcpy((void*)(pr_pack->bbuff.buffer + pr_pack->bbuff.offset),(void*)(rq_pack->bbuff.buffer + rq_pack->bbuff.offset),rq_pack->bbuff.length);
    mm_net_tcp_crypto_encrypt(net_tcp,t_tcp,pr_pack->bbuff.buffer, pr_pack->bbuff.offset, pr_pack->bbuff.length);
    return 0;
}

// proxy append
// mailbox t_tcp is target.rs_pack rs pack pr_pack proxy pack.
+(int)p_tcp_append_rq :(struct mm_mailbox*) mailbox
                 t_tcp:(struct mm_tcp*) t_tcp
               rs_pack:(struct mm_packet*) rs_pack
               pr_pack:(struct mm_packet*) pr_pack
{
    pr_pack->hbuff.length = rs_pack->hbuff.length;
    pr_pack->bbuff.length = rs_pack->bbuff.length;
    mm_streambuf_packet_overdraft(&t_tcp->buff_send, pr_pack);
    mm_packet_head_base_copy(pr_pack,rs_pack);
    mm_packet_head_base_encode(pr_pack);
    mm_memcpy((void*)(pr_pack->bbuff.buffer + pr_pack->bbuff.offset),(void*)(rs_pack->bbuff.buffer + rs_pack->bbuff.offset),rs_pack->bbuff.length);
    mm_mailbox_crypto_encrypt(mailbox,t_tcp,pr_pack->bbuff.buffer, pr_pack->bbuff.offset, pr_pack->bbuff.length);
    return 0;
}

// lock it outside manual.use for only mm_mailbox send.
+(int)n_tcp_flush_send :(struct mm_tcp*) tcp
{
    int rt = -1;
    rt = mm_tcp_flush_send(tcp);
    return rt;
}
// lock it outside manual.use for only mm_net_udp send.
+(int)n_udp_flush_send :(struct mm_udp*) udp
{
    int rt = -1;
    rt = mm_udp_flush_send(udp);
    return rt;
}
// lock it outside manual.use for client.
+(int)n_net_tcp_flush_send :(struct mm_net_tcp*) net_tcp
{
    int rt = -1;
    rt = mm_net_tcp_flush_send(net_tcp);
    return rt;
}
// lock it outside manual.use for client.
+(int)n_net_udp_flush_send :(struct mm_net_udp*) net_udp
{
    int rt = -1;
    rt = mm_net_udp_flush_send(net_udp);
    return rt;
}
+(int)n_udp_buffer_send_reset :(struct mm_udp*) udp
{
	mm_streambuf_reset(&udp->buff_send);
	return 0;
}
+(int)n_udp_buffer_send_flush :(struct mm_udp*) udp 
						remote:(struct sockaddr_storage*) remote
{
	int rt = -1;
	struct mm_streambuf* streambuf = &udp->buff_send;
	size_t sz = mm_streambuf_size(streambuf);
	rt = mm_udp_buffer_send(udp, udp->buff_send.buff, (mm_uint32_t)udp->buff_send.gptr, (mm_uint32_t)sz, remote);
	// we not need check why udp send error.just gbump the buffer.
	mm_streambuf_gbump(&udp->buff_send, sz);
	return rt;
}

// thread safe encode and append message.
+(int)q_tcp_flush_message_append :(struct mm_gl_tcp*) gl_tcp
                              uid:(mm_uint64_t) uid
                              mid:(mm_uint32_t) mid
                           rq_msg:(GPBMessage*) rq_msg
                          rq_pack:(struct mm_packet*) rq_pack
{
    int rt = -1;
    mm_gl_tcp_flush_lock(gl_tcp);
    rt = [mm_protobuf q_tcp_append_rq:&gl_tcp->net_tcp uid:uid mid:mid rq_msg:rq_msg rq_pack:rq_pack];
    mm_gl_tcp_flush_unlock(gl_tcp);
    return rt;
}
+(int)q_udp_flush_message_append :(struct mm_gl_udp*) gl_udp
                              uid:(mm_uint64_t) uid
                              mid:(mm_uint32_t) mid
                           rq_msg:(GPBMessage*) rq_msg
                          rq_pack:(struct mm_packet*) rq_pack
{
    int rt = -1;
    mm_gl_udp_flush_lock(gl_udp);
    rt = [mm_protobuf q_udp_append_rq:&gl_udp->net_udp uid:uid mid:mid rq_msg:rq_msg rq_pack:rq_pack];
    mm_gl_udp_flush_unlock(gl_udp);
    return rt;
}
// signal queue tcp flush.
+(int)q_tcp_flush_signal :(struct mm_gl_tcp*) gl_tcp
{
    mm_gl_tcp_flush_signal(gl_tcp);
    return 0;
}
// signal queue udp flush.
+(int)q_udp_flush_signal :(struct mm_gl_udp*) gl_udp
{
    mm_gl_udp_flush_signal(gl_udp);
    return 0;
}
@end
