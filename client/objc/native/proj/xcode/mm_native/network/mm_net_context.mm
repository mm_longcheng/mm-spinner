////
////  ViewController.m
////  mm_native
////
////  Created by longer on 2017/6/28.
////  Copyright © 2017年 longer. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//#import "mm_net_lobby.h"
//
//#import "net/mm_gl_tcp.h"
//#import "net/mm_tcp.h"
//#import "core/mm_logger_manager.h"
//#import "core/mm_context.h"
//#import "core/mm_time_cache.h"
//#import "core/mm_socket.h"
//#import "sys/socket.h"
//
//#import "mm_hd_n_gl_tcp_callback.h"
//#import "mm_hd_q_gl_tcp_callback.h"
//#import "mm_hd_gl_tcp_crypto_callback.h"
//#import "mm_hd_n_tcp_default.h"
//#import "mm_hd_q_tcp_default.h"
//#import "mm_hd_n_s_control_get_logger_rs.h"
//#import "mm_hd_q_s_control_get_logger_rs.h"
//
//#import "protodef/SControl.pbobjc.h"
//
//#import "cobweb/mm_protobuf.h"
//
//@interface mm_net_lobby ()
//
//@end
//
//@implementation mm_net_lobby
//
//-( id ) init
//{
//    self = [super init];
//    if(nil != self)
//    {
//        struct mm_logger* g_logger = mm_logger_instance();
//        struct mm_context* g_context = mm_context_instance();
//        struct mm_time_cache* g_time_cache = mm_time_cache_instance();
//        
//        mm_context_init(g_context);
//        mm_time_cache_init(g_time_cache);
//        //
//        mm_logger_log_I(g_logger,"%s %d.",__FUNCTION__,__LINE__);
//        // logger manager not fopen file and open console.
//        struct mm_logger_manager* g_logger_manager = mm_logger_manager_instance();
//        // mm_string_assigns(&g_logger_manager->file_name,"mm_view_net");
//        // mm_string_assigns(&g_logger_manager->logger_path,p->arg.argv[1]);
//        g_logger_manager->file_size = 300 * 1024 * 1024;
//        g_logger_manager->logger_level = MM_LOG_INFO;
//        g_logger_manager->check_file_size_time = 5000;
//        g_logger_manager->rolling_index = 100;
//        g_logger_manager->is_console = 1;
//        // mm_logger_manager_open(g_logger_manager);
//        //
//        mm_gl_tcp_init(&self->_gl_tcp);
//        mm_gl_tcp_assign_state_check_flag(&self->_gl_tcp,gl_tcp_check_activate);
//        
//        mm_gl_udp_init(&self->_gl_udp);
//        mm_gl_udp_assign_state_check_flag(&self->_gl_udp,gl_udp_check_activate);
//        
//        // default handle.
//        struct mm_gl_tcp_callback gl_tcp_callback;
//        gl_tcp_callback.handle = &mm_gl_tcp_default_handle_callback;
//        gl_tcp_callback.obj = (__bridge void*)(self);
//        
//        struct mm_mq_tcp_callback mq_tcp_callback;
//        mq_tcp_callback.handle = &mm_mq_tcp_default_handle_callback;
//        mq_tcp_callback.obj = (__bridge void*)(self);
//        mm_gl_tcp_assign_n_default_callback(&self->_gl_tcp,&gl_tcp_callback);
//        mm_gl_tcp_assign_q_default_callback(&self->_gl_tcp,&mq_tcp_callback);
//        // crypto handle.
//        struct mm_crypto_callback crypto_callback;
//        crypto_callback.decrypt = &hd_gl_tcp_crypto_decrypt_callback;
//        crypto_callback.encrypt = &hd_gl_tcp_crypto_encrypt_callback;
//        crypto_callback.obj = (__bridge void*)(self);
//        mm_gl_tcp_assign_crypto_callback(&self->_gl_tcp,&crypto_callback);
//        // net thread event handle.
//        mm_gl_tcp_assign_n_callback(&self->_gl_tcp,tcp_mid_broken,&mm_hd_n_tcp_broken_default);
//        mm_gl_tcp_assign_n_callback(&self->_gl_tcp,tcp_mid_nready,&mm_hd_n_tcp_nready_default);
//        mm_gl_tcp_assign_n_callback(&self->_gl_tcp,tcp_mid_finish,&mm_hd_n_tcp_finish_default);
//        // queue thread event handle.
//        mm_gl_tcp_assign_q_callback(&self->_gl_tcp,tcp_mid_broken,&mm_hd_q_tcp_broken_default);
//        mm_gl_tcp_assign_q_callback(&self->_gl_tcp,tcp_mid_nready,&mm_hd_q_tcp_nready_default);
//        mm_gl_tcp_assign_q_callback(&self->_gl_tcp,tcp_mid_finish,&mm_hd_q_tcp_finish_default);
//        //
//        // logic net thread event handle.
//        mm_gl_tcp_assign_n_callback(&self->_gl_tcp,S_control_get_logger_rs_msg_Id,&mm_hd_n_s_control_get_logger_rs_callback);
//        // logic queue thread event handle.
//        mm_gl_tcp_assign_q_callback(&self->_gl_tcp,S_control_get_logger_rs_msg_Id,&mm_hd_q_s_control_get_logger_rs_callback);
//        //
//        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0/20.0 target:self selector:@selector(thread_handle:) userInfo:nil repeats:YES];
//        
//        // assign remote target default.
//        mm_gl_tcp_assign_remote_target( &self->_gl_tcp, "101.200.169.28", 55000 );
//    }
//    return self;
//}
//
//- (void)thread_handle:(NSTimer *)timer
//{
//    mm_gl_tcp_thread_handle(&self->_gl_tcp);
//    mm_gl_udp_thread_handle(&self->_gl_udp);
//}
//
//-( void ) destroy
//{
//    struct mm_logger* g_logger = mm_logger_instance();
//    struct mm_context* g_context = mm_context_instance();
//    struct mm_time_cache* g_time_cache = mm_time_cache_instance();
//    mm_logger_log_I(g_logger,"%s %d.",__FUNCTION__,__LINE__);
//    //
//    mm_gl_tcp_destroy(&self->_gl_tcp);
//    mm_gl_udp_destroy(&self->_gl_udp);
//    //
//    mm_time_cache_destroy(g_time_cache);
//    mm_context_destroy(g_context);
//}
//
//// start wait thread.
//-( void ) start
//{
//    struct mm_logger* g_logger = mm_logger_instance();
//    struct mm_context* g_context = mm_context_instance();
//    struct mm_time_cache* g_time_cache = mm_time_cache_instance();
//    //
//    mm_context_start(g_context);
//    mm_time_cache_start(g_time_cache);
//    //
//    mm_gl_tcp_start(&self->_gl_tcp);
//    mm_gl_udp_start(&self->_gl_udp);
//    //
//    mm_logger_log_I(g_logger,"%s %d.",__FUNCTION__,__LINE__);
//}
//// interrupt wait thread.
//-( void ) interrupt
//{
//    struct mm_logger* g_logger = mm_logger_instance();
//    struct mm_context* g_context = mm_context_instance();
//    struct mm_time_cache* g_time_cache = mm_time_cache_instance();
//    //
//    mm_gl_tcp_interrupt(&self->_gl_tcp);
//    mm_gl_udp_interrupt(&self->_gl_udp);
//    //
//    mm_time_cache_interrupt(g_time_cache);
//    mm_context_interrupt(g_context);
//    mm_logger_log_I(g_logger,"%s %d.",__FUNCTION__,__LINE__);
//}
//// shutdown wait thread.
//-( void ) shutdown
//{
//    struct mm_logger* g_logger = mm_logger_instance();
//    struct mm_context* g_context = mm_context_instance();
//    struct mm_time_cache* g_time_cache = mm_time_cache_instance();
//    //
//    mm_gl_tcp_shutdown(&self->_gl_tcp);
//    mm_gl_udp_shutdown(&self->_gl_udp);
//    //
//    mm_time_cache_shutdown(g_time_cache);
//    mm_context_shutdown(g_context);
//    mm_logger_log_I(g_logger,"%s %d.",__FUNCTION__,__LINE__);
//}
//// join wait thread.
//-( void ) join
//{
//    struct mm_logger* g_logger = mm_logger_instance();
//    struct mm_context* g_context = mm_context_instance();
//    struct mm_time_cache* g_time_cache = mm_time_cache_instance();
//    //
//    mm_gl_tcp_join(&self->_gl_tcp);
//    mm_gl_udp_join(&self->_gl_udp);
//    //
//    mm_time_cache_join(g_time_cache);
//    mm_context_join(g_context);
//    mm_logger_log_I(g_logger,"%s %d.",__FUNCTION__,__LINE__);
//}
//
//@end
