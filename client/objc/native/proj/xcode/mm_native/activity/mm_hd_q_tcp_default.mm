//
//  ViewController.m
//  mm_native
//
//  Created by longer on 2017/6/28.
//  Copyright © 2017年 longer. All rights reserved.
//

#import "mm_hd_q_tcp_default.h"
#import "core/mm_logger.h"
#import "net/mm_tcp.h"
#import "net/mm_packet.h"

#import "protodef/SControl.pbobjc.h"
#import "cobweb/mm_protobuf.h"

MM_EXPORT_DLL void mm_hd_q_tcp_broken_default(void* obj, void* u, struct mm_packet* pack)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_W(g_logger,"%s %d q tcp:%s is broken.",__FUNCTION__,__LINE__,link_name);
}
MM_EXPORT_DLL void mm_hd_q_tcp_nready_default(void* obj, void* u, struct mm_packet* pack)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_W(g_logger,"%s %d q tcp:%s is nready.",__FUNCTION__,__LINE__,link_name);
}
MM_EXPORT_DLL void mm_hd_q_tcp_finish_default(void* obj, void* u, struct mm_packet* pack)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_I(g_logger,"%s %d q tcp:%s is finish.",__FUNCTION__,__LINE__,link_name);
    //
    struct mm_gl_tcp* gl_tcp = (struct mm_gl_tcp*)(tcp->callback.obj);
    //
    mm_uint64_t uid = 9008000000009999;
     NSMutableString* value = [[NSMutableString alloc]init];
    S_control_get_logger_rq* rq = [S_control_get_logger_rq alloc];
    //
    // assign rq.
    //
    struct mm_packet rq_pack;
    mm_packet_init(&rq_pack);
    for (int j = 0; j < 10; j++)
    {
        [mm_protobuf q_flush_message_append:gl_tcp uid:uid mid:S_control_get_logger_rq_msg_Id rq_msg:rq rq_pack:&rq_pack];
        [mm_protobuf q_tcp_flush_signal:gl_tcp];
        
        [value setString:@""];
        [mm_protobuf logger_append_packet_message:value pack:&rq_pack message:rq];
        mm_logger_log_I(g_logger,"qt rq %s",value.UTF8String);
    }
    mm_packet_destroy(&rq_pack);
}
