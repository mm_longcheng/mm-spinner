//
//  ViewController.m
//  mm_native
//
//  Created by longer on 2017/6/28.
//  Copyright © 2017年 longer. All rights reserved.
//

#import "mm_default_handle.h"
#import "core/mm_logger.h"
#import "net/mm_tcp.h"
#import "net/mm_udp.h"
#import "net/mm_packet.h"


//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_udp_handle_default(void* obj, void* u, struct mm_packet* pack, struct sockaddr_storage* remote)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_udp* udp = (struct mm_udp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_sockaddr_storage_native_remote_string( remote, &udp->addr.ss_native, udp->addr.socket, link_name );
    mm_logger_log_W(g_logger,"%s %d mid:0x%08X udp:%s not handle.",__FUNCTION__,__LINE__,pack->phead.mid,link_name);
}
MM_EXPORT_DLL void mm_net_udp_broken_default(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_udp* udp = (struct mm_udp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&udp->addr,link_name);
    mm_logger_log_W(g_logger,"%s %d udp:%s is broken.",__FUNCTION__,__LINE__,link_name);
}
MM_EXPORT_DLL void mm_net_udp_nready_default(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_udp* udp = (struct mm_udp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&udp->addr,link_name);
    mm_logger_log_W(g_logger,"%s %d udp:%s is nready.",__FUNCTION__,__LINE__,link_name);
}
MM_EXPORT_DLL void mm_net_udp_finish_default(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_udp* udp = (struct mm_udp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&udp->addr,link_name);
    mm_logger_log_I(g_logger,"%s %d udp:%s is finish.",__FUNCTION__,__LINE__,link_name);
}
MM_EXPORT_DLL void mm_net_tcp_handle_default(void* obj, void* u, struct mm_packet* pack)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_W(g_logger,"%s %d mid:0x%08X tcp:%s not handle.",__FUNCTION__,__LINE__,pack->phead.mid,link_name);
}
MM_EXPORT_DLL void mm_net_tcp_broken_default(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_W(g_logger,"%s %d tcp:%s is broken.",__FUNCTION__,__LINE__,link_name);
}
MM_EXPORT_DLL void mm_net_tcp_nready_default(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_W(g_logger,"%s %d tcp:%s is nready.",__FUNCTION__,__LINE__,link_name);
}
MM_EXPORT_DLL void mm_net_tcp_finish_default(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_I(g_logger,"%s %d tcp:%s is finish.",__FUNCTION__,__LINE__,link_name);
}

MM_EXPORT_DLL void mm_mailbox_handle_default(void* obj, void* u, struct mm_packet* pack)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_W(g_logger,"%s %d mid:0x%08X tcp:%s not handle.",__FUNCTION__,__LINE__,pack->phead.mid,link_name);
}
MM_EXPORT_DLL void mm_mailbox_broken_default(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_W(g_logger,"%s %d tcp:%s is broken.",__FUNCTION__,__LINE__,link_name);
}
MM_EXPORT_DLL void mm_mailbox_nready_default(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_W(g_logger,"%s %d tcp:%s is nready.",__FUNCTION__,__LINE__,link_name);
}
MM_EXPORT_DLL void mm_mailbox_finish_default(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_I(g_logger,"%s %d tcp:%s is finish.",__FUNCTION__,__LINE__,link_name);
}
MM_EXPORT_DLL void mm_contact_handle_default(void* obj, void* u, struct mm_packet* pack)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_W(g_logger,"%s %d mid:0x%08X tcp:%s not handle.",__FUNCTION__,__LINE__,pack->phead.mid,link_name);
}
MM_EXPORT_DLL void mm_contact_broken_default(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_W(g_logger,"%s %d tcp:%s is broken.",__FUNCTION__,__LINE__,link_name);
}
MM_EXPORT_DLL void mm_contact_nready_default(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_W(g_logger,"%s %d tcp:%s is nready.",__FUNCTION__,__LINE__,link_name);
}
MM_EXPORT_DLL void mm_contact_finish_default(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_I(g_logger,"%s %d tcp:%s is finish.",__FUNCTION__,__LINE__,link_name);
}

//////////////////////////////////////////////////////////////////////////
