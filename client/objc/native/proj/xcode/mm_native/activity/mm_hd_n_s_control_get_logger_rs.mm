//
//  ViewController.m
//  mm_native
//
//  Created by longer on 2017/6/28.
//  Copyright © 2017年 longer. All rights reserved.
//

#import "mm_hd_n_s_control_get_logger_rs.h"

#import "protodef/SControl.pbobjc.h"

#import "cobweb/mm_protobuf.h"
#import "core/mm_logger.h"

MM_EXPORT_DLL void mm_hd_n_s_control_get_logger_rs_callback(void* obj, void* u, struct mm_packet* pack)
{
    struct mm_logger* g_logger = mm_logger_instance();
    NSMutableString* value = [[NSMutableString alloc] init];
    S_control_get_logger_rs* rs = [[S_control_get_logger_rs alloc] init];
    if(0 == [mm_protobuf decode_message:pack message:rs])
    {
        [mm_protobuf logger_append_packet_message:value pack:pack message:rs];
        mm_logger_log_I(g_logger,"nt rs %s",value.UTF8String);
    }
    else
    {
        [mm_protobuf logger_append_packet:value pack:pack];
        mm_logger_log_I(g_logger,"nt rs %s",value.UTF8String);
    }
}
