//
//  AppDelegate.h
//  mm_native
//
//  Created by longer on 2017/6/28.
//  Copyright © 2017年 longer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mm_view_net.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) mm_view_net* view_net;

@end

