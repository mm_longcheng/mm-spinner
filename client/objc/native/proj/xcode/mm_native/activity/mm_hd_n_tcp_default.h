//
//  ViewController.h
//  mm_native
//
//  Created by longer on 2017/6/28.
//  Copyright © 2017年 longer. All rights reserved.
//
#include "core/mm_prefix.h"

#import "core/mm_core.h"

MM_EXPORT_DLL void mm_hd_n_tcp_broken_default(void* obj, void* u, struct mm_packet* pack);
MM_EXPORT_DLL void mm_hd_n_tcp_nready_default(void* obj, void* u, struct mm_packet* pack);
MM_EXPORT_DLL void mm_hd_n_tcp_finish_default(void* obj, void* u, struct mm_packet* pack);

#include "core/mm_suffix.h"
