//
//  ViewController.m
//  mm_native
//
//  Created by longer on 2017/6/28.
//  Copyright © 2017年 longer. All rights reserved.
//

#import "mm_hd_n_gl_tcp_callback.h"
#import "core/mm_logger.h"
#import "net/mm_tcp.h"
#import "net/mm_packet.h"


MM_EXPORT_DLL void mm_gl_tcp_default_handle_callback(void* obj, void* u, struct mm_packet* pack)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mm_tcp* tcp = (struct mm_tcp*)(obj);
    struct mm_logger* g_logger = mm_logger_instance();
    mm_addr_string(&tcp->addr,link_name);
    mm_logger_log_W(g_logger,"%s %d n mid:%d tcp:%s not handle.",__FUNCTION__,__LINE__,pack->phead.mid,link_name);
}
