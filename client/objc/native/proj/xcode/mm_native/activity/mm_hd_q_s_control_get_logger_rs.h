//
//  ViewController.h
//  mm_native
//
//  Created by longer on 2017/6/28.
//  Copyright © 2017年 longer. All rights reserved.
//
#include "core/mm_prefix.h"

#import "core/mm_core.h"

MM_EXPORT_DLL void mm_hd_q_s_control_get_logger_rs_callback(void* obj, void* u, struct mm_packet* pack);

#include "core/mm_suffix.h"

