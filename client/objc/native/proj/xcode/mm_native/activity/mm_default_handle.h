//
//  ViewController.h
//  mm_native
//
//  Created by longer on 2017/6/28.
//  Copyright © 2017年 longer. All rights reserved.
//
#include "core/mm_prefix.h"

#import "core/mm_core.h"

//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL void mm_net_udp_handle_default(void* obj, void* u, struct mm_packet* pack, struct sockaddr_storage* remote);
MM_EXPORT_DLL void mm_net_udp_broken_default(void* obj);
MM_EXPORT_DLL void mm_net_udp_nready_default(void* obj);
MM_EXPORT_DLL void mm_net_udp_finish_default(void* obj);

MM_EXPORT_DLL void mm_net_tcp_handle_default(void* obj, void* u, struct mm_packet* pack);
MM_EXPORT_DLL void mm_net_tcp_broken_default(void* obj);
MM_EXPORT_DLL void mm_net_tcp_nready_default(void* obj);
MM_EXPORT_DLL void mm_net_tcp_finish_default(void* obj);

MM_EXPORT_DLL void mm_mailbox_handle_default(void* obj, void* u, struct mm_packet* pack);
MM_EXPORT_DLL void mm_mailbox_broken_default(void* obj);
MM_EXPORT_DLL void mm_mailbox_nready_default(void* obj);
MM_EXPORT_DLL void mm_mailbox_finish_default(void* obj);

MM_EXPORT_DLL void mm_contact_handle_default(void* obj, void* u, struct mm_packet* pack);
MM_EXPORT_DLL void mm_contact_broken_default(void* obj);
MM_EXPORT_DLL void mm_contact_nready_default(void* obj);
MM_EXPORT_DLL void mm_contact_finish_default(void* obj);
//////////////////////////////////////////////////////////////////////////

#include "core/mm_suffix.h"
