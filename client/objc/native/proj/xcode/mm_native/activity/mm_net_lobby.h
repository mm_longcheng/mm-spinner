//
//  ViewController.h
//  mm_native
//
//  Created by longer on 2017/6/28.
//  Copyright © 2017年 longer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "net/mm_gl_tcp.h"
#import "net/mm_gl_udp.h"

@interface mm_net_lobby : NSObject

@property (nonatomic, assign) struct mm_gl_tcp gl_tcp;
@property (nonatomic, assign) struct mm_gl_udp gl_udp;

@property (strong, nonatomic) NSTimer* timer;

-( id ) init;
-( void ) destroy;

// start wait thread.
-( void ) start;
// interrupt wait thread.
-( void ) interrupt;
// shutdown wait thread.
-( void ) shutdown;
// join wait thread.
-( void ) join;

@end

