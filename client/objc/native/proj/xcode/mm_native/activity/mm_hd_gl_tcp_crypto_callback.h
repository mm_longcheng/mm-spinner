//
//  ViewController.h
//  mm_native
//
//  Created by longer on 2017/6/28.
//  Copyright © 2017年 longer. All rights reserved.
//
#include "core/mm_prefix.h"

#import "core/mm_core.h"

MM_EXPORT_DLL void hd_gl_tcp_crypto_encrypt_callback(void* p,void* e,mm_uint8_t* buffer_i,mm_uint32_t offset_i,mm_uint8_t* buffer_o,mm_uint32_t offset_o,mm_uint32_t length);
MM_EXPORT_DLL void hd_gl_tcp_crypto_decrypt_callback(void* p,void* e,mm_uint8_t* buffer_i,mm_uint32_t offset_i,mm_uint8_t* buffer_o,mm_uint32_t offset_o,mm_uint32_t length);

#include "core/mm_suffix.h"
