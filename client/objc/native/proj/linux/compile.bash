#!/bin/sh
make -f makefile_core_shared;
make -f makefile_core_static;

make -f makefile_net_shared;
make -f makefile_net_static;

make -f makefile_calendar_shared;
make -f makefile_calendar_static;
