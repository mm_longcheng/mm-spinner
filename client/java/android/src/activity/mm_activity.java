package activity;

import java.util.Timer;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import mm.core.mm_logger;
import mm.net.mm_gl_tcp;
import mm.net.mm_tcp;
import protodef.nano.s_control;

import mm.security.mm_chacha20;
import mm.security.mm_rc4;

public class mm_activity extends Activity
{
	private static final String TAG = mm_activity.class.getSimpleName();
	
	// timer task default nearby time milliseconds.we define it 1/20 = 50ms.
	public static final int MM_MAIN_THREAD_NEARBY_MSEC = 50;
	// timer task default nearby time milliseconds.
	public static final int MM_MAIN_THREAD_DELAY_MSEC = 0;
	
	public mm_gl_tcp gl_tcp = new mm_gl_tcp();
	private mm_android_handler_task handler_task = new mm_android_handler_task();
	// main thread timer.
	public Timer handler_timer = new Timer();

	public void rc4()
	{
		byte[] key = {1,2,3,5,9,0,7,8,1,2,3,5,9,0,7,8,1,2,3,5,9,0,7,8,1,2,3,5,9,0,7,8,}; // must 32.
        byte[] plain0 = "testing".getBytes();
		
		byte[] plain1 = new byte[plain0.length];
		byte[] plain2 = new byte[plain0.length];
		
		mm_rc4 cipher0 = new mm_rc4();
		mm_rc4 cipher1 = new mm_rc4();
		cipher0.set_key(key,0 ,key.length);
		cipher1.set_key(key,0 ,key.length);
		
		cipher0.encrypt(plain1, 0, plain0, 0, plain0.length);
		cipher1.decrypt(plain2, 0, plain1, 0, plain1.length);
		
		mm_logger.logI(TAG + " plain0:" + plain0.toString());
		mm_logger.logI(TAG + " plain1:" + plain1.toString());
		mm_logger.logI(TAG + " plain2:" + plain2.toString());
	}
	public void chacha20_coder()
	{
		byte[] key = {1,2,3,5,9,0,7,8,1,2,3,5,9,0,7,8,1,2,3,5,9,0,7,8,1,2,3,5,9,0,7,8,}; // must 32.
		byte[] nonce = {0,7,1,8,0,7,1,8,};// must 8.
        byte[] plain0 = "testing".getBytes();
		
		byte[] plain1 = new byte[plain0.length];
		byte[] plain2 = new byte[plain0.length];
		
		mm_chacha20 cipher0 = new mm_chacha20();
		mm_chacha20 cipher1 = new mm_chacha20();
		cipher0.keysetup(key);
		cipher0.ivsetup(nonce);
		cipher1.keysetup(key);
		cipher1.ivsetup(nonce);
		
		cipher0.encrypt(plain1, plain0, plain0.length);
		cipher1.decrypt(plain2, plain1, plain1.length);
		
		mm_logger.logI(TAG + " plain0:" + plain0.toString());
		mm_logger.logI(TAG + " plain1:" + plain1.toString());
		mm_logger.logI(TAG + " plain2:" + plain2.toString());
	}
	@Override
	public void onCreate(final Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		Log.d(TAG, "create succeed.");
		
		this.chacha20_coder();
		// init logger.
		mm_logger.assign_callback(new mm_logger_android_callback());
		// init
		this.gl_tcp.init();
		this.handler_task.init();
		// assign
		mm_logger.logI(TAG + " initialize begin.");
		// assign attributes.
		this.gl_tcp.assign_state_check_flag(mm_gl_tcp.gl_tcp_check_activate);
		// this.gl_tcp.assign_remote_target("::1", 55000);
		// this.gl_tcp.assign_remote_target("240c:f:1:6000::2697", 55000);
		// this.gl_tcp.assign_remote_target("2001:470:c:48e::2", 55000);		
		this.gl_tcp.assign_remote_target("101.200.169.28", 55000);
		// this.gl_tcp.assign_remote_target("fe80::216:3eff:fe00:3c6", 55000);
		// this.gl_tcp.assign_remote_target("::1", 55000);
		this.gl_tcp.assign_context(this);
		
		// default handle.
		this.gl_tcp.assign_n_default_callback(new mm_hd_n_gl_tcp_callback());
		this.gl_tcp.assign_q_default_callback(new mm_hd_q_gl_tcp_callback());
		// crypto handle.
		this.gl_tcp.assign_crypto_callback(new hd_gl_tcp_crypto_callback());
		// net thread event handle.
		this.gl_tcp.assign_n_callback(mm_tcp.tcp_mid_broken,new mm_hd_n_tcp_default.hd_n_tcp_broken());
		this.gl_tcp.assign_n_callback(mm_tcp.tcp_mid_nready,new mm_hd_n_tcp_default.hd_n_tcp_nready());
		this.gl_tcp.assign_n_callback(mm_tcp.tcp_mid_finish,new mm_hd_n_tcp_default.hd_n_tcp_finish());
		// queue thread event handle.
		this.gl_tcp.assign_q_callback(mm_tcp.tcp_mid_broken,new mm_hd_q_tcp_default.hd_q_tcp_broken());
		this.gl_tcp.assign_q_callback(mm_tcp.tcp_mid_nready,new mm_hd_q_tcp_default.hd_q_tcp_nready());
		this.gl_tcp.assign_q_callback(mm_tcp.tcp_mid_finish,new mm_hd_q_tcp_default.hd_q_tcp_finish());
		
		// logic net thread event handle.
		this.gl_tcp.assign_n_callback(s_control.get_logger_rs.id, new mm_hd_n_s_control_get_logger_rs());
		// logic queue thread event handle.
		this.gl_tcp.assign_q_callback(s_control.get_logger_rs.id, new mm_hd_q_s_control_get_logger_rs());
		
		mm_logger.logI(TAG + " initialize end.");
		//
		this.handler_task.assign_gl_tcp(this.gl_tcp);
		// start
		this.handler_timer.schedule(this.handler_task, MM_MAIN_THREAD_DELAY_MSEC, MM_MAIN_THREAD_NEARBY_MSEC);
		this.gl_tcp.start();
	}

	@Override
	protected void onPause() 
	{
		super.onPause();
	}

	@Override
	protected void onResume() 
	{
		super.onResume();
	}
	@Override
	protected void onStop()
	{
		super.onStop();
	}
	
	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		// shutdown.
		this.gl_tcp.shutdown();
		this.handler_timer.cancel();
		// join.
		this.gl_tcp.join();
		// destroy.
		this.gl_tcp.destroy();
		this.handler_task.destroy();
	}
}
