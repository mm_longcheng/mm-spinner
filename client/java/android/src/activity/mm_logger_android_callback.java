package activity;

import android.util.Log;
import mm.core.mm_logger;

public class mm_logger_android_callback extends mm_logger.mm_logger_callback_default
{
	@Override
	public void EventLogger(Object o, int lvl, String message) 
	{
		// android.util.Log is thread safe.
		// [ 8 V ]
		mm_logger.level_mark lm = mm_logger.level_mark_by_lvl(lvl);
		switch(lvl)
		{
		case mm_logger.MM_LOG_UNKNOW:
			Log.v("mm", " " + lvl + " " + lm.m + " "+ message);
			break;
		case mm_logger.MM_LOG_FATAL:
			Log.e("mm", " " + lvl + " " + lm.m + " "+ message);
			break;
		case mm_logger.MM_LOG_CRIT:
			Log.e("mm", " " + lvl + " " + lm.m + " "+ message);
			break;
		case mm_logger.MM_LOG_ERROR:
			Log.e("mm", " " + lvl + " " + lm.m + " "+ message);
			break;
		case mm_logger.MM_LOG_ALERT:
			Log.e("mm", " " + lvl + " " + lm.m + " "+ message);
			break;
		case mm_logger.MM_LOG_WARNING:
			Log.w("mm", " " + lvl + " " + lm.m + " "+ message);
			break;
		case mm_logger.MM_LOG_NOTICE:
			Log.i("mm", " " + lvl + " " + lm.m + " "+ message);
			break;
		case mm_logger.MM_LOG_INFO:
			Log.i("mm", " " + lvl + " " + lm.m + " "+ message);
			break;
		case mm_logger.MM_LOG_TRACE:
			Log.d("mm", " " + lvl + " " + lm.m + " "+ message);
			break;
		case mm_logger.MM_LOG_DEBUG:
			Log.d("mm", " " + lvl + " " + lm.m + " "+ message);
			break;
		case mm_logger.MM_LOG_VERBOSE:
			Log.v("mm", " " + lvl + " " + lm.m + " "+ message);
			break;
		default:
			Log.v("mm", " " + lvl + " " + lm.m + " "+ message);
			break;
		}
	}
}
