package activity;

import cobweb.mm_protobuf;
import mm.core.mm_logger;
import mm.net.mm_net_tcp;
import mm.net.mm_packet;
import protodef.nano.s_control;

class mm_hd_q_s_control_get_logger_rs implements mm_net_tcp.mm_net_tcp_handle_callback
{
	@Override
	public void handle(Object obj, Object u, mm_packet pack) 
	{
		s_control.get_logger_rs rs = new s_control.get_logger_rs();
		StringBuilder value = new StringBuilder();
		if(0 == mm_protobuf.decode_message(pack, rs))
		{
			mm_protobuf.logger_append_packet_message(value, pack, rs);
			mm_logger.logI("qt rs " + value.toString());
			// handle event.
		}
		else
		{
			mm_protobuf.logger_append_packet(value, pack);
			mm_logger.logW("qt rs " + value.toString());
		}
	}
}	
