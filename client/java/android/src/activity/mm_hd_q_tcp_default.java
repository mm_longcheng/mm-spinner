package activity;

import mm.core.mm_logger;
import mm.net.mm_net_tcp;
import mm.net.mm_packet;
import mm.net.mm_tcp;

public class mm_hd_q_tcp_default
{
	static class hd_q_tcp_broken implements mm_net_tcp.mm_net_tcp_handle_callback
	{
		@Override
		public void handle(Object obj, Object u, mm_packet pack) 
		{
			mm_tcp tcp = (mm_tcp)obj;
			mm_logger.logW("q tcp:" + tcp.addr.to_string() +" is broken.");
		}
	}
	static class hd_q_tcp_nready implements mm_net_tcp.mm_net_tcp_handle_callback
	{
		@Override
		public void handle(Object obj, Object u, mm_packet pack) 
		{
			mm_tcp tcp = (mm_tcp)obj;
			mm_logger.logW("q tcp:" + tcp.addr.to_string() +" is nready.");
		}
	}
	static class hd_q_tcp_finish implements mm_net_tcp.mm_net_tcp_handle_callback
	{
		@Override
		public void handle(Object obj, Object u, mm_packet pack) 
		{
			mm_tcp tcp = (mm_tcp)obj;
			mm_logger.logI("q tcp:" + tcp.addr.to_string() +" is finish.");
		}
	}
}
