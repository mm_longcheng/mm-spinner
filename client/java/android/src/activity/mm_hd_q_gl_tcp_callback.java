package activity;

import cobweb.mm_protobuf;
import mm.core.mm_logger;
import mm.net.mm_mq_tcp;
import mm.net.mm_packet;

class mm_hd_q_gl_tcp_callback extends mm_mq_tcp.mm_mq_tcp_callback
{
	@Override
	public void handle( Object obj, Object u, mm_packet pack )
	{
		String prefix = "qt rs ";
		StringBuilder value = new StringBuilder();
		mm_protobuf.logger_append_packet(value, pack);
		mm_logger.logW(prefix + value.toString() + " not handle.");
	}
}
