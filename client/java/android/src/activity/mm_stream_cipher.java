package activity;

import mm.security.mm_chacha20;

public class mm_stream_cipher
{
	public mm_chacha20 cipher_send = new mm_chacha20();
	public mm_chacha20 cipher_recv = new mm_chacha20();
	
    public void init()
    {
    	this.cipher_send.init();
    	this.cipher_recv.init();
    }
    
    public void destroy()
    {
    	this.cipher_send.destroy();
    	this.cipher_recv.destroy();
    }
}
