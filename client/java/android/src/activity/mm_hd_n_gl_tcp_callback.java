package activity;

import cobweb.mm_protobuf;
import mm.core.mm_logger;
import mm.net.mm_gl_tcp;
import mm.net.mm_packet;

class mm_hd_n_gl_tcp_callback extends mm_gl_tcp.mm_gl_tcp_callback
{
	@Override
	public void handle( Object obj, Object u, mm_packet pack )
	{
		String prefix = "nt rs ";
		StringBuilder value = new StringBuilder();
		mm_protobuf.logger_append_packet(value, pack);
		mm_logger.logW(prefix + value.toString() + " not handle.");
	}
}
