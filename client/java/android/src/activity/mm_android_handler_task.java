package activity;

import java.util.TimerTask;

import mm.net.mm_gl_tcp;

public class mm_android_handler_task extends TimerTask
{
	public mm_gl_tcp gl_tcp = null;// weak ref.
	
	public void init()
	{

	}
	public void destroy()
	{

	}
	public void assign_gl_tcp(mm_gl_tcp gl_tcp)
	{
		this.gl_tcp = gl_tcp;
	}
	@Override
	public void run() 
	{
		if(null != this.gl_tcp)
		{
			this.gl_tcp.thread_handle();
		}
	}
}
