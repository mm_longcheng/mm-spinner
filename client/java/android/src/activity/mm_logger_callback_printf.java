package activity;

import java.text.SimpleDateFormat;
import java.util.Date;

import mm.core.mm_logger;

public class mm_logger_callback_printf extends mm_logger.mm_logger_callback_default
{
	public SimpleDateFormat data_format = new SimpleDateFormat ("yyyy/MM/dd hh:mm:ss SSS");
	@Override
	public void EventLogger(Object o, int lvl, String message) 
	{
		synchronized (o)
		{
			// [1992/01/26 09:13:14-520 8 V ]
		    Date time_now = new Date();
			// [ 8 V ]
			mm_logger.level_mark lm = mm_logger.level_mark_by_lvl(lvl);
			System.out.println(data_format.format(time_now) + " " + lvl + " " + lm.m + " "+ message);	
		}
	}
}
