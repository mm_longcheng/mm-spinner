package mm.net;

import mm.core.mm_byte;
import mm.core.mm_type;

public class mm_packet
{
	// Ethernet len[46~1500] MTU 20 
	// udp head  8 1500 - 20 -  8 = 1472
	// tcp head 20 1500 - 20 - 20 = 1460
	// we define packet page size to 1024.
	public static final int MM_PACKET_PAGE_SIZE = 1024;

	// we not use typedef a new name for this type.
	// because uint16 uint32 is common type when cross language.

	// msg_size    type is mm_uint16_t
	// msg_id      type is mm_uint32_t
	// proxy_id    type is mm_uint32_t
	// session_id  type is mm_uint64_t
	// unique_id   type is mm_uint64_t
	// msg_head    type is mm_uint32_t = 2 * msg_size

	// msg pack struct.
	// |-mm_uint16_t-|-mm_uint16_t-|---char*---|---char*---|

	// use this quick decode encode.
	// |--------mm_uint32_t--------|---char*---|---char*---|

	// |-------------|-------------------------|-----------|
	// |--msg_size---|--head_size--|-head_data-|-body_data-|
	// |---------head_data---------|-head_data-|-body_data-|

	// package_size = 2 * sizeof(mm_msg_size_t) + head_size + body_size

	public static final int MM_MSG_SIZE_BIT_NUM = 16;
	public static final int MM_MSG_HEAD_H_MASK = 0xFFFF0000;// hight mask
	public static final int MM_MSG_HEAD_L_MASK = 0x0000FFFF;// low   mask

	// the hsize must use sizeof(T),make sure the package byte aligned.

	// msg base head size.
	// mm_uint32_t   mid;// msg     id.
	// byte aligned this struct sizeof is 4.
	public static final int MM_MSG_BASE_HEAD_SIZE = 4;
	// mm_uint32_t   mid;// msg     id.
	// mm_uint32_t   pid;// proxy   id.
	// mm_uint64_t   sid;// section id.
	// mm_uint64_t   uid;// unique  id.
	// byte aligned this struct sizeof is 24.
	public static final int MM_MSG_COMM_HEAD_SIZE = 24;

	public static class mm_packet_head
	{
		// msg base head.include into head data.
		public int    mid = 0;// msg     id.
		public int    pid = 0;// proxy   id.
		public long   sid = 0;// section id.
		public long   uid = 0;// unique  id.
		public void init()
		{
			this.mid = 0;
			this.pid = 0;
			this.sid = 0;
			this.uid = 0;
		}
		public void destroy()
		{
			this.mid = 0;
			this.pid = 0;
			this.sid = 0;
			this.uid = 0;
		}
	};
	
	public static class mm_packet_buff
	{
		public int    length = 0;// packet buff length.
		public int    offset = 0;// packet buff offset.
		public byte[] buffer = null;// packet buff buffer.
		public void init()
		{
			this.length = 0;
			this.offset = 0;
			this.buffer = null;
		}
		public void destroy()
		{
			this.length = 0;
			this.offset = 0;
			this.buffer = null;
		}
	};
	
	// packet struct.
	public mm_packet_head phead = new mm_packet_head();// packet head.
	public mm_packet_buff hbuff = new mm_packet_buff();// buffer for head.
	public mm_packet_buff bbuff = new mm_packet_buff();// buffer for body.
	//////////////////////////////////////////////////////////////////////////
	public void init()
	{
		this.phead.init();
		this.hbuff.init();
		this.bbuff.init();
	}
	//////////////////////////////////////////////////////////////////////////
	public void destroy()
	{
		this.phead.destroy();
		this.hbuff.destroy();
		this.bbuff.destroy();
	}
	// reset all to zero.
	public void reset()
	{
		this.phead.mid = 0;
		this.phead.pid = 0;
		this.phead.sid = 0;
		this.phead.uid = 0;
		
		this.hbuff.length = 0;
		this.hbuff.offset = 0;
		this.hbuff.buffer = null;
		
		this.bbuff.length = 0;
		this.bbuff.offset = 0;
		this.bbuff.buffer = null;
	}
	// use for quick zero head base.(mid,uid,sid,pid)
	public void head_base_zero()
	{
		this.phead.mid = 0;
		this.phead.pid = 0;
		this.phead.sid = 0;
		this.phead.uid = 0;
	}
	// use for quick copy head base r-->p.(mid,uid,sid,pid)
	public void head_base_copy( mm_packet r )
	{
		this.phead = r.phead;
	}
	
	// use for quick decode head base.depend on hsize (mid)/(mid,uid,sid,pid)/(other).
	// p : struct mm_packet*
	public void head_base_decode()
	{
		this.phead.mid = mm_byte.int32_decode_bytes(this.hbuff.buffer, this.hbuff.offset + 0 * mm_type.MM_INT32_SIZE);
		this.phead.pid = mm_byte.int32_decode_bytes(this.hbuff.buffer, this.hbuff.offset + 1 * mm_type.MM_INT32_SIZE);
		this.phead.sid = mm_byte.int64_decode_bytes(this.hbuff.buffer, this.hbuff.offset + 2 * mm_type.MM_INT32_SIZE);
		this.phead.uid = mm_byte.int64_decode_bytes(this.hbuff.buffer, this.hbuff.offset + 2 * mm_type.MM_INT64_SIZE);
	}
	// use for quick encode head base.depend on hsize (mid)/(mid,uid,sid,pid)/(other).
	// p : struct mm_packet*
	public void head_base_encode()
	{
		mm_byte.int32_encode_bytes(this.hbuff.buffer, this.hbuff.offset + 0 * mm_type.MM_INT32_SIZE, this.phead.mid);
		mm_byte.int32_encode_bytes(this.hbuff.buffer, this.hbuff.offset + 1 * mm_type.MM_INT32_SIZE, this.phead.pid);
		mm_byte.int64_encode_bytes(this.hbuff.buffer, this.hbuff.offset + 2 * mm_type.MM_INT32_SIZE, this.phead.sid);
		mm_byte.int64_encode_bytes(this.hbuff.buffer, this.hbuff.offset + 2 * mm_type.MM_INT64_SIZE, this.phead.uid);
	}
	//////////////////////////////////////////////////////////////////////////
	// copy data from p to t.will realloc the hbuff and bbuff.
	public void copy_realloc( mm_packet t )
	{
		// android assertions are unreliable.
		// assert null != t.hbuff.buffer : "t.hbuff.buffer is null.";
		// assert null != t.bbuff.buffer : "t.bbuff.buffer is null.";		
		t.hbuff.buffer = new byte[this.hbuff.length];
		t.bbuff.buffer = new byte[this.bbuff.length];		
		this.copy_weak(t);
	}
	// copy data from p to t.will alloc the hbuff and bbuff.
	public void copy_alloc( mm_packet t )
	{
		// android assertions are unreliable.
		// assert null != t.hbuff.buffer : "t.hbuff.buffer is null.";
		// assert null != t.bbuff.buffer : "t.bbuff.buffer is null.";		
		t.hbuff.buffer = new byte[this.hbuff.length];
		t.bbuff.buffer = new byte[this.bbuff.length];		
		this.copy_weak(t);
	}
	// copy data from p to t.not realloc the hbuff and bbuff.
	public void copy_weak( mm_packet t )
	{
		t.hbuff.length = this.hbuff.length;
		t.bbuff.length = this.bbuff.length;
		System.arraycopy(this.hbuff.buffer,this.hbuff.offset,t.hbuff.buffer,t.hbuff.offset,this.hbuff.length);
		System.arraycopy(this.bbuff.buffer,this.bbuff.offset,t.bbuff.buffer,t.bbuff.offset,this.bbuff.length);
		t.head_base_decode();
	}
	// free copy realloc data from.
	public void free_copy_realloc()
	{
		this.hbuff.buffer = null;
		this.bbuff.buffer = null;
		this.reset();
	}
	// free copy alloc data from.
	public void free_copy_alloc()
	{
		this.hbuff.buffer = null;
		this.bbuff.buffer = null;
		this.reset();
	}
	//////////////////////////////////////////////////////////////////////////
	public void shadow_alloc()
	{
		// android assertions are unreliable.
		// assert null == this.hbuff.buffer : "this.hbuff.buffer is not null.";
		// assert null == this.bbuff.buffer : "this.bbuff.buffer is not null.";
		this.hbuff.buffer = new byte[this.hbuff.length];
		this.bbuff.buffer = new byte[this.bbuff.length];	
	}
	public void shadow_free()
	{
		this.hbuff.buffer = null;
		this.bbuff.buffer = null;
	}
	//////////////////////////////////////////////////////////////////////////
	// packet handle function type.
	public static interface tcp_callBack 
	{  
	    public void handle_tcp(Object obj, mm_packet pack);
	}
	// packet handle function type.
	public static interface udp_callBack 
	{  
	    public void handle_udp(Object obj, mm_packet pack, mm_sockaddr remote);
	} 
	//////////////////////////////////////////////////////////////////////////
};
