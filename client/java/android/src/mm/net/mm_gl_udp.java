package mm.net;

import mm.core.mm_signal_task;
import mm.core.mm_spinlock;
import mm.core.mm_thread;

public class mm_gl_udp
{
	public static class mm_gl_udp_callback
	{
		public void handle( Object obj, Object u, mm_packet pack, mm_sockaddr remote ){}
		public Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	};
	
	// gl udp connect handle check milliseconds.default is 5 second(5000 ms).
	public static final int MM_GL_UDP_HANDLE_MSEC_CHECK = 5000;
	// gl udp connect broken check milliseconds.default is 1 second(1000 ms).
	public static final int MM_GL_UDP_BROKEN_MSEC_CHECK = 1000;

	// send thread msleep interval handle time.
	public static final int MM_GL_UDP_SEND_INTERVAL_HANDLE_TIME = 0;
	// send thread msleep interval broken time.
	public static final int MM_GL_UDP_SEND_INTERVAL_BROKEN_TIME = 1000;

	public static final int gl_udp_check_inactive = 0;// do not check the connect state.
	public static final int gl_udp_check_activate = 1;// check the connect state.
	
	public static final int gl_udp_ptq_inactive = 0;// not push to queue.
	public static final int gl_udp_ptq_activate = 1;// push to queue.
	
	public mm_sockaddr ss_native = new mm_sockaddr();// native end point.target value not current real.
	public mm_sockaddr ss_remote = new mm_sockaddr();// remote end point.target value not current real.
	public mm_net_udp net_udp = new mm_net_udp();// real net udp.
	public mm_mq_udp queue_udp = new mm_mq_udp();// thread handle message queue.
	public mm_gl_udp_callback callback = new mm_gl_udp_callback();// value ref. udp callback.
	public mm_signal_task flush_task = new mm_signal_task();// flush task for flush send message buffer.
	public mm_signal_task state_task = new mm_signal_task();// state task for check state.
	public mm_spinlock locker = new mm_spinlock();
	public int state_check_flag = gl_udp_check_inactive;// default is gl_udp_check_inactive.
	public int push_to_queue_flag = gl_udp_ptq_activate;// default is gl_udp_ptq_activate.
	//
	public int state = mm_thread.ts_closed;// mm_thread_state_t,default is ts_closed(0)
	public Object u = null;// user data.
	
	public void init()
	{
		this.ss_native.init();
		this.ss_remote.init();
		this.net_udp.init();
		this.queue_udp.init();
		this.callback.init();
		this.flush_task.init();
		this.state_task.init();
		this.locker.init();
		this.state_check_flag = gl_udp_check_inactive;
		this.state = mm_thread.ts_closed;
		
		this.net_udp.udp.assign_callback(new __gl_udp_udp_callback(this));
		this.net_udp.assign_net_udp_callback(new __gl_udp_net_udp_callback(this));
		this.flush_task.assign_callback(new __gl_udp_flush_signal_task(this));
		this.state_task.assign_callback(new __gl_udp_state_signal_task(this));
		
		this.flush_task.assign_success_nearby(MM_GL_UDP_SEND_INTERVAL_HANDLE_TIME);
		this.flush_task.assign_failure_nearby(MM_GL_UDP_SEND_INTERVAL_BROKEN_TIME);
		this.state_task.assign_success_nearby(MM_GL_UDP_HANDLE_MSEC_CHECK);
		this.state_task.assign_failure_nearby(MM_GL_UDP_BROKEN_MSEC_CHECK);
	}
	public void destroy()
	{
		this.clear_callback_holder();
		//
		this.ss_native.destroy();
		this.ss_remote.destroy();
		this.net_udp.destroy();
		this.queue_udp.destroy();
		this.callback.destroy();
		this.flush_task.destroy();
		this.state_task.destroy();
		this.locker.destroy();
		this.state_check_flag = gl_udp_check_inactive;
		this.state = mm_thread.ts_closed;
	}
	//////////////////////////////////////////////////////////////////////////
	// assign native address but not connect.
	public void assign_native(String node,int port)
	{
		this.net_udp.assign_native(node, port);
	}
	// assign remote address but not connect.
	public void assign_remote(String node,int port)
	{
		this.net_udp.assign_remote(node, port);
	}
	// assign native address but not connect.target value not current real.will apply at state thread.
	public void assign_native_target(String node,int port)
	{
		this.ss_native.assign(node, port);
	}
	// assign remote address but not connect.target value not current real..will apply at state thread.
	public void assign_remote_target(String node,int port)
	{
		this.ss_remote.assign(node, port);
	}
	//////////////////////////////////////////////////////////////////////////
	public void assign_q_default_callback(mm_mq_udp.mm_mq_udp_callback mq_udp_callback)
	{
		this.queue_udp.assign_queue_udp_callback(mq_udp_callback);
	}
	public void assign_n_default_callback(mm_gl_udp.mm_gl_udp_callback gl_udp_callback)
	{
		// android assertions are unreliable.
		// assert null != gl_udp_callback : "you can not assign null gl_udp_callback.";
		this.callback = gl_udp_callback;
	}
	// assign queue callback.at main thread.
	public void assign_q_callback(int id,mm_net_udp.mm_net_udp_handle_callback callback)
	{
		this.queue_udp.assign_callback(id, callback);
	}
	// assign net callback.not at main thread.
	public void assign_n_callback(int id,mm_net_udp.mm_net_udp_handle_callback callback)
	{
		this.net_udp.assign_callback(id, callback);
	}
	// do not assign when holder have some elem.
	public void assign_event_udp_alloc( mm_net_udp.mm_net_udp_event_udp_alloc event_udp_alloc)
	{
		this.net_udp.assign_event_udp_alloc(event_udp_alloc);
	}
	// do not assign when holder have some elem.
	public void assign_crypto_callback(mm_crypto.mm_crypto_callback crypto_callback)
	{
		this.net_udp.assign_crypto_callback(crypto_callback);
	}
	// assign context handle.
	public void assign_context( Object u )
	{
		this.u = u;
		this.queue_udp.assign_context(u);
		this.net_udp.assign_context(u);
	}
	// context for udp will use the mm_addr.u attribute.such as crypto context.
	public void set_addr_context( Object u )
	{
		this.net_udp.set_addr_context(u);
	}
	public Object get_addr_context()
	{
		return this.net_udp.get_addr_context();
	}
	public void clear_callback_holder()
	{
		this.net_udp.clear_callback_holder();
		this.queue_udp.clear_callback_holder();
	}
	//////////////////////////////////////////////////////////////////////////
	// gl_udp crypto encrypt buffer by tcp.
	public void crypto_encrypt(mm_udp udp,byte[] buffer,int offset,int length)
	{
		this.net_udp.crypto_encrypt(udp,buffer,offset,length);
	}
	// gl_udp crypto decrypt buffer by tcp.
	public void crypto_decrypt(mm_udp udp,byte[] buffer,int offset,int length)
	{
		this.net_udp.crypto_decrypt(udp,buffer,offset,length);
	}
	//////////////////////////////////////////////////////////////////////////
	public void assign_flush_handle_nearby(int milliseconds)
	{
		this.flush_task.assign_success_nearby(milliseconds);
	}
	public void assign_flush_broken_nearby(int milliseconds)
	{
		this.flush_task.assign_failure_nearby(milliseconds);
	}
	public void assign_state_handle_nearby(int milliseconds)
	{
		this.state_task.assign_success_nearby(milliseconds);
	}
	public void assign_state_broken_nearby(int milliseconds)
	{
		this.state_task.assign_failure_nearby(milliseconds);
	}
	// gl_udp_check_inactive
	// gl_udp_check_activate
	public void assign_state_check_flag(int flag)
	{
		this.state_check_flag = flag;
	}
	// gl_udp_ptq_inactive
	// gl_udp_ptq_activate
	public void assign_push_to_queue_flag(int flag)
	{
		this.push_to_queue_flag = flag;
	}
	//////////////////////////////////////////////////////////////////////////
	public void assign_max_poper_number(int max_pop)
	{
		this.queue_udp.assign_max_poper_number(max_pop);
	}
	//////////////////////////////////////////////////////////////////////////
	// lock to make sure the mm_gl_udp is thread safe.
	public void lock()
	{
		this.locker.lock();
	}
	// unlock net_udp.
	public void unlock()
	{
		this.locker.unlock();
	}
	// lock the flush send udp buffer.
	public void flush_lock()
	{
		this.net_udp.udp.lock();
	}
	// unlock the flush send udp buffer.
	public void flush_unlock()
	{
		this.net_udp.udp.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	// fopen socket.
	public void fopen_socket()
	{
		this.net_udp.fopen_socket();
	}
	// bind.
	public void bind()
	{
		this.net_udp.bind();
	}
	// close socket.
	public void close_socket()
	{
		this.net_udp.close_socket();
	}
	//////////////////////////////////////////////////////////////////////////
	public void check()
	{
		this.net_udp.check();
	}
	// get state finally.return 0 for all regular.not thread safe.
	public int finally_state()
	{
		return this.net_udp.finally_state();
	}
	//////////////////////////////////////////////////////////////////////////
	// synchronize attach to socket.not thread safe.
	public void attach_socket()
	{
		this.net_udp.attach_socket();
	}
	// synchronize detach to socket.not thread safe.
	public void detach_socket()
	{
		this.net_udp.detach_socket();
	}
	// main thread trigger self thread handle.such as gl thread.
	public void thread_handle()
	{
		this.queue_udp.thread_handle();
	}
	public void push(Object obj, mm_packet pack, mm_sockaddr remote)
	{
		this.queue_udp.push(obj, pack, remote);
	}
	//////////////////////////////////////////////////////////////////////////
	// handle send data from buffer and buffer length.>0 is send size success -1 failure.
	// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
	// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
	// 0 <  rt,means rt buffer is send,we must gbump rt size.
	// 0 == rt,means the send buffer can be full.
	// 0 >  rt,means the send process can be failure.
	public int buffer_send(byte[] buffer, int offset, int length, mm_sockaddr remote)
	{
		return this.net_udp.buffer_send(buffer, offset, length, remote);
	}
	// handle send data by flush send buffer.
	// 0 <  rt,means rt buffer is send,we must gbump rt size.
	// 0 == rt,means the send buffer can be full.
	// 0 >  rt,means the send process can be failure.
	public int flush_send()
	{
		return this.net_udp.flush_send();
	}
	//////////////////////////////////////////////////////////////////////////
	// use for trigger the flush send thread signal.
	// is useful for main thread asynchronous send message.
	public void flush_signal()
	{
		this.flush_task.signal();
	}
	// use for trigger the state check thread signal.
	// is useful for main thread asynchronous state check.
	public void state_signal()
	{
		this.state_task.signal();
	}
	//////////////////////////////////////////////////////////////////////////
	// start wait thread.
	public void start()
	{
		this.state = mm_thread.ts_finish == this.state ? mm_thread.ts_closed : mm_thread.ts_motion;
		this.net_udp.start();
		this.flush_task.start();
		this.state_task.start();
	}
	// interrupt wait thread.
	public void interrupt()
	{
		this.state = mm_thread.ts_closed;
		this.net_udp.interrupt();
		// 
		this.queue_udp.cond_not_null();
		this.queue_udp.cond_not_full();
		// signal the flush thread.
		this.flush_task.interrupt();
		this.state_task.interrupt();
	}
	// shutdown wait thread.
	public void shutdown()
	{
		this.state = mm_thread.ts_finish;
		this.net_udp.shutdown();
		// 
		this.queue_udp.cond_not_null();
		this.queue_udp.cond_not_full();
		// signal the flush thread.
		this.flush_task.shutdown();
		this.state_task.shutdown();
	}
	// join wait thread.
	public void join()
	{
		this.net_udp.join();
		this.flush_task.join();
		this.state_task.join();
	}
	//////////////////////////////////////////////////////////////////////////
	static final class __gl_udp_udp_callback extends mm_udp.mm_udp_callback
	{
		__gl_udp_udp_callback(mm_gl_udp _p)
		{
			this.obj = _p;
		}
		@Override
		public void handle( Object obj, byte[] buffer, int offset, int length, mm_sockaddr remote )
		{
			mm_udp udp = (mm_udp)(obj);
			mm_gl_udp gl_udp = (mm_gl_udp)(udp.callback.obj);
			mm_net_udp net_udp = gl_udp.net_udp;
			net_udp.crypto_decrypt(udp,buffer,offset,length);
			mm_streambuf_packet.handle_udp(udp.buff_recv, buffer, offset, length, new __gl_udp_udp_handle_packet_callback(), udp, remote);
		}
		@Override
		public void broken( Object obj )
		{
			mm_udp udp = (mm_udp)(obj);
			mm_gl_udp gl_udp = (mm_gl_udp)(udp.callback.obj);
			mm_net_udp net_udp = gl_udp.net_udp;
			// android assertions are unreliable.
			// assert null != net_udp.callback : "net_udp.callback is a null.";
			net_udp.apply_broken();
		}	
	}
	static final class __gl_udp_udp_handle_packet_callback implements mm_packet.udp_callBack
	{
		@Override
		public void handle_udp( Object obj, mm_packet pack, mm_sockaddr remote ) 
		{
			mm_udp udp = (mm_udp)(obj);
			mm_gl_udp gl_udp = (mm_gl_udp)(udp.callback.obj);
			mm_net_udp net_udp = gl_udp.net_udp;
			// android assertions are unreliable.
			// assert null != net_udp.callback : "net_udp.callback is a null.";
			net_udp.callback.handle(udp,net_udp.u,pack,remote);
		}		
	}
	static final class __gl_udp_net_udp_callback extends mm_net_udp.mm_net_udp_callback
	{
		__gl_udp_net_udp_callback(mm_gl_udp _p)
		{
			this.obj = _p;
		}
		@Override
		public void handle( Object obj, Object u, mm_packet pack, mm_sockaddr remote )
		{
			mm_udp udp = (mm_udp)(obj);
			mm_gl_udp gl_udp = (mm_gl_udp)(udp.callback.obj);
			mm_net_udp net_udp = gl_udp.net_udp;
			mm_net_udp.mm_net_udp_handle_callback handle = null;
			net_udp.holder_locker.lock();
			handle = (mm_net_udp.mm_net_udp_handle_callback)net_udp.holder.get(pack.phead.mid);
			net_udp.holder_locker.unlock();
			if (null != handle)
			{
				// fire the handle event.
				handle.handle(obj, u, pack, remote);
			}
			else
			{
				// android assertions are unreliable.
				// assert null != gl_udp.callback : "gl_udp.callback is a null.";
				gl_udp.callback.handle(obj, u, pack, remote);
			}
			// push to queue thread.
			if (gl_udp_ptq_activate == gl_udp.push_to_queue_flag)
			{
				gl_udp.push(obj, pack, remote);
			}			
		}
		@Override
		public void broken( Object obj )
		{
			mm_sockaddr remote = new mm_sockaddr();
			mm_packet pack = new mm_packet();
			mm_udp udp = (mm_udp)(obj);
			mm_gl_udp gl_udp = (mm_gl_udp)(udp.callback.obj);
			// push to queue thread.
			remote.init();
			pack.init();
			pack.head_base_zero();
			pack.hbuff.length = mm_packet.MM_MSG_COMM_HEAD_SIZE;
			pack.bbuff.length = 0;
			pack.shadow_alloc();
			pack.phead.mid = mm_udp.udp_mid_broken;
			pack.head_base_encode();
			this.handle(obj, gl_udp.u, pack, remote);
			pack.shadow_free();
			pack.destroy();
			remote.destroy();
			// net state signal.
			gl_udp.state_signal();
		}
		@Override
		public void nready( Object obj )
		{
			mm_sockaddr remote = new mm_sockaddr();
			mm_packet pack = new mm_packet();
			mm_udp udp = (mm_udp)(obj);
			mm_gl_udp gl_udp = (mm_gl_udp)(udp.callback.obj);
			// push to queue thread.
			remote.init();
			pack.init();
			pack.head_base_zero();
			pack.hbuff.length = mm_packet.MM_MSG_COMM_HEAD_SIZE;
			pack.bbuff.length = 0;
			pack.shadow_alloc();
			pack.phead.mid = mm_udp.udp_mid_nready;
			pack.head_base_encode();
			this.handle(obj, gl_udp.u, pack, remote);
			pack.shadow_free();
			pack.destroy();
			remote.destroy();
			// net state signal.
			gl_udp.state_signal();
		}
		@Override
		public void finish( Object obj )
		{
			mm_sockaddr remote = new mm_sockaddr();
			mm_packet pack = new mm_packet();
			mm_udp udp = (mm_udp)(obj);
			mm_gl_udp gl_udp = (mm_gl_udp)(udp.callback.obj);
			// push to queue thread.
			remote.init();
			pack.init();
			pack.head_base_zero();
			pack.hbuff.length = mm_packet.MM_MSG_COMM_HEAD_SIZE;
			pack.bbuff.length = 0;
			pack.shadow_alloc();
			pack.phead.mid = mm_udp.udp_mid_finish;
			pack.head_base_encode();
			this.handle(obj, gl_udp.u, pack, remote);
			pack.shadow_free();
			pack.destroy();
			remote.destroy();
			// net state signal.
			gl_udp.state_signal();
		}
	}
	static final class __gl_udp_flush_signal_task extends mm_signal_task.mm_signal_task_callback 
	{
		__gl_udp_flush_signal_task(mm_gl_udp _p)
		{
			this.obj = _p;
		}
		// factor for condition wait.return code for 0 is fire event -1 for condition wait.
		public int factor( Object obj) 
		{
			int code = -1;
			mm_signal_task signal_task = (mm_signal_task)(obj);
			mm_gl_udp gl_udp = (mm_gl_udp)(signal_task.callback.obj);
			mm_udp udp = gl_udp.net_udp.udp;
			int buff_len = 0;
			udp.lock();
			buff_len = udp.buff_send.size();
			udp.unlock();
			code = ( 0 == buff_len ? 0 : -1 );
			return code;
		}
		// handle for fire event.return code result 0 for success -1 for failure.
		public int handle( Object obj)
		{
			int code = -1;
			mm_signal_task signal_task = (mm_signal_task)(obj);
			mm_gl_udp gl_udp = (mm_gl_udp)(signal_task.callback.obj);
			mm_udp udp = gl_udp.net_udp.udp;
			int real_len = 0;
			// check the state.
			if (0 == gl_udp.finally_state())
			{
				// handle send data by flush send buffer.
				// 0 <  rt,means rt buffer is send,we must gbump rt size.
				// 0 == rt,means the send buffer can be full.
				// 0 >  rt,means the send process can be failure.
				udp.lock();
				real_len = udp.flush_send();
				udp.unlock();
				if ( 0 > real_len )
				{
					// the socket is broken.
					// but the send thread can not broken.
					// it is wait the reconnect and send.
					code = -1;
				}
				else
				{
					// the finally state is regular.
					// if 0 == real_len.the state is broken or the socket send buffer is full.
					// just think it is regular.
					code = 0;
				}
			}
			else
			{
				// the finally state is broken.
				// wake the state check process.
				gl_udp.state_signal();
				code = -1;
			}
			return code;
		}
    }
	static final class __gl_udp_state_signal_task extends mm_signal_task.mm_signal_task_callback 
	{
		__gl_udp_state_signal_task(mm_gl_udp _p)
		{
			this.obj = _p;
		}
		// factor for condition wait.return code for 0 is fire event -1 for condition wait.
		public int factor( Object obj) 
		{
			int code = -1;
			mm_signal_task signal_task = (mm_signal_task)(obj);
			mm_gl_udp gl_udp = (mm_gl_udp)(signal_task.callback.obj);
			mm_udp udp = gl_udp.net_udp.udp;
			mm_addr addr = udp.addr;
			code = (
					true == mm_sockaddr.sockaddr_storage_compare(addr.ss_native,gl_udp.ss_native) && 
					true == mm_sockaddr.sockaddr_storage_compare(addr.ss_remote,gl_udp.ss_remote) && 
					0 == gl_udp.finally_state() ) ? 0 : -1;
			return code;
		}
		// handle for fire event.return code result 0 for success -1 for failure.
		public int handle( Object obj)
		{
			int code = -1;
			mm_signal_task signal_task = (mm_signal_task)(obj);
			mm_gl_udp gl_udp = (mm_gl_udp)(signal_task.callback.obj);
			if (gl_udp_check_activate == gl_udp.state_check_flag)
			{
				mm_net_udp net_udp = gl_udp.net_udp;
				mm_udp udp = gl_udp.net_udp.udp;
				mm_addr addr = udp.addr;
				if ( 
					! mm_sockaddr.sockaddr_storage_compare(addr.ss_native,gl_udp.ss_native) || 
					! mm_sockaddr.sockaddr_storage_compare(addr.ss_remote,gl_udp.ss_remote) )
				{
					// if target address is not the real address,we disconnect current socket.
					gl_udp.lock();
					// first disconnect current socket.
					net_udp.detach_socket();
					// assign the target address.
					addr.assign_native_storage(gl_udp.ss_native);
					addr.assign_remote_storage(gl_udp.ss_remote);
					gl_udp.unlock();
				}
				if ( 
						mm_sockaddr.sockaddr_storage_compare_address(addr.ss_native,mm_sockaddr.MM_ADDR_DEFAULT_NODE,mm_sockaddr.MM_ADDR_DEFAULT_PORT) && 
						mm_sockaddr.sockaddr_storage_compare_address(addr.ss_remote,mm_sockaddr.MM_ADDR_DEFAULT_NODE,mm_sockaddr.MM_ADDR_DEFAULT_PORT))
				{
					// if the native and remote is empty.fire the broken event immediately.
					gl_udp.lock();
					net_udp.apply_broken();
					gl_udp.unlock();
				}
				else
				{
					gl_udp.lock();
					gl_udp.check();
					gl_udp.unlock();
				}
			}
			code = ( 0 == gl_udp.finally_state() ) ? 0 : -1;
			return code;
		}
    }
}
