package mm.net;

import java.nio.channels.spi.AbstractSelectableChannel;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

import mm.core.mm_socket;
import mm.core.mm_spinlock;

public class mm_mailbox
{
	public static final String TAG = mm_mailbox.class.getSimpleName();
	
	static final __mailbox_tcp_handle_packet_callback __mailbox_tcp_handle_packet_callback_impl = new __mailbox_tcp_handle_packet_callback();
	
	public interface mm_mailbox_traver_callback 
	{  
	    public void traver( mm_mailbox mailbox, mm_tcp tcp, Object u );
	}	
	// packet handle function type.
	public interface mm_mailbox_handle_callback 
	{  
	    public void handle( Object obj, Object u, mm_packet pack );
	}
	public static class mm_mailbox_callback
	{	
		public void handle( Object obj, Object u, mm_packet pack ){}
		public void broken( Object obj ){}
		public void nready( Object obj ){}
		public void finish( Object obj ){}
		Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	}
	public static class mm_mailbox_event_tcp_alloc
	{	
		public void event_tcp_alloc( Object p, mm_tcp tcp ){}
		public void event_tcp_relax( Object p, mm_tcp tcp ){}
		Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	}

	public mm_accepter accepter = new mm_accepter();
	public mm_nucleus nucleus = new mm_nucleus();// strong ref.
	public TreeMap<Integer,mm_mailbox_handle_callback> holder = new TreeMap<Integer,mm_mailbox_handle_callback>();// rb tree for msg_id <--> callback.
	public mm_mailbox_callback callback = new mm_mailbox_callback();// value ref. tcp callback.
	public mm_mailbox_event_tcp_alloc event_tcp_alloc = new mm_mailbox_event_tcp_alloc();// value ref.event tcp alloc.
	public mm_crypto.mm_crypto_callback crypto_callback = new mm_crypto.mm_crypto_callback();// crypto callback.
	public mm_spinlock holder_locker = new mm_spinlock();
	public mm_spinlock locker = new mm_spinlock();
	public Object u = null;// user data.
	
	//////////////////////////////////////////////////////////////////////////	
	public void init()
	{
		this.accepter.init();
		this.nucleus.init();
		this.holder.clear();
		this.callback.init();
		this.event_tcp_alloc.init();
		this.crypto_callback.init();
		this.holder_locker.init();
		this.locker.init();
		this.u = null;

		this.accepter.assign_callback(new __mailbox_accepter_accept_call_back(this));		
		this.nucleus.assign_callback(new __mailbox_nuclear_call_back(this));
	}
	public void destroy()
	{
		this.clear_callback_holder();
		//
		this.clear();
		//
		this.accepter.destroy();
		this.nucleus.destroy();
		this.holder.clear();
		this.callback.destroy();
		this.event_tcp_alloc.destroy();
		this.crypto_callback.destroy();
		this.holder_locker.destroy();
		this.locker.destroy();
		this.u = null;
	}
	public void assign_native(String node,int port)
	{
		this.accepter.assign_native(node, port);
	}
	public void assign_remote(String node,int port)
	{
		this.accepter.assign_remote(node, port);
	}
	// assign protocol_size.
	public void set_length(int length)
	{
		this.nucleus.set_length(length);
	}
	public int get_length()
	{
		return this.nucleus.get_length();
	}
	// fopen.
	public void fopen_socket()
	{
		this.accepter.fopen_socket();
	}
	// bind.
	public void bind()
	{
		this.accepter.bind();
	}
	// listen.
	public void listen()
	{
		this.accepter.listen();
	}
	// close socket. mm_mailbox_join before call this.
	public void close_socket()
	{
		this.accepter.close_socket();
	}
	//////////////////////////////////////////////////////////////////////////
	// lock to make sure the mailbox is thread safe.
	public void lock()
	{
		this.locker.lock();
	}
	// unlock mailbox.
	public void unlock()
	{
		this.locker.unlock();
	}

	public void assign_callback(int id,mm_mailbox_handle_callback callback)
	{
		this.holder_locker.lock();
		this.holder.put(id, callback);
		this.holder_locker.unlock();
	}
	public void assign_event_tcp_alloc(mm_mailbox_event_tcp_alloc event_tcp_alloc)
	{
		// android assertions are unreliable.
		// assert null != event_tcp_alloc : "you can not assign null event_tcp_alloc.";
		this.event_tcp_alloc = event_tcp_alloc;
	}
	public void assign_mailbox_callback(mm_mailbox_callback mailbox_callback)
	{
		// android assertions are unreliable.
		// assert null != mailbox_callback : "you can not assign null mailbox_callback.";
		this.callback = mailbox_callback;	
	}
	// do not assign when holder have some elem.
	public void assign_crypto_callback(mm_crypto.mm_crypto_callback crypto_callback)
	{
		// android assertions are unreliable.
		// assert null != crypto_callback : "you can not assign null crypto_callback.";
		this.crypto_callback = crypto_callback;
	}
	public void clear_callback_holder()
	{
		this.holder_locker.lock();
		this.holder.clear();
		this.holder_locker.unlock();
	}
	// mailbox crypto encrypt buffer by tcp.
	public void crypto_encrypt(mm_tcp tcp,byte[] buffer,int offset,int length)
	{
		this.crypto_callback.encrypt(this, tcp, buffer, offset, buffer, offset, length);
	}
	// mailbox crypto decrypt buffer by tcp.
	public void crypto_decrypt(mm_tcp tcp,byte[] buffer,int offset,int length)
	{
		this.crypto_callback.decrypt(this, tcp, buffer, offset, buffer, offset, length);
	}
	// not lock inside,lock tcp manual.this func will copy each nuclear poll fd holder,
	// and traver all fd(not the tcp weak ref),use fd get the tcp for nuclear and trigger callback.
	public void traver(mm_mailbox_traver_callback func,Object u)
	{
		int i = 0;
		//
		Entry<Integer, Object> n = null;
		Iterator<Entry<Integer, Object>> it = null;
		mm_nuclear nuclear = null;
		mm_tcp tcp = null;
		//
		TreeMap<Integer, Object> fd_holder = new TreeMap<Integer, Object>();
		//
		fd_holder.clear();
		this.nucleus.arrays_locker.unlock();
		for ( i = 0; i < this.nucleus.length; ++i)
		{
			nuclear = this.nucleus.arrays[i];
			// the nuclear is only one thread fd set.
			// N fd at one set.we copy a weak ref for set.
			// note: if the set is too big will case performance problem.
			// N is less than [2000,5000] recommend.
			// machine 2 cores 2G keep 2 * 5000 = 10000 connect.
			nuclear.fd_copy_holder(fd_holder);
			//
			it = nuclear.holder.entrySet().iterator();
			while (it.hasNext()) 
			{
				n = it.next();
				// here we use the cache fd at fd_holder.
				tcp = (mm_tcp)n.getValue();
				if(null != tcp)
				{
					// if the tcp value is exists.
					// we trigger callback.
					// note: the callback function inside recommend lock the tcp ref immediately
					//       make sure the tcp memory can be a valid.
					func.traver(this, tcp, u);
				}
			}
			
			
//			n = mm_rb_first(&fd_holder.rbt);
//			while(NULL != n)
//			{
//				it = (struct mm_holder_u32_vpt_iterator*)mm_rb_entry(n, struct mm_holder_u32_vpt_iterator, n);
//				n = mm_rb_next(n);
//				// here we use the cache fd at fd_holder.
//				tcp = (struct mm_tcp*)mm_nucleus_fd_get(&p->nucleus,it->k);
//				if (NULL != tcp)
//				{
//					// if the tcp value is exists.
//					// we trigger callback.
//					// note: the callback function inside recommend lock the tcp ref immediately
//					//       make sure the tcp memory can be a valid.
//					func.traver(this, tcp, u);
//				}
//			}
		}
		this.nucleus.arrays_locker.unlock();
		fd_holder.clear();
		
		
		
		
		
		
//		int i = 0;
//		this.nucleus.arrays_locker.lock();
//		for ( i = 0; i < this.nucleus.length; ++i)
//		{
//			Entry<Integer, Object> n = null;
//			Iterator<Entry<Integer, Object>> it = null;
//			mm_tcp tcp = null;
//			mm_nuclear nuclear = this.nucleus.arrays[i];
//			this.nucleus.arrays_locker.lock();
//			it = nuclear.holder.entrySet().iterator();
//			while (it.hasNext()) 
//			{
//				n = it.next();
//				tcp = (mm_tcp)n.getValue();
//				func.traver(this, tcp, u);
//			}
//			this.nucleus.arrays_locker.unlock();
//		}
//		this.nucleus.arrays_locker.unlock();
	}
	// poll size.
	public int poll_size()
	{
		return this.nucleus.poll_size();
	}
	public void start()
	{
		this.accepter.start();
		this.nucleus.start();
	}
	public void interrupt()
	{
		this.accepter.interrupt();
		this.nucleus.interrupt();
	}
	public void shutdown()
	{
		this.accepter.shutdown();
		this.nucleus.shutdown();
	}
	public void join()
	{
		this.accepter.join();
		this.nucleus.join();
	}
	public mm_tcp add(AbstractSelectableChannel fd,mm_sockaddr remote)
	{
		mm_tcp tcp = new mm_tcp();
		tcp.init();
		//
		tcp.assign_callback(new __mailbox_tcp_call_back(this));
		//
		tcp.assign_remote_storage(remote);
		tcp.assign_native_storage(this.accepter.addr.ss_native);
		tcp.addr.socket = fd;
		tcp.addr.set_block(mm_socket.MM_NONBLOCK);
		//
		this.nucleus.fd_add(fd, tcp);
		//
		this.event_tcp_alloc.event_tcp_alloc(this, tcp);
		//
		return tcp;
	}
	public mm_tcp get(AbstractSelectableChannel fd)
	{
		return (mm_tcp)this.nucleus.fd_get(fd);
	}
	public mm_tcp get_instance(AbstractSelectableChannel fd,mm_sockaddr remote)
	{
		mm_tcp tcp = this.get(fd);
		if (null == tcp)
		{
			tcp = this.add(fd,remote);
		}
		return tcp;
	}
	public void rmv(AbstractSelectableChannel fd)
	{
		mm_tcp tcp = this.get(fd);
		if (null != tcp)
		{
			// mm_mailbox_rmv_tcp(p,tcp);
			// here we can lock the tcp,becaue all callback.broken only recv process trigger.
			tcp.lock();
			this.nucleus.fd_rmv(fd, tcp);
			tcp.unlock();
			this.event_tcp_alloc.event_tcp_relax(this, tcp);
			tcp.destroy();
			tcp = null;
		}
	}
	public void rmv_tcp(mm_tcp tcp)
	{
		this.nucleus.fd_rmv(tcp.addr.socket, tcp);
	}
	public void clear()
	{
		int i = 0;
		Entry<Integer, Object> n = null;
		Iterator<Entry<Integer, Object>> it = null;
		mm_tcp tcp = null;
		mm_nuclear nuclear = null;
		//
		this.nucleus.arrays_locker.lock();
		for ( i = 0; i < this.nucleus.length; ++i)
		{
			nuclear = this.nucleus.arrays[i];
			nuclear.cond_locker.lock();
			it = nuclear.holder.entrySet().iterator();
			while (it.hasNext()) 
			{
				n = it.next();
				tcp = (mm_tcp)n.getValue();
				tcp.lock();
				nuclear.fd_rmv_poll(tcp.addr.socket, tcp);
				tcp.unlock();
				this.event_tcp_alloc.event_tcp_relax(this, tcp);
				tcp.destroy();
				tcp = null;
			}
			nuclear.cond_locker.unlock();
			// trigger poll wake signal.
			nuclear.fd_poll_signal();
		}
		this.nucleus.arrays_locker.unlock();
	}
	// use this function for manual shutdown tcp.
	// note: you can not use rmv or rmv_tcp for shutdown a activation tcp.
	public void shutdown_tcp(mm_tcp tcp)
	{
		tcp.shutdown_socket(mm_socket.MM_BOTH_SHUTDOWN);
	}
	/////////////////////////////////////////////////////////////
	static final class __mailbox_nuclear_call_back extends mm_nuclear.mm_nuclear_callback
	{
		__mailbox_nuclear_call_back(mm_mailbox _p)
		{
			this.obj = _p;
		}
		@Override
		public void handle_recv( Object obj, Object u, byte[] buffer, int offset, int max_length )
		{
			mm_tcp tcp = (mm_tcp)(u);
			tcp.handle_recv(buffer,offset,max_length);	
		}
		@Override
		public void handle_send( Object obj, Object u, byte[] buffer, int offset, int max_length )
		{
			mm_tcp tcp = (mm_tcp)(u);
			tcp.handle_send(buffer,offset,max_length);
		}
	};
	static final class __mailbox_accepter_accept_call_back extends mm_accepter.mm_accepter_callback
	{
		__mailbox_accepter_accept_call_back(mm_mailbox _p)
		{
			this.obj = _p;
		}
		@Override
		public void accept( Object obj, AbstractSelectableChannel fd, mm_sockaddr remote)
		{
			mm_accepter accepter = (mm_accepter)obj;
			mm_mailbox mailbox = (mm_mailbox)(accepter.callback.obj);
			mailbox.get_instance(fd,remote);
		}	
	};
	static final class __mailbox_tcp_call_back extends mm_tcp.mm_tcp_callback
	{
		__mailbox_tcp_call_back(mm_mailbox _p)
		{
			this.obj = _p;
		}
		@Override
		public void handle( Object obj, byte[] buffer, int offset, int length )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			mm_mailbox mailbox = (mm_mailbox)(tcp.callback.obj);
			mailbox.crypto_decrypt(tcp,buffer,offset,length);
			mm_streambuf_packet.handle_tcp(tcp.buff_recv, buffer, offset, length, __mailbox_tcp_handle_packet_callback_impl, tcp);
		}
		@Override
		public void broken( Object obj )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			mm_mailbox mailbox = (mm_mailbox)(tcp.callback.obj);
			// android assertions are unreliable.
			// assert null != mailbox.callback : "mailbox.callback is a null.";
			mailbox.callback.broken(tcp);
			mailbox.rmv(tcp.addr.socket);
		}	
	};
	static final class __mailbox_tcp_handle_packet_callback implements mm_packet.tcp_callBack
	{
		@Override
		public void handle_tcp( Object obj, mm_packet pack ) 
		{
			mm_tcp tcp = (mm_tcp)(obj);
			mm_mailbox mailbox = (mm_mailbox)(tcp.callback.obj);
			mm_mailbox_handle_callback handle = null;
			mailbox.holder_locker.lock();
			handle = mailbox.holder.get(pack.phead.mid);
			mailbox.holder_locker.unlock();
			if (null != handle)
			{
				// fire the handle event.
				handle.handle(tcp, mailbox.u, pack);
			}
			else
			{
				// android assertions are unreliable.
				// assert null != mailbox.callback : "mailbox.callback is a null.";
				mailbox.callback.handle(tcp, mailbox.u, pack);
			}
		}		
	}
}
