package mm.net;

import mm.core.mm_byte;
import mm.core.mm_streambuf;
import mm.core.mm_type;

public class mm_streambuf_packet
{
	//////////////////////////////////////////////////////////////////////////
	//util about struct mm_streambuf and struct mm_packet.
	//////////////////////////////////////////////////////////////////////////
	//append pack data to streambuf.
	public static int append( mm_streambuf p, mm_packet pack )
	{	
		int msg_size = (short) (mm_type.MM_INT32_SIZE + pack.hbuff.length + pack.bbuff.length);
		int head_data = (int) ( pack.hbuff.length << mm_packet.MM_MSG_SIZE_BIT_NUM ) | (int) msg_size;
		// if the idle put size is lack, we try remove the get buffer.
		p.aligned_memory(msg_size);
		//
		byte[] s = new byte[mm_type.MM_INT32_SIZE];
		mm_byte.int32_encode_bytes(s, 0, head_data);
		p.sputn(s, 0, mm_type.MM_INT32_SIZE);
		//
		p.sputn(pack.hbuff.buffer,pack.hbuff.offset,pack.hbuff.length);
		p.sputn(pack.bbuff.buffer,pack.bbuff.offset,pack.bbuff.length);
		return msg_size;
	}
	//overdraft pack.hbuff pack.bbuff for encode.
	//will pbump streambuf.
	public static int overdraft( mm_streambuf p, mm_packet pack )
	{
		int msg_size = (short) (mm_type.MM_INT32_SIZE + pack.hbuff.length + pack.bbuff.length);
		int head_data = (int) ( pack.hbuff.length << mm_packet.MM_MSG_SIZE_BIT_NUM ) | (int) msg_size;
		// if the idle put size is lack, we try remove the get buffer.
		p.aligned_memory(msg_size);
		//
		pack.hbuff.buffer = p.buff;
		pack.hbuff.offset = p.pptr + mm_type.MM_INT32_SIZE;
		pack.bbuff.buffer = p.buff;
		pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
		
		byte[] s = new byte[mm_type.MM_INT32_SIZE];
		mm_byte.int32_encode_bytes(s, 0, head_data);	
		System.arraycopy(s,0,p.buff,p.pptr,mm_type.MM_INT32_SIZE);
		p.pbump(msg_size);
		return msg_size;
	}
	//////////////////////////////////////////////////////////////////////////
	//MM_MSG_COMM_HEAD_SIZE valid.
	//////////////////////////////////////////////////////////////////////////
	//if need quick transmit one transport recv data to another transport,call this.it is safe.
	//assign sid quick,make sure the data must struct mm_packet format.
	public static void assign_sid( mm_streambuf p, long sid )
	{
		// the first mm_uint32_t is hsize|bsize.
		byte[] s = new byte[mm_type.MM_INT64_SIZE];
		mm_byte.int64_encode_bytes(s, 0, sid);	
		System.arraycopy( s, 0, p.buff, p.pptr + mm_type.MM_INT32_SIZE * 3, mm_type.MM_INT64_SIZE );
	}
	//if need quick transmit one transport recv data to another transport,call this.it is safe.
	//assign pid quick,make sure the data must struct mm_packet format.
	public static void assign_pid( mm_streambuf p, int pid )
	{
		// the first mm_uint32_t is hsize|bsize.
		byte[] s = new byte[mm_type.MM_INT32_SIZE];
		mm_byte.int32_encode_bytes(s, 0, pid);
		System.arraycopy( s, 0, p.buff, p.pptr + mm_type.MM_INT32_SIZE * 2, mm_type.MM_INT32_SIZE );
	}
	//////////////////////////////////////////////////////////////////////////
	//packet handle tcp buffer and length for streambuf handle separate packet.
	public static void handle_tcp( mm_streambuf p, byte[] buffer, int offset, int length, mm_packet.tcp_callBack handle, Object obj)
	{
		mm_packet pack = new mm_packet();
		int head_data = 0;
		short msg_size = 0;
		int buff_size = 0;
		// if the idle put size is lack, we try remove the get buffer.
		p.aligned_memory(length);
		// copy the recv buffer to streambuf.
		p.sputn(buffer,offset,length);
		do
		{
			buff_size = p.size();
			head_data = mm_byte.int32_decode_bytes(p.buff, p.gptr);
			msg_size = (short) (head_data & mm_packet.MM_MSG_HEAD_L_MASK);
			if (buff_size >= msg_size)
			{
				// here we just use the weak ref from streambuf.for data recv callback.
				pack.hbuff.length = head_data >> mm_packet.MM_MSG_SIZE_BIT_NUM;
				pack.hbuff.buffer = p.buff;
				pack.hbuff.offset = p.gptr + mm_type.MM_INT32_SIZE;
				//
				pack.bbuff.length = msg_size - mm_type.MM_INT32_SIZE - pack.hbuff.length;
				pack.bbuff.buffer = p.buff;
				pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
				//
				pack.head_base_decode();
				//
				// fire the field buffer recv event.
				handle.handle_tcp(obj,pack);
				// no matter session_recv how much buffer reading,
				// we removeread fixed size to make sure the socket buffer sequence is correct.
				p.gbump(msg_size);
			}
		} while ( buff_size > msg_size && msg_size >= mm_packet.MM_MSG_BASE_HEAD_SIZE );
	}
	//packet handle udp buffer and length for streambuf handle separate packet.
	public static void handle_udp( mm_streambuf p, byte[] buffer, int offset, int length, mm_packet.udp_callBack handle, Object obj, mm_sockaddr remote)
	{
		mm_packet pack = new mm_packet();
		int head_data = 0;
		short msg_size = 0;
		int buff_size = 0;
		// if the idle put size is lack, we try remove the get buffer.
		p.aligned_memory(length);
		// copy the recv buffer to streambuf.
		p.sputn(buffer,offset,length);
		do
		{
			buff_size = p.size();
			head_data = mm_byte.int32_decode_bytes(p.buff, p.gptr);
			msg_size = (short) (head_data & mm_packet.MM_MSG_HEAD_L_MASK);
			if (buff_size >= msg_size)
			{
				// here we just use the weak ref from streambuf.for data recv callback.
				pack.hbuff.length = head_data >> mm_packet.MM_MSG_SIZE_BIT_NUM;
				pack.hbuff.buffer = p.buff;
				pack.hbuff.offset = p.gptr + mm_type.MM_INT32_SIZE;
				//
				pack.bbuff.length = msg_size - mm_type.MM_INT32_SIZE - pack.hbuff.length;
				pack.bbuff.buffer = p.buff;
				pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
				//
				pack.head_base_decode();
				//
				// fire the field buffer recv event.
				handle.handle_udp(obj,pack,remote);
				// no matter session_recv how much buffer reading,
				// we removeread fixed size to make sure the socket buffer sequence is correct.
				p.gbump(msg_size);
			}
		} while ( buff_size > msg_size && msg_size >= mm_packet.MM_MSG_BASE_HEAD_SIZE );
	}
	//////////////////////////////////////////////////////////////////////////
};
