package mm.net;

import mm.core.mm_errno;
import mm.core.mm_logger;
import mm.core.mm_socket;
import mm.core.mm_streambuf;

public class mm_udp
{
	public static final String TAG = mm_udp.class.getSimpleName();
	
	public static final int udp_mid_broken = 0x0F000010;
	public static final int udp_mid_nready = 0x0F000011;
	public static final int udp_mid_finish = 0x0F000012;
	
	public static class mm_udp_callback
	{
		public void handle( Object obj, byte[] buffer, int offset, int length, mm_sockaddr remote ){}
		public void broken( Object obj ){}
		public Object obj;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	};
	
	public mm_addr addr = new mm_addr();// strong ref. tcp address. 
	public mm_streambuf buff_recv = new mm_streambuf();// strong ref.
	public mm_streambuf buff_send = new mm_streambuf();// strong ref.
	public mm_udp_callback callback = new mm_udp_callback();// value ref. transport callback.
	public long unique_id = 0;// feedback unique_id.
	
	public void init()
	{		
		this.addr.init();
		this.buff_recv.init();
		this.buff_send.init();
		this.callback.init();
		this.unique_id = 0;
	}
	public void destroy()
	{
		this.addr.destroy();
		this.buff_recv.destroy();
		this.buff_send.destroy();
		this.callback.destroy();
		this.unique_id = 0;
	}
	public void lock()
	{
		this.addr.lock();
	}
	public void unlock()
	{
		this.addr.unlock();
	}
	// assign addr native by ip port.
	public void assign_native( String node, int port )
	{
		this.addr.assign_native(node, port);
	}
	// assign addr remote by ip port.
	public void assign_remote( String node, int port )
	{
		this.addr.assign_remote(node, port);
	}
	// assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
	public void assign_native_storage( mm_sockaddr ss_native )
	{
		this.addr.assign_native_storage(ss_native);
	}
	// assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
	public void assign_remote_storage( mm_sockaddr ss_remote )
	{
		this.addr.assign_remote_storage(ss_remote);
	}
	//////////////////////////////////////////////////////////////////////////
	public void assign_callback(mm_udp_callback cb)
	{
		assert null != cb : "you can not assign null callback.";
		this.callback = cb;
	}
	public void set_unique_id(long unique_id)
	{
		this.unique_id = unique_id;
	}
	public long get_unique_id()
	{
		return this.unique_id;
	}
	// context for udp will use the mm_addr.u attribute.such as crypto context.
	public void set_context( Object u )
	{
		this.addr.set_context(u);
	}
	public Object get_context()
	{
		return this.addr.get_context();
	}
	//////////////////////////////////////////////////////////////////////////
	// fopen socket.ss_native.ss_family,SOCK_DGRAM,0
	public void fopen_socket()
	{
		this.addr.fopen_socket(mm_socket.MM_AF_INET6, mm_socket.SOCK_DGRAM, 0);
		if (mm_socket.MM_INVALID_SOCKET != this.addr.socket)
		{
			mm_logger.logI(TAG + " fopen_socket " + this.addr.to_string() + " success.");
		}
		else
		{
			mm_logger.logE(TAG + " fopen_socket " + this.addr.to_string() + " failure.");
		}
	}
	//close socket.
	public void close_socket()
	{
		this.addr.close_socket();
	}
	// shutdown socket.
	public void shutdown_socket(int opcode)
	{
		this.addr.shutdown_socket(opcode);
	}
	//////////////////////////////////////////////////////////////////////////
	//handle recv for buffer pool and pool max size.
	public void handle_recv( byte[] buffer, int offset, int max_length )
	{
		mm_sockaddr remote = new mm_sockaddr();
		int real_len = 0;
		// char link_name[MM_LINK_NAME_LENGTH];
		//
		// we not need assert the fd is must valid.
		assert null != this.callback : "this.callback is invalid.";
		// assert(p->callback.handle && p->callback.broken && "callback.handle or callback.broken is invalid.");
		//
		do
		{
			java.util.Arrays.fill(buffer, offset, max_length - offset, (byte) 0);
			// mm_memset(buffer + offset,0,max_length);
			real_len = this.addr.recv_dgram(buffer, offset, max_length, 0, remote);
			if ( -1 == real_len )
			{
				int errcode = mm_errno.get_code();
				//-MM_EBADF
				//-MM_ENOTSOCK
				//-MM_EAGAIN
				//-MM_ENOBUFS
				//-MM_ENOMEM
				//-MM_EINVAL

				// errno:(10022) An invalid argument was supplied.
				// when we not sendto but recvfrom will case this error.
				// this error is not serious.
				if ( mm_errno.MM_EINVAL == errcode )
				{
					// sleep_msec for waiting for something sendto remote.
					break;
				}
				else if ( mm_errno.MM_ECONNRESET == errcode )
				{
					// An existing connection was forcibly closed by the remote host.
					// when the remote connection not open will case this error.
					break;
				}
				else if ( mm_errno.MM_EAGAIN == errcode )
				{
					// MM_EAGAIN EWOULDBLOCK
					// is timeout if setsocketopt SO_RCVTIMEO.
					break;
				}
				else
				{
					// at here when we want interrupt accept, will case a socket errno.
					// [errno:(53) Software caused connection abort] it is expected result.
					mm_logger.logI(TAG + " " + this.addr.to_string() + " error occur.");
					// this.callback.broken(this);
					break;
				}
			}
			else if( 0 == real_len )
			{
				// if this recv operate 0 == real_len,means the recv buffer is full or target socket is closed.
				// so we must use poll for checking the io event make sure read event is fire.
				// at here send length is 0 will fire the broken event.
				
				// the other side close the socket.
				
				mm_logger.logI(TAG + " " + this.addr.to_string() + " error occur.");
				// this.callback.broken(this);
				break;
			}
			// if( real_len != max_length )
			// {
			// 	this.buffer_recv( buffer, offset, real_len, remote );
			// 	break;
			// }
			// else
			// {
			// 	this.buffer_recv( buffer, offset, real_len, remote );
			// }
			// udp not like tcp socket just recv until the udp socket is empty.
			this.buffer_recv( buffer, offset, real_len, remote );
		} while (true);
	}
	//handle send for buffer pool and pool max size.
	public void handle_send( byte[] buffer, int offset, int max_length )
	{
		// do nothing.
	}
	//////////////////////////////////////////////////////////////////////////
	//handle recv data from buffer and buffer length.0 success -1 failure.
	public int buffer_recv( byte[] buffer, int offset, int length, mm_sockaddr remote )
	{
		assert null != this.callback : "this.callback is invalid.";
		this.callback.handle( this, buffer, offset, length, remote );
		return 0;
	}
	//handle send data from buffer and buffer length.>0 is send size success -1 failure.
	//note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
	//only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
	//0 <  rt,means rt buffer is send,we must gbump rt size.
	//0 == rt,means the send buffer can be full.
	//0 >  rt,means the send process can be failure.
	public int buffer_send( byte[] buffer, int offset, int length, mm_sockaddr remote )
	{
		int real_len = 0;
		int send_len = 0;
		do
		{
			if (0 == length)
			{
				// nothing for send break immediately.
				break;
			}
			real_len = this.addr.send_dgram( buffer, offset, length, 0, remote );
			if ( -1 == real_len )
			{
				int errcode = mm_errno.get_code();
				if ( mm_errno.MM_EAGAIN == errcode )
				{
					// MM_EAGAIN errno:(10035) A non-blocking socket operation could not be completed immediately.
					// MM_EAGAIN is well.
					// send_len will be 0.
					send_len = 0;
					break;
				}
				else
				{
					mm_logger.logI(TAG + " " + this.addr.to_string() + " error occur.");
					send_len = -1;
					break;
				}
			}
			else
			{
				send_len = real_len;
				break;
			}
		}while(true);
		return send_len;
	}
	//////////////////////////////////////////////////////////////////////////
	//handle send data by flush send buffer.
	//0 <  rt,means rt buffer is send,we must gbump rt size.
	//0 == rt,means the send buffer can be full.
	//0 >  rt,means the send process can be failure.
	public int flush_send()
	{
		int sz = this.buff_send.size();
		// use self streambuf.
		this.buffer_send( this.buff_send.buff, this.buff_send.gptr, sz, this.addr.ss_remote);
		// we not need check why udp send error.just gbump the buffer.
		this.buff_send.gbump( sz );
		return sz;
	}
	//////////////////////////////////////////////////////////////////////////
	//udp streambuf recv send reset.
	public void streambuf_reset()
	{
		this.buff_recv.reset();
		this.buff_send.reset();
	}
	//////////////////////////////////////////////////////////////////////////
}
