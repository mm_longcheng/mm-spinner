package mm.net;

import mm.core.mm_signal_task;
import mm.core.mm_spinlock;
import mm.core.mm_thread;

public class mm_gl_tcp
{
	public static class mm_gl_tcp_callback
	{
		public void handle( Object obj, Object u, mm_packet pack ){}
		public Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	};
	
	// gl tcp connect handle check milliseconds.default is 5 second(5000 ms).
	public static final int MM_GL_TCP_HANDLE_MSEC_CHECK = 5000;
	// gl tcp connect broken check milliseconds.default is 1 second(1000 ms).
	public static final int MM_GL_TCP_BROKEN_MSEC_CHECK = 1000;

	// send thread msleep interval handle time.
	public static final int MM_GL_TCP_SEND_INTERVAL_HANDLE_TIME = 0;
	// send thread msleep interval broken time.
	public static final int MM_GL_TCP_SEND_INTERVAL_BROKEN_TIME = 1000;

	public static final int gl_tcp_check_inactive = 0;// do not check the connect state.
	public static final int gl_tcp_check_activate = 1;// check the connect state.
	
	public static final int gl_tcp_ptq_inactive = 0;// not push to queue.
	public static final int gl_tcp_ptq_activate = 1;// push to queue.
	
	public mm_sockaddr ss_native = new mm_sockaddr();// native end point.target value not current real.
	public mm_sockaddr ss_remote = new mm_sockaddr();// remote end point.target value not current real.
	public mm_net_tcp net_tcp = new mm_net_tcp();// real net tcp.
	public mm_mq_tcp queue_tcp = new mm_mq_tcp();// thread handle message queue.
	public mm_gl_tcp_callback callback = new mm_gl_tcp_callback();// value ref. tcp callback.
	public mm_signal_task flush_task = new mm_signal_task();// flush task for flush send message buffer.
	public mm_signal_task state_task = new mm_signal_task();// state task for check state.
	public mm_spinlock locker = new mm_spinlock();
	public int state_check_flag = gl_tcp_check_inactive;// default is gl_tcp_check_inactive.
	public int push_to_queue_flag = gl_tcp_ptq_activate;// default is gl_tcp_ptq_activate.
	//
	public int state = mm_thread.ts_closed;// mm_thread_state_t,default is ts_closed(0)
	public Object u = null;// user data.
	
	public void init()
	{
		this.ss_native.init();
		this.ss_remote.init();
		this.net_tcp.init();
		this.queue_tcp.init();
		this.callback.init();
		this.flush_task.init();
		this.state_task.init();
		this.locker.init();
		this.state_check_flag = gl_tcp_check_inactive;
		this.push_to_queue_flag = gl_tcp_ptq_activate;
		this.state = mm_thread.ts_closed;
		
		this.net_tcp.tcp.assign_callback(new __gl_tcp_tcp_callback(this));
		this.net_tcp.assign_net_tcp_callback(new __gl_tcp_net_tcp_callback(this));
		this.flush_task.assign_callback(new __gl_tcp_flush_signal_task(this));
		this.state_task.assign_callback(new __gl_tcp_state_signal_task(this));
		
		this.flush_task.assign_success_nearby(MM_GL_TCP_SEND_INTERVAL_HANDLE_TIME);
		this.flush_task.assign_failure_nearby(MM_GL_TCP_SEND_INTERVAL_BROKEN_TIME);
		this.state_task.assign_success_nearby(MM_GL_TCP_HANDLE_MSEC_CHECK);
		this.state_task.assign_failure_nearby(MM_GL_TCP_BROKEN_MSEC_CHECK);
	}
	public void destroy()
	{
		this.clear_callback_holder();
		//
		this.ss_native.destroy();
		this.ss_remote.destroy();
		this.net_tcp.destroy();
		this.queue_tcp.destroy();
		this.callback.destroy();
		this.flush_task.destroy();
		this.state_task.destroy();
		this.locker.destroy();
		this.state_check_flag = gl_tcp_check_inactive;
		this.push_to_queue_flag = gl_tcp_ptq_activate;
		this.state = mm_thread.ts_closed;
	}
	//////////////////////////////////////////////////////////////////////////
	// assign native address but not connect.
	public void assign_native(String node,int port)
	{
		this.net_tcp.assign_native(node, port);
	}
	// assign remote address but not connect.
	public void assign_remote(String node,int port)
	{
		this.net_tcp.assign_remote(node, port);
	}
	// assign native address but not connect.target value not current real.will apply at state thread.
	public void assign_native_target(String node,int port)
	{
		this.ss_native.assign(node, port);
	}
	// assign remote address but not connect.target value not current real..will apply at state thread.
	public void assign_remote_target(String node,int port)
	{
		this.ss_remote.assign(node, port);
	}
	//////////////////////////////////////////////////////////////////////////
	public void assign_q_default_callback(mm_mq_tcp.mm_mq_tcp_callback mq_tcp_callback)
	{
		this.queue_tcp.assign_queue_tcp_callback(mq_tcp_callback);
	}
	public void assign_n_default_callback(mm_gl_tcp.mm_gl_tcp_callback gl_tcp_callback)
	{
		// android assertions are unreliable.
		// assert null != gl_tcp_callback : "you can not assign null gl_tcp_callback.";
		this.callback = gl_tcp_callback;
	}
	// assign queue callback.at main thread.
	public void assign_q_callback(int id,mm_net_tcp.mm_net_tcp_handle_callback callback)
	{
		this.queue_tcp.assign_callback(id, callback);
	}
	// assign net callback.not at main thread.
	public void assign_n_callback(int id,mm_net_tcp.mm_net_tcp_handle_callback callback)
	{
		this.net_tcp.assign_callback(id, callback);
	}
	// do not assign when holder have some elem.
	public void assign_event_tcp_alloc( mm_net_tcp.mm_net_tcp_event_tcp_alloc event_tcp_alloc)
	{
		this.net_tcp.assign_event_tcp_alloc(event_tcp_alloc);
	}
	// do not assign when holder have some elem.
	public void assign_crypto_callback(mm_crypto.mm_crypto_callback crypto_callback)
	{
		this.net_tcp.assign_crypto_callback(crypto_callback);
	}
	// assign context handle.
	public void assign_context( Object u )
	{
		this.u = u;
		this.queue_tcp.assign_context(u);
		this.net_tcp.assign_context(u);
	}
	// context for tcp will use the mm_addr.u attribute.such as crypto context.
	public void set_addr_context( Object u )
	{
		this.net_tcp.set_addr_context(u);
	}
	public Object get_addr_context()
	{
		return this.net_tcp.get_addr_context();
	}
	public void clear_callback_holder()
	{
		this.net_tcp.clear_callback_holder();
		this.queue_tcp.clear_callback_holder();
	}
	//////////////////////////////////////////////////////////////////////////
	// gl_tcp crypto encrypt buffer by tcp.
	public void crypto_encrypt(mm_tcp tcp,byte[] buffer,int offset,int length)
	{
		this.net_tcp.crypto_encrypt(tcp,buffer,offset,length);
	}
	// gl_tcp crypto decrypt buffer by tcp.
	public void crypto_decrypt(mm_tcp tcp,byte[] buffer,int offset,int length)
	{
		this.net_tcp.crypto_decrypt(tcp,buffer,offset,length);
	}
	//////////////////////////////////////////////////////////////////////////
	public void assign_flush_handle_nearby(int milliseconds)
	{
		this.flush_task.assign_success_nearby(milliseconds);
	}
	public void assign_flush_broken_nearby(int milliseconds)
	{
		this.flush_task.assign_failure_nearby(milliseconds);
	}
	public void assign_state_handle_nearby(int milliseconds)
	{
		this.state_task.assign_success_nearby(milliseconds);
	}
	public void assign_state_broken_nearby(int milliseconds)
	{
		this.state_task.assign_failure_nearby(milliseconds);
	}
	// gl_tcp_check_inactive
	// gl_tcp_check_activate
	public void assign_state_check_flag(int flag)
	{
		this.state_check_flag = flag;
	}
	// gl_tcp_ptq_inactive
	// gl_tcp_ptq_activate
	public void assign_push_to_queue_flag(int flag)
	{
		this.push_to_queue_flag = flag;
	}
	//////////////////////////////////////////////////////////////////////////
	public void assign_max_poper_number(int max_pop)
	{
		this.queue_tcp.assign_max_poper_number(max_pop);
	}
	//////////////////////////////////////////////////////////////////////////
	// lock to make sure the mm_gl_tcp is thread safe.
	public void lock()
	{
		this.locker.lock();
	}
	// unlock net_tcp.
	public void unlock()
	{
		this.locker.unlock();
	}
	// lock the flush send tcp buffer.
	public void flush_lock()
	{
		this.net_tcp.tcp.lock();
	}
	// unlock the flush send tcp buffer.
	public void flush_unlock()
	{
		this.net_tcp.tcp.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	// fopen socket.
	public void fopen_socket()
	{
		this.net_tcp.fopen_socket();
	}
	// synchronize connect.can call this as we already open connect check thread.
	public void connect()
	{
		this.net_tcp.connect();
	}
	// close socket.
	public void close_socket()
	{
		this.net_tcp.close_socket();
	}
	//////////////////////////////////////////////////////////////////////////
	public void check()
	{
		this.net_tcp.check();
	}
	// get state finally.return 0 for all regular.not thread safe.
	public int finally_state()
	{
		return this.net_tcp.finally_state();
	}
	//////////////////////////////////////////////////////////////////////////
	// synchronize attach to socket.not thread safe.
	public void attach_socket()
	{
		this.net_tcp.attach_socket();
	}
	// synchronize detach to socket.not thread safe.
	public void detach_socket()
	{
		this.net_tcp.detach_socket();
	}
	// main thread trigger self thread handle.such as gl thread.
	public void thread_handle()
	{
		this.queue_tcp.thread_handle();
	}
	public void push(Object obj, mm_packet pack)
	{
		this.queue_tcp.push(obj, pack);
	}
	//////////////////////////////////////////////////////////////////////////
	// handle send data from buffer and buffer length.>0 is send size success -1 failure.
	// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
	// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
	// 0 <  rt,means rt buffer is send,we must gbump rt size.
	// 0 == rt,means the send buffer can be full.
	// 0 >  rt,means the send process can be failure.
	public int buffer_send(byte[] buffer, int offset, int length)
	{
		return this.net_tcp.buffer_send(buffer, offset, length);
	}
	// handle send data by flush send buffer.
	// 0 <  rt,means rt buffer is send,we must gbump rt size.
	// 0 == rt,means the send buffer can be full.
	// 0 >  rt,means the send process can be failure.
	public int flush_send()
	{
		return this.net_tcp.flush_send();
	}
	//////////////////////////////////////////////////////////////////////////
	// use for trigger the flush send thread signal.
	// is useful for main thread asynchronous send message.
	public void flush_signal()
	{
		this.flush_task.signal();
	}
	// use for trigger the state check thread signal.
	// is useful for main thread asynchronous state check.
	public void state_signal()
	{
		this.state_task.signal();
	}
	//////////////////////////////////////////////////////////////////////////
	// start wait thread.
	public void start()
	{
		this.state = mm_thread.ts_finish == this.state ? mm_thread.ts_closed : mm_thread.ts_motion;
		this.net_tcp.start();
		this.flush_task.start();
		this.state_task.start();
	}
	// interrupt wait thread.
	public void interrupt()
	{
		this.state = mm_thread.ts_closed;
		this.net_tcp.interrupt();
		// 
		this.queue_tcp.cond_not_null();
		this.queue_tcp.cond_not_full();
		// signal the flush thread.
		this.flush_task.interrupt();
		this.state_task.interrupt();
	}
	// shutdown wait thread.
	public void shutdown()
	{
		this.state = mm_thread.ts_finish;
		this.net_tcp.shutdown();
		// 
		this.queue_tcp.cond_not_null();
		this.queue_tcp.cond_not_full();
		// signal the flush thread.
		this.flush_task.shutdown();
		this.state_task.shutdown();
	}
	// join wait thread.
	public void join()
	{
		this.net_tcp.join();
		this.flush_task.join();
		this.state_task.join();
	}
	//////////////////////////////////////////////////////////////////////////
	static final class __gl_tcp_tcp_callback extends mm_tcp.mm_tcp_callback
	{
		__gl_tcp_tcp_callback(mm_gl_tcp _p)
		{
			this.obj = _p;
		}
		@Override
		public void handle( Object obj, byte[] buffer, int offset, int length )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			mm_gl_tcp gl_tcp = (mm_gl_tcp)(tcp.callback.obj);
			mm_net_tcp net_tcp = gl_tcp.net_tcp;
			net_tcp.crypto_decrypt(tcp,buffer,offset,length);
			mm_streambuf_packet.handle_tcp(tcp.buff_recv, buffer, offset, length, new __gl_tcp_tcp_handle_packet_callback(), tcp);
		}
		@Override
		public void broken( Object obj )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			mm_gl_tcp gl_tcp = (mm_gl_tcp)(tcp.callback.obj);
			mm_net_tcp net_tcp = gl_tcp.net_tcp;
			// android assertions are unreliable.
			// assert null != net_tcp.callback : "net_tcp.callback is a null.";
			net_tcp.apply_broken();
		}	
	}
	static final class __gl_tcp_tcp_handle_packet_callback implements mm_packet.tcp_callBack
	{
		@Override
		public void handle_tcp( Object obj, mm_packet pack ) 
		{
			mm_tcp tcp = (mm_tcp)(obj);
			mm_gl_tcp gl_tcp = (mm_gl_tcp)(tcp.callback.obj);
			mm_net_tcp net_tcp = gl_tcp.net_tcp;
			// android assertions are unreliable.
			// assert null != net_tcp.callback : "net_tcp.callback is a null.";
			net_tcp.callback.handle(tcp,net_tcp.u,pack);
		}		
	}
	static final class __gl_tcp_net_tcp_callback extends mm_net_tcp.mm_net_tcp_callback
	{
		__gl_tcp_net_tcp_callback(mm_gl_tcp _p)
		{
			this.obj = _p;
		}
		@Override
		public void handle( Object obj, Object u, mm_packet pack )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			mm_gl_tcp gl_tcp = (mm_gl_tcp)(tcp.callback.obj);
			mm_net_tcp net_tcp = gl_tcp.net_tcp;
			mm_net_tcp.mm_net_tcp_handle_callback handle = null;
			net_tcp.holder_locker.lock();
			handle = (mm_net_tcp.mm_net_tcp_handle_callback)net_tcp.holder.get(pack.phead.mid);
			net_tcp.holder_locker.unlock();
			if (null != handle)
			{
				// fire the handle event.
				handle.handle(obj, u, pack);
			}
			else
			{
				// android assertions are unreliable.
				// assert null != gl_tcp.callback : "gl_tcp.callback is a null.";
				gl_tcp.callback.handle(obj, u, pack);
			}
			// push to queue thread.
			if (gl_tcp_ptq_activate == gl_tcp.push_to_queue_flag)
			{
				gl_tcp.push(obj, pack);
			}			
		}
		@Override
		public void broken( Object obj )
		{
			mm_packet pack = new mm_packet();
			mm_tcp tcp = (mm_tcp)(obj);
			mm_gl_tcp gl_tcp = (mm_gl_tcp)(tcp.callback.obj);
			// push to queue thread.
			pack.init();
			pack.head_base_zero();
			pack.hbuff.length = mm_packet.MM_MSG_COMM_HEAD_SIZE;
			pack.bbuff.length = 0;
			pack.shadow_alloc();
			pack.phead.mid = mm_tcp.tcp_mid_broken;
			pack.head_base_encode();
			this.handle(obj, gl_tcp.u, pack);
			pack.shadow_free();
			pack.destroy();
			// net state signal.
			gl_tcp.state_signal();
		}
		@Override
		public void nready( Object obj )
		{
			mm_packet pack = new mm_packet();
			mm_tcp tcp = (mm_tcp)(obj);
			mm_gl_tcp gl_tcp = (mm_gl_tcp)(tcp.callback.obj);
			// push to queue thread.
			pack.init();
			pack.head_base_zero();
			pack.hbuff.length = mm_packet.MM_MSG_COMM_HEAD_SIZE;
			pack.bbuff.length = 0;
			pack.shadow_alloc();
			pack.phead.mid = mm_tcp.tcp_mid_nready;
			pack.head_base_encode();
			this.handle(obj, gl_tcp.u, pack);
			pack.shadow_free();
			pack.destroy();
			// net state signal.
			gl_tcp.state_signal();
		}
		@Override
		public void finish( Object obj )
		{
			mm_packet pack = new mm_packet();
			mm_tcp tcp = (mm_tcp)(obj);
			mm_gl_tcp gl_tcp = (mm_gl_tcp)(tcp.callback.obj);
			// push to queue thread.
			pack.init();
			pack.head_base_zero();
			pack.hbuff.length = mm_packet.MM_MSG_COMM_HEAD_SIZE;
			pack.bbuff.length = 0;
			pack.shadow_alloc();
			pack.phead.mid = mm_tcp.tcp_mid_finish;
			pack.head_base_encode();
			this.handle(obj, gl_tcp.u, pack);
			pack.shadow_free();
			pack.destroy();
			// net state signal.
			gl_tcp.state_signal();
		}
	}
	static final class __gl_tcp_flush_signal_task extends mm_signal_task.mm_signal_task_callback 
	{
		__gl_tcp_flush_signal_task(mm_gl_tcp _p)
		{
			this.obj = _p;
		}
		// factor for condition wait.return code for 0 is fire event -1 for condition wait.
		public int factor( Object obj) 
		{
			int code = -1;
			mm_signal_task signal_task = (mm_signal_task)(obj);
			mm_gl_tcp gl_tcp = (mm_gl_tcp)(signal_task.callback.obj);
			mm_tcp tcp = gl_tcp.net_tcp.tcp;
			int buff_len = 0;
			tcp.lock();
			buff_len = tcp.buff_send.size();
			tcp.unlock();
			code = ( 0 == buff_len ? 0 : -1 );
			return code;
		}
		// handle for fire event.return code result 0 for success -1 for failure.
		public int handle( Object obj)
		{
			int code = -1;
			mm_signal_task signal_task = (mm_signal_task)(obj);
			mm_gl_tcp gl_tcp = (mm_gl_tcp)(signal_task.callback.obj);
			mm_tcp tcp = gl_tcp.net_tcp.tcp;
			int real_len = 0;
			// check the state.
			if (0 == gl_tcp.finally_state())
			{
				// handle send data by flush send buffer.
				// 0 <  rt,means rt buffer is send,we must gbump rt size.
				// 0 == rt,means the send buffer can be full.
				// 0 >  rt,means the send process can be failure.
				tcp.lock();
				real_len = tcp.flush_send();
				tcp.unlock();
				if ( 0 > real_len )
				{
					// the socket is broken.
					// but the send thread can not broken.
					// it is wait the reconnect and send.
					code = -1;
				}
				else
				{
					// the finally state is regular.
					// if 0 == real_len.the state is broken or the socket send buffer is full.
					// just think it is regular.
					code = 0;
				}
			}
			else
			{
				// the finally state is broken.
				// wake the state check process.
				gl_tcp.state_signal();
				code = -1;
			}
			return code;
		}
    }
	static final class __gl_tcp_state_signal_task extends mm_signal_task.mm_signal_task_callback 
	{
		__gl_tcp_state_signal_task(mm_gl_tcp _p)
		{
			this.obj = _p;
		}
		// factor for condition wait.return code for 0 is fire event -1 for condition wait.
		public int factor( Object obj) 
		{
			int code = -1;
			mm_signal_task signal_task = (mm_signal_task)(obj);
			mm_gl_tcp gl_tcp = (mm_gl_tcp)(signal_task.callback.obj);
			mm_tcp tcp = gl_tcp.net_tcp.tcp;
			mm_addr addr = tcp.addr;
			code = (
					true == mm_sockaddr.sockaddr_storage_compare(addr.ss_native,gl_tcp.ss_native) && 
					true == mm_sockaddr.sockaddr_storage_compare(addr.ss_remote,gl_tcp.ss_remote) && 
					0 == gl_tcp.finally_state() ) ? 0 : -1;
			return code;
		}
		// handle for fire event.return code result 0 for success -1 for failure.
		public int handle( Object obj)
		{
			int code = -1;
			mm_signal_task signal_task = (mm_signal_task)(obj);
			mm_gl_tcp gl_tcp = (mm_gl_tcp)(signal_task.callback.obj);
			if (gl_tcp_check_activate == gl_tcp.state_check_flag)
			{
				mm_net_tcp net_tcp = gl_tcp.net_tcp;
				mm_tcp tcp = gl_tcp.net_tcp.tcp;
				mm_addr addr = tcp.addr;
				if ( 
					! mm_sockaddr.sockaddr_storage_compare(addr.ss_native,gl_tcp.ss_native) || 
					! mm_sockaddr.sockaddr_storage_compare(addr.ss_remote,gl_tcp.ss_remote) )
				{
					// if target address is not the real address,we disconnect current socket.
					gl_tcp.lock();
					// first disconnect current socket.
					net_tcp.detach_socket();
					// assign the target address.
					addr.assign_native_storage(gl_tcp.ss_native);
					addr.assign_remote_storage(gl_tcp.ss_remote);
					gl_tcp.unlock();
				}
				if ( 
						mm_sockaddr.sockaddr_storage_compare_address(addr.ss_native,mm_sockaddr.MM_ADDR_DEFAULT_NODE,mm_sockaddr.MM_ADDR_DEFAULT_PORT) && 
						mm_sockaddr.sockaddr_storage_compare_address(addr.ss_remote,mm_sockaddr.MM_ADDR_DEFAULT_NODE,mm_sockaddr.MM_ADDR_DEFAULT_PORT))
				{
					// if the native and remote is empty.fire the broken event immediately.
					gl_tcp.lock();
					net_tcp.apply_broken();
					gl_tcp.unlock();
				}
				else
				{
					gl_tcp.lock();
					gl_tcp.check();
					gl_tcp.unlock();
				}
			}
			code = ( 0 == gl_tcp.finally_state() ) ? 0 : -1;
			return code;
		}
    }
}
