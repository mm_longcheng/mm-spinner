package mm.net;

import java.nio.channels.SocketChannel;
import java.util.TreeMap;

import mm.core.mm_logger;
import mm.core.mm_socket;
import mm.core.mm_spinlock;
import mm.core.mm_thread;
import mm.core.mm_time;
import mm.poller.mm_poll;
import mm.poller.mm_poll_event;

public class mm_net_tcp
{
	public static final String TAG = mm_net_tcp.class.getSimpleName();
	// if bind listen failure will msleep this time.
	public static final int MM_NET_TCP_TRY_MSLEEP_TIME = 20;
	// net tcp nonblock timeout..
	public static final int MM_NET_TCP_NONBLOCK_TIMEOUT = 1000;
	// net tcp one time try time.
	public static final int MM_NET_TCP_TRYTIME = 3;
	
	public static final int net_tcp_state_closed = 0;// fd is closed,connect closed.invalid at all.
	public static final int net_tcp_state_motion = 1;// fd is opened,connect closed.at connecting.
	public static final int net_tcp_state_finish = 2;// fd is opened,connect opened.at connected.
	public static final int net_tcp_state_broken = 3;// fd is opened,connect broken.connect is broken fd not closed.

	static final __net_tcp_tcp_handle_packet_callback __net_tcp_tcp_handle_packet_callback_impl = new __net_tcp_tcp_handle_packet_callback();
	
	// packet handle function type.
	public static interface mm_net_tcp_handle_callback 
	{  
	    public void handle( Object obj, Object u, mm_packet pack );
	}
	public static class mm_net_tcp_callback
	{
		public void handle( Object obj, Object u, mm_packet pack ){}
		public void broken( Object obj ){}
		public void nready( Object obj ){}
		public void finish( Object obj ){}
		public Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	}
	public static class mm_net_tcp_event_tcp_alloc
	{
		public void event_tcp_alloc( Object obj, mm_tcp tcp ){}
		public void event_tcp_relax( Object obj, mm_tcp tcp ){}
		public Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	}	
	
	public mm_tcp tcp = new mm_tcp();// strong ref.
	public mm_nuclear nuclear = new mm_nuclear();// strong ref.
	public TreeMap<Integer,mm_net_tcp_handle_callback> holder = new TreeMap<Integer,mm_net_tcp_handle_callback>();// rb tree for msg_id <--> callback.
	public mm_net_tcp_callback callback = new mm_net_tcp_callback();// value ref. tcp callback.
	public mm_net_tcp_event_tcp_alloc event_tcp_alloc = new mm_net_tcp_event_tcp_alloc();
	public mm_crypto.mm_crypto_callback crypto_callback = new mm_crypto.mm_crypto_callback();// crypto callback.
	public mm_spinlock holder_locker = new mm_spinlock();
	public mm_spinlock locker = new mm_spinlock();
	
	public int nonblock_timeout = MM_NET_TCP_NONBLOCK_TIMEOUT;// tcp nonblock timeout.default is MM_NET_TCP_NONBLOCK_TIMEOUT milliseconds.

	public int tcp_state = net_tcp_state_closed;// mm_net_tcp_state_t,default is tcp_state_closed(0)

	public int trytimes = MM_NET_TCP_TRYTIME;// try bind and listen times.default is MM_NET_TCP_TRYTIME.
	public int state = mm_thread.ts_closed;// mm_thread_state_t,default is ts_closed(0)
	public Object u = null;// user data.
	
	/////////////////////////////////////////////////////////////
	public void init()
	{
		this.tcp.init();
		this.nuclear.init();
		this.holder.clear();
		this.callback.init();
		this.event_tcp_alloc.init();
		this.crypto_callback.init();
		this.holder_locker.init();
		this.locker.init();
		this.nonblock_timeout = MM_NET_TCP_NONBLOCK_TIMEOUT;
		this.tcp_state = net_tcp_state_closed;
		this.trytimes = MM_NET_TCP_TRYTIME;
		this.state = mm_thread.ts_closed;
		this.u = null;
		
		this.tcp.assign_callback(new __net_tcp_tcp_callback(this));		
		this.nuclear.assign_callback(new __net_tcp_nuclear_callback(this));
	}
	public void destroy()
	{
		this.clear_callback_holder();
		this.detach_socket();
		//
		this.tcp.destroy();
		this.nuclear.destroy();
		this.holder.clear();
		this.callback.destroy();
		this.event_tcp_alloc.destroy();
		this.crypto_callback.destroy();
		this.holder_locker.destroy();
		this.locker.destroy();
		this.nonblock_timeout = 0;
		this.tcp_state = net_tcp_state_closed;
		this.trytimes = 0;
		this.state = mm_thread.ts_closed;
		this.u = null;
	}
	/////////////////////////////////////////////////////////////
	// assign native address but not connect.
	public void assign_native(String node, int port)
	{
		this.tcp.assign_native(node, port);
	}
	// assign remote address but not connect.
	public void assign_remote(String node, int port)
	{
		this.tcp.assign_remote(node, port);
	}
	// assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
	public void assign_native_storage( mm_sockaddr ss_native )
	{
		this.tcp.assign_native_storage(ss_native);
	}
	// assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
	public void assign_remote_storage( mm_sockaddr ss_remote )
	{
		this.tcp.assign_remote_storage(ss_remote);
	}
	/////////////////////////////////////////////////////////////
	public void assign_callback(int id, mm_net_tcp_handle_callback callback)
	{
		this.holder_locker.lock();
		this.holder.put(id, callback);
		this.holder_locker.unlock();
	}
	public void assign_net_tcp_callback(mm_net_tcp_callback net_tcp_callback)
	{
		// android assertions are unreliable.
		// assert null != net_tcp_callback : "you can not assign null net_tcp_callback.";
		this.callback = net_tcp_callback;
	}
	// do not assign when holder have some elem.
	public void assign_event_tcp_alloc( mm_net_tcp_event_tcp_alloc event_tcp_alloc)
	{
		// android assertions are unreliable.
		// assert null != event_tcp_alloc : "you can not assign null event_tcp_alloc.";
		this.event_tcp_alloc = event_tcp_alloc;
	}
	// do not assign when holder have some elem.
	public void assign_crypto_callback(mm_crypto.mm_crypto_callback crypto_callback)
	{
		// android assertions are unreliable.
		// assert null != crypto_callback : "you can not assign null crypto_callback.";
		this.crypto_callback = crypto_callback;
	}
	// assign context handle.
	public void assign_context( Object u )
	{
		this.u = u;
	}
	// assign nonblock_timeout.
	public void assign_nonblock_timeout(int nonblock_timeout)
	{
		this.nonblock_timeout = nonblock_timeout;
	}
	// assign trytimes.
	public void assign_trytimes(int trytimes)
	{
		this.trytimes = trytimes;
	}
	// context for tcp will use the mm_addr.u attribute.such as crypto context.
	public void set_addr_context( Object u )
	{
		this.tcp.set_context(u);
	}
	public Object get_addr_context()
	{
		return this.tcp.get_context();
	}
	public void clear_callback_holder()
	{
		this.holder_locker.lock();
		this.holder.clear();
		this.holder_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	//net_tcp crypto encrypt buffer by tcp.
	public void crypto_encrypt(mm_tcp tcp,byte[] buffer,int offset,int length)
	{
		this.crypto_callback.encrypt(this, tcp, buffer, offset, buffer, offset, length);
	}
	//net_tcp crypto decrypt buffer by tcp.
	public void crypto_decrypt(mm_tcp tcp,byte[] buffer,int offset,int length)
	{
		this.crypto_callback.decrypt(this, tcp, buffer, offset, buffer, offset, length);
	}
	//////////////////////////////////////////////////////////////////////////
	//lock to make sure the net_tcp is thread safe.
	public void lock()
	{
		this.locker.lock();
	}
	//unlock net_tcp.
	public void unlock()
	{
		this.locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	//fopen socket.
	public void fopen_socket()
	{
		this.tcp.fopen_socket();
		this.nuclear.fd_add(this.tcp.addr.socket,this.tcp);
	}
	// synchronize connect.do not call this as we already open connect check thread.
	public void connect()
	{
		mm_addr addr = this.tcp.addr;
		if (mm_socket.MM_INVALID_SOCKET != addr.socket)
		{
			int _try_times = 0;
			int rt = -1;
			this.tcp_state = net_tcp_state_motion;
			// set socket to not block.
			addr.set_block(mm_socket.MM_NONBLOCK);
			mm_poll poll = new mm_poll();
			poll.init();
			poll.add_event(addr.socket, mm_poll_event.MM_PE_ECONNECT, this);
			do 
			{
				_try_times ++;				
				rt = addr.connect();
				if (0 == rt)
				{
					// connect immediately.
					break;
				}
				rt = poll.wait_event(this.nonblock_timeout);
				if ( 0 >= rt )
				{ 
					mm_logger.logE(TAG + " " + "connect " + addr.to_string() + " failure.try times:" + _try_times);
					mm_time.msleep(MM_NET_TCP_TRY_MSLEEP_TIME);
					rt = -1;
				}
				else
				{
					// select return > 0 means success.
					rt = 0;
				}
				if ( mm_thread.ts_finish == this.state )
				{
					break;
				}
			} while ( 0 != rt && _try_times < this.trytimes );
			poll.del_event(addr.socket, mm_poll_event.MM_PE_ECONNECT, this);
			poll.destroy();
			poll = null;
			// java special.we must call finishConnect for real finish this connect actually.
			SocketChannel _tcp_socket = (SocketChannel) addr.socket;
			boolean _connect_finish = false;
			try 
			{
				_connect_finish = _tcp_socket.finishConnect();
			} 
			catch (Exception e) 
			{
				// do nothing here.we only need the finally code.
			}
			if(false ==  _connect_finish)
			{
				rt = -1;
			}
			if ( 0 != rt )
			{
				mm_logger.logE(TAG + " " + "connect " + addr.to_string() + " failure.try times:" + _try_times);
				this.apply_broken();
				addr.close_socket();
			}
			else
			{
				this.apply_finish();
				mm_logger.logI(TAG + " " + "connect " + addr.to_string() + " success.try times:" + _try_times);
			}
		}
		else
		{
			mm_logger.logE(TAG + " " + "connect target fd is invalid.");
		}
	}
	// close socket.
	public void close_socket()
	{
		this.nuclear.fd_rmv(this.tcp.addr.socket,this.tcp);
		this.tcp.close_socket();
	}
	// shutdown socket.
	public void shutdown_socket(int opcode)
	{
		this.tcp.addr.shutdown_socket(opcode);
	}
	// set socket block.
	public void set_block(int block)
	{
		this.tcp.addr.set_block(block);
	}
	// streambuf reset.
	public void streambuf_reset()
	{
		this.tcp.streambuf_reset();
	}
	// check connect and reconnect if disconnect.
	public void check()
	{
		if ( 0 != this.finally_state() )
		{
			// we first disconnect this old connect.
			this.detach_socket();
			// fopen a socket.
			this.fopen_socket();
			// net tcp connect.
			this.connect();
		}
	}
	// get state finally.return 0 for all regular.not thread safe.
	public int finally_state()
	{
		return ( mm_socket.MM_INVALID_SOCKET != this.tcp.addr.socket && net_tcp_state_finish == this.tcp_state ) ? 0 : -1;
	}
	// synchronize attach to socket.not thread safe.
	public void attach_socket()
	{
		this.connect();
	}
	// synchronize detach to socket.not thread safe.
	public void detach_socket()
	{
		mm_tcp tcp = this.tcp;
		if ( mm_socket.MM_INVALID_SOCKET != tcp.addr.socket)
		{
			// set flag value.
			this.tcp_state = net_tcp_state_closed;
			// set block state.
			this.set_block(mm_socket.MM_BLOCKING);
			// shutdown socket first.
			this.shutdown_socket(mm_socket.MM_BOTH_SHUTDOWN);
			// close socket.
			this.close_socket();
			// buffer is ivalid.
			this.streambuf_reset();
		}
	}
	//////////////////////////////////////////////////////////////////////////
	public void apply_broken()
	{
		this.tcp_state = net_tcp_state_closed;
		this.tcp.streambuf_reset();
		this.callback.broken(this.tcp);
	}
	public void apply_finish()
	{
		this.tcp_state = net_tcp_state_finish;
		this.tcp.streambuf_reset();
		this.callback.finish(this.tcp);		
	}
	//////////////////////////////////////////////////////////////////////////
	//handle send data from buffer and buffer length.>0 is send size success -1 failure.
	//note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
	//only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
	//0 <  rt,means rt buffer is send,we must gbump rt size.
	//0 == rt,means the send buffer can be full.
	//0 >  rt,means the send process can be failure.
	public int buffer_send(byte[] buffer, int offset, int length)
	{
		int rt = -1;
		if ( net_tcp_state_finish == this.tcp_state && mm_socket.MM_INVALID_SOCKET != this.tcp.addr.socket )
		{
			rt = this.tcp.buffer_send(buffer,offset,length);
			// update the tcp state,because the mm_tcp_buffer_send can not fire broken event immediately.
			this.tcp_state = -1 == rt ? net_tcp_state_broken : this.tcp_state;
		}
		else
		{
			// net tcp nready.fire event.
			this.callback.nready(this.tcp);
		}
		return rt;
	}
	// handle send data by flush send buffer.
	// 0 <  rt,means rt buffer is send,we must gbump rt size.
	// 0 == rt,means the send buffer can be full.
	// 0 >  rt,means the send process can be failure.
	public int flush_send()
	{
		int rt = -1;
		if ( net_tcp_state_finish == this.tcp_state && mm_socket.MM_INVALID_SOCKET != this.tcp.addr.socket )
		{
			rt = this.tcp.flush_send();
			// update the tcp state,because the mm_tcp_buffer_send can not fire broken event immediately.
			this.tcp_state = -1 == rt ? net_tcp_state_broken : this.tcp_state;
		}
		else
		{
			// net tcp nready.fire event.
			this.callback.nready(this.tcp);
		}
		return rt;
	}
	//////////////////////////////////////////////////////////////////////////
	public void start()
	{
		this.state = mm_thread.ts_finish == this.state ? mm_thread.ts_closed : mm_thread.ts_motion;
		this.nuclear.start();
		this.event_tcp_alloc.event_tcp_alloc(this, this.tcp);
	}
	public void interrupt()
	{
		this.state = mm_thread.ts_closed;
		this.nuclear.interrupt();
	}
	public void shutdown()
	{
		this.state = mm_thread.ts_finish;
		this.nuclear.shutdown();
	}
	public void join()
	{
		this.nuclear.join();
		this.event_tcp_alloc.event_tcp_relax(this, this.tcp);
	}
	/////////////////////////////////////////////////////////////
	static final class __net_tcp_tcp_callback extends mm_tcp.mm_tcp_callback
	{
		__net_tcp_tcp_callback(mm_net_tcp _p)
		{
			this.obj = _p;
		}
		@Override
		public void handle( Object obj, byte[] buffer, int offset, int length )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			mm_net_tcp net_tcp = (mm_net_tcp)(tcp.callback.obj);
			net_tcp.crypto_decrypt(tcp,buffer,offset,length);
			mm_streambuf_packet.handle_tcp(tcp.buff_recv, buffer, offset, length, __net_tcp_tcp_handle_packet_callback_impl, tcp);
		}
		@Override
		public void broken( Object obj )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			mm_net_tcp net_tcp = (mm_net_tcp)(tcp.callback.obj);
			// android assertions are unreliable.
			// assert null != net_tcp.callback : "net_tcp.callback is a null.";
			net_tcp.apply_broken();
		}	
	};
	static final class __net_tcp_nuclear_callback extends mm_nuclear.mm_nuclear_callback
	{
		__net_tcp_nuclear_callback(mm_net_tcp _p)
		{
			this.obj = _p;
		}
		@Override
		public void handle_recv( Object obj, Object u, byte[] buffer, int offset, int max_length )
		{
			mm_tcp tcp = (mm_tcp)(u);
			tcp.handle_recv(buffer,offset,max_length);	
		}
		@Override
		public void handle_send( Object obj, Object u, byte[] buffer, int offset, int max_length )
		{
			mm_tcp tcp = (mm_tcp)(u);
			tcp.handle_send(buffer,offset,max_length);
		}
	};
	static final class __net_tcp_tcp_handle_packet_callback implements mm_packet.tcp_callBack
	{
		@Override
		public void handle_tcp( Object obj, mm_packet pack ) 
		{
			mm_tcp tcp = (mm_tcp)(obj);
			mm_net_tcp net_tcp = (mm_net_tcp)(tcp.callback.obj);
			mm_net_tcp_handle_callback handle = null;
			net_tcp.holder_locker.lock();
			handle = net_tcp.holder.get(pack.phead.mid);
			net_tcp.holder_locker.unlock();
			if (null != handle)
			{
				// fire the handle event.
				handle.handle(tcp, net_tcp.u, pack);
			}
			else
			{
				// android assertions are unreliable.
				// assert null != net_tcp.callback : "net_tcp.callback is a null.";
				net_tcp.callback.handle(tcp, net_tcp.u, pack);
			}
		}		
	}
};
