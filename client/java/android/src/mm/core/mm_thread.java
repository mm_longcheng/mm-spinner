package mm.core;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

//we catch all Exception e but print nothing but catch the error code.

// pthread behaver.
public class mm_thread
{
	public ReentrantLock cond_locker = new ReentrantLock();
	public Condition cond_not_null = cond_locker.newCondition();
	
	public static class mutex
	{
		public ReentrantLock impl = new ReentrantLock();
		public void init()
		{

		}
		public void destroy()
		{

		}
		public void lock()
		{
			this.impl.lock();
		}
		public void unlock()
		{
			this.impl.unlock();
		}
	};
	public static class cond
	{
		public Condition impl = null;
		public cond(mutex m)
		{
			this.impl = m.impl.newCondition();
		}
		public void init()
		{
			
		}
		public void destroy()
		{
			
		}
		public void signal()
		{
			this.impl.signal();
		}
		public void await()
		{
			try 
			{
				this.impl.await();
			} 
			catch (Exception e1) 
			{
				// do nothing here.we only need the finally code.
			}
		}
		public void timedwait(long milliseconds)
		{
			try 
			{
				this.impl.await(milliseconds, TimeUnit.MILLISECONDS);
			} 
			catch (Exception e1) 
			{
				// do nothing here.we only need the finally code.
			}
		}
	};
	
	// thread have some interface.
	//  init         state = ts_closed
	//  start        state = ts_finish == state ? ts_closed : ts_motion;
	//  interrupt    state = ts_closed
	//  shutdown     state = ts_finish
	//  join         state = state
	//  destroy      state = ts_closed
	public static final int ts_closed = 0;// thread not start or be closed or be interrupt.
	public static final int ts_motion = 1;// thread is running.
	public static final int ts_finish = 2;// application is termination and can not restart.
	
	public Thread work_thread = null;
	
	public void init()
	{
		this.work_thread = null;
	}
	public void destroy()
	{
		this.work_thread = null;
	}
	public void start(Runnable runnable)
	{
		this.work_thread = new Thread(runnable);
		this.work_thread.start();
	}
	public void join()
	{
		try 
		{
			this.work_thread.join();
		} 
		catch (Exception e) 
		{
			// do nothing here.we only need the finally code.
		}
	}
}
