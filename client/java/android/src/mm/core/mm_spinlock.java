package mm.core;

import java.util.concurrent.atomic.AtomicReference;

public class mm_spinlock
{
	public AtomicReference<Thread> atomic = new AtomicReference<Thread>();
	
	public void init()
	{
		// here nothing for init.
	}
	public void destroy()
	{
		// here nothing for destroy.
	}
    public void lock()
    {
        Thread current = Thread.currentThread();
        while(!atomic.compareAndSet(null, current))
        {
        	// busy-wait-loop.
        }
    }
    public void unlock()
    {
        Thread current = Thread.currentThread();
        atomic.compareAndSet(current, null);
    }
}
