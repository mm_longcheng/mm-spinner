package mm.core;

import mm.core.mm_thread;
import mm.core.mm_time;

public class mm_signal_task
{
	// signal task default success nearby time milliseconds.
	public static final int MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC = 0;
	// signal task default failure nearby time milliseconds.
	public static final int MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC = 200;
	
	public static class mm_signal_task_callback
	{
		// factor for condition wait.return code for 0 is fire event -1 for condition wait.
		public int factor( Object obj) 
		{
			return -1;
		}
		// handle for fire event.return code result 0 for success -1 for failure.
		public int handle( Object obj)
		{
			return 0;
		}
		public Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	}
	
	public mm_signal_task_callback callback = new mm_signal_task_callback();
	public mm_thread signal_thread = new mm_thread();	
	public mm_thread.mutex signal_locker = new mm_thread.mutex();
	public mm_thread.cond signal_cond = new mm_thread.cond(this.signal_locker);	
	public int msec_success_nearby = MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC;// nearby_time milliseconds for each time wait.default is MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC ms.
	public int msec_failure_nearby = MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC;// nearby_time milliseconds for each time wait.default is MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC ms.
	public int msec_delay = 0;// delay milliseconds before first callback.default is 0 ms.
	public int state = mm_thread.ts_closed;// mm_thread_state_t,default is ts_closed(0)

	public void init()
	{
		this.callback.init();
		this.signal_thread.init();
		this.signal_locker.init();
		this.signal_cond.init();
		this.msec_success_nearby = MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC;
		this.msec_failure_nearby = MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC;
		this.msec_delay = 0;
		this.state = mm_thread.ts_closed;
	}
	public void destroy()
	{
		this.callback.destroy();
		this.signal_thread.destroy();
		this.signal_locker.destroy();
		this.signal_cond.destroy();
		this.msec_success_nearby = MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC;
		this.msec_failure_nearby = MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC;
		this.msec_delay = 0;
		this.state = mm_thread.ts_closed;
	}
	//////////////////////////////////////////////////////////////////////////
	// lock to make sure the knot is thread safe.
	public void lock()
	{
		this.signal_locker.lock();
	}
	// unlock knot.
	public void unlock()
	{
		this.signal_locker.unlock();
	}
	public void assign_callback(mm_signal_task_callback callback)
	{
		// android assertions are unreliable.
		// assert null != callback : "you can not assign null callback.";
		this.callback = callback;
	}
	public void assign_success_nearby(int nearby)
	{
		this.msec_success_nearby = nearby;
	}
	public void assign_failure_nearby(int nearby)
	{
		this.msec_failure_nearby = nearby;
	}
	//////////////////////////////////////////////////////////////////////////
	// wait.
	public void signal_wait()
	{
		int code = -1;
		int _nearby_time = 0;
		// android assertions are unreliable.
		// assert null != this.callback : "this.callback is a null.";
		if ( 0 < this.msec_delay )
		{
			mm_time.msleep(this.msec_delay);
		}
		while( mm_thread.ts_motion == this.state )
		{
			// factor
			code = this.callback.factor(this);
			if (0 == code)
			{
				this.signal_locker.lock();
				this.signal_cond.await();
				this.signal_locker.unlock();
				// the condition is timeout invalid.we enter a new process.
				// we must make sure the code == 0.
				continue;
			}
			// fire event.
			code = this.callback.handle(this);
			_nearby_time = 0 == code ? this.msec_success_nearby : this.msec_failure_nearby;
			// some os pthread timedwait impl precision is low.
			// MM_MSEC_PER_SEC check can avoid some precision problem
			// but will extend the application shutdown time.
			if ( 0 == _nearby_time )
			{
				// next loop immediately.
				continue;
			}
			else if( mm_time.MM_MSEC_PER_SEC < _nearby_time )
			{
				// timedwait a while.
				this.signal_locker.lock();
				this.signal_cond.timedwait(_nearby_time);
				this.signal_locker.unlock();
			}
			else
			{
				// msleep a while.
				mm_time.msleep(_nearby_time);
			}
		}
	}
	// signal.
	public void signal()
	{
		this.signal_locker.lock();
		this.signal_cond.signal();
		this.signal_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	// start wait thread.
	public void start()
	{
		this.state = mm_thread.ts_finish == this.state ? mm_thread.ts_closed : mm_thread.ts_motion;
		this.signal_thread.start(new __static_signal_task_poll_wait_thread(this));
	}
	// interrupt wait thread.
	public void interrupt()
	{
		this.state = mm_thread.ts_closed;
		this.signal();
	}
	// shutdown wait thread.
	public void shutdown()
	{
		this.state = mm_thread.ts_finish;
		this.signal();
	}
	// join wait thread.
	public void join()
	{
		this.signal_thread.join();
	}
	//////////////////////////////////////////////////////////////////////////
	static final class __static_signal_task_poll_wait_thread implements Runnable
	{
		__static_signal_task_poll_wait_thread(mm_signal_task _p)
		{
			this.p = _p;
		}
		public mm_signal_task p = null;
		@Override
		public void run() 
		{
			p.signal_wait();
		}		
	}
}
