package mm.poller;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import mm.core.mm_errno;
import mm.core.mm_logger;
import mm.core.mm_socket;

//we catch all Exception e but print nothing but catch the error code.
public class mm_poll
{
	// java poll event.
	public static final int OP_READABLE = SelectionKey.OP_READ;
	public static final int OP_WRITABLE = SelectionKey.OP_WRITE;
	// java special.
	public static final int OP_ECONNECT = SelectionKey.OP_CONNECT;
	public static final int OP_ACCEPTOR = SelectionKey.OP_ACCEPT;
	
	public Vector<mm_poll_event> pe = new Vector<mm_poll_event>();
	public Selector impl = null;

	public void init()
	{
		this.pe.clear();
		try 
		{
			this.impl = Selector.open();
		} 
		catch (IOException e) 
		{
			// real Exception.
			mm_logger.logE("exception: " + e.toString());
		}
	}
	public void destroy()
	{
		this.pe.clear();
		if(null != this.impl)
		{
			try 
			{
				this.impl.close();
			} 
			catch (IOException e) 
			{
				// real Exception.
				mm_logger.logE("exception: " + e.toString());
			}
			this.impl = null;
		}
	}
	public int add_event(AbstractSelectableChannel fd, int mask, Object ud)
	{
		if(mm_socket.MM_INVALID_SOCKET == fd)
		{
			// socket is a invalid.
			return -1;
		}
		int rt = 0;
		int events = 0;
		if (0 != (mask & mm_poll_event.MM_PE_READABLE)) events |= OP_READABLE;
		if (0 != (mask & mm_poll_event.MM_PE_WRITABLE)) events |= OP_WRITABLE;
		if (0 != (mask & mm_poll_event.MM_PE_ECONNECT)) events |= OP_ECONNECT;
		if (0 != (mask & mm_poll_event.MM_PE_ACCEPTOR)) events |= OP_ACCEPTOR;
		try 
		{
			fd.register(this.impl, events, ud);
		} 
		catch (ClosedChannelException e) 
		{
			// do nothing here.we only need the finally code.
			rt = -1;
		}
		return rt;
	}
	public int mod_event(AbstractSelectableChannel fd, int mask, Object ud)
	{
		if(mm_socket.MM_INVALID_SOCKET == fd)
		{
			// socket is a invalid.
			return -1;
		}
		int rt = 0;
		int events = 0;
		if (0 != (mask & mm_poll_event.MM_PE_READABLE)) events |= OP_READABLE;
		if (0 != (mask & mm_poll_event.MM_PE_WRITABLE)) events |= OP_WRITABLE;
		if (0 != (mask & mm_poll_event.MM_PE_ECONNECT)) events |= OP_ECONNECT;
		if (0 != (mask & mm_poll_event.MM_PE_ACCEPTOR)) events |= OP_ACCEPTOR;
		// mod current poll event.
		SelectionKey sk = fd.keyFor(this.impl);
		if(null != sk)
		{
			sk.interestOps(events);
		}
		else
		{
			rt = -1;
		}
		return rt;
	}
	public int del_event(AbstractSelectableChannel fd, int mask, Object ud)
	{
		if(mm_socket.MM_INVALID_SOCKET == fd)
		{
			// socket is a invalid.
			return -1;
		}
		int rt = 0;
		int events = 0;		
		if (0 != (mask & mm_poll_event.MM_PE_READABLE)) events |= OP_READABLE;
		if (0 != (mask & mm_poll_event.MM_PE_WRITABLE)) events |= OP_WRITABLE;
		if (0 != (mask & mm_poll_event.MM_PE_ECONNECT)) events |= OP_ECONNECT;
		if (0 != (mask & mm_poll_event.MM_PE_ACCEPTOR)) events |= OP_ACCEPTOR;
		// del current poll event.
		SelectionKey sk = fd.keyFor(this.impl);
		if(null != sk)
		{
			int evt = sk.interestOps();
			sk.interestOps(evt & (~events));
		}
		else
		{
			rt = -1;
		}
		return rt;
	}
	public int wait_event(int milliseconds)
	{
		int n = 0;
		int i = 0;
		do
		{
			try 
			{
				n = this.impl.select(milliseconds);
				mm_errno.set_code(0);
			}
			catch (ClosedSelectorException e) 
			{
				// do nothing here.we only need the finally code.
				n = -1;
				mm_errno.set_code(mm_errno.MM_ENETDOWN);
				break;
			}
			catch (IllegalArgumentException e) 
			{
				// do nothing here.we only need the finally code.
				n = -1;
				mm_errno.set_code(mm_errno.MM_EINVAL);
				break;
			}
			catch (IOException e) 
			{
				// do nothing here.we only need the finally code.
				n = -1;
				mm_errno.set_code(mm_errno.MM_EIO);
				break;
			}
			catch (Exception e) 
			{
				// real Exception.
				mm_logger.logE("exception: " + e.toString());
				n = -1;
				mm_errno.set_code(mm_errno.MM_EINVAL);
			}
			// length is zero or select occur error.break here.
			if( 0 == n || -1 == n )
			{
				// we have none event.
				break;
			}
			if(null ==  this.impl)
			{
				// we have none event.
				n = -1;
				break;
			}
			this.pe.setSize(n);
			Set<SelectionKey> pe = this.impl.selectedKeys();
			Iterator<SelectionKey> it = pe.iterator();
			while (it.hasNext())
			{
				SelectionKey ee = it.next();
				int events = ee.interestOps();
				int mask = 0;
				//
				if (0 != (events & OP_READABLE))  mask |= mm_poll_event.MM_PE_READABLE;
				if (0 != (events & OP_WRITABLE))  mask |= mm_poll_event.MM_PE_WRITABLE;
				if (0 != (events & OP_ECONNECT))  mask |= mm_poll_event.MM_PE_ECONNECT;
				if (0 != (events & OP_ACCEPTOR))  mask |= mm_poll_event.MM_PE_ACCEPTOR;
				//
				mm_poll_event elem = new mm_poll_event();
				elem.s = ee.attachment();
				elem.mask = mask;
				this.pe.set(i, elem);
				++i;
				//
				it.remove();
			}
		}while(false);
		return n;
	}	
	public String api()
	{
		return "java.nio.channels.Selector";
	}
}
