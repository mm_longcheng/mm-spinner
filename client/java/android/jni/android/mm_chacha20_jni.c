// #include <jni.h>
// #include <android/log.h>

// #include "security/mm_chacha20.h"

// #ifndef LOGGER_ALL_MSG
// #define LOGGER_ALL_MSG 1
// #endif//LOGGER_ALL_MSG
// //
// #include <android/log.h>
// #define LOGSI(_section,...) ((void)__android_log_print(ANDROID_LOG_INFO, (_section), __VA_ARGS__))
// #define LOGSW(_section,...) ((void)__android_log_print(ANDROID_LOG_WARN, (_section), __VA_ARGS__))
// #define LOGSE(_section,...) ((void)__android_log_print(ANDROID_LOG_ERROR, (_section), __VA_ARGS__))

// #if defined( NDEBUG ) || ( LOGGER_ALL_MSG == 1 )
// #  define LOGSV(_section,...)  ((void)__android_log_print(ANDROID_LOG_VERBOSE, (_section), __VA_ARGS__))
// #else
// #  define LOGSV(_section,...)  ((void)0)
// #endif

// // mm.security.mm_chacha20
// // public native static void jni_encrypt(int[] m, byte[] i, byte[] o, int l);
// JNIEXPORT void JNICALL Java_mm_security_mm_chacha20_jni_encrypt(JNIEnv* env, jobject obj, jintArray jm,jbyteArray ji,jbyteArray jo,jint jl)
// {
// 	LOGSI("mm","%d %s %s %d",7,"I",__FUNCTION__,__LINE__);

// 	u32*  m = ( u32*)((*env)->GetIntArrayElements(env, jm, 0));
// 	byte* i = (byte*)((*env)->GetByteArrayElements(env, ji, 0));
// 	byte* o = (byte*)((*env)->GetByteArrayElements(env, jo, 0));
// 	u32 bytes = jl;
// 	//
// 	chacha_encrypt_bytes(m, i, o, bytes);
// }
// // public native static void jni_decrypt(int[] m, byte[] i, byte[] o, int l);
// JNIEXPORT void JNICALL Java_mm_security_mm_chacha20_jni_decrypt(JNIEnv* env, jobject obj, jintArray jm,jbyteArray ji,jbyteArray jo,jint jl)
// {
// 	LOGSI("mm","%d %s %s %d",7,"I",__FUNCTION__,__LINE__);

// 	u32*  m = ( u32*)((*env)->GetIntArrayElements(env, jm, 0));
// 	byte* i = (byte*)((*env)->GetByteArrayElements(env, ji, 0));
// 	byte* o = (byte*)((*env)->GetByteArrayElements(env, jo, 0));
// 	u32 bytes = jl;
// 	//
// 	chacha_encrypt_bytes(m, i, o, bytes);
// }
