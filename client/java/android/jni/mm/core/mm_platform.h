#ifndef __mm_platform_h__
#define __mm_platform_h__

//come form MyGUI Platform.
//come form Ogre  Platform.
//////////////////////////////////////////////////////////////////////////
/* Initial platform/compiler-related stuff to set.
*/
#define MM_PLATFORM_WIN32 1
#define MM_PLATFORM_LINUX 2
#define MM_PLATFORM_APPLE 3
#define MM_PLATFORM_APPLE_IOS 4
#define MM_PLATFORM_ANDROID 5
#define MM_PLATFORM_NACL 6
#define MM_PLATFORM_WINRT 7
#define MM_PLATFORM_FLASHCC 8
#define MM_PLATFORM_FREEBSD 9

#define MM_COMPILER_MSVC 1
#define MM_COMPILER_GNUC 2
#define MM_COMPILER_BORL 3
#define MM_COMPILER_WINSCW 4
#define MM_COMPILER_GCCE 5
#define MM_COMPILER_CLANG 6

#define MM_ENDIAN_LITTLE 1
#define MM_ENDIAN_BIG 2

#define MM_ARCHITECTURE_32 1
#define MM_ARCHITECTURE_64 2

/* Finds the compiler type and version.
*/
#if (defined( __WIN32__ ) || defined( _WIN32 )) && defined(__ANDROID__) // We are using NVTegra
#   define MM_COMPILER MM_COMPILER_GNUC
#   define MM_COMP_VER 470
#elif defined( __GCCE__ )
#   define MM_COMPILER MM_COMPILER_GCCE
#   define MM_COMP_VER _MSC_VER
//# include <staticlibinit_gcce.h> // This is a GCCE toolchain workaround needed when compiling with GCCE 
#elif defined( __WINSCW__ )
#   define MM_COMPILER MM_COMPILER_WINSCW
#   define MM_COMP_VER _MSC_VER
#elif defined( _MSC_VER )
#   define MM_COMPILER MM_COMPILER_MSVC
#   define MM_COMP_VER _MSC_VER
#elif defined( __clang__ )
#   define MM_COMPILER MM_COMPILER_CLANG
#   define MM_COMP_VER (((__clang_major__)*100) + \
	(__clang_minor__*10) + \
	__clang_patchlevel__)
#elif defined( __GNUC__ )
#   define MM_COMPILER MM_COMPILER_GNUC
#   define MM_COMP_VER (((__GNUC__)*100) + \
	(__GNUC_MINOR__*10) + \
	__GNUC_PATCHLEVEL__)
#elif defined( __BORLANDC__ )
#   define MM_COMPILER MM_COMPILER_BORL
#   define MM_COMP_VER __BCPLUSPLUS__
#   define __FUNCTION__ __FUNC__ 
#else
#   pragma error "No known compiler. Abort! Abort!"
#endif

/* define NORETURN macro */
#if MM_COMPILER == MM_COMPILER_MSVC
#	define MM_NORETURN __declspec(noreturn)
#elif MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG
#	define MM_NORETURN __attribute__((noreturn))
#else
#	define MM_NORETURN
#endif

/* See if we can use __forceinline or if we need to use __inline instead */
#if MM_COMPILER == MM_COMPILER_MSVC
#   if MM_COMP_VER >= 1200
#       define FORCEINLINE __forceinline
#   endif
#elif defined(__MINGW32__)
#   if !defined(FORCEINLINE)
#       define FORCEINLINE __inline
#   endif
#elif !defined(ANDROID) && (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG)
#   define FORCEINLINE inline __attribute__((always_inline))
#else
#   define FORCEINLINE __inline
#endif

#if defined __DragonFly__ && !defined __FreeBSD__
#define __FreeBSD__        4
#define __FreeBSD_version  480101
#endif

/* Finds the current platform */
#if (defined( __WIN32__ ) || defined( _WIN32 )) && !defined(__ANDROID__)
#   if defined(WINAPI_FAMILY)
#       define __MM_HAVE_DIRECTXMATH 1
#       include <winapifamily.h>
#       if WINAPI_FAMILY == WINAPI_FAMILY_APP|| WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP
#           define DESKTOP_APP 1
#           define PHONE 2
#           define MM_PLATFORM MM_PLATFORM_WINRT
#           ifndef _CRT_SECURE_NO_WARNINGS
#               define _CRT_SECURE_NO_WARNINGS
#           endif
#           ifndef _SCL_SECURE_NO_WARNINGS
#               define _SCL_SECURE_NO_WARNINGS
#           endif
#           if WINAPI_FAMILY == WINAPI_FAMILY_APP
#               define MM_WINRT_TARGET_TYPE DESKTOP_APP
#           endif
#           if WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP
#               define MM_WINRT_TARGET_TYPE PHONE
#           endif
#       else
#           define MM_PLATFORM MM_PLATFORM_WIN32
#       endif
#   else
#       define MM_PLATFORM MM_PLATFORM_WIN32
#   endif
#elif defined(__FLASHCC__)
#   define MM_PLATFORM MM_PLATFORM_FLASHCC
#elif defined( __APPLE_CC__)
// Device                                                     Simulator
// Both requiring OS version 6.0 or greater
#   if __ENVIRONMENT_IPHONE_OS_VERSION_MIN_REQUIRED__ >= 60000 || __IPHONE_OS_VERSION_MIN_REQUIRED >= 60000
#       define MM_PLATFORM MM_PLATFORM_APPLE_IOS
#   else
#       define MM_PLATFORM MM_PLATFORM_APPLE
#   endif
#elif defined(__ANDROID__)
#   define MM_PLATFORM MM_PLATFORM_ANDROID
#elif defined( __native_client__ ) 
#   define MM_PLATFORM MM_PLATFORM_NACL
#   ifndef MM_STATIC_LIB
#       error MM must be built as static for NaCl (MM_STATIC=true in CMake)
#   endif
#   ifdef MM_BUILD_RENDERSYSTEM_D3D9
#       error D3D9 is not supported on NaCl (MM_BUILD_RENDERSYSTEM_D3D9 false in CMake)
#   endif
#   ifdef MM_BUILD_RENDERSYSTEM_GL
#       error OpenGL is not supported on NaCl (MM_BUILD_RENDERSYSTEM_GL=false in CMake)
#   endif
#   ifndef MM_BUILD_RENDERSYSTEM_GLES2
#       error GLES2 render system is required for NaCl (MM_BUILD_RENDERSYSTEM_GLES2=false in CMake)
#   endif
#elif defined( __FreeBSD__ ) 
#	define MM_PLATFORM MM_PLATFORM_FREEBSD
#else
#   define MM_PLATFORM MM_PLATFORM_LINUX
#endif

// Check whether is Linux family.
#if MM_PLATFORM == MM_PLATFORM_LINUX || \
	MM_PLATFORM == MM_PLATFORM_APPLE || \
	MM_PLATFORM == MM_PLATFORM_APPLE_IOS || \
	MM_PLATFORM == MM_PLATFORM_ANDROID || \
	MM_PLATFORM == MM_PLATFORM_FREEBSD
#	define MM_UNIX_FAMILY
#endif

/* Find the arch type */
#if defined(__x86_64__) || defined(_M_X64) || defined(__powerpc64__) || defined(__alpha__) || defined(__ia64__) || defined(__s390__) || defined(__s390x__) || defined(__arm64__) || defined(__aarch64__) || defined(__mips64) || defined(__mips64_)
#   define MM_ARCH_TYPE MM_ARCHITECTURE_64
#else
#   define MM_ARCH_TYPE MM_ARCHITECTURE_32
#endif

// For generating compiler warnings - should work on any compiler
// As a side note, if you start your message with 'Warning: ', the MSVC
// IDE actually does catch a warning :)
#define MM_QUOTE_INPLACE(x) # x
#define MM_QUOTE(x) MM_QUOTE_INPLACE(x)
#define MM_WARN( x )  message( __FILE__ "(" QUOTE( __LINE__ ) ") : " x "\n" )

// For marking functions as deprecated
#if MM_COMPILER == MM_COMPILER_MSVC
#   define MM_DEPRECATED __declspec(deprecated)
#elif MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG
#   define MM_DEPRECATED __attribute__ ((deprecated))
#else
#   pragma message("WARNING: You need to implement MM_DEPRECATED for this compiler")
#   define MM_DEPRECATED
#endif

// Disable MM_WCHAR_T_STRINGS until we figure out what to do about it.
#define MM_WCHAR_T_STRINGS 0

// Windows settings
#if MM_PLATFORM == MM_PLATFORM_WIN32
#
#	if defined( MM_STATIC )
#		define MM_EXPORT
#	elif defined( MM_BUILD )
#		define MM_EXPORT __declspec( dllexport )
#	else
#		if defined( __MINGW32__ )
#			define MM_EXPORT
#		else
#			define MM_EXPORT __declspec( dllimport )
#		endif
#	endif
#
#	if defined( MM_STATIC )
#		define MM_EXPORT_DLL
#	elif defined( MM_BUILD_DLL )
#		define MM_EXPORT_DLL __declspec( dllexport )
#	else
#		if defined( __MINGW32__ )
#			define MM_EXPORT_DLL
#		else
#			define MM_EXPORT_DLL __declspec( dllimport )
#		endif
#	endif
#
#// Win32 compilers use _DEBUG for specifying debug builds.
#	ifdef _DEBUG
#		define MM_DEBUG_MODE 1
#	else
#		define MM_DEBUG_MODE 0
#	endif
#endif

// Linux/Apple Settings
#if defined( MM_UNIX_FAMILY )
#
// Add -fvisibility=hidden to compiler options. With -fvisibility=hidden, you are telling
// GCC that every declaration not explicitly marked with a visibility attribute (MM_EXPORT)
// has a hidden visibility (like in windows).
#	if __GNUC__ >= 4
#		define MM_EXPORT  __attribute__ ((visibility("default")))
#	else
#		define MM_EXPORT
#	endif
#
#	if __GNUC__ >= 4
#		define MM_EXPORT_DLL  __attribute__ ((visibility("default")))
#	else
#		define MM_EXPORT_DLL
#	endif
#
// Unlike the Win32 compilers, Linux compilers seem to use DEBUG for when
// specifying a debug build.
// (??? this is wrong, on Linux debug builds aren't marked in any way unless
// you mark it yourself any way you like it -- zap ???)
#	ifdef DEBUG
#		define MM_DEBUG_MODE 1
#	else
#		define MM_DEBUG_MODE 0
#	endif

#	if MM_PLATFORM == MM_PLATFORM_APPLE
#		define MM_PLATFORM_LIB "MMPlatform.bundle"
#	else // if MM_PLATFORM_LINUX
#		define MM_PLATFORM_LIB "libMMPlatform.so"
#	endif

#endif

/* See if we can use __forceinline or if we need to use __inline instead */
#if MM_COMPILER == MM_COMPILER_MSVC
#   if MM_COMP_VER >= 1200
#       define MM_FORCEINLINE __forceinline
#   endif
#elif defined(__MINGW32__)
#   if !defined(MM_FORCEINLINE)
#       define MM_FORCEINLINE __inline
#   endif
#elif !defined(ANDROID) && (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG)
#   define MM_FORCEINLINE inline __attribute__((always_inline))
#else
#   define MM_FORCEINLINE __inline
#endif
//----------------------------------------------------------------------------
// Endian Settings
// check for BIG_ENDIAN config flag, set MM_ENDIAN correctly
#ifdef MM_CONFIG_BIG_ENDIAN
#    define MM_ENDIAN MM_ENDIAN_BIG
#else
#    define MM_ENDIAN MM_ENDIAN_LITTLE
#endif

//----------------------------------------------------------------------------
// Library suffixes
// "_d" for debug builds, nothing otherwise
#if MM_DEBUG_MODE
#   define MM_BUILD_SUFFIX "_d"
#else
#   define MM_BUILD_SUFFIX ""
#endif

// Disable these warnings (too much noise)
#if MM_COMPILER == MM_COMPILER_MSVC
#	ifndef _CRT_SECURE_NO_WARNINGS
#		define _CRT_SECURE_NO_WARNINGS
#	endif
#	ifndef _SCL_SECURE_NO_WARNINGS
#	   define _SCL_SECURE_NO_WARNINGS
#	endif

// disable: "<type> needs to have dll-interface to be used by clients'
// Happens on STL member variables which are not public therefore is ok
#   pragma warning (disable : 4251)
	
#endif

#define MM_INLINE MM_FORCEINLINE

#endif//__mm_platform_h__
