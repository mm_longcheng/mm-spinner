#ifndef __mm_chacha20_h__
#define __mm_chacha20_h__

#include "core/mm_prefix.h"
#include "core/mm_platform.h"

#include <stdint.h>

typedef uint32_t u32 ;
typedef unsigned char byte ;

MM_EXPORT_DLL void chacha20_wordtobyte(byte output[64],const u32 input[16]);
MM_EXPORT_DLL void chacha_keysetup(u32 *x,const byte *k,u32 kbits,u32 ivbits);
MM_EXPORT_DLL void chacha_ivsetup(u32 *x,const byte *iv);
MM_EXPORT_DLL void chacha_encrypt_bytes(u32 *x,const byte *m,byte *c,u32 bytes);
MM_EXPORT_DLL void chacha_keystream_bytes(u32 *x,byte *stream,u32 bytes);

#include "core/mm_suffix.h"

#endif//__mm_chacha20_h__
