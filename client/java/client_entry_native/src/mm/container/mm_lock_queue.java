package mm.container;

import java.util.LinkedList;

import mm.core.mm_spinlock;
import mm.core.mm_thread;

//we catch all Exception e but print nothing but catch the error code.

//weak ref lock queue.
public class mm_lock_queue
{
	public LinkedList<Object> l = new LinkedList<Object>();
	public mm_thread.mutex cond_locker = new mm_thread.mutex();
	public mm_thread.cond cond_not_null = new mm_thread.cond(this.cond_locker);
	public mm_thread.cond cond_not_full = new mm_thread.cond(this.cond_locker);
	public mm_spinlock list_locker = new mm_spinlock();
	public mm_spinlock locker = new mm_spinlock();
	public int size = 0;// current size.
	public int max_size = Integer.MAX_VALUE;// max transport size.default is -1;
	//////////////////////////////////////////////////////////////////////////
	public void init()
	{
		this.l.clear();
		this.cond_locker.init();
		this.cond_not_null.init();
		this.cond_not_full.init();
		this.list_locker.init();
		this.locker.init();
		this.size = 0;
		this.max_size = Integer.MAX_VALUE;// -1 is default limit size_t value.
	}
	public void destroy()
	{
		this.l.clear();
		this.cond_locker.destroy();
		this.cond_not_null.destroy();
		this.cond_not_full.destroy();
		this.list_locker.destroy();
		this.locker.destroy();
		this.size = 0;
		this.max_size = Integer.MAX_VALUE;// -1 is default limit size_t value.
	}
	public void lock()
	{
		this.locker.lock();
	}
	public void unlock()
	{
		this.locker.unlock();
	}
	// Note: you can not lock it manual.
	public void cond_not_null()
	{
		this.cond_locker.lock();
		this.cond_not_null.signal();
		this.cond_locker.unlock();
	}
	// Note: you can not lock it manual.
	public void cond_not_full()
	{
		this.cond_locker.lock();
		this.cond_not_full.signal();
		this.cond_locker.unlock();
	}
	public void lpush(Object e)
	{
		this.list_locker.lock();
		this.l.addFirst(e);
		this.size ++;
		this.list_locker.unlock();
	}
	public Object lpop()
	{
		Object v = null;
		this.list_locker.lock();
		v = this.l.removeFirst();
		this.size --;
		this.list_locker.unlock();
		return v;
	}
	public void rpush(Object e)
	{
		this.list_locker.lock();
		this.l.addLast(e);
		this.size ++;
		this.list_locker.unlock();
	}
	public Object rpop()
	{
		Object v = null;
		this.list_locker.lock();
		v = this.l.removeLast();
		this.size --;
		this.list_locker.unlock();
		return v;
	}
	public void blpush(Object e)
	{
		this.cond_locker.lock();
		if ( this.max_size <= this.size )
		{
			this.cond_not_full.await();
		}
		//
		this.lpush(e);
		// conn map from 0 to 1,change to not empty,we signal to msg loop.
		if ( 1 == this.size)
		{
			this.cond_not_null.signal();
		}
		this.cond_locker.unlock();
	}
	public Object blpop()
	{
		Object v = null;
		this.cond_locker.lock();
		if ( 0 >= this.size )
		{
			this.cond_not_null.await();
		}
		//
		v = this.lpop();
		//
		if ( this.max_size - 1 == this.size )
		{
			this.cond_not_full.signal();
		}
		this.cond_locker.unlock();
		return v;
	}
	public void brpush(Object e)
	{
		this.cond_locker.lock();
		if ( this.max_size <= this.size )
		{
			this.cond_not_full.await();
		}
		//
		this.rpush(e);
		// conn map from 0 to 1,change to not empty,we signal to msg loop.
		if ( 1 == this.size)
		{
			this.cond_not_null.signal();
		}
		this.cond_locker.unlock();
	}
	public Object brpop()
	{
		Object v = null;
		this.cond_locker.lock();
		if ( 0 >= this.size )
		{
			this.cond_not_null.await();
		}
		//
		v = this.rpop();
		//
		if ( this.max_size - 1 == this.size )
		{
			this.cond_not_full.signal();
		}
		this.cond_locker.unlock();
		return v;
	}
	//////////////////////////////////////////////////////////////////////////
}
