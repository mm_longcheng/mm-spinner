package mm.core;

// linux errno
public class mm_errno
{
	public static final int MM_EPERM           =   1;// #define EPERM             1 /* Operation not permitted */
	public static final int MM_ENOENT          =   2;// #define ENOENT            2 /* No such file or directory */
	public static final int MM_ESRCH           =   3;// #define ESRCH             3 /* No such process */
	public static final int MM_EINTR           =   4;// #define EINTR             4 /* Interrupted system call */
	public static final int MM_EIO             =   5;// #define EIO               5 /* I/O error */
	public static final int MM_ENXIO           =   6;// #define ENXIO             6 /* No such device or address */
	public static final int MM_E2BIG           =   7;// #define E2BIG             7 /* Argument list too long */
	public static final int MM_ENOEXEC         =   8;// #define ENOEXEC           8 /* Exec format error */
	public static final int MM_EBADF           =   9;// #define EBADF             9 /* Bad file number */
	public static final int MM_ECHILD          =  10;// #define ECHILD           10 /* No child processes */
	public static final int MM_EAGAIN          =  11;// #define EAGAIN           11 /* Try again */
	public static final int MM_ENOMEM          =  12;// #define ENOMEM           12 /* Out of memory */
	public static final int MM_EACCES          =  13;// #define EACCES           13 /* Permission denied */
	public static final int MM_EFAULT          =  14;// #define EFAULT           14 /* Bad address */
	public static final int MM_ENOTBLK         =  15;// #define ENOTBLK          15 /* Block device required */
	public static final int MM_EBUSY           =  16;// #define EBUSY            16 /* Device or resource busy */
	public static final int MM_EEXIST          =  17;// #define EEXIST           17 /* File exists */
	public static final int MM_EXDEV           =  18;// #define EXDEV            18 /* Cross-device link */
	public static final int MM_ENODEV          =  19;// #define ENODEV           19 /* No such device */
	public static final int MM_ENOTDIR         =  20;// #define ENOTDIR          20 /* Not a directory */
	public static final int MM_EISDIR          =  21;// #define EISDIR           21 /* Is a directory */
	public static final int MM_EINVAL          =  22;// #define EINVAL           22 /* Invalid argument */
	public static final int MM_ENFILE          =  23;// #define ENFILE           23 /* File table overflow */
	public static final int MM_EMFILE          =  24;// #define EMFILE           24 /* Too many open files */
	public static final int MM_ENOTTY          =  25;// #define ENOTTY           25 /* Not a typewriter */
	public static final int MM_ETXTBSY         =  26;// #define ETXTBSY          26 /* Text file busy */
	public static final int MM_EFBIG           =  27;// #define EFBIG            27 /* File too large */
	public static final int MM_ENOSPC          =  28;// #define ENOSPC           28 /* No space left on device */
	public static final int MM_ESPIPE          =  29;// #define ESPIPE           29 /* Illegal seek */
	public static final int MM_EROFS           =  30;// #define EROFS            30 /* Read-only file system */
	public static final int MM_EMLINK          =  31;// #define EMLINK           31 /* Too many links */
	public static final int MM_EPIPE           =  32;// #define EPIPE            32 /* Broken pipe */
	public static final int MM_EDOM            =  33;// #define EDOM             33 /* Math argument out of domain of func */
	public static final int MM_ERANGE          =  34;// #define ERANGE           34 /* Math result not representable */
		
	public static final int MM_EDEADLK         =  35;// #define EDEADLK          35 /* Resource deadlock would occur */
	public static final int MM_ENAMETOOLONG    =  36;// #define ENAMETOOLONG     36 /* File name too long */
	public static final int MM_ENOLCK          =  37;// #define ENOLCK           37 /* No record locks available */
	public static final int MM_ENOSYS          =  38;// #define ENOSYS           38 /* Function not implemented */
	public static final int MM_ENOTEMPTY       =  39;// #define ENOTEMPTY        39 /* Directory not empty */
	public static final int MM_ELOOP           =  40;// #define ELOOP            40 /* Too many symbolic links encountered */
	public static final int MM_EWOULDBLOCK     = MM_EAGAIN;// #define EWOULDBLOCK EAGAIN  /* Operation would block */
	public static final int MM_ENOMSG          =  42;// #define ENOMSG           42 /* No message of desired type */
	public static final int MM_EIDRM           =  43;// #define EIDRM            43 /* Identifier removed */
	public static final int MM_ECHRNG          =  44;// #define ECHRNG           44 /* Channel number out of range */
	public static final int MM_EL2NSYNC        =  45;// #define EL2NSYNC         45 /* Level 2 not synchronized */
	public static final int MM_EL3HLT          =  46;// #define EL3HLT           46 /* Level 3 halted */
	public static final int MM_EL3RST          =  47;// #define EL3RST           47 /* Level 3 reset */
	public static final int MM_ELNRNG          =  48;// #define ELNRNG           48 /* Link number out of range */
	public static final int MM_EUNATCH         =  49;// #define EUNATCH          49 /* Protocol driver not attached */
	public static final int MM_ENOCSI          =  50;// #define ENOCSI           50 /* No CSI structure available */
	public static final int MM_EL2HLT          =  51;// #define EL2HLT           51 /* Level 2 halted */
	public static final int MM_EBADE           =  52;// #define EBADE            52 /* Invalid exchange */
	public static final int MM_EBADR           =  53;// #define EBADR            53 /* Invalid request descriptor */
	public static final int MM_EXFULL          =  54;// #define EXFULL           54 /* Exchange full */
	public static final int MM_ENOANO          =  55;// #define ENOANO           55 /* No anode */
	public static final int MM_EBADRQC         =  56;// #define EBADRQC          56 /* Invalid request code */
	public static final int MM_EBADSLT         =  57;// #define EBADSLT          57 /* Invalid slot */

	public static final int MM_EDEADLOCK       = MM_EDEADLK;// #define EDEADLOCK   EDEADLK

	public static final int MM_EBFONT          =  59;// #define EBFONT           59 /* Bad font file format */
	public static final int MM_ENOSTR          =  60;// #define ENOSTR           60 /* Device not a stream */
	public static final int MM_ENODATA         =  61;// #define ENODATA          61 /* No data available */
	public static final int MM_ETIME           =  62;// #define ETIME            62 /* Timer expired */
	public static final int MM_ENOSR           =  63;// #define ENOSR            63 /* Out of streams resources */
	public static final int MM_ENONET          =  64;// #define ENONET           64 /* Machine is not on the network */
	public static final int MM_ENOPKG          =  65;// #define ENOPKG           65 /* Package not installed */
	public static final int MM_EREMOTE         =  66;// #define EREMOTE          66 /* Object is remote */
	public static final int MM_ENOLINK         =  67;// #define ENOLINK          67 /* Link has been severed */
	public static final int MM_EADV            =  68;// #define EADV             68 /* Advertise error */
	public static final int MM_ESRMNT          =  69;// #define ESRMNT           69 /* Srmount error */
	public static final int MM_ECOMM           =  70;// #define ECOMM            70 /* Communication error on send */
	public static final int MM_EPROTO          =  71;// #define EPROTO           71 /* Protocol error */
	public static final int MM_EMULTIHOP       =  72;// #define EMULTIHOP        72 /* Multihop attempted */
	public static final int MM_EDOTDOT         =  73;// #define EDOTDOT          73 /* RFS specific error */
	public static final int MM_EBADMSG         =  74;// #define EBADMSG          74 /* Not a data message */
	public static final int MM_EOVERFLOW       =  75;// #define EOVERFLOW        75 /* Value too large for defined data type */
	public static final int MM_ENOTUNIQ        =  76;// #define ENOTUNIQ         76 /* Name not unique on network */
	public static final int MM_EBADFD          =  77;// #define EBADFD           77 /* File descriptor in bad state */
	public static final int MM_EREMCHG         =  78;// #define EREMCHG          78 /* Remote address changed */
	public static final int MM_ELIBACC         =  79;// #define ELIBACC          79 /* Can not access a needed shared library */
	public static final int MM_ELIBBAD         =  80;// #define ELIBBAD          80 /* Accessing a corrupted shared library */
	public static final int MM_ELIBSCN         =  81;// #define ELIBSCN          81 /* .lib section in a.out corrupted */
	public static final int MM_ELIBMAX         =  82;// #define ELIBMAX          82 /* Attempting to link in too many shared libraries */
	public static final int MM_ELIBEXEC        =  83;// #define ELIBEXEC         83 /* Cannot exec a shared library directly */
	public static final int MM_EILSEQ          =  84;// #define EILSEQ           84 /* Illegal byte sequence */
	public static final int MM_ERESTART        =  85;// #define ERESTART         85 /* Interrupted system call should be restarted */
	public static final int MM_ESTRPIPE        =  86;// #define ESTRPIPE         86 /* Streams pipe error */
	public static final int MM_EUSERS          =  87;// #define EUSERS           87 /* Too many users */
	public static final int MM_ENOTSOCK        =  88;// #define ENOTSOCK         88 /* Socket operation on non-socket */
	public static final int MM_EDESTADDRREQ    =  89;// #define EDESTADDRREQ     89 /* Destination address required */
	public static final int MM_EMSGSIZE        =  90;// #define EMSGSIZE         90 /* Message too long */
	public static final int MM_EPROTOTYPE      =  91;// #define EPROTOTYPE       91 /* Protocol wrong type for socket */
	public static final int MM_ENOPROTOOPT     =  92;// #define ENOPROTOOPT      92 /* Protocol not available */
	public static final int MM_EPROTONOSUPPORT =  93;// #define EPROTONOSUPPORT  93 /* Protocol not supported */
	public static final int MM_ESOCKTNOSUPPORT =  94;// #define ESOCKTNOSUPPORT  94 /* Socket type not supported */
	public static final int MM_EOPNOTSUPP      =  95;// #define EOPNOTSUPP       95 /* Operation not supported on transport endpoint */
	public static final int MM_EPFNOSUPPORT    =  96;// #define EPFNOSUPPORT     96 /* Protocol family not supported */
	public static final int MM_EAFNOSUPPORT    =  97;// #define EAFNOSUPPORT     97 /* Address family not supported by protocol */
	public static final int MM_EADDRINUSE      =  98;// #define EADDRINUSE       98 /* Address already in use */
	public static final int MM_EADDRNOTAVAIL   =  99;// #define EADDRNOTAVAIL    99 /* Cannot assign requested address */
	public static final int MM_ENETDOWN        = 100;// #define ENETDOWN        100 /* Network is down */
	public static final int MM_ENETUNREACH     = 101;// #define ENETUNREACH     101 /* Network is unreachable */
	public static final int MM_ENETRESET       = 102;// #define ENETRESET       102 /* Network dropped connection because of reset */
	public static final int MM_ECONNABORTED    = 103;// #define ECONNABORTED    103 /* Software caused connection abort */
	public static final int MM_ECONNRESET      = 104;// #define ECONNRESET      104 /* Connection reset by peer */
	public static final int MM_ENOBUFS         = 105;// #define ENOBUFS         105 /* No buffer space available */
	public static final int MM_EISCONN         = 106;// #define EISCONN         106 /* Transport endpoint is already connected */
	public static final int MM_ENOTCONN        = 107;// #define ENOTCONN        107 /* Transport endpoint is not connected */
	public static final int MM_ESHUTDOWN       = 108;// #define ESHUTDOWN       108 /* Cannot send after transport endpoint shutdown */
	public static final int MM_ETOOMANYREFS    = 109;// #define ETOOMANYREFS    109 /* Too many references: cannot splice */
	public static final int MM_ETIMEDOUT       = 110;// #define ETIMEDOUT       110 /* Connection timed out */
	public static final int MM_ECONNREFUSED    = 111;// #define ECONNREFUSED    111 /* Connection refused */
	public static final int MM_EHOSTDOWN       = 112;// #define EHOSTDOWN       112 /* Host is down */
	public static final int MM_EHOSTUNREACH    = 113;// #define EHOSTUNREACH    113 /* No route to host */
	public static final int MM_EALREADY        = 114;// #define EALREADY        114 /* Operation already in progress */
	public static final int MM_EINPROGRESS     = 115;// #define EINPROGRESS     115 /* Operation now in progress */
	public static final int MM_ESTALE          = 116;// #define ESTALE          116 /* Stale NFS file handle */
	public static final int MM_EUCLEAN         = 117;// #define EUCLEAN         117 /* Structure needs cleaning */
	public static final int MM_ENOTNAM         = 118;// #define ENOTNAM         118 /* Not a XENIX named type file */
	public static final int MM_ENAVAIL         = 119;// #define ENAVAIL         119 /* No XENIX semaphores available */
	public static final int MM_EISNAM          = 120;// #define EISNAM          120 /* Is a named type file */
	public static final int MM_EREMOTEIO       = 121;// #define EREMOTEIO       121 /* Remote I/O error */
	public static final int MM_EDQUOT          = 122;// #define EDQUOT          122 /* Quota exceeded */

	public static final int MM_ENOMEDIUM       = 123;// #define ENOMEDIUM       123 /* No medium found */
	public static final int MM_EMEDIUMTYPE     = 124;// #define EMEDIUMTYPE     124 /* Wrong medium type */
	public static final int MM_ECANCELED       = 125;// #define ECANCELED       125 /* Operation Canceled */
	public static final int MM_ENOKEY          = 126;// #define ENOKEY          126 /* Required key not available */
	public static final int MM_EKEYEXPIRED     = 127;// #define EKEYEXPIRED     127 /* Key has expired */
	public static final int MM_EKEYREVOKED     = 128;// #define EKEYREVOKED     128 /* Key has been revoked */
	public static final int MM_EKEYREJECTED    = 129;// #define EKEYREJECTED    129 /* Key was rejected by service */

	/* for robust mutexes */
	public static final int MM_EOWNERDEAD      = 130;// #define EOWNERDEAD      130 /* Owner died */
	public static final int MM_ENOTRECOVERABLE = 131;// #define ENOTRECOVERABLE 131 /* State not recoverable */
	
	public static ThreadLocal<Integer> g_thread_error_code = new ThreadLocal<Integer>();// thread error code.
	
	public static void set_code(int code)
	{
		g_thread_error_code.set(code);
	}
	public static int get_code()
	{
		return g_thread_error_code.get();
	}
}
