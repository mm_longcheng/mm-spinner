package mm.core;

// little endian
public class mm_byte
{
	public static void int16_encode_bytes( byte[] buffer, int offset, short i )
	{
		buffer[offset + 1] = (byte)((i >>  8) & 0xFF);
		buffer[offset + 0] = (byte)((i >>  0) & 0xFF);
	}
	
	public static short int16_decode_bytes( byte[] buffer, int offset ) 
	{
		short value = 0;
		value |= ((int)(buffer[offset + 1] & 0xFF)) <<  8;
		value |= ((int)(buffer[offset + 0] & 0xFF)) <<  0;
		return value;
	}
	public static void int32_encode_bytes( byte[] buffer, int offset, int i )
	{
		buffer[offset + 3] = (byte)((i >> 24) & 0xFF);
		buffer[offset + 2] = (byte)((i >> 16) & 0xFF);
		buffer[offset + 1] = (byte)((i >>  8) & 0xFF);
		buffer[offset + 0] = (byte)((i >>  0) & 0xFF);
	}
	
	public static int int32_decode_bytes( byte[] buffer, int offset ) 
	{
		int value = 0;
		value |= ((int)(buffer[offset + 3] & 0xFF)) << 24;
		value |= ((int)(buffer[offset + 2] & 0xFF)) << 16;
		value |= ((int)(buffer[offset + 1] & 0xFF)) <<  8;
		value |= ((int)(buffer[offset + 0] & 0xFF)) <<  0;
		return value;
	}
	public static void int64_encode_bytes( byte[] buffer, int offset, long i )
	{
		buffer[offset + 7] = (byte)((i >> 56) & 0xFF);
		buffer[offset + 6] = (byte)((i >> 48) & 0xFF);
		buffer[offset + 5] = (byte)((i >> 40) & 0xFF);
		buffer[offset + 4] = (byte)((i >> 32) & 0xFF);
		buffer[offset + 3] = (byte)((i >> 24) & 0xFF);
		buffer[offset + 2] = (byte)((i >> 16) & 0xFF);
		buffer[offset + 1] = (byte)((i >>  8) & 0xFF);
		buffer[offset + 0] = (byte)((i >>  0) & 0xFF);
	}
	
	public static long int64_decode_bytes( byte[] buffer, int offset ) 
	{
		long value = 0;
		value |= ((long)(buffer[offset + 7] & 0xFF)) << 56;
		value |= ((long)(buffer[offset + 6] & 0xFF)) << 48;
		value |= ((long)(buffer[offset + 5] & 0xFF)) << 40;
		value |= ((long)(buffer[offset + 4] & 0xFF)) << 32;
		value |= ((long)(buffer[offset + 3] & 0xFF)) << 24;
		value |= ((long)(buffer[offset + 2] & 0xFF)) << 16;
		value |= ((long)(buffer[offset + 1] & 0xFF)) <<  8;
		value |= ((long)(buffer[offset + 0] & 0xFF)) <<  0;
		return value;
	}
}
