package mm.core;

import java.util.TreeMap;

public class mm_logger
{
	public static final int MM_LOG_UNKNOW = 0;
	public static final int MM_LOG_FATAL = 1;
	public static final int MM_LOG_CRIT = 2;
	public static final int MM_LOG_ERROR = 3;
	public static final int MM_LOG_ALERT = 4;
	public static final int MM_LOG_WARNING = 5;
	public static final int MM_LOG_NOTICE = 6;
	public static final int MM_LOG_INFO = 7;
	public static final int MM_LOG_TRACE = 8;
	public static final int MM_LOG_DEBUG = 9;
	public static final int MM_LOG_VERBOSE = 10;

    public static class level_mark
    {
        public String m; // mark
        public String n; // name
        public level_mark( String _m, String _n )
        {
        	this.m = _m;
        	this.n = _n;
        }
    };
    public static TreeMap<Integer, level_mark> const_level_mark = new TreeMap<Integer, level_mark>() 
    {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			this.put(MM_LOG_UNKNOW, new level_mark("U","unknow") );
			this.put(MM_LOG_FATAL, new level_mark("F","fatal") );
			this.put(MM_LOG_CRIT, new level_mark("C","crit") );
			this.put(MM_LOG_ERROR, new level_mark("E","error") );
			this.put(MM_LOG_ALERT, new level_mark("A","alert") );
			this.put(MM_LOG_WARNING, new level_mark("W","warning") );
			this.put(MM_LOG_NOTICE, new level_mark("N","notice") );
			this.put(MM_LOG_INFO, new level_mark("I","info") );
			this.put(MM_LOG_TRACE, new level_mark("T","trace") );
			this.put(MM_LOG_DEBUG, new level_mark("D","debug") );
			this.put(MM_LOG_VERBOSE, new level_mark("V","verbose") );
        }
    };
    
	public static abstract class mm_logger_callback
	{
		public abstract void EventLogger(Object o, int lvl, String message);
	};
	public static class mm_logger_callback_default extends mm_logger_callback
	{
		@Override
		public void EventLogger(Object o, int lvl, String message) 
		{
			synchronized (o)
			{
				// [ 8 V ]
				level_mark lm = level_mark_by_lvl(lvl);
				System.out.println(" " + lvl + " " + lm.m + " "+ message);	
			}
		}	
	};

    public mm_logger_callback callback = new mm_logger_callback_default();
    public Object obj;//weak ref for obj.	
	
    private volatile static mm_logger _instance = null;
	static public mm_logger instance()
	{
		 if ( null == _instance)
		 {  
			 synchronized (mm_logger.class)
			 {
				 if ( null == _instance)
				 {
					 _instance = new mm_logger();
				 }
			 }
		 }
		return _instance;
	}
	public static void assign_callback(mm_logger_callback_default callback)
	{
		// android assertions are unreliable.
		// assert null != callback : "you can not assign null callback.";
		mm_logger g_logger = mm_logger.instance();
		synchronized (g_logger)
		{
			g_logger.callback = callback;
		}
	}
    public static level_mark level_mark_by_lvl(int lvl)
    {
        if ( MM_LOG_FATAL <= lvl && MM_LOG_VERBOSE >= lvl )
        {
	        return const_level_mark.get(lvl);
        }
        else
        {
        	return const_level_mark.get(MM_LOG_UNKNOW);
        }
    }
    // logger function.
	static public void logU(String message)
	{
		mm_logger g_logger = mm_logger.instance();
		g_logger.callback.EventLogger(g_logger, MM_LOG_UNKNOW, message);
	}
	static public void logF(String message)
	{
		mm_logger g_logger = mm_logger.instance();
		g_logger.callback.EventLogger(g_logger, MM_LOG_FATAL, message);
	}
	static public void logC(String message)
	{
		mm_logger g_logger = mm_logger.instance();
		g_logger.callback.EventLogger(g_logger, MM_LOG_CRIT, message);
	}
	static public void logE(String message)
	{
		mm_logger g_logger = mm_logger.instance();
		g_logger.callback.EventLogger(g_logger, MM_LOG_ERROR, message);
	}
	static public void logA(String message)
	{
		mm_logger g_logger = mm_logger.instance();
		g_logger.callback.EventLogger(g_logger, MM_LOG_ALERT, message);
	}
	static public void logW(String message)
	{
		mm_logger g_logger = mm_logger.instance();
		g_logger.callback.EventLogger(g_logger, MM_LOG_WARNING, message);
	}
	static public void logN(String message)
	{
		mm_logger g_logger = mm_logger.instance();
		g_logger.callback.EventLogger(g_logger, MM_LOG_NOTICE, message);
	}
	static public void logI(String message)
	{
		mm_logger g_logger = mm_logger.instance();
		g_logger.callback.EventLogger(g_logger, MM_LOG_INFO, message);
	}
	static public void logT(String message)
	{
		mm_logger g_logger = mm_logger.instance();
		g_logger.callback.EventLogger(g_logger, MM_LOG_TRACE, message);
	}
	static public void logD(String message)
	{
		mm_logger g_logger = mm_logger.instance();
		g_logger.callback.EventLogger(g_logger, MM_LOG_DEBUG, message);
	}
	static public void logV(String message)
	{
		mm_logger g_logger = mm_logger.instance();
		g_logger.callback.EventLogger(g_logger, MM_LOG_VERBOSE, message);
	}
}
