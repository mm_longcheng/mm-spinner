package mm.core;

import java.net.ServerSocket;
import java.net.Socket;

//we catch all Exception e but print nothing but catch the error code.
public class mm_socket
{
	public static final Object MM_INVALID_SOCKET = null;	
	// linux socket.h
	public static final int MM_AF_INET4 = 2;
	public static final int MM_AF_INET6 = 10;
	// linux socket.h
	public static final int SOCK_STREAM = 1;
	public static final int SOCK_DGRAM  = 2;
	public static final int SOCK_ROW    = 3;
	
	public static final int MM_READ_SHUTDOWN    = 0x00;
	public static final int MM_SEND_SHUTDOWN    = 0x01;
	public static final int MM_BOTH_SHUTDOWN    = 0x02;
	
	public static final int MM_NONBLOCK = 0;
	public static final int MM_BLOCKING = 1;
	
	public static final int MM_KEEPALIVE_INACTIVE = 0;// not keepalive.
	public static final int MM_KEEPALIVE_ACTIVATE = 1;// mod keepalive.
	
	public static void keepalive(Socket socket,int keepalive)
	{
		try 
		{
			socket.setKeepAlive(MM_KEEPALIVE_ACTIVATE == keepalive ? true : false);
		} 
		catch (Exception e) 
		{
			// do nothing here.we only need the finally code.
		}
	}
	public static int set_reuse_address(ServerSocket socket,boolean value)
	{
		int rt = -1;
		try 
		{
			socket.setReuseAddress(value);
			rt = 0;
		} 
		catch (Exception e) 
		{
			// do nothing here.we only need the finally code.
		}
		return rt;
		
	}
}
