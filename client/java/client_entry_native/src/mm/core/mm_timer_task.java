package mm.core;

import java.util.Timer;
import java.util.TimerTask;

import mm.core.mm_thread;

public class mm_timer_task
{
	// timer task default nearby time milliseconds.
	public static final int MM_TIME_TASK_NEARBY_MSEC = 200;
	
	public static class mm_timer_task_callback
	{
		public void handle( mm_timer_task p) {}
		public Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	}
	
	public Timer task_thread = new Timer();// check thread.
	public mm_timer_task_callback callback = new mm_timer_task_callback();
	public int nearby_time = MM_TIME_TASK_NEARBY_MSEC;// nearby_time milliseconds for each time wait.default is MM_TIME_TASK_NEARBY_MSEC ms.
	public int msec_delay = 0;// delay milliseconds before first callback.default is 0 ms.
	public int state = mm_thread.ts_closed;// mm_thread_state_t,default is ts_closed(0)

	public void init()
	{
		this.callback.init();
		this.nearby_time = MM_TIME_TASK_NEARBY_MSEC;
		this.msec_delay = 0;
		this.state = mm_thread.ts_closed;
	}
	public void destroy()
	{
		this.callback.init();
		this.nearby_time = 0;
		this.msec_delay = 0;
		this.state = mm_thread.ts_closed;
	}
	//////////////////////////////////////////////////////////////////////////
	// do not assign when holder have some elem.
	public void assign_callback(mm_timer_task_callback callback)
	{
		assert null != callback : "you can not assign null callback.";
		this.callback = callback;
	}
	public void assign_nearby_time(int nearby_time)
	{
		this.nearby_time = nearby_time;
	}
	//////////////////////////////////////////////////////////////////////////
	// sync.
	public void handle()
	{
		
	}
	//////////////////////////////////////////////////////////////////////////
	// start thread.
	public void start()
	{
		this.state = mm_thread.ts_finish == this.state ? mm_thread.ts_closed : mm_thread.ts_motion;
		this.task_thread.schedule(new __timer_task_task(this), this.msec_delay);
	}
	// interrupt thread.
	public void interrupt()
	{
		this.state = mm_thread.ts_closed;
		this.task_thread.cancel();
	}
	// shutdown thread.
	public void shutdown()
	{
		this.state = mm_thread.ts_finish;
		this.task_thread.cancel();
	}
	// join thread.
	public void join()
	{
		// java.util.Timer not need join at all.
	}
	//////////////////////////////////////////////////////////////////////////
	public String impl_version()
	{
		return "java.util.Timer";
	}
	//////////////////////////////////////////////////////////////////////////
	static final class __timer_task_task extends TimerTask 
	{
		public mm_timer_task obj = null;
		
		__timer_task_task(mm_timer_task _p)
		{
			this.obj = _p;
		}
		@Override
        public void run() 
		{
			this.obj.callback.handle(this.obj);
			if(mm_thread.ts_motion == this.obj.state)
			{
				this.obj.task_thread.schedule(new __timer_task_task(this.obj), this.obj.nearby_time);
			}			
        }
    }
}
