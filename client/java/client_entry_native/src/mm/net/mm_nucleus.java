package mm.net;

import java.nio.channels.spi.AbstractSelectableChannel;

import mm.core.mm_spinlock;
import mm.core.mm_thread;

public class mm_nucleus
{
	public mm_nuclear.mm_nuclear_callback callback = new mm_nuclear.mm_nuclear_callback();
	
	public mm_thread.mutex signal_mutex = new mm_thread.mutex();
	public mm_thread.cond signal_cond = new mm_thread.cond(this.signal_mutex);

	public mm_spinlock arrays_locker = new mm_spinlock();
	public mm_spinlock locker = new mm_spinlock();
	
	public int length = 0;// length. default is 0.
	public mm_nuclear[] arrays = null;// array pointer.
	// set poll len for mm_poll_wait.default is MM_NUCLEAR_POLL_LEN.set it before wait,or join_wait after.
	public int poll_length = mm_nuclear.MM_NUCLEAR_POLL_LENGTH;// poll length.
	// set poll timeout interval.-1 is forever.default is MM_NUCLEAR_IDLE_SLEEP_MSEC.
	// if forever,you must resolve break block state while poll wait,use pipeline,or other way.
	public int poll_timeout = mm_nuclear.MM_NUCLEAR_IDLE_SLEEP_MSEC;// poll timeout.
	//
	public int state = mm_thread.ts_closed;// mm_thread_state_t,default is ts_closed(0)
	
	public void init()
	{
		this.callback.init();
		this.signal_mutex.init();
		this.signal_cond.init();
		this.arrays_locker.init();
		this.locker.init();
		this.length = 0;
		this.arrays = null;
		this.poll_length = mm_nuclear.MM_NUCLEAR_POLL_LENGTH;
		this.poll_timeout = mm_nuclear.MM_NUCLEAR_IDLE_SLEEP_MSEC;// poll timeout.
		
		this.state = mm_thread.ts_closed;
	}
	public void destroy()
	{
		this.clear();
		//
		this.callback.destroy();
		this.signal_mutex.destroy();
		this.signal_cond.destroy();
		this.arrays_locker.destroy();
		this.locker.destroy();
		this.poll_length = 0;
		this.poll_timeout = 0;
		
		this.state = mm_thread.ts_closed;
	}
	//////////////////////////////////////////////////////////////////////////
	public void set_length(int length)
	{
		this.arrays_locker.lock();
		if (length < this.length)
		{
			int i = 0;
			mm_nuclear e = null;
			for ( i = length; i < this.length; ++i)
			{
				e = this.arrays[i];
				e.lock();
				this.arrays[i] = null;
				e.unlock();
				e.destroy();
				e = null;
			}
			mm_nuclear[] v = new mm_nuclear[length];
			System.arraycopy(this.arrays, 0, v, 0, length);
			this.arrays = v;
			this.length = length;
		}
		else if (length > this.length)
		{
			int i = 0;
			mm_nuclear[] v = new mm_nuclear[length];
			System.arraycopy(this.arrays, 0, v, 0, this.length);
			for ( i = this.length; i < length; ++i)
			{
				this.arrays[i] = new mm_nuclear();
				this.arrays[i].init();
				this.arrays[i].poll_length = this.poll_length;
				this.arrays[i].poll_timeout = this.poll_timeout;
				this.arrays[i].state = this.state;
				this.arrays[i].assign_callback(this.callback);
			}
			this.length = length;
		}
		this.arrays_locker.unlock();
	}
	public int get_length()
	{
		return this.length;
	}
	public void assign_callback(mm_nuclear.mm_nuclear_callback cb)
	{
		int i = 0;
		// android assertions are unreliable.
		// assert null != cb : "you can not assign null callback.";
		this.arrays_locker.lock();
		this.callback = cb;
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			this.arrays[i].assign_callback(this.callback);
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
	}
	// poll size.
	public int poll_size()
	{
		int sz = 0;
		int i = 0;
		this.arrays_locker.lock();
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			sz += this.arrays[i].poll_size();
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
		return sz;
	}
	//////////////////////////////////////////////////////////////////////////
	//wait for activation fd.
	public void poll_wait()
	{
		int i = 0;
		this.arrays_locker.lock();
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			this.arrays[i].poll_wait();
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	//start wait thread.
	public void start()
	{
		int i = 0;
		this.arrays_locker.lock();
		this.state = mm_thread.ts_finish == this.state ? mm_thread.ts_closed : mm_thread.ts_motion;
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			this.arrays[i].start();
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
	}
	//interrupt wait thread.
	public void interrupt()
	{
		int i = 0;
		this.arrays_locker.lock();
		this.state = mm_thread.ts_closed;
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			this.arrays[i].interrupt();
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
		//
		this.signal_mutex.lock();
		this.signal_cond.signal();
		this.signal_mutex.unlock();
	}
	//shutdown wait thread.
	public void shutdown()
	{
		int i = 0;
		this.arrays_locker.lock();
		this.state = mm_thread.ts_finish;
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			this.arrays[i].shutdown();
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
		//
		this.signal_mutex.lock();
		this.signal_cond.signal();
		this.signal_mutex.unlock();
	}
	//join wait thread.
	public void join()
	{
		int i = 0;
		if (mm_thread.ts_motion == this.state)
		{
			// we can not lock or join until cond wait all thread is shutdown.
			this.signal_mutex.lock();
			this.signal_cond.await();
			this.signal_mutex.unlock();	
		}
		//
		this.arrays_locker.lock();
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			this.arrays[i].join();
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
	}
	public void clear()
	{
		int i = 0;
		this.arrays_locker.lock();
		for ( i = 0; i < this.length; ++i)
		{
			mm_nuclear e = this.arrays[i];
			e.lock();
			this.arrays[i] = null;
			e.unlock();
			e.destroy();
			e = null;
		}
		this.arrays = null;
		this.length = 0;
		this.arrays_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	public int hash_index( AbstractSelectableChannel fd )
	{
		return fd.hashCode() % this.length;
	}
	//////////////////////////////////////////////////////////////////////////
	// add u into poll_fd.
	public void fd_add(AbstractSelectableChannel fd, Object u)
	{
		int idx = this.hash_index(fd);
		// android assertions are unreliable.
		// assert idx >= 0 && idx < this.length : "idx is out range.";
		this.arrays[idx].fd_add(fd, u);
	}
	// rmv u from poll_fd.
	public void fd_rmv(AbstractSelectableChannel fd, Object u)
	{
		int idx = this.hash_index(fd);
		// android assertions are unreliable.
		// assert idx >= 0 && idx < this.length : "idx is out range.";
		this.arrays[idx].fd_rmv(fd, u);
	}
	// mod u event from poll wait.
	public void fd_mod(AbstractSelectableChannel fd, Object u, int flag)
	{
		int idx = this.hash_index(fd);
		// android assertions are unreliable.
		// assert idx >= 0 && idx < this.length : "idx is out range.";
		this.arrays[idx].fd_mod(fd, u, flag);
	}
	// get u from poll.
	public Object fd_get(AbstractSelectableChannel fd)
	{
		int idx = this.hash_index(fd);
		// android assertions are unreliable.
		// assert idx >= 0 && idx < this.length : "idx is out range.";
		return this.arrays[idx].fd_get(fd);
	}
	//////////////////////////////////////////////////////////////////////////
}
