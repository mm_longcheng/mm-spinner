package mm.net;

import java.net.Socket;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;

import mm.core.mm_errno;
import mm.core.mm_logger;
import mm.core.mm_socket;
import mm.core.mm_thread;
import mm.core.mm_time;

//we catch all Exception e but print nothing but catch the error code.

//we not use select timeout for accept interrupt.
//we just use close listen socket fd and trigger a socket errno 
//[errno:(53) Software caused connection abort] to interrupt accept.
//this mode will make this class simpleness and negative effects receivability.
public class mm_accepter
{
	public static final String TAG = mm_tcp.class.getSimpleName();
	
	// listen backlog default 5;
	public static final int MM_ACCEPTER_BACKLOG = 5;
	// if bind listen failure will msleep this time.
	public static final int MM_ACCEPTER_TRY_MSLEEP_TIME = 1000;
	// net tcp nonblock timeout..
	public static final int MM_ACCEPTER_NONBLOCK_TIMEOUT = 1000;
	// net accept one time try time.
	public static final int MM_NET_ACCEPTER_TRYTIME = Integer.MAX_VALUE;
	
	public static final int accepter_state_closed = 0;// fd is closed,connect closed.invalid at all.
	public static final int accepter_state_motion = 1;// fd is opened,connect closed.at connecting.
	public static final int accepter_state_finish = 2;// fd is opened,connect opened.at connected.
	public static final int accepter_state_broken = 3;// fd is opened,connect broken.connect is broken fd not closed.
	
	public static class mm_accepter_callback
	{	
		public void accept( Object obj, AbstractSelectableChannel fd, mm_sockaddr remote){}
		Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	}
	
	public mm_addr addr = new mm_addr();// strong ref. accepter address.
	public mm_accepter_callback callback = new mm_accepter_callback();// value ref. accepter callback.
	public int backlog = MM_ACCEPTER_BACKLOG;// listen backlog,default MM_ACCEPTER_BACKLOG.
	public mm_thread accept_thread = new mm_thread();// thread.
	public int keepalive = mm_socket.MM_KEEPALIVE_ACTIVATE;// accepter_keepalive_activate true accepter_keepalive_inactive false keepalive the accept fd.default is accepter_keepalive_activate.
	public int nonblock_timeout = MM_ACCEPTER_NONBLOCK_TIMEOUT;// tcp nonblock timeout.default is MM_ACCEPTER_NONBLOCK_TIMEOUT milliseconds.
	public int accepter_state = accepter_state_closed;// mm_accepter_state_t,default is accepter_state_closed(0)
	public int trytimes = MM_NET_ACCEPTER_TRYTIME;// try bind and listen times.default is MM_NET_ACCEPTER_TRYTIME.
	public int state = mm_thread.ts_closed;// mm_thread_state_t,default is ts_closed(0)

	public void init()
	{
		this.addr.init();
		this.callback.init();
		this.backlog = MM_ACCEPTER_BACKLOG;
		this.accept_thread.init();
		this.keepalive = mm_socket.MM_KEEPALIVE_ACTIVATE;
		this.accepter_state = accepter_state_closed;// mm_accepter_state_t,default is accepter_state_closed(0)
		this.nonblock_timeout = MM_ACCEPTER_NONBLOCK_TIMEOUT;// tcp nonblock timeout.default is MM_ACCEPTER_NONBLOCK_TIMEOUT milliseconds.
		this.trytimes = MM_NET_ACCEPTER_TRYTIME;
		this.state = mm_thread.ts_closed;
	}
	public void destroy()
	{
		this.addr.destroy();
		this.callback.destroy();
		this.backlog = 0;
		this.accept_thread.destroy();
		this.keepalive = mm_socket.MM_KEEPALIVE_ACTIVATE;
		this.accepter_state = accepter_state_closed;
		this.nonblock_timeout = 0;
		this.trytimes = 0;
		this.state = mm_thread.ts_closed;
	}
	// assign native address but not connect.
	public void assign_native(String node, int port)
	{
		this.addr.assign_native(node, port);
	}
	// assign remote address but not connect.
	public void assign_remote(String node, int port)
	{
		this.addr.assign_remote(node, port);
	}
	public void assign_callback(mm_accepter_callback cb)
	{
		// android assertions are unreliable.
		// assert null != cb : "you can not assign null callback.";
		this.callback = cb;
	}
	// reset streambuf and fire event broken.
	public void apply_broken()
	{
		this.accepter_state = accepter_state_closed;
	}
	// reset streambuf and fire event broken.
	public void apply_finish()
	{
		this.accepter_state = accepter_state_finish;
	}
	// fopen socket.ss_native.ss_family,SOCK_STREAM,0
	public void fopen_socket()
	{
		// java special.we must assign the addr type for fopen correct socket struct.
		this.addr.set_addr_type(mm_addr.MM_ADDR_SERVER);
		this.addr.fopen_socket(mm_socket.MM_AF_INET6, mm_socket.SOCK_STREAM, 0);
		if (mm_socket.MM_INVALID_SOCKET != this.addr.socket)
		{
			mm_logger.logI(TAG + " fopen_socket " + this.addr.to_string() + " success.");
		}
		else
		{
			mm_logger.logE(TAG + " fopen_socket " + this.addr.to_string() + " failure.");
		}
		//SO_REUSEADDR
		//	Indicates that the rules used in validating addresses supplied
		//	in a bind(2) call should allow reuse of local addresses.  For
		//	AF_INET sockets this means that a socket may bind, except when
		//	there is an active listening socket bound to the address.
		//	When the listening socket is bound to INADDR_ANY with a
		//	specific port then it is not possible to bind to this port for
		//	any local address.  Argument is an integer boolean flag.
		ServerSocketChannel _socket = (ServerSocketChannel) this.addr.socket;
		if(-1 == mm_socket.set_reuse_address(_socket.socket(), true))
		{
			// the option for SO_REUSEADDR socket flag is optional.
			// this error is not serious.
			mm_logger.logI(TAG + " reuseaddr " + this.addr.to_string() + " success.");
		}
		else
		{
			mm_logger.logI(TAG + " reuseaddr " + this.addr.to_string() + " failure.");
		}
	}
	// bind address.
	public void bind()
	{
		mm_addr addr = this.addr;
		//
		if ( mm_socket.MM_INVALID_SOCKET != addr.socket )
		{
			int _try_times = 0;
			int rt = -1;
			this.accepter_state = accepter_state_motion;
			// set socket to blocking.
			addr.set_block(mm_socket.MM_BLOCKING);
			do 
			{
				_try_times ++;
				rt = addr.bind();
				if (0 == rt)
				{
					// bind immediately.
					break;
				}
				if ( mm_thread.ts_finish == this.state )
				{
					break;
				}
				else
				{
					mm_logger.logE(TAG + " " + "bind " + addr.to_string() + " failure.try times:" + _try_times);
					mm_time.msleep(MM_ACCEPTER_TRY_MSLEEP_TIME);
				}
			} while ( 0 != rt && _try_times < this.trytimes );
			// set socket to blocking.
			addr.set_block(mm_socket.MM_BLOCKING);
			if ( 0 != rt )
			{
				mm_logger.logE(TAG + " " + "bind " + addr.to_string() + " failure.try times:" + _try_times);
				this.apply_broken();
				addr.close_socket();
			}
			else
			{
				this.apply_finish();
				mm_logger.logI(TAG + " " + "bind " + addr.to_string() + " success.try times:" + _try_times);
			}
		}
		else
		{
			mm_logger.logE(TAG + " " + "bind target fd is invalid.");
		}
	}
	// listen address.
	public void listen()
	{
		mm_addr addr = this.addr;
		//
		if ( mm_socket.MM_INVALID_SOCKET != addr.socket )
		{
			int _try_times = 0;
			int rt = -1;
			this.accepter_state = accepter_state_motion;
			// set socket to blocking.
			addr.set_block(mm_socket.MM_BLOCKING);
			do 
			{
				_try_times ++;
				rt = addr.listen(this.backlog);
				if (0 == rt)
				{
					// listen immediately.
					break;
				}
				if ( mm_thread.ts_finish == this.state )
				{
					break;
				}
				else
				{
					mm_logger.logE(TAG + " " + "listen " + addr.to_string() + " failure.try times:" + _try_times);
					mm_time.msleep(MM_ACCEPTER_TRY_MSLEEP_TIME);
				}
			} while ( 0 != rt && _try_times < this.trytimes );
			// set socket to blocking.
			addr.set_block(mm_socket.MM_BLOCKING);
			if ( 0 != rt )
			{
				mm_logger.logE(TAG + " " + "listen " + addr.to_string() + " failure.try times:" + _try_times);
				this.apply_broken();
				addr.close_socket();
			}
			else
			{
				this.apply_finish();
				mm_logger.logI(TAG + " " + "listen " + addr.to_string() + " success.try times:" + _try_times);
			}
		}
		else
		{
			mm_logger.logE(TAG + " " + "listen target fd is invalid.");
		}
	}
	// close socket. mm_accepter_join before call this.
	public void close_socket()
	{
		this.addr.close_socket();
	}
	// accept sync.
	public void accept()
	{
		mm_sockaddr remote = new mm_sockaddr();
		SocketChannel afd = null;
		// we not need assert the fd is must valid.
		// assert( mm_socket.MM_INVALID_SOCKET != p->addr.fd&&"you must alloc socket fd first.");
		this.state = (mm_socket.MM_INVALID_SOCKET == this.addr.socket || mm_thread.ts_finish == this.state) ? mm_thread.ts_closed : mm_thread.ts_motion;
		//
		while(mm_thread.ts_motion == this.state)
		{
			remote.reset();
			
			afd = this.addr.accept();
			if(mm_socket.MM_INVALID_SOCKET == afd)
			{
				if(mm_errno.MM_EINTR == mm_errno.get_code())
				{
					// MM_EINTR errno:(10004) A blocking operation was interrupted.
					// this error is not serious.
					continue;
				}
				else
				{
					mm_logger.logI(TAG + " " + this.addr.to_string() + " error occur.");
					this.state = mm_thread.ts_closed;
					this.accepter_state = accepter_state_broken;
				}
			}
			else
			{
				Socket socket = afd.socket();
				if ( mm_socket.MM_KEEPALIVE_ACTIVATE == this.keepalive )
				{
					mm_socket.keepalive(socket, this.keepalive);
				}
				remote.node = socket.getInetAddress().getHostAddress();
				remote.port = socket.getPort();
				this.callback.accept(this, afd, remote);
				
				String link_name = mm_sockaddr.sockaddr_storage_native_remote_string(this.addr.ss_native, remote, afd.hashCode());
				mm_logger.logI(TAG + " " + link_name + " connect.");
			}
		}
	}
	//////////////////////////////////////////////////////////////////////////
	//start accept thread.
	public void start()
	{
		this.state = mm_thread.ts_finish == this.state ? mm_thread.ts_closed : mm_thread.ts_motion;
		this.accept_thread.start(new Thread(new __accepter_accept_thread_runnable(this)));
	}
	//interrupt accept thread.
	public void interrupt()
	{
		this.state = mm_thread.ts_closed;
		this.addr.shutdown_socket(mm_socket.MM_BOTH_SHUTDOWN);
		this.addr.close_socket();
	}
	//shutdown accept thread.
	public void shutdown()
	{
		this.state = mm_thread.ts_finish;
		this.addr.shutdown_socket(mm_socket.MM_BOTH_SHUTDOWN);
		this.addr.close_socket();
	}
	//join accept thread.
	public void join()
	{
		this.accept_thread.join();
	}
	//////////////////////////////////////////////////////////////////////////
	static final class __accepter_accept_thread_runnable implements Runnable
	{
		__accepter_accept_thread_runnable(mm_accepter _p)
		{
			this.p = _p;
		}
		public mm_accepter p = null;
		@Override
		public void run() 
		{
			p.accept();
		}		
	}
}
