package mm.net;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

import mm.core.mm_spinlock;
import mm.core.mm_thread;

public class mm_mt_contact
{
	public static class mm_mt_contact_event_mt_tcp_alloc
	{	
		public void event_mt_tcp_alloc( Object p, mm_mt_tcp mt_tcp ){}
		public void event_mt_tcp_relax( Object p, mm_mt_tcp mt_tcp ){}
		Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	}
	
	public mm_mt_contact_event_mt_tcp_alloc event_mt_tcp_alloc = new mm_mt_contact_event_mt_tcp_alloc();
	public mm_crypto.mm_crypto_callback crypto_callback = new mm_crypto.mm_crypto_callback();// crypto callback.
	public TreeMap<Integer,mm_mt_tcp> holder = new TreeMap<Integer,mm_mt_tcp>();
	public mm_thread.mutex signal_mutex = new mm_thread.mutex();
	public mm_thread.cond signal_cond = new mm_thread.cond(this.signal_mutex);
	public mm_spinlock holder_locker = new mm_spinlock();
	public mm_spinlock instance_locker = new mm_spinlock();
	public mm_spinlock locker = new mm_spinlock();
	public int state = mm_thread.ts_closed;// mm_thread_state_t,default is ts_closed(0)
	public Object u = null;// user data.

	public void init()
	{
		this.event_mt_tcp_alloc.init();
		this.crypto_callback.init();
		this.holder.clear();
		this.signal_mutex.init();
		this.signal_cond.init();
		this.holder_locker.init();
		this.instance_locker.init();
		this.locker.init();
		this.state = mm_thread.ts_closed;
	}
	public void destroy()
	{
		this.clear();
		//
		this.event_mt_tcp_alloc.destroy();
		this.crypto_callback.destroy();
		this.holder.clear();
		this.signal_mutex.destroy();
		this.signal_cond.destroy();
		this.holder_locker.destroy();
		this.instance_locker.destroy();
		this.locker.destroy();
		this.state = mm_thread.ts_closed;
	}
	//////////////////////////////////////////////////////////////////////////
	// lock to make sure the mt_contact is thread safe.
	public void lock()
	{
		this.locker.lock();
	}
	// unlock mt_contact.
	public void unlock()
	{
		this.locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	// do not assign when holder have some elem.
	public void assign_crypto_callback(mm_crypto.mm_crypto_callback crypto_callback)
	{
		mm_mt_tcp mt_tcp = null;
		Entry<Integer,mm_mt_tcp> n = null;
		Iterator<Entry<Integer,mm_mt_tcp>> it = null;
		this.holder_locker.lock();
		// android assertions are unreliable.
		// assert null != crypto_callback : "crypto_callback is a null.";
		this.crypto_callback = crypto_callback;
		it = this.holder.entrySet().iterator();
		while (it.hasNext())
		{
			n = it.next();
			mt_tcp = n.getValue();
			mt_tcp.lock();
			mt_tcp.assign_crypto_callback(crypto_callback);
			mt_tcp.unlock();
		}
		this.holder_locker.unlock();
	}
	// assign context handle.
	public void assign_context( Object u )
	{
		mm_mt_tcp mt_tcp = null;
		Entry<Integer,mm_mt_tcp> n = null;
		Iterator<Entry<Integer,mm_mt_tcp>> it = null;
		this.holder_locker.lock();
		this.u = u;
		it = this.holder.entrySet().iterator();
		while (it.hasNext())
		{
			n = it.next();
			mt_tcp = n.getValue();
			mt_tcp.lock();
			mt_tcp.assign_context(this.u);
			mt_tcp.unlock();
		}
		this.holder_locker.unlock();
	}
	public void check()
	{
		mm_mt_tcp mt_tcp = null;
		Entry<Integer,mm_mt_tcp> n = null;
		Iterator<Entry<Integer,mm_mt_tcp>> it = null;
		this.holder_locker.lock();
		it = this.holder.entrySet().iterator();
		while (it.hasNext())
		{
			n = it.next();
			mt_tcp = n.getValue();
			mt_tcp.lock();
			mt_tcp.check();
			mt_tcp.unlock();
		}
		this.holder_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	public void start()
	{
		mm_mt_tcp mt_tcp = null;
		Entry<Integer,mm_mt_tcp> n = null;
		Iterator<Entry<Integer,mm_mt_tcp>> it = null;
		this.holder_locker.lock();
		this.state = mm_thread.ts_finish == this.state ? mm_thread.ts_closed : mm_thread.ts_motion;
		it = this.holder.entrySet().iterator();
		while (it.hasNext())
		{
			n = it.next();
			mt_tcp = n.getValue();
			mt_tcp.lock();
			mt_tcp.start();
			mt_tcp.unlock();
		}
		this.holder_locker.unlock();
	}
	public void interrupt()
	{
		mm_mt_tcp mt_tcp = null;
		Entry<Integer,mm_mt_tcp> n = null;
		Iterator<Entry<Integer,mm_mt_tcp>> it = null;
		this.holder_locker.lock();
		this.state = mm_thread.ts_closed;
		it = this.holder.entrySet().iterator();
		while (it.hasNext())
		{
			n = it.next();
			mt_tcp = n.getValue();
			mt_tcp.lock();
			mt_tcp.interrupt();
			mt_tcp.unlock();
		}
		this.holder_locker.unlock();
		//
		this.signal_mutex.lock();
		this.signal_cond.signal();
		this.signal_mutex.unlock();
	}
	public void shutdown()
	{
		mm_mt_tcp mt_tcp = null;
		Entry<Integer,mm_mt_tcp> n = null;
		Iterator<Entry<Integer,mm_mt_tcp>> it = null;
		this.holder_locker.lock();
		this.state = mm_thread.ts_finish;
		it = this.holder.entrySet().iterator();
		while (it.hasNext())
		{
			n = it.next();
			mt_tcp = n.getValue();
			mt_tcp.lock();
			mt_tcp.shutdown();
			mt_tcp.unlock();
		}
		this.holder_locker.unlock();
		//
		this.signal_mutex.lock();
		this.signal_cond.signal();
		this.signal_mutex.unlock();
	}
	public void join()
	{
		mm_mt_tcp mt_tcp = null;
		Entry<Integer,mm_mt_tcp> n = null;
		Iterator<Entry<Integer,mm_mt_tcp>> it = null;
		if (mm_thread.ts_motion == this.state)
		{
			// we can not lock or join until cond wait all thread is shutdown.
			this.signal_mutex.lock();
			this.signal_cond.await();
			this.signal_mutex.unlock();
		}
		//
		this.holder_locker.lock();
		it = this.holder.entrySet().iterator();
		while (it.hasNext())
		{
			n = it.next();
			mt_tcp = n.getValue();
			mt_tcp.lock();
			mt_tcp.join();
			mt_tcp.unlock();
		}
		this.holder_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	public mm_mt_tcp add(int unique_id,String node_n,String node_r,int port)
	{
		mm_mt_tcp mt_tcp = null;
		this.instance_locker.lock();
		mt_tcp = this.get(unique_id);
		if(null == mt_tcp)
		{
			mt_tcp = new mm_mt_tcp();
			mt_tcp.init();

			mt_tcp.assign_native(node_n, port);
			mt_tcp.assign_remote(node_r, port);
			// assign sid index.
			mt_tcp.unique_id = unique_id;
			//
			mt_tcp.assign_crypto_callback(this.crypto_callback);
			//
			this.event_mt_tcp_alloc.event_mt_tcp_alloc(this, mt_tcp);
			//
			if ( mm_thread.ts_motion == this.state )
			{
				// if current mt_contact is in motion we start the new route .
				mt_tcp.start();
			}
			else
			{
				// if current mt_contact is not motion,we assign the same state.
				mt_tcp.state = this.state;
			}
			//
			this.holder_locker.lock();
			this.holder.put(unique_id, mt_tcp);
			this.holder_locker.unlock();
		}	
		this.instance_locker.unlock();
		return mt_tcp;
	}
	public void rmv(int unique_id)
	{
		mm_mt_tcp mt_tcp = null;
		this.lock();
		mt_tcp = this.holder.get(unique_id);
		if(null != mt_tcp)
		{
			mt_tcp.lock();
			this.holder.remove(unique_id);
			mt_tcp.unlock();
			this.event_mt_tcp_alloc.event_mt_tcp_relax(this, mt_tcp);
			mt_tcp.destroy();
			mt_tcp = null;
		}
		this.unlock();
	}
	public mm_mt_tcp get(int unique_id)
	{
		mm_mt_tcp mt_tcp = null;
		this.lock();
		mt_tcp = this.holder.get(unique_id);
		this.unlock();
		return mt_tcp;
	}
	public mm_mt_tcp get_instance(int unique_id,String node_n,String node_r,int port)
	{
		mm_mt_tcp mt_tcp = this.get(unique_id);
		if (null == mt_tcp)
		{
			mt_tcp = this.add(unique_id,node_n,node_r,port);
			//cache.
			mt_tcp.cache_native_node = node_n;
			mt_tcp.cache_remote_node = node_r;
			mt_tcp.cache_remote_port = port;
		}
		else
		{
			mt_tcp.lock();
			if ( 
				mt_tcp.cache_native_node != node_n || 
				mt_tcp.cache_remote_node != node_r ||
				mt_tcp.cache_remote_port != port)
			{
				// if ip and port not equal , we reset it.
				// reset mt_tcp address.
				mt_tcp.assign_native(node_n, port);
				mt_tcp.assign_remote(node_r, port);
				//cache.
				mt_tcp.cache_native_node = node_n;
				mt_tcp.cache_remote_node = node_r;
				mt_tcp.cache_remote_port = port;
				// the mt_tcp is invalid.
				mt_tcp.detach_socket();
			}
			mt_tcp.unlock();
		}
		return mt_tcp;
	}
	public void clear()
	{
		mm_mt_tcp mt_tcp = null;
		Entry<Integer,mm_mt_tcp> n = null;
		Iterator<Entry<Integer,mm_mt_tcp>> it = null;
		this.instance_locker.lock();
		this.holder_locker.lock();
		it = this.holder.entrySet().iterator();
		while (it.hasNext())
		{
			n = it.next();
			mt_tcp = n.getValue();
			mt_tcp.lock();
			it.remove();
			mt_tcp.unlock();
			this.event_mt_tcp_alloc.event_mt_tcp_relax(this, mt_tcp);
			mt_tcp.destroy();
			mt_tcp = null;
		}
		this.holder_locker.unlock();
		this.instance_locker.unlock();
	}
}
