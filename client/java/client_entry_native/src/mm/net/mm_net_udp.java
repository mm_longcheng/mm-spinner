package mm.net;

import java.util.TreeMap;

import mm.core.mm_logger;
import mm.core.mm_socket;
import mm.core.mm_spinlock;
import mm.core.mm_thread;
import mm.core.mm_time;

public class mm_net_udp
{
	public static final String TAG = mm_net_udp.class.getSimpleName();
	
	// net udp nonblock timeout..
	public static final int MM_NET_UDP_NONBLOCK_TIMEOUT = 20;
	// if bind listen failure will msleep this time.
	public static final int MM_NET_UDP_TRY_MSLEEP_TIME = 1000;
	// net udp one time try time.
	public static final int MM_NET_UDP_TRYTIME = 3;
	
	public static final int net_udp_state_closed = 0;// fd is closed,connect closed.invalid at all.
	public static final int net_udp_state_motion = 1;// fd is opened,connect closed.at connecting.
	public static final int net_udp_state_finish = 2;// fd is opened,connect opened.at connected.
	public static final int net_udp_state_broken = 3;// fd is opened,connect broken.connect is broken fd not closed.

	static final __net_udp_udp_handle_packet_callback __net_udp_udp_handle_packet_callback_impl = new __net_udp_udp_handle_packet_callback();
	
	// packet handle function type.
	public static interface mm_net_udp_handle_callback 
	{  
	    public void handle( Object obj, Object u, mm_packet pack, mm_sockaddr remote );
	}
	public static class mm_net_udp_callback
	{
		public void handle( Object obj, Object u, mm_packet pack, mm_sockaddr remote ){}
		public void broken( Object obj ){}
		public void nready( Object obj ){}
		public void finish( Object obj ){}
		public Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	}
	public static class mm_net_udp_event_udp_alloc
	{
		public void event_udp_alloc( Object obj, mm_udp udp ){}
		public void event_udp_relax( Object obj, mm_udp udp ){}
		public Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	}
	
	public mm_udp udp = new mm_udp();// strong ref.
	public mm_nuclear nuclear = new mm_nuclear();// strong ref.
	public TreeMap<Integer,mm_net_udp_handle_callback> holder = new TreeMap<Integer,mm_net_udp_handle_callback>();// rb tree for msg_id <--> callback.
	public mm_net_udp_callback callback = new mm_net_udp_callback();// value ref. udp callback.
	public mm_net_udp_event_udp_alloc event_udp_alloc = new mm_net_udp_event_udp_alloc();
	public mm_crypto.mm_crypto_callback crypto_callback = new mm_crypto.mm_crypto_callback();// crypto callback.
	public mm_spinlock holder_locker = new mm_spinlock();
	public mm_spinlock locker = new mm_spinlock();
	
	public int nonblock_timeout = MM_NET_UDP_NONBLOCK_TIMEOUT;// udp nonblock timeout.default is MM_NET_UDP_NONBLOCK_TIMEOUT milliseconds.

	public int udp_state = net_udp_state_closed;// mm_net_udp_state_t,default is udp_state_closed(0)

	public int trytimes = MM_NET_UDP_TRYTIME;// try bind and listen times.default is MM_NET_UDP_TRYTIME.
	public int state = mm_thread.ts_closed;// mm_thread_state_t,default is ts_closed(0)
	public Object u = null;// user data.
	
	/////////////////////////////////////////////////////////////
	public void init()
	{
		this.udp.init();
		this.nuclear.init();
		this.holder.clear();
		this.callback.init();
		this.event_udp_alloc.init();
		this.crypto_callback.init();
		this.holder_locker.init();
		this.locker.init();
		this.nonblock_timeout = MM_NET_UDP_NONBLOCK_TIMEOUT;
		this.udp_state = net_udp_state_closed;
		this.trytimes = MM_NET_UDP_TRYTIME;
		this.state = mm_thread.ts_closed;
		this.u = null;
		
		this.udp.assign_callback(new __net_udp_udp_callback(this));		
		this.nuclear.assign_callback(new __net_udp_nuclear_callback(this));
	}
	public void destroy()
	{
		this.clear_callback_holder();
		this.detach_socket();
		//
		this.udp.destroy();
		this.nuclear.destroy();
		this.holder.clear();
		this.callback.destroy();
		this.event_udp_alloc.destroy();
		this.crypto_callback.destroy();
		this.holder_locker.destroy();
		this.locker.destroy();
		this.nonblock_timeout = 0;
		this.udp_state = net_udp_state_closed;
		this.trytimes = 0;
		this.state = mm_thread.ts_closed;
		this.u = null;
	}
	/////////////////////////////////////////////////////////////
	// assign native address but not connect.
	public void assign_native(String node, int port)
	{
		this.udp.assign_native(node, port);
	}
	// assign remote address but not connect.
	public void assign_remote(String node, int port)
	{
		this.udp.assign_remote(node, port);
	}
	// assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
	public void assign_native_storage( mm_sockaddr ss_native )
	{
		this.udp.assign_native_storage(ss_native);
	}
	// assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
	public void assign_remote_storage( mm_sockaddr ss_remote )
	{
		this.udp.assign_remote_storage(ss_remote);
	}
	/////////////////////////////////////////////////////////////
	public void assign_callback(int id, mm_net_udp_handle_callback callback)
	{
		this.holder_locker.lock();
		this.holder.put(id, callback);
		this.holder_locker.unlock();
	}
	public void assign_net_udp_callback(mm_net_udp_callback net_udp_callback)
	{
		// android assertions are unreliable.
		// assert null != net_udp_callback : "you can not assign null net_udp_callback.";
		this.callback = net_udp_callback;
	}
	// do not assign when holder have some elem.
	public void assign_event_udp_alloc( mm_net_udp_event_udp_alloc event_udp_alloc)
	{
		// android assertions are unreliable.
		// assert null != event_udp_alloc : "you can not assign null event_udp_alloc.";
		this.event_udp_alloc = event_udp_alloc;
	}
	// do not assign when holder have some elem.
	public void assign_crypto_callback(mm_crypto.mm_crypto_callback crypto_callback)
	{
		// android assertions are unreliable.
		// assert null != crypto_callback : "you can not assign null crypto_callback.";
		this.crypto_callback = crypto_callback;
	}
	// assign context handle.
	public void assign_context( Object u )
	{
		this.u = u;
	}
	// assign nonblock_timeout.
	public void assign_nonblock_timeout(int nonblock_timeout)
	{
		this.nonblock_timeout = nonblock_timeout;
	}
	// assign trytimes.
	public void assign_trytimes(int trytimes)
	{
		this.trytimes = trytimes;
	}
	// context for udp will use the mm_addr.u attribute.such as crypto context.
	public void set_addr_context( Object u )
	{
		this.udp.set_context(u);
	}
	public Object get_addr_context()
	{
		return this.udp.get_context();
	}
	public void clear_callback_holder()
	{
		this.holder_locker.lock();
		this.holder.clear();
		this.holder_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	//net_udp crypto encrypt buffer by udp.
	public void crypto_encrypt(mm_udp udp,byte[] buffer,int offset,int length)
	{
		this.crypto_callback.encrypt(this, udp, buffer, offset, buffer, offset, length);
	}
	//net_udp crypto decrypt buffer by udp.
	public void crypto_decrypt(mm_udp udp,byte[] buffer,int offset,int length)
	{
		this.crypto_callback.decrypt(this, udp, buffer, offset, buffer, offset, length);
	}
	//////////////////////////////////////////////////////////////////////////
	//lock to make sure the net_udp is thread safe.
	public void lock()
	{
		this.locker.lock();
	}
	//unlock net_udp.
	public void unlock()
	{
		this.locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	//fopen socket.
	public void fopen_socket()
	{
		this.udp.fopen_socket();
		this.nuclear.fd_add(this.udp.addr.socket,this.udp);
	}
	// bind.
	public void bind()
	{
		mm_addr addr = this.udp.addr;
		if (mm_socket.MM_INVALID_SOCKET != addr.socket)
		{
			int _try_times = 0;
			int rt = -1;
			this.udp_state = net_udp_state_motion;
			// set socket to blocking.
			addr.set_block(mm_socket.MM_BLOCKING);
			do 
			{
				_try_times ++;
				rt = addr.bind();
				if (0 == rt)
				{
					// bind immediately.
					break;
				}
				if ( mm_thread.ts_finish == this.state )
				{
					break;
				}
				else
				{
					mm_logger.logE(TAG + " " + "bind " + addr.to_string() + " failure.try times:" + _try_times);
					mm_time.msleep(MM_NET_UDP_TRY_MSLEEP_TIME);
				}
			} while ( 0 != rt && _try_times < this.trytimes );
			// set socket to not block.
			addr.set_block(mm_socket.MM_NONBLOCK);
			if ( 0 != rt )
			{
				mm_logger.logE(TAG + " " + "bind " + addr.to_string() + " failure.try times:" + _try_times);
				this.apply_broken();
				addr.close_socket();
			}
			else
			{
				this.apply_finish();
				mm_logger.logI(TAG + " " + "bind " + addr.to_string() + " success.try times:" + _try_times);
			}
		}
		else
		{
			mm_logger.logE(TAG + " " + "bind target fd is invalid.");
		}
	}
	// close socket.
	public void close_socket()
	{
		this.nuclear.fd_rmv(this.udp.addr.socket,this.udp);
		this.udp.close_socket();
	}
	// shutdown socket.
	public void shutdown_socket(int opcode)
	{
		this.udp.addr.shutdown_socket(opcode);
	}
	// set socket block.
	public void set_block(int block)
	{
		this.udp.addr.set_block(block);
	}
	// streambuf reset.
	public void streambuf_reset()
	{
		this.udp.streambuf_reset();
	}
	//////////////////////////////////////////////////////////////////////////
	// check state.not thread safe.
	public void check()
	{
		if ( 0 != this.finally_state() )
		{
			// we first disconnect this old connect.
			this.detach_socket();
			// fopen a socket.
			this.fopen_socket();
			// net tcp connect.
			this.bind();
		}
	}
	//////////////////////////////////////////////////////////////////////////
	// get state finally.return 0 for all regular.not thread safe.
	public int finally_state()
	{
		return ( mm_socket.MM_INVALID_SOCKET != this.udp.addr.socket && net_udp_state_finish == this.udp_state ) ? 0 : -1;
	}
	//////////////////////////////////////////////////////////////////////////
	public void apply_broken()
	{
		this.udp_state = net_udp_state_closed;
		this.udp.streambuf_reset();
		this.callback.broken(this.udp);
	}
	public void apply_finish()
	{
		this.udp_state = net_udp_state_finish;
		this.udp.streambuf_reset();
		this.callback.finish(this.udp);
	}
	// synchronize attach to socket.not thread safe.
	public void attach_socket()
	{
		this.bind();
	}
	// synchronize detach to socket.not thread safe.
	public void detach_socket()
	{
		mm_udp udp = this.udp;
		if ( mm_socket.MM_INVALID_SOCKET != udp.addr.socket)
		{
			this.nuclear.fd_rmv(udp.addr.socket,udp);
			// set flag value.
			this.udp_state = net_udp_state_closed;
			// set block state.
			this.set_block(mm_socket.MM_BLOCKING);
			// shutdown socket first.
			this.shutdown_socket(mm_socket.MM_BOTH_SHUTDOWN);
			// close socket.
			this.close_socket();
			// buffer is ivalid.
			this.streambuf_reset();
		}
	}
	//////////////////////////////////////////////////////////////////////////
	//handle send data from buffer and buffer length.>0 is send size success -1 failure.
	//note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
	//only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
	//0 <  rt,means rt buffer is send,we must gbump rt size.
	//0 == rt,means the send buffer can be full.
	//0 >  rt,means the send process can be failure.
	public int buffer_send(byte[] buffer, int offset, int length, mm_sockaddr remote)
	{
		int rt = -1;
		if ( net_udp_state_finish == this.udp_state && mm_socket.MM_INVALID_SOCKET != this.udp.addr.socket )
		{
			rt = this.udp.buffer_send(buffer,offset,length,remote);
			// update the udp state,because the mm_udp_buffer_send can not fire broken event immediately.
			this.udp_state = -1 == rt ? net_udp_state_broken : this.udp_state;
		}
		else
		{
			// net udp nready.fire event.
			this.callback.nready(this.udp);
		}
		return rt;
	}
	// handle send data by flush send buffer.
	// 0 <  rt,means rt buffer is send,we must gbump rt size.
	// 0 == rt,means the send buffer can be full.
	// 0 >  rt,means the send process can be failure.
	public int flush_send()
	{
		int rt = -1;
		if ( net_udp_state_finish == this.udp_state && mm_socket.MM_INVALID_SOCKET != this.udp.addr.socket )
		{
			rt = this.udp.flush_send();
			// update the udp state,because the mm_udp_buffer_send can not fire broken event immediately.
			this.udp_state = -1 == rt ? net_udp_state_broken : this.udp_state;
		}
		else
		{
			// net udp nready.fire event.
			this.callback.nready(this.udp);
		}
		return rt;
	}
	//////////////////////////////////////////////////////////////////////////
	public void start()
	{
		this.state = mm_thread.ts_finish == this.state ? mm_thread.ts_closed : mm_thread.ts_motion;
		this.nuclear.start();
		this.event_udp_alloc.event_udp_alloc(this, this.udp);
	}
	public void interrupt()
	{
		this.state = mm_thread.ts_closed;
		this.nuclear.interrupt();
	}
	public void shutdown()
	{
		this.state = mm_thread.ts_finish;
		this.nuclear.shutdown();
	}
	public void join()
	{
		this.nuclear.join();
		this.event_udp_alloc.event_udp_relax(this, this.udp);
	}
	/////////////////////////////////////////////////////////////
	static final class __net_udp_udp_callback extends mm_udp.mm_udp_callback
	{
		__net_udp_udp_callback(mm_net_udp _p)
		{
			this.obj = _p;
		}
		@Override
		public void handle( Object obj, byte[] buffer, int offset, int length, mm_sockaddr remote )
		{
			mm_udp udp = (mm_udp)(obj);
			mm_net_udp net_udp = (mm_net_udp)(udp.callback.obj);
			net_udp.crypto_decrypt(udp,buffer,offset,length);
			mm_streambuf_packet.handle_udp(udp.buff_recv, buffer, offset, length, __net_udp_udp_handle_packet_callback_impl, udp, remote);
		}
		@Override
		public void broken( Object obj )
		{
			mm_udp udp = (mm_udp)(obj);
			mm_net_udp net_udp = (mm_net_udp)(udp.callback.obj);
			// android assertions are unreliable.
			// assert null != net_udp.callback : "net_udp.callback is a null.";
			net_udp.apply_broken();
		}	
	};
	static final class __net_udp_nuclear_callback extends mm_nuclear.mm_nuclear_callback
	{
		__net_udp_nuclear_callback(mm_net_udp _p)
		{
			this.obj = _p;
		}
		@Override
		public void handle_recv( Object obj, Object u, byte[] buffer, int offset, int max_length )
		{
			mm_udp udp = (mm_udp)(u);
			udp.handle_recv(buffer,offset,max_length);	
		}
		@Override
		public void handle_send( Object obj, Object u, byte[] buffer, int offset, int max_length )
		{
			mm_udp udp = (mm_udp)(u);
			udp.handle_send(buffer,offset,max_length);
		}
	};
	static final class __net_udp_udp_handle_packet_callback implements mm_packet.udp_callBack
	{
		@Override
		public void handle_udp( Object obj, mm_packet pack, mm_sockaddr remote ) 
		{
			mm_udp udp = (mm_udp)(obj);
			mm_net_udp net_udp = (mm_net_udp)(udp.callback.obj);
			mm_net_udp_handle_callback handle = null;
			net_udp.holder_locker.lock();
			handle = net_udp.holder.get(pack.phead.mid);
			net_udp.holder_locker.unlock();
			if (null != handle)
			{
				// fire the handle event.
				handle.handle(udp, net_udp.u, pack, remote);
			}
			else
			{
				// android assertions are unreliable.
				// assert null != net_udp.callback : "net_udp.callback is a null.";
				net_udp.callback.handle(udp, net_udp.u, pack, remote);
			}
		}		
	}
}
