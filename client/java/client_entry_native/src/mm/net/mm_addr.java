package mm.net;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.NotYetBoundException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;

import java.nio.channels.ServerSocketChannel;

import mm.core.mm_errno;
import mm.core.mm_logger;
import mm.core.mm_socket;
import mm.core.mm_spinlock;

// java socket is very different for c socket.
// we catch all Exception e but print nothing but catch the error code.
public class mm_addr
{
	// this type is java special.
	public static final int MM_ADDR_CLIENT = 0;
	public static final int MM_ADDR_SERVER = 1;
	
	public mm_sockaddr ss_native = new mm_sockaddr();
	public mm_sockaddr ss_remote = new mm_sockaddr();	
	public AbstractSelectableChannel socket = null;
	public mm_spinlock locker = new mm_spinlock();
	public Object u = null;// user data.
	
	private int sock_type = mm_socket.SOCK_STREAM;
	private int addr_type = MM_ADDR_CLIENT;// default is client.
	
	public void init()
	{
		this.ss_native.init();
		this.ss_remote.init();
		this.socket = null;
		this.locker.init();
		this.u = null;
		this.sock_type = mm_socket.SOCK_STREAM;
	}
	public void destroy()
	{
		this.ss_native.destroy();
		this.ss_remote.destroy();
		this.socket = null;
		this.locker.destroy();
		this.u = null;
		this.sock_type = mm_socket.SOCK_STREAM;
	}
	public void lock()
	{
		this.locker.lock();
	}
	public void unlock()
	{
		this.locker.unlock();
	}
	// this api is java special.
	public void set_addr_type(int _type)
	{
		this.addr_type = _type;
	}
	// this api is java special.
	public int set_addr_type()
	{
		return this.addr_type;
	}
	// alloc socket fd.
	// tcp PF_INET, SOCK_STREAM, 0
	// udp PF_INET, SOCK_DGRAM , 0
	public void fopen_socket(int domain, int type, int protocol)
	{
		this.close_socket();
		this.sock_type = type;
		if(MM_ADDR_CLIENT == this.addr_type)
		{
			if(mm_socket.SOCK_STREAM == this.sock_type)
			{
				try 
				{
					this.socket = SocketChannel.open();
				} 
				catch (Exception e)
				{
					// real Exception.
					mm_logger.logE("exception: " + e.toString());
				}
			}
			else
			{
				try 
				{
					this.socket = DatagramChannel.open();
				} 
				catch (Exception e) 
				{
					// real Exception.
					mm_logger.logE("exception: " + e.toString());
				}
			}
		}
		else
		{
			try 
			{
				this.socket = ServerSocketChannel.open();
			} 
			catch (Exception e) 
			{
				// real Exception.
				mm_logger.logE("exception: " + e.toString());
			}
		}
	}
	public void close_socket()
	{
		if (mm_socket.MM_INVALID_SOCKET != this.socket)
		{
			try 
			{
				this.socket.close();
			} 
			catch (Exception e) 
			{
				// do nothing here.we only need the finally code.
			}
			this.socket = null;
		}
	}
	// close socket fd.not set socket_t to invalid -1.
	// use to fire socket fd close event.
	public void close_socket_event()
	{
		if (mm_socket.MM_INVALID_SOCKET != this.socket)
		{
			try 
			{
				this.socket.close();
			} 
			catch (Exception e) 
			{
				// do nothing here.we only need the finally code.
			}
		}
	}
	// shutdown socket.
	public void shutdown_socket( int opcode )
	{
		// at java the socket is shutdown.not api.
		// here we just close it.
		if(MM_ADDR_CLIENT == this.addr_type)
		{
			if (mm_socket.MM_INVALID_SOCKET != this.socket)
			{
				if(mm_socket.SOCK_STREAM == this.sock_type)
				{
					SocketChannel _tcp_socket = (SocketChannel) this.socket;
					Socket _socket = _tcp_socket.socket();
					try 
					{
						if(mm_socket.MM_READ_SHUTDOWN == opcode)
						{
							_socket.shutdownInput();
						}
						else if(mm_socket.MM_SEND_SHUTDOWN == opcode)
						{
							_socket.shutdownOutput();
						}
						else
						{
							_socket.shutdownInput();
							_socket.shutdownOutput();
						}
					} 
					catch (Exception e) 
					{
						// do nothing here.we only need the finally code.
					}
				}
				else
				{
					// udp not need shutdown_socket.
				}				
			}
		}
		else
		{
			// java server socket not need shutdown_socket.
		}
	}
	// assign addr native by ip port.AF_INET;
	public void assign_native(String node, int port)
	{
		this.ss_native.assign(node, port);
	}
	// assign addr remote by ip port.AF_INET;
	public void assign_remote(String node, int port)
	{
		this.ss_remote.assign(node, port);
	}
	// assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
	public void assign_native_storage(mm_sockaddr ss_native)
	{
		this.ss_native.copy(ss_native);
	}
	// assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
	public void assign_remote_storage(mm_sockaddr ss_remote)
	{
		this.ss_remote.copy(ss_remote);
	}
	// context for tcp will use the mm_addr.u attribute.such as crypto context.
	public void set_context(Object u)
	{
		this.u = u;
	}
	public Object get_context()
	{
		return this.u;
	}
	// default is blocking.MM_BLOCKING means block MM_NONBLOCK no block.
	public void set_block( int block )
	{
		if(mm_socket.MM_INVALID_SOCKET != this.socket)
		{
			if( block == mm_socket.MM_BLOCKING )
			{
				// socket blocking.
				try 
				{
					this.socket.configureBlocking(true);
				} 
				catch (Exception e) 
				{
					// do nothing here.we only need the finally code.
				}
			}
			else
			{
				// socket nonblock.
				try 
				{
					this.socket.configureBlocking(false);
				} 
				catch (Exception e) 
				{
					// do nothing here.we only need the finally code.
				}
			}
		}
	}
	// connect address.
	public int connect()
	{
		int rt = -1;
		if(MM_ADDR_CLIENT == this.addr_type)
		{
			if(mm_socket.MM_INVALID_SOCKET != this.socket)
			{
				if(mm_socket.SOCK_STREAM == this.sock_type)
				{
					SocketChannel _tcp_socket = (SocketChannel) this.socket;
					// java fopen_socket will try connect here must check state.
					if(!_tcp_socket.isConnected())
					{
						try 
						{
							InetSocketAddress isa = new InetSocketAddress(this.ss_remote.node,this.ss_remote.port);
							rt = false == _tcp_socket.connect(isa) ? -1 : 0;
						} 
						catch (Exception e) 
						{
							// do nothing here.we only need the finally code.
						}
					}
					else
					{
						// it is already connect.
						rt = 0;
					}
				}
				else
				{
					DatagramChannel _udp_socket = (DatagramChannel) this.socket;
					// java fopen_socket will try connect here must check state.
					if(!_udp_socket.isConnected())
					{
						try 
						{
							InetSocketAddress isa = new InetSocketAddress(this.ss_remote.node,this.ss_remote.port);
							_udp_socket.connect(isa);
							rt = 0;
						} 
						catch (Exception e) 
						{
							// do nothing here.we only need the finally code.
						}
					}
					else
					{
						// it is already connect.
						rt = 0;
					}
				}	
			}
		}
		else
		{
			// server socket not need connect anything.
			rt = 0;
		}
		return rt;
	}
	// bind address.
	public int bind()
	{
		int rt = -1;
		if(MM_ADDR_CLIENT == this.addr_type)
		{
			if(mm_socket.MM_INVALID_SOCKET != this.socket)
			{
				if(mm_socket.SOCK_STREAM == this.sock_type)
				{
					SocketChannel _tcp_socket = (SocketChannel) this.socket;
					try 
					{
						InetSocketAddress isa = new InetSocketAddress(this.ss_native.node,this.ss_native.port);
						Socket _socket = _tcp_socket.socket();
						_socket.bind(isa);
						rt = 0;
					} 
					catch (Exception e) 
					{
						// do nothing here.we only need the finally code.
					}
				}
				else
				{
					DatagramChannel _udp_socket = (DatagramChannel) this.socket;
					try 
					{
						InetSocketAddress isa = new InetSocketAddress(this.ss_native.node,this.ss_native.port);
						DatagramSocket _socket = _udp_socket.socket();
						_socket.bind(isa);
						rt = 0;
					} 
					catch (Exception e) 
					{
						// do nothing here.we only need the finally code.
					}
				}	
			}
		}
		else
		{
			// java server socket bind at listen process.
			rt = 0;
		}
		return rt;
	}
	// listen address.
	public int listen( int backlog )
	{
		int rt = -1;
		if(MM_ADDR_CLIENT == this.addr_type)
		{
			// client socket not need listen anything.
			rt = 0;
		}
		else
		{
			if(mm_socket.MM_INVALID_SOCKET != this.socket)
			{
				ServerSocketChannel _tcp_socket = (ServerSocketChannel) this.socket;
				try 
				{
					InetSocketAddress isa = new InetSocketAddress(this.ss_native.node,this.ss_native.port);
					ServerSocket _server_socket = _tcp_socket.socket();
					_server_socket.bind(isa, backlog);
					rt = 0;
				} 
				catch (Exception e) 
				{
					// do nothing here.we only need the finally code.
				}
			}
		}
		return rt;
	}
	// accept address.
	public SocketChannel accept()
	{
		SocketChannel afd = null;
		if(MM_ADDR_CLIENT == this.addr_type)
		{
			// client socket not need accept anything.
			mm_errno.set_code(mm_errno.MM_EINVAL);
		}
		else
		{
			if(mm_socket.MM_INVALID_SOCKET != this.socket)
			{
				ServerSocketChannel _tcp_socket = (ServerSocketChannel) this.socket;				
				try 
				{
					afd = _tcp_socket.accept();
					mm_errno.set_code(0);
				} 
				catch (ClosedByInterruptException e) 
				{
					// do nothing here.we only need the finally code.
					mm_errno.set_code(mm_errno.MM_EINTR);
				}
				catch (AsynchronousCloseException e) 
				{
					// do nothing here.we only need the finally code.
					mm_errno.set_code(mm_errno.MM_ECONNABORTED);
				}
				catch (ClosedChannelException e) 
				{
					// do nothing here.we only need the finally code.
					mm_errno.set_code(mm_errno.MM_ENETRESET);
				}
				catch (NotYetBoundException e) 
				{
					// do nothing here.we only need the finally code.
					mm_errno.set_code(mm_errno.MM_EINVAL);
				}
				catch (SecurityException e) 
				{
					// do nothing here.we only need the finally code.
					mm_errno.set_code(mm_errno.MM_EINVAL);
				}
				catch (IOException e) 
				{
					// do nothing here.we only need the finally code.
					mm_errno.set_code(mm_errno.MM_EIO);
				}
				catch (Exception e) 
				{
					// real Exception.
					mm_logger.logE("exception: " + e.toString());
					mm_errno.set_code(mm_errno.MM_EINVAL);
				}
			}
			else
			{
				// the socket kis a invalid.
				mm_errno.set_code(mm_errno.MM_EINVAL);
			}
		}
		return afd;
	}
	//we not use c style char* but use buffer and offset target the real buffer,because other language might not have pointer.
	public int recv( byte[] buffer, int offset, int length, int flags )
	{
		// android assertions are unreliable.
		// assert mm_socket.SOCK_STREAM == this.sock_type : "mm_socket.SOCK_STREAM == this.d_type is not valid.";
		int rt = -1;
		if(mm_socket.MM_INVALID_SOCKET != this.socket)
		{
			SocketChannel _tcp_socket = (SocketChannel) this.socket;
			try 
			{
				ByteBuffer byte_buffer = ByteBuffer.wrap(buffer, offset, length);
				rt = _tcp_socket.read(byte_buffer);
				mm_errno.set_code(0);
			} 
			catch (NotYetConnectedException e) 
			{
				// do nothing here.we only need the finally code.
				mm_errno.set_code(mm_errno.MM_ENOTCONN);
			}	
			catch (IOException e) 
			{
				// do nothing here.we only need the finally code.
				mm_errno.set_code(mm_errno.MM_EIO);
			}			
			catch (Exception e) 
			{
				// real Exception.
				mm_logger.logE("exception: " + e.toString());
				mm_errno.set_code(mm_errno.MM_EINVAL);
			}
		}
		return rt;
	}
	public int send( byte[] buffer, int offset, int length, int flags )
	{
		// android assertions are unreliable.
		// assert mm_socket.SOCK_STREAM == this.sock_type : "mm_socket.SOCK_STREAM == this.d_type is not valid.";
		int rt = -1;
		if(mm_socket.MM_INVALID_SOCKET != this.socket)
		{
			SocketChannel _tcp_socket = (SocketChannel) this.socket;
			try 
			{
				ByteBuffer byte_buffer = ByteBuffer.wrap(buffer, offset, length);
				rt = _tcp_socket.write(byte_buffer);
				mm_errno.set_code(0);
			} 
			catch (NotYetConnectedException e) 
			{
				// do nothing here.we only need the finally code.
				mm_errno.set_code(mm_errno.MM_ENOTCONN);
			}	
			catch (IOException e) 
			{
				// do nothing here.we only need the finally code.
				mm_errno.set_code(mm_errno.MM_EIO);
			}			
			catch (Exception e) 
			{
				// real Exception.
				mm_logger.logE("exception: " + e.toString());
				mm_errno.set_code(mm_errno.MM_EINVAL);
			}
		}
		return rt;
	}
	public int send_dgram( byte[] buffer, int offset, int length, int flags, mm_sockaddr remote )
	{
		// android assertions are unreliable.
		// assert mm_socket.SOCK_DGRAM == this.sock_type : "mm_socket.SOCK_DGRAM == this.d_type is not valid.";
		int rt = -1;
		if(mm_socket.MM_INVALID_SOCKET != this.socket)
		{
			DatagramChannel _udp_socket = (DatagramChannel) this.socket;
			try 
			{
				ByteBuffer byte_buffer = ByteBuffer.wrap(buffer, offset, length);
				InetSocketAddress isa = new InetSocketAddress(remote.node,remote.port);
				rt = _udp_socket.send(byte_buffer, isa);
				mm_errno.set_code(0);
			} 
			catch (ClosedByInterruptException e) 
			{
				// do nothing here.we only need the finally code.
				mm_errno.set_code(mm_errno.MM_EINTR);
			}
			catch (AsynchronousCloseException e) 
			{
				// do nothing here.we only need the finally code.
				mm_errno.set_code(mm_errno.MM_ECONNABORTED);
			}
			catch (ClosedChannelException e) 
			{
				// do nothing here.we only need the finally code.
				mm_errno.set_code(mm_errno.MM_ENETRESET);
			}
			catch (SecurityException e) 
			{
				// do nothing here.we only need the finally code.
				mm_errno.set_code(mm_errno.MM_EINVAL);
			}
			catch (IOException e) 
			{
				// do nothing here.we only need the finally code.
				mm_errno.set_code(mm_errno.MM_EIO);
			}
			catch (Exception e) 
			{
				// real Exception.
				mm_logger.logE("exception: " + e.toString());
				mm_errno.set_code(mm_errno.MM_EINVAL);
			}
		}
		return rt;	
	}
	public int recv_dgram( byte[] buffer, int offset, int length, int flags, mm_sockaddr remote )
	{
		// android assertions are unreliable.
		// assert mm_socket.SOCK_DGRAM == this.sock_type : "mm_socket.SOCK_DGRAM == this.d_type is not valid.";
		int rt = -1;
		if(mm_socket.MM_INVALID_SOCKET != this.socket)
		{
			DatagramChannel _udp_socket = (DatagramChannel) this.socket;
			try 
			{
				ByteBuffer byte_buffer = ByteBuffer.wrap(buffer, offset, length);
				InetSocketAddress isa = (InetSocketAddress)_udp_socket.receive(byte_buffer);
				if(null == isa)
				{
					mm_errno.set_code(mm_errno.MM_EAGAIN);
				}
				else
				{
					remote.node = isa.getAddress().getHostAddress();
					remote.port = isa.getPort();
					rt = byte_buffer.position();
					mm_errno.set_code(0);
				}
			}
			catch (ClosedByInterruptException e) 
			{
				// do nothing here.we only need the finally code.
				mm_errno.set_code(mm_errno.MM_EINTR);
			}
			catch (AsynchronousCloseException e) 
			{
				// do nothing here.we only need the finally code.
				mm_errno.set_code(mm_errno.MM_ECONNABORTED);
			}
			catch (ClosedChannelException e) 
			{
				// do nothing here.we only need the finally code.
				mm_errno.set_code(mm_errno.MM_ENETRESET);
			}
			catch (SecurityException e) 
			{
				// do nothing here.we only need the finally code.
				mm_errno.set_code(mm_errno.MM_EINVAL);
			}
			catch (IOException e) 
			{
				// do nothing here.we only need the finally code.
				mm_errno.set_code(mm_errno.MM_EIO);
			}
			catch (Exception e) 
			{
				// real Exception.
				mm_logger.logE("exception: " + e.toString());
				mm_errno.set_code(mm_errno.MM_EINVAL);
			}	
		}
		return rt;
	}
	// (fd)|ip-port --> ip-port
	// ipv6 (65535)|2001:0DB8:0000:0023:0008:0800:200C:417A-65535 --> 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
	// ipv4                         (65535)|192.168.111.123-65535 --> 192.168.111.123-65535
	public String to_string()
	{
		return mm_sockaddr.sockaddr_storage_native_remote_string(this.ss_native,this.ss_remote,this.hashCode());
	}
};
