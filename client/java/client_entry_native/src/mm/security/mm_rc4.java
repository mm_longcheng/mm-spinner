package mm.security;

public class mm_rc4 
{
	// sbox length.
	public static final int SBOX_LENGTH = 256;
    
	public int x;
	public int y;
	public int[] data = new int[SBOX_LENGTH];

    public void init()
    {
    	this.x = 0;
    	this.y = 0;
    	java.util.Arrays.fill(this.data, 0, SBOX_LENGTH, (byte) 0);
    }
    
    public void destroy()
    {
    	this.x = 0;
    	this.y = 0;
    	java.util.Arrays.fill(this.data, 0, SBOX_LENGTH, (byte) 0);
    }
    
    public String options()
    {
    	return ("rc4(int)");
    }
    
    private static class __sk_data
    {
    	public int tmp = 0;
    	public int id1 = 0;
    	public int id2 = 0;
    	public int[] d = null;
    	//
    	public byte[] buffer = null;
    	public int offset = 0;
    	public int length = 0;
    }
    /*-
	 * #define SK_LOOP(d,n) { \
	 *       tmp=d[(n)]; \
	 *       id2 = (data[id1] + tmp + id2) & 0xff; \
	 *       if (++id1 == len) id1=0; \
	 *       d[(n)]=d[id2]; \
	 *       d[id2]=tmp; }
     */
    private static void __sk_loop( __sk_data m, int[] d, int n )
    {
        m.tmp=d[n];
        m.id2 = (m.buffer[m.offset+m.id1] + m.tmp + m.id2) & 0xff;
        if (++m.id1 == m.length) m.id1=0;
        d[n]=d[m.id2];
        d[m.id2]=m.tmp;
    }
    /*-
     * RC4 as implemented from a posting from
     * Newsgroups: sci.crypt
     * From: sterndark@netcom.com (David Sterndark)
     * Subject: RC4 Algorithm revealed.
     * Message-ID: <sternCvKL4B.Hyy@netcom.com>
     * Date: Wed, 14 Sep 1994 06:35:31 GMT
     */
    public void set_key( byte[] buffer , int offset, int length )
    {
    	__sk_data m = new __sk_data();
    	int i = 0;
    	//
        m.d = this.data;
        this.x = 0;
        this.y = 0;
        m.id1 = m.id2 = 0;
        //
        m.buffer = buffer;
        m.offset = offset;
        m.length = length;
        //
        for (i = 0; i < SBOX_LENGTH; i++)
        {
        	m.d[i] = i;
        }
        for (i = 0; i < SBOX_LENGTH; i += 4) 
        {
        	__sk_loop(m, m.d, i + 0);
        	__sk_loop(m, m.d, i + 1);
        	__sk_loop(m, m.d, i + 2);
        	__sk_loop(m, m.d, i + 3);
        }
    }

    private static class __coder_data
    {    	
    	public int[] d = null;
    	public int x = 0;
    	public int y = 0;
    	public int tx = 0;
    	public int ty = 0;
    }
    /*-
	 * #define LOOP(in,out) \
	 *    x=((x+1)&0xff); \
	 *    tx=d[x]; \
	 *    y=(tx+y)&0xff; \
	 *    d[x]=ty=d[y]; \
	 *    d[y]=tx; \
	 *    (out) = d[(tx+ty)&0xff]^ (in);
	 */
    private static void __coder_loop( __coder_data m, byte[] buffer_i, int offset_i, byte[] buffer_o, int offset_o )
    {
        m.x=((m.x+1)&0xff);
        m.tx=m.d[m.x];
        m.y=(m.tx+m.y)&0xff;
        m.d[m.x]=m.ty=m.d[m.y];
        m.d[m.y]=m.tx;
        buffer_o[offset_o] = (byte) (m.d[(m.tx+m.ty)&0xff] ^ (buffer_i[offset_i]));
    }
    /*-
     * RC4 as implemented from a posting from
     * Newsgroups: sci.crypt
     * From: sterndark@netcom.com (David Sterndark)
     * Subject: RC4 Algorithm revealed.
     * Message-ID: <sternCvKL4B.Hyy@netcom.com>
     * Date: Wed, 14 Sep 1994 06:35:31 GMT
     */
    public void RC4(int len, byte[] buffer_i, int offset_i, byte[] buffer_o, int offset_o)
    {
    	__coder_data m = new __coder_data();
    	int i = 0;
    	//
    	m.x = this.x;
    	m.y = this.y;
    	m.d = this.data;

        i = len >> 3;
        if ( 0 != i ) 
        {
            for (;;) 
            {
            	__coder_loop(m, buffer_i, offset_i + 0, buffer_o, offset_o + 0);
            	__coder_loop(m, buffer_i, offset_i + 1, buffer_o, offset_o + 1);
            	__coder_loop(m, buffer_i, offset_i + 2, buffer_o, offset_o + 2);
            	__coder_loop(m, buffer_i, offset_i + 3, buffer_o, offset_o + 3);
            	__coder_loop(m, buffer_i, offset_i + 4, buffer_o, offset_o + 4);
            	__coder_loop(m, buffer_i, offset_i + 5, buffer_o, offset_o + 5);
            	__coder_loop(m, buffer_i, offset_i + 6, buffer_o, offset_o + 6);
            	__coder_loop(m, buffer_i, offset_i + 7, buffer_o, offset_o + 7);
            	offset_i += 8;
            	offset_o += 8;
                if (--i == 0)
                {
                	break;
                }   
            }
        }
        i = len & 0x07;
        if ( 0 != i ) 
        {
            for (;;) 
            {
            	__coder_loop(m, buffer_i, offset_i + 0, buffer_o, offset_o + 0);
                if (--i == 0)
                    break;
                __coder_loop(m, buffer_i, offset_i + 1, buffer_o, offset_o + 1);
                if (--i == 0)
                    break;
                __coder_loop(m, buffer_i, offset_i + 2, buffer_o, offset_o + 2);
                if (--i == 0)
                    break;
                __coder_loop(m, buffer_i, offset_i + 3, buffer_o, offset_o + 3);
                if (--i == 0)
                    break;
                __coder_loop(m, buffer_i, offset_i + 4, buffer_o, offset_o + 4);
                if (--i == 0)
                    break;
                __coder_loop(m, buffer_i, offset_i + 5, buffer_o, offset_o + 5);
                if (--i == 0)
                    break;
                __coder_loop(m, buffer_i, offset_i + 6, buffer_o, offset_o + 6);
                if (--i == 0)
                    break;
            }
        }
        this.x = m.x;
        this.y = m.y;
    }

    public void encrypt(byte[] buffer_i, int offset_i, byte[] buffer_o, int offset_o, int length) 
    {
    	this.RC4(length, buffer_i, offset_i, buffer_o, offset_o);
    }
    
    public void decrypt(byte[] buffer_i, int offset_i, byte[] buffer_o, int offset_o, int length) 
    {
    	this.RC4(length, buffer_i, offset_i, buffer_o, offset_o);
    }
}
