package activity;

import activity.mm_client_entry_model.mm_model;
import activity.mm_client_entry_model.mm_model_account;

import cobweb.mm_protobuf;
import mm.core.mm_logger;
import mm.net.mm_gl_tcp;
import mm.net.mm_net_tcp;
import mm.net.mm_packet;
import protodef.nano.c_account;

public class mm_client_entry_tcp_c_account_hd
{
	public static void mm_gl_tcp_assign_c_account_callback( mm_gl_tcp opengl_tcp)
	{
		opengl_tcp.assign_q_callback(c_account.login_by_token_rs.id,new hd_client_entry_q_c_account_login_by_token_rs());
		opengl_tcp.assign_q_callback(c_account.logout_rs.id,new hd_client_entry_q_c_account_logout_rs());
		opengl_tcp.assign_q_callback(c_account.kickoff_nt.id,new hd_client_entry_q_c_account_kickoff_nt());
	}
	//////////////////////////////////////////////////////////////////////////
	static class hd_client_entry_q_c_account_login_by_token_rs implements mm_net_tcp.mm_net_tcp_handle_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack ) 
		{
			c_account.login_by_token_rs rs_msg = new c_account.login_by_token_rs();
			StringBuilder proto_desc = new StringBuilder();			
			if(0 == mm_protobuf.decode_message(pack, rs_msg))
			{
				mm_protobuf.logger_append_packet_message(proto_desc, pack, rs_msg);
				mm_logger.logI("q tcp rs: " + proto_desc.toString());
				// handle event.
				mm_model model = (mm_model)(u);
				//////////////////////////////////////////////////////////////////////////
				mm_model_account model_account = model.d_account;
				//////////////////////////////////////////////////////////////////////////
				model.d_account.d_uid = rs_msg.uid;
				model_account.evt_uid_update.fire_event(model_account);
				//////////////////////////////////////////////////////////////////////////
				model.d_account.d_state = mm_model_account.account_login;
				model_account.evt_state_update.fire_event(model_account);
				//////////////////////////////////////////////////////////////////////////
			}
			else
			{
				mm_protobuf.logger_append_packet(proto_desc, pack);
				mm_logger.logW("q tcp rs: " + proto_desc.toString());
			}
		}
	}
	static class hd_client_entry_q_c_account_logout_rs implements mm_net_tcp.mm_net_tcp_handle_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack ) 
		{
			c_account.login_by_token_rs rs_msg = new c_account.login_by_token_rs();
			StringBuilder proto_desc = new StringBuilder();			
			if(0 == mm_protobuf.decode_message(pack, rs_msg))
			{
				mm_protobuf.logger_append_packet_message(proto_desc, pack, rs_msg);
				mm_logger.logI("q tcp rs: " + proto_desc.toString());
				// handle event.
				mm_model model = (mm_model)(u);
				//////////////////////////////////////////////////////////////////////////
				mm_model_account model_account = model.d_account;
				//////////////////////////////////////////////////////////////////////////
				model.d_account.d_state = mm_model_account.account_logout;
				model_account.evt_state_update.fire_event(model_account);
				//////////////////////////////////////////////////////////////////////////
				model.d_account.d_uid = 0;
				model_account.evt_uid_update.fire_event(model_account);
				//////////////////////////////////////////////////////////////////////////
			}
			else
			{
				mm_protobuf.logger_append_packet(proto_desc, pack);
				mm_logger.logW("q tcp rs: " + proto_desc.toString());
			}
		}
	}
	static class hd_client_entry_q_c_account_kickoff_nt implements mm_net_tcp.mm_net_tcp_handle_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack ) 
		{
			c_account.kickoff_nt nt_msg = new c_account.kickoff_nt();
			StringBuilder proto_desc = new StringBuilder();			
			if(0 == mm_protobuf.decode_message(pack, nt_msg))
			{
				mm_protobuf.logger_append_packet_message(proto_desc, pack, nt_msg);
				mm_logger.logI("q tcp nt: " + proto_desc.toString());
				// handle event.
				mm_model model = (mm_model)(u);
				//////////////////////////////////////////////////////////////////////////
				mm_model_account model_account = model.d_account;
				//////////////////////////////////////////////////////////////////////////
				model.d_account.d_state = mm_model_account.account_kickoff;
				model_account.evt_state_update.fire_event(model_account);
				//////////////////////////////////////////////////////////////////////////
				model.d_account.d_uid = 0;
				model_account.evt_uid_update.fire_event(model_account);
				//////////////////////////////////////////////////////////////////////////
			}
			else
			{
				mm_protobuf.logger_append_packet(proto_desc, pack);
				mm_logger.logW("q tcp nt: " + proto_desc.toString());
			}
		}
	}
}
