package activity;

import activity.mm_client_entry_model.mm_model;
import cobweb.mm_protobuf;
import mm.core.mm_logger;
import mm.net.mm_gl_udp;
import mm.net.mm_packet;
import protodef.nano.b_math;
import protodef.nano.c_entry;

public class mm_client_entry_udp_c_entry_rq
{
	public static void hd_client_entry_q_c_entry_knock_rq(mm_gl_udp gl_udp, mm_model model)
	{
		c_entry.knock_rq rq_msg = new c_entry.knock_rq();
		mm_packet rq_pack = new mm_packet();
		StringBuilder proto_desc = new StringBuilder();
		//
		long uid = model.d_account.d_uid;
		long msec_current = System.currentTimeMillis();
		String source_version = Long.toString(msec_current);
		////////////////////////////////
		b_math.coord coord_info = new b_math.coord();
		rq_msg.coordInfo = coord_info;
		coord_info.j = model.d_account.d_coord_j;
		coord_info.w = model.d_account.d_coord_w;
		rq_msg.nativeClientVersion = model.d_version.d_native_client_version;
		rq_msg.nativeSourceVersion = source_version;
		//////////////////////////////////////////////////////////////////////////
		// send rq.
		mm_protobuf.q_udp_flush_message_append( gl_udp, uid, c_entry.knock_rq.id, rq_msg, rq_pack );
		mm_protobuf.q_udp_flush_signal(gl_udp);
		// logger rq.
		mm_protobuf.logger_append_packet_message( proto_desc, rq_pack, rq_msg );
		mm_logger.logI("q tcp rq: " + proto_desc);
	}
}
