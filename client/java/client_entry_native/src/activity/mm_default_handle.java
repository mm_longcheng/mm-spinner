package activity;

import mm.core.mm_logger;

import mm.net.mm_net_udp;
import mm.net.mm_net_tcp;
import mm.net.mm_gl_udp;
import mm.net.mm_gl_tcp;
import mm.net.mm_mailbox;
import mm.net.mm_mq_tcp;
import mm.net.mm_mq_udp;
import mm.net.mm_packet;
import mm.net.mm_sockaddr;
import mm.net.mm_tcp;
import mm.net.mm_udp;

public class mm_default_handle
{
	static class mm_net_udp_default_callback extends mm_net_udp.mm_net_udp_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack, mm_sockaddr remote )
		{
			mm_udp udp = (mm_udp)(obj);
			String message = String.format("mid:0x%08X udp:%s not handle.",pack.phead.mid,udp.addr.to_string());
			mm_logger.logW(message);
		}
		@Override
		public void broken( Object obj )
		{
			mm_udp udp = (mm_udp)(obj);
			String message = String.format("udp:%s is broken.",udp.addr.to_string());
			mm_logger.logW(message);
		}
		@Override
		public void nready( Object obj )
		{
			mm_udp udp = (mm_udp)(obj);
			String message = String.format("udp:%s is nready.",udp.addr.to_string());
			mm_logger.logW(message);
		}
		@Override
		public void finish( Object obj )
		{
			mm_udp udp = (mm_udp)(obj);
			String message = String.format("udp:%s is finish.",udp.addr.to_string());
			mm_logger.logI(message);
		}
	}
	static class mm_net_tcp_default_callback extends mm_net_tcp.mm_net_tcp_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("mid:0x%08X tcp:%s not handle.",pack.phead.mid,tcp.addr.to_string());
			mm_logger.logW(message);
		}
		@Override
		public void broken( Object obj )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("tcp:%s is broken.",tcp.addr.to_string());
			mm_logger.logW(message);
		}
		@Override
		public void nready( Object obj )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("tcp:%s is nready.",tcp.addr.to_string());
			mm_logger.logW(message);
		}
		@Override
		public void finish( Object obj )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("tcp:%s is finish.",tcp.addr.to_string());
			mm_logger.logI(message);
		}
	}
	static class mm_mailbox_default_callback extends mm_mailbox.mm_mailbox_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("mid:0x%08X tcp:%s not handle.",pack.phead.mid,tcp.addr.to_string());
			mm_logger.logW(message);
		}
		@Override
		public void broken( Object obj )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("tcp:%s is broken.",tcp.addr.to_string());
			mm_logger.logW(message);
		}
		@Override
		public void nready( Object obj )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("tcp:%s is nready.",tcp.addr.to_string());
			mm_logger.logW(message);
		}
		@Override
		public void finish( Object obj )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("tcp:%s is finish.",tcp.addr.to_string());
			mm_logger.logI(message);
		}
	}
	static class mm_contact_default_callback extends mm_net_tcp.mm_net_tcp_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("mid:0x%08X tcp:%s not handle.",pack.phead.mid,tcp.addr.to_string());
			mm_logger.logW(message);
		}
		@Override
		public void broken( Object obj )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("tcp:%s is broken.",tcp.addr.to_string());
			mm_logger.logW(message);
		}
		@Override
		public void nready( Object obj )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("tcp:%s is nready.",tcp.addr.to_string());
			mm_logger.logW(message);
		}
		@Override
		public void finish( Object obj )
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("tcp:%s is finish.",tcp.addr.to_string());
			mm_logger.logI(message);
		}
	}

	static class mm_gl_udp_handle_default extends mm_mq_udp.mm_mq_udp_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack, mm_sockaddr remote ) 
		{
			mm_udp udp = (mm_udp)(obj);
			String message = String.format("mid:0x%08X udp:%s not handle.",pack.phead.mid,udp.addr.to_string());
			mm_logger.logW(message);
		}
	}
	static class mm_gl_udp_broken_default extends mm_gl_udp.mm_gl_udp_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack, mm_sockaddr remote ) 
		{
			mm_udp udp = (mm_udp)(obj);
			String message = String.format("udp:%s is broken.",udp.addr.to_string());
			mm_logger.logW(message);
		}
	}
	static class mm_gl_udp_nready_default extends mm_gl_udp.mm_gl_udp_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack, mm_sockaddr remote ) 
		{
			mm_udp udp = (mm_udp)(obj);
			String message = String.format("udp:%s is nready.",udp.addr.to_string());
			mm_logger.logW(message);
		}
	}
	static class mm_gl_udp_finish_default extends mm_gl_udp.mm_gl_udp_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack, mm_sockaddr remote ) 
		{
			mm_udp udp = (mm_udp)(obj);
			String message = String.format("udp:%s is finish.",udp.addr.to_string());
			mm_logger.logI(message);
		}
	}

	static class mm_gl_tcp_handle_default extends mm_mq_tcp.mm_mq_tcp_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack ) 
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("mid:0x%08X tcp:%s not handle.",pack.phead.mid,tcp.addr.to_string());
			mm_logger.logW(message);
		}
	}
	static class mm_gl_tcp_broken_default extends mm_gl_tcp.mm_gl_tcp_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack ) 
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("tcp:%s is broken.",tcp.addr.to_string());
			mm_logger.logW(message);
		}
	}
	static class mm_gl_tcp_nready_default extends mm_gl_tcp.mm_gl_tcp_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack ) 
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("tcp:%s is nready.",tcp.addr.to_string());
			mm_logger.logW(message);
		}
	}
	static class mm_gl_tcp_finish_default extends mm_gl_tcp.mm_gl_tcp_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack ) 
		{
			mm_tcp tcp = (mm_tcp)(obj);
			String message = String.format("tcp:%s is finish.",tcp.addr.to_string());
			mm_logger.logI(message);
		}
	}
}
