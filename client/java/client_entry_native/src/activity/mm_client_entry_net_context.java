package activity;

import java.util.Timer;

import activity.mm_client_entry_model.mm_model;
import activity.mm_client_entry_net_entry.mm_net_entry;
import activity.mm_client_entry_net_lobby.mm_net_lobby;

public class mm_client_entry_net_context
{
	public static class mm_net_context
	{
		public mm_net_entry net_entry = new mm_net_entry();
		public mm_net_lobby net_lobby = new mm_net_lobby();
		public mm_model model = null;// weak ref.
		
		public void init()
		{
			this.net_entry.init();
			this.net_lobby.init();
			this.model = null;
		}
		public void destroy()
		{
			this.net_entry.destroy();
			this.net_lobby.destroy();
			this.model = null;
		}
		public void assign_addr_entry( String node, int port)
		{
			this.net_entry.assign_addr_entry(node, port);
		}
		public void assign_model( mm_model model)
		{
			this.net_entry.assign_model(model);
			this.net_lobby.assign_model(model);
		}
		public void mount_timer( Timer timer)
		{
			this.net_entry.mount_timer(timer);
			this.net_lobby.mount_timer(timer);
		}
		public void start()
		{
			this.net_entry.start();
			this.net_lobby.start();
		}
		public void interrupt()
		{
			this.net_entry.interrupt();
			this.net_lobby.interrupt();
		}
		public void shutdown()
		{
			this.net_entry.shutdown();
			this.net_lobby.shutdown();
		}
		public void join()
		{
			this.net_entry.join();
			this.net_lobby.join();
		}
	}
}
