package activity;

import java.io.UnsupportedEncodingException;

import activity.mm_client_entry_model.mm_model;

import cobweb.mm_protobuf;
import mm.core.mm_logger;
import mm.net.mm_gl_tcp;
import mm.net.mm_packet;
import protodef.nano.c_account;
import sun.misc.BASE64Encoder; 

public class mm_client_entry_tcp_c_account_rq
{
	public static void hd_client_entry_q_c_account_login_by_token_rq( mm_gl_tcp gl_tcp, mm_model model)
	{
		c_account.login_by_token_rq rq_msg = new c_account.login_by_token_rq();
		mm_packet rq_pack = new mm_packet();
		StringBuilder proto_desc = new StringBuilder();	
		//
		long uid = model.d_account.d_uid;
		long msec_current = System.currentTimeMillis();
		////////////////////////////////
		byte[] delims_byte = {-62,-89};
		String delims = null;
		try 
		{
			delims = new String(delims_byte, "UTF-8");
		} 
		catch (UnsupportedEncodingException e) 
		{
			delims = "";
		}
		BASE64Encoder b64 = new BASE64Encoder();
		
		String bufplain2 = uid + delims.toString() + msec_current + delims.toString() + msec_current;
		String bufplain1 = b64.encode(bufplain2.getBytes());
		String bufcoded1 = b64.encode(bufplain1.getBytes());
		//
		rq_msg.uid = uid;
		rq_msg.account = "mm";
		rq_msg.token = bufcoded1;
		rq_msg.kind = "pc";
		rq_msg.loginType = 0;
		rq_msg.anonymous = 0;
		rq_msg.sha1Pass = "xx";
		rq_msg.vid = "0xff";
		rq_msg.clientInfo = "java 0.0.0";
		rq_msg.clientVersion = 0;
		//////////////////////////////////////////////////////////////////////////
		// send rq.
		mm_protobuf.q_tcp_flush_message_append( gl_tcp, uid, c_account.login_by_token_rq.id, rq_msg, rq_pack );
		mm_protobuf.q_tcp_flush_signal(gl_tcp);
		// logger rq.
		mm_protobuf.logger_append_packet_message( proto_desc, rq_pack, rq_msg );
		mm_logger.logI("q tcp rq: " + proto_desc);
	}
	public static void hd_client_entry_q_c_account_logout_rq( mm_gl_tcp gl_tcp, mm_model model)
	{
		c_account.logout_rq rq_msg = new c_account.logout_rq();
		mm_packet rq_pack = new mm_packet();
		StringBuilder proto_desc = new StringBuilder();
		//
		long uid = model.d_account.d_uid;
		////////////////////////////////
		rq_msg.uid = uid;
		//////////////////////////////////////////////////////////////////////////
		// send rq.
		mm_protobuf.q_tcp_flush_message_append( gl_tcp, uid, c_account.logout_rq.id, rq_msg, rq_pack );
		mm_protobuf.q_tcp_flush_signal(gl_tcp);
		// logger rq.
		mm_protobuf.logger_append_packet_message( proto_desc, rq_pack, rq_msg );
		mm_logger.logI("q tcp rq: " + proto_desc);
	}

}
