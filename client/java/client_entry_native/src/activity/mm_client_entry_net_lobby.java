package activity;

import java.util.Timer;
import java.util.TimerTask;

import activity.mm_client_entry_model.mm_model;
import activity.mm_client_entry_model.mm_model_net_tcp;
import activity.mm_client_entry_model.mm_model_net_udp;
// import mm.core.mm_logger;
import mm.net.mm_gl_tcp;
import mm.net.mm_gl_udp;

public class mm_client_entry_net_lobby
{
	public static class mm_net_lobby
	{
		public static final int MM_NET_LOBBY_MSEC_UPDATE_UDP = 50;
		public static final int MM_NET_LOBBY_MSEC_UPDATE_TCP = 50;
		public static final int MM_NET_LOBBY_MSEC_UPDATE_BAT = 180000;
		
		public mm_gl_udp opengl_udp = new mm_gl_udp();// opengl udp
		public mm_gl_tcp opengl_tcp = new mm_gl_tcp();// opengl tcp
		public int msec_update_lobby_udp = 0;
		public int msec_update_lobby_tcp = 0;
		public int msec_update_lobby_bat = 0;
		public mm_model model = null;// weak ref.
		
		mm_event_motion.mm_event_callback _udp_state_update_callback = new __static_event_motion_lobby_udp_state_update_callback(this);
		mm_event_motion.mm_event_callback _udp_address_update_callback = new __static_event_motion_lobby_udp_address_update_callback(this);
		mm_event_motion.mm_event_callback _tcp_state_update_callback = new __static_event_motion_lobby_tcp_state_update_callback(this);
		mm_event_motion.mm_event_callback _tcp_address_update_callback = new __static_event_motion_lobby_tcp_address_update_callback(this);
		
		public void init()
		{
			this.opengl_udp.init();
			this.opengl_tcp.init();
			this.msec_update_lobby_udp = MM_NET_LOBBY_MSEC_UPDATE_UDP;
			this.msec_update_lobby_tcp = MM_NET_LOBBY_MSEC_UPDATE_TCP;
			this.msec_update_lobby_bat = MM_NET_LOBBY_MSEC_UPDATE_BAT;
			this.model = null;
			
			this.opengl_tcp.set_addr_context(this);

			this.opengl_tcp.assign_q_default_callback(new mm_default_handle.mm_gl_tcp_handle_default());
			this.opengl_udp.assign_q_default_callback(new mm_default_handle.mm_gl_udp_handle_default());
			//
			this.opengl_tcp.assign_state_check_flag(mm_gl_udp.gl_udp_check_activate);
			this.opengl_udp.assign_state_check_flag(mm_gl_tcp.gl_tcp_check_activate);
			//
			mm_client_entry_net_lobby_state_hd.mm_client_entry_net_lobby_assign_state_callback(this);
		}
		public void destroy()
		{
			this.opengl_udp.init();
			this.opengl_tcp.init();
			this.msec_update_lobby_udp = MM_NET_LOBBY_MSEC_UPDATE_UDP;
			this.msec_update_lobby_tcp = MM_NET_LOBBY_MSEC_UPDATE_TCP;
			this.msec_update_lobby_bat = MM_NET_LOBBY_MSEC_UPDATE_BAT;
			this.model = null;
		}
		public void assign_model( mm_model model)
		{
			// unlink.
			if (null != this.model)
			{
				this.model.d_net_lobby.d_udp.evt_state_update.rmv(this._udp_state_update_callback);
				this.model.d_net_lobby.d_udp.evt_address_update.rmv(this._udp_address_update_callback);
				this.model.d_net_lobby.d_tcp.evt_state_update.rmv(this._tcp_state_update_callback);
				this.model.d_net_lobby.d_tcp.evt_address_update.rmv(this._tcp_address_update_callback);
			}
			// assign ref value.
			this.model = model;
			this.opengl_udp.assign_context(this.model);
			this.opengl_tcp.assign_context(this.model);
			// first init.
			if (null != this.model)
			{
				this._udp_state_update_callback.fire_event(this.model.d_net_lobby.d_udp.evt_state_update, this, this.model.d_net_lobby.d_udp);
				this._udp_address_update_callback.fire_event(this.model.d_net_lobby.d_udp.evt_address_update, this, this.model.d_net_lobby.d_udp);
				this._tcp_state_update_callback.fire_event(this.model.d_net_lobby.d_tcp.evt_state_update, this, this.model.d_net_lobby.d_tcp);
				this._tcp_address_update_callback.fire_event(this.model.d_net_lobby.d_tcp.evt_address_update, this, this.model.d_net_lobby.d_tcp);
			}
			// link.
			if (null != this.model)
			{
				this.model.d_net_lobby.d_udp.evt_state_update.add(this._udp_state_update_callback);
				this.model.d_net_lobby.d_udp.evt_address_update.add(this._udp_address_update_callback);
				this.model.d_net_lobby.d_tcp.evt_state_update.add(this._tcp_state_update_callback);
				this.model.d_net_lobby.d_tcp.evt_address_update.add(this._tcp_address_update_callback);			}
		}
		public void mount_timer( Timer timer)
		{
			timer.schedule(new __static_net_lobby_msec_update_udp_handle(this), 10, this.msec_update_lobby_udp);
			timer.schedule(new __static_net_lobby_msec_update_tcp_handle(this), 10, this.msec_update_lobby_tcp);
			timer.schedule(new __static_net_lobby_msec_update_bat_handle(this), 10, this.msec_update_lobby_bat);
		}
		public void start()
		{
			this.opengl_udp.start();
			this.opengl_tcp.start();
		}
		public void interrupt()
		{
			this.opengl_udp.interrupt();
			this.opengl_tcp.interrupt();
		}
		public void shutdown()
		{
			this.opengl_udp.shutdown();
			this.opengl_tcp.shutdown();
		}
		public void join()
		{
			this.opengl_udp.join();
			this.opengl_tcp.join();
		}
		//////////////////////////////////////////////////////////////////////////
		public static class __static_net_lobby_msec_update_udp_handle extends TimerTask
		{
			public static final String TAG = __static_net_lobby_msec_update_udp_handle.class.getSimpleName();
			public mm_net_lobby obj = null;// weak ref.
			__static_net_lobby_msec_update_udp_handle(mm_net_lobby obj)
			{
				this.obj = obj;
			}
			@Override
			public void run() 
			{
				this.obj.opengl_udp.thread_handle();
				// mm_logger.logT(TAG + " update.");
			}
		}
		public static class __static_net_lobby_msec_update_tcp_handle extends TimerTask
		{
			public static final String TAG = __static_net_lobby_msec_update_tcp_handle.class.getSimpleName();
			public mm_net_lobby obj = null;// weak ref.
			__static_net_lobby_msec_update_tcp_handle(mm_net_lobby obj)
			{
				this.obj = obj;
			}
			@Override
			public void run() 
			{
				this.obj.opengl_tcp.thread_handle();
				// mm_logger.logT(TAG + " update.");
			}
		}
		public static class __static_net_lobby_msec_update_bat_handle extends TimerTask
		{
			public static final String TAG = __static_net_lobby_msec_update_bat_handle.class.getSimpleName();
			public mm_net_lobby obj = null;// weak ref.
			__static_net_lobby_msec_update_bat_handle(mm_net_lobby obj)
			{
				this.obj = obj;
			}
			@Override
			public void run() 
			{
				mm_net_lobby net_lobby = (mm_net_lobby)(obj);
				mm_model_net_udp model_net_udp = this.obj.model.d_net_lobby.d_udp;
				mm_model_net_tcp model_net_tcp = this.obj.model.d_net_lobby.d_tcp;
				if ( mm_model_net_tcp.opengl_tcp_state_finish == model_net_tcp.d_state && mm_model_net_udp.opengl_udp_state_finish == model_net_udp.d_state)
				{
					mm_client_entry_tcp_c_entry_rq.hd_client_entry_q_c_entry_heartbeat_rq(net_lobby.opengl_tcp,net_lobby.model);
				}
				// mm_logger.logT(TAG + " update.");
			}
		}
		//////////////////////////////////////////////////////////////////////////
		public static class __static_event_motion_lobby_udp_state_update_callback extends mm_event_motion.mm_event_callback
		{
			public static final String TAG = __static_event_motion_lobby_udp_state_update_callback.class.getSimpleName();
			__static_event_motion_lobby_udp_state_update_callback(mm_net_lobby obj)
			{
				this.obj = obj;
			}
			public void fire_event( Object p, Object obj, Object event)
			{
				
			}
		}
		public static class __static_event_motion_lobby_udp_address_update_callback extends mm_event_motion.mm_event_callback
		{
			public static final String TAG = __static_event_motion_lobby_udp_address_update_callback.class.getSimpleName();
			__static_event_motion_lobby_udp_address_update_callback(mm_net_lobby obj)
			{
				this.obj = obj;
			}
			public void fire_event( Object p, Object obj, Object event)
			{
				mm_net_lobby net_lobby = (mm_net_lobby)(obj);
				mm_model_net_udp model_net_udp = net_lobby.model.d_net_lobby.d_udp;
				if ( mm_model_net_udp.opengl_udp_state_finish != model_net_udp.d_state)
				{
					if (false == model_net_udp.d_node.isEmpty() && 0 != model_net_udp.d_port)
					{
						net_lobby.opengl_udp.assign_remote_target(model_net_udp.d_node,model_net_udp.d_port);
						net_lobby.opengl_udp.state_signal();
					}
				}
			}
		}
		public static class __static_event_motion_lobby_tcp_state_update_callback extends mm_event_motion.mm_event_callback
		{
			public static final String TAG = __static_event_motion_lobby_tcp_state_update_callback.class.getSimpleName();
			__static_event_motion_lobby_tcp_state_update_callback(mm_net_lobby obj)
			{
				this.obj = obj;
			}
			public void fire_event( Object p, Object obj, Object event)
			{
				mm_net_lobby net_lobby = (mm_net_lobby)(obj);
				mm_model_net_udp model_net_udp = net_lobby.model.d_net_lobby.d_udp;
				mm_model_net_tcp model_net_tcp = net_lobby.model.d_net_lobby.d_tcp;
				if ( mm_model_net_tcp.opengl_tcp_state_broken == model_net_tcp.d_state && mm_model_net_udp.opengl_udp_state_finish == model_net_udp.d_state)
				{
					mm_client_entry_udp_c_entry_rq.hd_client_entry_q_c_entry_knock_rq(net_lobby.opengl_udp,net_lobby.model);
				}
				if ( mm_model_net_tcp.opengl_tcp_state_finish == model_net_tcp.d_state && mm_model_net_udp.opengl_udp_state_finish == model_net_udp.d_state)
				{
					mm_client_entry_tcp_c_account_rq.hd_client_entry_q_c_account_login_by_token_rq(net_lobby.opengl_tcp,net_lobby.model);
				}
			}
		}
		public static class __static_event_motion_lobby_tcp_address_update_callback extends mm_event_motion.mm_event_callback
		{
			public static final String TAG = __static_event_motion_lobby_tcp_address_update_callback.class.getSimpleName();
			__static_event_motion_lobby_tcp_address_update_callback(mm_net_lobby obj)
			{
				this.obj = obj;
			}
			public void fire_event( Object p, Object obj, Object event)
			{
				mm_net_lobby net_lobby = (mm_net_lobby)(obj);
				mm_model_net_tcp model_net_tcp = net_lobby.model.d_net_lobby.d_tcp;
				if ( mm_model_net_tcp.opengl_tcp_state_finish != model_net_tcp.d_state)
				{
					if (false == model_net_tcp.d_node.isEmpty() && 0 != model_net_tcp.d_port)
					{
						net_lobby.opengl_tcp.assign_remote_target(model_net_tcp.d_node,model_net_tcp.d_port);
						net_lobby.opengl_tcp.state_signal();
					}
				}
			}
		}
	};
}
