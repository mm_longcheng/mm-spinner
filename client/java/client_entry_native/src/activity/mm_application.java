package activity;

import mm.core.mm_logger;
import mm.net.mm_mailbox;

public class mm_application 
{
	private static final String TAG = mm_application.class.getSimpleName();
	
	public String[] arg = null;
	// alloc.
	public mm_client_entry impl = new mm_client_entry();
	
	public void init()
	{
		mm_logger.assign_callback(new mm_logger_callback_printf());
		// init.
		this.impl.init();
		mm_logger.logI(TAG + " init.");
	}
	public void destroy()
	{
		mm_logger.logI(TAG + " destroy.");
		// destroy.
		this.impl.destroy();
	}

	public void initialize(String[] args)
	{
		this.arg = args;
		this.impl.assign_addr_target("47.93.118.164", 20100);
		// this.impl.assign_addr_target("127.0.0.1", 30100);
		this.impl.assign_assign_uid(10000018);
	}
	public void start()
	{
		this.impl.start();
		mm_logger.logI(TAG + " start.");
	}
	public void interrupt()
	{
		this.impl.interrupt();
		mm_logger.logI(TAG + " interrupt.");
	}
	public void shutdown()
	{
		this.impl.shutdown();
		mm_logger.logI(TAG + " shutdown.");
	}
	public void join()
	{
		this.impl.join();
		mm_logger.logI(TAG + " join.");
	}
}
