package activity;

import java.util.Timer;

import activity.mm_client_entry_model.mm_model;
import activity.mm_client_entry_model.mm_model_account;
import activity.mm_client_entry_net_context.mm_net_context;

public class mm_client_entry
{
	public mm_model model = new mm_model();
	public mm_net_context net_context = new mm_net_context();
	public Timer opengl_timer = new Timer();
	
	public void init()
	{
		this.model.init();
		this.net_context.init();
		// this.opengl_timer.init();
		//
		this.net_context.assign_model(this.model);
		// this.net_context.mount_timer(this.opengl_timer);
	}
	public void destroy()
	{
		this.model.destroy();
		this.net_context.destroy();
		// this.opengl_timer.destroy();
	}
	public void assign_addr_target(String node, int port)
	{
		this.net_context.assign_addr_entry(node, port);
	}
	public void assign_assign_uid(long uid)
	{
		mm_model_account model_account = this.model.d_account;
		model_account.d_uid = uid;
		model_account.evt_uid_update.fire_event(model_account);
	}

	public void start()
	{
		this.net_context.start();
		this.net_context.mount_timer(this.opengl_timer);
	}
	public void interrupt()
	{
		this.net_context.interrupt();
	}
	public void shutdown()
	{
		this.net_context.shutdown();
	}
	public void join()
	{
		this.net_context.join();
	}
}
