package activity;

import activity.mm_client_entry_model.mm_model;
import activity.mm_client_entry_model.mm_model_net_tcp;
import cobweb.mm_protobuf;
import mm.core.mm_logger;
import mm.net.mm_gl_udp;
import mm.net.mm_net_udp;
import mm.net.mm_packet;
import mm.net.mm_sockaddr;
import protodef.nano.c_entry;

public class mm_client_entry_udp_c_entry_hd
{
	public static void mm_gl_udp_assign_c_entry_callback(mm_gl_udp opengl_udp)
	{
		opengl_udp.assign_q_callback(c_entry.knock_rs.id,new hd_client_entry_q_c_entry_knock_rs());
	}
	//////////////////////////////////////////////////////////////////////////
	static class hd_client_entry_q_c_entry_knock_rs implements mm_net_udp.mm_net_udp_handle_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack, mm_sockaddr remote ) 
		{
			c_entry.knock_rs rs_msg = new c_entry.knock_rs();
			StringBuilder proto_desc = new StringBuilder();			
			if(0 == mm_protobuf.decode_message(pack, rs_msg))
			{
				mm_protobuf.logger_append_packet_message(proto_desc, pack, rs_msg);
				mm_logger.logI("q tcp nt: " + proto_desc.toString());
				// handle event.
				mm_model model = (mm_model)(u);
				//////////////////////////////////////////////////////////////////////////
				mm_model_net_tcp model_net_tcp = model.d_net_lobby.d_tcp;
				model_net_tcp.d_node = rs_msg.addr.host;
				model_net_tcp.d_port = rs_msg.addr.port;
				model_net_tcp.evt_address_update.fire_event(model_net_tcp);
				//////////////////////////////////////////////////////////////////////////
			}
			else
			{
				mm_protobuf.logger_append_packet(proto_desc, pack);
				mm_logger.logW("q tcp nt: " + proto_desc.toString());
			}
		}
	}
}
