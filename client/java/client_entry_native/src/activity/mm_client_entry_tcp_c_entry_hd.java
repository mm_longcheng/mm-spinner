package activity;

import activity.mm_client_entry_model.mm_model;
import activity.mm_client_entry_model.mm_model_net_lobby;
import cobweb.mm_protobuf;
import mm.core.mm_logger;
import mm.net.mm_gl_tcp;
import mm.net.mm_net_tcp;
import mm.net.mm_packet;
import protodef.nano.c_entry;

public class mm_client_entry_tcp_c_entry_hd
{
	public static void mm_gl_tcp_assign_c_entry_callback(mm_gl_tcp opengl_tcp)
	{
		opengl_tcp.assign_q_callback(c_entry.heartbeat_rs.id,new hd_client_entry_q_c_entry_heartbeat_rs());
	}
	static class hd_client_entry_q_c_entry_heartbeat_rs implements mm_net_tcp.mm_net_tcp_handle_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack ) 
		{
			c_entry.heartbeat_rs rs_msg = new c_entry.heartbeat_rs();
			StringBuilder proto_desc = new StringBuilder();			
			if(0 == mm_protobuf.decode_message(pack, rs_msg))
			{
				mm_protobuf.logger_append_packet_message(proto_desc, pack, rs_msg);
				mm_logger.logI("q tcp nt: " + proto_desc.toString());
				// handle event.
				mm_model model = (mm_model)(u);
				//////////////////////////////////////////////////////////////////////////
				mm_model_net_lobby model_net_lobby = model.d_net_lobby;
				model_net_lobby.d_heartbeat_timecode_native = rs_msg.timecodeNative;
				model_net_lobby.d_heartbeat_timecode_remote = rs_msg.timecodeRemote;
				model_net_lobby.evt_heartbeat_update.fire_event(model_net_lobby);
				//////////////////////////////////////////////////////////////////////////
			}
			else
			{
				mm_protobuf.logger_append_packet(proto_desc, pack);
				mm_logger.logW("q tcp nt: " + proto_desc.toString());
			}
		}
	}
}
