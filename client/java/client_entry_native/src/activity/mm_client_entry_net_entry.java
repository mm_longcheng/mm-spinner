package activity;

import java.util.Timer;

import activity.mm_client_entry_model.mm_model;
import activity.mm_client_entry_model.mm_model_net_udp;

public class mm_client_entry_net_entry
{
	public static class mm_net_entry
	{
		public mm_model model = null;// weak ref.
		
		public void init()
		{
			this.model = null;
		}
		public void destroy()
		{
			this.model = null;
		}
		public void assign_addr_entry( String node, int port)
		{
			mm_model_net_udp model_net_udp = this.model.d_net_lobby.d_udp;
			model_net_udp.d_node = node;
			model_net_udp.d_port = port;
			model_net_udp.evt_address_update.fire_event(model_net_udp);
		}
		public void assign_model( mm_model model)
		{
			this.model = model;
		}
		public void mount_timer( Timer timer)
		{
			this.model = null;
		}
		public void start()
		{
			this.model = null;
		}
		public void interrupt()
		{
			this.model = null;
		}
		public void shutdown()
		{
			this.model = null;
		}
		public void join()
		{
			this.model = null;
		}
	};
}
