package activity;

import java.util.Iterator;
import java.util.LinkedList;

// simplify event motion.not thread safe.
// rmv is not working.
public class mm_event_motion
{
	public static class mm_event_callback
	{	
		public void fire_event( Object p, Object obj, Object event){}
		Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	}
	
	public LinkedList<mm_event_callback> callback_list = new LinkedList<mm_event_callback>();
	
	public void init()
	{
		this.callback_list.clear();
	}
	public void destroy()
	{
		this.callback_list.clear();
	}
	
	public void add(mm_event_callback event_functor)
	{
		this.callback_list.addLast(event_functor);
	}
	public void rmv(mm_event_callback event_functor)
	{
		this.callback_list.remove(event_functor);
	}
	
	public void fire_event(Object event)
	{
		mm_event_callback event_functor = null;
		Iterator<mm_event_callback> it = null;
		it = this.callback_list.iterator();	
        while(it.hasNext())
        {
        	event_functor = it.next();
        	if(null != event_functor)
        	{
        		event_functor.fire_event(this, event_functor.obj, event);
        	}
        }
	}
}
