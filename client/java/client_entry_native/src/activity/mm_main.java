package activity;

import mm.core.mm_logger;

public class mm_main
{
	private static final String TAG = mm_main.class.getSimpleName();
	static class __signal_destroy extends Thread 
	{
		public mm_application application = null;
		__signal_destroy(mm_application application)
		{
		
		}
		public void run() 
		{
			application.shutdown();
			mm_logger.logI(TAG + "signal_destroy.");
		} 
	};
	public static void main(String[] args) 
	{
		mm_application app = new mm_application(); 
		Runtime.getRuntime().addShutdownHook(new __signal_destroy(app));
		try 
		{
			app.init();
			app.initialize(args);
			app.start();
			app.join();
			app.destroy();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
    }
}
