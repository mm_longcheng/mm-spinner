package activity;

public class mm_client_entry_model
{
	public static class mm_model_version
	{
		// data
		public String d_native_client_version = new String();
		public String d_native_source_version = new String();
		// event
		public mm_event_motion evt_version_update = new mm_event_motion();
		
		public void init()
		{
			this.d_native_client_version = "";
			this.d_native_source_version = "";
			this.evt_version_update.init();
		}
		public void destroy()
		{
			this.d_native_client_version = "";
			this.d_native_source_version = "";
			this.evt_version_update.destroy();
		}
	};
	//////////////////////////////////////////////////////////////////////////
	public static class mm_model_account
	{
		public static final int account_logout = 0;
		public static final int account_login = 1;
		public static final int account_kickoff = 2;
		// data
		public long d_uid = 0;
		public int d_state = 0;
		public double d_coord_j = 0;
		public double d_coord_w = 0;
		// event
		public mm_event_motion evt_uid_update = new mm_event_motion();
		public mm_event_motion evt_state_update = new mm_event_motion();
		
		public void init()
		{
			this.d_uid = 0;
			this.d_state = account_logout;
			this.d_coord_j = 0;
			this.d_coord_w = 0;
			this.evt_uid_update.init();
			this.evt_state_update.init();
		}
		public void destroy()
		{
			this.d_uid = 0;
			this.d_state = account_logout;
			this.d_coord_j = 0;
			this.d_coord_w = 0;
			this.evt_uid_update.destroy();
			this.evt_state_update.destroy();
		}
	};
	//////////////////////////////////////////////////////////////////////////
	public static class mm_model_net_tcp
	{
		public static final int opengl_tcp_state_broken = 0x0F000000;
		public static final int opengl_tcp_state_nready = 0x0F000001;
		public static final int opengl_tcp_state_finish = 0x0F000002;
		// data
		public String d_node = new String();
		public int d_port = 0;
		public int d_state = 0;
		// event
		public mm_event_motion evt_address_update = new mm_event_motion();
		public mm_event_motion evt_state_update = new mm_event_motion();
		
		public void init()
		{
			this.d_node = "";
			this.d_port = 0;
			this.d_state = opengl_tcp_state_broken;
			this.evt_address_update.init();
			this.evt_state_update.init();
		}
		public void destroy()
		{
			this.d_node = "";
			this.d_port = 0;
			this.d_state = opengl_tcp_state_broken;
			this.evt_address_update.destroy();
			this.evt_state_update.destroy();
		}
	};
	//////////////////////////////////////////////////////////////////////////
	public static class mm_model_net_udp
	{
		public static final int opengl_udp_state_broken = 0x0F000000;
		public static final int opengl_udp_state_nready = 0x0F000001;
		public static final int opengl_udp_state_finish = 0x0F000002;
		// data
		public String d_node = new String();
		public int d_port = 0;
		public int d_state = 0;
		// event
		public mm_event_motion evt_address_update = new mm_event_motion();
		public mm_event_motion evt_state_update = new mm_event_motion();
		
		public void init()
		{
			this.d_node = "";
			this.d_port = 0;
			this.d_state = opengl_udp_state_broken;
			this.evt_address_update.init();
			this.evt_state_update.init();
		}
		public void destroy()
		{
			this.d_node = "";
			this.d_port = 0;
			this.d_state = opengl_udp_state_broken;
			this.evt_address_update.destroy();
			this.evt_state_update.destroy();
		}
	};
	//////////////////////////////////////////////////////////////////////////
	public static class mm_model_net_entry
	{
		// data
		public int d_unique_id = 0;// entry unique_id.
		// event
		public mm_event_motion evt_unique_id_update = new mm_event_motion();
		
		public void init()
		{
			this.d_unique_id = 0;
			this.evt_unique_id_update.init();
		}
		public void destroy()
		{
			this.d_unique_id = 0;
			this.evt_unique_id_update.destroy();
		}
	};
	//////////////////////////////////////////////////////////////////////////
	public static class mm_model_net_lobby
	{
		// data
		public long d_heartbeat_timecode_native = 0;
		public long d_heartbeat_timecode_remote = 0;
		// model
		public mm_model_net_tcp d_tcp = new mm_model_net_tcp();
		public mm_model_net_udp d_udp = new mm_model_net_udp();
		// event
		public mm_event_motion evt_heartbeat_update = new mm_event_motion();
		
		public void init()
		{
			this.d_heartbeat_timecode_native = 0;
			this.d_heartbeat_timecode_remote = 0;
			this.d_tcp.init();
			this.d_udp.init();
			this.evt_heartbeat_update.init();
		}
		public void destroy()
		{
			this.d_heartbeat_timecode_native = 0;
			this.d_heartbeat_timecode_remote = 0;
			this.d_tcp.destroy();
			this.d_udp.destroy();
			this.evt_heartbeat_update.destroy();
		}
	};
	//////////////////////////////////////////////////////////////////////////
	public static class mm_model
	{
		// model
		public mm_model_version d_version = new mm_model_version();
		public mm_model_account d_account = new mm_model_account();
		public mm_model_net_entry d_net_entry = new mm_model_net_entry();
		public mm_model_net_lobby d_net_lobby = new mm_model_net_lobby();
		
		public void init()
		{
			this.d_version.init();
			this.d_account.init();
			this.d_net_entry.init();
			this.d_net_lobby.init();
		}
		public void destroy()
		{
			this.d_version.destroy();
			this.d_account.destroy();
			this.d_net_entry.destroy();
			this.d_net_lobby.destroy();
		}
	};
}
