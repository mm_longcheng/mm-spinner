package activity;

import activity.mm_client_entry_model.mm_model;
import activity.mm_client_entry_model.mm_model_net_tcp;
import activity.mm_client_entry_model.mm_model_net_udp;
import activity.mm_client_entry_net_lobby.mm_net_lobby;

import mm.net.mm_net_tcp;
import mm.net.mm_net_udp;
import mm.net.mm_packet;
import mm.net.mm_sockaddr;
import mm.net.mm_tcp;
import mm.net.mm_udp;

public class mm_client_entry_net_lobby_state_hd
{
	public static void mm_client_entry_net_lobby_assign_state_callback( mm_net_lobby impl)
	{
		impl.opengl_udp.assign_q_callback(mm_udp.udp_mid_broken,new hd_client_entry_net_lobby_udp_mid_broken());
		impl.opengl_udp.assign_q_callback(mm_udp.udp_mid_nready,new hd_client_entry_net_lobby_udp_mid_nready());
		impl.opengl_udp.assign_q_callback(mm_udp.udp_mid_finish,new hd_client_entry_net_lobby_udp_mid_finish());
		
		impl.opengl_tcp.assign_q_callback(mm_tcp.tcp_mid_broken,new hd_client_entry_net_lobby_tcp_mid_broken());
		impl.opengl_tcp.assign_q_callback(mm_tcp.tcp_mid_nready,new hd_client_entry_net_lobby_tcp_mid_nready());
		impl.opengl_tcp.assign_q_callback(mm_tcp.tcp_mid_finish,new hd_client_entry_net_lobby_tcp_mid_finish());
		
		mm_client_entry_udp_c_entry_hd.mm_gl_udp_assign_c_entry_callback(impl.opengl_udp);
		mm_client_entry_tcp_c_entry_hd.mm_gl_tcp_assign_c_entry_callback(impl.opengl_tcp);
		mm_client_entry_tcp_c_account_hd.mm_gl_tcp_assign_c_account_callback(impl.opengl_tcp);
	}

	static class hd_client_entry_net_lobby_udp_mid_broken implements mm_net_udp.mm_net_udp_handle_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet rs_pack, mm_sockaddr remote ) 
		{
			mm_model model = (mm_model)(u);
			mm_model_net_udp model_net_udp = model.d_net_lobby.d_udp;
			model_net_udp.d_state = mm_model_net_udp.opengl_udp_state_broken;
			model_net_udp.evt_state_update.fire_event(model_net_udp);
			//
			mm_default_handle.mm_gl_udp_broken_default hd = new mm_default_handle.mm_gl_udp_broken_default();
			hd.handle(obj, u, rs_pack, remote);
		}
	}
	static class hd_client_entry_net_lobby_udp_mid_nready implements mm_net_udp.mm_net_udp_handle_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet rs_pack, mm_sockaddr remote ) 
		{
			mm_model model = (mm_model)(u);
			mm_model_net_udp model_net_udp = model.d_net_lobby.d_udp;
			model_net_udp.d_state = mm_model_net_udp.opengl_udp_state_nready;
			model_net_udp.evt_state_update.fire_event(model_net_udp);
			//
			mm_default_handle.mm_gl_udp_nready_default hd = new mm_default_handle.mm_gl_udp_nready_default();
			hd.handle(obj, u, rs_pack, remote);
		}
	}
	static class hd_client_entry_net_lobby_udp_mid_finish implements mm_net_udp.mm_net_udp_handle_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet rs_pack, mm_sockaddr remote ) 
		{
			mm_model model = (mm_model)(u);
			mm_model_net_udp model_net_udp = model.d_net_lobby.d_udp;
			model_net_udp.d_state = mm_model_net_udp.opengl_udp_state_finish;
			model_net_udp.evt_state_update.fire_event(model_net_udp);
			//
			mm_default_handle.mm_gl_udp_finish_default hd = new mm_default_handle.mm_gl_udp_finish_default();
			hd.handle(obj, u, rs_pack, remote);
		}
	}
	
	static class hd_client_entry_net_lobby_tcp_mid_broken implements mm_net_tcp.mm_net_tcp_handle_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet rs_pack ) 
		{
			mm_model model = (mm_model)(u);
			mm_model_net_tcp model_net_tcp = model.d_net_lobby.d_tcp;
			model_net_tcp.d_state = mm_model_net_tcp.opengl_tcp_state_broken;
			model_net_tcp.evt_state_update.fire_event(model_net_tcp);
			//
			mm_default_handle.mm_gl_tcp_broken_default hd = new mm_default_handle.mm_gl_tcp_broken_default();
			hd.handle(obj, u, rs_pack);
		}
	}
	static class hd_client_entry_net_lobby_tcp_mid_nready implements mm_net_tcp.mm_net_tcp_handle_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet rs_pack ) 
		{
			mm_model model = (mm_model)(u);
			mm_model_net_tcp model_net_tcp = model.d_net_lobby.d_tcp;
			model_net_tcp.d_state = mm_model_net_tcp.opengl_tcp_state_nready;
			model_net_tcp.evt_state_update.fire_event(model_net_tcp);
			//
			mm_default_handle.mm_gl_tcp_nready_default hd = new mm_default_handle.mm_gl_tcp_nready_default();
			hd.handle(obj, u, rs_pack);
		}
	}
	static class hd_client_entry_net_lobby_tcp_mid_finish implements mm_net_tcp.mm_net_tcp_handle_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet rs_pack ) 
		{
			mm_model model = (mm_model)(u);
			mm_model_net_tcp model_net_tcp = model.d_net_lobby.d_tcp;
			model_net_tcp.d_state = mm_model_net_tcp.opengl_tcp_state_finish;
			model_net_tcp.evt_state_update.fire_event(model_net_tcp);
			//
			mm_default_handle.mm_gl_tcp_finish_default hd = new mm_default_handle.mm_gl_tcp_finish_default();
			hd.handle(obj, u, rs_pack);
		}
	}
}
