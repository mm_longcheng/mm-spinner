package cobweb;

import com.google.protobuf.nano.CodedInputByteBufferNano;
import com.google.protobuf.nano.MessageNano;

import mm.core.mm_logger;
import mm.core.mm_streambuf;
import mm.net.mm_gl_tcp;
import mm.net.mm_gl_udp;
import mm.net.mm_mailbox;
import mm.net.mm_net_tcp;
import mm.net.mm_net_udp;
import mm.net.mm_packet;
import mm.net.mm_sockaddr;
import mm.net.mm_streambuf_packet;
import mm.net.mm_tcp;
import mm.net.mm_udp;

public class mm_protobuf
{
	// max length for cut out.
	public static final int MM_MAX_MESSAGE_LOGGER_LENGTH = 1024;
	// min lenght for not return.
	public static final int MM_MIN_MESSAGE_LOGGER_LENGTH = 64;
	
	public static void logger_append_packet(StringBuilder value, mm_packet pack)
	{
		value.append("uid:" + pack.phead.uid + " mid:0x" + Integer.toHexString(pack.phead.mid));
	}
	public static void logger_append_message(StringBuilder value, MessageNano message)
	{
		// append the message class name first.
		value.append(message.getClass().getSimpleName());
		//
		String message_string = message.toString();
		int bsz = message.getSerializedSize();
		int ssz = message_string.length();
		if (MM_MIN_MESSAGE_LOGGER_LENGTH >= ssz)
		{
			// actually we not need '\n' any where.
			// if need look pretty.shut down this line.
			message_string = message_string.replace('\n', ' ');
		}
		// append binary size and string size.
		value.append(" [" + bsz + "][" + ssz + "]:= ");
		// if message string is to long.we cut it.
		if(MM_MAX_MESSAGE_LOGGER_LENGTH < ssz)
		{
			value.append(message_string.substring(0, MM_MAX_MESSAGE_LOGGER_LENGTH));
			value.append("...");
		}
		else
		{
			value.append(message_string);
		}
	}
	public static void logger_append_packet_message(StringBuilder value, mm_packet pack, MessageNano message)
	{
		logger_append_packet(value, pack);
		value.append(" ");
		logger_append_message(value, message);
	}
	// decode message form mm_packet.
	// return code.
	//   -1 is failure.
	//	  0 is success.
	public static int decode_message(mm_packet pack, MessageNano message)
	{
		int rt = -1;
		CodedInputByteBufferNano data = CodedInputByteBufferNano.newInstance(pack.bbuff.buffer, pack.bbuff.offset, pack.bbuff.length);
		try 
		{
			message.mergeFrom(data);
			rt = 0;
		} 
		catch (Exception e) 
		{
			// real Exception.
			mm_logger.logI("exception: " + e.toString());
		}
		return rt;
	}
	// encode message message --> mm_packet.
	// return code.
	//   -1 is failure.
	//	  0 is success.
	public static int encode_message(mm_packet pack, MessageNano message)
	{
		int rt = -1;
		try 
		{
			MessageNano.toByteArray(message, pack.bbuff.buffer, pack.bbuff.offset, pack.bbuff.length);
			rt = 0;
		} 
		catch (Exception e) 
		{
			// real Exception.
			mm_logger.logI("exception: " + e.toString());
		}
		return rt;
	}
	// encode message message --> mm_packet.
	// return code.
	//   -1 is failure.
	//	  0 is success.
	public static int encode_message_streambuf_packet(mm_streambuf streambuf, MessageNano message, mm_packet pack, int mid, long sid, long uid)
	{
		int rt = -1;
		pack.hbuff.length = mm_packet.MM_MSG_COMM_HEAD_SIZE;
		pack.bbuff.length = message.getSerializedSize();
		mm_streambuf_packet.overdraft(streambuf, pack);
		pack.head_base_zero();
		pack.phead.mid = mid;
		pack.phead.sid = sid;
		pack.phead.uid = uid;
		pack.head_base_encode();
		rt = mm_protobuf.encode_message(pack,message);
		return rt;
	}

	public static int n_tcp_append_rs( mm_mailbox mailbox, mm_tcp tcp, int mid,MessageNano rs_msg, mm_packet rq_pack, mm_packet rs_pack )
	{
		int rt = -1;
		rs_pack.hbuff.length = rq_pack.hbuff.length;
		rs_pack.bbuff.length = rs_msg.getSerializedSize();
		mm_streambuf_packet.overdraft(tcp.buff_send,rs_pack);
		rs_pack.head_base_copy(rq_pack);
		rs_pack.phead.mid = mid;
		rs_pack.head_base_encode();
		rt = mm_protobuf.encode_message(rs_pack,rs_msg);
		mailbox.crypto_encrypt(tcp,rs_pack.bbuff.buffer, rs_pack.bbuff.offset, rs_pack.bbuff.length);
		return rt;
	}
	public static int n_udp_append_rs( mm_net_udp net_udp, mm_udp udp, int mid,MessageNano rs_msg, mm_packet rq_pack, mm_packet rs_pack )
	{
		int rt = -1;
		rs_pack.hbuff.length = rq_pack.hbuff.length;
		rs_pack.bbuff.length = rs_msg.getSerializedSize();
		mm_streambuf_packet.overdraft(udp.buff_send,rs_pack);
		rs_pack.head_base_copy(rq_pack);
		rs_pack.phead.mid = mid;
		rs_pack.head_base_encode();
		rt = mm_protobuf.encode_message(rs_pack,rs_msg);
		net_udp.crypto_encrypt(udp,rs_pack.bbuff.buffer, rs_pack.bbuff.offset, rs_pack.bbuff.length);
		return rt;
	}
	public static int q_tcp_append_rq( mm_net_tcp net_tcp,long uid, int mid,MessageNano rq_msg, mm_packet rq_pack )
	{
		int rt = -1;
		mm_tcp tcp = net_tcp.tcp;
		rq_pack.hbuff.length = mm_packet.MM_MSG_COMM_HEAD_SIZE;
		rq_pack.bbuff.length = rq_msg.getSerializedSize();
		mm_streambuf_packet.overdraft(tcp.buff_send, rq_pack);
		rq_pack.head_base_zero();
		rq_pack.phead.mid = mid;
		rq_pack.phead.uid = uid;
		rq_pack.head_base_encode();
		rt = mm_protobuf.encode_message(rq_pack,rq_msg);
		net_tcp.crypto_encrypt(tcp,rq_pack.bbuff.buffer, rq_pack.bbuff.offset, rq_pack.bbuff.length);
		return rt;
	}
	public static int q_udp_append_rq( mm_net_udp net_udp,long uid, int mid,MessageNano rq_msg, mm_packet rq_pack )
	{
		int rt = -1;
		mm_udp udp = net_udp.udp;
		rq_pack.hbuff.length = mm_packet.MM_MSG_COMM_HEAD_SIZE;
		rq_pack.bbuff.length = rq_msg.getSerializedSize();
		mm_streambuf_packet.overdraft(udp.buff_send, rq_pack);
		rq_pack.head_base_zero();
		rq_pack.phead.mid = mid;
		rq_pack.phead.uid = uid;
		rq_pack.head_base_encode();
		rt = mm_protobuf.encode_message(rq_pack,rq_msg);
		net_udp.crypto_encrypt(udp,rq_pack.bbuff.buffer, rq_pack.bbuff.offset, rq_pack.bbuff.length);
		return rt;
	}
	public static int n_tcp_append_nt( mm_mailbox maibox, mm_tcp tcp, long uid, int mid, MessageNano nt_msg, mm_packet nt_pack)
	{
		int rt = -1;
		nt_pack.hbuff.length = mm_packet.MM_MSG_COMM_HEAD_SIZE;
		nt_pack.bbuff.length = nt_msg.getSerializedSize();
		mm_streambuf_packet.overdraft(tcp.buff_send, nt_pack);
		nt_pack.head_base_zero();
		nt_pack.phead.mid = mid;
		nt_pack.phead.uid = uid;
		nt_pack.head_base_encode();
		rt = mm_protobuf.encode_message(nt_pack,nt_msg);
		maibox.crypto_encrypt(tcp,nt_pack.bbuff.buffer, nt_pack.bbuff.offset, nt_pack.bbuff.length);
		return rt;
	}
	// proxy append
	// net_tcp t_tcp is target.rq_pack rq pack pr_pack proxy pack.
	public static int p_tcp_append_rq( mm_net_tcp net_tcp,mm_tcp t_tcp,int pid,long sid, mm_packet rq_pack,mm_packet pr_pack )
	{
		pr_pack.hbuff.length = rq_pack.hbuff.length;
		pr_pack.bbuff.length = rq_pack.bbuff.length;
		mm_streambuf_packet.overdraft(t_tcp.buff_send,pr_pack);
		pr_pack.head_base_copy(rq_pack);
		pr_pack.phead.pid = pid;
		pr_pack.phead.sid = sid;
		pr_pack.head_base_encode();
		System.arraycopy(rq_pack.bbuff.buffer, rq_pack.bbuff.offset,pr_pack.bbuff.buffer, pr_pack.bbuff.offset,rq_pack.bbuff.length);
		net_tcp.crypto_encrypt(t_tcp,pr_pack.bbuff.buffer, pr_pack.bbuff.offset, pr_pack.bbuff.length);
		return 0;
	}
	// proxy append
	// mailbox t_tcp is target.rs_pack rs pack pr_pack proxy pack.
	public static int p_tcp_append_rs( mm_mailbox mailbox,mm_tcp t_tcp, mm_packet rs_pack, mm_packet pr_pack )
	{
		pr_pack.hbuff.length = rs_pack.hbuff.length;
		pr_pack.bbuff.length = rs_pack.bbuff.length;
		mm_streambuf_packet.overdraft(t_tcp.buff_send, pr_pack);
		pr_pack.head_base_copy(rs_pack);
		pr_pack.head_base_encode();
		System.arraycopy(rs_pack.bbuff.buffer, rs_pack.bbuff.offset,pr_pack.bbuff.buffer, pr_pack.bbuff.offset,rs_pack.bbuff.length);
		mailbox.crypto_encrypt(t_tcp,pr_pack.bbuff.buffer, pr_pack.bbuff.offset, pr_pack.bbuff.length);
		return 0;
	}
	// lock it outside manual.use for only mm_mailbox send.
	public static int n_tcp_flush_send( mm_tcp tcp )
	{
		int rt = -1;
		rt = tcp.flush_send();
		return rt;
	}
	// lock it outside manual.use for only mm_net_udp send.
	public static int n_udp_flush_send( mm_udp udp )
	{
		int rt = -1;
		rt = udp.flush_send();
		return rt;
	}
	// lock it outside manual.use for client.
	public static int n_net_tcp_flush_send( mm_net_tcp net_tcp )
	{
		int rt = -1;
		rt = net_tcp.flush_send();
		return rt;
	}
	// lock it outside manual.use for client.
	public static int n_net_udp_flush_send( mm_net_udp net_udp )
	{
		int rt = -1;
		rt = net_udp.flush_send();
		return rt;
	}
	
	public static int n_udp_buffer_send_reset( mm_udp udp )
	{
		udp.buff_send.reset();
		return 0;
	}
	public static int n_udp_buffer_send_flush( mm_udp udp, mm_sockaddr remote)
	{
		int rt = -1;
		mm_streambuf streambuf = udp.buff_send;
		int sz = streambuf.size();
		rt = udp.buffer_send(udp.buff_send.buff, udp.buff_send.gptr, sz, remote);
		// we not need check why udp send error.just gbump the buffer.
		udp.buff_send.gbump(sz);
		return rt;
	}
	// thread safe encode and append message.
	public static int q_tcp_flush_message_append( mm_gl_tcp gl_tcp, long uid, int mid, MessageNano rq_msg, mm_packet rq_pack)
	{
		int rt = -1;
		gl_tcp.flush_lock();
		rt = q_tcp_append_rq(gl_tcp.net_tcp,uid,mid,rq_msg,rq_pack);
		gl_tcp.flush_unlock();
		return rt;
	}
	public static int q_udp_flush_message_append( mm_gl_udp gl_udp, long uid, int mid, MessageNano rq_msg, mm_packet rq_pack)
	{
		int rt = -1;
		gl_udp.flush_lock();
		rt = q_udp_append_rq(gl_udp.net_udp,uid,mid,rq_msg,rq_pack);
		gl_udp.flush_unlock();
		return rt;
	}
	public static int q_tcp_flush_signal( mm_gl_tcp gl_tcp )
	{
		gl_tcp.flush_signal();
		return 0;
	}
	public static int q_udp_flush_signal( mm_gl_udp gl_udp )
	{
		gl_udp.flush_signal();
		return 0;
	}
}
