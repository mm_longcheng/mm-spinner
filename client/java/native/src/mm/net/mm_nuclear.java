package mm.net;

import java.nio.channels.spi.AbstractSelectableChannel;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.Map.Entry;

import mm.core.mm_errno;
import mm.core.mm_logger;
import mm.core.mm_socket;
import mm.core.mm_spinlock;
import mm.core.mm_thread;
import mm.poller.mm_poll;
import mm.poller.mm_poll_event;

public class mm_nuclear
{
	public static final String TAG = mm_nuclear.class.getSimpleName();
	
	// nuclear default page size.
	public static final int MM_NUCLEAR_PAGE_SIZE = 1024;
	// nuclear default poll length.
	public static final int MM_NUCLEAR_POLL_LENGTH = 256;
	// nuclear default poll idle sleep milliseconds.
	public static final int MM_NUCLEAR_IDLE_SLEEP_MSEC = 200;
	
	public static class mm_nuclear_callback
	{		
		public void handle_recv( Object obj, Object u, byte[] buffer, int offset, int max_length ){}
		public void handle_send( Object obj, Object u, byte[] buffer, int offset, int max_length ){}
		public Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	}
	public TreeMap<Integer, Object> holder = new TreeMap<Integer, Object>();// rb tree for fd <--> addr.
	
	public mm_nuclear_callback callback = new mm_nuclear_callback();
	
	public mm_poll poll = new mm_poll();
	
	public mm_thread poll_thread = new mm_thread();
	public mm_thread.mutex cond_locker = new mm_thread.mutex();
	public mm_thread.cond cond_not_null = new mm_thread.cond(this.cond_locker);
	public mm_thread.cond cond_not_full = new mm_thread.cond(this.cond_locker);
	public mm_spinlock locker = new mm_spinlock();
	
	// set poll len for mm_poll_wait.default is MM_NUCLEAR_POLL_LEN.set it before wait,or join_wait after.
	public int poll_length = MM_NUCLEAR_POLL_LENGTH;// poll length.
	// set poll timeout interval.-1 is forever.default is MM_nuclear_IDLE_SLEEP_MSEC.
	// if forever,you must resolve break block state while poll wait,use pipeline,or other way.
	public int poll_timeout = MM_NUCLEAR_IDLE_SLEEP_MSEC;// poll timeout.
	public int size = 0;// addr size.
	public int max_size = Integer.MAX_VALUE;// max addr size.default is -1;
	
	int state = mm_thread.ts_closed;// mm_thread_state_t,default is ts_closed(0)
	
	public void init()
	{
		this.holder.clear();
		this.callback.init();
		this.poll.init();
		this.poll_thread.init();
		this.cond_locker.init();
		this.cond_not_null.init();
		this.cond_not_full.init();
		this.locker.init();		
		this.poll_length = MM_NUCLEAR_POLL_LENGTH;
		this.poll_timeout = MM_NUCLEAR_IDLE_SLEEP_MSEC;
		this.size = 0;
		this.max_size = Integer.MAX_VALUE;
		this.state = mm_thread.ts_closed;
	}
	public void destroy()
	{
		this.holder.clear();
		this.callback.destroy();
		this.poll.destroy();
		this.poll_thread.destroy();
		this.cond_locker.destroy();
		this.cond_not_null.destroy();
		this.cond_not_full.destroy();
		this.locker.destroy();	
		this.poll_length = 0;
		this.poll_timeout = 0;
		this.size = 0;
		this.max_size = 0;		
		this.state = mm_thread.ts_closed;
	}
	// lock to make sure the knot is thread safe.
	public void lock()
	{
		this.locker.lock();
	}
	// unlock knot.
	public void unlock()
	{
		this.locker.unlock();
	}
	public void assign_callback(mm_nuclear_callback cb)
	{
		// android assertions are unreliable.
		// assert null != cb : "you can not assign null callback.";
		this.callback = cb;
	}
	// poll size.
	public int poll_size()
	{
		return this.size;
	}
	// wait for activation fd.
	public void poll_wait()
	{
		mm_poll_event pe = null;
		byte[] buffer = new byte[MM_NUCLEAR_PAGE_SIZE];
		int real_len = 0;
		int i = 0;
		while( mm_thread.ts_motion == this.state )
		{
			// the first quick size checking.
			if ( 0 == this.size )
			{
				this.cond_locker.lock();
				// double lock checking.
				// because the first quick size checking is not thread safe.
				// here we lock and check the size once again.
				if (0 == this.size)
				{
					mm_logger.logI(TAG + " " + this.hashCode() + " " + "wait.");
					this.cond_not_null.await();
					mm_logger.logI(TAG + " " + this.hashCode() + " " + "wake.");
				}
				this.cond_locker.unlock();
				continue;
			}
			real_len = this.poll.wait_event(this.poll_timeout);
			if( 0 == real_len)
			{
				continue;
			}
			else if( -1 == real_len )
			{
				int errcode = mm_errno.get_code();
				if (mm_errno.MM_ENOTSOCK == errcode)
				{
					mm_logger.logE(TAG + " " + this.hashCode() + " error occur.");
					break;
				}
				else
				{
					mm_logger.logE(TAG + " " + this.hashCode() + " error occur.");
					continue;
				}
			}
			for ( i = 0; i < real_len ; ++i )
			{
				pe = this.poll.pe.get(i);
				// android assertions are unreliable.
				// assert null != pe.s : "pe.s is a null.";
				//
				if (0 != (pe.mask & mm_poll_event.MM_PE_READABLE))
				{
					this.callback.handle_recv(this, pe.s, buffer, 0, MM_NUCLEAR_PAGE_SIZE);
				}
				if (0 != (pe.mask & mm_poll_event.MM_PE_WRITABLE))
				{
					this.callback.handle_send(this, pe.s, buffer, 0, MM_NUCLEAR_PAGE_SIZE);
				}
			}
		}
	}
	//////////////////////////////////////////////////////////////////////////
	//start wait thread.
	public void start()
	{
		this.state = mm_thread.ts_finish == this.state ? mm_thread.ts_closed : mm_thread.ts_motion;
		this.poll_thread.start(new __nuclear_wait_thread_runnable(this));
	}
	//interrupt wait thread.
	public void interrupt()
	{
		this.state = mm_thread.ts_closed;
		this.cond_locker.lock();
		this.cond_not_null.signal();
		this.cond_not_full.signal();
		this.cond_locker.unlock();
	}
	//shutdown wait thread.
	public void shutdown()
	{
		this.state = mm_thread.ts_finish;
		this.cond_locker.lock();
		this.cond_not_null.signal();
		this.cond_not_full.signal();
		this.cond_locker.unlock();
	}
	//join wait thread.
	public void join()
	{
		this.poll_thread.join();
	}
	//////////////////////////////////////////////////////////////////////////
	// add u into poll_fd.you must add rmv in pairs.
	public void fd_add(AbstractSelectableChannel fd, Object u)
	{
		do
		{
			if (mm_socket.MM_INVALID_SOCKET == fd)
			{
				// the socket is invalid.do nothing.
				break;
			}
			//
			if ( this.max_size <= this.size )
			{
				// pthread_cond_wait(&p->cond_not_null,&p->locker);
				// nothing need remove and not need cond wait at all.
				mm_logger.logT(TAG + " we can not fd add into full poll.");
				break;
			}
			this.poll.add_event(fd,mm_poll_event.MM_PE_READABLE,u);
			// add addr to rbt.
			this.cond_locker.lock();
			this.holder.put(fd.hashCode(), u);
			this.size ++;
			this.cond_not_null.signal();
			this.cond_locker.unlock();	
		}while(false);	
	}
	// rmv u from poll_fd.you must add rmv in pairs.
	public void fd_rmv(AbstractSelectableChannel fd, Object u)
	{
		do
		{
			if (mm_socket.MM_INVALID_SOCKET == fd)
			{
				// the socket is invalid.do nothing.
				break;
			}
			//
			if ( 0 >= this.size )
			{
				// pthread_cond_wait(&p->cond_not_full,&p->locker);
				// nothing need remove and not need cond wait at all.
				mm_logger.logT(TAG + " we can not fd rmv from null poll.");
				break;
			}
			this.poll.del_event( fd, mm_poll_event.MM_PE_READABLE | mm_poll_event.MM_PE_WRITABLE, u );
			// rmv addr from rbt.
			this.cond_locker.lock();
			this.holder.remove(fd.hashCode());
			this.size --;
			this.cond_not_full.signal();
			this.cond_locker.unlock();	
		}while(false);
	}
	// mod u event from poll wait.
	public void fd_mod(AbstractSelectableChannel fd, Object u, int flag)
	{
		this.poll.mod_event( fd, flag, u );
	}
	// get u event from poll wait.
	public Object fd_get(AbstractSelectableChannel fd)
	{
		Object e = null;
		this.cond_locker.lock();
		e = this.holder.get(fd.hashCode());
		this.cond_locker.unlock();
		return e;
	}
	// add u into poll_fd.not cond and locker.you must lock it outside manual.
	public void fd_add_poll(AbstractSelectableChannel fd, Object u)
	{
		this.poll.add_event(fd,mm_poll_event.MM_PE_READABLE,u);
		this.holder.put(fd.hashCode(), u);
		this.size ++;
	}
	// rmv u from poll_fd..not cond and locker.you must lock it outside manual.
	public void fd_rmv_poll(AbstractSelectableChannel fd, Object u)
	{
		this.poll.del_event( fd, mm_poll_event.MM_PE_READABLE | mm_poll_event.MM_PE_WRITABLE, u );
		this.holder.remove(fd.hashCode());
		this.size --;
	}
	// signal.
	public void fd_poll_signal()
	{
		this.cond_locker.lock();
		this.cond_not_null.signal();
		this.cond_not_full.signal();
		this.cond_locker.unlock();
	}
	// this function is used for copy current fd holder.
	public void fd_copy_holder(TreeMap<Integer, Object> holder)
	{
		Entry<Integer, Object> n = null;
		Iterator<Entry<Integer, Object>> it = null;	
		//
		// clear the holder first.
		holder.clear();
		//
		it = this.holder.entrySet().iterator();
		while (it.hasNext()) 
		{
			n = it.next();
			holder.put(n.getKey(), n.getValue());
		}
	}
	//////////////////////////////////////////////////////////////////////////
	static final class __nuclear_wait_thread_runnable implements Runnable
	{
		__nuclear_wait_thread_runnable(mm_nuclear _p)
		{
			this.p = _p;
		}
		public mm_nuclear p = null;
		@Override
		public void run() 
		{
			this.p.poll_wait();
		}		
	}
};
