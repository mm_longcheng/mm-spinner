package mm.net;

import java.nio.channels.spi.AbstractSelectableChannel;
import java.util.TreeMap;

import mm.core.mm_logger;
import mm.core.mm_socket;
import mm.core.mm_spinlock;
import mm.core.mm_thread;
import mm.core.mm_time;
import mm.net.mm_crypto.mm_crypto_callback;

public class mm_headset
{
	public static final String TAG = mm_headset.class.getSimpleName();
	// headset default page size.
	public static final int MM_HEADSET_PAGE_SIZE = 1024;
	// net udp nonblock timeout..
	public static final int MM_HEADSET_NONBLOCK_TIMEOUT = 20;
	// if bind listen failure will msleep this time.
	public static final int MM_HEADSET_TRY_MSLEEP_TIME = 1000;
	// net udp one time try time.
	public static final int MM_HEADSET_TRYTIME = -1;

	public static final int headset_state_closed = 0;// fd is closed,connect closed.invalid at all.
	public static final int headset_state_motion = 1;// fd is opened,connect closed.at connecting.
	public static final int headset_state_finish = 2;// fd is opened,connect opened.at connected.
	public static final int headset_state_broken = 3;// fd is opened,connect broken.connect is broken fd not closed.
	
	static final __headset_udp_handle_packet_callback __headset_udp_handle_packet_callback_impl = new __headset_udp_handle_packet_callback();
	
	public static class mm_mic
	{	
		public mm_udp udp = new mm_udp();// weak ref.
		public mm_thread poll_thread = new mm_thread();
		public mm_spinlock locker = new mm_spinlock();
		int udp_state;// mm_net_udp_state_t,default is headset_state_closed(0)
		public int state;// mm_thread_state_t,default is ts_closed(0)
		
		public void init()
		{
			this.udp.init();
			this.poll_thread.init();
			this.locker.init();
			this.udp_state = headset_state_closed;
			this.state = mm_thread.ts_closed;
		}
		public void destroy()
		{
			this.udp.destroy();
			this.poll_thread.destroy();
			this.locker.destroy();
			this.udp_state = headset_state_closed;
			this.state = mm_thread.ts_closed;
		}
		public void lock()
		{
			this.locker.lock();
		}
		public void unlock()
		{
			this.locker.unlock();
		}
		public void assign_udp_shared(mm_udp udp)
		{
			this.udp.addr.ss_native.copy(udp.addr.ss_native);
			this.udp.addr.ss_remote.copy(udp.addr.ss_remote);
			this.udp.addr.socket = udp.addr.socket;
			this.udp.addr.u = udp.addr.u;
			this.udp.addr = udp.addr;
			this.udp.callback = udp.callback;
			this.udp.unique_id = udp.unique_id;
		}
		public void update_udp_state(int state)
		{
			this.udp_state = state;
		}
		public void clear()
		{
			this.udp.addr.socket = (AbstractSelectableChannel) mm_socket.MM_INVALID_SOCKET;
		}
		public void start()
		{
			this.state = mm_thread.ts_finish == this.state ? mm_thread.ts_closed : mm_thread.ts_motion;
			this.poll_thread.start(new __static_mic_udp_poll_thread(this));
		}
		public void interrupt()
		{
			this.state = mm_thread.ts_closed;
		}
		public void shutdown()
		{
			this.state = mm_thread.ts_finish;
		}
		public void join()
		{
			this.poll_thread.join();
		}
		public void poll_wait()
		{
			byte[] buffer = new byte[mm_headset.MM_HEADSET_PAGE_SIZE];
			while( mm_thread.ts_motion == this.state && headset_state_broken != this.udp_state)
			{
				this.udp.handle_recv(buffer,0,mm_headset.MM_HEADSET_PAGE_SIZE);
			}
		}
		// mic streambuf recv send reset.
		public void streambuf_reset()
		{
			this.udp.buff_recv.reset();
			this.udp.buff_send.reset();
		}
	}
	// packet handle function type.
	public interface mm_headset_handle_callback 
	{  
	    public void handle( Object obj, Object u, mm_packet pack, mm_sockaddr remote );
	}
	public static class mm_headset_callback
	{	
		public void handle( Object obj, Object u, mm_packet pack, mm_sockaddr remote ){}
		public void broken( Object obj ){}
		public void nready( Object obj ){}
		public void finish( Object obj ){}
		Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	}

	public mm_udp udp = new mm_udp();// strong ref.
	public mm_headset_callback callback = new mm_headset_callback();
	public mm_crypto_callback crypto_callback = new mm_crypto_callback();// crypto callback.
	public TreeMap<Integer,mm_headset_handle_callback> holder = new TreeMap<Integer,mm_headset_handle_callback>();// rb tree for msg_id <--> callback.
	public mm_thread.mutex signal_mutex = new mm_thread.mutex();
	public mm_thread.cond signal_cond = new mm_thread.cond(this.signal_mutex);
	public mm_spinlock arrays_locker = new mm_spinlock();
	public mm_spinlock holder_locker = new mm_spinlock();
	public mm_spinlock locker = new mm_spinlock();
	public int length = 0;// length. default is 0.
	public mm_mic[] arrays = null;// array pointer.
	public int nonblock_timeout = MM_HEADSET_NONBLOCK_TIMEOUT;// udp nonblock timeout.default is MM_HEADSET_NONBLOCK_TIMEOUT milliseconds.
	public int trytimes = MM_HEADSET_TRYTIME;// try bind and listen times.default is MM_HEADSET_TRYTIME.
	//
	public int state = mm_thread.ts_closed;// mm_thread_state_t,default is ts_closed(0)
	public int udp_state = headset_state_closed;// mm_net_udp_state_t,default is udp_state_closed(0)
	public Object u = null;// user data.
	
	public void init()
	{
		this.udp.init();
		this.callback.init();
		this.crypto_callback.init();
		this.holder.clear();
		this.signal_mutex.init();
		this.signal_cond.init();
		this.arrays_locker.init();
		this.holder_locker.init();
		this.locker.init();
		this.length = 0;
		this.arrays = null;
		this.nonblock_timeout = MM_HEADSET_NONBLOCK_TIMEOUT;
		this.trytimes = MM_HEADSET_TRYTIME;
		this.state = mm_thread.ts_closed;
		this.udp_state = headset_state_closed;
		this.u = null;
		//
		this.udp.assign_callback(new __mm_headset_udp_callback(this));
	}
	public void destroy()
	{
		this.clear_callback_holder();
		//
		this.clear();
		//
		this.udp.destroy();
		this.callback.destroy();
		this.crypto_callback.destroy();
		this.holder.clear();
		this.signal_mutex.destroy();
		this.signal_cond.destroy();
		this.arrays_locker.destroy();
		this.holder_locker.destroy();
		this.locker.destroy();
		this.length = 0;
		this.arrays = null;
		this.nonblock_timeout = MM_HEADSET_NONBLOCK_TIMEOUT;
		this.trytimes = MM_HEADSET_TRYTIME;
		this.state = mm_thread.ts_closed;
		this.udp_state = headset_state_closed;
		this.u = null;
	}
	// lock.
	public void lock()
	{
		this.locker.lock();
	}
	// unlock.
	public void unlock()
	{
		this.locker.unlock();
	}
	// assign native address but not connect.
	public void assign_native(String node,int port)
	{
		int i = 0;
		this.arrays_locker.lock();
		this.udp.assign_native(node, port);
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			this.arrays[i].udp.assign_native_storage(this.udp.addr.ss_native);
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
	}
	// assign remote address but not connect.
	public void assign_remote(String node,int port)
	{
		int i = 0;
		this.arrays_locker.lock();
		this.udp.assign_remote(node, port);
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			this.arrays[i].udp.assign_remote_storage(this.udp.addr.ss_remote);
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
	}
	// assign protocol_size.
	public void set_length(int length)
	{
		this.arrays_locker.lock();
		if (length < this.length)
		{
			int i = 0;
			mm_mic e = null;
			for ( i = length; i < this.length; ++i)
			{
				e = this.arrays[i];
				e.lock();
				this.arrays[i] = null;
				e.unlock();
				e.destroy();
				e = null;
			}
			mm_mic[] v = new mm_mic[length];
			System.arraycopy(this.arrays, 0, v, 0, length);
			this.arrays = v;
			this.length = length;
		}
		else if (length > this.length)
		{
			int i = 0;
			mm_mic[] v = new mm_mic[length];
			System.arraycopy(this.arrays, 0, v, 0, this.length);
			for ( i = this.length; i < length; ++i)
			{
				this.arrays[i] = new mm_mic();
				this.arrays[i].init();
				this.arrays[i].assign_udp_shared(this.udp);
				this.arrays[i].state = this.state;
			}
			this.length = length;
		}
		this.arrays_locker.unlock();
	}
	public int get_length()
	{
		return this.length;
	}
	// assign context handle.
	public void assign_context(Object u)
	{
		this.u = u;
	}
	// assign nonblock_timeout.
	public void assign_nonblock_timeout(int nonblock_timeout)
	{
		this.nonblock_timeout = nonblock_timeout;
	}
	// assign trytimes.
	public void assign_trytimes(int trytimes)
	{
		this.trytimes = trytimes;
	}
	public void assign_callback(int id,mm_headset_handle_callback callback)
	{
		this.holder_locker.lock();
		this.holder.put(id, callback);
		this.holder_locker.unlock();
	}
	// do not assign when holder have some elem.
	public void assign_crypto_callback(mm_crypto_callback crypto_callback)
	{
		this.crypto_callback = crypto_callback;
	}
	// do not assign when holder have some elem.
	public void assign_headset_callback(mm_headset_callback headset_callback)
	{
		// android assertions are unreliable.
		// assert(NULL != headset_callback && "you can not assign null headset_callback.");
		this.callback = headset_callback;
	}
	// context for udp will use the mm_addr.u attribute.such as crypto context.
	public void set_addr_context( Object u)
	{
		this.udp.set_context(u);
	}
	public Object get_addr_context()
	{
		return this.udp.get_context();
	}
	public void clear_callback_holder()
	{
		this.holder_locker.lock();
		this.holder.clear();
		this.holder_locker.unlock();
	}
	// headset crypto encrypt buffer by udp.
	public void crypto_encrypt(mm_udp udp,byte[] buffer,int offset,int length)
	{
		this.crypto_callback.encrypt(this, udp, buffer, offset, buffer, offset, length);
	}
	// headset crypto decrypt buffer by udp.
	public void crypto_decrypt(mm_udp udp,byte[] buffer,int offset,int length)
	{
		this.crypto_callback.decrypt(this, udp, buffer, offset, buffer, offset, length);
	}
	// fopen.
	public void fopen_socket()
	{
		int i = 0;
		this.arrays_locker.lock();
		this.udp.fopen_socket();
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			this.arrays[i].udp.addr.socket = this.udp.addr.socket;
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
	}
	// bind.
	public void bind()
	{
		mm_addr addr = this.udp.addr;
		if (mm_socket.MM_INVALID_SOCKET != addr.socket)
		{
			int _try_times = 0;
			int rt = -1;
			this.udp_state = headset_state_motion;
			{
				int i = 0;
				this.arrays_locker.lock();
				for ( i = 0; i < this.length; ++i)
				{
					this.arrays[i].lock();
					this.arrays[i].update_udp_state(this.udp_state);
					this.arrays[i].unlock();
				}
				this.arrays_locker.unlock();
			}
			// set socket to blocking.
			addr.set_block(mm_socket.MM_BLOCKING);
			do 
			{
				_try_times ++;
				rt = addr.bind();
				if (0 == rt)
				{
					// bind immediately.
					break;
				}
				if ( mm_thread.ts_finish == this.state )
				{
					break;
				}
				else
				{
					mm_logger.logE(TAG + " " + "bind " + addr.to_string() + " failure.try times:" + _try_times);
					mm_time.msleep(MM_HEADSET_TRY_MSLEEP_TIME);
				}
			} while ( 0 != rt && _try_times < this.trytimes );
			// set socket to not block.
			addr.set_block(mm_socket.MM_NONBLOCK);
			if ( 0 != rt )
			{
				mm_logger.logE(TAG + " " + "bind " + addr.to_string() + " failure.try times:" + _try_times);
				this.apply_broken();
				addr.close_socket();
			}
			else
			{
				this.apply_finish();
				mm_logger.logI(TAG + " " + "bind " + addr.to_string() + " success.try times:" + _try_times);
			}
		}
		else
		{
			mm_logger.logE(TAG + " " + "bind target fd is invalid.");
		}
	}
	// close socket.mm_headset_join before call this.
	public void close_socket()
	{
		this.udp.close_socket();
	}
	public void clear()
	{
		int i = 0;
		this.arrays_locker.lock();
		for ( i = 0; i < this.length; ++i)
		{
			mm_mic e = this.arrays[i];
			e.lock();
			this.arrays[i] = null;
			e.unlock();
			e.destroy();
			e = null;
		}
		this.arrays = null;
		this.length = 0;
		this.arrays_locker.unlock();
	}
	public void apply_broken()
	{
		int i = 0;
		this.arrays_locker.lock();
		this.udp_state = headset_state_closed;
		this.udp.streambuf_reset();
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			this.arrays[i].streambuf_reset();
			this.arrays[i].update_udp_state(this.udp_state);
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
		this.callback.broken(this.udp);
	}
	public void apply_finish()
	{
		int i = 0;
		this.arrays_locker.lock();
		this.udp_state = headset_state_finish;
		this.udp.streambuf_reset();
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			this.arrays[i].streambuf_reset();
			this.arrays[i].update_udp_state(this.udp_state);
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
		this.callback.finish(this.udp);
	}
	public void start()
	{
		int i = 0;
		this.arrays_locker.lock();
		this.state = mm_thread.ts_finish == this.state ? mm_thread.ts_closed : mm_thread.ts_motion;
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			this.arrays[i].start();
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
	}
	public void interrupt()
	{
		int i = 0;
		this.arrays_locker.lock();
		this.state = mm_thread.ts_closed;
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			this.arrays[i].interrupt();
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
		//
		this.signal_mutex.lock();
		this.signal_cond.signal();
		this.signal_mutex.unlock();
	}
	public void shutdown()
	{
		int i = 0;
		this.arrays_locker.lock();
		this.state = mm_thread.ts_finish;
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			this.arrays[i].shutdown();
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
		//
		this.signal_mutex.lock();
		this.signal_cond.signal();
		this.signal_mutex.unlock();
	}
	public void join()
	{
		int i = 0;
		if (mm_thread.ts_motion == this.state)
		{
			// we can not lock or join until cond wait all thread is shutdown.
			this.signal_mutex.lock();
			this.signal_cond.await();
			this.signal_mutex.unlock();
		}
		//
		this.arrays_locker.lock();
		for ( i = 0; i < this.length; ++i)
		{
			this.arrays[i].lock();
			this.arrays[i].join();
			this.arrays[i].unlock();
		}
		this.arrays_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	public static class __static_mic_udp_poll_thread implements Runnable
	{
		__static_mic_udp_poll_thread(mm_mic _p)
		{
			this.p = _p;
		}
		public mm_mic p = null;
		@Override
		public void run() 
		{
			this.p.poll_wait();
		}		
	}
	//////////////////////////////////////////////////////////////////////////
	static final class __mm_headset_udp_callback extends mm_udp.mm_udp_callback
	{
		__mm_headset_udp_callback(mm_headset _p)
		{
			this.obj = _p;
		}
		@Override
		public void handle( Object obj, byte[] buffer, int offset, int length, mm_sockaddr remote )
		{
			mm_udp udp = (mm_udp)(obj);
			mm_headset headset = (mm_headset)(udp.callback.obj);
			headset.crypto_decrypt(udp,buffer,offset,length);
			mm_streambuf_packet.handle_udp(udp.buff_recv, buffer, offset, length, __headset_udp_handle_packet_callback_impl, udp, remote);
		}
		@Override
		public void broken( Object obj )
		{
			mm_udp udp = (mm_udp)(obj);
			mm_headset headset = (mm_headset)(udp.callback.obj);
			// android assertions are unreliable.
			// assert null != net_udp.callback : "net_udp.callback is a null.";
			headset.apply_broken();
		}	
	};
	static final class __headset_udp_handle_packet_callback implements mm_packet.udp_callBack
	{
		@Override
		public void handle_udp( Object obj, mm_packet pack, mm_sockaddr remote ) 
		{
			mm_udp udp = (mm_udp)(obj);
			mm_headset headset = (mm_headset)(udp.callback.obj);
			mm_headset_handle_callback handle = null;
			headset.holder_locker.lock();
			handle = headset.holder.get(pack.phead.mid);
			headset.holder_locker.unlock();
			if (null != handle)
			{
				// fire the handle event.
				handle.handle(udp, headset.u, pack, remote);
			}
			else
			{
				// android assertions are unreliable.
				// assert null != headset.callback : "headset.callback is a null.";
				headset.callback.handle(udp, headset.u, pack, remote);
			}
		}		
	}
}

