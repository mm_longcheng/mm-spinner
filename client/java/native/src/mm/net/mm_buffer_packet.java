package mm.net;

import mm.core.mm_byte;
import mm.core.mm_logger;
import mm.core.mm_type;

public class mm_buffer_packet
{
	public static final String TAG = mm_buffer_packet.class.getSimpleName();
	
	// packet handle tcp buffer and length for string handle only one packet.
	public static void handle_tcp(byte[] buffer, int offset, int length,mm_packet.tcp_callBack handle, Object obj)
	{
		mm_packet pack = new mm_packet();
		int head_data = 0;
		int msg_size = 0;
		int buff_size = 0;
		//
		buff_size = length;
		head_data = mm_byte.int32_decode_bytes(buffer, offset);
		msg_size = (short) (head_data & mm_packet.MM_MSG_HEAD_L_MASK);
		if (buff_size == msg_size)
		{
			// here we just use the weak ref from streambuf.for data recv callback.
			pack.hbuff.length = head_data >> mm_packet.MM_MSG_SIZE_BIT_NUM;
			pack.hbuff.buffer = buffer;
			pack.hbuff.offset = offset + mm_type.MM_INT32_SIZE;
			//
			pack.bbuff.length = msg_size - mm_type.MM_INT32_SIZE - pack.hbuff.length;
			pack.bbuff.buffer = buffer;
			pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
			//
			pack.head_base_decode();
			//
			// fire the field buffer recv event.
			handle.handle_tcp(obj,pack);
		}
		else
		{
			mm_logger.logI(TAG + " data size is invalid.buff_size:" + buff_size + " msg_size:" + msg_size);
		}
	}
	public static void handle_udp(byte[] buffer, int offset, int length,mm_packet.udp_callBack handle, Object obj, mm_sockaddr remote)
	{
		mm_packet pack = new mm_packet();
		int head_data = 0;
		int msg_size = 0;
		int buff_size = 0;
		//
		buff_size = length;
		head_data = mm_byte.int32_decode_bytes(buffer, offset);
		msg_size = (short) (head_data & mm_packet.MM_MSG_HEAD_L_MASK);
		if (buff_size == msg_size)
		{
			// here we just use the weak ref from streambuf.for data recv callback.
			pack.hbuff.length = head_data >> mm_packet.MM_MSG_SIZE_BIT_NUM;
			pack.hbuff.buffer = buffer;
			pack.hbuff.offset = offset + mm_type.MM_INT32_SIZE;
			//
			pack.bbuff.length = msg_size - mm_type.MM_INT32_SIZE - pack.hbuff.length;
			pack.bbuff.buffer = buffer;
			pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
			//
			pack.head_base_decode();
			//
			// fire the field buffer recv event.
			handle.handle_udp(obj,pack,remote);
		}
		else
		{
			mm_logger.logI(TAG + " data size is invalid.buff_size:" + buff_size + " msg_size:" + msg_size);
		}
	}
};
