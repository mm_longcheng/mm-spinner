package mm.net;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

import mm.core.mm_spinlock;
import mm.core.mm_thread;

public class mm_mt_tcp
{
	public static class mm_mt_tcp_callback
	{	
		public void handle( Object obj, Object u,mm_packet pack ){}
		public void broken( Object obj ){}
		public void nready( Object obj ){}
		public void finish( Object obj ){}
		Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	}
	
	public mm_sockaddr ss_native = new mm_sockaddr();// native end point.
	public mm_sockaddr ss_remote = new mm_sockaddr();// remote end point.
	public ArrayList<mm_net_tcp> list = new ArrayList<mm_net_tcp>();// silk strong ref list.
	public TreeMap<Integer,mm_net_tcp.mm_net_tcp_handle_callback> holder = new TreeMap<Integer,mm_net_tcp.mm_net_tcp_handle_callback>();// rb tree for msg_id <--> callback.
	public mm_mt_tcp_callback callback = new mm_mt_tcp_callback();// value ref. tcp callback.
	public mm_crypto.mm_crypto_callback crypto_callback = new mm_crypto.mm_crypto_callback();// crypto callback.
	public String cache_native_node;// cache ss_native node use for quick compare.
	public String cache_remote_node;// cache ss_remote node use for quick compare.
	public mm_thread.mutex signal_mutex = new mm_thread.mutex();
	public mm_thread.cond signal_cond = new mm_thread.cond(this.signal_mutex);
	public mm_spinlock list_locker = new mm_spinlock();
	public mm_spinlock holder_locker = new mm_spinlock();
	public mm_spinlock locker = new mm_spinlock();
	public ThreadLocal<mm_net_tcp> thread_key = new ThreadLocal<mm_net_tcp>();// thread key.
	public int cache_remote_port;// cache ss_remote port use for quick compare.

	public int nonblock_timeout = mm_net_tcp.MM_NET_TCP_NONBLOCK_TIMEOUT;// tcp nonblock timeout.default is MM_NET_TCP_NONBLOCK_TIMEOUT milliseconds.

	public int trytimes = Integer.MAX_VALUE;// try bind and listen times.default is -1.

	public int unique_id = 0;// unique_id.
	public int state = mm_thread.ts_closed;// mm_thread_state_t,default is ts_closed(0)
	public Object u = null;// user data.

	public void init()
	{
		this.ss_native.init();
		this.ss_remote.init();
		this.list.clear();
		this.holder.clear();
		this.callback.init();
		this.crypto_callback.init();
		this.cache_native_node = "";
		this.cache_remote_node = "";
		this.signal_mutex.init();
		this.signal_cond.init();
		this.list_locker.init();
		this.holder_locker.init();
		this.locker.init();
		this.cache_remote_port = 0;
		this.nonblock_timeout = mm_net_tcp.MM_NET_TCP_NONBLOCK_TIMEOUT;
		this.trytimes = Integer.MAX_VALUE;
		this.unique_id = 0;
		this.state = mm_thread.ts_closed;
		this.u = null;
	}
	public void destroy()
	{
		this.clear_callback_holder();
		//
		this.clear();
		//
		this.ss_native.destroy();
		this.ss_remote.destroy();
		this.list.clear();
		this.holder.clear();
		this.callback.destroy();
		this.crypto_callback.destroy();
		this.cache_native_node = "";
		this.cache_remote_node = "";
		this.signal_mutex.destroy();
		this.signal_cond.destroy();
		this.list_locker.destroy();
		this.holder_locker.destroy();
		this.locker.destroy();
		this.cache_remote_port = 0;
		this.nonblock_timeout = 0;
		this.trytimes = 0;
		this.unique_id = 0;
		this.state = mm_thread.ts_closed;
		this.u = null;
	}
	//////////////////////////////////////////////////////////////////////////
	// assign native address but not connect.
	public void assign_native(String node,int port)
	{
		Iterator<mm_net_tcp> it = null;
		mm_net_tcp net_tcp = null;
		this.list_locker.lock();
		this.ss_native.assign(node, port);
        it = this.list.iterator();
        while(it.hasNext())
        {
        	net_tcp = it.next();
        	net_tcp.lock();
        	net_tcp.assign_native(node, port);
        	net_tcp.unlock();
        }
		this.list_locker.unlock();
	}
	// assign remote address but not connect.
	public void assign_remote(String node,int port)
	{
		Iterator<mm_net_tcp> it = null;
		mm_net_tcp net_tcp = null;
		this.list_locker.lock();
		this.ss_remote.assign(node, port);
        it = this.list.iterator();
        while(it.hasNext())
        {
        	net_tcp = it.next();
        	net_tcp.lock();
        	net_tcp.assign_remote(node, port);
        	net_tcp.unlock();
        }
		this.list_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	public void assign_callback(int id,mm_net_tcp.mm_net_tcp_handle_callback callback)
	{
		// we not need assign all net tcp instance,but use default and hander by p->holder.
		this.holder_locker.lock();
		this.holder.put(id, callback);
		this.holder_locker.unlock();
	}
	public void mm_mt_tcp_assign_default_callback(mm_mt_tcp_callback mt_tcp_callback)
	{
		// we not need assign all net tcp instance,but use default and hander by p->holder.
		this.callback = mt_tcp_callback;
	}
	// do not assign when holder have some elem.
	public void assign_crypto_callback(mm_crypto.mm_crypto_callback crypto_callback)
	{
		Iterator<mm_net_tcp> it = null;
		mm_net_tcp net_tcp = null;
		this.list_locker.lock();
		// android assertions are unreliable.
		// assert null != crypto_callback : "you can not assign null crypto_callback.";
		this.crypto_callback = crypto_callback;
        it = this.list.iterator();
        while(it.hasNext())
        {
        	net_tcp = it.next();
        	net_tcp.lock();
        	net_tcp.assign_crypto_callback(this.crypto_callback);
        	net_tcp.unlock();
        }
		this.list_locker.unlock();
	}
	// assign context handle.
	public void assign_context( Object u )
	{
		Iterator<mm_net_tcp> it = null;
		mm_net_tcp net_tcp = null;
		this.list_locker.lock();
		this.u = u;
        it = this.list.iterator();
        while(it.hasNext())
        {
        	net_tcp = it.next();
        	net_tcp.lock();
        	net_tcp.assign_context(this.u);
        	net_tcp.unlock();
        }
		this.list_locker.unlock();
	}
	public void clear_callback_holder()
	{
		this.holder_locker.lock();
		this.holder.clear();
		this.holder_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	// lock to make sure the mt_tcp is thread safe.
	public void lock()
	{
		this.locker.lock();
	}
	// unlock mt_tcp.
	public void unlock()
	{
		this.locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	// fopen socket.
	public void fopen_socket()
	{
		Iterator<mm_net_tcp> it = null;
		mm_net_tcp net_tcp = null;
		this.list_locker.lock();
        it = this.list.iterator();
        while(it.hasNext())
        {
        	net_tcp = it.next();
        	net_tcp.lock();
        	net_tcp.fopen_socket();
        	net_tcp.unlock();
        }
		this.list_locker.unlock();
	}
	// synchronize connect.can call this as we already open connect check thread.
	public void connect()
	{
		Iterator<mm_net_tcp> it = null;
		mm_net_tcp net_tcp = null;
		this.list_locker.lock();
        it = this.list.iterator();
        while(it.hasNext())
        {
        	net_tcp = it.next();
        	net_tcp.lock();
        	net_tcp.connect();
        	net_tcp.unlock();
        }
		this.list_locker.unlock();
	}
	// close socket.
	public void close_socket()
	{
		Iterator<mm_net_tcp> it = null;
		mm_net_tcp net_tcp = null;
		this.list_locker.lock();
        it = this.list.iterator();
        while(it.hasNext())
        {
        	net_tcp = it.next();
        	net_tcp.lock();
        	net_tcp.close_socket();
        	net_tcp.unlock();
        }
		this.list_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	// check connect and reconnect if disconnect.not thread safe.
	public void check()
	{
		Iterator<mm_net_tcp> it = null;
		mm_net_tcp net_tcp = null;
		this.list_locker.lock();
        it = this.list.iterator();
        while(it.hasNext())
        {
        	net_tcp = it.next();
        	net_tcp.lock();
        	net_tcp.check();
        	net_tcp.unlock();
        }
		this.list_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	// synchronize attach to socket.not thread safe.
	public void attach_socket()
	{
		Iterator<mm_net_tcp> it = null;
		mm_net_tcp net_tcp = null;
		this.list_locker.lock();
        it = this.list.iterator();
        while(it.hasNext())
        {
        	net_tcp = it.next();
        	net_tcp.lock();
        	net_tcp.attach_socket();
        	net_tcp.unlock();
        }
		this.list_locker.unlock();
	}
	// synchronize detach to socket.not thread safe.
	public void detach_socket()
	{
		Iterator<mm_net_tcp> it = null;
		mm_net_tcp net_tcp = null;
		this.list_locker.lock();
        it = this.list.iterator();
        while(it.hasNext())
        {
        	net_tcp = it.next();
        	net_tcp.lock();
        	net_tcp.detach_socket();
        	net_tcp.unlock();
        }
		this.list_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	// get current thread net tcp.
	public mm_net_tcp thread_instance()
	{
		mm_net_tcp net_tcp = this.thread_key.get();
		if (null == net_tcp)
		{
			net_tcp = new mm_net_tcp();
			this.thread_key.set(net_tcp);
			net_tcp.init();
			net_tcp.assign_native_storage(this.ss_native);
			net_tcp.assign_remote_storage(this.ss_remote);
			net_tcp.assign_context(this.u);

			net_tcp.assign_net_tcp_callback(new __mt_tcp_net_tcp_call_back(this));

			net_tcp.assign_crypto_callback(this.crypto_callback);
			//
			if ( mm_thread.ts_motion == this.state )
			{
				// if current route is in motion we start the new net tcp .
				net_tcp.start();
			}
			else
			{
				// if current route is not motion,we assign the same state.
				net_tcp.state = this.state;
			}
			//
			this.list_locker.lock();
			this.list.add(net_tcp);
			this.list_locker.unlock();
		}
		return net_tcp;
	}
	// clear all net tcp.
	public void clear()
	{
		Iterator<mm_net_tcp> it = null;
		mm_net_tcp net_tcp = null;
		this.list_locker.lock();
        it = this.list.iterator();
        while(it.hasNext())
        {
        	net_tcp = it.next();
        	this.list.remove(net_tcp);
        	net_tcp.lock();
        	net_tcp.detach_socket();
			net_tcp.unlock();
			net_tcp.destroy();
			net_tcp = null;
        }
		this.list_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	public void start()
	{
		Iterator<mm_net_tcp> it = null;
		mm_net_tcp net_tcp = null;
		this.list_locker.lock();
		this.state = mm_thread.ts_finish == this.state ? mm_thread.ts_closed : mm_thread.ts_motion;
        it = this.list.iterator();
        while(it.hasNext())
        {
        	net_tcp = it.next();
        	net_tcp.lock();
        	net_tcp.start();
        	net_tcp.unlock();
        }
		this.list_locker.unlock();
	}
	public void interrupt()
	{
		Iterator<mm_net_tcp> it = null;
		mm_net_tcp net_tcp = null;
		this.list_locker.lock();
		this.state = mm_thread.ts_closed;
        it = this.list.iterator();
        while(it.hasNext())
        {
        	net_tcp = it.next();
        	net_tcp.lock();
        	net_tcp.interrupt();
        	net_tcp.unlock();
        }
		this.list_locker.unlock();
		//
		this.signal_mutex.lock();
		this.signal_cond.signal();
		this.signal_mutex.unlock();
	}
	public void shutdown()
	{
		Iterator<mm_net_tcp> it = null;
		mm_net_tcp net_tcp = null;
		this.list_locker.lock();
		this.state = mm_thread.ts_finish;
        it = this.list.iterator();
        while(it.hasNext())
        {
        	net_tcp = it.next();
        	net_tcp.lock();
        	net_tcp.shutdown();
        	net_tcp.unlock();
        }
		this.list_locker.unlock();
		//
		this.signal_mutex.lock();
		this.signal_cond.signal();
		this.signal_mutex.unlock();
	}
	public void join()
	{
		Iterator<mm_net_tcp> it = null;
		mm_net_tcp net_tcp = null;
		if (mm_thread.ts_motion == this.state)
		{
			// we can not lock or join until cond wait all thread is shutdown.
			this.signal_mutex.lock();
			this.signal_cond.await();
			this.signal_mutex.unlock();
		}
		//
		this.list_locker.lock();
        it = this.list.iterator();
        while(it.hasNext())
        {
        	net_tcp = it.next();
        	net_tcp.lock();
        	net_tcp.join();
        	net_tcp.unlock();
        }
		this.list_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	static final class __mt_tcp_net_tcp_call_back extends mm_net_tcp.mm_net_tcp_callback
	{
		__mt_tcp_net_tcp_call_back(mm_mt_tcp _p)
		{
			this.obj = _p;
		}
		@Override
		public void handle( Object obj, Object u,mm_packet pack )
		{
			mm_tcp tcp = (mm_tcp)obj;
			mm_net_tcp net_tcp = (mm_net_tcp)(tcp.callback.obj);
			mm_mt_tcp mt_tcp = (mm_mt_tcp)(net_tcp.callback.obj);
			mm_net_tcp.mm_net_tcp_handle_callback handle = null;
			mt_tcp.holder_locker.lock();
			handle = mt_tcp.holder.get(pack.phead.mid);
			mt_tcp.holder_locker.unlock();
			if (null != handle)
			{
				// fire the handle event.
				handle.handle(tcp, u, pack);
			}
			else
			{
				// android assertions are unreliable.
				// assert null != mt_tcp.callback : "mt_tcp.callback is a null.";
				mt_tcp.callback.handle(tcp, u, pack);
			}
		}
		@Override
		public void broken( Object obj )
		{
			mm_tcp tcp = (mm_tcp)obj;
			mm_net_tcp net_tcp = (mm_net_tcp)(tcp.callback.obj);
			mm_mt_tcp mt_tcp = (mm_mt_tcp)(net_tcp.callback.obj);
			// android assertions are unreliable.
			// assert null != mt_tcp.callback : "mt_tcp.callback is a null.";
			mt_tcp.callback.broken(tcp);
		}
		@Override
		public void nready( Object obj )
		{
			mm_tcp tcp = (mm_tcp)obj;
			mm_net_tcp net_tcp = (mm_net_tcp)(tcp.callback.obj);
			mm_mt_tcp mt_tcp = (mm_mt_tcp)(net_tcp.callback.obj);
			// android assertions are unreliable.
			// assert null != mt_tcp.callback : "mt_tcp.callback is a null.";
			mt_tcp.callback.nready(tcp);
		}
		@Override
		public void finish( Object obj )
		{
			mm_tcp tcp = (mm_tcp)obj;
			mm_net_tcp net_tcp = (mm_net_tcp)(tcp.callback.obj);
			mm_mt_tcp mt_tcp = (mm_mt_tcp)(net_tcp.callback.obj);
			// android assertions are unreliable.
			// assert null != mt_tcp.callback : "mt_tcp.callback is a null.";
			mt_tcp.callback.finish(tcp);
		}
	};
}
