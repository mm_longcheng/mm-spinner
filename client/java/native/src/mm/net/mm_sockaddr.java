package mm.net;

import java.net.InetAddress;

public class mm_sockaddr
{
	//////////////////////////////////////////////////////////////////////////
	public static final String MM_ADDR_DEFAULT_NODE = "::";
	public static final int MM_ADDR_DEFAULT_PORT = 0;
	//////////////////////////////////////////////////////////////////////////
	public static final int MM_NODE_NAME_LENGTH = 64;
	public static final int MM_PORT_NAME_LENGTH = 8;
	public static final int MM_SOCK_NAME_LENGTH = 16;
	public static final int MM_ADDR_NAME_LENGTH = 64;
	public static final int MM_LINK_NAME_LENGTH = 128;
	
	//////////////////////////////////////////////////////////////////////////
	public String node = MM_ADDR_DEFAULT_NODE;// socketaddr node.
	public int port = MM_ADDR_DEFAULT_PORT;   // socketaddr port.
	//////////////////////////////////////////////////////////////////////////
	public void init()
	{
		this.node = MM_ADDR_DEFAULT_NODE;
		this.port = MM_ADDR_DEFAULT_PORT;
	}
	public void destroy()
	{
		this.node = MM_ADDR_DEFAULT_NODE;
		this.port = MM_ADDR_DEFAULT_PORT;
	}
	public void reset()
	{
		this.node = MM_ADDR_DEFAULT_NODE;
		this.port = MM_ADDR_DEFAULT_PORT;
	}
	public void copy(mm_sockaddr rhs)
	{
		sockaddr_storage_assign(this,rhs.node,rhs.port);
	}
	public void assign( String node, int port )
	{
		sockaddr_storage_assign(this,node,port);
	}	
	// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
	// ipv4                         192.168.111.123-65535
	public static String sockaddr_storage_string(mm_sockaddr addr)
	{
		return addr.node + "-" + addr.port;
	}
	// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
	// ipv4                         192.168.111.123-65535
	public static void sockaddr_storage_assign( mm_sockaddr addr, String node, int port )
	{
		addr.node = node;
		addr.port = port;
	}
	// (fd)|ip-port --> ip-port
	// ipv6 (65535)|2001:0DB8:0000:0023:0008:0800:200C:417A-65535 --> 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
	// ipv4                         (65535)|192.168.111.123-65535 --> 192.168.111.123-65535
	public static String sockaddr_storage_native_remote_string( mm_sockaddr native_addr, mm_sockaddr remote_addr, int fd )
	{
		return "(" + fd + ")|" + sockaddr_storage_string(native_addr) + "-->" + sockaddr_storage_string(remote_addr);
	}
	// true if same.
	public static boolean sockaddr_storage_compare_address( mm_sockaddr addr,String node, int port)
	{
		return addr.node == node && addr.port == port;
	}
	public static boolean sockaddr_storage_compare( mm_sockaddr addrl, mm_sockaddr addrr )
	{
		return addrl.node == addrr.node && addrl.port == addrr.port;
	}
	public static InetAddress default_inet_address()
	{
		InetAddress value = null;
		try 
		{
			value = InetAddress.getLocalHost();
		} 
		catch (Exception e) 
		{
			// do nothing here.we only need the finally code.
			// ok here have special real exception.
			e.printStackTrace();
		}
		return value;
	}
}
