package mm.net;

import java.util.Iterator;
import java.util.TreeMap;

import mm.container.mm_lock_queue;
import mm.core.mm_spinlock;

public class mm_mq_tcp
{
	// max poper number at one loop.the default main thread timer frequency is 1/20 = 50 ms.
	// 2ms one message handle is suitable 50/2 = 25.
	public static final int MM_MQ_TCP_MAX_POPER_NUMBER = 25;

	public static class mm_mq_tcp_data
	{
		Object obj = null;// weak ref for udp handle callback.it is mm_udp.
		public mm_packet pack = new mm_packet();
		
		public void init()
		{
			this.obj = null;
			this.pack.init();
		}
		public void destroy()
		{
			this.obj = null;
			this.pack.destroy();
		}
	};
	public static class mm_mq_tcp_callback
	{
		public void handle( Object obj, Object u, mm_packet pack ){}
		public Object obj = null;// weak ref. user data for callback.
		public void init()
		{
			this.obj = null;
		}
		public void destroy()
		{
			this.obj = null;
		}
	};
	
	public mm_lock_queue lock_queue = new mm_lock_queue();
	public TreeMap<Integer,mm_net_tcp.mm_net_tcp_handle_callback> holder = new TreeMap<Integer,mm_net_tcp.mm_net_tcp_handle_callback>();// rb tree for msg_id <--> callback.
	public mm_mq_tcp_callback callback = new mm_mq_tcp_callback();// value ref. queue udp callback.
	public mm_spinlock holder_locker = new mm_spinlock();
	public mm_spinlock locker = new mm_spinlock();
	public int max_pop = MM_MQ_TCP_MAX_POPER_NUMBER;// max pop to make sure current not wait long times.default is MM_MQ_TCP_MAX_POPER_NUMBER. 
	public Object u = null;// user data.
	
	public void init()
	{
		this.lock_queue.init();
		this.holder.clear();
		this.callback.init();
		this.holder_locker.init();
		this.locker.init();
		this.max_pop = MM_MQ_TCP_MAX_POPER_NUMBER;
	}
	public void destroy()
	{
		this.clear_callback_holder();
		this.dispose();
		//
		this.lock_queue.destroy();
		this.holder.clear();
		this.callback.destroy();
		this.holder_locker.destroy();
		this.locker.destroy();
		this.max_pop = Integer.MAX_VALUE;
	}
	//////////////////////////////////////////////////////////////////////////
	public void lock()
	{
		this.locker.lock();
	}
	public void unlock()
	{
		this.locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	public void assign_callback(int id,mm_net_tcp.mm_net_tcp_handle_callback callback)
	{
		this.holder_locker.lock();
		this.holder.put(id, callback);
		this.holder_locker.unlock();
	}
	public void assign_queue_tcp_callback(mm_mq_tcp_callback queue_tcp_callback)
	{
		// android assertions are unreliable.
		// assert null != queue_tcp_callback : "you can not assign null queue_tcp_callback.";
		this.callback = queue_tcp_callback;
	}
	public void assign_max_poper_number(int max_pop)
	{
		this.max_pop = max_pop;
	}
	// assign context handle.
	public void assign_context( Object u )
	{
		this.u = u;
	}
	public void clear_callback_holder()
	{
		this.holder_locker.lock();
		this.holder.clear();
		this.holder_locker.unlock();
	}
	//////////////////////////////////////////////////////////////////////////
	// you can interrupt block state.but will make the size error.
	// other way is push a NULL.
	public void cond_not_null()
	{
		this.lock_queue.cond_not_null();
	}
	public void cond_not_full()
	{
		this.lock_queue.cond_not_full();
	}
	//////////////////////////////////////////////////////////////////////////
	// main thread trigger self thread handle.such as gl thread.
	public void thread_handle()
	{
		this.pop();
	}
	// delete all not handle pack at queue.
	public void dispose()
	{
		Object obj = null;
		mm_mq_tcp_data data = null;
		Iterator<Object> it = null;
		this.lock_queue.list_locker.lock();
		it = this.lock_queue.l.iterator();	
        while(it.hasNext())
        {
        	obj = it.next();
        	data = (mm_mq_tcp_data)obj;
        	data.pack.free_copy_alloc();
        	data.destroy();
        	data = null;
        	it.remove();
        }
        this.lock_queue.list_locker.unlock();
	}
	// push a pack and context tp queue.
	public void push(Object obj, mm_packet pack)
	{
		mm_mq_tcp_data data = new mm_mq_tcp_data();
		data.init();
		data.obj = obj;
		pack.copy_alloc(data.pack);
		// push to message queue.
		this.lock_queue.blpush(data);
	}
	// pop and trigger handle.
	public void pop()
	{
		mm_mq_tcp_data data = null;
		int n = 0;
		// lock queue size is lock free.and we not need check it thread safe.
		while( 0 != this.lock_queue.size && n <= this.max_pop )
		{
			data = (mm_mq_tcp_data)this.lock_queue.brpop();
			if (null != data)
			{
				mm_packet pack = data.pack;
				Object obj = data.obj;
				mm_net_tcp.mm_net_tcp_handle_callback handle = null;
				this.holder_locker.lock();
				handle = (mm_net_tcp.mm_net_tcp_handle_callback)this.holder.get(pack.phead.mid);
				this.holder_locker.unlock();
				if (null != handle)
				{
					handle.handle(obj, this.u, pack);
				}
				else
				{
					// if not define the handler,fire the default function.
					// android assertions are unreliable.
					// assert null != this.callback : "this.callback is a null.";
					this.callback.handle(obj, this.u, pack);
				}
				data.pack.free_copy_alloc();
				data.destroy();
				data = null;
			}
			n++;
		}
	}
}
