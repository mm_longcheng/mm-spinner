package mm.poller;

public class mm_poll_event
{	
	public static final int MM_PE_NONEABLE = 0;
	public static final int MM_PE_READABLE = 1;
	public static final int MM_PE_WRITABLE = 2;
	// java special.
	public static final int MM_PE_ECONNECT = 4;
	public static final int MM_PE_ACCEPTOR = 8;
	
	public Object s = null;
	public int mask = MM_PE_NONEABLE;// mask for MM_PE_NONEABLE | MM_PE_READABLE | MM_PE_WRITABLE
}
