package mm.core;

//we catch all Exception e but print nothing but catch the error code.
public class mm_time
{
	public static final int MM_MSEC_PER_SEC = 1000;
	public static final int MM_USEC_PER_SEC = 1000000;
	public static final int MM_NSEC_PER_SEC = 1000000000;
	
	public static void ssleep(int milliseconds)
	{
		try 
		{
			Thread.sleep(milliseconds * 1000);
		} 
		catch (Exception e) 
		{
			// do nothing here.we only need the finally code.
		}
	}
	public static void msleep(int milliseconds)
	{
		try 
		{
			Thread.sleep(milliseconds);
		} 
		catch (Exception e) 
		{
			// do nothing here.we only need the finally code.
		}
	}
}
