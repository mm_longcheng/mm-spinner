package mm.core;

//output sequence (put)
//+++++-------
//|    |      |
//0  pptr   size
///////////////////////
//input  sequence (get)
//+++++-------
//|    |      |
//0  gptr   size
public class mm_streambuf
{
	public static final int MM_STREAMBUF_PAGE_SIZE = 1024;
	
	// output sequence (put)
	public int pptr = 0;
	// input  sequence (get)
	public int gptr = 0;

	// max size for this streambuf.default is MM_STREAMBUF_PAGE_SIZE = 1024
	public int size = 0;
	public byte[] buff = null;// buffer for real data.
	
	public void init()
	{
		this.pptr = 0;
		this.gptr = 0;
		this.size = MM_STREAMBUF_PAGE_SIZE;
		this.buff = new byte[MM_STREAMBUF_PAGE_SIZE];
	}
	public void destroy()
	{
		this.pptr = 0;
		this.gptr = 0;
		this.size = 0;
		this.buff = null;
	}
	
	// copy q to p.
	public void copy(mm_streambuf q)
	{
		byte[] buff = new byte[q.size];
		System.arraycopy(q.buff,0,buff,0,q.size);
		this.buff = buff;
		this.size = q.size;
		// setp
		this.pptr = q.pptr;
		// setg
		this.gptr = q.gptr;
	}
	// add max streambuf size.not checking overflow,use it when overflow only.
	public void addsize(int size)
	{
		byte[] buff = new byte[this.size + size];
		System.arraycopy(this.buff,0,buff,0,this.size);
		this.buff = buff;
		this.size += size;
	}

	public void removeget()
	{
		int rsize = this.gptr;
		if (0 < rsize)
		{
			int wr = this.pptr;
			System.arraycopy(this.buff, rsize, this.buff, 0, this.pptr - this.gptr);
			wr -= rsize;
			// setp
			this.pptr = wr;
			// setg
			this.gptr = 0;
		}
	}
	public void removeget_size( int rsize )
	{
		int rdmax = 0;
		this.clearget();
		rdmax = this.pptr - this.gptr;
		if (rdmax < rsize)
		{
			rsize = rdmax;
		}
		if (0 < rsize)
		{
			int wr = this.pptr;
			System.arraycopy(this.buff, rsize, this.buff, 0, this.pptr - this.gptr);
			wr -= rsize;
			// setp
			this.pptr = wr;
			// setg
			this.gptr = 0;
		}
	}
	public void clearget()
	{
		// setg
		this.gptr = 0;
	}

	public void removeput()
	{
		int wsize = this.pptr;
		if (0 < wsize)
		{
			int rd = this.gptr;
			System.arraycopy(this.buff, wsize, this.buff, 0, this.pptr - this.gptr);
			rd -= wsize;
			// setp
			this.pptr = 0;
			// setg
			this.gptr = rd;
		}
	}
	public void removeput_size( int wsize )
	{
		int wrmax = 0;
		this.clearput();
		wrmax = this.pptr - this.gptr;
		if (wrmax < wsize)
		{
			wsize = wrmax;
		}
		if (0 < wsize)
		{
			int rd = this.gptr;
			System.arraycopy(this.buff, wsize, this.buff, 0, this.pptr - this.gptr);
			rd -= wsize;
			// setp
			this.pptr = 0;
			// setg
			this.gptr = rd;
		}
	}
	public void clearput()
	{
		// setp
		this.pptr = 0;
	}

	public int getsize()
	{
		return this.gptr;
	}
	public int putsize()
	{
		return this.pptr;	
	}

	// size for input and output interval.
	public int size()
	{
		return this.pptr - this.gptr;
	}

	// set get pointer to new pointer.
	public void setg_ptr( int new_ptr )
	{
		// setg
		this.gptr = new_ptr;
	}
	// set put pointer to new pointer.
	public void setp_ptr( int new_ptr )
	{
		// setp
		this.pptr = new_ptr;
	}
	// set get and put pointer to zero.
	public void reset()
	{
		// setp
		this.pptr = 0;
		// setg
		this.gptr = 0;
	}

	// get get pointer(offset).
	public int gptr()
	{
		return this.gptr;
	}
	// get put pointer(offset).
	public int pptr()
	{
		return this.pptr;
	}
	//////////////////////////////////////////////////////////////////////////
	// get data s + offset length is n.and gbump n.
	public int sgetn( byte[] s, int o, int n )
	{
		while( this.gptr + n > this.size )
		{
			this.underflow();
		}
		System.arraycopy(this.buff, this.gptr, s, o, n);
		this.gptr += n;
		return n;
	}
	// put data s + offset length is n.and pbump n.
	public int sputn( byte[] s, int o, int n )
	{
		while( this.pptr + n > this.size )
		{
			this.overflow();
		}
		System.arraycopy(s, o, this.buff, this.pptr, n);
		this.pptr += n;
		return n;
	}
	// gptr += n
	public void gbump( int n )
	{
		this.gptr += n;
	}
	// pptr += n
	public void pbump( int n )
	{
		this.pptr += n;
	}
	//////////////////////////////////////////////////////////////////////////
	public void underflow()
	{
		if(0 < this.gptr)
		{
			// if gptr have g data.we try remove for free space.
			this.removeget();
		}
		else
		{
			// if gptr no data we just add page size.
			this.addsize(MM_STREAMBUF_PAGE_SIZE);
		}
	}
	public void overflow()
	{
		this.addsize(MM_STREAMBUF_PAGE_SIZE);
	}
	// if p->gptr >= MM_STREAMBUF_PAGE_SIZE we realloc a thin memory.ohter we just remove get.make sure the p->pptr + n <= p->size
	public void try_decrease( int n )
	{
		int cursz = this.pptr - this.gptr;
		int fresz = this.size - cursz;
		// free size > n and MM_STREAMBUF_PAGE_SIZE < fresz - n.
		if ( fresz > n && MM_STREAMBUF_PAGE_SIZE < fresz - n )
		{
			// the surplus size for decrease.
			int ssize = fresz - n;
			// memmove size is <  MM_STREAMBUF_PAGE_SIZE.
			// decrease
			byte[] buff = null;
			// use memove to remove the already get buffer.
			int wr = this.pptr;
			this.size -= (ssize / MM_STREAMBUF_PAGE_SIZE) * MM_STREAMBUF_PAGE_SIZE;
			buff = new byte[this.size];
			System.arraycopy(this.buff, this.gptr, buff, 0, cursz);
			this.buff = buff;
			wr -= this.gptr;
			// setp
			this.pptr = wr;
			// setg
			this.gptr = 0;
		}
		else
		{
			// use memove to remove the already get buffer.
			this.removeget();
		}
	}
	// check overflow when need write size n.
	public void try_increase( int n )
	{
		if ( this.pptr + n > this.size )
		{
			// the increase process is here.
			// assert( p->pptr + n > p->size && "(p->pptr + n > p->size) is invalid.");
			// size_t dsz = p->pptr + n - p->size;
			// assert( 0 < dsz );
			// size_t asz = dsz / MM_STREAMBUF_PAGE_SIZE;
			// size_t bsz = dsz % MM_STREAMBUF_PAGE_SIZE;
			// assert( 0 != bsz );
			// size_t nsz = ( asz + ( 0 == bsz ? 0 : 1 ) );
			// assert( 0 < nsz );
			// mm_streambuf_addsize(p, nsz * MM_STREAMBUF_PAGE_SIZE);

			// the quick process is here.
			int dsz = this.pptr + n - this.size;
			int asz = dsz / MM_STREAMBUF_PAGE_SIZE;
			int nsz = asz + 1;
			this.addsize( nsz * MM_STREAMBUF_PAGE_SIZE );
		}
	}
	// aligned streambuf memory if need sputn n size buffer before.
	public void aligned_memory( int n )
	{
		// if current pptr + n not enough for target n.we make a aligned memory.
		if ( this.pptr + n > this.size )
		{
			int cursz = this.pptr - this.gptr;
			// the free size is enough and memmove size is <  MM_STREAMBUF_PAGE_SIZE.
			if ( this.size - cursz >= n && MM_STREAMBUF_PAGE_SIZE > cursz)
			{
				this.try_decrease( n );
			} 
			else
			{
				this.try_increase( n );
			}
		}
	}
}
