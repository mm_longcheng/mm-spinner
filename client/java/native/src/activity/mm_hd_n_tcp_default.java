package activity;

import cobweb.mm_protobuf;
import mm.core.mm_logger;
import mm.net.mm_gl_tcp;
import mm.net.mm_net_tcp;
import mm.net.mm_packet;
import mm.net.mm_tcp;
import protodef.nano.s_control;

public class mm_hd_n_tcp_default
{
	static class hd_n_tcp_broken implements mm_net_tcp.mm_net_tcp_handle_callback
	{
		@Override
		public void handle(Object obj, Object u, mm_packet pack) 
		{
		    mm_tcp tcp = (mm_tcp)(obj);
		    mm_logger.logW("n tcp:" + tcp.addr.to_string() +" is broken.");
		}
	}
	static class hd_n_tcp_nready implements mm_net_tcp.mm_net_tcp_handle_callback
	{
		@Override
		public void handle(Object obj, Object u, mm_packet pack) 
		{
		    mm_tcp tcp = (mm_tcp)(obj);
		    mm_logger.logW("n tcp:" + tcp.addr.to_string() +" is nready.");
		}
	}
	static class hd_n_tcp_finish implements mm_net_tcp.mm_net_tcp_handle_callback
	{
		@Override
		public void handle(Object obj, Object u, mm_packet pack) 
		{
			mm_tcp tcp = (mm_tcp)obj;
			mm_logger.logI("n tcp:" + tcp.addr.to_string() +" is finish.");
			//
			mm_gl_tcp gl_tcp = (mm_gl_tcp)(tcp.callback.obj);
			//
			long uid = 9008000000009999L;
			s_control.get_logger_rq rq = new s_control.get_logger_rq();
			//
			// assign rq.
			//
			StringBuilder value = new StringBuilder();
			mm_packet rq_pack = new mm_packet();
			for (int j = 0; j < 10; j++) 
			{
				mm_protobuf.q_tcp_flush_message_append(gl_tcp, uid, s_control.get_logger_rq.id, rq, rq_pack);
				//
				value.setLength(0);
				mm_protobuf.logger_append_packet_message(value, rq_pack, rq);
				mm_logger.logI("nt rq " + value.toString());
				mm_protobuf.q_tcp_flush_signal(gl_tcp);
			} 
		}
	}
}
