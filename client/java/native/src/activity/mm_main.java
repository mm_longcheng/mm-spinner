package activity;

import mm.core.mm_logger;

public class mm_main
{
	private static final String TAG = mm_main.class.getSimpleName();
	static class __signal_destroy extends Thread 
	{
       public void run() 
       {
    	   mm_application app = mm_application.instance();
    	   app.shutdown();
    	   mm_logger.logI(TAG + "signal_destroy.");
       } 
	};
	public static void main(String[] args) 
	{
		Runtime.getRuntime().addShutdownHook(new __signal_destroy());
		try 
		{
			mm_application app = mm_application.instance();
			app.init();
			app.initialize(args);
			app.start();
			app.join();
			app.destroy();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
    }
}
