package activity;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Arrays;

import cobweb.mm_protobuf;
import mm.core.mm_logger;
import mm.core.mm_socket;
import mm.core.mm_streambuf;
import mm.net.mm_gl_udp;
import mm.net.mm_gl_tcp;
import mm.net.mm_net_udp;
import mm.net.mm_packet;
import mm.net.mm_sockaddr;
import mm.net.mm_tcp;

import mm.security.mm_chacha20;
import mm.security.mm_rc4;
import protodef.nano.s_control;
import sun.misc.BASE64Encoder;
import protodef.nano.b_math;
import protodef.nano.b_network;
import protodef.nano.c_account;
import protodef.nano.c_entry;
import protodef.nano.c_relation;

public class mm_application 
{
	private static final String TAG = mm_application.class.getSimpleName();
	
	public String[] arg = null;
	// alloc.
	public mm_gl_tcp gl_tcp = new mm_gl_tcp();
	
    private volatile static mm_application _instance = null;
	static public mm_application instance()
	{
		 if ( null == _instance)
		 {  
			 synchronized (mm_application.class)
			 {
				 if ( null == _instance)
				 {
					 _instance = new mm_application();
				 }
			 }
		 }
		return _instance;
	}
	// rerutn null if not instance;
	static public mm_application instance_weak()
	{
		return _instance;
	}
	public void rc4()
	{
		byte[] key = {1,2,3,5,9,0,7,8,1,2,3,5,9,0,7,8,1,2,3,5,9,0,7,8,1,2,3,5,9,0,7,8,}; // must 32.
        byte[] plain0 = "testingtestingtestingtestingtestingtestingtestingtestingtestingtesting".getBytes();
		
		byte[] plain1 = new byte[plain0.length];
		byte[] plain2 = new byte[plain0.length];
		byte[] plain3 = new byte[plain0.length];
		byte[] plain4 = new byte[plain0.length];
		
		mm_rc4 cipher0 = new mm_rc4();
		mm_rc4 cipher1 = new mm_rc4();
		mm_rc4 cipher2 = new mm_rc4();
		mm_rc4 cipher3 = new mm_rc4();
		cipher0.set_key(key, 0, key.length);
		cipher1.set_key(key, 0, key.length);
		cipher2.set_key(key, 0, key.length);
		cipher3.set_key(key, 0, key.length);
		mm_logger.logI(TAG + " data:" + Arrays.toString(cipher0.data));
		
		cipher0.encrypt(plain0, 0, plain1, 0, plain0.length);
		cipher1.decrypt(plain1, 0, plain2, 0, plain1.length);
		
		System.arraycopy(plain1, 0, plain3, 0, plain1.length);
		cipher2.decrypt(plain3, 0, plain3, 0, plain3.length);
		
		System.arraycopy(plain1, 0, plain4, 0, plain1.length);
		cipher3.decrypt(plain4, 0, plain4, 0, 3);
		cipher3.decrypt(plain4, 3, plain4, 3, 4);
		cipher3.decrypt(plain4, 7, plain4, 7, plain1.length-7);
		
		
		mm_logger.logI(TAG + " plain0:" + Arrays.toString(plain0));
		mm_logger.logI(TAG + " plain1:" + Arrays.toString(plain1));
		mm_logger.logI(TAG + " plain2:" + Arrays.toString(plain2));
		mm_logger.logI(TAG + " plain3:" + Arrays.toString(plain3));
		mm_logger.logI(TAG + " plain4:" + Arrays.toString(plain4));
		mm_logger.logI(TAG + " ");
	}
	public void chacha20_coder()
	{
		byte[] key = {1,2,3,5,9,0,7,8,1,2,3,5,9,0,7,8,1,2,3,5,9,0,7,8,1,2,3,5,9,0,7,8,}; // must 32.
		byte[] nonce = {0,7,1,8,0,7,1,8,};// must 8.
        byte[] plain0 = "testing".getBytes();
		
		byte[] plain1 = new byte[plain0.length];
		byte[] plain2 = new byte[plain0.length];
		
		mm_chacha20 cipher0 = new mm_chacha20();
		mm_chacha20 cipher1 = new mm_chacha20();
		cipher0.keysetup(key);
		cipher0.ivsetup(nonce);
		cipher1.keysetup(key);
		cipher1.ivsetup(nonce);
		
		cipher0.encrypt(plain1, plain0, plain0.length);
		cipher1.decrypt(plain2, plain1, plain1.length);
		
		mm_logger.logI(TAG + " plain0:" + plain0.toString());
		mm_logger.logI(TAG + " plain1:" + plain1.toString());
		mm_logger.logI(TAG + " plain2:" + plain2.toString());	
	}
	static class hd_client_entry_q_c_entry_knock_rs implements mm_net_udp.mm_net_udp_handle_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack, mm_sockaddr remote ) 
		{
			c_entry.knock_rs rs_msg = new c_entry.knock_rs();
			StringBuilder proto_desc = new StringBuilder();			
			if(0 == mm_protobuf.decode_message(pack, rs_msg))
			{
				mm_protobuf.logger_append_packet_message(proto_desc, pack, rs_msg);
				mm_logger.logI("q tcp nt: " + proto_desc.toString());
				// handle event.
				//////////////////////////////////////////////////////////////////////////
				//////////////////////////////////////////////////////////////////////////
			}
			else
			{
				mm_protobuf.logger_append_packet(proto_desc, pack);
				mm_logger.logW("q tcp nt: " + proto_desc.toString());
			}
		}
	}
	public static void hd_client_entry_q_c_entry_knock_rq(mm_net_udp net_udp)
	{
		c_entry.knock_rq rq_msg = new c_entry.knock_rq();
		mm_packet rq_pack = new mm_packet();
		StringBuilder proto_desc = new StringBuilder();
		//
		long uid = 9008000000004998L;
		long msec_current = System.currentTimeMillis();
		String source_version = Long.toString(msec_current);
		////////////////////////////////
		b_math.coord coord_info = new b_math.coord();
		rq_msg.coordInfo = coord_info;
		coord_info.j = 1;
		coord_info.w = 2;
		rq_msg.nativeClientVersion = "";
		rq_msg.nativeSourceVersion = source_version;
		//////////////////////////////////////////////////////////////////////////
		// send rq.
		mm_protobuf.q_udp_append_rq( net_udp, uid, c_entry.knock_rq.id, rq_msg, rq_pack );
		mm_protobuf.n_net_udp_flush_send(net_udp);
		// logger rq.
		mm_protobuf.logger_append_packet_message( proto_desc, rq_pack, rq_msg );
		mm_logger.logI("q tcp rq: " + proto_desc);
	}
	public void init()
	{
		{
			mm_streambuf streambuf = new mm_streambuf();
			mm_packet nt_pack = new mm_packet();
			streambuf.init();
			
			c_relation.open_guard_nt nt_msg = new c_relation.open_guard_nt();

			nt_msg.uid = 2222333;
			nt_msg.zid = 3333222;
			nt_msg.guardName = "sssf";
			nt_msg.guardType = 2;

			mm_protobuf.encode_message_streambuf_packet(streambuf, nt_msg, nt_pack, c_relation.open_guard_nt.id, -1, -1);
			byte[] buffer = streambuf.buff;
			int offset = streambuf.gptr;
			int length = streambuf.size();
			StringBuilder value = new StringBuilder();
			StringBuffer sbf = new StringBuffer();
			ByteBuffer byte_buffer = ByteBuffer.wrap(buffer, offset, length);
			byte[] bbh = byte_buffer.array();
			String aa = byte_buffer.toString();
			String buff = new String(buffer,offset,length);
			byte[] bytes = buff.getBytes();
			byte[] new_arr = new byte[length];
			System.arraycopy(buffer,offset,new_arr,0,length);
			mm_logger.logI("buff:" + buff);
			streambuf.destroy();
		}
		{
			mm_net_udp net_udp = new mm_net_udp();
			net_udp.init();
			net_udp.assign_remote("47.93.118.164", 20100);
			net_udp.assign_context(net_udp);
			net_udp.assign_callback(c_entry.knock_rs.id,new hd_client_entry_q_c_entry_knock_rs());
			net_udp.start();
			
			net_udp.check();
			for(int i = 0 ;i<1;i++)
			{
				hd_client_entry_q_c_entry_knock_rq(net_udp);
			}
			
			
			net_udp.join();
			net_udp.destroy();
		}
		{
			byte[] delims = {-62,-89};
			
			BASE64Encoder b64 = new BASE64Encoder();//.decodeBuffer(key)

			String str = null;
			try 
			{
				str = new String(delims, "UTF-8");
			} catch (UnsupportedEncodingException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String bufplain2 = 1 + str + 2 + str + 3;
			String bufplain1 = b64.encode(bufplain2.getBytes());
			String bufcoded1 = b64.encode(bufplain1.getBytes());
			
			mm_logger.logI(TAG + " bufplain2:" + bufplain2);
			mm_logger.logI(TAG + " bufplain1:" + bufplain1);
			mm_logger.logI(TAG + " bufcoded1:" + bufcoded1);
		}
		//
		mm_logger.assign_callback(new mm_logger_callback_printf());
		// init.
		this.gl_tcp.init();
		mm_logger.logI(TAG + " init.");
		
		rc4();
		// chacha20_coder();
	}
	public void destroy()
	{
		mm_logger.logI(TAG + " destroy.");
		// destroy.
		this.gl_tcp.destroy();
	}
	public void thread_handle()
	{
		this.gl_tcp.thread_handle();
	}
	static class mm_hd_n_c_entry_knock_rs implements mm_net_udp.mm_net_udp_handle_callback
	{
		@Override
		public void handle( Object obj, Object u, mm_packet pack, mm_sockaddr remote ) 
		{
			c_entry.knock_rs rs = new c_entry.knock_rs();
			StringBuilder value = new StringBuilder();
			if(0 == mm_protobuf.decode_message(pack, rs))
			{
				mm_protobuf.logger_append_packet_message(value, pack, rs);
				mm_logger.logI("qt rs " + value.toString());
				// handle event.
			}
			else
			{
				mm_protobuf.logger_append_packet(value, pack);
				mm_logger.logW("qt rs " + value.toString());
			}
		}
	}
	static void mm_hd_n_c_entry_knock_rq(mm_gl_udp gl_udp)
	{
		long uid = 9008000000009999L;
		c_entry.knock_rq rq = new c_entry.knock_rq();
		//
		// assign rq.
		b_math.coord coordInfo = new b_math.coord();
		rq.coordInfo = coordInfo;
		rq.nativeClientVersion = "1.0.0";
		rq.nativeSourceVersion = "1.0.0";
		coordInfo.j = 1;
		coordInfo.w = 2;
		//
		StringBuilder value = new StringBuilder();
		mm_packet rq_pack = new mm_packet();
		
		mm_protobuf.q_udp_flush_message_append(gl_udp, uid, c_entry.knock_rq.id, rq, rq_pack);
		//
		value.setLength(0);
		mm_protobuf.logger_append_packet_message(value, rq_pack, rq);
		mm_logger.logI("nt rq " + value.toString());
		mm_protobuf.q_udp_flush_signal(gl_udp);
	}
	static void udp_entry()
	{
		mm_gl_udp gl_udp = new mm_gl_udp();
		gl_udp.init();
		gl_udp.assign_remote("47.93.118.164", 20100);
		gl_udp.assign_remote_target("47.93.118.164", 20100);
		gl_udp.assign_state_check_flag(mm_gl_udp.gl_udp_check_activate);
		gl_udp.assign_n_callback(c_entry.knock_rs.id, new mm_hd_n_c_entry_knock_rs());

		gl_udp.start();
		
		mm_hd_n_c_entry_knock_rq(gl_udp);
		
		gl_udp.join();
		gl_udp.destroy();
	}
	public void initialize(String[] args)
	{
		String message = String.format("mid:0x%08X udp:%s not handle.",1,"127.0.0.1-444");
		mm_logger.logW(message);
		// udp_entry();
		//
		this.arg = args;
		mm_logger.logI(TAG + " initialize begin.");
		// assign attributes.
		this.gl_tcp.assign_state_check_flag(mm_gl_tcp.gl_tcp_check_activate);
		// this.gl_tcp.assign_remote_target("::1", 55000);
		// this.gl_tcp.assign_remote_target("240c:f:1:6000::2697", 55000);
		// this.gl_tcp.assign_remote_target("2001:470:c:48e::2", 55000);		
		this.gl_tcp.assign_remote_target("101.200.169.28", 55000);
		// this.gl_tcp.assign_remote_target("fe80::216:3eff:fe00:3c6", 55000);
		// this.gl_tcp.assign_remote_target("::1", 55000);
		this.gl_tcp.assign_context(this);
		
		// default handle.
		this.gl_tcp.assign_n_default_callback(new mm_hd_n_gl_tcp_callback());
		this.gl_tcp.assign_q_default_callback(new mm_hd_q_gl_tcp_callback());
		// crypto handle.
		this.gl_tcp.assign_crypto_callback(new hd_gl_tcp_crypto_callback());
		// net thread event handle.
		this.gl_tcp.assign_n_callback(mm_tcp.tcp_mid_broken,new mm_hd_n_tcp_default.hd_n_tcp_broken());
		this.gl_tcp.assign_n_callback(mm_tcp.tcp_mid_nready,new mm_hd_n_tcp_default.hd_n_tcp_nready());
		this.gl_tcp.assign_n_callback(mm_tcp.tcp_mid_finish,new mm_hd_n_tcp_default.hd_n_tcp_finish());
		// queue thread event handle.
		this.gl_tcp.assign_q_callback(mm_tcp.tcp_mid_broken,new mm_hd_q_tcp_default.hd_q_tcp_broken());
		this.gl_tcp.assign_q_callback(mm_tcp.tcp_mid_nready,new mm_hd_q_tcp_default.hd_q_tcp_nready());
		this.gl_tcp.assign_q_callback(mm_tcp.tcp_mid_finish,new mm_hd_q_tcp_default.hd_q_tcp_finish());
		
		// logic net thread event handle.
		this.gl_tcp.assign_n_callback(s_control.get_logger_rs.id, new mm_hd_n_s_control_get_logger_rs());
		// logic queue thread event handle.
		this.gl_tcp.assign_q_callback(s_control.get_logger_rs.id, new mm_hd_q_s_control_get_logger_rs());
		
		mm_logger.logI(TAG + " initialize end.");
	}
	public void start()
	{
		this.gl_tcp.start();
		mm_logger.logI(TAG + " start.");
	}
	public void interrupt()
	{
		this.gl_tcp.interrupt();
		mm_logger.logI(TAG + " interrupt.");
	}
	public void shutdown()
	{
		this.gl_tcp.shutdown();
		mm_logger.logI(TAG + " shutdown.");
	}
	public void join()
	{
		this.gl_tcp.join();
		mm_logger.logI(TAG + " join.");
	}
}
