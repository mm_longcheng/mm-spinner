##directory
###design
document for project.
###make
common make file template.
###depend
depend third party lib.include source and build project.
###common
cross-platform lib for net.db tool common lib.
###tools
depend tools.
###protobuff
protobuff define project.
###module
logic application implement.
###resource
config file.
###script
generator,compile and tool script.

##compile

1.asm tools
windows nasm add ./depend/tools/NASM to PATH 

cp ./depend/tools/vs2012_nasm/* to
Program Files (x86)\MSBuild\Microsoft.Cpp\v4.0\V110\BuildCustomizations

2.ENV
```
MAKE_HOME=target to ./make directory
MYSQL_HOME=target to mysql directory
```
windows ENV
```
VLD_HOME=target to vld directory
```

3.linux
make sure mysql soft link to correct directory.
```
ubuntu
lrwxrwxrwx 1 longer longer 18 Jun  5 21:05 include -> /usr/include/mysql
lrwxrwxrwx 1 longer longer 25 Jun  5 21:06 lib -> /usr/lib/x86_64-linux-gnu
linux
lrwxrwxrwx 1 longer longer 18 May 17  2016 include -> /usr/include/mysql
lrwxrwxrwx 1 longer longer 16 May 17  2016 lib -> /usr/lib64/mysql
```
launch proj script compile_<platform>.xxx